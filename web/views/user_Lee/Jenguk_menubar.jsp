<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
/* left 메뉴 */
.leftblock {
	float: left;
	text-align: left
}

#left2, #left3, #left4 {
	margin-top: 10px;
}

#left1 {
	width: 200px;
	height: 170px;
	margin-top: 10px;
	align: left;
}

.myPage {
	margin-left: 10px;
}

#left2 {
	height: 290px;
}

#left3 {
	height: 260px;
}

#left4 {
	height: 250px;
}

/* left메뉴 가로선 */
hr {
	width: 90%;
	color: lightgray;
	size: 2px;
	margin-left: 5%;
}

ul {
	list-style: none;
}

.leftMenu {
	float: left;
	width: 20%;
	margin-left: 30px;
	margin-top: 10px;
}

/* 내용 부분 */
/* section{width: 75%; height: 1500px; background-color: #ffca28;} */
.leftMenu>ul {
	margin: 10;
	padding: 0;
	list-style: none;
}

.leftMenu a {
	display: block;
	text-decoration: none;
	padding: 10px 20px;
	color: black;
}

.left {
	border: 2px solid rgb(192, 192, 192);
}

.wrap {
	width: 1200px;
	margin: 0 auto
}


li:hover {font-weight: bold}

</style>
</head>
<body>
	<aside class="leftMenu">
		<div class="left leftblock" id="left1">
			<br>&nbsp; <label class="myPage" style="font-size: 20px;"><b>열린마당</b></label>
			<br>
			<br>
			<hr>
			<ul>
				<li><a href="#" id="jenguk" style="font-size: 18px; color: #2fa599"><b>전국보금자리 자랑</b></a></li>
				<hr>
				<li><a href="<%=request.getContextPath()%>/loadFront.faq" id="jenguk" style="font-size: 18px;"><b>자주묻는질문</b></a></li>
			</ul>
		</div>
	</aside>
</body>
</html>