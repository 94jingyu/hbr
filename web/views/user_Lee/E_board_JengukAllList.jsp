<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.ArrayList, java.util.HashMap, com.kh.hbr.board.jengukBoard.model.vo.*"%>
<%
	ArrayList<HashMap<String, Object>> list 
		= (ArrayList<HashMap<String, Object>>) request.getAttribute("list");

	ArrayList<HashMap<String, Object>> bestList 
		= (ArrayList<HashMap<String, Object>>) request.getAttribute("bestList");

	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
	int limit = pi.getLimit();

%>  
<!DOCTYPE html>         
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>  
/* * {text-align:left} */
/* 상단메뉴바 */
.topMenu {
	/* display: inline-block; */
	width: 100%;
	height: 50px;
	/* background: #013252; */
	line-height: 30px; /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	vertical-align: middle; /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	text-align: center; /* 글씨 정렬을 가운데로 설정 */
	font-weight: bolder;
	font-size: 20px;
	color: #013252;
}


/*header*/
.page_wrap {
	width: 930px;
	float: right;
	
}

.mypage_root {
	padding: 0 0 20px 0;
	text-align: left;
}

.header_top {
	background: #013252;
	padding: 20px;
	margin-bottom: 20px;
	text-align: left;
}

.header_top p {
	color: #fff;
	font-size: 20px;
	font-weight: bold
}

/* 진규 추가 */
.searchBox {width: 930px; background: #ecf0f7;}
.searchTable {margin: 0 auto; padding: 30px; padding-top: 10px;}
#searchBtn {width: 90px; height: 40px; text-align:center; font-size: 15px; background: #013252;
	color: white; padding: 5px; border-radius: 1px; border: 1px; outline: 1px;}
.writeBtn {width: 90px; height: 40px; text-align:center; font-size: 15px; background: #013252;
	color: white; padding: 5px; border-radius: 1px; border: 1px; outline: 1px;}
#searchBtn, .writeBtn:hover {cursor: pointer;}
.searchSelect {width: 120px; height: 40px; font-size: 20px;}
.searchTable td {padding: 3px; }

/* 소문난 보금자리 */
.topBoard{background: #ecf0f7; width: 930px; height: 100%}
.topTable{padding: 30px; padding-top: 10px; width: 100%;}
.topList{width:265px; height: 250px; background: white; margin:10px; border:1px solid black;}
.topDetail{width:100%}
.topList:hover{cursor: pointer; background: lightgray;}
.newList:hover{cursor: pointer; background: lightgray;}
/* 등록된 보금자리 */
.newBoard{background: #f1f3f4; width: 930px; height: 100%}
.newTable{padding: 30px; padding-top: 10px; width: 100%;}
.newList{width:265px; height: 250px; background: white; margin:10px; border:1px solid black;}
.newDetail{width:100%}
.titleDetail{font-size: 22px;}
.cNameDetail{font-size: 15px;}
.jobDetail{font-size: 15px;}
.scrap_img{width: 20px; height: 20px;}
.img{width: 17px; height: 17px; margin-left: 30px; padding: 0}
.likeCount{color: #0A74EC}
.dislikeCount{color: #FB3A4A}
</style>
</head>
<body>
<div id="wrap">
		<!-- 로고 영역 -->
		<%@ include file="../common/E_user_menubar.jsp" %>
		<hr style="border: solid 2px #013252; width:100%; margin:0 auto;"><br>
		
		<!-- 좌측 메뉴마 -->
		<%@ include file="../user_Lee/Jenguk_menubar.jsp" %><br>
			
    <div class="page_wrap">
        <header>
            <div class="mypage_root">
                <span>HOME</span>
                <span>> 열린마당</span>
                <span>> 전국보금자리자랑</span>
            </div>
            <div class="header_top">
                <p style="font-size: 30px">전국보금자리자랑</p>
            </div>
        </header> 
        
        <div class="searchBox">
        <div style="text-align:left; font-size: 20px; padding: 10px; padding-bottom: 0px;"><b>검색조건</b></div>
					<form id="searchForm" action="<%=request.getContextPath()%>/searchJenguk.bo" method="get">
						<table class="searchTable">
							<tr>
								<td style="width:80px; text-align:center">지역선택</td>
								<td colspan="2">
									<select class="searchSelect" name="selectLocation" style="width: 200px; height: 40px; font-size: 15px;">
										<option selected value="all">전체</option>
										<option value="서울">서울</option>
										<option value="경기">경기</option>
										<option value="인천">인천</option>
										<option value="부산">부산</option>
										<option value="대구">대구</option>
										<option value="광주">광주</option>
										<option value="대전">대전</option>
										<option value="울산">울산</option>
										<option value="세종">세종</option>
										<option value="강원">강원</option>
										<option value="경남">경남</option>
										<option value="경북">경북</option>
										<option value="전남">전남</option>
										<option value="전북">전북</option>
										<option value="충남">충남</option>
										<option value="충북">충북</option>
										<option value="제주">제주</option>
									</select>
								</td>
								<td style="width: 80px; text-align:center">직종선택</td>
								<td colspan="2">
									<select class="searchSelect" name="selectJob" style="width: 240px; height: 40px; font-size: 15px;">
										<option selected value="all">전체</option>
										<option value="요리/서빙">요리/서빙</option>
										<option value="간호/의료">간호/의료</option>
										<option value="생산/기술">생산/기술</option>
										<option value="사무/경리">사무/경리</option>
										<option value="운전/배달">운전/배달</option>
										<option value="상담/영업">상담/영업</option>
										<option value="매장관리">매장관리</option>
										<option value="교사/강사">교사/강사</option>
										<option value="서비스/기타">서비스/기타</option>
									</select>
								</td>
								<td rowspan="2" style="width: 90px; text-align: center">
									<button type="submit" id="searchBtn" class="btn">검색</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
        <br>
        <div class="topBoard"><!--  topBoard -->
	        <div style="text-align:left; padding: 20px; padding-bottom: 0px;">
	        	<span  style="font-size: 30px;"><b>소문난 보금자리</b></span>&nbsp;&nbsp;
	        	<span>매달 1일 소문난 보금자리로 선정시 소정의 선물을 보내드립니다. </span>
	        </div>
			
			<!-- 개별 리스트 -->
			<table class="topTable">
				<tr>
				
				
				<% for(int i = 0; i < bestList.size(); i++) { 
      				HashMap<String, Object> hmap = bestList.get(i); %>
				
					<td> <!-- 개별 리스트 -->
						<div class="topList">
							<table class="topDetail" style="padding: 5px;">
								<tr>
									<td style="text-align:right; font-size: 18px;" hidden>
										<input type="hidden" class="jengukNo" value="<%= hmap.get("jengukNo") %>">
									</td>
									<td rowspan="2" colspan="6" style="width:160px;">
										<% int score = (Integer) hmap.get("score"); %>
										
										<% for(int s = 0; s < score; s++) {%>
										 <span><img class="scrap_img" alt="" src="<%=request.getContextPath()%>/static/images/common/star.png"></span>
										<% } %>
										<% for(int e = 0; e < (5-score); e++) {%>
										 <span><img class="scrap_img" alt="" src="<%=request.getContextPath()%>/static/images/common/star_em.png"></span>
										<% } %>
									</td>
									<td colspan="4" style="text-align:right; font-size: 18px;" ><b><%= hmap.get("jengukArea") %></b></td>
								</tr>
								<tr>
									<td colspan="4"style="text-align:right; color: #2fa599"><b><%= hmap.get("jobChoice") %></b></td>
								</tr>
								<tr>
									<td colspan="10" style="padding: 10px">
										<br><p class=titleDetail><b><%= hmap.get("title") %></b><p>
									</td>
								</tr>
								<tr>	
									<td colspan="10" style="padding-left: 10px">
										<p class=cNameDetail><%= hmap.get("companyName") %><p>
									</td>
								</tr>
								<tr>
									<td colspan="10" style="padding-left: 10px; padding-bottom: 10px">
										<p class=jobDetail><%= hmap.get("task") %><p><br>
									</td>
								</tr>
								<tr>
									<td colspan="3"><%= hmap.get("memberId") %></td>
									<td colspan="4"></td>
									<td colspan="2">
										<img class="btn img like" alt="" src="<%=request.getContextPath()%>/static/images/user/like.png">
									</td>
									<td><p class="<%= hmap.get("jengukNo") %>_bestLikeCount">0</p></td>
								</tr>
								<tr>
									<td colspan="3"><%= hmap.get("jengukEnrollDate") %></td>
									<td colspan="4">조회수: <%= hmap.get("boardCount") %></td>
									<td colspan="2">
										<img class="btn img dislike" alt="" src="<%=request.getContextPath()%>/static/images/user/dislike.png">
									</td>
									<td><p class="dislike <%= hmap.get("jengukNo") %>_bestHateCount">0</p></td>
								</tr>
							</table> 
						</div>
					</td><!-- /개별 리스트 -->
				<% } %>

			</table>
        </div> <!-- / topBoard -->
		<br>
		
		<!-- //////////////////////////////////////// -->
        <div class="newBoard"><!--  newBoard -->
	        <div style="text-align:left; padding: 20px; padding-bottom: 0px;">
	        	<span  style="font-size: 30px;"><b>등록된 보금자리</b></span>&nbsp;&nbsp;
	        	<span>매달 1일 추첨을 통해 3분께 소정의 선물을 보내드립니다. </span>
	        </div>
			
			<!-- 개별 리스트 -->
			<table class="newTable">
				<tr>
					<% for(int i = 0; i < list.size(); i++) { 
      				HashMap<String, Object> hmap = list.get(i); %>
				
					<td> <!-- 개별 리스트 -->
						<div class="newList">
							<table class="newDetail" style="padding: 5px;">
								<tr>
									<td style="text-align:right; font-size: 18px;" hidden>
										<input type="hidden" class="jengukNo" value="<%= hmap.get("jengukNo") %>">
									</td>
									<td rowspan="2" colspan="6" style="width:160px;">
										<% int score = (Integer) hmap.get("score"); %>
										
										<% for(int s = 0; s < score; s++) {%>
										 <span><img class="scrap_img" alt="" src="<%=request.getContextPath()%>/static/images/common/star.png"></span>
										<% } %>
										<% for(int e = 0; e < (5-score); e++) {%>
										 <span><img class="scrap_img" alt="" src="<%=request.getContextPath()%>/static/images/common/star_em.png"></span>
										<% } %>
									</td>
									<td colspan="4" style="text-align:right; font-size: 18px;" ><b><%= hmap.get("jengukArea") %></b></td>
								</tr>
								<tr>
									<td colspan="4" style="text-align:right; color: #2fa599; "><b><%= hmap.get("jobChoice") %></b></td>
								</tr>
								<tr>
									<td colspan="10" style="padding: 10px">
										<br><p class=titleDetail><b><%= hmap.get("title") %></b><p>
									</td>
								</tr>
								<tr>	
									<td colspan="10" style="padding-left: 10px">
										<p class=cNameDetail><%= hmap.get("companyName") %><p>
									</td>
								</tr>
								<tr>
									<td colspan="10" style="padding-left: 10px; padding-bottom: 10px">
										<p class=jobDetail><%= hmap.get("task") %><p><br>
									</td>
								</tr>
								<tr>
									<td colspan="3"><%= hmap.get("memberId") %></td>
									<td colspan="4"></td>
									<td colspan="2">
										<img class="btn img like" alt="" src="<%=request.getContextPath()%>/static/images/user/like.png">
									</td>
									<td><p class="<%= hmap.get("jengukNo") %>_likeCount">0</p></td>
								</tr>
								<tr>
									<td colspan="3"><%= hmap.get("jengukEnrollDate") %></td>
									<td colspan="4">조회수: <%= hmap.get("boardCount") %></td>
									<td colspan="2">
										<img class="btn img dislike" alt="" src="<%=request.getContextPath()%>/static/images/user/dislike.png">
									</td>
									<td><p class="dislike <%= hmap.get("jengukNo") %>_hateCount">0</p></td>
								</tr>
							</table> 
						</div>
					</td><!-- /개별 리스트 -->
				 	<% if((i+1) % 3 == 0) {%>
					<tr><td><br></td></tr>
					<% } %>
					
				<% } %>
				</tr>
					
					
			</table>
        </div> <!-- / newBoard -->
		<br>
	
	
	
	
	 <%-- 페이지 처리 --%>
     <div class="pagingArea" align="center">
        <button onclick="location.href='<%=request.getContextPath()%>/selectJenguk.tn?currentPage=1'"><<</button>
        <% if(currentPage <= 1) { %>
        <button disabled><</button>
        <% } else { %>
        <button onclick="location.href='<%=request.getContextPath()%>/selectJenguk.tn?currentPage=<%=currentPage - 1%>'"><</button>
        <% } %>
        
        <% for(int p = startPage; p <= endPage; p++) { 
        	  if(p == currentPage){
        %>
        		<button disabled><%= p %></button>
        <%
        	  } else {
        %>
        		<button onclick="location.href='<%=request.getContextPath()%>/selectJenguk.tn?currentPage=<%=p%>'"><%= p %></button>
        <% 
        	  }
           }
        %>
      
      	
      	<% if(currentPage >= maxPage) { %>
      	<button disabled>></button>
      	<% } else { %>
      	<button onclick="location.href='<%=request.getContextPath()%>/selectJenguk.tn?currentPage=<%=currentPage + 1%>'">></button>
      	<% } %>
      	
      	<button onclick="location.href='<%=request.getContextPath()%>/selectJenguk.tn?currentPage=<%=maxPage%>'">>></button>
      	
      	
      	
	
		<div><button type="button" class="writeBtn" style="float:right">글쓰기</button></div>
		<br><br><br>


		</div>
	</div>
        
<script>
$(function() {
	$(".writeBtn").click(function(){
		<% if(bloginUser != null) { %>
			alert("개인회원만 이용할 수 있습니다.");	
			<%	} else if(loginUser != null) {%>
				location.href = "<%=request.getContextPath()%>/views/user_Lee/E_board_JengukEnroll.jsp";
		<% } else { %>
			alert("로그인 후 이용하실 수 있습니다.");
			
			var check = window.confirm("로그인 하시겠습니까?");
			
			if(check) {
				location.href = "<%=request.getContextPath()%>/views/common/E_LoginPage.jsp";
			}
		<% } %> 			
	});
});

<%-- $(function() {
	$(".topList").click(function(){
		location.href = "<%=request.getContextPath()%>/views/user_Lee/E_board_JengukDetail.jsp";
	});
}); --%>


$(function() {
	
		$(".topList").click(function(){
			
			<% if(bloginUser != null) { %>
					alert("개인회원만 이용할 수 있습니다.");	
			<%	} else if(loginUser != null) {%>
				var num = $(this).children().children().children().children().eq(0).children().val();
				//var num = $(".jengukNo").val();
				console.log("num : " + num);
				location.href = "<%=request.getContextPath()%>/selectOne.tn?num="+num;
			<% } else { %>
				alert("로그인 후 이용하실 수 있습니다.");
				
				var check = window.confirm("로그인 하시겠습니까?");
				
				if(check) {
					location.href = "<%=request.getContextPath()%>/views/common/E_LoginPage.jsp";
				}
			<% } %> 
		})
		
		
		$(".newList").click(function(){
			<% if(bloginUser != null) { %>
					alert("개인회원만 이용할 수 있습니다.");	
			<%	} else if(loginUser != null) {%>
				var num = $(this).children().children().children().children().eq(0).children().val();
				//var num = $(".jengukNo").val();
				console.log(num);
				location.href = "<%=request.getContextPath()%>/selectOne.tn?num="+num;
			<% } else { %>
				alert("로그인 후 이용하실 수 있습니다.");
				
				var check = window.confirm("로그인 하시겠습니까?");
				
				if(check) {
					location.href = "<%=request.getContextPath()%>/views/common/E_LoginPage.jsp";
				}
			<% } %> 
		});
		
});


//베스트 좋아요&싫어요 조회하기
<% for(int i = 0; i < bestList.size(); i++) { 
	HashMap<String, Object> hmap = bestList.get(i); %>

	$(function() {
		$(".like").ready(function() {
			var boardNo = <%= hmap.get("jengukNo") %>;
			
			$.ajax({
				url: "/h/selectLike",
				data: {
					boardNo: boardNo,
				},
				type: "post",
				success: function(data) {
					likeCount = data;
					//console.log(likeCount); 
					$(".<%= hmap.get("jengukNo") %>_bestLikeCount").html(likeCount);
				},
				error: function(error) {
					console.log(error);
				}
			}); 
		});
		
		$(".dislike").ready(function() {
			var boardNo = <%= hmap.get("jengukNo") %>;
			
			$.ajax({
				url: "/h/selectHate",
				data: {
					boardNo: boardNo,
				},
				type: "post",
				success: function(data) {
					hateCount = data;
					$(".<%= hmap.get("jengukNo") %>_bestHateCount").html(hateCount);
				},
				error: function(error) {
					console.log(error);
				}
			}); 
		});
	});
<% } %>


//좋아요&싫어요 조회하기
<% for(int i = 0; i < list.size(); i++) { 
	HashMap<String, Object> hmap = list.get(i); %>

	$(function() {
		$(".like").ready(function() {
			var boardNo = <%= hmap.get("jengukNo") %>;
			
			$.ajax({
				url: "/h/selectLike",
				data: {
					boardNo: boardNo,
				},
				type: "post",
				success: function(data) {
					likeCount = data;
					//console.log(likeCount); 
					$(".<%= hmap.get("jengukNo") %>_likeCount").html(likeCount);
				},
				error: function(error) {
					console.log(error);
				}
			}); 
		});
		
		$(".dislike").ready(function() {
			var boardNo = <%= hmap.get("jengukNo") %>;
			
			$.ajax({
				url: "/h/selectHate",
				data: {
					boardNo: boardNo,
				},
				type: "post",
				success: function(data) {
					hateCount = data;
					$(".<%= hmap.get("jengukNo") %>_hateCount").html(hateCount);
				},
				error: function(error) {
					console.log(error);
				}
			}); 
		});
	});
<% } %>
</script>        
        
</body>
</html>