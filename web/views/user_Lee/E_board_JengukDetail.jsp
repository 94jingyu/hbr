<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.kh.hbr.board.jengukBoard.model.vo.Jenguk" %>
<%@ page import="com.kh.hbr.board.jengukBoard.model.vo.Attachment" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.util.ArrayList" %>
<%
	Jenguk j = (Jenguk) request.getAttribute("jenguk");
	ArrayList<Attachment> fileList = (ArrayList<Attachment>) request.getAttribute("fileList");
	Attachment titleImg = fileList.get(0);
	
	// 숫자에 콤마 찍기
	DecimalFormat formatter = new DecimalFormat("###,###");
	
%>
<!DOCTYPE html>         
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>  
/* * {text-align:left} */
/* 상단메뉴바 */
.topMenu {
	/* display: inline-block; */
	width: 100%;
	height: 50px;
	/* background: #013252; */
	line-height: 30px; /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	vertical-align: middle; /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	text-align: center; /* 글씨 정렬을 가운데로 설정 */
	font-weight: bolder;
	font-size: 20px;
	color: #013252;
}


/*header*/
.page_wrap {
	width: 930px;
	float: right;
	
}

.mypage_root {
	padding: 0 0 20px 0;
	text-align: left;
}

.header_top {
	background: #013252;
	padding: 20px;
	margin-bottom: 20px;
	text-align: left;
}

.header_top p {
	color: #fff;
	font-size: 20px;
	font-weight: bold
}

/* 진규 추가 */
.searchBox {width: 930px; background: #ecf0f7;}
.searchTable {margin: 0 auto; padding: 30px; padding-top: 0px;}
#searchBtn {width: 90px; height: 40px; text-align:center; font-size: 15px; background: #013252;
	color: white; padding: 5px; border-radius: 1px; border: 1px; outline: 1px;}
.wbtn {width: 90px; height: 40px; text-align:center; font-size: 15px; background: #013252;
	color: white; padding: 5px; border-radius: 1px; border: 1px; outline: 1px;}
#searchBtn, .wbtn, .btn:hover {cursor: pointer;}
.searchSelect {width: 120px; height: 40px; font-size: 20px;}
.searchTable td {padding: 3px; }

/* 소문난 보금자리 */
.bodyTable{width: 930px; height: 100%; border: 1px solid #c0c0c0;}
.topTable{padding-top: 0px; width: 100%;}
.contentTitle{font-size: 22px; width: 500px; height: 70px;}
.innerTable{width: 100%; background: #ecf0f7; padding: 30px;}	
.titleTd{font-size: 20px; font-weight: bold; padding: 10px;}	
.contentTd{font-size: 18px;}
.contentImg{widht: 500px; height: 300px;}
.scrap_img{width: 20px; height: 20px;}	
.replySelectTable{width: 100%; background: #f5f6f7}
.pBtn{width: 50px; height: 30px; background: #bc4b4b; color: white; border: 1px; outline: 1px; margin: 5px}
.insertComent{width: 99%; height: 40px; font-size: 17px;}
.insertBtn{width: 100%; height: 45px; font-size: 17px; background:#013252; color: white; border: 1px; outline: 1px;}
.img{width: 60px; height: 60px; padding: 20px 30px 0px 20px;}
.count{font-size: 40px;}
.likeCount{color: #0A74EC}
.hateCount{color: #FB3A4A}
.backBtn{width: 130px; height: 50px; font-size: 22px; background:#013252; color: white; 
border: 1px; border-radius:5px; outline: 1px;}
</style>
</head>
<body>
<div id="wrap">
		<!-- 로고 영역 -->
		<%@ include file="../common/E_user_menubar.jsp" %>
		<hr style="border: solid 2px #013252; width:100%; margin:0 auto;"><br>
		
		<!-- 좌측 메뉴마 -->
		<%@ include file="../user_Lee/Jenguk_menubar.jsp" %><br>
			
    <div class="page_wrap">
        <header>
            <div class="mypage_root">
                <span>HOME</span>
                <span>> 열린마당</span>
                <span>> 전국보금자리자랑</span>
            </div>
            <div class="header_top">
                <p style="font-size: 30px">전국보금자리자랑</p>
            </div>
        </header> 
        
        <br>
        <div class="bodyTable">
			<table class="topTable">
				<tr>
					<td class="contentTitle">&nbsp;<%= j.getJengukTitle() %></td>
					<td class="contentTitle2">작성자: <%= j.getMemberId() %></td>
					<td class="contentTitle2">작성일: <%= j.getJengukEnrollDate()%></td>
					<td class="contentTitle2">조회수: <%= j.getBoardCount() %></td>
				</tr>	
				<tr><td colspan="4" style="height: 30px; border-top: 5px solid #013252;"></td></tr>
				<tr>
					<td colspan="4">
						<table class="innerTable">
							<tr>
								<td class="titleTd">근무지역 </td>
								<td class="contentTd"><%= j.getJengukArea()%></td>
								<td class="titleTd">회사명 </td>
								<td class="contentTd"><%= j.getCompanyName()%></td>
								<td rowspan="3" class="titleTd" style="text-align:center">만족도</td>
								<td rowspan="3" class="contentTd" style="width: 130px;">
									<% int score = j.getScore(); %>
									
									<% for(int s = 0; s < score; s++) {%>
									 <span><img class="scrap_img" alt="" src="<%=request.getContextPath()%>/static/images/common/star.png"></span>
									<% } %>
									<% for(int e = 0; e < (5-score); e++) {%>
									 <span><img class="scrap_img" alt="" src="<%=request.getContextPath()%>/static/images/common/star_em.png"></span>
									<% } %>
								</td>
							</tr>
							<tr>
								<td class="titleTd">직종 </td>
								<td class="contentTd"><%= j.getJobChoice()%></td>
								<td class="titleTd">담당업무  </td>
								<td class="contentTd"><%= j.getTask() %></td>
							</tr>
							<tr>
								<td class="titleTd"></td>
								<td class="contentTd"></td>
								<td rowspan="2" class="titleTd">급여 </td>
 								<% if(j.getSalOpenYn().equals("Y")) { %>
								<td class="contentTd"><%=j.getSalKind()%></td>
							</tr>
							<tr>
								<td class="titleTd"></td>
								<td class="contentTd"></td>
								<td colspan="2" class="contentTd">
									<!-- 금액 불러오기 -->
									<%= formatter.format(j.getSalMount())%>원 
								</td>
								<td class="titleTd"> </td>
							</tr>
								<% } else { %>
								<td class="contentTd">비공개</td>
							</tr>
								<% } %>
						</table>
					</td>
				</tr>
				<tr><td style="height: 30px"></td></tr>
				<tr>
					<td colspan="4">
						<div class="content" style="padding: 20px; font-size: 20px; text-align:center">
							<img class="contentImg" alt="" src="<%=request.getContextPath()%>/thumbnail_uploadFiles/<%=titleImg.getChangeName()%>">
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<div class="content" style="padding: 20px; font-size: 20px;"><%= j.getJengukContent() %></div>
					</td>
				</tr>
				<tr>
					<td colspan="4" style="display:table-cell; text-align:center; vertical-align:middle;">
					<span>
						<b class="count likeCount">0</b>&nbsp;&nbsp;
						<img class="btn img like" alt="" src="<%=request.getContextPath()%>/static/images/user/like.png">
					</span>&nbsp;&nbsp;&nbsp;
					<span>
						<img class="btn img dislike hate" alt="" src="<%=request.getContextPath()%>/static/images/user/dislike.png">&nbsp;&nbsp;
						<b class="count hateCount">0</b>&nbsp;&nbsp;
					</span>
					</td>
				</tr>
				<tr>
					<td colspan="4" style="text-align: right; height: 40px; border-bottom: 1px solid black;">
					
					<% 
					String memberId = loginUser.getMemberId();	
					if(j.getMemberId().equals(memberId) || loginUser.getMemberId().equals("admin")) {%>				
					<button class="btn pBtn dBtn" style="margin-bottom: 10px; background: gray">삭제</button>
					<% } %>
					<button class="btn pBtn ppBtn" style="margin-bottom: 10px">신고</button><br>
					</td>
				</tr>
				<tr>
					<td colspan="4" >
						<table class="replySelectTable" id="replySelectTable" style="padding: 20px;">
						</table>
						
						<table class="replySelectTable" style="padding: 20px; padding-top: 0px;">
							<tr>
								<td colspan="3">
									<input type="text" class="insertComent" id="replyContent" name="replyContent" placeholder="댓글을 작성해보세요."> 
								</td>
								<td>
									<button class="btn insertBtn" id="addReply">등록</button> 
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
       	</div> <!-- / bodyTable -->
		<br><br>
		<div style="text-align:center"><button type="button" id="backBtn"class="btn backBtn" onclick="back();">뒤로가기</button></div> 
		<br><br><br><br>
	</div>
</div>
       
       
<script>
	function back() {
		   history.back();
	}

	$(function(){
		// 웹페이지 로드시 바로 댓글 조회
		$("#addReply").ready(function(){
			var boardNo = <%= j.getJengukNo() %>;
			
			$.ajax({
				url: "/h/selectReply.bo",
				data: {
					boardNo: boardNo,
				},
				type: "post",
				success: function(data) {
					
					var $replySelectTable = $("#replySelectTable");
					$replySelectTable.html('');

					for(var key in data) {
						var $tr = $("<tr>");
						var $writerTd = "<td style='width: 90px'><b>&nbsp;" + data[key].memberId + "</b></td>";
						var $contentTd = "<td style='width: 510px'>" + data[key].replyContent + "</td>";
						var $dateTd = "<td style='width: 100px; text-align:center'>" + data[key].dd + "</td>"
						var writer = data[key].memberId;
						var $pbtn = "<td style='width: 50px; text-align:center'><button class='btn pBtn  ppBtn'>" + "신고" + "</button></td>";
					
						$tr.append($writerTd);
						$tr.append($contentTd);
						$tr.append($dateTd);
						$tr.append($pbtn);
						
						$replySelectTable.append($tr); 
					}
					
				},
				error: function(error) {
					console.log(error);
					
				}
			}); 
		});
		
		
		// 댓글 입력
		$("#addReply").click(function(){
			var writer = "<%= loginUser.getMemberId()%>";
			var boardNo = <%= j.getJengukNo() %>;
			var content = $("#replyContent").val();
			//console.log(writer);
     		//console.log(boardNo);
     		//console.log(content);
     		
     		if(writer == "") {
     			alert("로그인을 해주세요.");
     			return;
     		} else if(content == ""){
     			alert("내용을 입력해주세요.");
     			return;
     		}
     		
     		
			$.ajax({
				url: "/h/insertReply.bo",
				data: {
					writer: writer,
					boardNo: boardNo,
					content: content
				},
				type: "post",
				success: function(data) {
					alert("댓글이 등록되었습니다.")
					
					var $replySelectTable = $("#replySelectTable");
					$replySelectTable.html('');
					//console.log($replySelectTable);

					for(var key in data) {
						var $tr = $("<tr>");
						var $writerTd = "<td style='width: 90px'><b>&nbsp;" + data[key].memberId + "</b></td>";
						var $contentTd = "<td style='width: 510px'>" + data[key].replyContent + "</td>";
						var $dateTd = "<td style='width: 100px; text-align:center'>" + data[key].dd + "</td>";
						var writer = data[key].memberId;
						var $pbtn = "<td style='width: 50px; text-align:center'><button class='btn pBtn'>" + "신고" + "</button></td>";
					
						$tr.append($writerTd);
						$tr.append($contentTd);
						$tr.append($dateTd);
						$tr.append($pbtn);
						
						$replySelectTable.append($tr); 
					}
					
				},
				error: function(error) {
					console.log(error);
					
				}
			}); 
		});
		
		// 게시글 삭제
		$(".dBtn").click(function(){
			var writer = "<%= loginUser.getMemberId()%>";
			var boardNo = <%= j.getJengukNo() %>;
			var check = window.confirm("게시글을 삭제하시겠습니까?");
				
			if(check) {
				location.href = "<%=request.getContextPath()%>/deleteJenguk.tn?writer=" + writer + "&boardNo=" + boardNo;
			}
			
		});
		
		
		
		// 좋아요/싫어요 조회하기
		$(".like").ready(function() {
			var boardNo = <%= j.getJengukNo() %>;
			
			$.ajax({
				url: "/h/selectLike",
				data: {
					boardNo: boardNo,
				},
				type: "post",
				success: function(data) {
					likeCount = data;
					$(".likeCount").html(likeCount);
				},
				error: function(error) {
					console.log(error);
				}
			}); 
		});
		
		$(".dislike").ready(function() {
			var boardNo = <%= j.getJengukNo() %>;
			
			$.ajax({
				url: "/h/selectHate",
				data: {
					boardNo: boardNo,
				},
				type: "post",
				success: function(data) {
					hateCount = data;
					$(".hateCount").html(hateCount);
				},
				error: function(error) {
					console.log(error);
				}
			}); 
		});
		
		
		// 좋아요  기능 
		$(".like").click(function() {
			var memberId = "<%= loginUser.getMemberId()%>";
			var boardNo = <%= j.getJengukNo() %>;
			
			$.ajax({
				url: "/h/plusLike",
				data: {
					memberId: memberId,
					boardNo: boardNo,
				},
				type: "post",
				success: function(data) {
					likeCount = data;
					//console.log(likeCount);
					alert("좋아요 변경");
					$(".likeCount").html(likeCount);
				},
				error: function(error) {
					console.log(error);
				}
			}); 
		});
		
		// 싫어요 기능
		$(".dislike").click(function() {
			var memberId = "<%= loginUser.getMemberId()%>";
			var boardNo = <%= j.getJengukNo() %>;
			
			$.ajax({
				url: "/h/plusHate",
				data: {
					memberId: memberId,
					boardNo: boardNo,
				},
				type: "post",
				success: function(data) {
					hateCount = data;
					//console.log(hateCount);
					
					alert("싫어요 변경");
					$(".hateCount").html(hateCount);
				},
				error: function(error) {
					console.log(error);
				}
			}); 
		});
		
		
		$(".ppBtn").click(function(){
			alert("허위신고는 제재될 수 있습니다.");
			
			var check = window.confirm("신고하시겠습니까?");
			
			if(check) {
				alert("신고가 완료되었습니다.");
			}
		});
		
	});



</script>        
</body>
</html>