<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
    <button onclick="calculate()">알람받기</button>
 
<script type="text/javascript">
	// 알림 권한 허용여부
	Notification.requestPermission().then(function(result) {
		console.log(result);
	});


    window.onload = function () {
        if (window.Notification) {
            Notification.requestPermission();
        }
    }

    // 알림 메소드 - 지정한 시간 뒤에 알림 메시지 전송
    function calculate() {
        setTimeout(function () {
            notify();
        }, 2000); 
    }


    // 동의여부에 따른 메시지 설정
    function notify() {
        if (Notification.permission !== 'granted') {
            alert('notification is disabled');
        }
        else {
        	// 알림 메세지 내용 설정
            var notification = 
            	new Notification('제목', {
            		icon: '<%=request.getContextPath()%>/static/images/common/star.png',
            		body: '알림 메시지입니다.',}
            	);

            // 알림 클릭시 이동할 url
            notification.onclick = function () {
                window.open('http://google.com');
            };
            
         	// 지정한 시간 뒤에 알림 메시지 close
            /* setTimeout(function(){
            	notification.close();
            }, 3000);   */
        }
    }	
    
    
    
</script>
</body>
</html>
