<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>         
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>  
/* * {text-align:left} */
/* 상단메뉴바 */
.topMenu {
	/* display: inline-block; */
	width: 100%;
	height: 50px;
	/* background: #013252; */
	line-height: 30px; /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	vertical-align: middle; /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	text-align: center; /* 글씨 정렬을 가운데로 설정 */
	font-weight: bolder;
	font-size: 20px;
	color: #013252;
}


/*header*/
.page_wrap {
	width: 930px;
	float: right;
	
}

.mypage_root {
	padding: 0 0 20px 0;
	text-align: left;
}

.header_top {
	background: #013252;
	padding: 20px;
	margin-bottom: 20px;
	text-align: left;
}

.header_top p {
	color: #fff;
	font-size: 20px;
	font-weight: bold
}

/* 진규 추가 */
.searchBox {width: 930px; background: #ecf0f7;}
.searchTable {margin: 0 auto; padding: 30px; padding-top: 10px;}
#searchBtn {width: 90px; height: 40px; text-align:center; font-size: 15px; background: #013252;
	color: white; padding: 5px; border-radius: 1px; border: 1px; outline: 1px;}
.wbtn {width: 90px; height: 40px; text-align:center; font-size: 15px; background: #013252;
	color: white; padding: 5px; border-radius: 1px; border: 1px; outline: 1px;}
#searchBtn, .wbtn, .btn:hover {cursor: pointer;}
.searchSelect {width: 120px; height: 40px; font-size: 20px;}
.searchTable td {padding: 3px; }

/* 소문난 보금자리 */
.bodyTable{width: 930px; height: 1030px; border: 1px solid #c0c0c0;}
.topTable{padding: 30px; padding-top: 10px; width: 100%;}
.topList{width:100%; height: 250px; background: white; margin:10px;}
.topDetail{width:100%}
.boardTd{font-size: 20px; text-align:left; width: 140px;}
.boardTd2{font-size: 15px;}
.select{width: 280px; height: 45px;}
.input1{width: 276px; height: 41px;}
.input2{width: 100%; height: 41px;}
.input3{width: 100%; height: 121px;}
.radio{margin-left: 5px; margin-right: 5px;}
.score{margin-left: 5px; margin-right: 5px;}
.inputSal{width: 190px; height: 41px;}
.bottomBtn{width:140px; height:50px; font-size:20px; color: black; padding: 5px; margin: 2px; 
	border-radius:5px; border: 1px; outline: 1px; font-weight: bold;}
</style>
<script>
function back() {
	   history.back();
}

// 급여 입력할 때 숫자에 콤마찍기
$(function(){
	$(".inputSal").on( "keyup", function( event ) {
	 
	    // 1.
	    var selection = window.getSelection().toString();
	    if ( selection !== '' ) {
	        return;
	    }
	 
	    // 2.
	    if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
	        return;
	    }
	    
	        // 3
	        var $this = $( this );
	        var input = $this.val();
	 
	        // 4
	        var input = input.replace(/[\D\s\._\-]+/g, "");
	 
	        // 5
	        input = input ? parseInt( input, 10 ) : 0;
	 
	        // 6
	        $this.val( function() {
	            return ( input === 0 ) ? "" : input.toLocaleString( "en-US" );
	        });
	} );
	
});




</script>
</head>
<body>
<div id="wrap">
		<!-- 로고 영역 -->
		<%@ include file="../common/E_user_menubar.jsp" %>
		<hr style="border: solid 2px #013252; width:100%; margin:0 auto;"><br>
		
		<!-- 좌측 메뉴마 -->
		<%@ include file="../user_Lee/Jenguk_menubar.jsp" %><br>
			
    <div class="page_wrap">
        <header>
            <div class="mypage_root">
                <span>HOME</span>
                <span>> 열린마당</span>
                <span>> 전국보금자리자랑</span>
            </div>
            <div class="header_top">
                <p style="font-size: 30px">전국보금자리자랑</p>
            </div>
        </header> 
        
        <br>
        <div class="bodyTable">
			<table class="topTable">
				<tr>
					<td> 
						<div class="topList">
 							<form id="boardForm" action="<%= request.getContextPath() %>/insertJenguk.tn" method="post" encType="multipart/form-data">
								<table class="topDetail" style="padding: 5px;">
									<tr class="boardTr">
										<td class="boardTd"><b>근무지역</b></td>
										<td>
											<select class="select" name="location1" style="font-size: 17px">
												<option selected value="null">시/도 선택</option>
												<option value="서울">서울</option>
												<option value="경기">경기</option>
												<option value="인천">인천</option>
												<option value="부산">부산</option>
												<option value="대구">대구</option>
												<option value="광주">광주</option>
												<option value="대전">대전</option>
												<option value="울산">울산</option>
												<option value="세종">세종</option>
												<option value="강원">강원</option>
												<option value="경남">경남</option>
												<option value="경북">경북</option>
												<option value="전남">전남</option>
												<option value="전북">전북</option>
												<option value="충남">충남</option>
												<option value="충북">충북</option>
												<option value="제주">제주</option>
											</select>
											<select class="select" name="location2" style="font-size: 17px" hidden>
												<option value="findAll">시/군/구 선택</option>
												<option>이걸 언제 다 쳐</option>
											</select>
										</td>
									</tr>
									<tr><td style="height: 15px"></td></tr>
									<tr>
										<td class="boardTd"><b>회사명</b></td>
										<td><input type="text" class="input1" name="cName" placeholder="회사명을 입력해주세요" style="font-size: 17px"></td>
									</tr>
									<tr><td style="height: 15px"></td></tr>
									<tr>
										<td class="boardTd"><b>직종선택</b></td>
										<td>
											<select class="select" name="jobChoice" style="font-size: 17px">
												<option selected value="0">선택</option>
												<option value="요리/서빙">요리/서빙</option>
												<option value="간호/의료">간호/의료</option>
												<option value="생산/기술/건설">생산/기술/건설</option>
												<option value="사무/경리">사무/경리</option>
												<option value="운전/배달">운전/배달</option>
												<option value="상담/영업">상담/영업</option>
												<option value="매장관리">매장관리</option>
												<option value="교사/강사">교사/강사</option>
												<option value="서비스/기타">서비스/기타</option>
											</select>
										</td>
									</tr>
									<tr><td style="height: 15px"></td></tr>
									<tr>
										<td class="boardTd"><b>담당업무</b></td>
										<td><input type="text" class="input1" name="task" placeholder="담당업무를 입력해주세요" style="font-size: 17px"></td>
									</tr>
									<tr><td style="height: 15px"></td></tr>
									<tr>
										<td class="boardTd"><b>급여</b></td>
										<td>
											<input type="radio" class="radio" name="sal" value="연봉" id="year"><label for="year">연봉</label>
											<input type="radio" class="radio" name="sal" value="월급" id="month"><label for="month">월급</label>
											<input type="radio" class="radio" name="sal" value="주급" id="week"><label for="week">주급</label>
											<input type="radio" class="radio" name="sal" value="시급" id="hour"><label for="hour">시급</label>
										</td>
									<tr>
										<td></td>
										<td>
											<input type="text" class="inputSal" name="salMount" maxlength="10" placeholder="급여를 입력해주세요" style="font-size: 17px; margin-top: 10px;">원&nbsp;&nbsp;
											<input type="checkbox" id="openYn" name="openYn" value="Y"> <label for="openYn">공개</label>&nbsp;&nbsp;&nbsp; 
											<span style="color:gray">* 공개 미선택시 비공개처리 됩니다.</span>
										</td>
									</tr>
									<tr><td style="height: 15px"></td></tr>
									<tr>
										<td class="boardTd"><b>만족도</b></td>
										<td>
											<input type="radio" class="score" name="score" value="1" id="score1"><label for="score1">1점</label>
											<input type="radio" class="score" name="score" value="2" id="score2"><label for="score2">2점</label>
											<input type="radio" class="score" name="score" value="3" id="score3"><label for="score3">3점</label>
											<input type="radio" class="score" name="score" value="4" id="score4"><label for="score4">4점</label>
											<input type="radio" class="score" name="score" value="5" id="score5"><label for="score5">5점</label>
										</td>
									</tr>
									<tr><td style="height: 15px"></td></tr>
									<tr>
										<td class="boardTd"><b>글제목</b></td>
										<td><input type="text" class="input2" name="title" placeholder="제목을 입력해주세요" style="font-size: 17px" maxlength="10"></td>
									</tr>
									<tr><td style="height: 15px"></td></tr>
									<tr>
										<td class="boardTd" style="vertical-align: top;"><b>글내용</b></td>
										<td><textarea class="input3" name="content" placeholder="이달의 보금자리 자랑에 선정되시면 소정의 선물을 보내드립니다." style="vertical-align: top; font-size: 17px;"></textarea></td>
									</tr>
									<tr><td style="height: 15px"></td></tr>
									<tr>
										<td rowspan="2" class="boardTd"><b>파일 첨부</b></td>
										<td>
											<input type="file" class="file" name="attachment1" style="font-size: 15px">
										</td>
									</tr>
									<tr>
										<td>
											<p style="color:gray">500MB 이내의 jpg, png 확장자의 파일만 첨부할 수 있습니다.</p>
										</td>
									</tr>
									<tr><td style="height: 15px"></td></tr>
									<tr hidden>
										<td rowspan="2" class="boardTd"><b>증빙파일 첨부</b></td>
										<td>
											<input type="file" class="file" name="attachment2" style="font-size: 15px">
										</td>
									</tr>
									<tr hidden>
										<td>
											<p style="color:gray">* 증빙파일 미첨부 시 관리자의 승인처리가 이루어지지 않습니다. 반드시 첨부해주세요.</p>
										</td>
									</tr>
									<tr><td colspan="3" style="height: 20px; border-bottom:1px solid black;"></td></tr>
									<tr>
										<td colspan="3">
											<br><br>
											<div align="center">
												<input type="checkbox" style="width: 17px; height: 17px;margin-right:5px;  margin-bottom: 20px"><span style="font-size: 20px;">개인정보 제공에 동의합니다.</span>
												<p style="font-size: 15px">- 입력하신 이름, 이메일은 상담내역에 대한 안내 외에는 사용되지 않습니다.</p>
												<p style="font-size: 15px">- 개인정보 보관 기간 : 3년 (소비자 보호에 관한 법률시행령 제 6조에 근거)</p>
											</div>
										</td>
									</tr>
									<tr><td style="height: 50px"></td></tr>
									<tr>
										<td colspan="3" align="center">
											<button type="button" class="btn bottomBtn backBtn" style="margin-right:10px" onclick="back();">뒤로가기</button>
											<button type="submit" class="btn bottomBtn enrollBtn" style="background:#013252; color:white">등록하기</button>
										</td>
									</tr>
								</table>
							</form>

						</div>
					</td> 
			</table>
        </div> <!-- / bodyTable -->
		<br>
		</div>
	</div>
        
</body>
</html>