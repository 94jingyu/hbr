<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.kh.hbr.recruit.model.vo.Recruit, java.util.*"%>
<%  
	ArrayList<Recruit> approvedList = (ArrayList<Recruit>) request.getAttribute("approvedList");
%>	
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>보금자리 등록 관리자 승인심사</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
* {margin: 0; padding: 0;}
#wrap {width: 1200px; height: 100%; margin: 0 auto;}

/* ============= 헤더 영역 ============= */
header {width: 100%; height: 100px; background: #013252;}
.logo {font-size: 45px; color: white;}
#title {margin-left: 10px;}

/* 사이트 바로가기 td id명 */
#goSite {font-size: 18px;}

/* =========== 좌측메뉴 및 내용 ============ */
.leftMenu {float: left; width: 20%; height: 1800px; background-color: #ECF0F7; border-right: 3px solid #CDCDCD}
.leftMenu {/* border-right: hidden; */ border-right: 3px solid #CDCDCD}
.leftMenu>ul {margin: 0; padding: 0; list-style: none;}
.leftMenu a {display: block; text-decoration: none; padding: 10px 20px; color: black;}
.leftMenu a:hover {background: darkgray; color: white;}

/* ========== HOME > 회원관리 > 기업인증내역 ========== */
.homeNavi {margin-top: 50px; float: right;}

/* ========== 기업인증내역 - 위아래 선 ========== */
.pageTitle {border-top: 3px solid rgb(200, 200, 200); border-bottom: 3px solid rgb(200, 200, 200);
	width: 930px; height: 80px; float: right; margin-top: 30px; margin-bottom: 50px;}

/* ========== 기업인증내역 -> 제목 ========== */
.pageTitle2 {padding: 23px;}

/* ============= 테이블 영역 ============= */

/* 기업인증내역 테이블 div class */
.Request {width: 850px;	margin-left: 30px; margin-top:20px;}



/* 검색할 키워드 입력 div class (text상자) */
.searchKey {
	width: 180px;
	height: 50px;
	float: right;
}



/* ============= 다정 부분 테이블 영역 ============= */
	
	/* 기업인증내역 테이블 div class */
	.Request {
		width:900px;
		height:100%;
		margin-left:20px;
	}

   	/* 상품적용 테이블 테두리*/
	.TB { 
	border-collapse:collapse;
	text-align:center;

	}
	tr {
	border :1px solid #afafaf;
	}
    
    /* 테이블컬럼 배경 */
    .tableBack{
	background-color:#f0f0f0;
	font-weight:bold;
	
	}
	/* 테이블안에 나만 왼쪽 정렬 */
	#align_left{
	text-align: left;
	}
	
</style>
</head>
<body>
	<div id="wrap">
	<%@ include file="../guide/admin_menubar.jsp" %>
	
		<div class="homeNavi">
			<label style="font-size: 15px;">HOME > 보금자리관리 > 보금자리등록현황</label>
		</div>

		<div class="pageTitle">
			<div class="pageTitle2">
				<label style="font-size: 25px;"><b>보금자리 등록현황</b></label>
			</div>

     	<% if( approvedList != null) {%>
       <!-- 테이블 -->
             <div class="Request"> 
            	<table class="TB">
	          		<tr style="border:0;">
            			 <td colspan="4" width="580px" height="60px" id="align_left"><h3>&nbsp;총&nbsp;<label style="color:Orangered;"><%= approvedList.size() %></label>건</h3></td>
            			 
            			 
            			 <td></td>
            			 <td width="100px"> </td>
            			 <td colspan="2" width="200px">
            			 	</td>
            			 	
            		</tr>
            		<tr class="tableBack" >
            			
            			<th width="70px" height="50px">번호</td>
            			<th width="100px">아이디</td>
            			<th width="100px">담당자명</td>
            			<th width="320px">보금자리 제목</td>
            			<th width="1px"></td>
            			<th width="100px">상태</td>
            			<th width="100px">신청일</td>
            			<th width="100px">처리일</td>
            			
            		
            		</tr>
            		<% int i = approvedList.size() + 1; %>
            		<% for(Recruit r : approvedList) { %>
              		<% i--; %>
            		<tr>
            			
						<td height="50px"><%= i %></td>
            			<td><%=r.getBmemberId()%></td>
            			<td><%=r.getManagerName() %></td>
            			<td id="align_left"><%=r.getRecruitTitle()%></td>
            			<td><label hidden><%=r.getRecruitNo() %></label></td>
            			<td><% if(r.getStatus().equals("END")){%>만료<%} else if(r.getStatus().equals("APPROVED")){%>승인<%}else if(r.getStatus().equals("REJECTED")){%>반려<%}else{ %>대기<%} %></td>
            			<td><%=r.getEnrollDate() %></td>
            			<td><%=r.getRecruitCheckDate() %></td>
            			
            			
            		
            		</tr>
            		<%}%>
            		<!-- <tr>
						
						<td height="50px">2</td>
            			<td>abc1234</td>
            			<td>김태원</td>
            			<td id="align_left">분식집 200만 배달 라이더 모집</td>
            			<td>반려</td>
            			<td>20/01/01</td>
            			<td>20/01/14</td>
            			<td><button id="status">상세</button></td>
            		</tr>
            		<tr>
						
						<td height="50px">1</td>
            			<td>lucy5</td>
            			<td>정다정</td>
            			<td id="align_left">신전떡볶이 군포점 맛있는 떡볶이 만들어요~</td>
            			<td>승인</td>
            			<td>20/01/01</td>
            			<td>20/01/14</td>
            			<td><button id="status">상세</button></td>
            		</tr> -->
            		<!-- <tr style="text-align:center; border:0;">
        				<td colspan="7" height="70px;">
        					<button style="border:0;"> << </button>
	         				<button style="border:0;"> < </button>
	         				<button style="border:0;"> 1 </button>
	         				<button style="border:0;"> > </button>
	        				<button style="border:0;"> >> </button>
	        				
        				</td>
        			</tr> -->
   
            	</table>
            </div>
            
            </div>
        </div> 
    </div>
    
     <%}%>
     <script type="text/javascript">
     $(function(){
			$(".TB td").mouseenter(function(){
				$(this).parent().css({"background":"#EFEFEF", "cursor":"pointer"});
			}).mouseout(function(){
				$(this).parent().css("background", "none");
			}).click(function(){
				var num = $(this).parent().children().eq(4).text();
				console.log(num);
				location.href = "<%=request.getContextPath()%>/approvedAgian.ad?num=" + num;
			});
		});
     </script>
</body>
</html>