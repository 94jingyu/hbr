<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<title>기업승인</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	#wrap {width: 1200px; height: 100%; margin: 0 auto;}
	
	/* ============= 헤더 영역 ============= */ 
	header {width: 100%; height: 100px; background: #013252;}
	.logo {font-size:45px; color: white;}
	#title {margin-left:10px;}
	
	/* 사이트 바로가기 td id명 */
	#goSite {
		font-size: 18px;
	}

	/* =========== 좌측메뉴 및 내용 ============ */
	.leftMenu{float: left; width: 20%; height: 1800px; background-color: #ECF0F7; border-right: 3px solid #CDCDCD}
	
	.leftMenu {
		border-right: hidden;
	}
	
	.leftMenu>ul {    	
		margin: 0;
    	padding: 0;
    	list-style: none;
    }
    .leftMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: black;
  	}
	.leftMenu a:hover { background:darkgray; color:white; }
	
	
	/* ========== HOME > 회원관리 > 기업인증내역 ========== */
	.homeNavi {
		margin-top: 50px;
		float: right;
	}
	
	/* ========== 기업인증내역 - 위아래 선 ========== */
	.pageTitle {
		border-top: 3px solid rgb(200, 200, 200);
		border-bottom: 3px solid rgb(200, 200, 200);
		width:930px;
		height: 80px;
		float:right;
		margin-top: 30px;
		margin-bottom: 90px; 
	}
	
	/* ========== 기업인증내역 -> 제목 ========== */
	.pageTitle2 {
		padding: 23px;
	}
	
	/* ============= 테이블 영역 ============= */
	
	/* 기업인증내역 테이블 div class */
	.Request {
		width:900px;
		height:100%;
		margin-left:20px;
	}
	
	/* 기업인증내역 테이블 클래스명 */
	/*  .table {
    	 width: 880px;
         height: 100%;
      	 text-align: center;
         border-collapse: collapse;
   	}  */
   	/* 상품적용 테이블 테두리*/
	.TB { 
	border-collapse:collapse;
	}
	tr {
	border :1px solid #afafaf;
	}
    
    /* 테이블컬럼 배경 */
    .tableBack{
	background-color:#f0f0f0;
	font-weight:bold;
	}
	/* 테이블안에 나만 왼쪽 정렬 */
	#align_left{
	text-align: left;
	}
	table{
	text-align:center;
	}
	/* 상태버튼  */
	#status{
	width:50px;
	height:30px;
	background: #ecf0f7;
	border-radius: 7px;
	border:1px solid black;
	}
	/* 삭제버튼  */
	#delete{
	width:50px;
	height:30px;
	background: red;
	border-radius: 7px;
	border:1px solid red;
	color:white;
	font-weight:bold;
	}
    /* 테이블 제목 div class - 총 3건 */
	.total {
		width:100px;
		height:30px;
		float: left;
	}
	
	/* 테이블 제목 label ID - 총 3건 */
    #tableTitle {
    	float:left;
    	margin-bottom: 10px;
    }
	
	/* select - 전체, 기업명, 아이디, 미승인, 승인완료, 반려 */
	.selectbox {
		width:110px;
		height:30px;
		float: right;
		margin-top:8px;
	}
    
    /* 검색할 키워드 입력 div class (text상자) */
	.searchKey {
		width:180px;
		height:50px;
		float: right;
	}
	
	/* 검색할 키워드 입력 input class (text상자) */
    .searchKeyword {
    	float:right;
    	position: relative;
    	width: 180px;
    	height: 35px;
    	mix-blend-mode: normal;
    	border: 3px solid #013252;
    	box-sizing: border-box;
   
    }
    
    /* 검색버튼 div 클래스명 */
	.searchB {
		width:70px;
		height:40px;
		float: right;
	}
    
     /* 검색버튼 */
    .searchButton {
		background: #013252;
		border: 0px;
		color: white;
		height: 35px;
		width: 70px;
		text-size: 15px;
	}
	
	/* -----------------다정 보금자리 상세보기 관련 css ------------------------ */
	/* 테이블 글씨 가운데 정렬 */
	table{
	text-align :center;
	}
	/* 테이블안에 나만 왼쪽 정렬 */
	#align_left{
	text-align: left;
	padding-left:10px;
	}
	#align_left1{
	text-align: left;
	}
	.align_left1{
	text-align: left;
	}
	.align_left2{
	vertical-align:top;
	}
	/* 보금자리 미리보기 요약  배경색*/
	#table{
	background:#ECF0F7;
	}
	/* 보금자리 미리보기 상세내용 테이블 테두리 */
	.line{
	border-collapse:collapse;
	border :1px solid #afafaf;
	}
	/* 보금자리 미리보기 상세내용 작성내용 div */
	 .content{
	margin-left:10px;
	margin-top:20px;
	width:1050px;
	border:1px solid green;
	}
	/* 뒤로가기 버튼 */
	.backButton {
		float: right;
		margin-right: 10px;
		margin-top: 25px;
		background: #013252;
		border: 0px;
		color: white;
		height: 35px;
		width: 70px;
		text-size: 15px;
	}
	
</style>
</head>
<body>
	<div id="wrap">
		<!-- 공통 헤더 영역 (보금자리, 사이트바로가기, 관리자님, 홈 아이콘)-->
        <header class="logo">
        	<table width="100%;" border="0px;" style="padding: 20px;">
        		<tr>
        			<td width="65%" id="align_left"><label id="title" >보금자리</label></td>
        			<td id="goSite" width="320" id="align_left">
        				<img id="arrow" alt="admin_arrow.png" src="../../static/images/admin/admin_arrow.png" 
						width="25px" height="25px">
						<a>사이트 바로가기</a>
						&nbsp;&nbsp;&nbsp;
						<img alt="admin_account.png" src="../../static/images/admin/admin_account.png" 
						width="25px" height="25px">
						<a><label style="font-size: 18px;">관리자 님</label></a>
        			</td>
        			<td width="40" style="float:right;">
        				<img alt="admin_home.png" src="../../static/images/admin/admin_home.png" 
						width="45px" height="45px">
        			</td>
        		</tr>
        	</table>
        </header>
        
        <div class="container">
	        <aside class="leftMenu">
	           	<ul>
			   		<li><a href="#" style="font-size:23px; margin-top:20px;"><b>회원관리</b></a></li>
			    	<li><a href="#">개인회원</a></li>
			    	<li><a href="#">기업회원</a></li>
			    	<li><a href="#">기업인증내역</a></li>
		  			<li><a href="#" style="font-size:23px; margin-top:20px;"><b>신고관리</b></a></li>
			    	<li><a href="#">개인신고내역</a></li>
			    	<li><a href="#">기업신고내역</a></li>
			    	<li><a href="#">신고처리내역</a></li>
			    	<li><a href="#" style="font-size:23px; margin-top:20px;"><b>상품관리</b></a></li>
			    	<li><a href="#">상품사용내역</a></li>
			    	<li><a href="#">결제내역</a></li>
			    	<li><a href="#">환불내역</a></li>
			    	<li><a href="#" style="font-size:23px; margin-top:20px;"><b>광고관리</b></a></li>
			    	<li><a href="#">광고문의내역</a></li>
			    	<li><a href="#">광고처리내역</a></li>
			    	<li><a href="#">광고적용</a></li>
			    	<li><a href="#" style="font-size:23px; margin-top:20px;"><b>이력서관리</b></a></li>
			    	<li><a href="#">이력서 등록현황</a></li>
			   		<li><a href="#" style="font-size:23px; margin-top:20px;"><b>보금자리관리</b></a></li>
			    	<li><a href="#">보금자리 등록현황</a></li>
			    	<li><a href="#">보금자리 심사</a></li>
			    	<li><a href="#" style="font-size:23px; margin-top:20px;"><b>취업박사관리</b></a></li>
			    	<li><a href="#">취업뉴스</a></li>
			    	<li><a href="#">고용복지정책</a></li>
			    	<li><a href="#">자격증정보</a></li>
			    	<li><a href="#">취업박람회/교육일정</a></li>
			    	<li><a href="#" style="font-size:23px; margin-top:20px;"><b>열린마당관리</b></a></li>
			    	<li><a href="#"><b>전국보금자리자랑</b></a></li>
			    	<li><a href="#">&nbsp;보금자리자랑 등록현황</a></li>
			    	<li><a href="#">&nbsp;보금자리자랑 심사</a></li>
			    	<li><a href="#"><b>자유게시판</b></a></li>
			    	<li><a href="#" style="font-size:23px; margin-top:20px;"><b>고객센터관리</b></a></li>
			    	<li><a href="#">묻고답하기</a></li>
			    	<li><a href="#">자주하는질문</a></li>
			    	<li><a href="#">공지사항</a></li>
			    	<li><a href="#">튜토리얼</a></li>
		  		</ul>
	        </aside>
            
            <!-- HOME > 회보금자리관리 > 보금자리등록현황 -->
        	<div class="homeNavi" >
       		 	<label style="font-size:15px;">HOME > 보금자리관리 > 보금자리등록현황 상세보기</label>
    	    </div>
        
    	    <!-- 보금자리등록현황 -->
      	  <div class="pageTitle">
      		  	<div class="pageTitle2">
        			<label style="font-size:25px;"><b>보금자리 등록현황 상세보기</b></label>
        			<br><br><br>
        		</div>
     	
     	  
     	  <!-- 테이블 -->
             <div class="Request"> 
            	 <div class = "mainDiv">   
	    	<!-- 채용정보 미리보기 -->
	    	
	    	<div>
	    		 <div id="table">
	    		 <form action="">
	    		 	<table>
	    		 		<tr>
	    		 			<td colspan="8" width="100px" height="100px" id="align_left"><h3>(주) 덕승재</h3>
	    		 			<h2>한정식 남녀 서빙 245만 주 5일 220만</h2></td>
	    		 			
	    		 			<td width="200px" height="100px"><img alt="duck.png" src="../../static/images/register/duck.png" width="150px" height="80px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="100px" height="100px"><img alt="money.png" src="../../static/images/register/money.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="120px" height="100px" id="align_left1"><h4>월급<br>250만원</h4></td>
	    		 			<td width="100px" height="100px"><img alt="calendar.png" src="../../static/images/register/calendar.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="120px" height="100px" id="align_left"><h4>요일<br>월 ~ 금</h4></td>
	    		 			<td width="100px" height="100px"><img alt="clock.png" src="../../static/images/register/clock.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="120px" height="100px" id="align_left1"><h4>시간<br>10:00~18:00</h4></td>
	    		 			<td width="90px" height="100px"><img alt="placeholder.png" src="../../static/images/register/placeholder.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="140px" height="100px" id="align_left1"><h4>지역<a href="#" style="color:red;"> 지도></a><br>서울 강남구</h4></td>
	    		 			<td width="200px" height="100px" id="align_left"><h4>(주) 덕승재<br>사업내용<br>한식당 </h4></td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="110px" height="50px"><h3>근무조건</h3></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="110px" height="50px"><h3>자격조건</h3></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="90px" height="50px"></td>
	    		 			<td width="90px" height="50px"></td>
	    		 			<td rowspan="2" width="200px" height="100px"><br><br><h3>마감시간</h3><h4>2020년 2월 10일 17시</h4></td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="110px" height="50px" class="align_left2">근무유형</td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2">정규직,계약직</td>
	    		 			<td width="110px" height="50px" class="align_left2">근무기간</td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2">1년</td>
	    		 			<td width="110px" height="50px" class="align_left2">경력</td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2">무관</td>
	    		 			<td width="90px" height="50px" class="align_left2">학  력</td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2">무관</td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="110px" height="50px" class="align_left2">근무요일</td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2">월~금</td>
	    		 			<td width="110px" height="50px" class="align_left2">근무직종</td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2">서빙</td>
	    		 			<td width="110px" height="50px" class="align_left2">우대/가능</td>
	    		 			<td rowspan ="2" width="110px" height="50px" class="align_left1 align_left2">초보가능,<br> 동종업계경력자</td>
	    		 			<td width="90px" height="50px" class="align_left2">복리후생</td>
	    		 			<td rowspan ="2" width="110px" height="50px" class="align_left1 align_left2">주 5일 근무</td>
	    		 			<td rowspan="2" width="200px" height="100px"><h2 style="color:red;">12일 3시간 30분</h2><br><br></td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="110px" height="50px" class="align_left2">급여</td>
	    		 			<td colspan="3" width="110px" height="50px" class="align_left1 align_left2"></td>
	    		 			

	    		 			
	    		 		</tr>
	    		 	</table>
	    		 </form>
	    		</div><!-- 하늘색 요약 테이블 div 끝 -->
	    		<div><!-- 공간 띄우기용 div 진혁아 이부분에 지원하기 버튼만들어서 사용하면된다.-->
	    		<br><br><br>
	    		</div><!-- 공간 띄우기 끝 -->
	    		 <div><!-- 상세모집 내용 회사 정보 근무 지역  tap부분  -->
	 				
	 				<table class="line">
	 					<tr id="table">
	 						<td width="365px" height="60px" class="line" onclick=""><h3>상세모집내용</h3></td>
	 						<td width="365px" height="60px" class="line"><h3>회사정보</h3></td>
	 						<td width="365px" height="60px" class="line"><h3>근무지역</h3></td>
	 					</tr>
	 					<tr>
	 						<td id="align_left" colspan="3" class="line" >
	 						
	 								<br>
	 								<h4>[모집내용]</h4>
	 								<p>- 모집직종  : 서빙 <br>
	 								<p>- 근무유형 : 정규직, 계약직<br>
	 								<p>- 근무요일  : 월 ~ 금 <br>
	 								<p>- 근무시간  : 10:00 ~ 18:00<br>
	 								<p>- 급여  : 2,200,000<br>
	 								</p>
	 								<h4>[우대·가능]</h4>
	 								<p>초보가능, 동종업계경력자</p>
	 								<h4>[복리후생]</h4>
	 									<p>주 5일 근무</p>
	 								<h3>* 전화 연락시 "보금자리 구인정보 보고 전화 드렸어요"라고 말씀해주세요.</h3>
	 								<br>
	 								<h4>여기는 보금자리 작성자가 남기고 싶은 text</h4>
	 								<br>
	 							
	 							<br>
	 						</td>
	 					</tr>
	 					<tr>
	 						<td id="align_left" colspan="3" class="line" >
	 						
	 								<table>
	 									<tr>
	 										<td rowspan="2" width="300px" style="text-align: left; padding-left:20px;">
	 										<h2> 회사정보</h2>
	 										<br>
	 										<h3>(주) 덕승재</h3>
	 										<p>[회사주소]<br>
	 										서울 강남구 대치4동 889-53<br>
	 										[사업내용]<br>
	 										한식당</p>
	 										</td>
	 										<td rowspan="2" width="420px" style="text-align: left; padding-left:20px;">
	 										<h2>채용담당자 정보</h2>
	 										<br>
	 										<h3>[채용담장자] 김태원</h3>
	 										<h3>[대 표 번 호] 02-1234-5678</h3>
	 										<h3>[담장자전화] 010-1234-5678</h3>
	 										<h3>[ 이 메 일  ] ktw1984@khacademy.or.kr</h3>
	 										</td>
	 										<td width="380px" height="140px"  style="text-align: left; padding-left:20px;">
	 										<img alt="phone.png" src="../../static/images/register/phone.png" width="30px" height="30px" style="float: left"><h3>&nbsp;&nbsp;전화문의 할 경우</h3><br>
	 										<label style="font-size: smail">"보금자리에서 보고 전화드렸어요."<br>&nbsp;라고 하시면빠른 문의가 가능합니다. </label></h4>
	 										</td>
	 									</tr>
	 									<tr>
	 										<td width="380px" height="140px"  style="text-align: left; padding-left:20px;">
	 										<img alt="alert.png" src="../../static/images/register/alert.png" width="30px" height="30px" style="float: left"><h3>&nbsp;&nbsp;주의사항</h3><br>
	 										<label style="font-size: smail"> 통장 / 신분증 / 비밀번호 요구에는<br>절대 응하지 마세요. <br> 사기나 범죄에 이용될 수 있습니다. </label></h4>
	 										</td>
	 									</tr>
	 								</table>
	 							
	 							<br>
	 						</td>
	 					</tr>
	 					<tr>
	 						<td id="align_left" colspan="3" class="line" >
	 						
	 								<table>
	 									 <tr>
	 									 	<td width="550px" height="50px" style="text-align: left;">근무지역 : 경기도 군포시 번영로 403 여기에요 </td>
	 									 	<td width="600px" height="50px"><img alt="alert.png" src="../../static/images/register/alert.png" width="20px" height="20px">&nbsp;&nbsp;근무지 위치를 나타내며 호사 소재지와 일치하지 않을 수 있습니다.</td>
	 									 </tr>
	 									<tr>
	 										<td colspan="2"><img alt="map.png" src="../../static/images/register/map.png" width="100%" height="400">
	 									</tr>	 
	 								</table>
	 							<br>
	 						</td>
	 					</tr>
	 					<table>
	 						<tr>
        						<td width="500px"></td>
        						<td width="100px" height="40px"><button class="backButton"><b>뒤로</b></button></td>
        						<td width="500px"></td>
        					</tr>
	 					</table>
	 				</table>
	 			
	 		</div><!-- 공고 상세보기내용 tap부분 끝 -->
	 		
	    	
	    	
	 		<div><!-- 공간띄우기용 div -->
	    	<br><br><br>
	    	</div><!-- 공간띄우기 끝~! -->
            <section class=""></section>
        </div><!-- mainDiv끝 -->
            </div>
            </div>
        </div> 
    </div>
</body>
</html>