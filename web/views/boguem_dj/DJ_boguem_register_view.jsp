<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.hbr.recruit.model.vo.*"%>
<%@ page import="com.kh.hbr.board.jengukBoard.model.vo.Attachment" %>
<%@ page import="java.util.ArrayList" %>
<% 
	Recruit recruit = (Recruit) session.getAttribute("recruit");
	int click = recruit.getClickCount();
	
	ArrayList<Attachment> fileList = (ArrayList<Attachment>) session.getAttribute("fileList");
	Attachment titleImg = fileList.get(0);

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style type="text/css">
	/* 가장 큰 DIV */
	.jari_register {
	
	height : 100%;
	/* margin-top : 20px;  */
	padding : 30px;
	width: 1100px;
	margin: 0 auto;
	}
		/* 상단에 패딩 */
	.t{
	padding-top: 10px;
	}
	/* 하단에 패딩 */
	.b{
	padding-bottom: 10px;
	}
	/* 테이블 글씨 가운데 정렬 */
	table{
	text-align :center;
	}
	/* 테이블안에 나만 왼쪽 정렬 */
	#align_left{
	text-align: left;
	padding-left:10px;
	}
	#align_left1{
	text-align: left;
	}
	.align_left1{
	text-align: left;
	}
	.align_left2{
	vertical-align:top;
	}
	/* 보금자리 미리보기 요약  배경색*/
	#table{
	background:#ECF0F7;
	}
	/* 보금자리 미리보기 상세내용 테이블 테두리 */
	.line{
	border-collapse:collapse;
	border :1px solid #afafaf;
	}
	/* 보금자리 미리보기 상세내용 작성내용 div */
	 .content{
	margin-left:10px;
	margin-top:20px;
	width:1050px;
	border:1px solid green;
	}
	/* 뒤로가기 버튼 */
	.backButton {
		float: right;
		margin-right: 10px;
		margin-top: 25px;
		background: #013252;
		border: 0px;
		color: white;
		height: 35px;
		width: 70px;
		text-size: 15px;
	}
</style>
</head>
<body>
<%@ include file="../guide/business_menubar.jsp" %>
<%
	String managerPhone1 =recruit.getManagerPhone().substring(0,3);
	String managerPhone2 =recruit.getManagerPhone().substring(3,7);
	String managerPhone3 =recruit.getManagerPhone().substring(7,11);
	
	String address[] =recruit.getDetailArea().split(" ");
	String address1[] =recruit.getDetailArea().split("-");
	
	String status = recruit.getStatus();
%>
 <!-- section 영역 -->
	    <div class = "jari_register">   
	    	<!-- 채용정보 미리보기 -->
	    	<div>HOME > 채용정보</div>
	    	<div>
	    		 <form action="">
	    		 	<table>
	    		 		<tr>
	    		 			<td width="76%" height="40px"></td>
	    		 			<td width="130px"></td>
	    		 			<td width="130px">조회수 :  <%=click%> </td>
	    		 		</tr>
	    		 	</table>
	    		 </form>
	    		 <div id="table">
	    		 <form action="">
	    		 	<table>
	    		 		<tr>
	    		 			<td colspan="8" width="100px" height="100px" id="align_left"><h3><%=recruit.getCompanyName() %></h3>
	    		 			<h2><%=recruit.getRecruitTitle() %></h2></td>
	    		 			
	    		 			<td width="200px" height="100px">
	    		 			<!-- <img alt="duck.png" src="../../static/images/register/duck.png" width="150px" height="80px" style="margin-left: auto; margin-right: auto; display: block";> -->
	    		 			<img class="contentImg" width="156px" height="60px" alt="" src="<%=request.getContextPath()%>/thumbnail_uploadFiles/<%=titleImg.getChangeName()%>">
	    		 			</td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="100px" height="100px"><img alt="money.png" src="../../static/images/register/money.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="120px" height="100px" id="align_left1"><h4><%=recruit.getSalaryType() %><br><%=recruit.getSalary() %>원</h4></td>
	    		 			<td width="100px" height="100px"><img alt="calendar.png" src="../../static/images/register/calendar.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="120px" height="100px" id="align_left"><h4>요일<br><%=recruit.getJobDate() %></h4></td>
	    		 			<td width="100px" height="100px"><img alt="clock.png" src="../../static/images/register/clock.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="120px" height="100px" id="align_left1"><h4>시간<br><%=recruit.getJobTime() %></h4></td>
	    		 			<td width="90px" height="100px"><img alt="placeholder.png" src="../../static/images/register/placeholder.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="140px" height="100px" id="align_left1"><h4>지역<a href="#" style="color:red;"> 지도></a><br><%=address[0] %> <%=address[1] %></h4></td>
	    		 			<td width="200px" height="100px" id="align_left"><h4><%=recruit.getCompanyName() %><br>사업내용<br><%=recruit.getBusinessContent() %></h4></td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="110px" height="50px"><h3>[근무조건]</h3></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="110px" height="50px"><h3>[자격조건]</h3></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="90px" height="50px"></td>
	    		 			<td width="90px" height="50px"></td>
	    		 			<td rowspan="2" width="200px" height="100px"><br><br><h3>마감시간</h3></td>
	    		 		</tr>
	    		 		<% 
	    		 			String CareerChoice = recruit.getCareerChoice();
							String EduYn = recruit.getEduYn();
							String preference = recruit.getPreference();
	    		 			String welfare = recruit.getWelfare(); 
	    		 			String detailText = recruit.getDetailText();
	    		 		%>
	    		 		<tr>
	    		 			<td width="110px" height="50px" class="align_left2"><b>근무유형</b></td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><%=recruit.getJobType() %></td>
	    		 			<td width="110px" height="50px" class="align_left2"><b>근무시간</b></td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><%=recruit.getJobTime() %></td>
	    		 			<td width="110px" height="50px" class="align_left2"><b>경력</b></td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><% if(CareerChoice.equals("경력")){ %><%=CareerChoice%> <%=recruit.getCareerPeriod()%>년<% }else{ %><%=recruit.getCareerChoice()%><%} %></td>
	    		 			<td width="90px" height="50px" class="align_left2"><b>학  력</b></td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><% if(EduYn.equals("선택")){ %><%=recruit.getEducation()%><% }else{ %><%=EduYn%><%} %></td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="110px" height="50px" class="align_left2"><b>근무요일</b></td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><%=recruit.getJobDate() %></td>
	    		 			<td width="110px" height="50px" class="align_left2"><b>근무직종</b></td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><p><%=recruit.getRecruitMajor() %></td>
	    		 			<td width="110px" height="50px" class="align_left2"><b>우대/가능</b></td>
	    		 			<td rowspan ="2" width="110px" height="50px" class="align_left1 align_left2"><% if(preference==null){ %>-<% }else{ %><%=recruit.getPreference()%><%} %></td>
	    		 			<td width="90px" height="50px" class="align_left2"><b>복리후생</b></td>
	    		 			<td rowspan ="2" width="110px" height="50px" class="align_left1 align_left2"><% if(welfare==null){ %>-<% }else{ %><%=recruit.getWelfare()%><%} %></td>
	    		 			<td rowspan="2" width="200px" height="100px"><h2 style="color:red;"><%=recruit.getExpirationDate()%></h2><br><br></td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="110px" height="50px" class="align_left2">급여</td>
	    		 			<td colspan="3" width="110px" height="50px" class="align_left1 align_left2"><%=recruit.getSalaryType() %> : <%=recruit.getSalary() %> 원</td>
	    		 			

	    		 			
	    		 		</tr>
	    		 	</table>
	    		 </form>
	    		</div><!-- 하늘색 요약 테이블 div 끝 -->
	    		<div><!-- 공간 띄우기용 div 진혁아 이부분에 지원하기 버튼만들어서 사용하면된다.-->
	    		<br><br><br>
	    		</div><!-- 공간 띄우기 끝 -->
	    		 <div><!-- 상세모집 내용 회사 정보 근무 지역  tap부분  -->
	 				
	 				<table class="line">
	 					<tr id="table">
	 						<td width="365px" height="60px" class="line1" onclick=""><h3>상세모집내용</h3></td>
	 						<td width="365px" height="60px" class="line2" style="border:1px solid #afafaf;"><h3>회사정보</h3></td>
	 						
	 						<td width="365px" height="60px" class="line3"><h3>근무지역</h3></td>
	 					</tr>
	 					<tr>
	 						<td id="align_left" colspan="3" class="line recruitContent"  >
	 						
	 								<br>
	 								<h4>[모집내용]</h4>
	 								<p>- 모집직종  : <%=recruit.getRecruitMajor() %><br>
	 								<p>- 근무유형 :  <%=recruit.getJobType()%><br>
	 								<p>- 근무요일  :  <%=recruit.getJobDate()%><br>
	 								<p>- 근무시간  : <%=recruit.getJobTime()%><br>
	 								<p>- 급여  : <%=recruit.getSalaryType()%> <%=recruit.getSalary()%><br>
	 								</p>
	 								<h4>[우대·가능]</h4>
	 									<p><% if(preference==null){ %>-<% }else{ %><%=recruit.getPreference()%><%} %></p>
	 								<h4>[복리후생]</h4>
	 									<p><% if(welfare==null){ %>-<% }else{ %><%=recruit.getWelfare()%><%} %></p>
	 								<h3>* 전화 연락시 "보금자리 구인정보 보고 전화 드렸어요"라고 말씀해주세요.</h3>
	 								<br>
	 								<h4><% if(detailText==null){ %>-<% }else{ %><%=recruit.getDetailText()%><%} %></h4>
	 								<br>
	 							
	 							<br>
	 						</td>
	 					</tr>
	 					<tr>
	 						<td id="align_left" colspan="3" class="line CompanyContent" style="display: none;" >
	 						
	 								<table>
	 									<tr>
	 										<td rowspan="2" width="300px" style="text-align: left; padding-left:20px;">
	 										<h2> 회사정보</h2>
	 										<img class="contentImg" width="200px" height="80px" alt="" src="<%=request.getContextPath()%>/thumbnail_uploadFiles/<%=titleImg.getChangeName()%>">
	 										<br><h3><%=recruit.getCompanyName()%></h3>
	 										<b>[회사주소]</b><br>
	 										<p><%=address1[0]%><br>
	 										<%=address1[1]%><br>
	 										<b>[사업내용]</b><br>
	 										<%=recruit.getBusinessContent()%></p>
	 										</td>
	 										<td rowspan="2" width="420px" style="text-align: left; padding-left:20px;">
	 										<h2>채용담당자 정보</h2>
	 										<br>
	 										<h3>[채용담장자] <%=recruit.getManagerName()%></h3>
	 										<h3>[대 표 번 호] <%=recruit.getCompanyPhone()%></h3>
	 										<h3>[담장자전화] <%=managerPhone1%>-<%=managerPhone2%>-<%=managerPhone3%></h3>
	 										<h3>[ 이 메 일  ] <%=recruit.getManagerEmail()%></h3>
	 										</td>
	 										<td width="380px" height="140px"  style="text-align: left; padding-left:20px;">
	 										<img alt="phone.png" src="../../static/images/register/phone.png" width="30px" height="30px" style="float: left"><h3>&nbsp;&nbsp;전화문의 할 경우</h3><br>
	 										<label style="font-size: smail">"보금자리에서 보고 전화드렸어요."<br>&nbsp;라고 하시면빠른 문의가 가능합니다. </label></h4>
	 										</td>
	 									</tr>
	 									<tr>
	 										<td width="380px" height="140px"  style="text-align: left; padding-left:20px;">
	 										<img alt="alert.png" src="../../static/images/register/alert.png" width="30px" height="30px" style="float: left"><h3>&nbsp;&nbsp;주의사항</h3><br>
	 										<label style="font-size: smail"> 통장 / 신분증 / 비밀번호 요구에는<br>절대 응하지 마세요. <br> 사기나 범죄에 이용될 수 있습니다. </label></h4>
	 										</td>
	 									</tr>
	 								</table>
	 							
	 							<br>
	 						</td>
	 					</tr>
	 					<tr>
	 						<td id="align_left" colspan="3" class="line AreaContent" style="display: none;" >
	 						
	 								<table>
	 									 <tr>
	 									 	<td width="550px" height="50px" style="text-align: left;"><%=address1[0]%> <%=address1[1]%></td>
	 									 	<td width="600px" height="50px"><img alt="alert.png" src="../../static/images/register/alert.png" width="20px" height="20px">&nbsp;&nbsp;근무지 위치를 나타내며 호사 소재지와 일치하지 않을 수 있습니다.</td>
	 									 </tr>
	 									<tr>
	 										<td colspan="2"><img alt="map.png" src="../../static/images/register/map.png" width="100%" height="400">
	 									</tr>	 
	 								</table>
	 							<br>
	 						</td>
	 					</tr>
	 					<% if(status.equals("END")) {%>
	 					<table>
	 						<tr>
	 							<td colspan="7" height="20px"></td>
	 						</tr>
	 						<tr>
	 							<td colspan="7"><h2 style="color:orangered;"> 마감된 공고입니다. </h2></td>
	 						</tr>
	 						<tr>
        						<td width="380px"></td>
        						<td width="100px" height="40px">
        						<td width=20px"></td>
        						<td width="100px" height="40px"><button id="returnList"class="backButton"><b>뒤로</b></button></td>
        						<td width=20px"></td>
        						<td width="100px" height="40px">
        						<td width=380px"></td>
        					</tr>
	 					</table> 
	 					<% }else if(status.equals("REJECTED")){ %>
	 					
	 					<table>
	 						<tr>
	 							<td colspan="7" height="20px"></td>
	 						</tr>
	 						<tr>
	 							<td colspan="7"><h2 style="color:orangered;">반려  사유 : <%=recruit.getRejectReason()%></h2></td>
	 						</tr>
	 						<tr>
        						<td width="380px"></td>
        						<td width="100px" height="40px">
        						<td width=20px"></td>
        						<td width="100px" height="40px"><button id="returnList"class="backButton"><b>뒤로</b></button></td>
        						<td width=20px"></td>
        						<td width="100px" height="40px">
        						<td width=380px"></td>
        					</tr>
	 					</table> 
	 					<% }else{ %>
	 					<table>
	 						<tr>
        						<td width="380px"></td>
        						<td width="100px" height="40px"><button id="returnList"class="backButton"><b>뒤로</b></button></td>
        						<td width=20px"></td>
        						<td width="100px" height="40px"><button id="viewUpdate"class="backButton"><b>수정</b></button></td>
        						<td width=20px"></td>
        						<td width="100px" height="40px"><button id="end"class="backButton"><b>마감</b></button></td>
        						<td width=380px"></td>
        					</tr>
	 					</table> 
						<% } %>
	 				</table>
	 			
	 		</div><!-- 공고 상세보기내용 tap부분 끝 -->
	 		</div>
	 		</div>
	 		
	    	
	    	
	 	
	    	
        <%@ include file="../guide/footer.jsp" %>
        
        
        <script type="text/javascript">
        //수정
        $(function(){
        	$("#viewUpdate")
        	.click(function(){
				var num =<%=recruit.getRecruitNo()%>
				console.log(num);
				location.href = "<%=request.getContextPath()%>/updateRecruit.bg?num=" + num;
			});
        });
        //뒤로
        $(function(){
        	$("#returnList")
        	.click(function(){
				var num =<%=recruit.getBmemberNo()%>
				console.log(num);
				location.href = "<%=request.getContextPath()%>/selectList.bg?num=" + num;
			});
        });
        //마감
        $(function(){
        	$("#end")
        	.click(function(){
				var num = <%=recruit.getRecruitNo()%>
				var bnum = <%=recruit.getBmemberNo()%>
				console.log(num);
				console.log(bnum);
				location.href = "<%=request.getContextPath()%>/deleteRecruit.bg?num=" + num +"&bnum="+ bnum;
			});
        });
        $(function(){
        	$(".line2").click(function(){
        		$(".CompanyContent").show();
        		$(".recruitContent").hide();
        		$(".AreaContent").hide();
        	});
        });
        $(function(){
        	$(".line3").click(function(){
        		$(".CompanyContent").hide();
        		$(".recruitContent").hide();
        		$(".AreaContent").show();
        	});
        });
        $(function(){
        	$(".line1").click(function(){
        		$(".CompanyContent").hide();
        		$(".recruitContent").show();
        		$(".AreaContent").hide();
        	});
        });
        </script>
</body>
</html>
	 								