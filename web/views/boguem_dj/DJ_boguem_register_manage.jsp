<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,com.kh.hbr.recruit.model.vo.*"%>
<% 
	ArrayList<Recruit> list = (ArrayList<Recruit>) session.getAttribute("list");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
<style type="text/css">
	/* ================= 다 정 ======================= */
	/* 가장 큰 DIV */
	.jari_register {
	
	height : 100%;
	/* margin-top : 20px;  */
	padding : 30px;
	width: 1100px;
	margin: 0 auto;
	}
	/* HOME > 보금자리 관리  */
	.homeNavi {
		margin-top: 47.5px;
		margin-left:20px;
	}
	
	/*보금자리관리  - 위아래 선  */
	.pageTitle {
		border-top: 3px solid rgb(192, 192, 192);
		border-bottom: 3px solid rgb(192, 192, 192);
		width:930px;
		height: 80px;
		float:right;
		margin-top: 30.5px;
		margin-bottom: 30.5px;
	}
	
	/* 다정 채용공고 검색 영역 */
	#tableDiv{
	width: 900px;
	height:100%;
	border: 1px solid #FOFOFO;
	float: left;
	margin-left: 15px;
	color: black;
	margin-bottom:50px;
	}
	/* 검색 테이블 배경색 */
	.selectTb{
	background:#ECF0F7;
	width:800px;
	height:180px;
	margin-left:60px;
	
	}
	
	/* 채용공고 테이블 css*/
	.TB { 
	border-collapse:collapse;
	border :1px solid #afafaf;
	}
	/* 채용공고 목록 글자크기 색 */
	.tableBack{
	background-color:#f0f0f0;
	font-weight:bold;
	border :1px solid #afafaf;
	}
	tr{
	border :1px solid #afafaf;
	}
	/* ========== 테이블 안에 정렬 ========== */
	.checkName{
	text-align: center;
	}
	.checkTB_right{
	text-align: center;
	}
	.checkTB_left{
	text-align: left;
	}
	.checkTb1{
	vertical-align: bottom;
	}
	.checkTb3{
	vertical-align: top;
	}
	/* 내용 DIV */
	#main{
	
	width:950px;
	 height:100%; 
	  margin-top: -31px; 
	 margin-left:200px;"
	}
	/* =========다정 검색영역 버튼=========== */
	.selectBtn{
	width :50px;
	height :70px;
	background:#013252;
	color:white;
	margin-bottom:20px;
	border:0;
	font-weight:bold;
	}
	/* ========== 이력서 열람내역 -> 제목 ========== */
	.pageTitle2 {
		padding: 23px;
	}
	/* 삭제버튼 */
	.tableButton {
		float: right;
		margin-right: 10px;
		margin-top: 25px;
		background: #013252;
		border: 0px;
		color: white;
		height: 35px;
		width: 70px;
		text-size: 15px;
	}
		/* ================ 좌측 메뉴바 ================ */
	
	/* left 메뉴 aside 클래스 */
	.leftMenu {
		float: left; 
		width: 20%; 
		height: 800px; 
		margin-left:-20px ;
		margin-top:20px;
	}
	
    .leftMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: black;
  	}
	
	/* left 메뉴  */
	.leftblock {
		display: block;
	}
	
	/* 이력서 관리 */
	#left1 {
		
		width:200px;
		height:250px;
	}

	/* left메뉴 가로선 */
	hr {
		width:85%;
		color:lightgray;
		size:2px;
	    margin-left: 5%;
	}
	
	/* 블렛 삭제 */
	ul {
		list-style:none;
	}
	.left {
		border:2px solid rgb(192, 192, 192);
	}
</style>
</head>
<body>
<%@ include file="../guide/business_menubar.jsp" %>
<div class="jari_register">
<!-- 좌측 메뉴바 -->
        <aside class="leftMenu">
        	<div class="left leftblock" id="left1">
        	<br>&nbsp;&nbsp;
        	<label style="font-size:20px;"><b>보금자리 관리</b></label>
        	<br><br>
        	<hr>
        	<br>
           	<ul>
		   		<li><a href="#" style="font-size:18px; color:#2FA599;"><b>보금자리 현황</b></a>
		   		</li>
		   		<br>
		   		<hr>
		   		<br>
		    	<li><a href="#" style="font-size:18px;" onclick="applicantManage();"><b>지원자관리</b></a>
		    	</li>
		    	<br>
	  		</ul>
	  		</div>

       	 </aside>
       	
       	<div id="main">
        <!-- HOME > 보금자리 관리 -->
        <div class="homeNavi" >
        	<label style="font-size:15px;">HOME > 보금자리 관리  > 채용공고 현황</label>
        </div>
        
        <!-- 보금자리 관리-->
        <div class="pageTitle">
        	<div class="pageTitle2">
        		<label style="font-size:25px;"><b>보금자리 관리</b></label>
        	</div>
        </div>
        <!-- 보금자리 채용 검색 -->
         <div id="tableDiv">
         	 <form id="searchForm" action="<%= request.getContextPath() %>/search.bg" method="post">
         		<table class="selectTb"  >
         		<tr></tr>
         		<tr >
         			<td rowspan ="2" width="70px"><input type="text" name="bmemberNo" value="<%=bloginUser.getBmemberNo() %>" hidden></td>
         			<td width="90px" rowspan = "2"><h3>검색<br>조건</h3></td>
         			<td width="120px"><p style="font-weight:900;" >&nbsp; 게시글 찾기</p></td>
         				<td width="130px;" height="70px" >
         					<select name="selectName" style="width:120px; height:35px;;">
								<option value="all">선택</option>
								<option value="num">등록번호</option>
								<option value="registerName">공고명</option>
								<option value="productName">적용상품명</option>
							</select></td>
						<td width="230px;"><input type="text" name="searchValue"style="width:200px; height:32px;"></td>
					<td rowspan="2" >&nbsp;&nbsp;&nbsp;<button class="selectBtn">검색</button></td>
         
         		</tr>
         		
         		<tr>
         			<td width="120px"  height="60px" class="checkTb3"><p style="font-weight:900;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 상 태</p> </td>
         				<td colspan="4" width="320px" class="checkTb3">
         				<input type="radio" id="all" name="status" value="all"><lable style="font-weight:900;">전체선택&nbsp;&nbsp;
         				<input type="radio" id="now" name="status" value="심사대기">대기&nbsp;&nbsp;
         				<input type="radio" id="notyet" name="status" value="APPROVED">승인&nbsp;&nbsp;
         				<input type="radio" id="end" name="status" value="REJECTED">반려&nbsp;&nbsp;
         				<input type="radio" id="end" name="status" value="END">마감&nbsp;&nbsp;</lable></td>
         		</tr>
         
         		<tr>
         		</tr>
         		
         		</table>
         	</form> 
         
         </div> 
         <!-- 채용공고 목록 -->
        <div id="tableDiv">
        	<form action="">
        		<table>
        			<tr>
        				<th height="50px"><h3>채용공고 목록&nbsp;&nbsp;&nbsp;&nbsp;|</h3></th>
        				<th><h4>&nbsp;&nbsp;총 &nbsp;<a style="color:red"><%= list.size() %></a>건</h4></th>
        				
        			</tr>
        		</table>
        		<table class="TB" style="text-align:center;">
	    	 		<tr class="tableBack">
	    	 			<!-- <th width="65px" height="50px">선택</th> -->
	    	 			<th width="100px" height="50px">등록번호</th>
	    	 			<th width="415px">공고명</th>
	    	 			<th width="100px">적용상품</th>
	    	 			<th width="80px">조회수</th>
	    	 			<th width="80px">상태</th>
	    	 			<th width="120px">등록일</th>
	    	 		</tr>
	    	 		<% for (Recruit rc : list){ 
	    	 			
	    				//기업 회원번호가 필요해서 추가함 (다영)
	    	 			int bmemberNo = bloginUser.getBmemberNo();
	    				String productName = rc.getProductName();
	    				String proName = "";
	    				if(productName != null && productName.length()>0 ){
	    					proName = productName.substring(0,productName.length()-1);
	    					
	    				}
	    	 		%>
	    	 		<tr height="130px">
	    	 			<!-- <td height="130px"><input type="radio" name="selectRecruit" ></td> -->
	    	 			<td id="detail"><%= rc.getRecruitNo() %></td>
	    	 			<td id="detail" style="text-align:left; padding-left:10px;"><label style="font-weight: bold;font-size:20px;"><%= rc.getRecruitTitle() %></label><br>
	    	 				<label style="font-weight: bold;">게재기간 : <%= rc.getEnrollDate() %> ~ <%= rc.getExpirationDate() %></label><br>
	    	 				<p>지역 : <%= rc.getDetailArea() %>
	    	 				<br>업종 : <%= rc.getRecruitMajor() %></p></td>
	    	 			<%-- <td id="detail"><%=rc.getProductName()%></td> --%>
	    	 			<td><% if(productName==null){ %>일반<% }else{ %><%=proName%><%} %></td>
	    	 			<td id="detail"><%= rc.getClickCount() %></td>
	    	 			<td id="detail"><% if(rc.getStatus().equals("END")){%>마감<%} else if(rc.getStatus().equals("APPROVED")){%>승인<%}else if(rc.getStatus().equals("REJECTED")){%>반려<%}else{ %>대기<%} %></td>
	    	 			<td id="detail"><%= rc.getEnrollDate() %></td>
	    	 			<!-- 기업회원번호가 필요해서 추가함! (다영) -->
	    	 			<input type="hidden" id="bMemberNo" value="<%=bmemberNo%>">
	    	 		</tr>
	    	 		<% } %>
	    	 		
        		</table>
        		
        	</form>
        
        </div>
        <br><br><br><br><br><br><br>
         <br><br><br><br><br><br><br>  
         </div>      

</div>


<script>
		$(function(){
			$(".TB td").mouseenter(function(){
				$(this).parent().css({"background":"#EFEFEF", "cursor":"pointer"});
			}).mouseout(function(){
				$(this).parent().css("background", "none");
			}).click(function(){
				var num = $(this).parent().children().eq(0).text();
				console.log(num);
				location.href = "<%=request.getContextPath()%>/registerView.bg?num=" + num;
			});
		});
		
		//지원자관리 서블릿으로 이동 - 다영
		function applicantManage() {
			
			var bnum = $("#bMemberNo").val();
			
			location.href="<%=request.getContextPath()%>/applicantManage.bs?bmemberNo=" + bnum;
		}

	</script>
</body>
</html>