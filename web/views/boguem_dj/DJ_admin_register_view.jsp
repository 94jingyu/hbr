<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.kh.hbr.recruit.model.vo.Recruit, java.util.*"%>
<%@ page import="com.kh.hbr.board.jengukBoard.model.vo.Attachment" %>
<%@ page import="java.util.ArrayList" %>
<% 
	Recruit recruit = (Recruit) session.getAttribute("recruit");
	int click = recruit.getClickCount();
	
	ArrayList<Attachment> fileList = (ArrayList<Attachment>) session.getAttribute("fileList");
	Attachment titleImg = fileList.get(0);
	

%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>보금자리 등록 관리자 승인심사</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
* {margin: 0; padding: 0;}
#wrap {width: 1200px; height: 100%; margin: 0 auto;}

/* ============= 헤더 영역 ============= */
header {width: 100%; height: 100px; background: #013252;}
.logo {font-size: 45px; color: white;}
#title {margin-left: 10px;}

/* 사이트 바로가기 td id명 */
#goSite {font-size: 18px;}

/* =========== 좌측메뉴 및 내용 ============ */
.leftMenu {float: left; width: 20%; height: 1800px; background-color: #ECF0F7; border-right: 3px solid #CDCDCD}
.leftMenu {/* border-right: hidden; */ border-right: 3px solid #CDCDCD}
.leftMenu>ul {margin: 0; padding: 0; list-style: none;}
.leftMenu a {display: block; text-decoration: none; padding: 10px 20px; color: black;}
.leftMenu a:hover {background: darkgray; color: white;}

/* ========== HOME > 회원관리 > 기업인증내역 ========== */
.homeNavi {margin-top: 50px; float: right;}

/* ========== 기업인증내역 - 위아래 선 ========== */
.pageTitle {border-top: 3px solid rgb(200, 200, 200); border-bottom: 3px solid rgb(200, 200, 200);
	width: 930px; height: 80px; float: right; margin-top: 30px; margin-bottom: 90px;}

/* ========== 기업인증내역 -> 제목 ========== */
.pageTitle2 {padding: 23px;}

/* ============= 테이블 영역 ============= */

/* 기업인증내역 테이블 div class */
.Request {width: 850px;	margin-left: 30px; margin-top:50px;}



/* 검색할 키워드 입력 div class (text상자) */
.searchKey {
	width: 180px;
	height: 50px;
	float: right;
}



/* ============= 다정 부분 테이블 영역 ============= */
	
		/* 상단에 패딩 */
	.t{
	padding-top: 10px;
	}
	/* 하단에 패딩 */
	.b{
	padding-bottom: 10px;
	}
	/* 테이블 글씨 가운데 정렬 */
	table{
	text-align :center;
	}
	/* 테이블안에 나만 왼쪽 정렬 */
	#align_left{
	text-align: left;
	padding-left:10px;
	}
	#align_left1{
	text-align: left;
	}
	.align_left1{
	text-align: left;
	}
	.align_left2{
	vertical-align:top;
	}
	/* 보금자리 미리보기 요약  배경색*/
	#table{
	background:#ECF0F7;
	}
	/* 보금자리 미리보기 상세내용 테이블 테두리 */
	.line{
	border-collapse:collapse;
	border :1px solid #afafaf;
	}
	/* 보금자리 미리보기 상세내용 작성내용 div */
	 .content{
	margin-left:10px;
	margin-top:20px;
	width:1050px;
	border:1px solid green;
	}
	/* 뒤로가기 버튼 */
	.backButton {
		float: right;
		margin-right: 10px;
		margin-top: 25px;
		background: #013252;
		border: 0px;
		color: white;
		height: 35px;
		width: 70px;
		text-size: 15px;
	}

	
</style>
</head>
<body>
	<div id="wrap">
	<%@ include file="../guide/admin_menubar.jsp" %>
	<%
	String managerPhone1 =recruit.getManagerPhone().substring(0,3);
	String managerPhone2 =recruit.getManagerPhone().substring(3,7);
	String managerPhone3 =recruit.getManagerPhone().substring(7,11);
	
	String address[] =recruit.getDetailArea().split(" ");
	String address1[] =recruit.getDetailArea().split("-");
	String status = recruit.getStatus();
%>
		<div class="homeNavi">
			<label style="font-size: 15px;">HOME > 보금자리관리 > 보금자리등록심사</label>
		</div>

		<div class="pageTitle">
			<div class="pageTitle2">
				<label style="font-size: 25px;"><b>보금자리 등록심사 상세보기</b></label>
			</div>
			<br>
			<!-- 여기부터 고치기 -->
			 <div id="table"> 
	    		 	<table>
	    		 		<tr>
	    		 			<td colspan="8" width="100px" height="100px" id="align_left"><h3><%=recruit.getCompanyName() %></h3>
	    		 			<h2><%=recruit.getRecruitTitle() %></h2></td>
	    		 			
	    		 			<td width="200px" height="100px"><img class="contentImg" width="156px" height="60px" alt="" src="<%=request.getContextPath()%>/thumbnail_uploadFiles/<%=titleImg.getChangeName()%>"></td>
	    		 			</tr>
	    		 		<tr>
	    		 			<td width="100px" height="100px"><img alt="money.png" src="../../static/images/register/money.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="120px" height="100px" id="align_left1"><h4><%=recruit.getSalaryType() %><br><%=recruit.getSalary() %>원</h4></td>
	    		 			<td width="100px" height="100px"><img alt="calendar.png" src="../../static/images/register/calendar.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="120px" height="100px" id="align_left"><h4>요일<br><%=recruit.getJobDate() %></h4></td>
	    		 			<td width="100px" height="100px"><img alt="clock.png" src="../../static/images/register/clock.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="120px" height="100px" id="align_left1"><h4>시간<br><%=recruit.getJobTime() %></h4></td>
	    		 			<td width="90px" height="100px"><img alt="placeholder.png" src="../../static/images/register/placeholder.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="140px" height="100px" id="align_left1"><h4>지역</a><br><%=address[0] %><br><%=address[1] %></h4></td>
	    		 			<td width="200px" height="100px" id="align_left"><h4><%=recruit.getCompanyName() %><br>사업내용<br><%=recruit.getBusinessContent() %></h4></td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="110px" height="50px"><h4>[근무조건]</h4></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="110px" height="50px"><h4>[자격조건]</h4></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="90px" height="50px"></td>
	    		 			<td width="90px" height="50px"></td>
	    		 			<td rowspan="2" width="200px" height="100px"><br><br><h3>마감시간</h3></td>
	    		 		</tr>
	    		 		<% 
	    		 			String CareerChoice = recruit.getCareerChoice();
							String EduYn = recruit.getEduYn();
							String preference = recruit.getPreference();
	    		 			String welfare = recruit.getWelfare(); 
	    		 			String detailText = recruit.getDetailText();
	    		 			
	    		 		%>
	    		 		<tr>
	    		 			<td width="110px" height="50px" class="align_left2">근무유형</td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><%=recruit.getJobType() %></td>
	    		 			<td width="110px" height="50px" class="align_left2">근무시간</td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><%=recruit.getJobTime() %></td>
	    		 			<td width="110px" height="50px" class="align_left2">경력</td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><% if(CareerChoice.equals("경력")){ %><%=CareerChoice%> <%=recruit.getCareerPeriod()%>년<% }else{ %><%=recruit.getCareerChoice()%><%} %></td>
	    		 			<td width="90px" height="50px" class="align_left2">학  력</td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><% if(EduYn.equals("선택")){ %><%=recruit.getEducation()%><% }else{ %><%=EduYn%><%} %></td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="110px" height="50px" class="align_left2">근무요일</td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><%=recruit.getJobDate() %></td>
	    		 			<td width="110px" height="50px" class="align_left2">근무직종</td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><%=recruit.getRecruitMajor() %></td>
	    		 			<td width="110px" height="50px" class="align_left2">우대/가능</td>
	    		 			<td rowspan ="2" width="110px" height="50px" class="align_left1 align_left2"><% if(preference==null){ %>-<% }else{ %><%=recruit.getPreference()%><%} %></td>
	    		 			<td width="90px" height="50px" class="align_left2">복리후생</td>
	    		 			<td rowspan ="2" width="110px" height="50px" class="align_left1 align_left2"><% if(welfare==null){ %>-<% }else{ %><%=recruit.getWelfare()%><%} %></td>
	    		 			<td rowspan="2" width="200px" height="100px"><h2 style="color:red;"><%=recruit.getExpirationDate()%></h2><br><br></td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="110px" height="50px" class="align_left2">급여</td>
	    		 			<td colspan="3" width="110px" height="50px" class="align_left1 align_left2"><%=recruit.getSalaryType() %> : <%=recruit.getSalary() %> 원</td>
	    		 			

	    		 			
	    		 		</tr>
	    		 	</table>
	    		 
	    		</div><!-- 하늘색 요약 테이블 div 끝 -->
	    		<div><!-- 공간 띄우기용 div 진혁아 이부분에 지원하기 버튼만들어서 사용하면된다.-->
	    		<br><br><br>
	    		</div><!-- 공간 띄우기 끝 -->
	    		 <div><!-- 상세모집 내용 회사 정보 근무 지역  tap부분  -->
	 				
	 				<table class="line">
	 					<tr id="table">
	 						<td width="365px" height="60px" class="line1" onclick=""><h3>상세모집내용</h3></td>
	 						<td width="365px" height="60px" class="line2" style="border:1px solid #afafaf;"><h3>회사정보</h3></td>
	 						<td width="365px" height="60px" class="line3"><h3>근무지역</h3></td>
	 					</tr>
	 					<tr>
	 						<td id="align_left" colspan="3" class="line recruitContent"  >
	 						
	 								<br>
	 								<h4>[모집내용]</h4>
	 								<p>- 모집직종  : <%=recruit.getRecruitMajor() %><br>
	 								<p>- 근무유형 :  <%=recruit.getJobType()%><br>
	 								<p>- 근무요일  :  <%=recruit.getJobDate()%><br>
	 								<p>- 근무시간  : <%=recruit.getJobTime()%><br>
	 								<p>- 급여  : <%=recruit.getSalaryType()%> <%=recruit.getSalary()%><br>
	 								</p>
	 								<h4>[우대·가능]</h4>
	 								<p><% if(preference==null){ %>-<% }else{ %><%=recruit.getPreference()%><%} %></p>
	 								<h4>[복리후생]</h4>
	 									<p><% if(welfare==null){ %>-<% }else{ %><%=recruit.getWelfare()%><%} %></p>
	 								<h3>* 전화 연락시 "보금자리 구인정보 보고 전화 드렸어요"라고 말씀해주세요.</h3>
	 								<br>
	 								<h4><% if(detailText==null){ %>-<% }else{ %><%=recruit.getDetailText()%><%} %></h4>
	 								<br>
	 							
	 							<br>
	 						</td>
	 					</tr>
	 					<tr>
	 						<td id="align_left" colspan="3" class="line CompanyContent" style="display: none;" >
	 						
	 								<table>
	 									<tr>
	 										<td rowspan="2" width="280px" style="text-align: left; padding-left:20px;">
	 										<h2> 회사정보</h2>
	 										<div>
	 										<img class="contentImg" width="220px" height="85px" alt="" src="<%=request.getContextPath()%>/thumbnail_uploadFiles/<%=titleImg.getChangeName()%>">
	 										</div>
	 										<h3><%=recruit.getCompanyName()%></h3>
	 										<b>[회사주소]</b><br>
	 										<p><%=address1[0]%><br>
	 										<%=address1[1]%><br>
	 										<b>[사업내용]</b><br>
	 										<%=recruit.getBusinessContent()%></p>
	 										</td>
	 										<td rowspan="2" width="410px" style="text-align: left; padding-left:20px;">
	 										<h2>채용담당자 정보</h2>
	 										<br>
	 										<h3>[채용담장자] <%=recruit.getManagerName()%></h3>
	 										<h3>[대 표 번 호] <%=recruit.getCompanyPhone()%></h3>
	 										<h3>[담장자전화] <%=managerPhone1%>-<%=managerPhone2%>-<%=managerPhone3%></h3>
	 										<h3>[ 이 메 일  ] <%=recruit.getManagerEmail()%></h3>
	 										</td>
	 										<td width="410px" height="140px"  style="text-align: left; padding-left:20px;">
	 										<img alt="phone.png" src="../../static/images/register/phone.png" width="30px" height="30px" style="float: left"><h3>&nbsp;&nbsp;전화문의 할 경우</h3><br>
	 										<label style="font-size: smail">"보금자리에서 보고 전화드렸어요."<br>&nbsp;라고 하시면빠른 문의가 가능합니다.</label></h4>
	 										</td>
	 									</tr>
	 									<tr>
	 										<td width="410px" height="140px"  style="text-align: left; padding-left:20px;">
	 										<img alt="alert.png" src="../../static/images/register/alert.png" width="30px" height="30px" style="float: left"><h3>&nbsp;&nbsp;주의사항</h3><br>
	 										<label style="font-size: smail"> 통장 / 신분증 / 비밀번호 요구에는<br>절대 응하지 마세요. <br> 사기나 범죄에 이용될 수 있습니다. </label></h4>
	 										</td>
	 									</tr>
	 								</table>
	 							
	 							<br>
	 						</td>
	 					</tr>
	 					<tr>
	 						<td id="align_left" colspan="3" class="line AreaContent" style="display: none;" >
	 						
	 								<table>
	 									 <tr>
	 									 	<td width="550px" height="50px" style="text-align: left;"><%=address1[0]%> <%=address1[1]%> </td>
	 									 	<td width="600px" height="50px"><img alt="alert.png" src="../../static/images/register/alert.png" width="20px" height="20px" ><label style="font-size:14px;">&nbsp;&nbsp;근무지 위치를 나타내며 회사 소재지와 일치하지 않을 수 있습니다.</label></td>
	 									 </tr>
	 									<tr>
	 										<td colspan="2"><img alt="map.png" src="../../static/images/register/map.png" width="100%" height="400">
	 									</tr>	 
	 								</table>
	 							<br>
	 						</td>
	 					</tr>

	 			
	 				<% if( status.equals("APPROVED")) { %>
						
	 					<table>
	 						<tr>
        						<td width="380px"></td>
        						<td width="100px" height="40px">
        						<td width=20px"></td>
        						<td width="100px" height="40px"><button type="button" id="returnList"class="backButton"><b>뒤로</b></button></td>
        						<td width=20px"></td>
        						<td width="100px" height="40px">
        						<td width=380px"></td>
        					</tr>
	 					</table>
	 					
	 					
	 					<% } else if(status.equals("REJECTED")){ %>
							<table>
								<tr>
	 								<td colspan="7" height="20px"></td>
	 							</tr>
	 							<tr>
	 								<td colspan="7"><h2 style="color:orangered;">반려  사유 : <%=recruit.getRejectReason()%></h2></td>
	 							</tr>
	 							<tr>
        							<td width="380px"></td>
        							<td width="100px" height="40px">
	        						<td width=20px"></td>
	        						<td width="100px" height="40px"><button type="button" id="returnList"class="backButton"><b>뒤로</b></button></td>
        							<td width=20px"></td>
        							<td width="100px" height="40px">
        							<td width=380px"></td>
        						</tr>
	 						</table>	 					
	 					<% } else if(status.equals("END")){ %>
	 						<table>
	 							<tr>
	 								<td colspan="7" height="20px"></td>
	 							</tr>
	 							<tr>
	 								<td colspan="7"><h2 style="color:orangered;">마감된 공고입니다.</h2></td>
	 							</tr>
	 							<tr>
        							<td width="380px"></td>
        							<td width="100px" height="40px">
	        						<td width=20px"></td>
	        						<td width="100px" height="40px"><button type="button" id="returnList"class="backButton"><b>뒤로</b></button></td>
        							<td width=20px"></td>
        							<td width="100px" height="40px">
        							<td width=380px"></td>
        						</tr>
	 						</table>
	 					<% } else { %>
	 					<table  style="margin-left: 100px;" width=730px; height=100%; > <!-- class="line" -->
							<tr>
								<td colspan="2" height=50px;><a><%=recruit.getRecruitNo() %></a></td>
							</tr>
							<tr>
								<td colspan="2"height=50px;><h1>보금자리 등록 처리</h1></td>
							</tr>
							<tr>
								<td colspan="2" height=10px;></td>
							</tr>
							<tr>
								<td width=200px; height=35px; style="text-align: right;">제 목 :  &nbsp;&nbsp;</td>
								<td style="text-align: left;"><%=recruit.getRecruitTitle() %></td>
							</tr>
							<tr>
							<% 
								String productName = recruit.getProductName();
		    					String proName = "";
		    					if(productName != null && productName.length()>0 ){
		    					proName = productName.substring(0,productName.length()-1);}
		    					
							%>
								<td width=200px; height=35px; style="text-align: right;">적 용 상 품 :  &nbsp;&nbsp;</td>
								<td style="text-align: left;"><% if(productName==null){%>일반<%}else{ %><%= proName %><%} %><label hidden><%=recruit.getItemNo() %></label></td>
								
							</tr>
							<tr>
								<td width=200px; height=35px; style="text-align: right;">적 용 개 수 :  &nbsp;&nbsp;</td>
								<td style="text-align: left;"><%=recruit.getProductCount() %> 개</td>
							</tr>
							<tr>
								<td width=200px; height=35px; style="text-align: right;">처 리 :  &nbsp;&nbsp;</td>
								<td style="text-align: left;"><label><input id="approve" type="radio" name="approveORreject" value="승인" >승인&nbsp;&nbsp;&nbsp;&nbsp;</label><label><input id="reject" type="radio" value="반려" name="approveORreject">반려</label></td>
							</tr>
							<tr>
								<td width=200px; height=40px; style="text-align: right;">반 려 사 유 :  &nbsp;&nbsp;</td>
								<td style="text-align: left;">
									<select name="rejectReason"style="width:423px; height:30px;" id="rejectReason">
										<option value="">선택</option>
										<option value="광고">광고</option>
										<option value="음란">음란</option>
										<option value="욕설">욕설</option>
										<option value="기타">기타</option>
									</select>
								</td>
							</tr>
							<tr>
								<td width=200px; height=35px; style="text-align: right;">처 리 내 용  :  &nbsp;&nbsp;</td>
								<td rowspan="2" style="text-align: left; border:0;">
								<textarea rows="7" cols=50" style="width:420px; height:200px;" id="rejectReasonDetail"></textarea></td>
							</tr>
							<tr>
								<td width=200px; height=165px;></td>
								
							</tr>
						
						</table>
						
	 					<table>
	 						<tr>
        						<td width="380px"></td>
        						<td width="100px" height="40px"><button type="button" id="returnList"class="backButton"><b>뒤로</b></button></td>
        						<td width=20px"></td>
        						<td width="100px" height="40px"><button type="button" id="approved"class="backButton" onclick="approvedAndreject()" value="APPROVED"><b>승인</b></button></td>
        						<td width=20px"></td>
        						<td width="100px" height="40px"><button type="button" id="rejected"class="backButton" value="REJECTED"><b>반려</b></button></td>
        						<td width=380px"></td>
        					</tr>
	 					</table>
	 					<% } %>
	 				</table>
	 			
	 		</div><!-- 공고 상세보기내용 tap부분 끝 -->
     	</div>
     </div><!-- 가장큰 div 끝 -->
        <script type="text/javascript">

        //뒤로
        $(function(){
        	$("#returnList")
        	.click(function(){
			<%-- 	var num =<%=recruit.getBmemberNo()%>
				console.log(num); --%>
				location.href = "<%=request.getContextPath()%>/approvedList.ad";
			});
        });
        //승인
        $(function(){
        	$("#approved").click(function(){
        	alert("승인처리가 되었습니다.");
        	var recruitNo =<%=recruit.getRecruitNo()%>
        	var productCount =<%=recruit.getProductCount()%>
        	var itemNo =<%=recruit.getItemNo()%>
        	location.href = "<%=request.getContextPath()%>/approved.ad?productCount=" + productCount + "&recruitNo=" + recruitNo + "&itemNo=" + itemNo;
			});
        });
        //반려
        $(function(){
        	$("#rejected").click(function(){
        		alert("반려처리가 되었습니다.");
				var recruitNo =<%=recruit.getRecruitNo()%>
				var reasonDetail = $("#rejectReasonDetail").val();
				var productCount =<%=recruit.getProductCount()%>
				location.href = "<%=request.getContextPath()%>/rejected.ad?recruitNo=" + recruitNo + "&reasonDetail="+reasonDetail +"&productCount=" + productCount;
			});
        }); 
   


        
        //상세보기 show hide
        $(function(){
        	$(".line2").click(function(){
        		$(".CompanyContent").show();
        		$(".recruitContent").hide();
        		$(".AreaContent").hide();
        	});
        });
        $(function(){
        	$(".line3").click(function(){
        		$(".CompanyContent").hide();
        		$(".recruitContent").hide();
        		$(".AreaContent").show();
        	});
        });
        $(function(){
        	$(".line1").click(function(){
        		$(".CompanyContent").hide();
        		$(".recruitContent").show();
        		$(".AreaContent").hide();
        	});
        });
       //승인 라디오 버튼 눌렀을때 사유 못작성하게
        $(function(){
     	   $("#approve").click(function(){ 
     			$("#rejectReason").val("").prop("disabled",true).css("background","lightgray");
     			$("#rejectReasonDetail").val("").prop("disabled",true).css("background","lightgray");
     			$("#rejected").prop("disabled",true).css("background","lightgray");
     			$("#approved").prop("disabled",false).css("background","#013252");
     			
     		});
        });
       //반려 라디오 버튼 눌렀을때 쓸수 있게
       $(function(){
	   $("#reject").click(function(){ 
			$("#rejectReason").prop("disabled",false).css("background","white");
			$("#rejectReasonDetail").prop("disabled",false).css("background","white");
			$("#approved").prop("disabled",true).css("background","lightgray");
			$("#rejected").prop("disabled",false).css("background","#013252");
		});   
   });
      
      
        </script>
</body>
</html>