<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.hbr.recruit.model.vo.*"%>
<%@ page import="com.kh.hbr.board.jengukBoard.model.vo.Attachment" %>
<%@ page import="java.util.ArrayList" %>
<% 
	Recruit recruit = (Recruit) session.getAttribute("recruit");
	int click = recruit.getClickCount();
	
	ArrayList<Attachment> fileList = (ArrayList<Attachment>) session.getAttribute("fileList");
	Attachment titleImg = fileList.get(0);

%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style type="text/css">
/* 보금자리 등록 */
	.jari_register {
	border : 1px solid gray;
	height : 100%;
	/* margin-top : 20px;  */
	padding : 30px;
	width: 1100px;
	margin: 0 auto;

	}
	/* 상단에 패딩 */
	.t{
	padding-top: 10px;
	}
	/* 하단에 패딩 */
	.b{
	padding-bottom: 10px;
	}
	/* 상품적용 테이블 테두리*/
	.TB { 
	border-collapse:collapse;
	}
	tr {
	border :1px solid #afafaf;
	}
	/* 테이블 글씨 가운데 정렬 */
	table{
	text-align :center;
	}
	/* 테이블 화면 가운데 정렬 */
	#jari_2,.jari_3 {
	margin-left:300px;
	}
	/* 상품적용 버튼 */
	.button{
	width :100px;
	height :40px;
	background:#013252;
	color:white;
	margin-left: 190px;
	margin-top:10px;
	border-radius:8px;
	border:0;
	font-weight:bold;
	}
	/* 보금자리 등록 버튼 */
	.button1{
	width :200px;
	height :60px;
	background:#013252;
	color:white;
	margin-left: 450px;
	margin-top:10px;
	border-radius:8px;
	border:0;
	font-weight:bold;
	font-size: 18px;
	}
	/* 상품 컬럼 배경색과 글씨굵기 */
	.tableBack{
	background-color:#f0f0f0;
	font-weight:bold;
	border :1px solid #afafaf;
	}
	/* 테이블안에 나만 왼쪽 정렬 */
	#align_left{
	text-align: left;
	}
	/*===========  복리후생 모달  =============*/
	/* 모달 안보이게 하기 */
	#jdj_modal{
      width:900px; 
      display: none;
    }
	#jdj_modal{
      position:relative;
      width:100%; 
      height:100%;
      z-index:1;
    } 
    /* 모달창에 관련된 div 설정창 */
    #jdj_modal .jyh_modal_content {
      width:570px;
      height:100%;
      margin:20px auto;
      padding:30px 30px;
      background:#fff;
      border:2px solid #666;
    } 
	/* 모달창 배경 흐리게 하는 것  */
    #jdj_modal .jyh_modal_layer {
      position:fixed;
      top:0;
      left:0;
      width:100%;
      height:100%;
      background: rgba(0, 0, 0, 0.8);
      z-index:-1;
    }   
    /*===========우대 가능 모달 =============*/
	/* 모달 안보이게 하기 */
	#jdj_modal2{
      width:900px; 
      display: none;
    }
	#jdj_modal2{
      position:relative;
      width:100%; 
      height:100%;
      z-index:1;
    } 
    /* 모달창에 관련된 div 설정창 */
    #jdj_modal2 .jyh_modal_content2 {
      width:570px;
      height:100%;
      margin:20px auto;
      padding:30px 30px;
      background:#fff;
      border:2px solid #666;
    } 
	/* 모달창 배경 흐리게 하는 것  */
    #jdj_modal2 .jyh_modal_layer2 {
      position:fixed;
      top:0;
      left:0;
      width:100%;
      height:100%;
      background: rgba(0, 0, 0, 0.8);
      z-index:-1;
    }   
      /*===========상품구매 모달 =============*/
	/* 모달 안보이게 하기 */
	#jdj_modal3{
      width:900px; 
      display: none;
    }
	#jdj_modal3{
      position:relative;
      width:100%; 
      height:100%;
      z-index:1;
    } 
    /* 모달창에 관련된 div 설정창 */
    #jdj_modal3 .jyh_modal_content3 {
      width:800px;
      height:100%;
      margin-left:-170px;
      padding:30px 30px;
      background:#fff;
      border:2px solid #666;
    } 
	/* 모달창 배경 흐리게 하는 것  */
    #jdj_modal3 .jyh_modal_layer3 {
      position:fixed;
      top:0;
      left:0;
      width:100%;
      height:100%;
      background: rgba(0, 0, 0, 0.8);
      z-index:-1;
    }   
    /*===========상품구매 2 모달 =============*/
	/* 모달 안보이게 하기 */
	#jdj_modal4{
      width:900px; 
      display: none;
    }
	#jdj_modal4{
      position:relative;
      width:100%; 
      height:100%;
      z-index:1;
    } 
    /* 모달창에 관련된 div 설정창 */
    #jdj_modal4 .jyh_modal_content4 {
      width:800px;
      height:100%;
      margin-left:-170px;
      padding:30px 30px;
      background:#fff;
      border:2px solid #666;
    } 
	/* 모달창 배경 흐리게 하는 것  */
    #jdj_modal4 .jyh_modal_layer4 {
      position:fixed;
      top:0;
      left:0;
      width:100%;
      height:100%;
      background: rgba(0, 0, 0, 0.8);
      z-index:-1;
    }   
    /*===========기업로고 업로드 모달 =============*/
	/* 모달 안보이게 하기 */
	#jdj_modal5{
      width:900px; 
      display: none;
    }
	#jdj_modal5{
      position:relative;
      width:100%; 
      height:100%;
      z-index:1;
    } 
    /* 모달창에 관련된 div 설정창 */
    #jdj_modal5 .jyh_modal_content5 {
      width:600px;
      height:100%;
      margin:20px auto;
      padding:30px 30px;
      background:#fff;
      border:2px solid #666;
    } 
	/* 모달창 배경 흐리게 하는 것  */
    #jdj_modal5 .jyh_modal_layer5 {
      position:fixed;
      top:0;
      left:0;
      width:100%;
      height:100%;
      background: rgba(0, 0, 0, 0.8);
      z-index:-1;
    }   
     /*===========모집직종 선택 모달 =============*/
	/* 모달 안보이게 하기 */
	#jdj_modal6{
      width:900px; 
      display: none;
    }
	#jdj_modal6{
      position:relative;
      width:100%; 
      height:100%;
      z-index:1;
    } 
    /* 모달창에 관련된 div 설정창 */
    #jdj_modal6 .jyh_modal_content6 {
      width:1200px;
      height:100%;
      margin-left:-90px;
      padding:30px 30px;
      background:#fff;
      border:2px solid #666;
    } 
	/* 모달창 배경 흐리게 하는 것  */
    #jdj_modal6 .jyh_modal_layer6 {
      position:fixed;
      top:0;
      left:0;
      width:100%;
      height:100%;
      background: rgba(0, 0, 0, 0.8);
      z-index:-1;
    }   
/* ================모집내용 모달 관련 CSS모음=================== */
/* 채용정보 전체 div */
.iqWrap {
	width: 1200px;
	/* height: 800px; */
	border: 1px solid lightgray;
	margin: 0 auto;
}

.iqLocation, .seouldiv {
	width: 1130px;
	/* height: 40px; */
	/* border: 1px solid red; */
	margin: 0 auto;
	margin-top: 1px;
}

.iqJob {
	width: 1120px;
	height: 500px;
	border: 1px solid red;
	margin: 0 auto;
	/* margin-top: 10px; */
}

.trLocation {
	align: center;
}

.tdLocation {
	height: 25px;
	width: 58px;
	border: 1px solid gray;
	text-align: center;
	margin: 1px;
	line-height: 28px;
	cursor: pointer;
}

/* 지역 선택버튼 */
.tdLoSeoul, .gyoengi2, .inchoen2, .busan2, .daegu2, .gwangju2, .daejeon2,
	.ulsan2, .sejong2, .gangwon2, .kyeoungnam2, .kyeoungbuk2, .jeonnam2,
	.jeonbuk2, .chungnam2, .chungbuk2, .jeju2 {
	border: 1px solid gray;
	text-align: center;
	height: 25px;
	width: 126px;
	background-color: white;
	float: left;
	margin: 6px;
	line-height: 28px;
	cursor: pointer;
}

/* 지역선택 div영역 */
#seouldiv, #gyoengidiv, #inchoendiv, #busandiv, #daegudiv, #gwangjudiv,
	#daejeondiv, #ulsandiv, #sejongdiv, #gangwondiv, #kyeoungnamdiv,
	#kyeoungbukdiv, #jeonnamdiv, #jeonbukdiv, #chungnamdiv, #chungbukdiv,
	#jejudiv {
	width: 1130px;
	/* height: 160px; */
	border: 1px solid #8D8D8D;
	margin-left: 34px;
	background-color: lightgray;
}

/* 직종 선택버튼  */
.kindsJobTop {
	height: 25px;
	width: 111px;
	border: 1px solid gray;
	text-align: center;
	font-size: 15px;
	background-color: white;
	/* margin: 0.1px; */
	float: left;
	vertical-align: middle;
	line-height: 28px;
	cursor: pointer;
}

#kindsJob {
	width: 1130px;
	margin-left: 34px;
	/* border: 1px solid red; */
	/* background-color: lightgray; */
}

.cooking2, .medical2, .construct2, .office2, .driving2, .sales2,
	.storeManger2, .teacher2, .etc2 {
	border: 1px solid gray;
	text-align: center;
	height: 25px;
	width: 126px;
	background-color: white;
	float: left;
	margin: 6px;
	line-height: 26px;
	cursor: pointer;
}

/* 직종선택 div영역 */
/* #kindsJob,  */
#cookingDiv, #medicalDiv, #constructDiv, #officeDiv, #drivingDiv,
	#salesDiv, #storeMangerDiv, #teacherDiv, #etcDiv {
	width: 1130px;
	border: 1px solid #8D8D8D;
	margin-left: 34px;
	background-color: #d3d3d3;
	
}
.cooking2:hover, .medical2:hover, .construct2:hover, .office2:hover, .driving2:hover, .sales2:hover,
   .storeManger2:hover, .teacher2:hover, .etc2:hover {
   background:#013252;
   color:white;
}
.kindsJobTop:hover {
   background:#013252;
   color:white;
}
.tdLoSeoul:hover, .gyoengi2:hover, .inchoen2:hover, .busan2:hover, .daegu2:hover, .gwangju2:hover, .daejeon2:hover,
   .ulsan2:hover, .sejong2:hover, .gangwon2:hover, .kyeoungnam2:hover, .kyeoungbuk2:hover, .jeonnam2:hover,
   .jeonbuk2:hover, .chungnam2:hover, .chungbuk2:hover, .jeju2:hover {
   background:#013252;
   color:white;
   
}
.tdLocation:hover {
   background:#013252;
   color:white;
}
.jKinds{
font-size:12px
}
/* ================모집내용 모달 관련 CSS모음 끝=================== */
	
</style>
</head>
<body>
 <%@ include file="../guide/business_menubar.jsp" %>
 <%		String email = bloginUser.getManagerEmail();
		String managerEmail[] = recruit.getManagerEmail().split("@");
 		String companyPhone[] = recruit.getCompanyPhone().split("-");
 		/* String managerPhone[] = bloginUser.getManagerPhone().split("-"); */
 		String managerPhone1 =recruit.getManagerPhone().substring(0,3);
 		String managerPhone2 =recruit.getManagerPhone().substring(3,7);
 		String managerPhone3 =recruit.getManagerPhone().substring(7,11);
 		String age[] = recruit.getInterestAge().split("~");
 		String Time[] = recruit.getJobTime().split("~");
 		String jobTime[] = Time[0].split(":");
 		String jobTime1[] = Time[1].split(":");
 		String gender = recruit.getInterestGender();
 		String fax[] = recruit.getFax().split("-");
 		String address[] = recruit.getDetailArea().split("-");
 		String homepage = recruit.getHomepage();
 		
 		String interestGender = recruit.getInterestGender();
 		String interestAgeYn = recruit.getInterestAgeYn();
 		String eduYn =recruit.getEduYn();
 		String careerChoice =recruit.getCareerChoice();
 		String productName = recruit.getProductName();
 		String jobDate = recruit.getJobDate();
 		String jobType =recruit.getJobType();
 		String salaryType = recruit.getSalaryType();
 		String salaryNego = recruit.getSalaryNego();
 		String jobTimeNego = recruit.getJobTimeNego();
%>
<!-- ================section 영역====================== -->
 <br>
	    <div class = "jari_register">  
	     <form id="registerUpdateForm" action="<%= request.getContextPath() %>/registerUpdateComplete.bg" method="post" encType="multipart/form-data">
	    	<!-- 내용소개 부분 -->
	    	 <div class = "jari_1">
	    	 	<h2>보금자리 수정</h2>
	    	 	<br>
					<p> &nbsp;&nbsp;&nbsp;• 등록 후 24시간 이내 심사를 통해 14일 동안 게재됩니다. (주말 및 공휴일 제외) / 1일 1건, 최대 5건 게재<br>
						&nbsp;&nbsp;&nbsp;• 부적합한 공고는 사전 동의 없이 수정/삭제될 수 있으며, 게재 중인 공고는 보류나 환불이 되지 않습니다.<br>
						&nbsp;&nbsp;&nbsp;• 상품의 경우, 주말/업무시간 외 신청시 다음 업무일에 적용되며 사용갯수만큼 일수가 계산되어 적용됩니다.<br>
						&nbsp;&nbsp;&nbsp;• 한 번 적용된 상품은 보금자리 수정 시 수정이 불가능합니다.  <br>
						&nbsp;&nbsp;&nbsp;• 적용할 상품을 선택하고 상품적용 버튼을 클릭하면 현재 보유하고 있는 상품내역을 볼 수 있습니다.<br><br>
					</p>
	
    	 	<!-- 기업정보 -->
				<h2 class = "b">기업정보<label style="color:red; font-size:12px; text-align:right;">&nbsp;&nbsp;&nbsp;* 필수입력</label></h2>
				<hr style="border:solid 1.5px #013252">
				<br>
<!-- ================================= 우대가능 모달  ============ -->
	   	<div id="jdj_modal5">
		<div class="jyh_modal_content5">
		   	<table style="text-align: left;">
		   	    <tr>
		   	   		<td><h1> 로고등록</h1></td>
		   	    	<td colspan="3" width="150px" height="60px" colspan="3" width="570px" style="text-align:right; vertical-align: bottom;">*로고 파일 선택 후 적용 버튼을 꼭 눌러주세요</td>
		   	    </tr>
		   	    <tr>
		   			<td colspan="4" height="12px"><hr style="border:solid 1.5px #013252"></td>
		   			
		   		</tr>
		   		 <tr>
		   			<td colspan="4" height="60px" style="text-align:center;">로고 미리보기</td>
		   			
		   		</tr>
		   		 <tr>
		   			<td colspan="4" height="12px"><hr style="border:solid 1.5px #013252"></td>
		   			
		   		</tr>
		   		<tr>
		   			<td width="200px" height="30px" style="text-align:center;" >파일등록</td>
		   			<td colspan="2" width="250px"><input type="file"></td>
		   			<td width="120px"></td>
		   		</tr>
		   		 <tr>
		   			<td colspan="4" height="12px"><hr style="border:solid 1px gray"></td>
		   			
		   		</tr>
		   		 <tr>
		   			<td colspan="4" height="50px">
		   			<label style="font-size: 12px; color:darkgray;">
		   			추천로고 사이즈 : <label style="font-weight:900; color:#37363a;">156px * 60px</label><br>
		   			<label style="font-weight: 900; color:#37363a;">JPG,PNG</label> 확장자를 가진 움직이지 않는 단 컷 이미지만 등록 합니다.
		   			규정에 맞지 않는 이미지는 임의 삭제/수정 될 수 있습니다. 
		   			</label>
		   			</td>
		   			
		   		</tr>
		   		<tr>
		   			<td></td>
		   			<td></td>
		   			<td></td>
		   			<td width="120px">
		   			<button type="button" id="jdj_modal_close5"  style="width:50px; height:30px; background:gray; color:white;">취소</button> 
		   			<button type="button" id="jdj_modal_close5"  style="width:50px; height:30px; background:#013252; color:white; float:right;">적용</button></td>
		   		</tr>
		   	</table>
		</div>
	   
		<div class="jyh_modal_layer5"></div>
	</div><!-- 우대  모달 div 끝  -->
			<div>
	    	 		<table>
	    	 			<tr>
	    	 				<td width="230px" height="50px">회사명<label style="color:red;">*</label></td>
							<td colspan="2" width="270px" height="50px"style="text-align: left;"><input type="text" style="width:300px; height:30px;" id="comName" name="companyName" value="<%= recruit.getCompanyName()%>"></td>
							<td width="200px" height="50px">홈페이지</td>
							
							<td width="500px" height="50px"><input type="text" placeholder="http://" style="width:500px; height:30px;" name="homepage" value="<% if(homepage == null){%>""<%} else if(homepage == ""){%>""<%}else{ %><%=homepage%><%} %>" ></td>
						</tr>
	    	 			<tr>
	    	 				<td width="230px" height="50px">주요사업내용<label style="color:red;">*</label></td>
							<td colspan ="4" width="600px" height="50px" style="text-align: left;"><input type="text" id="content1" class="recTitle" class="busiContent"name="businessContent" maxlength="35" style="width:750px; height:30px;" value="<%=recruit.getBusinessContent()%>">
    					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="counter1">###</span></td>
	    	 			<script type="text/javascript">
	    	 			$(function() {
	    	 			      $('#content1').keyup(function (e){
	    	 			          var content = $(this).val();
	    	 			          $(this).height(((content.split('\n').length + 1) * 1.13) + 'em');
	    	 			          $('#counter1').html('('+content.length + '/35자)');
	    	 			      });
	    	 			      $('#content1').keyup();
	    	 			});
	    	 			</script>
						<tr>
	    	 				<td width="230px" height="100px">로고 및 <br>기업소개 이미지</td>
						<!-- 	<td width="500px" ><div style="background:pink; width:156px; height:60px; margin-left:42px;" type="button" id="jdj_modal_open5"><br>사진</div></td> -->
							<td style="text-align: left;" width="180px" style="text-align: left;"><div class ="titleImg"style="border:1px, solid gray; cursor:pointer; width:156px; height:60px; margin-left:3px;" id ="contentImgArea">
							 <% String imgName = titleImg.getOriginName();%>
							 <img id ="titleImg"class="contentImg" width="156px" height="60px" alt="" 
							 	   src=<% if(imgName==null){%>
							 				"<%=request.getContextPath()%>/static/images/register/logoImg.png"
							 			<% }else{%>
							 				"<%=request.getContextPath()%>/thumbnail_uploadFiles/<%=titleImg.getChangeName()%>"
							 			<%}%>></div></td>
							<td colspan="3" width="1040px" style="text-align: left;">
								<label style="font-size: 12px; color:darkgray;">
		   						추천로고 사이즈 : <label style="font-weight:900; color:#37363a; ">156px * 60px</label><br>
		   						<label style="font-weight: 900; color:#37363a;">JPG,PNG</label> 확장자를 가진 움직이지 않는 단 컷 이미지만 등록 합니다.
		   						규정에 맞지 않는 이미지는 임의 삭제/수정 될 수 있습니다. </label><br>
		   						<label style="font-size: 14px; color:orangered;">로고 변경을 원할 시 로고를 클릭하세요 </label>
								
							</td>
						</tr>
						<tr>
							<td colspan="4" width="740px" height="20px">
								<!-- 	필요한 정보 숨겨 놓기 -->		   						
								<input type="text" name="bmemberNo" value="<%=bloginUser.getBmemberNo() %>" style="visibility:hidden;">
		   						<input type="text" name="recruitNo" value="<%=recruit.getRecruitNo() %>" style="visibility:hidden;">
								<input type="file" id="thumbnailImg1" name="thumbnailImg1" onchange="loadImg(this, 1);" style="visibility:hidden;">
							</td>
						</tr>
    	 			</table>
    	 			<!-- 로고관련 스크립트 -->
    	 			<script type="text/javascript">
    	 			$(function() {
    					/* $("#fileArea").hide(); */
    					
    					$(".titleImg").click(function(){
    						$("#thumbnailImg1").click();
    					});
    	 			});
    	 			
    	 			function loadImg(value, num) {
    					if(value.files && value.files[0]) {
    						var reader = new FileReader();
    						
    						reader.onload = function(e) {
    							console.log(e.target.result);
    							switch(num) {
    								case 1 : $("#titleImg").attr("src", e.target.result); break;
    								
    							}
    							
    						}
    						
    						reader.readAsDataURL(value.files[0]);
    					}
    				}
    	 			</script>
	    	 <br>
	    	 <br>
    	 	</div>
				
				<!-- 담당자 정보 -->
				<h2 class = "b">담당자 정보<label style="color:red; font-size:12px; text-align:right;">&nbsp;&nbsp;&nbsp;* 필수입력</label></h2>
				<hr style="border:solid 1.5px #013252">
				<br>
				<div>
	    	 		<table>
	    	 			<tr>
	    	 				<td width="150px" height="50px">담당자명<label style="color:red;">*</label></td>
							<td colspan="4" width="400px" height="50px" style="text-align: left; "><input type="text" style="width:350px; height:30px; margin-left:10px;" id="manName" name="managerName" value="<%= recruit.getManagerName()%>"></td>
							<td  width="120px" height="50px">&nbsp;&nbsp;&nbsp;&nbsp;이메일<label style="color:red;">*</label></td>
							<td width="130px" height="50px"><input type="text" style="width:100px; height:30px;" id="manEmail1" name="managerEmail1" value="<% if(email == null){%>""<%} else if(email.equals("@")){%>""<%}else{ %><%=managerEmail[0]%><%} %>">@</td>
							<td width="100px" height="50px"><input type="text" style="width:100px; height:30px;" id="manEmail2" name="managerEmail2" id="inputEmail" value="<% if(email == null){%>""<%} else if(email.equals("@")){%>""<%}else{ %><%=managerEmail[1]%><%} %>"></td>
							<td width="110px" height="50px"><select name="manegerEmail2"style="width:110px; height:30px;" id="selectEmail">
								<option value="">직접입력</option>
								<option value="naver.com">naver.com</option>
								<option value="gmail.com">gmail.com</option>
								<option value="daum.net">hanmail.net</option>
							</select></td>
							<td width="120px" height="50px"><input type="checkbox">비공개</td>
						
						</tr>
	    	 			<tr>
	    	 				<td width="150px" height="50px">대표 전화번호<label style="color:red;">*</label></td>
							<td width="120px" height="50px"><select id="companyPhone1" name="companyPhone1"style="width:80px; height:30px;">
								<option value="010">010</option>
								<option value="011">011</option>
								<option value="016">016</option>
								<option value="017">017</option>
								<option value="019">019</option>
								<option value="02">02</option>
								<option value="031">031</option>
								<option value="032">032</option>
								<option value="033">033</option>
								<option value="041">041</option>
								<option value="042">042</option>
								<option value="043">043</option>
								<option value="044">044</option>
								<option value="051">051</option>
								<option value="052">052</option>
								<option value="053">053</option>
								<option value="054">054</option>
								<option value="055">055</option>
								<option value="061">061</option>
								<option value="062">062</option>
								<option value="063">063</option>
								<option value="064">064</option>
							</select> -</td>
							<td width="100px" height="50px"><input type="text" style="width:80px; height:30px;" id="comPhone2" name="companyPhone2" value="<%=companyPhone[1]%>"> -</td>
							<td width="100px" height="50px"><input type="text" style="width:80px; height:30px;" id="comPhone3" name="companyPhone3" value="<%=companyPhone[2]%>"></td>
							<td width="100px" height="50px"><input type="checkbox">비공개</td>
							<td width="120px" height="50px"> 번호<label style="color:red;">*</label></td>
							<td width="100px" height="50px"><select id="managerPhone1" name="managerPhone1" style="width:80px; height:30px;" >
								<option value="010">010</option>
								<option value="011">011</option>
								<option value="016">016</option>
								<option value="017">017</option>
								<option value="019">019</option>
							</select> -</td>
							<td width="100px" height="50px"><input type="text" style="width:80px; height:30px;" id="manPhone2" name="managerPhone2" value="<%=managerPhone2%>"> -</td>
							<td width="100px" height="50px"><input type="text" style="width:80px; height:30px;" id="manPhone3" name="managerPhone3" value="<%=managerPhone3%>"></td>
							<td width="100px" height="50px"><input type="checkbox">비공개</td>
						</tr>
						<tr>
	    	 				<td width="150px" height="50px">팩스번호</td>
							<td width="120px" height="50px"><select id="fax1" name="fax1"style="width:80px; height:30px;">
								<option value="02" >02</option>
								<option value="031">031</option>
								<option value="032">032</option>
								<option value="033">033</option>
								<option value="041">041</option>
								<option value="042">042</option>
								<option value="043">043</option>
								<option value="044">044</option>
								<option value="051">051</option>
								<option value="052">052</option>
								<option value="053">053</option>
								<option value="054">054</option>
								<option value="055">055</option>
								<option value="061">061</option>
								<option value="062">062</option>
								<option value="063">063</option>
								<option value="064">064</option>
							</select> -</td>
							<td width="100px" height="50px"><input type="text" style="width:80px; height:30px;" name="fax2" > -</td>
							<td width="100px" height="50px"><input type="text" style="width:80px; height:30px;" name="fax3" ></td>
							<td width="100px" height="50px"><input type="checkbox"  >비공개</td>
							<td colspan="6"width="120px" height="50px"></td>
							
						</tr>
						
    	 			</table>
	    	 <br>
	    	 <br>
    	 	</div>
    	 	<!-- 모집내용 -->
    	 	<h2 class = "b">모집내용<label style="color:red; font-size:12px; text-align:right;">&nbsp;&nbsp;&nbsp;* 필수입력</label></h2>
				<hr style="border:solid 1.5px #013252">
				<br>
    	 	<div>
<!-- ================================= 모집 직종 선택 모달  ============ -->
	   	<div id="jdj_modal6">
		<div class="jyh_modal_content6 Request">
				<table class="TB" >
            		<tr style="border:0;">
            			<td width="400px" height="60px" style="text-align:left;"><h1>모집직종 선택</h1></td>
            			<td width="800px"  style="text-align:right; vertical-align: bottom;">*복리후생 선택 후 적용 버튼을 꼭 눌러주세요.</td>
            		
            		</tr>
            		<tr style="border:0;">
            		 	<td colspan="2" height="10px"><hr style="border:solid 1.5px #013252; "></td>
            		 </tr>
            	</table>
            	<div class="iqWrap">
			<!-- 직종선택 영역 -->			
			<!-- <p style="margin-left: 40px"><b>직종선택</b></p> -->
			<div id="kindsJob" style="height: 30px">
				<div class="kindsJob">
					<div class="kindsJobTop">전체</div>
					<div class="kindsJobTop" id="cooking">요리/서빙</div>
					<div class="kindsJobTop" id="medical">간호/의료</div>
					<div class="kindsJobTop" id="construct">생산/건설</div>
					<div class="kindsJobTop" id="office">사무/경리</div>
					<div class="kindsJobTop" id="driving">운전/배달</div>
					<div class="kindsJobTop" id="sales">상담/영업</div>
					<div class="kindsJobTop" id="storeManger">매장관리</div>
					<div class="kindsJobTop" id="teacher">교사/강사</div>
					<div class="kindsJobTop" id="etc">일반/기타</div>
				</div>
			</div>
			<div id="cookingDiv" class="jKinds" style="height: 80px">
				<div class="cooking">
					<div class="cooking2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="전체">전체</div>
					<div class="cooking2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="주방장/조리사">주방장/조리사</div>
					<div class="cooking2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="주방/주방보조">주방/주방보조</div>
					<div class="cooking2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="찬모/밥모">찬모/밥모</div>
					<div class="cooking2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="설거지">설거지</div>
					<div class="cooking2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="서빙">서빙</div>
					<div class="cooking2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="카운터">카운터</div>
					<div class="cooking2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="점장/매니저">점장/매니저</div>
					<div class="cooking2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="기타">기타</div>
				</div>                    
			</div>
			<div id="medicalDiv" class="jKinds" style="height: 40px">
				<div class="medical">
					<div class="medical2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="전체">전체</div>
					<div class="medical2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="간호/간호조무사">간호/간호조무사</div>
					<div class="medical2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="간병/요양보호사">간병/요양보호사</div>
					<div class="medical2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="의료기사">의료기사</div>
					<div class="medical2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="기타의료직">기타의료직</div>
				</div>                    
			</div>
			<div id="constructDiv" class="jKinds" style="height: 80px">
				<div class="construct">
					<div class="construct2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="전체">전체</div>
					<div class="construct2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="제조/조림">제조/조립</div>
					<div class="construct2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="미싱/재단/섬유">미싱/재단/섬유</div>
					<div class="construct2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="노무현장/조선소">노무현장/조선소</div>
					<div class="construct2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="건설/공사/보수">건설/공사/보수</div>
					<div class="construct2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="전기/시설관리">전기/시설관리</div>
					<div class="construct2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="배관/용접/취부">배관/용접/취부</div>
					<div class="construct2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="선반/밀링/머시닝">선반/밀링/머시닝</div>
					<div class="construct2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="식품제조/가공">식품제조/가공</div>
					<div class="construct2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="자동차정비/튜닝">자동차정비/튜닝</div>
					<div class="construct2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="설치/수리">설치/수리</div>
					<div class="construct2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="생산/포장/검사">생산/포장/검사</div>
					<div class="construct2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="가구/목공/주방">가구/목공/주방</div>
					<div class="construct2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="금형/프레스/성형">금형/프레스/성형</div>
					<div class="construct2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="기타">기타</div>
				</div>                                
			</div>
			<div id="officeDiv" class="jKinds" style="height: 40px">
				<div class="office">
					<div class="office2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="전체">전체</div>
					<div class="office2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="경리/회계/인사">경리/회계/인사</div>
					<div class="office2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="일반사무/내근직">일반사무/내근직</div>
					<div class="office2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="기획/총무/관리">기획/총무/관리</div>
					<div class="office2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="구매/자재/물류">구매/자재/물류</div>
					<div class="office2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="비서/안내">비서/안내</div>
					<div class="office2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="기타">기타</div>
				</div>                             
			</div>
			<div id="drivingDiv" class="jKinds" style="height: 40px">
				<div class="driving">
					<div class="driving2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="전체">전체</div>
					<div class="driving2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="이사/택배/퀵배송">이사/택배/퀵배송</div>
					<div class="driving2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="대리/승용차/일반">대리/승용차/일반</div>
					<div class="driving2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="버스/택시/승합차">버스/택시/승합차</div>
					<div class="driving2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="지입/차량용역">지입/차량용역</div>
					<div class="driving2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="화물/중장비/특수차">화물/중장비/특수차</div>
					<div class="driving2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="음식점/식음료배달">음식점/식음료배달</div>
					<div class="driving2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="기타">기타</div>
				</div>
			</div>
			<div id="salesDiv" class="jKinds" style="height: 80px">
				<div class="sales">
					<div class="sales2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="전체">전체</div>
					<div class="sales2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="인바운드/CS">인바운드/CS</div>
					<div class="sales2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="아웃바운드">아웃바운드/TM</div>
					<div class="sales2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="일반/기술영업">일반/기술영업</div>
					<div class="sales2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="보험/금융상담">보험/금융상담</div>
					<div class="sales2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="방문판매">방문판매</div>
					<div class="sales2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="부동산상담">부동산상담</div>
					<div class="sales2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="홍보/마케팅">홍보/마케팅</div>
					<div class="sales2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="기타">기타</div>
				</div>                            
			</div>
			<div id="storeMangerDiv" class="jKinds" style="height: 80px">
				<div class="storeManger">
					<div class="storeManger2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="전체">전체</div>
					<div class="storeManger2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="슈퍼/마트">슈펴/마트</div>
					<div class="storeManger2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="편의점">편의점</div>
					<div class="storeManger2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="대형마트">대형마트</div>
					<div class="storeManger2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="쇼핑몰/아울렛">쇼핑몰/아울렛</div>
					<div class="storeManger2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="백화점">백화점</div>
					<div class="storeManger2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="찜질방/사우나">찜질방/사우나</div>
					<div class="storeManger2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="농수산/청과/축산">농수산/청과/축산</div>
					<div class="storeManger2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="의류/잡화">의류/잡화</div>
					<div class="storeManger2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="가전/휴대폰">가전/휴대폰</div>
					<div class="storeManger2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="물류/재고">물류/재고</div>
					<div class="storeManger2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="PC방/오락실">PC방/오락실</div>
					<div class="storeManger2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="노래방/볼링장">노래방/볼링장</div>
					<div class="storeManger2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="기타">기타</div>
				</div>                                  
			</div>                          
			<div id="teacherDiv" class="jKinds" style="height: 40px">
				<div class="teacher">
					<div class="teacher2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="전체">전체</div>
					<div class="teacher2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="어린이집/유치원">어린이집/유치원</div>
					<div class="teacher2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="방문/학습지">방문/학습지</div>
					<div class="teacher2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="입시/보습/과외">입시/보습/과외</div>
					<div class="teacher2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="외국어">외국어</div>
					<div class="teacher2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="예체능/컴퓨터">예체능/컴퓨터</div>
					<div class="teacher2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="학원관리">학원관리</div>
					<div class="teacher2 JobChoice"><label><input type="radio" name="jobkind" style="visibility:hidden;"value="기타교사">기타교사</label></div>
 				</div>
			</div>
			<div id="etcDiv" class="jKinds" style="height: 80px">
				<div class="etc">
					<div class="etc2 JobChoice"><label><input type="radio" id="test" name="jobkind" style="visibility:hidden;"value="전체">전체</label></div>
					<div class="etc2 JobChoice"><label><input type="radio" id="test" name="jobkind" style="visibility:hidden;"value="미화/청소/세탁">미화/청소/세탁</label></div>
					<div class="etc2 JobChoice"><label><input type="radio" id="test" name="jobkind" style="visibility:hidden;"value="생활도우미/파출부">생활도우미/파출부</label></div>
					<div class="etc2 JobChoice"><label><input type="radio" id="test" name="jobkind" style="visibility:hidden;"value="경비/보안">경비/보안</label></div>
					<div class="etc2 JobChoice"><label><input type="radio" id="test" name="jobkind" style="visibility:hidden;"value="주유/세차/광택">주유/세차/광택</label></div>
					<div class="etc2 JobChoice"><label><input type="radio" id="test" name="jobkind" style="visibility:hidden;"value="헤어/피부/미용">헤어/피부/미용</label></div>
					<div class="etc2 JobChoice"><label><input type="radio" id="test" name="jobkind" style="visibility:hidden;"value="골프장/골프연습장">골프장/골프연습장</label></div>
					<div class="etc2 JobChoice" style="font-size:11px"><label><input type="radio" id="test" name="jobkind"style="visibility:hidden;"value="웨딩/이벤트/스튜디오">웨딩/이벤트/스튜디오</label></div>
					<div class="etc2 JobChoice"><label><input type="radio" id="test" name="jobkind" style="visibility:hidden;"value="상조/장례식장">상조/장례서비스</label></div>
					<div class="etc2 JobChoice"><label><input type="radio" id="test" name="jobkind" style="visibility:hidden;"value="컴퓨터/IT/디자인">컴퓨터/IT/디자인</label></div>
					<div class="etc2 JobChoice"><label><input type="radio" id="test" name="jobkind" style="visibility:hidden;"value="호텔/모텔/숙박">호텔/모델/숙박</label></div>
					<div class="etc2 JobChoice"><label><input type="radio" id="test" name="jobkind" style="visibility:hidden;" value="주차관리/대리주차">주차관리/대리주차</label></div>
					<div class="etc2 JobChoice"><label><input type="radio" id="test" name="jobkind" style="visibility:hidden;" value="기타서비스">기타서비스</label></div>
 				</div>
			</div><br>
		</div><br>
		<!-- 직종선택 END -->
  			<table>
		   		<tr style="border:0;">
		   			<td width="1080px"></td>
		   			<td width="120px">
		   			<button type="button" id="jdj_modal_close6"  style="width:50px; height:30px; background:gray; color:white;">취소</button> 
		   			<button type="button" id="kindsJob_close"  style="width:50px; height:30px; background:#013252; color:white; float:right;">적용</button></td>
		   		</tr>
		   	</table>
		</div>
	   
		<div class="jyh_modal_layer6"></div>
	</div><!--  모달 끝  -->
	    	 	<table>
	    	 		<tr>
	    	 			<td width="200px" height="50px">제목<label style="color:red;">*</label></td>
	    	 			<td width="800px" height="50px"><input type="text" name ="recruitTitle" value="<%=recruit.getRecruitTitle() %>" id="content" class="recTitle" name="recruitTitle"maxlength="35" style="width:750px; height:30px;"></td>
    					<td><span id="counter">###</span></td>
	    	 			<script type="text/javascript">
	    	 			$(function() {
	    	 			      $('#content').keyup(function (e){
	    	 			          var content = $(this).val();
	    	 			          $(this).height(((content.split('\n').length + 1) * 1.13) + 'em');
	    	 			          $('#counter').html('('+content.length + '/35자)');
	    	 			      });
	    	 			      $('#content').keyup();
	    	 			});
	    	 			</script>
	    	 		</tr>
	    	 		<tr>
	    	 			<td width="200px" height="100px"></td>
	    	 			<td width="800px" height="100px" id="align_left">
	    	 			<h3>반드시 시켜주세요!</h3>
	    	 			<p>채용공고에 특정 성별이나 연령을 표기하면 벌금이 부과될 수 있습니다. <a style="color:red;" href="#">[관련법률 자세히 보기]</a><br>
							허위/과대표현 - 최고대우, 고수입, 선불 등 - 공고는 5년 이하의 징역 또는 5천만원 이하의 벌금이 부과될 수 있으며,
							별도 통보없이 내용 수정 및 삭제될 수 있습니다. <a href="#"style="color:red;">[관련법률 자세히 보기]</a></p>
	    	 			</td>
	    	 			<td width="200px" height="100px"></td>
	    	 		</tr>
	    	 		<tr>
	    	 			<td width="200px" height="50px">모집직종<label style="color:red;">*</label></td>
	    	 			<td width="800px" height="50px"><input id="kindsJobDetail" type="text" style="width:750px; height:30px;" name="recruitMajor"value="<%= recruit.getRecruitMajor() %>"></td>
	    	 			<td width="200px" height="50px"><button type="button" id="jdj_modal_open6">모집직종 선택</button></td>
	    	 		</tr>
    	 	</table>
    	 	 
    	 	 <table>
    	 	 	<tr>
    	 	 		<td width="150px">근무유형<label style="color:red;">*</label></td>
    	 	 		<td width="150px" height="50px"><input type="checkbox" id="jobType1" name="jobType" value="정규직"<%if("정규직".equals(jobType)){%>checked<%}%>>정규직(정직원)</td>
    	 	 		<td width="150px" height="50px"><input type="checkbox" id="jobType2" name="jobType" value="계약직"<%if("계약직".equals(jobType)){%>checked<%}%>>계약직</td>
    	 	 		<td width="150px" height="50px"><input type="checkbox" id="jobType3" name="jobType" value="파견직"<%if("파견직".equals(jobType)){%>checked<%}%>>파견직</td>
    	 	 		<td width="200px" height="50px"><input type="checkbox" id="jobType4" name="jobType" value="아르바이트"<%if("아르바이트".equals(jobType)){%>checked<%}%>>아르바이트(파트타임)</td>
    	 	 		
    	 	 	</tr>
    	 	 </table>
    	 	  
    	 	 <table>
    	 	 	<tr>
    	 	 		<td width="150px">근무요일<label style="color:red;">*</label></td>
    	 	 		<td width="100px" height="50px"><input type="radio" id="1" name="jobDate" value="월~금"<%if("월~금".equals(jobDate)){%>checked<%}%>>월~금</td>
    	 	 		<td width="100px" height="50px"><input type="radio" id="2" name="jobDate" value="월~토"<%if("월~토".equals(jobDate)){%>checked<%}%>>월~토</td>
    	 	 		<td width="100px" height="50px"><input type="radio" id="3" name="jobDate" value="주말(토,일)"<%if("주말(토,일)".equals(jobDate)){%>checked<%}%>>주말(토,일)</td>
    	 	 		<td width="80px" height="50px"><input type="radio" id="4" name="jobDate" value="주6일"<%if("주6일".equals(jobDate)){%>checked<%}%>>주6일</td>
    	 	 		<td width="80px" height="50px"><input type="radio" id="5" name="jobDate" value="주5일"<%if("주5일".equals(jobDate)){%>checked<%}%>>주5일</td>
    	 	 		<td width="80px" height="50px"><input type="radio" id="6" name="jobDate" value="주4일"<%if("주4일".equals(jobDate)){%>checked<%}%>>주4일</td>
    	 	 		<td width="80px" height="50px"><input type="radio" id="7" name="jobDate" value="주3일"<%if("주3일".equals(jobDate)){%>checked<%}%>>주3일</td>
    	 	 		<td width="80px" height="50px"><input type="radio" id="8" name="jobDate" value="주2일"<%if("주2일".equals(jobDate)){%>checked<%}%>>주2일</td>
    	 	 		<td width="100px" height="50px"><input type="radio" id="9" name="jobDate" value="요일협의"<%if("요일협의".equals(jobDate)){%>checked<%}%>>요일협의</td>
    	 	 		<td width="100px" height="50px"><input type="radio" id="10" name="jobDate" value="선택안함"<%if("선택안함".equals(jobDate)){%>checked<%}%>>선택안함</td>
    	 	 	</tr>
    	 	 </table>
    	 	 <table>
    	 	 	<tr>
    	 	 		<td width="150px">근무시간<label style="color:red;">*</label></td>
    	 	 		<td width="100px" height="50px"><select id="jobTime1" name="jobTime1"style="width:100px; height:30px;">
								<option value="">시간선택</option>
								<option value="00">00</option>
								<option value="01">01</option>
								<option value="02">02</option>
								<option value="03">03</option>
								<option value="04">04</option>
								<option value="05">05</option>
								<option value="06">06</option>
								<option value="07">07</option>
								<option value="08">08</option>
								<option value="09">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
							</select></td>
						<td width="120px" height="50px"><select id="jobTime2" name="jobTime2"style="width:100px; height:30px;">
								<option value="">분선택</option>
								<option value="00">00</option>
								<option value="10">10</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option value="40">40</option>
								<option value="50">50</option>
							</select> ~ </td>
						<td width="100px" height="50px"><select id="jobTime3" name="jobTime3"style="width:100px; height:30px;">
								<option value="">시간선택</option>
								<option value="00">00</option>
								<option value="01">01</option>
								<option value="02">02</option>
								<option value="03">03</option>
								<option value="04">04</option>
								<option value="05">05</option>
								<option value="06">06</option>
								<option value="07">07</option>
								<option value="08">08</option>
								<option value="09">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="23">24</option>
							</select></td>
						<td width="100px" height="50px"><select id="jobTime4"name="jobTime4"style="width:100px; height:30px;">
								<option value="">분선택</option>
								<option value="00">00</option>
								<option value="10">10</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option value="40">40</option>
								<option value="50">50</option>
							</select></td>
						<td>
    	 	 		<td width="100px" height="50px"><input type="checkbox" id="anytime" name="jobTimeNego" value="협의"<%if("협의".equals(jobTimeNego)){%>checked<%}%>>협의</td>
    	 	 		
    	 	 	</tr>
    	 	 	
    	 	 </table>
    	 	 <table>
    	 	  	<tr>
    	 	 		<td width="150px" height="30px"></td>
    	 	 		<td><button type="button" style="width:150px; height:30px;" id="time1">정규(09:00~18:00)</button></td>
    	 	 		<td><button type="button"style="width:150px; height:30px;" id="time2">오전(06:00~12:00)</button></td>
    	 	 		<td><button type="button" style="width:150px; height:30px;" id="time3">오후(12:00~18:00)</button></td>
    	 	 		<td><button type="button" style="width:150px; height:30px;" id="time4">저녁(18:00~24:00)</button></td>
    	 	 		<td><button type="button" style="width:150px; height:30px;" id="time5">새벽(00:00~06:00)</button></td>
    	 	 	</tr>
    	 	 </table>
    	 	 <table>
    	 	 	<tr>
    	 	 		<td width="150px">급여<label style="color:red;">*</label></td>
    	 	 		<td width="100px" height="50px"><input type="radio" id="year" class="salaryType" name="salaryType" value="연봉"<%if("연봉".equals(salaryType)){%>checked<%}%>>연봉</td>
    	 	 		<td width="100px" height="50px"><input type="radio" id="month" class="salaryType" name="salaryType" value="월급"<%if("월급".equals(salaryType)){%>checked<%}%>>월급</td>
    	 	 		<td width="100px" height="50px"><input type="radio" id="day" class="salaryType" name="salaryType" value="일급"<%if("일급".equals(salaryType)){%>checked<%}%>>일급</td>
    	 	 		<td width="100px" height="50px"><input type="radio" id="time" class="salaryType" name="salaryType" value="시급"<%if("시급".equals(salaryType)){%>checked<%}%>>시급</td>
    	 	 		<td width="100px" height="50px"><input type="checkbox" id="anytime" name="salaryNego" value="협의"<%if("협의".equals(salaryNego)){%>checked<%}%>>협의</td>
    	 	 	</tr>
    	 	 </table>
    	 	 <table>
    	 	 	<tr>
    	 	 		<td width="150px"></td>
    	 	 		<td width="300px" height="30px">급여 : 
    	 	 		<input type="text" onKeyup="inputNumberAutoComma(this);" style="text-align:right;width:200px; height:30px;" id="salary" name="salary" value="<%=recruit.getSalary() %>"/>원 </td>
    	 	 		
    	 	 		<td width="500px" height="30px">최저임금 : 시급 8590원/월급(209시간) 1,795,310원 <a href="#" style="color:red;">[자세히보기]</a></td>
       	 	 	</tr>
    	 	 </table>
    	 	  <script type="text/javascript">
    	 	function inputNumberAutoComma(obj) {
    	        
    	        // 콤마( , )의 경우도 문자로 인식되기때문에 콤마를 따로 제거한다.
    	        var deleteComma = obj.value.replace(/\,/g, "");

    	        // 콤마( , )를 제외하고 문자가 입력되었는지를 확인한다.
    	        if(isFinite(deleteComma) == false) {
    	            alert("문자는 입력하실 수 없습니다.");
    	            obj.value = "";
    	            return false;
    	        }
    	       
    	        // 기존에 들어가있던 콤마( , )를 제거한 이 후의 입력값에 다시 콤마( , )를 삽입한다.
    	        obj.value = inputNumberWithComma(inputNumberRemoveComma(obj.value));
    	    }
    	   
    	    // 천단위 이상의 숫자에 콤마( , )를 삽입하는 함수
    	    function inputNumberWithComma(str) {

    	        str = String(str);
    	        return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, "$1,");
    	    }

    	    // 콤마( , )가 들어간 값에 콤마를 제거하는 함수
    	    function inputNumberRemoveComma(str) {

    	        str = String(str);
    	        return str.replace(/[^\d]+/g, "");
    	    }
    	 	 </script>
    	 	 <!-- <table>
    	 	 	<tr>
    	 	 		<td width="150px"></td>
    	 	 		<td width="100px" height="50px"><input type="checkbox" id="today" name="payment" value="today">당일지급</td>
    	 	 		<td width="200px" height="50px"><input type="checkbox" id="meal" name="payment" value="meal">식대별도지급/식사제공</td>
    	 	 		<td width="100px" height="50px"><input type="checkbox" id="bus" name="payment" value="bus">교통비지급</td>
    	 	 	</tr>
    	 	 </table>  지우기로 한부분-->
    	 	<table>
    	 		<tr>
    	 			<td width="150px">근무지역<label style="color:red;">*</label></td>
    	 			<td width="400px" height="30px"> 
    	 				 주소 : <input type="text" id="address" style="width:340px; height:30px;" name="Area1" value="<%=address[0]%>"></td>
    	 			<td width="400px">
						 상세주소 : <input type="text" id="address_etc"  style="width:300px; height:30px;" name="Area2" value="<%=address[1]%>"> &nbsp;</td>
					<td  width="100px">
						<button type="button" onclick="openDaumZipAddress();" style="width:100px; height:30px;">주소찾기</button>
    	 			</td>
    	 		</tr>
    	 	</table>
	    	 <br>
	    	 <br>
    	 	</div>
    	 	<h2 class = "b">자격조건<label style="color:red; font-size:12px; text-align:right;">&nbsp;&nbsp;&nbsp;* 필수입력</label></h2>
				<hr style="border:solid 1.5px #013252">
				<br>
				<div>

	    	 	<table>
	    	 		<tr>
	    	 			<td width="150px" height="50px">성별</td>
	    	 			<td width="100px" height="50px"><input type="radio" id="mf" name="interestGender" value="무관"<%if("무관".equals(interestGender)){%>checked<%}%>>무관</td>
    	 	 			<td width="120px" height="50px"><input type="radio" id="m" name="interestGender" value="남자"<%if("남자".equals(interestGender)){%>checked<%}%>>남자</td>
    	 	 			<td width="100px" height="50px"><input type="radio" id="f" name="interestGender" value="여자"<%if("여자".equals(interestGender)){%>checked<%}%>>여자</td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="130px" height="50px"></td>
	    	 			
	    	 		</tr>
	    	 		<tr>
	    	 			<td width="150px" height="50px"></td>
	    	 			<td colspan="11" width="1100px" height="50px"id="align_left">남녀고용평등법에 의거 입력하신 성별정보는 공고 상세 페이지에 노출되지 않으며, 성별을 포함한 상세검색 조건에만 반영 됩니다.<br>
						<a href="#" style="color:red;">[남녀고용평등법 안내]</a></td>
	    	 		</tr>
	    	 		<tr>
	    	 			<td width="150px" height="50px">연령</td>
	    	 			<td width="100px" height="50px"><input type="radio" id="interestAgeN" name="interestAgeYn" value="무관"<%if("무관".equals(interestAgeYn)){%>checked<%}%>>무관</td>
    	 	 			<td width="100px" height="50px"><input type="radio" id="interestAgeY" name="interestAgeYn" value="선택"<%if("선택".equals(interestAgeYn)){%>checked<%}%>>선택</td>
    	 	 			<td width="100px" height="50px"><select id="ageSelect1" name="interestAge1"style="width:100px; height:30px;">
								<option value="">선택</option>
								<option value="15">15</option>
								<option value="20">20</option>
								<option value="25">25</option>
								<option value="30">30</option>
								<option value="35">35</option>
								<option value="40">40</option>
								<option value="45">45</option>
								<option value="50">50</option>
								<option value="55">55</option>
								<option value="60">60</option>
								<option value="65">65</option>
								<option value="70">70</option>
								<option value="75">75</option>
								<option value="80">80</option>
								<option value="85">85</option>
							</select></td>
							<td width="80px" height="50px" style="text-align:left;">&nbsp; 이상 &nbsp;&nbsp;&nbsp;~ </td>
	    	 			<td width="100px" height="50px"><select id="ageSelect2"name="interestAge2"style="width:100px; height:30px;">
								<option value="" selectied="selectied">선택</option>
								<option value="20">20</option>
								<option value="25">25</option>
								<option value="30">30</option>
								<option value="35">35</option>
								<option value="40">40</option>
								<option value="45">45</option>
								<option value="50">50</option>
								<option value="55">55</option>
								<option value="60">60</option>
								<option value="65">65</option>
								<option value="70">70</option>
								<option value="75">75</option>
								<option value="80">80</option>
								<option value="85">85</option>
								<option value="90">80</option>
							</select></td>
	    	 			
	    	 			<td width="100px" height="50px" style="text-align:left;">&nbsp;이하</td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="150px" height="50px"></td>
	    	 			
	    	 		</tr>
	    	 		<tr>
	    	 			<td width="150px" height="50px"></td>
	    	 			<td colspan="11" width="1100px" height="50px"id="align_left">연령차별금지법에 의거 입력하신 연령정보는 공고 상세 페이지에 노출되지 않으며, 연령을 포함한 상세검색 조건에만 반영됩니다.<br>
						<a href="#" style="color:red;">[연령차별금지법 안내]  | </a>
						<a href="#" style="color:red;"> [만 18세 미만 청소년 고용금지 직종 안내 ]</a></td>
	    	 		</tr>
	    	 		<tr>
	    	 			<td width="150px" height="50px">학력<label style="color:red;">*</label></td>
	    	 			<td width="100px" height="50px"><input type="radio" id="eduN" class="edu" name="eduYn" value="무관"<%if("무관".equals(eduYn)){%>checked<%}%>>무관</td>
	    	 			<td width="100px" height="50px"><input type="radio" id="eduY" class="edu" name="eduYn" value="선택"<%if("선택".equals(eduYn)){%>checked<%}%>>선택</td>
    	 	 			<td width="100px" height="50px"><select id="education" name="education"style="width:100px; height:30px;">
								<option value="">학력선택</option>
								<option value="초졸">초졸</option>
								<option value="중졸">중졸</option>
								<option value="고졸">고졸</option>
								<option value="대졸">대졸</option>
								<option value="대학원졸">대학원졸</option>
							</select></td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="150px" height="50px"></td>
	    	 			
	    	 		</tr>
	    	 		<tr>
	    	 			<td width="150px" height="50px">경력<label style="color:red;">*</label></td>
	    	 			<td width="100px" height="50px"><input type="radio" id="careerN" class="careerChoice" name="careerChoice" value="무관"<%if("무관".equals(careerChoice)){%>checked<%}%>>무관</td>
	    	 			<td width="100px" height="50px"><input type="radio" id="careerY" class="careerChoice" name="careerChoice" value="신입"<%if("신입".equals(careerChoice)){%>checked<%}%>>신입</td>
	    	 			<td width="100px" height="50px"><input type="radio" id="careerYY" class="careerChoice" name="careerChoice" value="경력"<%if("경력".equals(careerChoice)){%>checked<%}%>>경력</td>
    	 	 			<td width="100px" height="50px"><select id="careerPeriod" name="careerPeriod"style="width:100px; height:30px;">
								<option value="">선택</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="5">5</option>
								<option value="7">7</option>
								<option value="10">10</option>
								<option value="15">15</option>
							</select></td>
	    	 			<td width="100px" height="50px"  style="text-align:left;">&nbsp; 이상</td>
	    	 			<!-- <td width="100px" height="50px"><select name="exp2"style="width:100px; height:30px;">
								<option value="exp0" selectied="selectied">선택</option>
								<option value="초졸">초졸</option>
								<option value="중졸">중졸</option>
								<option value="고졸">고졸</option>
								<option value="대졸">대졸</option>
								<option value="대학원졸">대학원졸</option>
							</select></td>
	    	 			<td width="100px" height="50px">이하</td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="100px" height="50px"></td>
	    	 			<td width="150px" height="50px"></td> 지우기로 한부분-->
	    	 			
	    	 		</tr>
	    	 		<% 		String recruitPeople = recruit.getRecruitPeople();
	    		 			String CareerChoice = recruit.getCareerChoice();
							String EduYn = recruit.getEduYn();
							String preference = recruit.getPreference();
	    		 			String welfare = recruit.getWelfare(); 
	    		 			String detailText = recruit.getDetailText();
	    		 		%>
	    	 		<tr>
	    	 			<td width="150px" height="50px">모집인원</td>
	    	 			<td colspan ="2" width="200px" height="50px"><input type="text" style="width:120px; height:30px;" name="recruitPeople" id="recruitPeople" value="<% if(recruitPeople == null){%>""<%} else if(recruitPeople == ""){%>""<%}else{ %><%=recruitPeople%><%} %>"> 명 </td>
	    	 			<td width="100px" height="50px"><button type="button" style="width:50px; height:30px;" id="people0" > 0명 </button></td>
	    	 			<td width="100px" height="50px"><button type="button" style="width:50px; height:30px;" id="people00"> 00명 </button></td>
	    	 		
	    	 		</tr>
	    	 		
	    	 		<tr>
	    	 			<td width="150px" height="50px">우대·가능</td>
	    	 			<td colspan ="5" width="500px" height="50px"><input type="text" style="width:450px; height:30px;" name="preference" id="preferenceDetail" value="<% if(preference == null){%>""<%} else if(preference == ""){%>""<%}else{ %><%=preference%><%} %>" readonly></td>
	    	 			<td width="100px" height="50px"><button style="width:80px; height:30px;" type="button" id="jdj_modal_open2"> 조건 선택 </button></td>
	    	 			
	    	 		
	    	 		</tr>
	    	 		<tr>
	    	 			<td width="150px" height="50px">복리후생</td>
	    	 			<td colspan ="5" width="500px" height="50px"><input type="text" style="width:450px; height:30px;" name="welfare" id="welfareDetail" value="<% if(welfare == null){%>""<%} else if(welfare == ""){%>""<%}else{ %><%=welfare%><%} %>" readonly></td>
	    	 			<td width="100px" height="50px"><button style="width:80px; height:30px;" type="button" id="jdj_modal_open"> 조건선택 </button></td>
	    	 			
	    	 		
	    	 		</tr>
	    	 		
    	 	</table>
    	 	 <!-- ================================= 복리후생 모달  ============ -->
	   	<div id="jdj_modal">
		<div class="jyh_modal_content">
		   	<table style="text-align: left;">
		   	    <tr> 
		   	   		<td><h1> 복리후생</h1></td>
		   	    	<td width="150px" height="60px" colspan="3" width="570px" style="text-align:right; vertical-align: bottom;">*복리후생 선택 후 적용 버튼을 꼭 눌러주세요</td>
		   	    </tr>
		   	    <tr>
		   			<td colspan="4" height="12px"><hr style="border:solid 1.5px #013252"></td>
		   			
		   		</tr>
		   		<tr>
		   			<td width="150px" height="30px" ><input type="checkbox" name="welfare" value="국민연금"> 국민연금</td>
		   			<td width="150px"><input type="checkbox" name="welfare" value="건강보험"> 건강보험</td>
		   			<td width="150px"><input type="checkbox" name="welfare" value="고용보험"> 고용보험</td>
		   			<td width="120px"><input type="checkbox" name="welfare" value="산재보험"> 산재보험</td>
		   		</tr>
		   		<tr>
		   			<td width="150px" height="30px" ><input type="checkbox" name="welfare"  value="퇴직금"> 퇴직금</td>
		   			<td width="150px"><input type="checkbox" name="welfare" value="퇴직연금"> 퇴직연금</td>
		   			<td width="150px"><input type="checkbox" name="welfare" value="경조금"> 경조금</td>
		   			<td width="120px"><input type="checkbox" name="welfare" value="야근수당"> 야근수당</td>
		   		</tr>
		   		<tr>
		   			<td width="150px" height="30px" ><input type="checkbox" name="welfare"  value="휴일수당" > 휴일수당</td>
		   			<td width="150px"><input type="checkbox" name="welfare" value="위험수당"> 위험수당</td>
		   			<td width="150px"><input type="checkbox" name="welfare" value="정기보너스"> 정기보너스</td>
		   			<td width="120px"><input type="checkbox" name="welfare" value="인센티브제"> 인센티브제</td>
		   		</tr>
		   		<tr>
		   			<td width="150px" height="30px" ><input type="checkbox" name="welfare"  value="연/월차 수당"> 연/월차 수당</td>
		   			<td width="150px"><input type="checkbox" name="welfare" value="연차"> 연차</td>
		   			<td width="150px"><input type="checkbox" name="welfare" value="월차"> 월차</td>
		   			<td width="120px"><input type="checkbox" name="welfare"  value="정기휴가">정기휴가</td>
		   		</tr>
		   		<tr>
		   			<td width="150px" height="30px" ><input type="checkbox" name="welfare"  value="포상휴가"> 포상휴가</td>
		   			<td width="150px"><input type="checkbox" name="welfare" value="주 5일근무"> 주 5일 근무</td>
		   			<td width="150px"><input type="checkbox" name="welfare" value="격주 휴무"> 격주 휴무</td>
		   			<td width="120px"><input type="checkbox" name="welfare" value="기숙사"> 기숙사</td>
		   		</tr>
		   		<tr>
		   			<td width="150px" height="30px" ><input type="checkbox" name="welfare"  value="통근버스 운행"> 통근버스 운행</td>
		   			<td width="150px"><input type="checkbox" name="welfare" value="교통비 지급"> 교통비지급</td>
		   			<td width="150px"><input type="checkbox" name="welfare" value="차량유류보조금"> 차량유류보조금</td>
		   			<td width="120px"><input type="checkbox" name="welfare" value="건강검진"> 건강검진</td>
		   		</tr>
		   		<tr>
		   			<td colspan="4" height="12px"><hr style="border:solid 1.5px #013252"></td>
		   			
		   		</tr>
		   		<tr>
		   			<td width="150px" height="60px" ></td>
		   			<td width="150px"></td>
		   			<td width="150px"></td>
		   			<td width="120px">
		   			<button type="button" id="jdj_modal_close"  style="width:50px; height:30px; background:gray; color:white; float:left;">취소</button>
		   			<button type="button" id="welfareText"  style="width:50px; height:30px; background:#013252; color:white; float:right;" onclick="welfareText()">적용</button></td>
		   		</tr>
		   	</table>
		</div>
	   
		<div class="jyh_modal_layer"></div>
	</div><!-- 모달 div 끝  -->
   <!-- ================================= 우대 모달  ============ -->
	   	<div id="jdj_modal2">
		<div class="jyh_modal_content2">
		   	<table style="text-align: left;">
		   	    <tr>
		   	   		<td><h1> 우대·가능</h1></td>
		   	    	<td width="150px" height="60px" colspan="3" width="570px" style="text-align:right; vertical-align: bottom;">*우대·가능 선택 후 적용 버튼을 꼭 눌러주세요</td>
		   	    </tr>
		   	    <tr>
		   			<td colspan="4" height="12px"><hr style="border:solid 1.5px #013252"></td>
		   			
		   		</tr>
		   		<tr>
		   			<td width="200px" height="30px" ><input type="checkbox" id="preference" name="preference" value="동종업계경력자"> 동종업계경력자</td>
		   			<td width="200px"><input type="checkbox" name="preference" value="원동기면허소지자"> 원동기면허소지자</td>
		   			<td colspan="2" width="170px"><input type="checkbox" name="preference"  value="컴퓨터활용가능자"> 컴퓨터활용가능자</td>
		   		</tr>
		   		<tr>
		   			<td  height="30px" ><input type="checkbox" name="preference" value="운전면허소지자"> 운전면허소지자</td>
		   			<td ><input type="checkbox" name="preference" value="인근거주자"> 인근거주자</td>
		   			<td colspan="2"><input type="checkbox" name="ence"  value="차량소지자"> 차량소지자</td>
		   		</tr>
		   		<tr>
		   			<td  height="30px" ><input type="checkbox" name="ence"  value="영어가능자"> 영어가능자</td>
		   			<td ><input type="checkbox" name="ence"  value="관련자격증 소지자"> 관련자격증소지자</td>
		   			<td colspan="2"><input type="checkbox" name="ence" value="중국어 가능자"> 중국어 가능자</td>
		   		</tr>
		   		<tr>
		   			<td  height="30px" ><input type="checkbox" name="ence"  value="장기근무가능자"> 장기근무가능자</td>
		   			<td ></td>
		   			<td colspan="2"></td>
		   		</tr>
		   		
		   		<tr>
		   			<td colspan="4" height="12px"><hr style="border:solid 1.5px #013252"></td>
		   			
		   		</tr>
		   		<tr>
		   			<td></td>
		   			<td></td>
		   			<td></td>
		   			<td width="120px">
		   			<button type="button" id="jdj_modal_close2"  style="width:50px; height:30px; background:gray; color:white;">취소</button> 
		   			<button type="button" id="preferenceText"  style="width:50px; height:30px; background:#013252; color:white; float:right;" onclick="preferenceText()">적용</button></td>
		   		</tr>
		   	</table>
		</div>
	   
		<div class="jyh_modal_layer2"></div>
	</div><!-- 우대 가능 모달 div 끝  -->
	    	 <br>
	    	 <br>
    	 	</div>
			<h2 class = "b">상세 모집 내용</h2>
				<hr style="border:solid 1.5px #013252">
				<br>
				<div>
	    	 	<table id="totalTable"> 

	    	 		<!-- <tr>
	    	 		
	    	 			<td rowspan ="2" width="400px" height="100px"><h3>상세 모집 자동완성</h3><p>상세공고에 넣으실 항목을 선택해 주세요.</p></td>
	    	 			<!-- <td rowspan ="2" width="400px" height="100px"><h3>상세 모집 자동완성</h3><p>상세공고에 넣으실 항목을 선택해 주세요.</p></td>
	    	 			<td width="150px" height="50px"><input type="checkbox" id="all" name="" value="all">전체선택</td>
    	 	 			<td width="150px" height="50px"><input type="checkbox" id="work" name="" value="work">모집직종</td>
    	 	 			<td width="150px" height="50px"><input type="checkbox" id="workstyle" name="" value="workstyle">근무유형</td>
    	 	 			<td width="150px" height="50px"><input type="checkbox" id="workday" name="" value="workday">근무요일</td>
    	 	 			<td></td>
    	 	 			<td rowspan ="2" width="150px" height="50px"><button type="button" id="total" style="width:80px; height:30px;"> 적용하기 </button></td>
	    	 		</tr>
	    	 		<tr>
	    	 			<td width="150px" height="50px"><input type="checkbox" id="worktime" name="근무시간" value="1">근무시간</td>
    	 	 			<td width="150px" height="50px"><input type="checkbox" id="payment" name="급여" value="payment">급여</td>
    	 	 			<td width="150px" height="50px"><input type="checkbox" id="wellbeing" name="복리후생" value="a">복리후생</td>
    	 	 			<td width="150px" height="50px"><input type="checkbox" id="good" name="우대사항" value="1">우대사항</td>
    	 	 		
	    	 		</tr> -->
	    	 		<tr>
	    	 			<td colspan ="8" ><textarea rows="10" cols="1000" id="textArea" name="detailText" type="textarea" style="width:1090px; height:200px;" style="font-size:20px;" placeholder="추가로 등록하실 내용을 입력해주세요." ><% if(detailText==null){ %>-<% }else{ %><%=detailText%><%} %></textarea></td>
	    	 		</tr>
	    	 		<tr>
	    	 			<td colspan ="8" width="1100px" height="50px" id="align_left"> &nbsp; 공고가 정상적인 분류에 게재되지 않았거나, 등록 제한된 공고 및 허위공고로 확인되는 경우 사전 안내 없이 공고가 삭제되며,
						유료상품 결제금액도 환불되지 않습니다. <a href="#" style="color:red;">[등록제한규정안내]</td>
	    	 			<!-- <td  width="150px" height="50px">
	    	 			<button type="button"style="width:120px; height:40px;"> <a href="DJ_boguem_register_view.jsp" style="color:black;">미리보기</a></button>
	    	 			</td> -->
	    	 		</tr>
	    	 	
	    	 	</table>
	    	 	<br>
	    	 	<br>
	    	 	<br>
	    	 	<button type="button" class="button1" id ="registerComplete" onclick="register_update_complete()">수정완료</button></td>
	    	</div><br><br> 
	    	 	</div>
	    	 	
	    	 	</form>
	    	 	</div>
	    	 	<br>
	    	 	<%@ include file="../guide/footer.jsp" %>
	  

	   <!-- =================================script시작================ -->
	   <script>
	 //페이지 이동
		/* function register_update_complete(){ 
			$("#registerForm").submit();
		}; */
		
		 $("#registerComplete").click(function(){
				//필수부분  변수에 담기
				var companyName = $("#comName").val(); //회사명
				var businessContent = $(".busiContent").val(); //주요사업내용
				var managerName = $("#manName").val(); //담장자명
				var managerEmail2 = $("#manEmail1").val(); //이메일1
				var managerEmail3 = $("#manEmail2").val(); //이메일2
				var selectEmail = $("#selectEmail").val(); //이메일 선택부분
				var companyPhone2 = $("#comPhone2").val();//대표 번호2
				var companyPhone3 = $("#comPhone3").val(); //대표번호3
				var managerPhone2 = $("#manPhone2").val(); //담당자 번호2
				var managerPhone3 = $("#manPhone3").val(); //담당자 번호3
				var recruitTitle = $(".recTitle").val(); //모집 공고명
				var recruitMajor = $("#kindsJobDetail").val(); //모집직종 
				var jobType = $("input[name=jobType]:checked").val(); //근무유형 
				var jobDate = $("input[name=jobDate]:checked").val(); //근무요일
				var jobTime1 = $("#jobTime1").val(); //근무시간1
				var jobTime2 = $("#jobTime2").val(); //근무시간2
				var jobTime3 = $("#jobTime3").val(); //근무시간3
				var jobTime4 = $("#jobTime4").val(); //근무시간4
				var salaryType = $("input[name=salaryType]:checked").val(); //급여유형
				var salary = $("#salary").val(); //급여 액수
				var address = $("#address").val(); //주소
				var addressEtc =$("#address_etc").val(); //주소상세
				var edu = $(".edu[name=eduYn]:checked").val();//학력선택
				var education =$("#education").val();//초졸고졸이런거
				var careerChoice = $(".careerChoice[name=careerChoice]:checked").val();//경력선택
				var careerPeriod =$("#careerPeriod").val();
				
				
				if(companyName === ""){
					alert("회사명을 입력해주세요.");
			    } else if(businessContent ===""){
					alert("주요사업내용을 입력해주세요.")
			    } else if(managerName ===""){
					alert("담당자명을 입력해주세요.")
				} else if(managerEmail2 ===""){
					alert("담당자 이메일을 입력해주세요.")
				} else if(managerEmail3 ==="" && selectEmail ===""){
					alert("담당자 이메일을 완성해주세요.")
				} else if(companyPhone2 ===""){
					alert("대표번호를 입력해주세요.")
				} else if(companyPhone3 ===""){
					alert("대표번호를 완성해주세요.") 
				} else if(managerPhone2 ===""){
					alert("담당자 번호를 입력해주세요.")
				} else if(managerPhone3 ===""){
					alert("담당자 번호를 완성해주세요.") 
				} else if(recruitTitle ===""){
					alert("모집 제목을 입력해주세요.") 
				} else if(recruitMajor ===""){
					alert("모집직종을 선택해주세요.")
				} else if(jobType === undefined){
					alert("근무유형을 선택해주세요.") //이것도 안됨.... 
				} else if(jobDate === undefined){
					alert("근무요일을 선택해주세요.") //이것도 안됨...
				} else if(jobTime1 ===""){
					alert("근무시간을 선택해주세요.") 
				} else if(jobTime2 ===""){
					alert("근무시간을 모두 선택해주세요.") 
				} else if(jobTime3 ===""){
					alert("근무시간을 모두 선택해주세요.") 
				} else if(jobTime4 ===""){
					alert("근무시간을 모두 선택해주세요.") 
				} else if(salaryType === undefined){
					alert("급여종류를 선택해주세요.")
				} else if(salary ===""){
					alert("급여를 입력해주세요.") 
				} else if(address ===""){
					alert("주소를 입력해주세요.") 
				} else if(addressEtc ===""){
					alert("상세주소를 입력해주세요.") 
				} else if(edu ===undefined){
					alert("학력을 선택해주세요.") 
				} else if(edu ==="선택" && education===""){
					alert("상세학력을 선택해주세요.")
				} else if(careerChoice ===undefined){
					alert("경력 선택해주세요.") 
				} else if(careerChoice ==="경력" && careerPeriod===""){
					alert("상세경력을 선택해주세요.")
				}else{
			
					$("#registerUpdateForm").submit();	
				}
			}); 
	  
	   /* 복리후생 모달 */
		$("#jdj_modal_open").click(function(){
		$("#jdj_modal").attr("style", "display:block");
		});
		$("#jdj_modal_close").click(function(){
		$("#jdj_modal").attr("style", "display:none");
		}); 
		/* 우대가능 모달 */
		$("#jdj_modal_open2").click(function(){
		$("#jdj_modal2").attr("style", "display:block");
		});
		$("#jdj_modal_close2").click(function(){
		$("#jdj_modal2").attr("style", "display:none");
		}); 
		/* 상품선택 모달 */
		$("#jdj_modal_open3").click(function(){
		$("#jdj_modal3").attr("style", "display:block");
		});
		$("#jdj_modal_close3").click(function(){
		$("#jdj_modal3").attr("style", "display:none");
		}); 
		/* 상품선택2 모달 */
		$("#jdj_modal_open4").click(function(){
		$("#jdj_modal4").attr("style", "display:block");
		});
		$("#jdj_modal_close4").click(function(){
		$("#jdj_modal4").attr("style", "display:none");
		}); 
		/* 사진업로드 모달 */
		$("#jdj_modal_open5").click(function(){
		$("#jdj_modal5").attr("style", "display:block");
		});
		$("#jdj_modal_close5").click(function(){
		$("#jdj_modal5").attr("style", "display:none");
		}); 
		/* 모집직종 모달 */
		$("#jdj_modal_open6").click(function(){
		$("#jdj_modal6").attr("style", "display:block");
		});
		$("#jdj_modal_close6").click(function(){
		$("#jdj_modal6").attr("style", "display:none");
		}); 
		/* 주소 */
		function openDaumZipAddress(){
			new daum.Postcode({
				oncomplete:function(data) {
	
					$("#address").val(data.address);
					$("#address_etc").focus();
	
					console.log(data);
				}
			}).open();
			
		}
		
		 //업종 hide and show
	      $(function(){
		
	    	  var jarr = $(".jKinds");
	          
	          for(var i = 0; i < jarr.length; i++) {
	          	jarr[i].style.display = "none";
	          }
	          
	          var jKinds = $(".kindsJob").children();
	          
	          $(".kindsJob").children().each(function(index){
					$(this).click(function(){
						console.log(index);         
			            console.log($.type(index));
	          	for(var i = 0; i < jarr.length; i++) {
	              	jarr[i].style.display = "none";
	           	}
				
	          	switch(index) {
	          	case 1 : $("#cookingDiv").show(); break;
	          	case 2 : $("#medicalDiv").show(); break;
	          	case 3 : $("#constructDiv").show(); break;
	          	case 4 : $("#officeDiv").show(); break;
	          	case 5 : $("#drivingDiv").show(); break;
	          	case 6 : $("#salesDiv").show(); break;
	          	case 7 : $("#storeMangerDiv").show(); break;
	          	case 8 : $("#teacherDiv").show(); break;
	          	case 9 : $("#etcDiv").show(); break;
        		}
	          });
	     	 });
	     });
		 
/* =========== 페이지이동 =========== */

	

/*========= 이메일 select================== */
		$(function(){
			$("#selectEmail").change(function(){
				if($("#selectEmail option:selected").text() == "직접입력"){
					var value = ""; 
				} else {
					value = $("#selectEmail option:selected").text(); // 이외 메일 주소 선택일 경우 selectbox에서 선택한 값
				   	$("#manEmail2").val(value);                           // 값 입력됨
				}
			});
		});
/* ========모집인원 버튼 선택시 값입력 ==========*/
   $(function(){
		$("#people0").click(function(){
			$("#recruitPeople").val(0);
		});
	});
   $(function(){
		$("#people00").click(function(){
			$("#recruitPeople").val("00");
		});
	});
/* =============근무시간 버튼선택시 selectbox 값변경=========== */
   $(function(){
		$("#time1").click(function(){
			$("#jobTime1 option:eq(10)").prop("selected",true);
			$("#jobTime2 option:eq(1)").prop("selected",true);	
			$("#jobTime3 option:eq(19)").prop("selected",true);	
			$("#jobTime4 option:eq(1)").prop("selected",true);	
		});
		$("#time2").click(function(){
			$("#jobTime1 option:eq(7)").prop("selected",true);
			$("#jobTime2 option:eq(1)").prop("selected",true);	
			$("#jobTime3 option:eq(13)").prop("selected",true);	
			$("#jobTime4 option:eq(1)").prop("selected",true);	
		});
		$("#time3").click(function(){
			$("#jobTime1 option:eq(13)").prop("selected",true);
			$("#jobTime2 option:eq(1)").prop("selected",true);	
			$("#jobTime3 option:eq(19)").prop("selected",true);	
			$("#jobTime4 option:eq(1)").prop("selected",true);	
		});
		$("#time4").click(function(){
			$("#jobTime1 option:eq(19)").prop("selected",true);
			$("#jobTime2 option:eq(1)").prop("selected",true);	
			$("#jobTime3 option:eq(25)").prop("selected",true);	
			$("#jobTime4 option:eq(1)").prop("selected",true);	
		});
		$("#time5").click(function(){
			$("#jobTime1 option:eq(1)").prop("selected",true);
			$("#jobTime2 option:eq(1)").prop("selected",true);	
			$("#jobTime3 option:eq(7)").prop("selected",true);	
			$("#jobTime4 option:eq(1)").prop("selected",true);	
		});
	});
   /* ======== 연령선택시 무관일 때 ===============  */
   $(function(){
	   $("#interestAgeN").click(function(){ 
			$("#ageSelect1").val("").prop("disabled",true).css("background","lightgray");
			$("#ageSelect2").val("").prop("disabled",true).css("background","lightgray");
		});
   });
/* ======== 연령 선태일 때 ===============  */
   $(function(){
	   $("#interestAgeY").click(function(){ 
			$("#ageSelect1").prop("disabled",false).css("background","white");
			$("#ageSelect2").prop("disabled",false).css("background","white");
		});
   });
/* ======= 경력선택 무관일때 =============== */  
 $(function(){
	   $("#careerN").click(function(){ 
			$("#careerPeriod").val("").prop("disabled",true).css("background","lightgray");
		});
	   $("#careerY").click(function(){ 
			$("#careerPeriod").val("").prop("disabled",true).css("background","lightgray");
		});
   });
/* ======== 경력 선택일 때 ===============  */
   $(function(){
	   $("#careerYY").click(function(){ 
			$("#careerPeriod").prop("disabled",false).css("background","white");
		});
   });
   /* ======= 학력선택 무관일때 =============== */  
   $(function(){
  	   $("#eduN").click(function(){ 
  			$("#education").val("").prop("disabled",true).css("background","lightgray");
  		});
     });
  /* ======== 학력 선택일 때 ===============  */
     $(function(){
  	   $("#eduY").click(function(){ 
  			$("#education").prop("disabled",false).css("background","white");
  		});
     });
 
	/* =====우대 조건 값선택하기 =========== */
 	  window.onload = function () {
        $("#preferenceText").on("click", function () {
            var checked_items = "";
            $(".jyh_modal_content2 input:checked").each(function () {
                if (checked_items.length != 0) {
                    checked_items += ", ";
                }
                checked_items += $(this).val();
            });
          
            $("#preferenceDetail").val(checked_items);
            $("#jdj_modal2").attr("style", "display:none");
        });
        $("#welfareText").on("click", function () {
            var checked_items = "";
            $(".jyh_modal_content input:checked").each(function () {
                if (checked_items.length != 0) {
                    checked_items += ", ";
                }
               
                checked_items += $(this).val();
                
            });
           
            $("#welfareDetail").val(checked_items);
            $("#jdj_modal").attr("style", "display:none");
        });
  
        //모집직종
        $("#kindsJob_close").on("click", function () {
            var kindsJob = "";
            $(".JobChoice input:checked").each(function () {
               
                kindsJob += $(this).val();
                
            });
           
            $("#kindsJobDetail").val(kindsJob);
            $("#jdj_modal6").attr("style", "display:none");
           
        });
     
    }   
/* ===========저장된 값으로 셀렉스 선택되기 =========== */
	jQuery(function(){
	  	 $("#companyPhone1").val("<%=companyPhone[0]%>");	
	  	 $("#managerPhone1").val("<%=managerPhone1%>");
	  	 $("#fax1").val("<%=fax[0]%>");
	  	 $("#jobTime1").val("<%=jobTime[0]%>");
	  	 $("#jobTime2").val("<%=jobTime[1]%>");
	  	 $("#jobTime3").val("<%=jobTime1[0]%>");
	  	 $("#jobTime4").val("<%=jobTime1[1]%>");
	  	 $("#education").val("<%=recruit.getEducation() %>");
	  	 $("#careerPeriod").val("<%=recruit.getCareerPeriod()%>");
	  	 $("#ageSelect1").val("<%=age[0]%>");
	  	 $("#ageSelect2").val("<%=age[1]%>"); 
	  	
		});
		
		
 


</script>
</body>
</html>