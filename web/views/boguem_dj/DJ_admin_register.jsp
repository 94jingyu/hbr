<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.kh.hbr.recruit.model.vo.Recruit, java.util.*"%>
<%   
	ArrayList<Recruit> approvedList = (ArrayList<Recruit>) request.getAttribute("approvedList");
%>	
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>보금자리 등록 관리자 승인심사</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
* {margin: 0; padding: 0;}
#wrap {width: 1200px; height: 100%; margin: 0 auto;}

/* ============= 헤더 영역 ============= */
header {width: 100%; height: 100px; background: #013252;}
.logo {font-size: 45px; color: white;}
#title {margin-left: 10px;}

/* 사이트 바로가기 td id명 */
#goSite {font-size: 18px;}

/* =========== 좌측메뉴 및 내용 ============ */
.leftMenu {float: left; width: 20%; height: 1800px; background-color: #ECF0F7; border-right: 3px solid #CDCDCD}
.leftMenu {/* border-right: hidden; */ border-right: 3px solid #CDCDCD}
.leftMenu>ul {margin: 0; padding: 0; list-style: none;}
.leftMenu a {display: block; text-decoration: none; padding: 10px 20px; color: black;}
.leftMenu a:hover {background: darkgray; color: white;}

/* ========== HOME > 회원관리 > 기업인증내역 ========== */
.homeNavi {margin-top: 50px; float: right;}

/* ========== 기업인증내역 - 위아래 선 ========== */
.pageTitle {border-top: 3px solid rgb(200, 200, 200); border-bottom: 3px solid rgb(200, 200, 200);
	width: 930px; height: 80px; float: right; margin-top: 30px; margin-bottom: 90px;}

/* ========== 기업인증내역 -> 제목 ========== */
.pageTitle2 {padding: 23px;}

/* ============= 테이블 영역 ============= */

/* 기업인증내역 테이블 div class */
.Request {width: 850px;	margin-left: 30px; margin-top:20px;}



/* 검색할 키워드 입력 div class (text상자) */
.searchKey {
	width: 180px;
	height: 50px;
	float: right;
}



/* ============= 다정 부분 테이블 영역 ============= */
	
	/* 기업인증내역 테이블 div class */
	.Request {
		width:900px;
		height:100%;
		margin-left:20px;
	}

   	/* 상품적용 테이블 테두리*/
	.TB { 
	border-collapse:collapse;
	text-align:center;

	}
	
	tr {
	border :1px solid #afafaf;
	}
    
    /* 테이블컬럼 배경 */
    .tableBack{
	background-color:#f0f0f0;
	font-weight:bold;
	
	}
	/* 테이블안에 나만 왼쪽 정렬 */
	#align_left{
	text-align: left;
	}
	/* 검색버튼 */
	.backButton {
		float: right;
		margin-right: 10px;
		margin-left:30px;
		background: #013252;
		border: 0px;
		color: white;
		height: 35px;
		width: 70px;
		text-size: 15px;
	}
</style>
</head>
<body>
	<div id="wrap">
	<%@ include file="../guide/admin_menubar.jsp" %>
	
		<div class="homeNavi">
			<label style="font-size: 15px;">HOME > 보금자리관리 > 보금자리등록심사</label>
		</div>

		<div class="pageTitle">
			<div class="pageTitle2">
				<label style="font-size: 25px;"><b>보금자리 등록심사</b></label>
			</div>
			
     
       <!-- 테이블 -->
             <div class="Request"> 
			 <form id="searchForm" action="<%= request.getContextPath()%>/sorting.ad" method="post">
            	<table class="TB">
	          		<tr style="border:0;">
            			 <th colspan="4" width="580px" height="60px" id="align_left"><h3>&nbsp;총&nbsp;<label style="color:Orangered;"><%= approvedList.size() %></label>건</h3></th>
            			 
            			 
            			 <th></th>
            			 <th width="100px">상태선택 : </th>
            			 <th width="100px">
            			 	<select name="select" id="select" style="font-size:15px;width:100px; height:35px;">
		   							<option value="0">전체</option>
		   							<option value="3">대기</option>
		   							<option value="1">승인</option>
		   							<option value="2">반려</option>
		   						</select></th>
            			 <th><button class="backButton" id="sorting"><b>검색</b></button></th>
            		</tr>
            		</form>
            		<table class="TB">
            		<tr class="tableBack">
            			<th width="70px" height="50px">번호</th>
            			<th width="100px">아이디</th>
            			<th width="100px">담당자명</th>
            			<th width="320px">보금자리 제목</th>
            			<th width="1px"></th>
            			<th width="100px">상태</th>
            			<th width="100px">신청일</th>
            			<th width="100px">처리일</th>
            		<!-- 	<td width="90px">상세보기</td> -->	
            		</tr>
            		<% if( approvedList != null) {%>
            		<% int i = approvedList.size() + 1; 
            			//이렇게 값을 찍어 볼수도 있구나~!
            			/* System.out.println("sssss" + approvedList.get(5).getEnrollDate()); */
            		%>
            		<% for(Recruit r : approvedList) { 
            			/* Date enrollDate = r.getEnrollDate(); */
            		i--; %>
            		<tr>
						<td height="50px"><%= i %></td>
            			<td><%=r.getBmemberId()%></td>
            			<td><%=r.getManagerName() %></td>
            			<td id="align_left"><%=r.getRecruitTitle()%></td>
            			<td><label hidden><%=r.getRecruitNo() %></label></td>
            			<td><% if(r.getStatus().equals("END")){%>만료<%} else if(r.getStatus().equals("APPROVED")){%>승인<%}else if(r.getStatus().equals("REJECTED")){%>반려<%}else{ %>대기<%} %></td>
            			<td><% if(r.getEnrollDate() == null){%>-<%}else{ %><%=r.getEnrollDate() %><%} %></td>
            			<td><% if(r.getRecruitCheckDate() == null){%>-<%}else{ %><%=r.getRecruitCheckDate() %><%} %></td>
            			
            			<!-- <td><button id="status">상세</button></td> -->
            		</tr>
            		<%}%>
            		<!-- <tr>
						
						<td height="50px">2</td>
            			<td>abc1234</td>
            			<td>김태원</td>
            			<td id="align_left">분식집 200만 배달 라이더 모집</td>
            			<td>반려</td>
            			<td>20/01/01</td>
            			<td>20/01/14</td>
            			<td><button id="status">상세</button></td>
            		</tr>
            		<tr>
						
						<td height="50px">1</td>
            			<td>lucy5</td>
            			<td>정다정</td>
            			<td id="align_left">신전떡볶이 군포점 맛있는 떡볶이 만들어요~</td>
            			<td>승인</td>
            			<td>20/01/01</td>
            			<td>20/01/14</td>
            			<td><button id="status">상세</button></td>
            		</tr> -->
            		<!-- <tr style="text-align:center; border:0;">
        				<td colspan="7" height="70px;">
        					<button style="border:0;"> << </button>
	         				<button style="border:0;"> < </button>
	         				<button style="border:0;"> 1 </button>
	         				<button style="border:0;"> > </button>
	        				<button style="border:0;"> >> </button>
	        				
        				</td>
        			</tr> -->
   
            	</table>
            </div>
            
            </div>
        </div> 
    </div>
    
     <%}%>
     <script type="text/javascript">
     $(function(){
			$(".TB td").mouseenter(function(){
				$(this).parent().css({"background":"#EFEFEF", "cursor":"pointer"});
			}).mouseout(function(){
				$(this).parent().css("background", "none");
			}).click(function(){
				var num = $(this).parent().children().eq(4).text();
				console.log(num);
				location.href = "<%=request.getContextPath()%>/checkRecruitView.ad?num=" + num;
			});
		});
  <%--  function sorting(){
    	  if($("#select option:selected").text() == "대기"){
     		 var status = "심사대기";
     		 } else if($("#select option:selected").text() == "승인"){
     		 var status = "APPROVED"
     	 	 } else if(($("#select option:selected").text() == "반려"){
 		 	 var status = "REJECTED"
     	 } 
      			location.href ="<%=request.getContextPath()%>/sorting.ad?status=" + status;"
     }  --%>
    <%--  $(function(){
    	 $("#select").change(function(){
    		 if($("#select option:selected").text() == "대기"){
    		 var status = "심사대기";
    		 } else if($("#select option:selected")).text() =="승인"){
    		 var status = "APPROVED"
    	 	 } else if($("#select option:selected")).text() =="반려"){
		 	 var status = "REJECTED"
    	 } 
     			location.href ="<%=request.getContextPath()%>/sorting.ad?status=" + status;"
 
    	 });
     }); --%>
    <%--  $(function(){
    	 $("#sorting").click(function(){
    		 if($("#select").val() == "대기"){
    		 var status = "심사대기";
    		 } else if($("#select").val() == "승인"){
    		 var status = "APPROVED"
    	 	 } else if(($("#select").val() == "반려"){
		 	 var status = "REJECTED"
    	 } 
     			location.href ="<%=request.getContextPath()%>/sorting.ad?status=" + status;"
 
    	 });
     }); --%>
     </script>
</body>
</html>