<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.hbr.recruit.model.vo.*"%>
<% 
	Recruit requestRecruit = (Recruit) session.getAttribute("requestRecruit");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
<style type="text/css">
	/* 가장 큰 DIV */
	.jari_register {
	
	height : 100%;
	/* margin-top : 20px;  */
	padding : 30px;
	width: 1100px;
	margin: 0 auto;
	}
	/* 등록완료 배경 div */
	.backgray{
	height  : 465px;
	background: #EFEFEF;
	}
	/* 구인공고 등록 완료 메시지 div */
	.completeMsg{
	text-align: "center";
	padding-top:50px;
	padding-left:180px;
	padding-right:180px;
	text-align:"center";
	}
	/* 테이블안에 나만 왼쪽 정렬 */
	#align_left{
	text-align: left;
	}
	/* 상단에 패딩 */
	.t{
	padding-top: 10px;
	}
	/* 하단에 패딩 */
	.b{
	padding-bottom: 10px;
	}
	/* 상품적용 테이블 테두리*/
	.TB { 
	border-collapse:collapse;
	}
	tr {
	border :1px solid #afafaf;
	}
	/* 테이블 글씨 가운데 정렬 */
	table{
	text-align :center;
	}
	/* 테이블 화면 가운데 정렬 */
	#jari_2,.jari_3 {
	margin-left:300px;
	}
	/* 상품적용 버튼 */
	.button{
	width :100px;
	height :40px;
	background:#013252;
	color:white;
	margin-left: 190px;
	margin-top:10px;
	border-radius:8px;
	border:0;
	font-weight:bold;
	} 
	/* 보금자리 등록 버튼 */
	.button1{
	width :200px;
	height :60px;
	background:#013252;
	color:white;
/* 	margin-left: 450px;
	margin-top:10px; */
	border-radius:8px;
	border:0;
	font-weight:bold;
	font-size: 18px;
	}
	/* 상품 컬럼 배경색과 글씨굵기 */
	.tableBack{
	background-color:#f0f0f0;
	font-weight:bold;
	}</style>
</head>
<body>
<%@ include file="../guide/business_menubar.jsp" %>
 <div class = "jari_register">   
	    	<!-- 내용소개 부분 -->
	    	 	<h2 class = "b">보금자리 등록 완료</h2>
				<hr style="border:solid 1.5px #013252">
				<div class="backgray"> 
					<div class="completeMsg"><br><h1 class="b" style="color:gray"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;보금자리 등록 신청이 완료되었습니다.</h1>
					 	<hr><h3 class="t" style="color:gray">&nbsp;&nbsp;&nbsp;&nbsp;등록하신 구인 공고는<label style="color:red;"> 24시간 이내 심사를 통해 게재</label>됩니다.(주말 및 공휴일 제외)</h3>
					</div>
					<br>
					<br>
					<br>
					<h3 class="b">  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 공고 정보 </h3>
					<div  style="width:1040px; height:180px; background:white; margin-left:30px; border:1px solid #afafaf;">
					<table style="margin-top:12px; margin-left:20px;">
						<tr>
							<td width="120px" height="35px">· 공고 기업명 :</td>
							<td id="align_left"><%=requestRecruit.getCompanyName() %></td>
						</tr>
						<tr>
							<td width="120px" height="35px">· 공고 담당자 :</td>
							<td id="align_left"><%=requestRecruit.getManagerName() %></td>
						</tr>
						<tr>
							<td width="120px" height="35px"> · 공고 제목 :  </td>
							<td id="align_left"><%=requestRecruit.getRecruitTitle() %></td>
						</tr>
						<tr>
							<td width="120px" height="35px"> · 게재 상태 :  </td>
							<td id="align_left">심사대기<input type="text" id="bmemberNo" value="<%=bloginUser.getBmemberNo() %>" style="visibility:hidden;"></td>
						</tr>
					</table>
					</div>
					
				</div>
				<br><br><br>
				<table>
						<tr>
							<td width="220px" height="70px"></td>
							<td width="220px" height="70px"><button id = "myPage" class="button1" style="background:#afafaf;"> 마이페이지로 이동 </button></td>
							<td width="220px" height="70px"><button id = "Register" class="button1" onclick=""> 보금자리 관리 </button></td>
							<td width="220px" height="70px"><button id = "plusRegister" class="button1"> 보금자리 추가등록 </button></td>
							<td width="220px" height="70px"></td>                                       
							
						</tr>
						
					</table>
					<br><br><br>
					</div>
					 <br>
	    	 	<%@ include file="../guide/footer.jsp" %>
		<script type="text/javascript">
			$("#Register").click(function(){
				var num = $("#bmemberNo").val();
				console.log(num);
				location.href="<%= request.getContextPath() %>/selectList.bg?num="+num;
			})
			//보금자리등록으로 연결해주세요~
			$("#plusRegister").click(function(){
				location.href = "<%=request.getContextPath()%>/views/boguem_dj/DJ_boguem_register.jsp";
			})
			//마이페이지로 연결
			$("#myPage").click(function(){
				var bmemberNo = <%=bloginUser.getBmemberNo()%>;
				
				console.log(bmemberNo);
				
				location.href = "<%=request.getContextPath()%>/companyMypage.bs?bmemberNo="+bmemberNo;
		<%-- 		location.href = "<%=request.getContextPath()%>/views/business_DY/DY_business_MyPage.jsp"; --%>
			})
		</script>
</body>
</html>