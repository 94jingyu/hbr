<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.hbr.member.model.vo.Member"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>

/* nav */
* {
	text-align: left;
}

button {
	cursor:pointer;
}

.nav {
	width: 15%;
	height: 300px;
	border: solid 1px lightgray;
	margin-left: 5px;
}

.leftMenuArea {
	border: 1px solid lightgray;
	width: 13%;
	height: 400px;
	margin-left: 10px;
	float: left;
}

.leftMenu{
	margin-top: 10px;
	padding: 15px;
}

/* 문의하기 작성영역 */

#iqArea {
	width: 80%;
	margin-left: 220px;
	
}

.myInquiry {
	width:100%;
	height: 60px;
	margin-left: 20px;
	background-color: #013252;
	font-size: 20px;
	color: white;
	line-height: 60px;
}

.iqSelect {
	width: 200px;
	height: 30px;
	border: 1px solid lightgray;
	line-height: 30px;
	text-align: center;
	font-size: 14px;
	color : #333333;
	float:left;
	margin: 14px;
	margin-left: 0px;
	cursor:pointer;
	background: #ECF0F7;
	
}

.tableTr {
	margin-left: 30px;
	padding: 30px;
}

</style>

</head>
<body>

	<%@ include file="../common/E_user_menubar.jsp" %>
	
	<%
		Member aaa = (Member) session.getAttribute("loginUser");
	%>
	
	<!-- 로그인 유저 확인 -->
	<%-- <% if(loginUser != null) { %> --%>
		

	<hr style="border: solid 2px #013252; width:100%; margin:0 auto"><br>
	
	<%@ include file="../payment/faq_leftMenu.jsp" %>
	
	
	<!-- 사이트경로 -->
	    <p style="margin-left: 240px">HOME > 고객센터 > 묻고답하기 > 문의하기</p>
	    <br>
		
		
	<!-- 전체영역 -->
	<div id="wrap">
		
	<form class="inquiry_form" action="<%= request.getContextPath() %>/insert.iq" name="" method="post">
		<!-- 문의하기 영역 -->
		<div id="iqArea">
			<div class="myInquiry" style="font-weight:bold">&nbsp;&nbsp;&nbsp; 문의하기</div><br><br>
			<div style="border:1px solid lightgray; margin-left: 40px; background:#ECF0F7">
				<table id="myInquiryTable" style="width:95%">
				
				<input type="hidden" name="MemberNo" value="<%= aaa.getMemberNo() %>">
				
					<tr class="tableTr">
						<td width="25%" height="80px" style="font-weight: bold; font-size: 18px; padding-left: 40px; color:#333">이름</td>
						<td width="75%"><input type="text" placeholder="이름을 입력해주세요." style="width:650px; height:40px; font-size: 15px" name="writerName"><td>
					</tr>
					<tr class="tableTr">
						<td height="80px" style="font-weight: bold; font-size: 18px; ; padding-left: 40px; color:#333">문의유형</td>
						<td>	
							<select name="category" style="width:200px; height: 40px; color:gray; font-size:15px">
		                        <option value="입사지원관리">입사지원관리</option>
		                        <option value="이력서작성/관리">이력서작성/관리</option>
		                        <option value="회원가입/탈퇴">회원가입/탈퇴</option>
		                        <option value="아이디/비밀번호분실">아이디/비밀번호분실</option>
		                        <option value="배너광고문의">배너광고문의</option>
		                        <option value="기타">기타</option>
                    		 </select>
						<td>
					</tr>
					<tr class="tableTr">
						<td height="80px" style="font-weight: bold; font-size: 18px; padding-left: 40px; color:#333">문의제목</td>
						<td>
							<input type="text" name="iqTitle" style="border: 1px solid lightgray; width:650px; height:40px; text-align: text-top; font-size: 15px" placeholder="제목을 입력해주세요.">
						</td>
					</tr>
					<tr class="tableTr">
						<td height="80px" style="font-weight: bold; font-size: 18px; padding-left: 40px; color:#333">문의내용</td>
						<td>
							<textarea name="iqContent" placeholder="문의하실 내용을 입력해주세요." 
								style="border: 1px solid lightgray; width:630px; height:300px; font-size: 15px; padding:10px; resize: none" ></textarea><br>
						</td>
					</tr>
					<tr class="tableTr">
						<td height="80px" style="font-weight: bold; font-size: 18px">
							<label for="secret" style="cursor:pointer; padding-left: 40px; color:#333">비밀글</label>
							<input id="secret" name="" type="checkbox" id="checkBox" style="margin-left:20px">&nbsp;
						</td>
						
						<td>
							<input type="password" id="password" name="pwd" maxlength="4" style="width:190px; height:30px; font-size: 15px; text-align:center" placeholder="비밀번호 입력(숫자4자리)">
						</td>
					</tr>
					<!-- <tr class="tableTr">
						<td height="80px" style="font-weight: bold; font-size: 18px; padding-left: 40px">파일첨부</td>
						<td>
							<input type="file" style="width:650px">
							<p style="font-size:10px; color:#767676"> 2MB 이내의 PNG, JPEG, BMP, HWP, DOC, PDF, pptx, xlsx 파일을 첨부할 수 있습니다. 첨부할 파일명은 25자 이내로 작성하여 주시기 바랍니다.</p>
						</td>
					</tr> -->
				</table>
			</div><br>
			<div>
				<input id="agree" type="checkbox" id="essential1" style="margin-left:420px;">&nbsp;
				<label for="agree" style="cursor:pointer; font-weight:bold; font-size:18px; color:gray">개인정보 제공에 동의합니다.</label>
			</div><br><br>
			<div style="font-size:14px; color:#767676; margin-left: 300px">
				<p >- 입력하신 이름, 이메일은 상담내역에 대한 안내외에는 사용되지 않습니다.</p>
				<p>- 개인정보 보관기간 : 3년(소비자보호에 관한 법률시행령 제6조에 근거)</p>
			</div><br><br><br>
			<div style="margin-left: 380px">
				<button type="button" style="width:140px; height: 50px; font-size: 18px; font-weight: bold; background-color: gray; color:white; border:none; text-align:center" onclick="history.go(-1);">
				뒤로가기</button>&nbsp;&nbsp;
				<button type="submit" style="width:140px; height: 50px; font-size: 18px; font-weight: bold; background-color: #013252; color:white; border:none; text-align:center">
				등록하기</button>
			</div><br><br>
		</div><br><br>
		
		</form><!-- 문의하기 영역 End -->
		
		
		
		
		<!-- footer영역 -->
		<!-- <footer>Footer</footer> -->
	</div> <!-- 전체 wrap END -->
	
	<br><br><br><br>
	
	<%-- <%
	} else {
		request.setAttribute("msg", "잘못된 경로로 접근하셨습니다.");
		request.getRequestDispatcher("").forward(request, response);
	}
	%> --%>
	
	
	<script>
	
	//비밀글 text 활성화
	/* $("#essential1").click(function(){
		if($("#essential1")..val() == "essential1") {
			$("#password").removeAttr("disabled");
		}
	}); */
	
	var locked = false;
	
	$(function() {
		
		$("#checkBox").change(function() {
			if($("#checkBox").prop("checked")) {
				$("#password").attr("disabled", false);
			} else { 
				$("#password").attr("disabled", true);
			}
		});
	});
		
	
	
	<%-- 
	//이용약관 동의
	function enroll() { 
		var ch1 = $('input:checkbox[id="essential1"]').is(":checked");
		
		if(ch1 == true) {
			alert("게시물이 등록되었습니다.");
			location.href ="<%= request.getContextPath() %>/insert.iq";
		} else {
			alert("이용약관에 동의해야합니다.")	;
		}
	} --%>
	
	
	</script>
	
	
</body>
</html>












