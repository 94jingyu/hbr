<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*, com.kh.hbr.recruit.model.vo.*"%>

<%@ page import="java.text.DecimalFormat" %>
<%@ page import="com.kh.hbr.recruit.model.vo.*" %>
<%@ page import = "com.kh.hbr.resume.model.vo.Resume" %>

<%
	ArrayList<Recruit> slist = (ArrayList<Recruit>) request.getAttribute("list");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
* {
	margin: 0;
	padding: 0;
	text-align: left;
}
/* body {background-color: #fffde7;} */
#wrap {
	width: 1200px;
	margin: 0 auto;
}

/* 헤더 영역 */
.logo {
	float: left;
	width: 22%;
	height: 130px;
}

.toplogin {
	width: 100%;
	height: 50px;
	padding: 1%;
}

/* 상단메뉴바 */
.topMenu {
	/* display: inline-block; */
	width: 100%;
	height: 50px;
	/* background: #013252; */
	line-height: 30px; /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	vertical-align: middle; /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	text-align: center; /* 글씨 정렬을 가운데로 설정 */
	font-weight: bolder;
	font-size: 20px;
	color: #013252;
}

.topMenu>div {
	float: right;
}

.topMenu>ul {
	float: left;
	margin: 0;
	padding: 0;
	list-style: none;
}

.topMenu>ul>li {
	display: inline-block;
	padding: 0;
}

.topMenu a {
	display: block;
	text-decoration: none;
	padding: 10px 20px;
	color: black;
}

.topMenu a:hover {
	background: #013252;
	color: white;
}

/* 하이퍼텍스트 효과 */
a {
	text-decoration: none;
} /* 하이퍼텍스트 밑줄 효과 없애기 */

/* 광고 영역 */
.ads {
	width: 100%;
	height: 500px;
	clear: both;
}

/* 공고조회 및 로그인 영역  */
.b2_cotainer {
	width: 100%;
	height: 260px;
	background-color: #F0F0F0;
	text-align: center;
}

#selectJob {
	float: left;
	width: 70%;
	height: 250px;
	background-color: #039be5;
	display: inline-block;
}

#login {
	width: 30%;
	height: 250px;
	background-color: #0288d1;
	display: inline-block;
}

/* 채용정보 전체 div */
.iqWrap {
	width: 1200px;
	/* height: 800px; */
	border: 1px solid lightgray;
	margin: 0 auto;
	/* background: #ECF0F7; */
}

.iqLocation, .seouldiv {
	width: 1130px;
	/* height: 40px; */
	/* border: 1px solid red; */
	margin: 0 auto;
	margin-top: 1px;
}

.iqJob {
	width: 1120px;
	height: 500px;
	border: 1px solid red;
	margin: 0 auto;
	/* margin-top: 10px; */
}

.trLocation {
	align: center;
}

/* 광역시도 */
.tdLocation {
	height: 25px;
	width: 58px;
	border: 1px solid gray;
	text-align: center;
	margin: 1px;
	line-height: 28px;
	cursor: pointer;
}

 .tdLocation:hover {
 	background:#013252;
	color:white;
}

/* 지역 선택버튼 */
.seoul2, .gyoengi2, .inchoen2, .busan2, .daegu2, .gwangju2, .daejeon2,
	.ulsan2, .sejong2, .gangwon2, .kyeoungnam2, .kyeoungbuk2, .jeonnam2,
	.jeonbuk2, .chungnam2, .chungbuk2, .jeju2 {
	border: 1px solid gray;
	text-align: center;
	height: 25px;
	width: 126px;
	background-color: white;
	float: left;
	margin: 6px;
	line-height: 28px;
	cursor: pointer;
}

.seoul2:hover, .gyoengi2:hover, .inchoen2:hover, .busan2:hover, .daegu2:hover, .gwangju2:hover, .daejeon2:hover,
	.ulsan2:hover, .sejong2:hover, .gangwon2:hover, .kyeoungnam2:hover, .kyeoungbuk2:hover, .jeonnam2:hover,
	.jeonbuk2:hover, .chungnam2:hover, .chungbuk2:hover, .jeju2:hover {
	background:#013252;
	color:white;
	
}

/* 지역선택 div영역 */
	#seouldiv, #gyoengidiv, #inchoendiv, #busandiv, #daegudiv, #gwangjudiv,
	#daejeondiv, #ulsandiv, #sejongdiv, #gangwondiv, #kyeoungnamdiv,
	#kyeoungbukdiv, #jeonnamdiv, #jeonbukdiv, #chungnamdiv, #chungbukdiv,
	#jejudiv {
	width: 1130px;
	/* height: 160px; */
	border: 1px solid #8D8D8D;
	margin-left: 34px;
	background-color: lightgray;
}

/* 직종 선택버튼  */
.kindsJobTop {
	height: 25px;
	width: 111px;
	border: 1px solid gray;
	text-align: center;
	font-size: 15px;
	background-color: white;
	/* margin: 0.1px; */
	float: left;
	vertical-align: middle;
	line-height: 28px;
	cursor: pointer;
}

.kindsJobTop:hover {
	background:#013252;
	color:white;
}

#kindsJob {
	width: 1130px;
	margin-left: 34px;
	/* border: 1px solid red; */
	/* background-color: lightgray; */
}

.cooking2, .medical2, .construct2, .office2, .driving2, .sales2,
	.storeManger2, .teacher2, .etc2 {
	border: 1px solid gray;
	text-align: center;
	height: 25px;
	width: 126px;
	background-color: white;
	float: left;
	margin: 6px;
	line-height: 26px;
	cursor: pointer;
}

.cooking2:hover, .medical2:hover, .construct2:hover, .office2:hover, .driving2:hover, .sales2:hover,
	.storeManger2:hover, .teacher2:hover, .etc2:hover {
	background:#013252;
	color:white;
}


/* 직종선택 div영역 */
/* #kindsJob,  */
#cookingDiv, #medicalDiv, #constructDiv, #officeDiv, #drivingDiv,
	#salesDiv, #storeMangerDiv, #teacherDiv, #etcDiv {
	width: 1130px;
	border: 1px solid #8D8D8D;
	margin-left: 34px;
	background-color: #d3d3d3;
	
}
/* 지역 div영역 END */

/* 지역, 업종 선택 검색버튼*/
.searchBtn {
	width:150px;
	height: 50px;
	font-size: 18px;
	background-color:#013252;
	color: white;
	font-weight: bold;
	border: none;
	/* margin: 0 auto; */
	text-align: center;
	margin-left: 520px;
}



/* 일반채용 리스트 테이블*/
#tableTr1 {
	background-color: lightgray;
	text-align: center;
}

#tableTr2 {
	border-bottom: 1px solid lightgray;
	text-align: center;
}

#underListTab {
	width: 100%;
	border: 1px solid lightgray;
	border-collapse: collapse;
	text-align: center;
}

.tableTh {
	text-align: center;
}

.salary {
	float:left;
	background:#ECF0F7;
	text-align:center; 
	width: 40px;
	border:1px solid darkgray;
	border-radius: 5px;
}


/* 일반채용 리스트 테이블 END*/

#iqdiv {
	height: 40px;
	margin: 0 auto;
}

/* footer 영역 */
footer {
	width: 100%;
	height: 200px;
	background-color: #ffb300;
}

/* 버튼 효과 */
.btns {
	align: center;
}

#loginBtn, #memberJoinBtn, #logoutBtn, #changeInfo, #adminBtn {
	display: inline-block;
	text-align: center;
	background: orangered;
	color: white;
	height: 25px;
	width: 100px;
	border-radius: 5px;
}

#memberJoinBtn, #logoutBtn {
	background: yellowgreen;
}

#loginBtn:hover, #changeInfo:hover, #logoutBtn:hover, #memberJoinBtn:hover,
	#adminBtn:hover {
	cursor: pointer;
}

/* 선택 지역, 업종 출력*/
#printArea {
	width: 1200px;
	height: 100px;
	border: 1px solid lightgray;
	margin: 0 auto;
}

.printArea2 {
	width: 100%;
	height: 100%;
	background: #ECF0F7;
}

.printDiv {
	width: 400px;
	height: 26px;
	border: 1px solid gray;
	margin-left: 130px;
	padding-left: 20px;
}



/* 반짝 보금자리 공고 영역 */
.section {
	overflow: hidden;
	padding: 20px;
	width: 1160px;
	margin-top: 0px;
	border: 1px solid lightgray;
	/* border-top: 1px; */
	/* border-bottom: 1px; */
}

.item1 {
	width: 265px;
	margin: 0;
	height: 240px;
	padding: 10px; /* background-color: #ff9800; */
	border: 1px solid #66B2FF;
	float: left;
}

.item2 {
	width: 265px;
	margin: 0;
	height: 210px;
	padding: 10px; /* background-color: #ff9800; */
	border: 1px solid #AFAFAF;
	float: left;
}

.item3 {
	width: 265px;
	margin: 0;
	height: 150px;
	padding: 10px; /* background-color: #ff9800; */
	border: 1px solid #AFAFAF;
	float: left;
}

.item_gold {
	width: 265px;
	height: 110px;
}

.item_silver {
	width: 265px;
	height: 90px;
}

.bName {
	font-size: 17px;
	margin-bottom: 5px;
}

.bTitle {
	font-size: 22px;
	margin-bottom: 10px;
}

.bPeriods {
	font-size: 15px;
	color: gray;
	float: left;
}

.bScrap {
	float: right;
}

.scrap_img {
	width: 20px;
	height: 20px;
}


.getRecruitNo{
	display: none;
}


/* 진규 추가 */
/* td.clickBtn {background:#013252; color:white; } */
.gu.clickGu {background-color:#013252; color:white; }

</style>
<script>
	/* 진규 추가 */
	$(function(){
/* 		$("td").click(function(){
			$(this).addClass("clickBtn");    
			$(this).siblings().removeClass("clickBtn");
		}); */

		$(".gu").click(function(){
			$(this).addClass("clickGu");    
			$(this).siblings().removeClass("clickGu");
		});
		
		
	});
	
</script>
</head>
<body>
	<%@ include file="../common/E_user_menubar.jsp" %>
	
	
	<!-- 주소 문자열 끊어주기 -->
	<%
		String[] address = new String[slist.size()];
	
		for(int i = 0; i < address.length; i++) {
			if(slist.get(i).getDetailArea() != null) {
				address[i] = slist.get(i).getDetailArea();
			}
		}	
		
		String a = address[0];
	 %>


	<!-- 전체영역 -->
	<div id="wrap">
		
		<hr style="border: solid 2px #013252; width:100%; margin:0 auto"><br>
		<br>
		<!-- 사이트경로 -->
	    <p style="text-align:left">HOME > 채용정보</p><br><br>
		
		
		<!-- 서치박스 -->
		<p style="font-size: 22px; text-align:left"><b>채용정보</b><p>
		
		<!-- <table class="top">
			<tr>
				<td>채용정보</td>
				<td style="align:right"><input type="search" placeholder="어떤 일자리를 찾고 계신가요?" width="250px"></td>
			<tr>
		</table> -->
		
		<br>
		<div class="iqWrap">
		
			<p style="margin-left: 40px; margin-top: 30px; margin-bottom: 15px"><b>지역선택</b></p>
			<div id="iqdiv">
				<table class="iqLocation">
					<tr class="trLocation" id="location">
						<td><div class="tdLocation gu">전체</div></td>
						<td><div class="tdLocation gu" id="seoul">서울</div></td>
						<td><div class="tdLocation gu" id="gyoengi">경기</div></td>
						<td><div class="tdLocation gu" id="inchoen">인천</div></td>
						<td><div class="tdLocation gu" id="busan">부산</div></td>
						<td><div class="tdLocation gu" id="daegu">대구</div></td>
						<td><div class="tdLocation gu" id="gwangju">광주</div></td>
						<td><div class="tdLocation gu" id="daejeon">대전</div></td>
						<td><div class="tdLocation gu" id="ulsan">울산</div></td>
						<td><div class="tdLocation gu" id="sejong">세종</div></td>
						<td><div class="tdLocation gu" id="gangwon">강원</div></td>
						<td><div class="tdLocation gu" id="kyeoungnam">경남</div></td>
						<td><div class="tdLocation gu" id="kyeoungbuk">경북</div></td>
						<td><div class="tdLocation gu" id="jeonnam">전남</div></td>
						<td><div class="tdLocation gu" id="jeonbuk">전북</div></td>
						<td><div class="tdLocation gu" id="chungnam">충남</div></td>
						<td><div class="tdLocation gu" id="chungbuk">충북</div></td>
						<td><div class="tdLocation gu" id="jeju">제주</div></td>
					</tr>
				</table>
			</div>
			
			
			<div id="seouldiv" class="metro" style="height: 160px">
				<div class="seoul">
					<div class="seoul2 gu">전체</div>
					<div class="seoul2 gu">강남구</div>
					<div class="seoul2 gu">강동구</div>
					<div class="seoul2 gu">강북구</div>
					<div class="seoul2 gu">강서구</div>
					<div class="seoul2 gu">관악구</div>
					<div class="seoul2 gu">광진구</div>
					<div class="seoul2 gu">구로구</div>
					<div class="seoul2 gu">금천구</div>
					<div class="seoul2 gu">노원구</div>
					<div class="seoul2 gu">도봉구</div>
					<div class="seoul2 gu">동대문구</div>
					<div class="seoul2 gu">동작구</div>
					<div class="seoul2 gu">마포구</div>
					<div class="seoul2 gu">서대문구</div>
					<div class="seoul2 gu">서초구</div>
					<div class="seoul2 gu">성동구</div>
					<div class="seoul2 gu">성북구</div>
					<div class="seoul2 gu">송파구</div>
					<div class="seoul2 gu">양천구</div>
					<div class="seoul2 gu">영등포구</div>
					<div class="seoul2 gu">용산구</div>
					<div class="seoul2 gu">은평구</div>
					<div class="seoul2 gu">종로구</div>
					<div class="seoul2 gu">중구</div>
					<div class="seoul2 gu" >중랑구</div>
				</div>
			</div>
			<div id="gyoengidiv" class="metro" style="height: 160px">
				<div class="gyoengi">
					<div class="gyoengi2 gu">전체</div>
					<div class="gyoengi2 gu">가평군</div>
					<div class="gyoengi2 gu">고양시</div>
					<div class="gyoengi2 gu">과천시</div>
					<div class="gyoengi2 gu">광명시</div>
					<div class="gyoengi2 gu">광주시</div>
					<div class="gyoengi2 gu">구리시</div>
					<div class="gyoengi2 gu">군포시</div>
					<div class="gyoengi2 gu">김포시</div>
					<div class="gyoengi2 gu">남양주시</div>
					<div class="gyoengi2 gu">동두천시</div>
					<div class="gyoengi2 gu">부천시</div>
					<div class="gyoengi2 gu">성남시</div>
					<div class="gyoengi2 gu">수원시</div>
					<div class="gyoengi2 gu">시흥시</div>
					<div class="gyoengi2 gu">안산시</div>
					<div class="gyoengi2 gu">안성시</div>
					<div class="gyoengi2 gu">안양시</div>
					<div class="gyoengi2 gu">양주시</div>
					<div class="gyoengi2 gu">양평군</div>
					<div class="gyoengi2 gu">여주시</div>
					<div class="gyoengi2 gu">연천군</div>
					<div class="gyoengi2 gu">오산시</div>
					<div class="gyoengi2 gu">용인시</div>
					<div class="gyoengi2 gu">의왕시</div>
					<div class="gyoengi2 gu">의정부시</div>
					<div class="gyoengi2 gu">이천시</div>
					<div class="gyoengi2 gu">파주시</div>
					<div class="gyoengi2 gu">평택시</div>
					<div class="gyoengi2 gu">포천시</div>
					<div class="gyoengi2 gu">하남시</div>
					<div class="gyoengi2 gu">화성시</div>
				</div>
			</div>
			<div id="inchoendiv" class="metro" style="height: 80px">
				<div class="inchoen">
					<div class="inchoen2 gu">전체</div>
					<div class="inchoen2 gu">강화군</div>
					<div class="inchoen2 gu">계양구</div>
					<div class="inchoen2 gu">남동구</div>
					<div class="inchoen2 gu">동구</div>
					<div class="inchoen2 gu">미추홀구</div>
					<div class="inchoen2 gu">부평구</div>
					<div class="inchoen2 gu">서구</div>
					<div class="inchoen2 gu">연수구</div>
					<div class="inchoen2 gu">옹진군</div>
					<div class="inchoen2 gu">중구</div>
				</div>
			</div>
			<div id="busandiv" class="metro" style="height: 120px">
				<div class="busan">
					<div class="busan2 gu">전체</div>
					<div class="busan2 gu">강서구</div>
					<div class="busan2 gu">금정구</div>
					<div class="busan2 gu">기장군</div>
					<div class="busan2 gu">남구</div>
					<div class="busan2 gu">동구</div>
					<div class="busan2 gu">동래구</div>
					<div class="busan2 gu">부산진구</div>
					<div class="busan2 gu">북구</div>
					<div class="busan2 gu">사상구</div>
					<div class="busan2 gu">사하구</div>
					<div class="busan2 gu">서구</div>
					<div class="busan2 gu">수영구</div>
					<div class="busan2 gu">연제구</div>
					<div class="busan2 gu">영도구</div>
					<div class="busan2 gu">중구</div>
					<div class="busan2 gu">해운대구</div>
				</div>
			</div>
			<div id="daegudiv" class="metro" style="height: 80px"> 
				<div class="daegu">
					<div class="daegu2 gu">전체</div>
					<div class="daegu2 gu">남구</div>
					<div class="daegu2 gu">달서구</div>
					<div class="daegu2 gu">달성군</div>
					<div class="daegu2 gu">동구</div>
					<div class="daegu2 gu">북구</div>
					<div class="daegu2 gu">서구</div>
					<div class="daegu2 gu">수성구</div>
					<div class="daegu2 gu">중구</div>
				</div>
			</div>
			<div id="gwangjudiv" class="metro" style="height: 40px">
				<div class="gwangju">
					<div class="gwangju2 gu">전체</div>
					<div class="gwangju2 gu">광산구</div>
					<div class="gwangju2 gu">남구</div>
					<div class="gwangju2 gu">동구</div>
					<div class="gwangju2 gu">북구</div>
					<div class="gwangju2 gu">서구</div>
				</div>
			</div>
			<div id="daejeondiv" class="metro" style="height: 40px">
				<div class="daejeon">
					<div class="daejeon2 gu">전체</div>
					<div class="daejeon2 gu">대덕구</div>
					<div class="daejeon2 gu">동구</div>
					<div class="daejeon2 gu">서구</div>
					<div class="daejeon2 gu">유성구</div>
					<div class="daejeon2 gu">중구</div>
				</div>
			</div>
			<div id="ulsandiv" class="metro" style="height: 40px">
				<div class="ulsan">
					<div class="ulsan2 gu">전체</div>
					<div class="ulsan2 gu">남구</div>
					<div class="ulsan2 gu">동구</div>
					<div class="ulsan2 gu">북구</div>
					<div class="ulsan2 gu">울주군</div>
					<div class="ulsan2 gu">중구</div>
				</div>
			</div>
			<div id="sejongdiv" class="metro" style="height: 40px">
				<div class="sejong" style="height: 40px">
					<div class="sejong2 gu">세종특별시</div>
				</div>
			</div>
			<div id="gangwondiv" class="metro" style="height: 120px">
				<div class="gangwon">
					<div class="gangwon2 gu">전체</div>
					<div class="gangwon2 gu">강릉시</div>
					<div class="gangwon2 gu">고성군</div>
					<div class="gangwon2 gu">동해시</div>
					<div class="gangwon2 gu">삼척시</div>
					<div class="gangwon2 gu">속초시</div>
					<div class="gangwon2 gu">양구군</div>
					<div class="gangwon2 gu">양양군</div>
					<div class="gangwon2 gu">영월군</div>
					<div class="gangwon2 gu">원주시</div>
					<div class="gangwon2 gu">인제군</div>
					<div class="gangwon2 gu">정선군</div>
					<div class="gangwon2 gu">철원군</div>
					<div class="gangwon2 gu">춘천시</div>
					<div class="gangwon2 gu">태백시</div>
					<div class="gangwon2 gu">평창군</div>
					<div class="gangwon2 gu">홍천군</div>
					<div class="gangwon2 gu">화천군</div>
					<div class="gangwon2 gu">횡성군</div>
				</div>
			</div>
			<div id="kyeoungnamdiv" class="metro" style="height: 120px">
				<div class="kyeoungnam">
					<div class="kyeoungnam2 gu">전체</div>
					<div class="kyeoungnam2 gu">거제시</div>
					<div class="kyeoungnam2 gu">거창군</div>
					<div class="kyeoungnam2 gu">고성군</div>
					<div class="kyeoungnam2 gu">김해시</div>
					<div class="kyeoungnam2 gu">남해군</div>
					<div class="kyeoungnam2 gu">밀양시</div>
					<div class="kyeoungnam2 gu">사천시</div>
					<div class="kyeoungnam2 gu">산청군</div>
					<div class="kyeoungnam2 gu">양산시</div>
					<div class="kyeoungnam2 gu">의령군</div>
					<div class="kyeoungnam2 gu">진주시</div>
					<div class="kyeoungnam2 gu">창녕군</div>
					<div class="kyeoungnam2 gu">창원시</div>
					<div class="kyeoungnam2 gu">통영시</div>
					<div class="kyeoungnam2 gu">하동군</div>
					<div class="kyeoungnam2 gu">함안군</div>
					<div class="kyeoungnam2 gu">함양군</div>
					<div class="kyeoungnam2 gu">함천군</div>
				</div>
			</div>
			<div id="kyeoungbukdiv" class="metro" style="height: 120px">
				<div class="kyeoungbuk">
					<div class="kyeoungbuk2 gu">전체</div>
					<div class="kyeoungbuk2 gu">경산시</div>
					<div class="kyeoungbuk2 gu">경주시</div>
					<div class="kyeoungbuk2 gu">고령군</div>
					<div class="kyeoungbuk2 gu">구미시</div>
					<div class="kyeoungbuk2 gu">군위군</div>
					<div class="kyeoungbuk2 gu">김천시</div>
					<div class="kyeoungbuk2 gu">문경시</div>
					<div class="kyeoungbuk2 gu">봉화군</div>
					<div class="kyeoungbuk2 gu">상주시</div>
					<div class="kyeoungbuk2 gu">성주군</div>
					<div class="kyeoungbuk2 gu">안동시</div>
					<div class="kyeoungbuk2 gu">영덕군</div>
					<div class="kyeoungbuk2 gu">영양군</div>
					<div class="kyeoungbuk2 gu">영주시</div>
					<div class="kyeoungbuk2 gu">영천시</div>
					<div class="kyeoungbuk2 gu">예천군</div>
					<div class="kyeoungbuk2 gu">울릉군</div>
					<div class="kyeoungbuk2 gu">울진군</div>
					<div class="kyeoungbuk2 gu">의성군</div>
					<div class="kyeoungbuk2 gu">청도군</div>
					<div class="kyeoungbuk2 gu">청송군</div>
					<div class="kyeoungbuk2 gu">칠곡군</div>
					<div class="kyeoungbuk2 gu">포항시</div>
				</div>
			</div>
			<div id="jeonnamdiv" class="metro" style="height: 120px">
				<div class="jeonnam">
					<div class="jeonnam2 gu">전체</div>
					<div class="jeonnam2 gu">강진군</div>
					<div class="jeonnam2 gu">고흥군</div>
					<div class="jeonnam2 gu">곡성군</div>
					<div class="jeonnam2 gu">광양시</div>
					<div class="jeonnam2 gu">구례군</div>
					<div class="jeonnam2 gu">나주시</div>
					<div class="jeonnam2 gu">담양군</div>
					<div class="jeonnam2 gu">목포시</div>
					<div class="jeonnam2 gu">무안군</div>
					<div class="jeonnam2 gu">보성군</div>
					<div class="jeonnam2 gu">순천시</div>
					<div class="jeonnam2 gu">신안군</div>
					<div class="jeonnam2 gu">여수시</div>
					<div class="jeonnam2 gu">영광군</div>
					<div class="jeonnam2 gu">영암군</div>
					<div class="jeonnam2 gu">완도군</div>
					<div class="jeonnam2 gu">장성군</div>
					<div class="jeonnam2 gu">장흥군</div>
					<div class="jeonnam2 gu">진도군</div>
					<div class="jeonnam2 gu">함평군</div>
					<div class="jeonnam2 gu">해남군</div>
					<div class="jeonnam2 gu">화순군</div>
				</div>
			</div>
			<div id="jeonbukdiv" class="metro" style="height: 80px">
				<div class="jeonbuk">
					<div class="jeonbuk2 gu">전체</div>
					<div class="jeonbuk2 gu">고창군</div>
					<div class="jeonbuk2 gu">군산시</div>
					<div class="jeonbuk2 gu">김제시</div>
					<div class="jeonbuk2 gu">남원시</div>
					<div class="jeonbuk2 gu">무주군</div>
					<div class="jeonbuk2 gu">부안군</div>
					<div class="jeonbuk2 gu">순창군</div>
					<div class="jeonbuk2 gu">완주군</div>
					<div class="jeonbuk2 gu">익산시</div>
					<div class="jeonbuk2 gu">임실군</div>
					<div class="jeonbuk2 gu">장수군</div>
					<div class="jeonbuk2 gu">전주시</div>
					<div class="jeonbuk2 gu">정읍시</div>
					<div class="jeonbuk2 gu">진안군</div>
				</div>
			</div>
			<div id="chungnamdiv" class="metro" style="height: 120px">
				<div class="chungnam">
					<div class="chungnam2 gu">전체</div>
					<div class="chungnam2 gu">계룡시</div>
					<div class="chungnam2 gu">공주시</div>
					<div class="chungnam2 gu">금산군</div>
					<div class="chungnam2 gu">논산시</div>
					<div class="chungnam2 gu">당진시</div>
					<div class="chungnam2 gu">보령시</div>
					<div class="chungnam2 gu">부여군</div>
					<div class="chungnam2 gu">서산시</div>
					<div class="chungnam2 gu">서천군</div>
					<div class="chungnam2 gu">아산시</div>
					<div class="chungnam2 gu">연기군</div>
					<div class="chungnam2 gu">예산군</div>
					<div class="chungnam2 gu">천안시</div>
					<div class="chungnam2 gu">청양군</div>
					<div class="chungnam2 gu">태안군</div>
					<div class="chungnam2 gu">홍성군</div>
				</div>
			</div>
			<div id="chungbukdiv" class="metro" style="height: 80px">
				<div class="chungbuk">
					<div class="chungbuk2 gu">전체</div>
					<div class="chungbuk2 gu">괴산군</div>
					<div class="chungbuk2 gu">단양군</div>
					<div class="chungbuk2 gu">보은군</div>
					<div class="chungbuk2 gu">영동군</div>
					<div class="chungbuk2 gu">옥천군</div>
					<div class="chungbuk2 gu">음성군</div>
					<div class="chungbuk2 gu">제천시</div>
					<div class="chungbuk2 gu">중평군</div>
					<div class="chungbuk2 gu">진천군</div>
					<div class="chungbuk2 gu">청원군</div>
					<div class="chungbuk2 gu">청주시</div>
					<div class="chungbuk2 gu">충주시</div>
				</div>
			</div>
			<div id="jejudiv" class="metro" style="height: 40px">
				<div class="jeju">
					<div class="jeju2 gu">전체</div>
					<div class="jeju2 gu">서귀포시</div>
					<div class="jeju2 gu">제주시</div>
				</div>
			</div><br>
			
			<hr>
			<br>
			
			<!-- 직종선택 영역 -->			
			<p style="margin-left: 40px; margin-bottom:15px"><b>직종선택</b></p>
			<div id="kindsJob" style="height: 30px">
				<div class="kindsJob">
					<div class="kindsJobTop gu">전체</div>
					<div class="kindsJobTop gu" id="cooking">요리/서빙</div>
					<div class="kindsJobTop gu" id="medical">간호/의료</div>
					<div class="kindsJobTop gu" id="construct">생산/건설</div>
					<div class="kindsJobTop gu" id="office">사무/경리</div>
					<div class="kindsJobTop gu" id="driving">운전/배달</div>
					<div class="kindsJobTop gu" id="sales">상담/영업</div>
					<div class="kindsJobTop gu" id="storeManger">매장관리</div>
					<div class="kindsJobTop gu" id="teacher">교사/강사</div>
					<div class="kindsJobTop gu" id="etc">일반/기타</div>
				</div>
			</div>
			<div id="cookingDiv" class="jKinds" style="height: 80px">
				<div class="cooking">
					<div class="cooking2 gu">전체</div>
					<div class="cooking2 gu">주방장/조리사</div>
					<div class="cooking2 gu">주방/주방보조</div>
					<div class="cooking2 gu">찬모/밥모</div>
					<div class="cooking2 gu">설거지</div>
					<div class="cooking2 gu">서빙</div>
					<div class="cooking2 gu">카운터</div>
					<div class="cooking2 gu">점장/매니저</div>
					<div class="cooking2 gu">기타</div>
				</div>
			</div>
			<div id="medicalDiv" class="jKinds" style="height: 40px">
				<div class="medical">
					<div class="medical2 gu">전체</div>
					<div class="medical2 gu" style="font-size:15px">간호/간호조무사</div>
					<div class="medical2 gu">간병/요양보호사</div>
					<div class="medical2 gu">의료기사</div>
					<div class="medical2 gu">기타의료직</div>
				</div>
			</div>
			<div id="constructDiv" class="jKinds" style="height: 80px">
				<div class="construct">
					<div class="construct2 gu">전체</div>
					<div class="construct2 gu">제조/조립</div>
					<div class="construct2 gu">미싱/재단/섬유</div>
					<div class="construct2 gu">노무현장/조선소</div>
					<div class="construct2 gu">건설/공사/보수</div>
					<div class="construct2 gu">전기/시설관리</div>
					<div class="construct2 gu">배관/용접/취부</div>
					<div class="construct2 gu" style="font-size:14px">선반/밀링/머시닝</div>
					<div class="construct2 gu">식품제조/가공</div>
					<div class="construct2 gu" style="font-size:14px">자동차정비/튜닝</div>
					<div class="construct2 gu">설치/수리</div>
					<div class="construct2 gu">생산/포장/검사</div>
					<div class="construct2 gu">가구/목공/주방</div>
					<div class="construct2 gu" style="font-size:14px">금형/프레스/성형</div>
					<div class="construct2 gu">기타</div>
				</div>
			</div>
			<div id="officeDiv" class="jKinds" style="height: 40px">
				<div class="office">
					<div class="office2 gu">전체</div>
					<div class="office2 gu">경리/회계/인사</div>
					<div class="office2 gu">일반사무/내근직</div>
					<div class="office2 gu">기획/총무/관리</div>
					<div class="office2 gu">구매/자재/물류</div>
					<div class="office2 gu">비서/안내</div>
					<div class="office2 gu">기타</div>
				</div>
			</div>
			<div id="drivingDiv" class="jKinds" style="height: 40px">
				<div class="driving">
					<div class="driving2 gu">전체</div>
					<div class="driving2 gu" style="font-size:14px">이사/택배/퀵배송</div>
					<div class="driving2 gu" style="font-size:14px">대리/승용차/일반</div>
					<div class="driving2 gu" style="font-size:14px">버스/택시/승합차</div>
					<div class="driving2 gu">지입/차량용역</div>
					<div class="driving2 gu" style="font-size:14px">화물/중장비/특수차</div>
					<div class="driving2 gu" style="font-size:14px">음식점/식음료배달</div>
					<div class="driving2 gu">기타</div>
				</div>
			</div>
			<div id="salesDiv" class="jKinds" style="height: 80px">
				<div class="sales">
					<div class="sales2 gu">전체</div>
					<div class="sales2 gu">인바운드/CS</div>
					<div class="sales2 gu">아웃바운드/TM</div>
					<div class="sales2 gu">일반/기술영업</div>
					<div class="sales2 gu">보험/금융상담</div>
					<div class="sales2 gu">방문판매</div>
					<div class="sales2 gu">부동산상담</div>
					<div class="sales2 gu">홍보/마케팅</div>
					<div class="sales2 gu">기타</div>
				</div>
			</div>
			<div id="storeMangerDiv" class="jKinds" style="height: 80px">
				<div class="storeManger">
					<div class="storeManger2 gu">전체</div>
					<div class="storeManger2 gu">슈펴/마트</div>
					<div class="storeManger2 gu">편의점</div>
					<div class="storeManger2 gu">대형마트</div>
					<div class="storeManger2 gu">쇼핑몰/아울렛</div>
					<div class="storeManger2 gu">백화점</div>
					<div class="storeManger2 gu">찜질방/사우나</div>
					<div class="storeManger2 gu">농수산/청과/축산</div>
					<div class="storeManger2 gu">의류/잡화</div>
					<div class="storeManger2 gu">가전/휴대폰</div>
					<div class="storeManger2 gu">물류/재고</div>
					<div class="storeManger2 gu">PC방/오락실</div>
					<div class="storeManger2 gu">노래방/볼링장</div>
					<div class="storeManger2 gu">기타</div>
				</div>
			</div>
			<div id="teacherDiv" class="jKinds" style="height: 40px">
				<div class="teacher">
					<div class="teacher2 gu">전체</div>
					<div class="teacher2 gu">어린이집/유치원</div>
					<div class="teacher2 gu">방문/학습지</div>
					<div class="teacher2 gu">입시/보습/과외</div>
					<div class="teacher2 gu">외국어</div>
					<div class="teacher2 gu">예체능/컴퓨터</div>
					<div class="teacher2 gu">학원관리</div>
					<div class="teacher2 gu">기타교사</div>
 				</div>
			</div>
			<div id="etcDiv" class="jKinds" style="height: 80px">
				<div class="etc">
					<div class="etc2 gu">전체</div>
					<div class="etc2 gu">미화/청소/세탁</div>
					<div class="etc2 gu" style="font-size:14px">생활도우미/파출부</div>
					<div class="etc2 gu">경비/보안</div>
					<div class="etc2 gu">주유/세차/광택</div>
					<div class="etc2 gu">헤어/피부/미용</div>
					<div class="etc2 gu" style="font-size:14px">골프장/골프연습장</div>
					<div class="etc2 gu" style="font-size:10px">웨딩/이벤트/스튜디오</div>
					<div class="etc2 gu">상조/장례서비스</div>
					<div class="etc2 gu">컴퓨터/IT/디자인</div>
					<div class="etc2 gu">호텔/모델/숙박</div>
					<div class="etc2 gu" style="font-size:14px">주차관리/대리주차</div>
					<div class="etc2 gu">기타서비스</div>
 				</div>
			</div><br>
		</div><br>
		<!-- 지역, 직종선택 END -->
		
		<!-- 지역, 업종 출력영역 -->
		<div id="printArea">
			<table class="printArea2" >
				<tr>
					<td width="50%">
						<label style="font-weight:bold; margin-left: 40px; float:left">선택지역 : &nbsp;</label>
						<div class="printDiv">sdsd</div>
					</td>
					<td width="50%">
						<label style="font-weight:bold; margin-left: 40px; float:left">선택직종 : &nbsp;</label>
						<div class="printDiv">ssads</div>
					</td>
				</tr>
			</table>
		</div><br>
		
		<!-- 지역, 직종선택 검색버튼 -->
		<div class="serchBtn" align="center" style="width: 1200px; height: 80px; border: 1px solid lightgray; line-height: 80px">
			<button class="searchBtn">검색
			</button>
		</div><br>
		
		
		<!-- 반짝 상품 그리드  -->
        <div id="item_title" style="padding-top:30px;">
        	<!-- <img class="item_banjjak" alt="" src="../../static/images/user/crown.png">
        	<b style="color:#2FA599; font-size:22px; padding:2000">반짝</b><b style="font-size:22px; margin-top:-600px">채용공고</b> -->
        	<table style="margin-bottom:-10px">
        		<tr>
        			<td><img class="item_banjjak" alt="" src="../../static/images/user/crown.png"></td>
        			<td><b style="color:#2FA599; font-size:22px; margin-left: 20px">반짝</b><b style="font-size:22px;">채용공고</b></td>
        		</tr>
        	</table>
        </div><br>
        
        <div class="section">
	     		<div class="item1" style="clear:both;">
	            	<img class="item_gold" alt="" src="../../static/images/user/logo.png">
	            	<div class="bName">해볼래(주)</div>
	            	<div class="bTitle">서버 및 풀스택 개발자 채용</div>
	            	<div class="bPeriods">D-10</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star.png">
	        		</div>
	      		</div>
	            <div class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/common/logo_bm.png">
	            	<div class="bName">자바의 민족</div>
	            	<div class="bTitle">IT 별짓기 마스터 채용(신입/경력)</div>
	            	<div class="bPeriods">D-12</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
	            <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/user/logo.png">
	            	<div class="bName">해볼래(주)</div>
	            	<div class="bTitle">서버 및 풀스택 개발자 채용</div>
	            	<div class="bPeriods">D-10</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
	            <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/common/logo_bm.png">
	            	<div class="bName">자바의 민족</div>
	            	<div class="bTitle">IT 별짓기 마스터 채용(신입/경력)</div>
	            	<div class="bPeriods">D-12</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
	            <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/common/logo_bm.png">
	            	<div class="bName">자바의 민족</div>
	            	<div class="bTitle">IT 별짓기 마스터 채용(신입/경력)</div>
	            	<div class="bPeriods">D-12</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
		        <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/user/logo.png">
	            	<div class="bName">해볼래(주)</div>
	            	<div class="bTitle">서버 및 풀스택 개발자 채용</div>
	            	<div class="bPeriods">D-10</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
	            <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/common/logo_bm.png">
	            	<div class="bName">자바의 민족</div>
	            	<div class="bTitle">IT 별짓기 마스터 채용(신입/경력)</div>
	            	<div class="bPeriods">D-12</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
		        <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/user/logo.png">
	            	<div class="bName">해볼래(주)</div>
	            	<div class="bTitle">서버 및 풀스택 개발자 채용</div>
	            	<div class="bPeriods">D-10</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
		        <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/user/logo.png">
	            	<div class="bName">해볼래(주)</div>
	            	<div class="bTitle">서버 및 풀스택 개발자 채용</div>
	            	<div class="bPeriods">D-10</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
         	    <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/common/logo_bm.png">
	            	<div class="bName">자바의 민족</div>
	            	<div class="bTitle">IT 별짓기 마스터 채용(신입/경력)</div>
	            	<div class="bPeriods">D-12</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
		        <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/user/logo.png">
	            	<div class="bName">해볼래(주)</div>
	            	<div class="bTitle">서버 및 풀스택 개발자 채용</div>
	            	<div class="bPeriods">D-10</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
         	    <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/common/logo_bm.png">
	            	<div class="bName">자바의 민족</div>
	            	<div class="bTitle">IT 별짓기 마스터 채용(신입/경력)</div>
	            	<div class="bPeriods">D-12</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
			</div>
			<!-- 반짝 상품 그리드 END-->
			
			<br><br>
			
			<!-- 일반채용 정보 -->
			<div style="font-size:20px"><b>일반 보금자리 채용정보</b></div>
			<div id="iqNormalList" style="border: 1px solid lightgray; margin-top: 15px">
				<div id="iqNormalListArea">
					<div id="iqnorTableArea">
						<table id="underListTab">
							<tr id="tableTr1" style="text-align: center">
								<th class="tableTh" style="width:20%; height:35px;">근무지</th>
								<th class="tableTh" style="width:45%;">회사명 / 모집내용</th>
								<th class="tableTh" style="width:15%;">급여(원)</th>
								<th class="tableTh" style="width:25%;">등록일</th>
							</tr>
							
							<% for (Recruit r : slist) { %>
							<tr id="tableTr2" style="height: 100px;">
								<td class="getRecruitNo" hidden><%= r.getRecruitNo() %></td>
								<!-- 아래 조건이 아닌 경우 if로 설정해 줌  -->
								<% if (r.getDetailArea().equals("(null)")) { %>
									<td>주소없음</td>
								<% } else {
																	
									if(r.getDetailArea().length() > 10) { %>
									<td style="text-align:center"><%= r.getDetailArea().substring(0, 10).split(" ")[0] + " " + r.getDetailArea().substring(0, 15).split(" ")[1]%> </td>
									
									<%} else { %>
										<td><%= r.getDetailArea() %></td>
									<% } 
								} %>
								
								<td style="text-align:left; margin-left: 60px">
									<label style="font-size: 16px"><b><%= r.getCompanyName() %></b></label><br>
									<label style="font-size: 18px"><%= r.getRecruitTitle() %></label><br>
									<label style="color:#505050; font-weight:bold"><%= r.getJobTime() %></label>
								</td>
								<td style="text-align:right; color:#505050; font-weight:bold">
									<%-- <% if(r.getSalaryType().equals("시급")) { %> --%>
										<p class="salary"><%= r.getSalaryType() %></p>
									<%-- <% } else if(r.getSalaryType().equals("일급")) { %>
										<p style="float:left; background:#ECF0F7; text-align:center; width: 40px; border:1px solid navy; border-radius: 5px">
										<%= r.getSalaryType() %></p>
									<% } else if(r.getSalaryType().equals("월급")) { %>
										<p style="float:left; background:#ECF0F7; text-align:center; width: 40px; border:1px solid green; border-radius: 5px">
										<%= r.getSalaryType() %></p>
									<% } else if(r.getSalaryType().equals("연봉")) {%>
										<p style="float:left; background:#ECF0F7; text-align:center; width: 40px; border:1px solid pink; border-radius: 5px">
										<%= r.getSalaryType() %></p>
									<% } else { %>
										<p style="float:left; background:#ECF0F7; text-align:center; width: 40px; border:1px solid pink; border-radius: 5px">협의</p>
									<% } %> --%>
									
									<%= r.getSalary() %>원
								</td>
								<td style="text-align:center"><%= r.getEnrollDate() %></td>
							</tr>
							
							<% } %>
						
						</table>
					</div>
				</div>
			</div> <!-- 일반채용 리스트 end -->
			<br><br><br><br><br><br><br><br>
			
			
			<!-- 
			페이징 처리
			<div class="pagingArea" align="center"> 
			<button><<</button>
			<button><</button>1 페이지
			<button>></button>
			<button>>></button>
			</div>
			 -->
			

		
	</div> <!--전체 wrap 영역 END -->
	

		
	<script>
	//지역 hide and show
	$(function(){
        var arr = $(".metro");
         
        for(var i = 0; i < arr.length; i++) {
			arr[i].style.display = "none";
        }
        
      	var metroLo = $(".trLocation").children();

       	$(".trLocation").children().each(function(index){
            $(this).click(function(){
               //console.log(index);         
               //console.log($.type(index));
               
               for(var i = 0; i < arr.length; i++) {
       				arr[i].style.display = "none";
                }
	               
               switch (index){
                  case 1 : $("#seouldiv").show(); break;
                  case 2 : $("#gyoengidiv").show(); break;
                  case 3 : $("#inchoendiv").show(); break;
                  case 4 : $("#busandiv").show(); break;
                  case 5 : $("#daegudiv").show(); break;
                  case 6 : $("#gwangjudiv").show(); break;
                  case 7 : $("#daejeondiv").show(); break;
                  case 8 : $("#ulsandiv").show(); break;
                  case 9 : $("#sejongdiv").show(); break;
                  case 10 : $("#gangwondiv").show(); break;
                  case 11 : $("#kyeoungnamdiv").show(); break;
                  case 12 : $("#kyeoungbukdiv").show(); break;
                  case 13 : $("#jeonnamdiv").show(); break;
                  case 14 : $("#jeonbukdiv").show(); break;
                  case 15 : $("#chungnamdiv").show(); break;
                  case 16 : $("#chungbukdiv").show(); break;
                  case 17 : $("#jejudiv").show(); break;
              }
           });
        });         
        
      });
	      
    //업종 hide and show
	$(function(){
	
		var jarr = $(".jKinds");
          
         for(var i = 0; i < jarr.length; i++) {
         	jarr[i].style.display = "none";
         }
         
         var jKinds = $(".kindsJob").children();
         
         $(".kindsJob").children().each(function(index){
			$(this).click(function(){
				//console.log(index);         
	            //console.log($.type(index));
         	for(var i = 0; i < jarr.length; i++) {
             	jarr[i].style.display = "none";
          	}
		
         	switch(index) {
         	case 1 : $("#cookingDiv").show(); break;
         	case 2 : $("#medicalDiv").show(); break;
         	case 3 : $("#constructDiv").show(); break;
         	case 4 : $("#officeDiv").show(); break;
         	case 5 : $("#drivingDiv").show(); break;
         	case 6 : $("#salesDiv").show(); break;
         	case 7 : $("#storeMangerDiv").show(); break;
         	case 8 : $("#teacherDiv").show(); break;
         	case 9 : $("#etcDiv").show(); break;
       		}
        });
   	 });
	});
    
    
    
    //선택한 지역 + 업종 검색창에 삽입
 	/* $(".tdLocation").click(function() {
 		var selectedLocation = $(".tdLocation").eq(1).text();
		console.log(selectedLocation);
    }); */
/*     
   $(".seouldiv").children().each(function(){
		$(this).click(function() {
			
			/* var selectedLocation = $(".seoul2").text();
			console.log(selectedLocation); */
			
		/* 	var print = "서울 > 강남구";
			console.log(print);
			
		});
    }); */
    

 /* 
 	$(".seoul").children().each(function() {
		var location = $(".seoul2").text();
		console.log(location);
		
 	});
     */
     
     /* $(".tdLocation").click(function() {
    	var aa = $(".tdLocation").attr("#seoul");
    	$(".printLocation").attr('printLocation', aa);
    	console.log(aa);
     }); */
     
    /*  $(document).ready(function() {
    	var aa = $(".tdLocation").attr(".tdLocation");
    	console.log(aa);
     }); */
     
     
     //-------------------------------------------------------------
     
    /*  //salary 금액 콤마처리
    $(".salary").text(function(){
    	$(this).text(
    		$(this).text().format()
    	); 
     }); */
     
     
     
     //일반채용공고 리스트 상세페이지로 클릭
     $(function(){
    	 $("#underListTab td").mouseenter(function(){
    		$(this).parent().css({"background":"#E5E5E5", "cursor":"pointer"}) 
    	 }).mouseout(function(){
    		$(this).parent().css("background", ""); 
    	 }).click(function(){
    		var num = $(this).parent().children().eq(0).text();
	 		console.log(num);
    		location.href = "<%= request.getContextPath()%>/selectOneSearch.rc?num=" + num;
    	 });
     });
     
      
     <%-- //상세페이지에서 뒤로가기 버튼 클릭
      $(function(){
      	$("#returnList").click(function(){
		var num ="<%= recruit.getMemberNo()%>";
		console.log(num);
		location.href="<%= request.getContentLength()%>/selectListSearch.rc";
		});
      }); --%>
      
     <%--  $(function(){
      	$("#returnList")
      	.click(function(){
				var num =<%=recruit.ㅎㄷ%>
				console.log(num);
				location.href = "<%=request.getContextPath()%>/selectListSearch.rc?num=" + num;
			});
      }); --%>
      
      
      
   </script>

   
	
</body>
</html>












