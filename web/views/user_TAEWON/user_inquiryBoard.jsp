<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*, com.kh.hbr.board.inquiryBoard.model.vo.*"%>

<%
	ArrayList<InquiryBoard> list = (ArrayList<InquiryBoard>) session.getAttribute("list");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>

<style>

*{
	text-align:left;
}

button {
	cursor:pointer;
}

/* 레프트 메뉴바*/
.leftMenuArea {
	border: 1px solid lightgray;
	width: 12%;
	height: 400px;
	margin-left: 10px;
	float: left;
}

.leftMenu{
	margin-top: 10px;
	padding: 15px;
}


#menu {
	cursor: pointer;
	margin-top: 10px;
}

#menu:hover {
	text-weight: bold;
	color: #2FA599;
	margin-top: 10px;
}

.mainArea {
	width:85%;
	height: 500px;
	margin-left: 240px;
}

.qnaTitle{
	width:94%;
	height: 60px;
	color: white;
	font-weight: bold;
	font-size: 24px;
	background-color: #013252; 
	font-size: 20px;
	line-height: 60px;
	
	/* margin-left: 180px; */
	
}

.board{
	border-collapse: collapse;
	border: 1px solid #d3d3d3;
	width: 93%;
}

.boardTh{
	font-size: 16px;
	height: 40px;
	background-color: #d3d3d3;
	border-collapse: collapse;
	text-align: center;
}

.boardTr {
	height: 40px;
	text-align: center;
	border-bottom: 1px solid #d3d3d3;
}

.boardTd {
	text-align: center;
}

.iqbtn {
	width: 120px;
	height: 50px;
	text-align: center;
	font-size: 16px;
	font-weight: bold;
	color:white;
	background: #013252;
	border: none;
	margin-left: 800px;
}


</style>


</head>
<body>
	<%@ include file="../common/E_user_menubar.jsp" %>
	<%-- <%@ include file="../payment/faq_leftMenu.jsp" %> --%>
	
	<hr style="border: solid 2px #013252; width:100%; margin:0 auto"><br>
	
	<%@ include file="../payment/faq_leftMenu.jsp" %>
		
	<!-- 전체영역 -->
	<div id="wrap">
		<!-- 경로 -->
		<p style="font-size: 16px; margin-left: 240px">고객센터 > 묻고답하기</p><br>
		
		<!-- 메인영역 -->
		<div class="mainArea">
			
			<div class="qnaTitle">&nbsp;&nbsp;&nbsp; 묻고답하기</div>
			<br><br><br>
			<div class="iqTitle" style="font-size:18px">
				 <span><b>&nbsp;&nbsp;&nbsp;문의게시판</b></span>
				 <span class="scrap1"> | </span>
	             <span class="scrap2"><b>게시물 총</b></span>
	             <span class="scrap3" style="color:red"><b><%= list.size() %></b></span>
	             <span class="scrap4"><b>건</b></span>
			</div><br>
			  
			<!-- 테이블 -->
			<div class="boardListArea">
				<table class="board">
					<tr>
						<th class="boardTh" style="width:10%">작성일</th>
						<th class="boardTh" style="width:20%">문의유형</th>
						<th class="boardTh" style="width:25%">문의제목</th>
						<th class="boardTh" style="width:10%">작성자</th>
						<th class="boardTh" style="width:10%">답변여부</th>
					</tr>
					<% for (InquiryBoard iq : list) { %>
					<tr class="boardTr">
						<td class="boardTd"><%= iq.getEnrollDate() %></td>
						
						<% if(iq.getCategory() == null) { %>
						<td style="text-align: center"> 일반문의 </td>
						<% } else { %>
						<td class="boardTd"><%-- <%= iq.getCategory() %> --%>일반문의</td>
						<% } %>
						<td style="text-align: left"><%= iq.getIqTitle() %></td>
						<td class="boardTd"><%= iq.getWriterName() %></td>
						<% if(iq.getAnswerYn().equals("N")) { %>						
						<td class="boardTd">답변대기</td>
						<% } else { %>
						<td class="boardTd">답변완료</td>
						<% } %>
					</tr>
					<% } %>
				</table><br>
				
				<%-- <%
         			if(loginUser != null) {
         		%>	 --%>
				<button class="iqbtn" onclick="iqBtn();")>문의하기</button>
				<%-- <% } %> --%>
				
			</div>
		</div><!-- 메인영역 End -->
		
		<br><br><br><br><br><br><br><br><br><br><br><br><br>
		<br><br><br><br><br><br><br><br><br><br><br><br><br>
		
		
	</div><!-- 전체영역 wrap End -->
	
	
	
	<script>
	
	//문의 글쓰기 버튼
	function iqBtn() {
		
		location.href = "views/user_TAEWON/user_Inquiry.jsp";
		
	}
	
	
	
	
	</script>
	
	
	
	
	
	
	
	
</body>
</html>









