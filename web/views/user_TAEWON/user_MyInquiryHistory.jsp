<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script	src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
* {
	margin: 0;
	padding: 0;
}
/* body {background-color: #fffde7;} */
#wrap {
	width: 1200px;
	margin: 0 auto;
}

/* 헤더 영역 */
.logo {
	float: left;
	width: 22%;
	height: 130px;
}

.toplogin {
	width: 100%;
	height: 50px;
	padding: 1%;
}

/* 상단메뉴바 */
.topMenu {
	/* display: inline-block; */
	width: 100%;
	height: 50px;
	/* background: #013252; */
	line-height: 30px; /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	vertical-align: middle; /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	text-align: center; /* 글씨 정렬을 가운데로 설정 */
	font-weight: bolder;
	font-size: 20px;
	color: #013252;
}

.topMenu>div {
	float: right;
}

.topMenu>ul {
	float: left;
	margin: 0;
	padding: 0;
	list-style: none;
}

.topMenu>ul>li {
	display: inline-block;
	padding: 0;
}

.topMenu a {
	display: block;
	text-decoration: none;
	padding: 10px 20px;
	color: black;
}

.topMenu a:hover {
	background: #013252;
	color: white;
}

/* 하이퍼텍스트 효과 */
a {
	text-decoration: none;
} /* 하이퍼텍스트 밑줄 효과 없애기 */

/* 광고 영역 */
.ads {
	width: 100%;
	height: 500px;
	clear: both;
}

/* 공고조회 및 로그인 영역 */
.b2_cotainer {
	width: 100%;
	height: 260px;
	background-color: #F0F0F0;
	text-align: center;
}

#selectJob {
	float: left;
	width: 70%;
	height: 250px;
	background-color: #039be5;
	display: inline-block;
}

#login {
	width: 30%;
	height: 250px;
	background-color: #0288d1;
	display: inline-block;
}

.fsearch_wp {
	width: 100%;
	border: 1px solid #e1e1e1;
	border-bottom: none;
	font-size: 14px;
	box-sizing: border-box;
}

/* 일반채용리스트 */
.normalJobList {
	width: 88%;
	font-size: 20px;
	font-weight: bold;
}

#normaljobList {
	font-size: 16px;
	font-weight: bold;
	border: none;
	border-bottom: 3px;
	border-bottom-color: black;
	background-color: lightGray;
	
}

/* footer 영역 */
footer {
	width: 100%;
	height: 200px;
	background-color: #ffb300;
}

/* 버튼 효과 */
.btns {
	align: center;
}

#loginBtn, #memberJoinBtn, #logoutBtn, #changeInfo, #adminBtn {
	display: inline-block;
	text-align: center;
	background: orangered;
	color: white;
	height: 25px;
	width: 100px;
	border-radius: 5px;
}

#memberJoinBtn, #logoutBtn {
	background: yellowgreen;
}

#loginBtn:hover, #changeInfo:hover, #logoutBtn:hover, #memberJoinBtn:hover,
	#adminBtn:hover {
	cursor: pointer;
}

/* nav */
.nav {
	width: 15%;
	height: 300px;
	border: solid 1px lightgray;
	margin-left: 5px;
}

/* 박다영 스타일 */
/* left 메뉴 */
   .leftblock {
      display: block;
   }
   
   #left2, #left3, #left4 {
      margin-top:10px;
   }
   
   #left1 {
      width: 180px;
      height:900px;
      margin-top:10px;
      align: left;
   }
   
   .myPage {
   	margin-left: 10px;
   }
   
   #left2 {
      height:290px;
   }
   
   #left3 {
      height:260px;
   }
   
   #left4 {
      height:250px;
   }
   
   /* left메뉴 가로선 */
   hr {
      width:90%;
      color:lightgray;
      size:2px;
      margin-left: 5%;
   }
   
   ul {
      list-style:none;
   }

.leftMenu{float: left; width: 20%; height: 580px; margin-left: 30px; margin-top: 10px; text-align: left}
   
   /* 내용 부분 */
/* .section{width: 75%; height: 1500px; } */
   
   .leftMenu>ul {       
       margin: 10;
       padding: 0;
       list-style: none;
    }
    .leftMenu a {
       display: block;
       text-decoration: none;
       padding: 10px 20px;
       color: black;
     }
.left {
      border:2px solid rgb(192, 192, 192);
   }

/* .inquiryType {
	display: inline-block;
} */

.myInquiry {
	width: 80%;
	height: 60px;
	margin-left: 240px;
	background-color: #013252;
	font-size: 20px;
	color: white;
	line-height: 60px; 
}


#inquiry {
	display: inline-block;
}

#period {
	display: inline-block;
	width: 50px;
	height: 25px;
	border: 1px solid gray;
	background-color: lightgray;
	/* line-height: 60px; */
}

#inquiryTable {
	border-collapse: collapse;
	align: left;
}

#qTable {
	width: 60px;
	height: 80px;
	background-color: #013252;
	font-weight: bolder;
	font-size: 14px;
	color: white;
	border: none;
}

#iqTable {
	width: 76%;
	margin-left: 260px;
	text-align: center;
	border-collapse: collapse;
}

.iqTr{
	background-color: lightgray;
	font-weight: bold;
}


/* footer 영역 */
.footer {border: 1px solid #F0F0F0; width: 1200px; height: 210px; text-align: center; background-color: #afafaf; margin-top: 1000px}
.footerTable{margin: 0 auto; padding: 30px;}

</style>

</head>
<body>
	<%@ include file="../common/E_user_menubar.jsp" %>
	<div id="wrap">
		
		<hr style="border: solid 2px #013252; width:100%; margin:0 auto"><br>
		<br>
		
		<!-- 사이트경로 -->
	    <p style="margin-left: 240px; text-align:left">HOME > 마이페이지 > 나의문의내역</p>
		
		<!-- 레프트메뉴바 -->
		<aside class="leftMenu">
             <div class="left leftblock" id="left1"><br>&nbsp;
             <label class="myPage" onclick="userMypage();" style="font-size:20px;"><b>마이페이지</b></label><br><br>
             <hr>
             <br>
                <ul>
                <li>
                    <a href="#" style="font-size:18px;"><b>이력서관리</b></a>
                    <a href="#" onclick="resumeWrite();" style="font-size:15px;">이력서 등록</a>
                    <a href="#" onclick="resumeManage();" style="font-size:15px;">이력서 현황</a>
                 </li><br>
                 <hr><br>
                 <li>
                      <a href="#" style="font-size:18px;"><b>입사지원 관리</b></a>
                    <a href="#" onclick="applyHistory();" style="font-size:15px;">입사지원목록</a>
                    <a href="#" onclick="myresumeBusiness();"style="font-size:15px;">내 이력서 열람기업</a>
                    <a href="#" onclick="resumeLimit();"style="font-size:15px;">이력서 열람 제한</a>
                 </li><br>
               <hr><br>
               <li>
                  <a href="#" onclick="recruitBookmark();" style="font-size:18px;"><b>스크랩보금자리</b></a>
               </li><br>
               <hr><br>
               <li> 
                  <a href="#" onclick="recentViewRecruit();" style="font-size:18px;"><b>최근 본 보금자리</b></a>
               </li><br>
               <hr><br>
               <li><a href="#" style="font-size:18px;"><b>회원정보 관리</b></a>
                   <a href="#" onclick="changeInfo()" style="font-size:15px;">회원정보수정</a>
                   <a href="#" onclick="del();" style="font-size:15px;">회원탈퇴</a>
               </li><br>
               <hr><br>
               <li><a href="#" onclick="myinquiry();" style="font-size:18px;"><b>나의 문의내역</b></a>
               </li>
             </ul>  
             </div>
         </aside><br>
			
			
			
		<div class="section">	
			<div class="myInquiry" style="font-weight: bold; text-align:left">나의 문의내역</div><br>
			<table style="align: left; float: left">
				<tr>
					<td style="font-weight: bold">문의내역 목록 |</td>
					<td style="font-weight: bold">총 0건</td>
				</tr>
			</table><br><br>

			<table id="inquiryTable" style="float:left">
				<tr>
					<td colspan="2" width="85px" height="60px">
					<img src="../../static/images/user/inquiry.png" width="80px" height="80px"></td>
					<td><p style="font-size:14px;">
							• 문의내역의 제목을 클릭하면 상세내용과 답변 확인이 가능합니다.<br>
							• 1:1 문의는 1:1 문의하기 버튼을 클릭하거나 고객센터의 묻고 답하기 메뉴에서 가능합니다.
						</p>
					</td>
				</tr>
			</table><br><br><br>
			
			<table width="920px" height="100px" style="border:1px solid lightgray; margin-top: 20px">
				<tr>
					<td colspan="1" rowspan="2" width="80px" height="80px" style="font-weight: bold; text-align:center">검색조건</td>
					<td colspan="1" width="400px">
						<label style="font-size:14px">조회기간</label>
						<button id="period" style="font-size:12px">1주일</button>
						<button id="period" style="font-size:12px">1개월</button>
						<button id="period" style="font-size:12px">2개월</button>
						<button id="period" style="font-size:12px">3개월</button>
						<button id="period" style="font-size:12px">6개월</button>
						<button id="period" style="font-size:12px">1년</button>
					</td>
					<td colspan="2">
						<input type="date" style="width: 160px; height:30px">
						 ~ <input type="date" style="width: 160px; height:30px">
					</td>
					<td rowspan="2">
						<button id="qTable" style="font-weight: bold">검색</button>
					</td>
					
				</tr>
				<tr>
					<td colspan="1" width="120px">
						<label style="font-size:14px">검색어</label>&nbsp; &nbsp;
						<input type="text" placeholder="검색할 단어를 입력하세요." style="font-size:14px; width:315px; height:30px">
					</td>
				</tr>
			</table><br><br><br>

			<div style="border:1px solid lightgay">
			<table id="iqTable">
				<tr class="iqTr">
					<td id="iqList" width="100px" height="40px">번호</td>
					<td id="iqList" width="140px">문의종류</td>
					<td id="iqList" width="500px">제목</td>
					<td id="iqList" width="140px">답변상태</td>
					<td id="iqList" width="140px">등록일</td>
				</tr>
				<tr>
					<td id="iqList">3</td>
					<td id="iqList">기타문의</td>
					<td id="iqList" style="margin-left:20px; float:left; line-height: 40px;">궁금한 점이 있어 글남깁니다asdssssss.</td>
					<td id="iqList">답변완료</td>
					<td id="iqList">20/01/25</td>
				</tr>
				<tr>
					<td id="iqList">2</td>
					<td id="iqList">기타문의</td>
					<td id="iqList" style="margin-left:20px; float:left; line-height: 40px;">관리자 나와라.</td>
					<td id="iqList">답변완료</td>
					<td id="iqList">20/01/25</td>
				</tr>
				<tr>
					<td id="iqList">1</td>
					<td id="iqList">광고문의</td>
					<td id="iqList" style="margin-left:20px; float:left; line-height: 40px;">광고문의 답변바람</td>
					<td id="iqList">답변완료</td>
					<td id="iqList">20/01/25</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>
						<button style="width:120px; height:40px; background-color: #013252; color:white; font-size:14px">1:1 문의하기</button>
					</td>
				</tr>
			</table><br>
			</div><br>
			
			
			
			<%-- 페이지 처리 --%>
			<div class="pagingArea" align="center">
	         <button> << </button>
	         <button> < </button>1 페이지
	         <button> > </button>
	         <button> >> </button>
	         </div>
	         
	      <%-- <div class="pagingArea" align="center">
	         <button onclick="location.href='<%= request.getContextPath()%>/selectList.bo?currentPage=1'"><<</button>
	         
	         <% if (currentPage <= 1) { %>
	         <button disabled><</button>
	         <% } else { %>
	      	 <button onclick="location.href='<%= request.getContextPath()%>/selectList.bo?currentPage=<%= currentPage - 1%>'"><</button>
	      	 <% } %>
	      	 
	      	 
	      	 <% for (int p = startPage; p <= endPage; p++) {
	      	 		if(p == currentPage) {
	      	 %>
	      	 		<button disabled><%= p %></button>
	      	 <%
	      	 	} else {
	      	 %>
	      	 		<button onclick="location.href='<%= request.getContextPath() %>/selectList.bo?currentPage=<%= p %>'"><%= p %></button>
	      	 <%
	      		 }
				}
	      	 %>
	      	 
	      	 
			<% if (currentPage >= maxPage) { %>
			<button disabled>></button>
			<% } else { %>
			<button onclick="location.href='<%= request.getContextPath() %>/selectList.bo?currentPage=<%= currentPage + 1 %>'">></button>
			<% } %>
	      	 
	      	 
	      	 <button onclick="location.href='<%= request.getContextPath()%>/selectList.bo?currentPage=<%= maxPage %>'">>></button>
	      </div> --%>
		
	
		
		</div> <!-- section 영역 END -->
		
	
	
	
	<div class="footer">
		<table class="footerTable">
			<tr>
				<td>홈&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;광고문의&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;제휴문의&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;인재채용&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;이용약관&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>개인정보처리방침</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;고객센터</td>
			</tr>
			<tr>
				<td colspan="7" style="height: 20px;"></td>
			</tr>
			<tr>
				<td colspan="7">고객센터: 1588-9350(평일 09:00 ~ 19:00 토요일 09:00 ~ 15:00)&nbsp;&nbsp;&nbsp;FAX : 02-123-456&nbsp;&nbsp;&nbsp;Email : sunshine@kh.co.kr</td>
			</tr>
			<tr>
				<td colspan="7">서울특별시 강남구 강남구 테헤란로14길 6 해볼래&nbsp;&nbsp;&nbsp;대표 : 윤햇살&nbsp;&nbsp;&nbsp;사업자등록번호 : 110-81-34859</td>
			</tr>
			<tr>
				<td colspan="7">통신판매업 신고번호 : 2020-서울역삼-0287호&nbsp;&nbsp;&nbsp;직업정보제공사업 신고번호 : 서울청 제2020-01호</td>
			</tr>
			<tr>
				<td colspan="7" style="height: 20px;"></td>
			</tr>
			<tr>
				<td colspan="7">Copyright ⓒ Sunshine Corp. All Right Reserved.</td>
			</tr>
		</table>
    </div> 	
	<%-- <%@ include file="../guide/footer.jsp" %> --%>
	</div> <!-- 전체 wrap 영역 END -->
	
	 <script>
	 
		//레프트바 페이지 이동
	    function resumeWrite() {
           location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/resumeWrite.jsp";
           }
         function resumeManage() {
            location.href = "<%=request.getContextPath()%>/res.list";
         }
         function recruitBookmark() {
            location.href = "<%=request.getContextPath()%>/views/user_TAEWON/user_recruitBookmark.jsp";
         }
         function myinquiry(){
            <%-- location.href = "<%=request.getContextPath()%>/select.iq"; --%>
            location.href = "<%=request.getContextPath()%>/views/user_TAEWON/user_MyInquiryHistory.jsp";
         }
         function applyHistory(){
            location.href = "<%=request.getContextPath()%>/selectApplyList.ap";
         }
         function recentViewRecruit(){
            location.href = "<%=request.getContextPath()%>/views/user_TAEWON/user_recentView.jsp";
         }
         function changeInfo() {
            location.href = "<%=request.getContextPath()%>/views/common/E_UpdateInfoPage1.jsp";
         }
         function del() {
            location.href = "<%=request.getContextPath()%>/views/common/E_DeletePage1.jsp";
         }
         function myresumeBusiness(){
          location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/myresumeBusiness.jsp";
         }
         function resumeLimit() {
          location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/resumeLimit.jsp";
         }
         function userMypage() {
           location.href = "<%=request.getContextPath()%>/user.my";
        }

	      
     </script>
	
</body>
</html>










