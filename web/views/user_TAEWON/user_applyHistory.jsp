<%@page import="oracle.net.aso.b"%>
<%@page import="com.sun.org.apache.regexp.internal.REUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*, com.kh.hbr.apply.model.vo.*, com.kh.hbr.recruit.model.vo.*"%>

<%@ page import="com.kh.hbr.member.model.vo.*" %>
<%@ page import="com.kh.hbr.recruit.model.vo.*" %>
<%@ page import="com.kh.hbr.apply.certification.model.vo.*" %>

<%
	ArrayList<Apply> alist  = (ArrayList<Apply>) session.getAttribute("alist");
	Apply apply = (Apply) session.getAttribute("apply");
	
	Business business = (Business) session.getAttribute("business");
	Member member = (Member) session.getAttribute("member");
	Recruit recruit = (Recruit) session.getAttribute("searchRecruit");
	ArrayList<Certification> clist = (ArrayList<Certification>) session.getAttribute("clist");
	Certification cert = (Certification) session.getAttribute("cert");
	
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
* {
	margin: 0;
	padding: 0;
	/* text-align: left; */
}

button {
	cursor: pointer;
}

/* body {background-color: #fffde7;} */
#wrap {
	width: 1200px;
	margin: 0 auto;
	margin-left: 240px;
}

/* 헤더 영역 */
.logo {
	float: left;
	width: 22%;
	height: 130px;
}

.toplogin {
	width: 100%;
	height: 50px;
	padding: 1%;
}

/* 상단메뉴바 */
.topMenu {
	/* display: inline-block; */
	width: 100%;
	height: 50px;
	/* background: #013252; */
	line-height: 30px; /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	vertical-align: middle; /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	text-align: center; /* 글씨 정렬을 가운데로 설정 */
	font-weight: bolder;
	font-size: 20px;
	color: #013252;
}

.topMenu>div {
	float: right;
}

.topMenu>ul {
	float: left;
	margin: 0;
	padding: 0;
	list-style: none;
}

.topMenu>ul>li {
	display: inline-block;
	padding: 0;
}

.topMenu a {
	display: block;
	text-decoration: none;
	padding: 10px 20px;
	color: black;
}

.topMenu a:hover {
	background: #013252;
	color: white;
}

/* 하이퍼텍스트 효과 */
a {
	text-decoration: none;
} /* 하이퍼텍스트 밑줄 효과 없애기 */

/* 광고 영역 */
.ads {
	width: 100%;
	height: 500px;
	clear: both;
}

/* 공고조회 및 로그인 영역 */
.b2_cotainer {
	width: 100%;
	height: 260px;
	background-color: #F0F0F0;
	text-align: center;
}

#selectJob {
	float: left;
	width: 70%;
	height: 250px;
	background-color: #039be5;
	display: inline-block;
}

#login {
	width: 30%;
	height: 250px;
	background-color: #0288d1;
	display: inline-block;
}

.fsearch_wp {
	width: 100%;
	border: 1px solid #e1e1e1;
	border-bottom: none;
	font-size: 14px;
	box-sizing: border-box;
}

/* 일반채용리스트 */
.normalJobList {
	width: 88%;
	font-size: 20px;
	font-weight: bold;
}

#normaljobList {
	font-size: 16px;
	font-weight: bold;
	border: none;
	border-bottom: 3px;
	border-bottom-color: black;
	background-color: lightGray;
	
}

/* footer 영역 */
.footer {
	width: 100%;
	height: 200px;
	background-color: #ffb300;
	margin-top: 300px;
}

/* 버튼 효과 */
.btns {
	align: center;
}

#loginBtn, #memberJoinBtn, #logoutBtn, #changeInfo, #adminBtn {
	display: inline-block;
	text-align: center;
	background: orangered;
	color: white;
	height: 25px;
	width: 100px;
	border-radius: 5px;
}

#memberJoinBtn, #logoutBtn {
	background: yellowgreen;
}

#loginBtn:hover, #changeInfo:hover, #logoutBtn:hover, #memberJoinBtn:hover,
	#adminBtn:hover {
	cursor: pointer;
}

/* nav */
.nav {
	width: 15%;
	height: 300px;
	border: solid 1px lightgray;
	margin-left: 5px;
}

/* 박다영 스타일 */
/* left 메뉴 */
   .leftblock {
      display: block;
   }
   
   #left2, #left3, #left4 {
      margin-top:10px;
   }
   
   #left1 {
      width: 180px;
      height:900px;
      margin-top:10px;
      align: left;
   }
   
   .myPage {
   	margin-left: 10px;
   }
   
   #left2 {
      height:290px;
   }
   
   #left3 {
      height:260px;
   }
   
   #left4 {
      height:250px;
   }
   
   /* left메뉴 가로선 */
   hr {
      width:90%;
      color:lightgray;
      size:2px;
      margin-left: 5%;
   }
   
   ul {
      list-style:none;
   }

.leftMenu{float: left; width: 20%; height: 580px; margin-left: 30px; margin-top: -28px; text-align: left}

.leftMenu a:hover{
	color: #2FA599;
	font-weight: bold;
}
   
   /* 내용 부분 */
   section{width: 40%; height: 1500px; background-color: #ffca28;}
   
   .leftMenu>ul {       
       margin: 10;
       padding: 0;
       list-style: none;
    }
    .leftMenu a {
       display: block;
       text-decoration: none;
       padding: 10px 20px;
       color: black;
     }
.left {
      border:2px solid rgb(192, 192, 192);
   }

/* .inquiryType {
	display: inline-block;
} */

.applyManage {
	width: 80%;
	height: 60px;
	margin-left: 240px;
	background-color: #013252;
	font-size: 20px;
	color: white;
	line-height: 60px;
	text-align: left;
}


#inquiry {
	display: inline-block;
}

#period {
	display: inline-block;
	width: 50px;
	height: 25px;
	border: 1px solid gray;
	background-color: lightgray;
	/* line-height: 60px; */
}

/* * #inquiryTable {
	
	border-collapse: collapse;
	height: px;
	
} */

#qTable {
	width: 60px;
	height: 80px;
	background-color: #013252;
	font-weight: bolder;
	font-size: 14px;
	color: white;
	border: none;
}

#applyTable {
	width: 76%;
	text-align: center;
	border: 1px solid lightgray;
	border-collapse: collapse;
}

.iqTr{
	background-color: lightgray;
	font-weight: bold;
}

.iqTr2{
	border-bottom: 1px solid lightgray;
}

.sectionWrap {
	text-align: left;
}

/* 취업활동증명서 페이지 모달 */
.searchModal {
	display: none; /* Hidden by default */
	position: fixed; /* Stay in place */
	z-index: 10; /* Sit on top */
	left: 0;
	top: 0;
	width: 100%; /* Full width */
	height: 100%; /* Full height */
	overflow: auto; /* Enable scroll if needed */
	background-color: rgb(0,0,0); /* Fallback color */
	background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

.search-modal-content {
	background-color: #fefefe;
	margin: 15% auto; /* 15% from the top and centered */
	padding: 5px;
	border: 3px solid #888;
	width: 800px;  /* Could be more or less, depending on screen size */
	height: 100%;
}

</style>

</head>
<body>
	<%@ include file="../common/E_user_menubar.jsp" %>
	
	<hr style="border: solid 2px #013252; width:100%; margin:0 auto"><br>
	<br>
	
	<!-- 전체영역 -->
	<div id="wrap">
		
		<!-- 사이트경로 -->
	    <p style="margin-left: 240px; text-align: left">HOME > 마이페이지 > 입사지원관리 > 입사지원목록</p>

		<!-- 레프트메뉴바 -->
		   <aside class="leftMenu">
              <div class="left leftblock" id="left1"><br>&nbsp;
              <label class="myPage" onclick="userMypage();" style="font-size:20px;"><b>마이페이지</b></label><br><br>
              <hr>
              <br>
                 <ul>
                 <li>
                       <a href="#" style="font-size:18px;"><b>이력서관리</b></a>
                     <a href="#" onclick="resumeWrite();" style="font-size:15px;">이력서 등록</a>
                     <a href="#" onclick="resumeManage();" style="font-size:15px;">이력서 현황</a>
                  </li><br>
                  <hr><br>
                  <li>
                     <a href="#" style="font-size:18px;"><b>입사지원 관리</b></a>
                     <a href="#" onclick="applyHistory();" style="font-size:15px; color: #2FA599; font-weight: bold;">입사지원목록</a>
                     <a href="#" onclick="myresumeBusiness();"style="font-size:15px;">내 이력서 열람기업</a>
                     <a href="#" onclick="resumeLimit();"style="font-size:15px;">이력서 열람 제한</a>
                  </li><br>
                <hr><br>
                <li>
                   <a href="#" onclick="recruitBookmark();" style="font-size:18px;"><b>스크랩보금자리</b></a>
                </li><br>
                <hr><br>
                <li> 
                   <a href="#" onclick="recentViewRecruit();" style="font-size:18px;"><b>최근 본 보금자리</b></a>
                </li><br>
                <hr><br>
                <li><a href="#" style="font-size:18px;"><b>회원정보 관리</b></a>
                    <a href="#" onclick="changeInfo()" style="font-size:15px;">회원정보수정</a>
                    <a href="#" onclick="del();" style="font-size:15px;">회원탈퇴</a>
                </li><br>
                <hr><br>
                <li><a href="#" onclick="myinquiry();" style="font-size:18px;"><b>나의 문의내역</b></a>
                </li>
              </ul>  
              </div>
         </aside><br>

			 
		<!-- 메인 -->
		<div class="sectionWrap">
		<div class="section">	
			<div class="applyManage" style="font-weight: bold">입사지원 관리</div><br>
			<table>
				<tr>
					<td style="font-weight: bold">입사지원 목록 | 총 <p></p></td>
					<td style="font-weight: bold;"><div style="float:left; color:red"><%= alist.size()%></div>건</td>
				</tr>
			</table><br><br>


			<!--  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ -->
	        <!-- 취업활동 증명서 모달 -->
	        <div id="wrap">
	        	<div id="modal" class="searchModal" style="border:2px solid gray; width: 100%; /* height: 600px; */ margin:0 auto">
	        		<div class="search-modal-content" style="border: 4px solid gray">
	        			<table style="padding:10px; width: 100%">
	        				<tr>
	        					<td id="currentYear" style="width: 50%; font-weight: bold; vertical-align: top; float:left"></td>
	        					<td style="width: 50%; text-align:right">
	        						<img src="../../static/images/user/logo.png"; style="width: 35%; hegith:35%">
	        					</td>
	        				</tr>
	        			</table><br><br>
	        			<div style="font-size: 40px; font-weight: bold; text-align:center">취업활동 증명서</div>
	        			<br><br><br>
	        			<table id="memberInfo" style="border: 1px solid gray; width: 95%; margin:0 auto; border-collapse: collapse">
	        				<tr style="height:30px; ">
	        					<td style="background: #d3d3d3; padding-left:20px; font-weight:bold; width:25%;">성명</td>
	        					<td style="padding-left:20px"></td>
	        				</tr>
	        				<tr style="height:30px">
	        					<td style="background: #d3d3d3; padding-left:20px; font-weight:bold">연락처</td>
	        					<td style="padding-left:20px"></td>
	        				</tr>
	        			</table><br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;
	        			<p id="pName" style="font-weight:bold; margin-left:20px"> </p>
	        			<table id="certInfo" style="border: 1px solid gray; width: 95%; margin:0 auto; border-collapse: collapse; text-align: center; margin-top:10px">
	        				<thead>
	        				<tr style="height: 40px; background: #d3d3d3">
	        					<th width="10%">입사지원일</th>
	        					<th width="10%">기업명</th>
	        					<th width="20%">주소</th>
	        					<th width="15%">연락처</th>
	        					<th width="10%">지원방법</th>
	        					<th width="10%">비고</th>
	        				</tr>
	        				</thead>
	        				<tbody>
	        				</tbody>
	        			</table><br><br>
	        			
	        			<div style="font-weight: bold">
	        				<p id="" style="float:left">&nbsp;&nbsp;&nbsp;&nbsp;</p>
	        					        				
	        				<p id="work">&nbsp;&nbsp;</p>
	        				
	        				<p><b>&nbsp;&nbsp;&nbsp;&nbsp;(주)해볼래 온라인 입사지원시스템을 통해 위 기업에 입사지원하였음을 증명합니다.</b></p>
	        				
	        			</div><br><br><br><br><br>
	        			<div>
	        				<table style="width:95%; margin:0 auto;">
	        					<tr>
	        						<td style="width: 10%;"></td>
	        						<td style="width: 40%; text-align: right; font-size:16px; font-weight:bold">
	        							<p>중장년층 전문 구인구직사이트 </p>
	        						</td>
	        						<td style="width: 12%; text-align: left; font-size:30px; font-weight:bold">
	        							<p>해볼래</p>
	        						</td>
	        						<td style="width: 2%"></td>
	        					</tr>
	        				</table>
	        				<p style="font-size: 16px; font-weight: bold; text-align:right; margin-right:110px;">(주)해볼래</p>
	        				<img src="../../static/images/user/hbr_signature.png" style="width:200px; height:200px; margin-left:600px; margin-top:-960px; margin-bottom:-40px">
	        			</div><br><br><br>
		        		<div style="margin-left:300px">
				        	<button style="width: 100px; height: 40px; background: #013252; 
				        	color:white; font-weight: bold; font-size: 16px; border:none;" onclick="certPrint();">인쇄</button>
				        	<button style="width: 100px; height: 40px; background: #013252; 
				        	color:white; font-weight: bold; font-size: 16px; border:none;" onclick="certClose()">닫기</button>
		        		</div><br><br><br>
	        		</div>
	        	</div>
	        	
	        	<%-- <% } %> --%>
	        </div>
	        <!-- 취준활동증명서 End -->


			<table id="inquiryTable" style="float:left">
				<tr>
					<td colspan="2" width="85px" height="60px">
					<img src="../../static/images/user/applyHistory.png" width="80px" height="80px"></td>
					<td><p style="font-size:14px">
							• <b>최근 2년간의 지원내역</b>에 대한 확인이 가능합니다. (단, 지원서류는 최근 2개월 동안 확인 가능)<br>
							• 지원취소는 최근 1년간의 지원에 대해서만 가능하며, 공고가 삭제된 경우 지원취소를 제공하지 않습니다.
						</p>
					</td>
				</tr>
			</table><br><br>
			
			<div>
				<form id="searchForm" action="<%= request.getContextPath()%>/search.ap" method="get">
					<table width="930px" height="100px" style="border:1px solid lightgray; margin-top: 80px">
						<tr>
							<td colspan="1" rowspan="2" width="80px" height="80px" style="font-weight: bold; text-align:center">
							검색조건<br><p style="font-size:10px; text-align:center">(최근 2년)<p></td>
							<td colspan="1" width="400px">
								<label style="font-size:14px">조회기간</label>
								<button id="period" style="font-size:12px">1주일</button>
								<button id="period" style="font-size:12px">1개월</button>
								<button id="period" style="font-size:12px">2개월</button>
								<button id="period" style="font-size:12px">3개월</button>
								<button id="period" style="font-size:12px">6개월</button>
								<button id="period" style="font-size:12px">1년</button>
							</td>
							<td colspan="2">
								<input type="date" style="height:30px">
								 ~ <input type="date" style="height:30px">
							</td>
							<td rowspan="2">
								<button type="submit" id="qTable" style="font-weight: bold">검색</button>
							</td>
							
						</tr>
						<tr>
							<td colspan="2" width="120px">
								<label style="font-size:14px; height:25px">마감여부</label>
								<select style="width:80px; height:25px">
									<option id="expire">전체</option>
									<option id="expire">마감</option>
									<option id="expire">게재중</option>
								</select> &nbsp;&nbsp;
								
								
								<label style="font-size:14px; height:25px">열람여부</label>
								<select name="openYn" style="width:80px; height:25px">
									<option id="open" selected value="">전체</option>
									<option id="open" value="Y">열람완료</option>
									<option id="open" value="N">미열람</option>
								</select> &nbsp;&nbsp;
								
								
								<label style="font-size:14px; height:25px">지원상태</label>
								<select name="cancelYn" style="width:80px; height:25px">
									<option id="applyStatus" selected value="">전체</option>
									<option id="applyStatus" value="applied">지원완료</option>
									<option id="applyStatus" value="canceled">지원취소</option>
									<option id="applyStatus" value="interview">면접요청</option>
								</select> &nbsp;&nbsp;
								<label style="font-size:14px">검색어</label>
								<input type="search" placeholder="검색할 내용을 입력하세요." style="width:210px; height:25px">
							</td>
						</tr>
					</table><br><br><br>
				</form>
			</div>
	
	
	
	
				<div style="border:1px solid lightgay" align="center">
				<table id="applyTable">
					<tr class="iqTr">
						<th id="applyList" width="5%" height="40px">선택</th>
						<th id="applyList" width="10%">지원일</th>
						<th id="applyList" width="15%">회사명</th>
						<th id="applyList" width="35%">보금자리 제목</th>
						<th id="applyList" width="10%">열람여부</th>
						<th id="applyList" width="15%">마감일</th>
						<th id="applyList" width="15%">지원상태</th>
					</tr>
					
						<% for (Apply a : alist) { %>
					
						<tr style="border-bottom: 1px solid #d3d3d3" height="40">
							<td id="checkApply"><input type="checkbox" name="certCheck"></td>
							<td><%= a.getApplyEnrollDate() %></td>
							<td><%= a.getCompanyName() %></td>
							<td><%= a.getRecruitTitle() %></td>
							
							<% if(a.getOpenYn().equals("Y")) { %>
							<td style="margin: 0 auto">
								<p style="font-weight: bold">열람완료<p>
							</td>
							<% } else { %>
							<td style="color:gray">미열람</td>
							<% } %>
							
							<td><%= a.getExpirationDate() %>
								<input type="hidden" id="applyNo" name="applyNo" value="<%= a.getApplyNo()%>">
							</td>
							<% if(a.getCancelYn().equals("APPLIED")) { %>
							<td style="font-weight: bold">지원완료</td>
							<% } else if(a.getCancelYn().equals("CANCELED")) { %>
							<td style="color:red">지원취소</td>
							<% } else { %>
							<td style="color:blue;">면접요청</td>
							<% } %>
						</tr>
						
						<% } %>
					
					
				</table><br>
				</div><br>
			</div>
			
			
			
			
			
			<div class="pagingArea" align="center">
	    <!--     
	        페이지 처리 
	         <button> << </button>
	         <button> < </button>1 페이지
	         <button> > </button>
	         <button> >> </button>
	         페이지처리 End
	          -->
	         
	         <br>
	         
	         <div align="right" style="margin-right: 20px">
		         <button style="width:100px; height:40px; background-color: #013252; color:white; font-size:14px; border:none; font-weight: bold"
		         onclick="deleteApply();">삭제</button>
		         <button style="width:100px; height:40px; background-color: #013252; color:white; font-size:14px; border:none; font-weight: bold" 
		         onclick="cancelApply();">지원취소</button>
		         <button style="width:160px; height:40px; background-color: #013252; color:white; font-size:14px; border:none; font-weight: bold"
		         class="OpenCertPage" onclick="OpenCertPage();">취업활동증명서 발급</button>
	         </div>
	        </div>
	         
	         
	         
	         <br>
	         
		</div>
		<br><br><br><br><br><br><br><br>		
		
		<%@ include file="../guide/footer.jsp" %>
	</div>
	
	
	
	<script>
	
	//취업활동증명서 페이지 열기
	
	function OpenCertPage() {
		
		var manyCheck = $("input[name=certCheck]:checked").length;
		
		if(manyCheck >= 2) {
			
			$(".searchModal").css("style", "display:none");
			alert("하나의 내역만 선택해주세요.");
			
		}
		
		
		
		
		
		if($("input").is(":checked") == true) {
			
		var applyNo = "";
		<%-- var recuitNo = "<%= recruit.getRecruitNo()%>"; --%>
		
		$("#applyTable tr").each(function(index) {
			
			if(index !== 0) {
				var a = $(this).children().eq(0).children().is(":checked");
				
				if(a === true) {
					applyNo += $(this).children().eq(5).children().val();
				}
			}
		});

		console.log("applyNo : " + applyNo);
			
		$(".searchModal").show();
	
		} else {
			alert("원하시는 입사지원내역을 선택해주세요.")
		}
			
		
		
		$.ajax({
			url: "<%= request.getContextPath()%>/insert.certNo",
			data: {
				applyNo: applyNo
			},
			type: "post",
			success: function(data) {
				
				console.log(data);
				
				//date 표시
			  	var now = new Date();
			    var year = now.getFullYear();
			    var mon = (now.getMonth()+1)>9 ? ''+(now.getMonth()+1) : '0' + (now.getMonth()+1);
			    var day = now.getDate()>9 ? ''+now.getDate() : '0' + now.getDate();
				
				//certNo 삽입
				var certNo = data[0].certNo;
				var certNum = " 제 " + year + " - " + certNo + " 호";
				console.log("certNum : " + certNum);
				var certNum2 = $("#currentYear").text(certNum);
	
				
				var memberName = data[0].memberName;
				//멤버이름 삽입
				var a = $("#memberInfo").children().children().eq(0).children().eq(1).text(memberName);
				console.log(a);
				
				//아래 이름 삽입
				var name = memberName + "님의 취업활동 현황입니다.";
				var name2 = $("#pName").text(name);
				
				//전화번호 삽입 : split 해야 함 !
				var phone = data[0].phone;
				
				var phone0 = phone.substring(0, 3);
				var phone1 = phone.substring(3, 7);
				var phone2 = phone.substring(7, 11);
				
				var phoneNum = phone0 + "-" + phone1 + "-" + phone2;
				console.log("phoneNum : " + phoneNum);
				var phoneNum2 = $("#memberInfo").children().children().eq(1).children().eq(1).text(phoneNum);
								
				
				//두 번째 테이블 항목 변수들
				var applyEnrollDate = data[0].applyEnrollDateYear;
				var companyName = data[0].companyName;
				var detailArea = data[0].DetailArea;
				var companyPhone = data[0].companyPhone;
					
				//td를 만들어서 tr에 붙이고 -> tr을 table에 붙여야 한다.
				var certTr = $("<tr>");
				var ctEnrollDate = $("<td>").text(applyEnrollDate);
				var ctCompanyName = $("<td>").text(companyName);
				var ctDetailArea = $("<td>").text(detailArea);
				var ctCompanyPhone = $("<td>").text(companyPhone);
				var ctApplyType = $("<td>").text("온라인");
				var ctETC = $("<td>").text("");
					
				//tr에 append를 함 -> td를 붙여주는 작업
				certTr.append(ctEnrollDate);
				certTr.append(ctCompanyName);
				certTr.append(ctDetailArea);
				certTr.append(ctCompanyPhone);
				certTr.append(ctApplyType);
				certTr.append(ctETC);
				
				//tr을 table에 붙여줌
				$("#certInfo tbody").append(certTr);
			            
				
			    var currentDate = year + '-' + mon + '-' + day;
			    /* $(this).val(chan_val); */
			    /* console.log("currentDate : " + currentDate); */
	
				//하단 기간 문구
				var work = memberName + "님은 " + applyEnrollDate + " 부터 " + currentDate + " 까지";
				var work2 = $("#work").text(work);
					
				
			},
			error: function(error) {
				console.log(error);
			}
		});
		
	}
		
	
	
	//취업활동증명서 모달 닫기
	function certClose() {
		$(".searchModal").hide();
	}
	
	//입사지원내역 삭제하기(체크박스)
	function deleteApply() {
		
		var deleteApply = $("input[name=certCheck]:checked").length;
		var deleteApplyNo = "";
		
		if(deleteApply <= 0) {
			alert("원하시는 입사지원내역을 선택해주세요.");
		} else {
			
			var result = confirm("선택하신 입사지원내역을 삭제하시겠습니까?");
			
			if(result) {
				
				$("input[name=certCheck]:checked").each(function(){
					deleteApplyNo += $(this).parent().parent().children().eq(5).children().val() + "#";
					console.log("deleteApplyNo : " + deleteApplyNo);

				});
				
					alert("삭제되었습니다.");
					location.href = "<%= request.getContextPath()%>/update.ap?deleteApplyNo=" + encodeURIComponent(deleteApplyNo);
			}

		}
		
	}
	
	//입사지원 취소하기 cancel
	function cancelApply() {
		
		var cancelApply = $("input[name=certCheck]:checked").length;
		var cancelApplyNo = "";
		
		if(cancelApply <= 0) {
			alert("원하시는 입사지원내역을 선택해주세요.");
		} else {
			
			var result = confirm("입사지원취소를 하시면 해당기업에서 이력서 열람이 불가능 합니다.\n입사지원을 취소하시겠습니까?");
			
			if(result) {
				
				$("input[name=certCheck]:checked").each(function(){
					cancelApplyNo += $(this).parent().parent().children().eq(5).children().val() + "#";
					console.log("cancelApplyNo : " + cancelApplyNo);

				});
				
					alert("입사지원이 취소되었습니다.");
					location.href = "<%= request.getContextPath()%>/cancel.ap?cancelApplyNo=" + encodeURIComponent(cancelApplyNo);
			}

		}
		
	}
	
	
	//취업활동증명서 인쇄하기
	function certPrint() {
		window.print();
		return false;
	}
	
	
	
	
	
	
	</script>







	 
	<script>
	//페이지 이동
    function resumeWrite() {
          location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/resumeWrite.jsp";
          }
        function resumeManage() {
           location.href = "<%=request.getContextPath()%>/res.list";
        }
        function recruitBookmark() {
           location.href = "<%=request.getContextPath()%>/views/user_TAEWON/user_recruitBookmark.jsp";
        }
        function myinquiry(){
           <%-- location.href = "<%=request.getContextPath()%>/select.iq"; --%>
           location.href = "<%=request.getContextPath()%>/views/user_TAEWON/user_MyInquiryHistory.jsp";
        }
        function applyHistory(){
           location.href = "<%=request.getContextPath()%>/selectApplyList.ap";
        }
        function recentViewRecruit(){
           location.href = "<%=request.getContextPath()%>/views/user_TAEWON/user_recentView.jsp";
        }
        function changeInfo() {
           location.href = "<%=request.getContextPath()%>/views/common/E_UpdateInfoPage1.jsp";
        }
        function del() {
           location.href = "<%=request.getContextPath()%>/views/common/E_DeletePage1.jsp";
        }
        function myresumeBusiness(){
         location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/myresumeBusiness.jsp";
        }
        function resumeLimit() {
         location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/resumeLimit.jsp";
        }
        function userMypage() {
          
          location.href = "<%=request.getContextPath()%>/user.my";
       }
	      
    </script>
	
	
</body>
</html>












