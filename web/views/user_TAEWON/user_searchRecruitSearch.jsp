<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*, com.kh.hbr.recruit.model.vo.*"%>

<%@ page import="java.text.DecimalFormat" %>
<%@ page import="com.kh.hbr.recruit.model.vo.*" %>
<%@ page import = "com.kh.hbr.resume.model.vo.Resume" %>

<%
	ArrayList<Recruit> slist = (ArrayList<Recruit>) request.getAttribute("list");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
* {
	margin: 0;
	padding: 0;
	text-align: left;
}
/* body {background-color: #fffde7;} */ 
#wrap {
	width: 1200px;
	margin: 0 auto;
}

/* 헤더 영역 */
.logo {
	float: left;
	width: 22%;
	height: 130px;
}

.toplogin {
	width: 100%;
	height: 50px;
	padding: 1%;
}

/* 상단메뉴바 */
.topMenu {
	/* display: inline-block; */
	width: 100%;
	height: 50px;
	/* background: #013252; */
	line-height: 30px; /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	vertical-align: middle; /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	text-align: center; /* 글씨 정렬을 가운데로 설정 */
	font-weight: bolder;
	font-size: 20px;
	color: #013252;
}

.topMenu>div {
	float: right;
}

.topMenu>ul {
	float: left;
	margin: 0;
	padding: 0;
	list-style: none;
}

.topMenu>ul>li {
	display: inline-block;
	padding: 0;
}

.topMenu a {
	display: block;
	text-decoration: none;
	padding: 10px 20px;
	color: black;
}

.topMenu a:hover {
	background: #013252;
	color: white;
}

/* 하이퍼텍스트 효과 */
a {
	text-decoration: none;
} /* 하이퍼텍스트 밑줄 효과 없애기 */

/* 광고 영역 */
.ads {
	width: 100%;
	height: 500px;
	clear: both;
}

/* 공고조회 및 로그인 영역  */
.b2_cotainer {
	width: 100%;
	height: 260px;
	background-color: #F0F0F0;
	text-align: center;
}

#selectJob {
	float: left;
	width: 70%;
	height: 250px;
	background-color: #039be5;
	display: inline-block;
}

#login {
	width: 30%;
	height: 250px;
	background-color: #0288d1;
	display: inline-block;
}

/* 채용정보 전체 div */
.iqWrap {
	width: 1200px;
	/* height: 800px; */
	border: 1px solid lightgray;
	margin: 0 auto;
	/* background: #ECF0F7; */
}

.iqLocation, .seouldiv {
	width: 1130px;
	/* height: 40px; */
	/* border: 1px solid red; */
	margin: 0 auto;
	margin-top: 1px;
}

.iqJob {
	width: 1120px;
	height: 500px;
	border: 1px solid red;
	margin: 0 auto;
	/* margin-top: 10px; */
}

.trLocation {
	align: center;
}

/* 광역시도 */
.tdLocation {
	height: 25px;
	width: 58px;
	border: 1px solid gray;
	text-align: center;
	margin: 1px;
	line-height: 28px;
	cursor: pointer;
}

.tdLocation:hover {
	background:#013252;
	color:white;
}


/* 지역 선택버튼 */
.seoul2, .gyoengi2, .inchoen2, .busan2, .daegu2, .gwangju2, .daejeon2,
	.ulsan2, .sejong2, .gangwon2, .kyeoungnam2, .kyeoungbuk2, .jeonnam2,
	.jeonbuk2, .chungnam2, .chungbuk2, .jeju2 {
	border: 1px solid gray;
	text-align: center;
	height: 25px;
	width: 126px;
	background-color: white;
	float: left;
	margin: 6px;
	line-height: 28px;
	cursor: pointer;
}

.seoul2:hover, .gyoengi2:hover, .inchoen2:hover, .busan2:hover, .daegu2:hover, .gwangju2:hover, .daejeon2:hover,
	.ulsan2:hover, .sejong2:hover, .gangwon2:hover, .kyeoungnam2:hover, .kyeoungbuk2:hover, .jeonnam2:hover,
	.jeonbuk2:hover, .chungnam2:hover, .chungbuk2:hover, .jeju2:hover {
	background:#013252;
	color:white;
	
}

/* 지역선택 div영역 */
	#seouldiv, #gyoengidiv, #inchoendiv, #busandiv, #daegudiv, #gwangjudiv,
	#daejeondiv, #ulsandiv, #sejongdiv, #gangwondiv, #kyeoungnamdiv,
	#kyeoungbukdiv, #jeonnamdiv, #jeonbukdiv, #chungnamdiv, #chungbukdiv,
	#jejudiv {
	width: 1130px;
	/* height: 160px; */
	border: 1px solid #8D8D8D;
	margin-left: 34px;
	background-color: lightgray;
}

/* 직종 선택버튼  */
.kindsJobTop {
	height: 25px;
	width: 111px;
	border: 1px solid gray;
	text-align: center;
	font-size: 15px;
	background-color: white;
	/* margin: 0.1px; */
	float: left;
	vertical-align: middle;
	line-height: 28px;
	cursor: pointer;
}

.kindsJobTop:hover {
	background:#013252;
	color:white;
}

#kindsJob {
	width: 1130px;
	margin-left: 34px;
	/* border: 1px solid red; */
	/* background-color: lightgray; */
}

.cooking2, .medical2, .construct2, .office2, .driving2, .sales2,
	.storeManger2, .teacher2, .etc2 {
	border: 1px solid gray;
	text-align: center;
	height: 25px;
	width: 126px;
	background-color: white;
	float: left;
	margin: 6px;
	line-height: 26px;
	cursor: pointer;
}

.cooking2:hover, .medical2:hover, .construct2:hover, .office2:hover, .driving2:hover, .sales2:hover,
	.storeManger2:hover, .teacher2:hover, .etc2:hover {
	background:#013252;
	color:white;
}


/* 직종선택 div영역 */
/* #kindsJob,  */
#cookingDiv, #medicalDiv, #constructDiv, #officeDiv, #drivingDiv,
	#salesDiv, #storeMangerDiv, #teacherDiv, #etcDiv {
	width: 1130px;
	border: 1px solid #8D8D8D;
	margin-left: 34px;
	background-color: #d3d3d3;
	
}
/* 지역 div영역 END */

/* 지역, 업종 선택 검색버튼*/
.searchBtn {
	width:150px;
	height: 50px;
	font-size: 18px;
	background-color:#013252;
	color: white;
	font-weight: bold;
	border: none;
	/* margin: 0 auto; */
	text-align: center;
	margin-left: 520px;
}



/* 일반채용 리스트 테이블*/
#tableTr1 {
	background-color: lightgray;
	text-align: center;
}

#tableTr2 {
	border-bottom: 1px solid lightgray;
	text-align: center;
}

#underListTab {
	width: 100%;
	border: 1px solid lightgray;
	border-collapse: collapse;
	text-align: center;
}

.tableTh {
	text-align: center;
}

.salary {
	float:left;
	background:#ECF0F7;
	text-align:center; 
	width: 40px;
	border:1px solid darkgray;
	border-radius: 5px;
}


/* 일반채용 리스트 테이블 END*/

#iqdiv {
	height: 40px;
	margin: 0 auto;
}

/* footer 영역 */
footer {
	width: 100%;
	height: 200px;
	background-color: #ffb300;
}

/* 버튼 효과 */
.btns {
	align: center;
	margin-left: 1000px;
}

#loginBtn, #memberJoinBtn, #logoutBtn, #changeInfo, #adminBtn {
	display: inline-block;
	text-align: center;
	background: orangered;
	color: white;
	height: 25px;
	width: 100px;
	border-radius: 5px;
}

#memberJoinBtn, #logoutBtn {
	background: yellowgreen;
}

#loginBtn:hover, #changeInfo:hover, #logoutBtn:hover, #memberJoinBtn:hover,
	#adminBtn:hover {
	cursor: pointer;
}

/* 선택 지역, 업종 출력*/
#printArea {
	width: 1200px;
	height: 100px;
	border: 1px solid lightgray;
	margin: 0 auto;
}

.printArea2 {
	width: 100%;
	height: 100%;
	background: #ECF0F7;
}

#printLo, #printJob {
	width: 150px;
	height: 20px;
	/* border: 1px solid gray; */
	/* border: none; */
	border: 2px solid #013252;
	border-radius: 10px; 
	background: white;	
	/* margin-left: 130px; */
	padding: 10px;
	text-align: center;
	font-weight: bold;
	color: black;
	line-height: 20px;
	
}



/* 반짝 보금자리 공고 영역 */
.section {
	overflow: hidden;
	padding: 20px;
	width: 1160px;
	margin-top: 0px;
	border: 1px solid lightgray;
	/* border-top: 1px; */
	/* border-bottom: 1px; */
}

.item1 {
	width: 265px;
	margin: 0;
	height: 240px;
	padding: 10px; /* background-color: #ff9800; */
	border: 1px solid #66B2FF;
	float: left;
}

.item2 {
	width: 265px;
	margin: 0;
	height: 210px;
	padding: 10px; /* background-color: #ff9800; */
	border: 1px solid #AFAFAF;
	float: left;
}

.item3 {
	width: 265px;
	margin: 0;
	height: 150px;
	padding: 10px; /* background-color: #ff9800; */
	border: 1px solid #AFAFAF;
	float: left;
}

.item_gold {
	width: 265px;
	height: 110px;
}

.item_silver {
	width: 265px;
	height: 90px;
}

.bName {
	font-size: 17px;
	margin-bottom: 5px;
}

.bTitle {
	font-size: 22px;
	margin-bottom: 10px;
}

.bPeriods {
	font-size: 15px;
	color: gray;
	float: left;
}

.bScrap {
	float: right;
}

.scrap_img {
	width: 20px;
	height: 20px;
}


.getRecruitNo{
	display: none;
}

/* 진규 추가 */
/* td.clickBtn {background:#013252; color:white; } */
.gu.clickGu {background-color:#013252; color:white; }

</style>
<!-- <script>
	$(document).ready(function() {
		$('.slider').bxSlider();
	});
</script> -->
</head>
<body>
	<%@ include file="../common/E_user_menubar.jsp" %>
	
	
	<!-- 주소 문자열 끊어주기 -->
	<%
		String[] address = new String[slist.size()];
	
		for(int i = 0; i < address.length; i++) {
			if(slist.get(i).getDetailArea() != null) {
				address[i] = slist.get(i).getDetailArea();
			}
		}	
		
		String a = address[0];
	 %>


	<!-- 전체영역 -->
	<div id="wrap">
		
		<hr style="border: solid 2px #013252; width:100%; margin:0 auto"><br>
		<br>
		<!-- 사이트경로 -->
	    <p style="text-align:left">HOME > 채용정보</p><br><br>
		
		
		<!-- 서치박스 -->
		<p style="font-size: 22px; text-align:left"><b>채용정보</b><p>
		
		<!-- <table class="top">
			<tr>
				<td>채용정보</td>
				<td style="align:right"><input type="search" placeholder="어떤 일자리를 찾고 계신가요?" width="250px"></td>
			<tr>
		</table> -->
		
		<br>
		<div class="iqWrap">
		
			<p style="margin-left: 40px; margin-top: 30px; margin-bottom: 15px"><b>지역선택</b></p>
			<div id="iqdiv">
				<table class="iqLocation">
					<tr class="trLocation">
						<td><div class="tdLocation gu">전체</div></td>
						<td><div class="tdLocation gu" id="seoul">서울</div></td>
						<td><div class="tdLocation gu" id="gyoengi">경기</div></td>
						<td><div class="tdLocation gu" id="inchoen">인천</div></td>
						<td><div class="tdLocation gu" id="busan">부산</div></td>
						<td><div class="tdLocation gu" id="daegu">대구</div></td>
						<td><div class="tdLocation gu" id="gwangju">광주</div></td>
						<td><div class="tdLocation gu" id="daejeon">대전</div></td>
						<td><div class="tdLocation gu" id="ulsan">울산</div></td>
						<td><div class="tdLocation gu" id="sejong">세종</div></td>
						<td><div class="tdLocation gu" id="gangwon">강원</div></td>
						<td><div class="tdLocation gu" id="kyeoungnam">경남</div></td>
						<td><div class="tdLocation gu" id="kyeoungbuk">경북</div></td>
						<td><div class="tdLocation gu" id="jeonnam">전남</div></td>
						<td><div class="tdLocation gu" id="jeonbuk">전북</div></td>
						<td><div class="tdLocation gu" id="chungnam">충남</div></td>
						<td><div class="tdLocation gu" id="chungbuk">충북</div></td>
						<td><div class="tdLocation gu" id="jeju">제주</div></td>
					</tr>
				</table>
			</div>
			<div id="seouldiv" class="metro" style="height: 160px">
				<div class="seoul">
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="전체">전체</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="강남구">강남구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="강동구">강동구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="강북구">강북구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="강서구">강서구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="관악구">관악구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="광진구">광진구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="구로구">구로구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="금천구">금천구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="노원구">노원구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="도봉구">도봉구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="동대문구">동대문구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="동작구">동작구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="마포구">마포구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="서대문구">서대문구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="서초구">서초구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="성동구">성동구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="성북구">성북구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="송파구">송파구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="양천구">양천구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="영등포구">영등포구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="용산구">용산구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="은평구">은평구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="종로구">종로구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="중구">중구</div>
					<div class="seoul2 gu"><input type="hidden" name="seoul" value="중랑구">중랑구</div>
				</div>
			</div>
			<div id="gyoengidiv" class="metro" style="height: 160px">
				<div class="gyoengi">
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="전체">전체</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="가평군">가평군</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="고양시">고양시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="과천시">과천시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="광명시">광명시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="광주시">광주시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="구리시">구리시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="구리시">군포시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="김포시">김포시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="남양주시">남양주시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="동두천시">동두천시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="부천시">부천시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="성남시">성남시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="수원시">수원시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="시흥시">시흥시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="안산시">안산시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="안성시">안성시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="안양시">안양시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="양주시">양주시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="양평군">양평군</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="여주시">여주시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="연천군">연천군</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="오산시">오산시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="용인시">용인시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="의왕시">의왕시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="의정부시">의정부시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="이천시">이천시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="파주시">파주시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="평택시">평택시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="포천시">포천시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="하남시">하남시</div>
					<div class="gyoengi2 gu"><input type="hidden" name="gyoengi" value="화성시">화성시</div>
				</div>
			</div>
			<div id="inchoendiv" class="metro" style="height: 80px">
				<div class="inchoen">
					<div class="inchoen2 gu"><input type="hidden" name="inchoen" value="전체">전체</div>
					<div class="inchoen2 gu"><input type="hidden" name="inchoen" value="강화군">강화군</div>
					<div class="inchoen2 gu"><input type="hidden" name="inchoen" value="계양구">계양구</div>
					<div class="inchoen2 gu"><input type="hidden" name="inchoen" value="남동구">남동구</div>
					<div class="inchoen2 gu"><input type="hidden" name="inchoen" value="동구">동구</div>
					<div class="inchoen2 gu"><input type="hidden" name="inchoen" value="미추홀구">미추홀구</div>
					<div class="inchoen2 gu"><input type="hidden" name="inchoen" value="부평구">부평구</div>
					<div class="inchoen2 gu"><input type="hidden" name="inchoen" value="서구">서구</div>
					<div class="inchoen2 gu"><input type="hidden" name="inchoen" value="연수구">연수구</div>
					<div class="inchoen2 gu"><input type="hidden" name="inchoen" value="옹진군">옹진군</div>
					<div class="inchoen2 gu"><input type="hidden" name="inchoen" value="중구">중구</div>
				</div>
			</div>
			<div id="busandiv" class="metro" style="height: 120px">
				<div class="busan">
					<div class="busan2 gu"><input type="hidden" name="busan" value="전체">전체</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="강서구">강서구</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="금정구">금정구</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="기장군">기장군</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="남구">남구</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="동구">동구</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="동래구">동래구</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="부산진구">부산진구</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="북구">북구</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="사상구">사상구</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="사하구">사하구</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="서구">서구</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="수영구">수영구</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="연제구">연제구</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="영도구">영도구</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="중구">중구</div>
					<div class="busan2 gu"><input type="hidden" name="busan" value="해운대구">해운대구</div>
				</div>
			</div>
			<div id="daegudiv" class="metro" style="height: 80px"> 
				<div class="daegu">
					<div class="daegu2 gu"><input type="hidden" name="daegu" value="전체">전체</div>
					<div class="daegu2 gu"><input type="hidden" name="daegu" value="남구">남구</div>
					<div class="daegu2 gu"><input type="hidden" name="daegu" value="달서구">달서구</div>
					<div class="daegu2 gu"><input type="hidden" name="daegu" value="달성군">달성군</div>
					<div class="daegu2 gu"><input type="hidden" name="daegu" value="동구">동구</div>
					<div class="daegu2 gu"><input type="hidden" name="daegu" value="북구">북구</div>
					<div class="daegu2 gu"><input type="hidden" name="daegu" value="서구">서구</div>
					<div class="daegu2 gu"><input type="hidden" name="daegu" value="수성구">수성구</div>
					<div class="daegu2 gu"><input type="hidden" name="daegu" value="중구">중구</div>
				</div>
			</div>
			<div id="gwangjudiv" class="metro" style="height: 40px">
				<div class="gwangju">
					<div class="gwangju2 gu"><input type="hidden" name="gwangju" value="전체">전체</div>
					<div class="gwangju2 gu"><input type="hidden" name="gwangju" value="광산구">광산구</div>
					<div class="gwangju2 gu"><input type="hidden" name="gwangju" value="남구">남구</div>
					<div class="gwangju2 gu"><input type="hidden" name="gwangju" value="동구">동구</div>
					<div class="gwangju2 gu"><input type="hidden" name="gwangju" value="북구">북구</div>
					<div class="gwangju2 gu"><input type="hidden" name="gwangju" value="서구">서구</div>
				</div>
			</div>
			<div id="daejeondiv" class="metro" style="height: 40px">
				<div class="daejeon">
					<div class="daejeon2 gu"><input type="hidden" name="daejeon" value="전체">전체</div>
					<div class="daejeon2 gu"><input type="hidden" name="daejeon" value="대덕구">대덕구</div>
					<div class="daejeon2 gu"><input type="hidden" name="daejeon" value="동구">동구</div>
					<div class="daejeon2 gu"><input type="hidden" name="daejeon" value="서구">서구</div>
					<div class="daejeon2 gu"><input type="hidden" name="daejeon" value="유성구">유성구</div>
					<div class="daejeon2 gu"><input type="hidden" name="daejeon" value="중구">중구</div>
				</div>
			</div>
			<div id="ulsandiv" class="metro" style="height: 40px">
				<div class="ulsan">
					<div class="ulsan2 gu"><input type="hidden" name="daejeon" value="중구">전체</div>
					<div class="ulsan2 gu"><input type="hidden" name="daejeon" value="남구">남구</div>
					<div class="ulsan2 gu"><input type="hidden" name="daejeon" value="동구">동구</div>
					<div class="ulsan2 gu"><input type="hidden" name="daejeon" value="북구">북구</div>
					<div class="ulsan2 gu"><input type="hidden" name="daejeon" value="울주군">울주군</div>
					<div class="ulsan2 gu"><input type="hidden" name="daejeon" value="중구">중구</div>
				</div>
			</div>
			<div id="sejongdiv" class="metro" style="height: 40px">
				<div class="sejong" style="height: 40px">
					<div class="sejong2"><input type="hidden" name="sejong" value="세종특별시">세종특별시</div>
				</div>
			</div>
			<div id="gangwondiv" class="metro" style="height: 120px">
				<div class="gangwon">
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="전체">전체</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="강릉시">강릉시</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="고성군">고성군</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="동해시">동해시</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="삼척시">삼척시</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="속초시">속초시</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="양구군">양구군</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="양양군">양양군</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="영월군">영월군</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="원주시">원주시</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="인제군">인제군</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="정선군">정선군</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="철원군">철원군</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="춘천시">춘천시</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="태백시">태백시</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="평창군">평창군</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="홍천군">홍천군</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="화천군">화천군</div>
					<div class="gangwon2 gu"><input type="hidden" name="gangwon" value="횡성군">횡성군</div>
				</div>
			</div>
			<div id="kyeoungnamdiv" class="metro" style="height: 120px">
				<div class="kyeoungnam">
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="전체">전체</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="거제시">거제시</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="거창군">거창군</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="고성군">고성군</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="김해시">김해시</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="남해군">남해군</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="밀양시">밀양시</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="사천시">사천시</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="산청군">산청군</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="양산시">양산시</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="의령군">의령군</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="진주시">진주시</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="창녕군">창녕군</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="창원시">창원시</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="통영시">통영시</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="하동군">하동군</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="함안군">함안군</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="함양군">함양군</div>
					<div class="kyeoungnam2 gu"><input type="hidden" name="kyeoungnam" value="함천군">함천군</div>
				</div>
			</div>
			<div id="kyeoungbukdiv" class="metro" style="height: 120px">
				<div class="kyeoungbuk">
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="전체">전체</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="경산시">경산시</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="경주시">경주시</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="고령군">고령군</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="구미시">구미시</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="군위군">군위군</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="김천시">김천시</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="문경시">문경시</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="봉화군">봉화군</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="상주시">상주시</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="성주군">성주군</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="안동시">안동시</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="영덕군">영덕군</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="영양군">영양군</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="영주시">영주시</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="영천시">영천시</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="예천군">예천군</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="울릉군">울릉군</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="울진군">울진군</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="의성군">의성군</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="청도군">청도군</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="청송군">청송군</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="칠곡군">칠곡군</div>
					<div class="kyeoungbuk2 gu"><input type="hidden" name="kyeoungbuk" value="포항시">포항시</div>
				</div>
			</div>
			<div id="jeonnamdiv" class="metro" style="height: 120px">
				<div class="jeonnam">
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="전체">전체</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="강진군">강진군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="고흥군">고흥군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="곡성군">곡성군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="광양시">광양시</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="구례군">구례군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="나주시">나주시</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="담양군">담양군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="목포시">목포시</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="무안군">무안군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="보성군">보성군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="순천시">순천시</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="신안군">신안군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="여수시">여수시</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="영광군">영광군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="영암군">영암군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="완도군">완도군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="장성군">장성군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="장흥군">장흥군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="진도군">진도군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="함평군">함평군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="해남군">해남군</div>
					<div class="jeonnam2 gu"><input type="hidden" name="jeonnam" value="화순군">화순군</div>
				</div>
			</div>
			<div id="jeonbukdiv" class="metro" style="height: 80px">
				<div class="jeonbuk">
					<div class="jeonbuk2 gu"><input type="hidden" name="jeonbuk" value="전체">전체</div>
					<div class="jeonbuk2 gu"><input type="hidden" name="jeonbuk" value="고창군">고창군</div>
					<div class="jeonbuk2 gu"><input type="hidden" name="jeonbuk" value="군산시">군산시</div>
					<div class="jeonbuk2 gu"><input type="hidden" name="jeonbuk" value="김제시">김제시</div>
					<div class="jeonbuk2 gu"><input type="hidden" name="jeonbuk" value="남원시">남원시</div>
					<div class="jeonbuk2 gu"><input type="hidden" name="jeonbuk" value="무주군">무주군</div>
					<div class="jeonbuk2 gu"><input type="hidden" name="jeonbuk" value="부안군">부안군</div>
					<div class="jeonbuk2 gu"><input type="hidden" name="jeonbuk" value="순창군">순창군</div>
					<div class="jeonbuk2 gu"><input type="hidden" name="jeonbuk" value="완주군">완주군</div>
					<div class="jeonbuk2 gu"><input type="hidden" name="jeonbuk" value="익산시">익산시</div>
					<div class="jeonbuk2 gu"><input type="hidden" name="jeonbuk" value="임실군">임실군</div>
					<div class="jeonbuk2 gu"><input type="hidden" name="jeonbuk" value="장수군">장수군</div>
					<div class="jeonbuk2 gu"><input type="hidden" name="jeonbuk" value="전주시">전주시</div>
					<div class="jeonbuk2 gu"><input type="hidden" name="jeonbuk" value="정읍시">정읍시</div>
					<div class="jeonbuk2 gu"><input type="hidden" name="jeonbuk" value="진안군">진안군</div>
				</div>
			</div>
			<div id="chungnamdiv" class="metro" style="height: 120px">
				<div class="chungnam">
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="전체">전체</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="계룡시">계룡시</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="공주시">공주시</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="금산군">금산군</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="논산시">논산시</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="당진시">당진시</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="보령시">보령시</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="부여군">부여군</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="서산시">서산시</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="서천군">서천군</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="아산시">아산시</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="연기군">연기군</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="예산군">예산군</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="천안시">천안시</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="청양군">청양군</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="태안군">태안군</div>
					<div class="chungnam2 gu"><input type="hidden" name="chungnam" value="홍성군">홍성군</div>
				</div>
			</div>
			<div id="chungbukdiv" class="metro" style="height: 80px">
				<div class="chungbuk">
					<div class="chungbuk2 gu"><input type="hidden" name="chungbuk" value="전체">전체</div>
					<div class="chungbuk2 gu"><input type="hidden" name="chungbuk" value="괴산군">괴산군</div>
					<div class="chungbuk2 gu"><input type="hidden" name="chungbuk" value="단양군">단양군</div>
					<div class="chungbuk2 gu"><input type="hidden" name="chungbuk" value="보은군">보은군</div>
					<div class="chungbuk2 gu"><input type="hidden" name="chungbuk" value="영동군">영동군</div>
					<div class="chungbuk2 gu"><input type="hidden" name="chungbuk" value="옥천군">옥천군</div>
					<div class="chungbuk2 gu"><input type="hidden" name="chungbuk" value="음성군">음성군</div>
					<div class="chungbuk2 gu"><input type="hidden" name="chungbuk" value="제천시">제천시</div>
					<div class="chungbuk2 gu"><input type="hidden" name="chungbuk" value="중평군">중평군</div>
					<div class="chungbuk2 gu"><input type="hidden" name="chungbuk" value="진천군">진천군</div>
					<div class="chungbuk2 gu"><input type="hidden" name="chungbuk" value="청원군">청원군</div>
					<div class="chungbuk2 gu"><input type="hidden" name="chungbuk" value="청주시">청주시</div>
					<div class="chungbuk2 gu"><input type="hidden" name="chungbuk" value="충주시">충주시</div>
				</div>
			</div>
			<div id="jejudiv" class="metro" style="height: 40px">
				<div class="jeju">
					<div class="jeju2 gu"><input type="hidden" name="jeju" value="전체">전체</div>
					<div class="jeju2 gu"><input type="hidden" name="jeju" value="서귀포시">서귀포시</div>
					<div class="jeju2 gu"><input type="hidden" name="jeju" value="제주시">제주시</div>
				</div>
			</div><br>
			
			<hr>
			<br>
			
			<!-- 직종선택 영역 -->			
			<p style="margin-left: 40px; margin-bottom:15px"><b>직종선택</b></p>
			<div id="kindsJob" style="height: 30px">
				<div class="kindsJob">
					<div class="kindsJobTop gu">전체</div>
					<div class="kindsJobTop gu" id="cooking">요리/서빙</div>
					<div class="kindsJobTop gu" id="medical">간호/의료</div>
					<div class="kindsJobTop gu" id="construct">생산/건설</div>
					<div class="kindsJobTop gu" id="office">사무/경리</div>
					<div class="kindsJobTop gu" id="driving">운전/배달</div>
					<div class="kindsJobTop gu" id="sales">상담/영업</div>
					<div class="kindsJobTop gu" id="storeManger">매장관리</div>
					<div class="kindsJobTop gu" id="teacher">교사/강사</div>
					<div class="kindsJobTop gu" id="etc">일반/기타</div>
				</div>
			</div>
			<div id="cookingDiv" class="jKinds" style="height: 80px">
				<div class="cooking">
					<div class="cooking2 gu"><input type="hidden" name="cooking" value="전체">전체</div>
					<div class="cooking2 gu"><input type="hidden" name="cooking" value="주방장/조리사">주방장/조리사</div>
					<div class="cooking2 gu"><input type="hidden" name="cooking" value="주방/주방보조">주방/주방보조</div>
					<div class="cooking2 gu"><input type="hidden" name="cooking" value="찬모/밥모">찬모/밥모</div>
					<div class="cooking2 gu"><input type="hidden" name="cooking" value="설거지">설거지</div>
					<div class="cooking2 gu"><input type="hidden" name="cooking" value="서빙">서빙</div>
					<div class="cooking2 gu"><input type="hidden" name="cooking" value="카운터">카운터</div>
					<div class="cooking2 gu"><input type="hidden" name="cooking" value="점장/매니저">점장/매니저</div>
					<div class="cooking2 gu"><input type="hidden" name="cooking" value="기타">기타</div>
				</div>
			</div>
			<div id="medicalDiv" class="jKinds" style="height: 40px">
				<div class="medical">
					<div class="medical2 gu"><input type="hidden" name="medical" value="전체">전체</div>
					<div class="medical2 gu" style="font-size:15px"><input type="hidden" name="medical" value="간호/간호조무사">간호/간호조무사</div>
					<div class="medical2 gu"><input type="hidden" name="medical" value="간병/요양보호사">간병/요양보호사</div>
					<div class="medical2 gu"><input type="hidden" name="medical" value="의료기사">의료기사</div>
					<div class="medical2 gu"><input type="hidden" name="medical" value="기타의료직">기타의료직</div>
				</div>
			</div>
			<div id="constructDiv" class="jKinds" style="height: 80px">
				<div class="construct">
					<div class="construct2 gu"><input type="hidden" name="construct" value="전체">전체</div>
					<div class="construct2 gu"><input type="hidden" name="construct" value="제조/조립">제조/조립</div>
					<div class="construct2 gu"><input type="hidden" name="construct" value="미싱/재단/섬유">미싱/재단/섬유</div>
					<div class="construct2 gu"><input type="hidden" name="construct" value="노무현장/조선소">노무현장/조선소</div>
					<div class="construct2 gu"><input type="hidden" name="construct" value="건설/공사/보수">건설/공사/보수</div>
					<div class="construct2 gu"><input type="hidden" name="construct" value="전기/시설관리">전기/시설관리</div>
					<div class="construct2 gu"><input type="hidden" name="construct" value="배관/용접/취부">배관/용접/취부</div>
					<div class="construct2 gu" style="font-size:14px"><input type="hidden" name="construct" value="선반/밀링/머시닝">선반/밀링/머시닝</div>
					<div class="construct2 gu"><input type="hidden" name="construct" value="식품제조/가공">식품제조/가공</div>
					<div class="construct2 gu" style="font-size:14px"><input type="hidden" name="construct" value="자동차정비/튜닝">자동차정비/튜닝</div>
					<div class="construct2 gu"><input type="hidden" name="construct" value="설치/수리">설치/수리</div>
					<div class="construct2 gu"><input type="hidden" name="construct" value="생산/포장/검사">생산/포장/검사</div>
					<div class="construct2 gu"><input type="hidden" name="construct" value="가구/목공/주방">가구/목공/주방</div>
					<div class="construct2 gu" style="font-size:14px"><input type="hidden" name="construct" value="금형/프레스/성형">금형/프레스/성형</div>
					<div class="construct2 gu"><input type="hidden" name="construct" value="기타">기타</div>
				</div>
			</div>
			<div id="officeDiv" class="jKinds" style="height: 40px">
				<div class="office">
					<div class="office2"><input type="hidden" name="office" value="기타">전체</div>
					<div class="office2"><input type="hidden" name="office" value="경리/회계/인사">경리/회계/인사</div>
					<div class="office2"><input type="hidden" name="office" value="일반사무/내근직">일반사무/내근직</div>
					<div class="office2"><input type="hidden" name="office" value="기획/총무/관리">기획/총무/관리</div>
					<div class="office2"><input type="hidden" name="office" value="구매/자재/물">구매/자재/물류</div>
					<div class="office2"><input type="hidden" name="office" value="비서/안내">비서/안내</div>
					<div class="office2"><input type="hidden" name="office" value="기타">기타</div>
				</div>
			</div>
			<div id="drivingDiv" class="jKinds" style="height: 40px">
				<div class="driving">
					<div class="driving2 gu">전체</div>
					<div class="driving2 gu" style="font-size:14px"><input type="hidden" name="driving" value="기타">이사/택배/퀵배송</div>
					<div class="driving2 gu" style="font-size:14px"><input type="hidden" name="driving" value="기타">대리/승용차/일반</div>
					<div class="driving2 gu" style="font-size:14px"><input type="hidden" name="driving" value="기타">버스/택시/승합차</div>
					<div class="driving2 gu"><input type="hidden" name="office" value="기타">지입/차량용역</div>
					<div class="driving2 gu" style="font-size:14px"><input type="hidden" name="driving" value="기타">화물/중장비/특수차</div>
					<div class="driving2 gu" style="font-size:14px"><input type="hidden" name="driving" value="기타">음식점/식음료배달</div>
					<div class="driving2 gu"><input type="hidden" name="driving" value="기타">기타</div>
				</div>
			</div>
			<div id="salesDiv" class="jKinds" style="height: 80px">
				<div class="sales">
					<div class="sales2 gu"><input type="hidden" name="sales" value="전체">전체</div>
					<div class="sales2 gu"><input type="hidden" name="sales" value="인바운드/CS">인바운드/CS</div>
					<div class="sales2 gu"><input type="hidden" name="sales" value="아웃바운드/TM">아웃바운드/TM</div>
					<div class="sales2 gu"><input type="hidden" name="sales" value="일반/기술영업">일반/기술영업</div>
					<div class="sales2 gu"><input type="hidden" name="sales" value="보험/금융상담">보험/금융상담</div>
					<div class="sales2 gu"><input type="hidden" name="sales" value="방문판매">방문판매</div>
					<div class="sales2 gu"><input type="hidden" name="sales" value="부동산상담">부동산상담</div>
					<div class="sales2 gu"><input type="hidden" name="sales" value="홍보/마케팅">홍보/마케팅</div>
					<div class="sales2 gu"><input type="hidden" name="sales" value="기타">기타</div>
				</div>
			</div>
			<div id="storeMangerDiv" class="jKinds" style="height: 80px">
				<div class="storeManger">
					<div class="storeManger2 gu"><input type="hidden" name="storeManger" value="전체">전체</div>
					<div class="storeManger2 gu"><input type="hidden" name="storeManger" value="슈펴/마트">슈펴/마트</div>
					<div class="storeManger2 gu"><input type="hidden" name="storeManger" value="편의점">편의점</div>
					<div class="storeManger2 gu"><input type="hidden" name="storeManger" value="대형마트">대형마트</div>
					<div class="storeManger2 gu"><input type="hidden" name="storeManger" value="쇼핑몰/아울렛">쇼핑몰/아울렛</div>
					<div class="storeManger2 gu"><input type="hidden" name="storeManger" value="백화점">백화점</div>
					<div class="storeManger2 gu"><input type="hidden" name="storeManger" value="찜질방/사우나">찜질방/사우나</div>
					<div class="storeManger2 gu"><input type="hidden" name="storeManger" value="농수산/청과/축산">농수산/청과/축산</div>
					<div class="storeManger2 gu"><input type="hidden" name="storeManger" value="의류/잡화">의류/잡화</div>
					<div class="storeManger2 gu"><input type="hidden" name="storeManger" value="가전/휴대폰">가전/휴대폰</div>
					<div class="storeManger2 gu"><input type="hidden" name="storeManger" value="물류/재고">물류/재고</div>
					<div class="storeManger2 gu"><input type="hidden" name="storeManger" value="PC방/오락실">PC방/오락실</div>
					<div class="storeManger2 gu"><input type="hidden" name="storeManger" value="노래방/볼링장">노래방/볼링장</div>
					<div class="storeManger2 gu"><input type="hidden" name="storeManger" value="기타">기타</div>
				</div>
			</div>
			<div id="teacherDiv" class="jKinds" style="height: 40px">
				<div class="teacher">
					<div class="teacher2 gu"><input type="hidden" name="teacher" value="전체">전체</div>
					<div class="teacher2 gu"><input type="hidden" name="teacher" value="어린이집/유치원">어린이집/유치원</div>
					<div class="teacher2 gu"><input type="hidden" name="teacher" value="방문/학습지">방문/학습지</div>
					<div class="teacher2 gu"><input type="hidden" name="teacher" value="입시/보습/과외">입시/보습/과외</div>
					<div class="teacher2 gu"><input type="hidden" name="teacher" value="외국어">외국어</div>
					<div class="teacher2 gu"><input type="hidden" name="teacher" value="예체능/컴퓨터">예체능/컴퓨터</div>
					<div class="teacher2 gu"><input type="hidden" name="teacher" value="학원관리">학원관리</div>
					<div class="teacher2 gu"><input type="hidden" name="teacher" value="기타교사">기타교사</div>
 				</div>
			</div>
			<div id="etcDiv" class="jKinds" style="height: 80px">
				<div class="etc">
					<div class="etc2 gu"><input type="hidden" name="etc" value="전체">전체</div>
					<div class="etc2 gu"><input type="hidden" name="etc" value="미화/청소/세탁">미화/청소/세탁</div>
					<div class="etc2 gu" style="font-size:14px"><input type="hidden" name="etc" value="생활도우미/파출부">생활도우미/파출부</div>
					<div class="etc2 gu"><input type="hidden" name="etc" value="경비/보안">경비/보안</div>
					<div class="etc2 gu"><input type="hidden" name="etc" value="주유/세차/광택">주유/세차/광택</div>
					<div class="etc2 gu"><input type="hidden" name="etc" value="헤어/피부/미용">헤어/피부/미용</div>
					<div class="etc2 gu" style="font-size:14px"><input type="hidden" name="etc" value="골프장/골프연습장">골프장/골프연습장</div>
					<div class="etc2 gu" style="font-size:10px"><input type="hidden" name="etc" value="웨딩/이벤트/스튜디오">웨딩/이벤트/스튜디오</div>
					<div class="etc2 gu"><input type="hidden" name="etc" value="상조/장례서비스">상조/장례서비스</div>
					<div class="etc2 gu"><input type="hidden" name="etc" value="컴퓨터/IT/디자인">컴퓨터/IT/디자인</div>
					<div class="etc2 gu"><input type="hidden" name="etc" value="호텔/모델/숙박">호텔/모델/숙박</div>
					<div class="etc2 gu" style="font-size:14px"><input type="hidden" name="etc" value="주차관리/대리주차">주차관리/대리주차</div>
					<div class="etc2 gu"><input type="hidden" name="etc" value="기타서비스">기타서비스</div>
 				</div>
			</div><br>
		</div><br>
		<!-- 지역, 직종선택 END -->
		
		<!-- 지역, 업종 출력영역 -->
		<div id="printArea">
			<table class="printArea2" style="padding:40px; font-weight:bold; font-size:16px; text-align:center">
				<tr>
					<td width="10%">선택지역</td>
					<td width="">
						<!-- <label style="font-weight:bold; margin-left: 40px;">선택지역 : &nbsp;</label> -->
						<div id="printLo"></div>
					</td>
					<td width="10%">선택직종</td>
					<td width="">
						<!-- <label style="font-weight:bold; margin-left: 40px;">선택직종 : &nbsp;</label> -->
						<div id="printJob"></div>
					</td>
				</tr>
			</table>
		</div><br><br>
		
		<!-- 지역, 직종선택 검색버튼 -->
		<div class="serchBtn" align="center" style="width: 1200px; height: 80px; border: 1px solid lightgray; line-height: 80px">
			<button class="searchBtn" onclick="searchRecruit()">검색
			</button>
		</div><br>
		
		<!-- 
		반짝 상품 그리드 
        <div id="item_title" style="padding-top:30px;">
        	<img class="item_banjjak" alt="" src="../../static/images/user/crown.png">
        	<b style="color:#2FA599; font-size:22px; padding:2000">반짝</b><b style="font-size:22px; margin-top:-600px">채용공고</b>
        	<table style="margin-bottom:-10px">
        		<tr>
        			<td><img class="item_banjjak" alt="" src="../../static/images/user/crown.png"></td>
        			<td><b style="color:#2FA599; font-size:22px; margin-left: 20px">반짝</b><b style="font-size:22px;">채용공고</b></td>
        		</tr>
        	</table>
        </div><br>
        
        <div class="section">
	     		<div class="item1" style="clear:both;">
	            	<img class="item_gold" alt="" src="../../static/images/user/logo.png">
	            	<div class="bName">해볼래(주)</div>
	            	<div class="bTitle">서버 및 풀스택 개발자 채용</div>
	            	<div class="bPeriods">D-10</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star.png">
	        		</div>
	      		</div>
	            <div class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/common/logo_bm.png">
	            	<div class="bName">자바의 민족</div>
	            	<div class="bTitle">IT 별짓기 마스터 채용(신입/경력)</div>
	            	<div class="bPeriods">D-12</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
	            <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/user/logo.png">
	            	<div class="bName">해볼래(주)</div>
	            	<div class="bTitle">서버 및 풀스택 개발자 채용</div>
	            	<div class="bPeriods">D-10</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
	            <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/common/logo_bm.png">
	            	<div class="bName">자바의 민족</div>
	            	<div class="bTitle">IT 별짓기 마스터 채용(신입/경력)</div>
	            	<div class="bPeriods">D-12</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
	            <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/common/logo_bm.png">
	            	<div class="bName">자바의 민족</div>
	            	<div class="bTitle">IT 별짓기 마스터 채용(신입/경력)</div>
	            	<div class="bPeriods">D-12</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
		        <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/user/logo.png">
	            	<div class="bName">해볼래(주)</div>
	            	<div class="bTitle">서버 및 풀스택 개발자 채용</div>
	            	<div class="bPeriods">D-10</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
	            <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/common/logo_bm.png">
	            	<div class="bName">자바의 민족</div>
	            	<div class="bTitle">IT 별짓기 마스터 채용(신입/경력)</div>
	            	<div class="bPeriods">D-12</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
		        <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/user/logo.png">
	            	<div class="bName">해볼래(주)</div>
	            	<div class="bTitle">서버 및 풀스택 개발자 채용</div>
	            	<div class="bPeriods">D-10</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
		        <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/user/logo.png">
	            	<div class="bName">해볼래(주)</div>
	            	<div class="bTitle">서버 및 풀스택 개발자 채용</div>
	            	<div class="bPeriods">D-10</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
         	    <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/common/logo_bm.png">
	            	<div class="bName">자바의 민족</div>
	            	<div class="bTitle">IT 별짓기 마스터 채용(신입/경력)</div>
	            	<div class="bPeriods">D-12</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
		        <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/user/logo.png">
	            	<div class="bName">해볼래(주)</div>
	            	<div class="bTitle">서버 및 풀스택 개발자 채용</div>
	            	<div class="bPeriods">D-10</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
         	    <div  class="item1">
	            	<img class="item_gold" alt="" src="../../static/images/common/logo_bm.png">
	            	<div class="bName">자바의 민족</div>
	            	<div class="bTitle">IT 별짓기 마스터 채용(신입/경력)</div>
	            	<div class="bPeriods">D-12</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="../../static/images/common/star_em.png">
	            	</div>
	            </div>
			</div>
			반짝 상품 그리드 END
			 -->
			
			<br><br>
			
			<!-- 일반채용 정보 -->
			<div style="font-size:20px"><b>일반 보금자리 채용정보</b></div>
			<div id="iqNormalList" style="border: 1px solid lightgray; margin-top: 15px">
				<div id="iqNormalListArea">
					<div id="iqnorTableArea">
						<table id="underListTab">
							<tr id="tableTr1" style="text-align: center">
								<th class="tableTh" style="width:20%; height:35px;">근무지</th>
								<th class="tableTh" style="width:45%;">회사명 / 모집내용</th>
								<th class="tableTh" style="width:15%;">급여(원)</th>
								<th class="tableTh" style="width:25%;">등록일</th>
							</tr>
							
							<% for (Recruit r : slist) { %>
							<tr id="tableTr2" style="height: 100px;">
								<td class="getRecruitNo" hidden><%= r.getRecruitNo() %></td>
								<!-- 아래 조건이 아닌 경우 if로 설정해 줌  -->
								<% if (r.getDetailArea().equals("(null)")) { %>
									<td>주소없음</td>
								<% } else {
																	
									if(r.getDetailArea().length() > 10) { %>
									<td style="text-align:center"><%= r.getDetailArea().substring(0, 10).split(" ")[0] + " " + r.getDetailArea().substring(0, 15).split(" ")[1]%> </td>
									
									<%} else { %>
										<td><%= r.getDetailArea() %></td>
									<% } 
								} %>
								
								<td style="text-align:left; margin-left: 60px">
									<label style="font-size: 16px"><b><%= r.getCompanyName() %></b></label><br>
									<label style="font-size: 18px"><%= r.getRecruitTitle() %></label><br>
									<label style="color:#505050; font-weight:bold"><%= r.getJobTime() %></label>
								</td>
								<td style="text-align:right; color:#505050; font-weight:bold">
									<%-- <% if(r.getSalaryType().equals("시급")) { %> --%>
										<p class="salary"><%= r.getSalaryType() %></p>
									<%-- <% } else if(r.getSalaryType().equals("일급")) { %>
										<p style="float:left; background:#ECF0F7; text-align:center; width: 40px; border:1px solid navy; border-radius: 5px">
										<%= r.getSalaryType() %></p>
									<% } else if(r.getSalaryType().equals("월급")) { %>
										<p style="float:left; background:#ECF0F7; text-align:center; width: 40px; border:1px solid green; border-radius: 5px">
										<%= r.getSalaryType() %></p>
									<% } else if(r.getSalaryType().equals("연봉")) {%>
										<p style="float:left; background:#ECF0F7; text-align:center; width: 40px; border:1px solid pink; border-radius: 5px">
										<%= r.getSalaryType() %></p>
									<% } else { %>
										<p style="float:left; background:#ECF0F7; text-align:center; width: 40px; border:1px solid pink; border-radius: 5px">협의</p>
									<% } %> --%>
									
									<%= r.getSalary() %>원
								</td>
								<td style="text-align:center"><%= r.getEnrollDate() %></td>
							</tr>
							
							<% } %>
						
						</table>
					</div>
				</div>
			</div> <!-- 일반채용 리스트 end -->
			<br><br><br><br><br><br><br><br>
			
			
			<!-- 
			페이징 처리
			<div class="pagingArea" align="center"> 
			<button><<</button>
			<button><</button>1 페이지
			<button>></button>
			<button>>></button>
			</div>
			 -->
			
	
	<%@ include file="../guide/footer.jsp" %>
		
	</div> <!--전체 wrap 영역 END -->
	


	<script>
	//지역 hide and show
	$(function(){
        var arr = $(".metro");
         
        for(var i = 0; i < arr.length; i++) {
			arr[i].style.display = "none";
        }
        
      	var metroLo = $(".trLocation").children();

       	$(".trLocation").children().each(function(index){
            $(this).click(function(){
               //console.log(index);         
               //console.log($.type(index));
               
               for(var i = 0; i < arr.length; i++) {
       				arr[i].style.display = "none";
                }
	               
               switch (index){
                  case 1 : $("#seouldiv").show(); break;
                  case 2 : $("#gyoengidiv").show(); break;
                  case 3 : $("#inchoendiv").show(); break;
                  case 4 : $("#busandiv").show(); break;
                  case 5 : $("#daegudiv").show(); break;
                  case 6 : $("#gwangjudiv").show(); break;
                  case 7 : $("#daejeondiv").show(); break;
                  case 8 : $("#ulsandiv").show(); break;
                  case 9 : $("#sejongdiv").show(); break;
                  case 10 : $("#gangwondiv").show(); break;
                  case 11 : $("#kyeoungnamdiv").show(); break;
                  case 12 : $("#kyeoungbukdiv").show(); break;
                  case 13 : $("#jeonnamdiv").show(); break;
                  case 14 : $("#jeonbukdiv").show(); break;
                  case 15 : $("#chungnamdiv").show(); break;
                  case 16 : $("#chungbukdiv").show(); break;
                  case 17 : $("#jejudiv").show(); break;
              }
           });
        });         
        
      });
	      
    //업종 hide and show
	$(function(){
	
		var jarr = $(".jKinds");
          
         for(var i = 0; i < jarr.length; i++) {
         	jarr[i].style.display = "none";
         }
         
         var jKinds = $(".kindsJob").children();
         
         $(".kindsJob").children().each(function(index){
			$(this).click(function(){
				//console.log(index);         
	            //console.log($.type(index));
         	for(var i = 0; i < jarr.length; i++) {
             	jarr[i].style.display = "none";
          	}
		
         	switch(index) {
         	case 1 : $("#cookingDiv").show(); break;
         	case 2 : $("#medicalDiv").show(); break;
         	case 3 : $("#constructDiv").show(); break;
         	case 4 : $("#officeDiv").show(); break;
         	case 5 : $("#drivingDiv").show(); break;
         	case 6 : $("#salesDiv").show(); break;
         	case 7 : $("#storeMangerDiv").show(); break;
         	case 8 : $("#teacherDiv").show(); break;
         	case 9 : $("#etcDiv").show(); break;
       		}
        });
   	 });
	});
    
    
    
    //선택한 지역 + 업종 검색창에 삽입
 	/* $(".tdLocation").click(function() {
 		var selectedLocation = $(".tdLocation").eq(1).text();
		console.log(selectedLocation);
    }); */
/*     
   $(".seouldiv").children().each(function(){
		$(this).click(function() {
			
			/* var selectedLocation = $(".seoul2").text();
			console.log(selectedLocation); */
			
		/* 	var print = "서울 > 강남구";
			console.log(print);
			
		});
    }); */
    

 /* 
 	$(".seoul").children().each(function() {
		var location = $(".seoul2").text();
		console.log(location);
		
 	});
     */
     
     /* $(".tdLocation").click(function() {
    	var aa = $(".tdLocation").attr("#seoul");
    	$(".printLocation").attr('printLocation', aa);
    	console.log(aa);
     }); */
     
    /*  $(document).ready(function() {
    	var aa = $(".tdLocation").attr(".tdLocation");
    	console.log(aa);
     }); */
     
     
     
    // 
     
    
    
    
    
    
    /* 진규 추가 css */
	$(function(){

		$(".gu").click(function(){
			$(this).addClass("clickGu");    
			$(this).siblings().removeClass("clickGu");
		});
		
	});
     
     
    //------- 지역값 뽑기 --------
    $("#printLo").hide();
    $("#printJob").hide();
        
    //서울 지역값
    $(".seoul2").click(function(){
    	var seoul2 = $(this).children().val();
    	console.log(seoul2);
    	$("#printLo").text(seoul2);
    	$("#printLo").show();
    });
    //경기지역값
    $(".gyoengi2").click(function(){
    	var gyoengi2 = $(this).children().val();
    	console.log(gyoengi2);
    	$("#printLo").text(gyoengi2);
    	$("#printLo").show();
    });
	//인천지역값
	$(".inchoen2").click(function(){
    	var inchoen2 = $(this).children().val();
    	console.log(inchoen2);
    	$("#printLo").text(inchoen2);
    	$("#printLo").show();
    });
    //부산지역값
    $(".busan2").click(function(){
    	var busan2 = $(this).children().val();
    	console.log(busan2);
    	$("#printLo").text(busan2);
    	$("#printLo").show();
    });
	//대구지역값
	$(".daegu2").click(function(){
    	var busan2 = $(this).children().val();
    	console.log(busan2);
    	$("#printLo").text(busan2);
    	$("#printLo").show();
    });
    //광주
    $(".gwangju2").click(function(){
    	var gwangju2 = $(this).children().val();
    	console.log(gwangju2);
    	$("#printLo").text(gwangju2);
    	$("#printLo").show();
    });
    //대전
    $(".daejeon2").click(function(){
    	var daejeon2 = $(this).children().val();
    	console.log(daejeon2);
    	$("#printLo").text(daejeon2);
    	$("#printLo").show();
    });
    //울산
    $(".ulsan2").click(function(){
    	var ulsan2 = $(this).children().val();
    	console.log(ulsan2);
    	$("#printLo").text(ulsan2);
    	$("#printLo").show();
    });
    //세종
    $(".sejong2").click(function(){
    	var sejong2 = $(this).children().val();
    	console.log(sejong2);
    	$("#printLo").text(sejong2);
    	$("#printLo").show();
    });
    //강원
    $(".gangwon2").click(function(){
    	var gangwon2 = $(this).children().val();
    	console.log(gangwon2);
    	$("#printLo").text(gangwon2);
    	$("#printLo").show();
    });
    //경남
    $(".kyeoungnam2").click(function(){
    	var kyeoungnam2 = $(this).children().val();
    	console.log(kyeoungnam2);
    	$("#printLo").text(kyeoungnam2);
    	$("#printLo").show();
    });
    //경북
    $(".kyeoungbuk2").click(function(){
    	var kyeoungbuk2 = $(this).children().val();
    	console.log(kyeoungbuk2);
    	$("#printLo").text(kyeoungbuk2);
    	$("#printLo").show();
    });
    //전남
    $(".jeonnam2").click(function(){
    	var jeonnam2 = $(this).children().val();
    	console.log(jeonnam2);
    	$("#printLo").text(jeonnam2);
    	$("#printLo").show();
    });
  	//전북
    $(".jeonbuk2").click(function(){
    	var jeonbuk2 = $(this).children().val();
    	console.log(jeonbuk2);
    	$("#printLo").text(jeonbuk2);
    	$("#printLo").show();
    });
  	//충남
  	$(".chungnam2").click(function(){
    	var chungnam2 = $(this).children().val();
    	console.log(chungnam2);
    	$("#printLo").text(chungnam2);
    	$("#printLo").show();
    });
  	//충북
  	$(".chungbuk2").click(function(){
    	var chungbuk2 = $(this).children().val();
    	console.log(chungbuk2);
    	$("#printLo").text(chungbuk2);
    	$("#printLo").show();
    });
  	//제주
  	$(".jeju2").click(function(){
    	var jeju2 = $(this).children().val();
    	console.log(jeju2);
    	$("#printLo").text(jeju2);
    	$("#printLo").show();
    });
    
     
  	//------- 직종값 뽑기 --------
  	//요리서빙
	$(".cooking2").click(function(){
    	var cooking2 = $(this).children().val();
    	console.log(cooking2);
    	$("#printJob").text(cooking2);
    	$("#printJob").show();
    });
	//간호의료
  	$(".medical2").click(function(){
    	var medical2 = $(this).children().val();
    	console.log(medical2);
    	$("#printJob").text(medical2);
    	$("#printJob").show();
    });
    //생산건설
    $(".construct2").click(function(){
    	var construct2 = $(this).children().val();
    	console.log(construct2);
    	$("#printJob").text(construct2);
    	$("#printJob").show();
    });
    //사무경리
    $(".office2").click(function(){
    	var office2 = $(this).children().val();
    	console.log(office2);
    	$("#printJob").text(office2);
    	$("#printJob").show();
    });
    //운전배달
    $(".driving2").click(function(){
    	var driving2 = $(this).children().val();
    	console.log(driving2);
    	$("#printJob").text(driving2);
    	$("#printJob").show();
    });
    //상담영업
    $(".sales2").click(function(){
    	var sales2 = $(this).children().val();
    	console.log(sales2);
    	$("#printJob").text(sales2);
    	$("#printJob").show();
    });
    //매장관리
    $(".storeManger2").click(function(){
    	var storeManger2 = $(this).children().val();
    	console.log(storeManger2);
    	$("#printJob").text(storeManger2);
    	$("#printJob").show();
    });
    //교사강사
    $(".teacher2").click(function(){
    	var teacher2 = $(this).children().val();
    	console.log(teacher2);
    	$("#printJob").text(teacher2);
    	$("#printJob").show();
    });
    //일반/기타
    $(".etc2").click(function(){
    	var etc2 = $(this).children().val();
    	console.log(etc2);
    	$("#printJob").text(etc2);
    	$("#printJob").show();
    });
     
    
    //조건검색 버튼
    function searchRecruit() {
    	
    	var condition = $("#printLo").text();
    	condition += "#" + $("#printJob").text();
    	console.log(condition);
    	
    	location.href = "<%= request.getContextPath()%>/searchCondition.rc?condition=" + encodeURIComponent(condition);
    	<%-- var url = "<%=request.getContextPath()%>/delete.faq?deleteNo=" + encodeURIComponent(deleteNo); --%>
    	
    }
     
     
  	
  	
  	
  	
  	
  	
  	
  	
     //일반채용공고 리스트 상세페이지로 클릭
     $(function(){
    	 $("#underListTab td").mouseenter(function(){
    		$(this).parent().css({"background":"#E5E5E5", "cursor":"pointer"}) 
    	 }).mouseout(function(){
    		$(this).parent().css("background", ""); 
    	 }).click(function(){
    		var num = $(this).parent().children().eq(0).text();
	 		console.log(num);
    		location.href = "<%= request.getContextPath()%>/selectOneSearch.rc?num=" + num;
    	 });
     });
     
      
     
     
     
     
     
     <%-- //상세페이지에서 뒤로가기 버튼 클릭
      $(function(){
      	$("#returnList").click(function(){
		var num ="<%= recruit.getMemberNo()%>";
		console.log(num);
		location.href="<%= request.getContentLength()%>/selectListSearch.rc";
		});
      }); --%>
      
     <%--  $(function(){
      	$("#returnList")
      	.click(function(){
				var num =<%=recruit.ㅎㄷ%>
				console.log(num);
				location.href = "<%=request.getContextPath()%>/selectListSearch.rc?num=" + num;
			});
      }); --%>
      
      
      
   </script>

   
	
</body>
</html>












