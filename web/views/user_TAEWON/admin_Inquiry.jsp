<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>기업승인</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	#wrap {width: 1200px; height: 100%; margin: 0 auto;}
	
	/* ============= 헤더 영역 =============  */
	header {width: 100%; height: 100px; background: #013252;}
	.logo {font-size:45px; color: white;}
	#title {margin-left:10px;}
	
	/* 사이트 바로가기 td id명  */
	#goSite {
		font-size: 18px;
	}

	/* =========== 좌측메뉴 및 내용 ============ */
	.leftMenu{float: left; width: 20%; height: 1800px; background-color: #ECF0F7; border-right: 3px solid #CDCDCD}
	
	.leftMenu {
		border-right: hidden;
	}
	
	.leftMenu>ul {    	
		margin: 0;
    	padding: 0;
    	list-style: none;
    }
    .leftMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: black;
  	}
	.leftMenu a:hover { background:darkgray; color:white; }
	
	
	/* ========== HOME > 회원관리 > 기업인증내역 ========== */
	.homeNavi {
		margin-top: 50px;
		float: right;
	}
	
	/* ========== 기업인증내역 - 위아래 선 ========== */
	.pageTitle {
		border-top: 3px solid rgb(200, 200, 200);
		border-bottom: 3px solid rgb(200, 200, 200);
		width:930px;
		height: 80px;
		float:right;
		margin-top: 30px;
		margin-bottom: 90px;
	}
	
	/* ========== 기업인증내역 -> 제목 ========== */
	.pageTitle2 {
		padding: 23px;
	}
	
	/* ============= 테이블 영역 ============= */
	
	
	
	
	/* 기업인증내역 테이블 div class */
	.Request {
		width:850px;
		margin-left:300px;
	}
	
	.iqTitle {
		margin-left: 45px;
	}
	
	.scrap1{color:#7f7f7f;}
	.scrap2{font-size:15px}
	.scrap3{color:#f00}
	
	
	#iqTable {
		width: 800px;
		/* margin-left: 80px; */
		float: left;
	}
	
	.iqTable {
		width: 900px;
		margin-left: 45px;
		border-collapse: collapse;
		border: 1px solid lightgray;
		
	}
	
	.iqTr {
		height: 40px;
		
	}
	
	.iqTr1 {
		text-align: center;
		height: 40px;
		border: 1px solid lightgray;
	}
	
	
</style>
</head>
<body>
	<div id="wrap">
		<!-- 공통 헤더 영역 (보금자리, 사이트바로가기, 관리자님, 홈 아이콘)-->
        <header class="logo">
        	<table width="100%" border="0px" style="padding: 20px">
        		<tr>
        			<td width="65%"><label id="title">보금자리</label></td>
        			<td id="goSite" width="320">
        				<img id="arrow" alt="admin_arrow.png" src="../../static/images/admin/admin_arrow.png" 
						width="25px" height="25px">
						<a>사이트 바로가기</a>
						&nbsp;&nbsp;&nbsp;
						<img alt="admin_account.png" src="../../static/images/admin/admin_account.png" 
						width="25px" height="25px">
						<a><label style="font-size: 18px;">관리자 님</label></a>
        			</td>
        			<td width="40" style="float:right;">
        				<img alt="admin_home.png" src="../../static/images/admin/admin_home.png" 
						width="45px" height="45px">
        			</td>
        		</tr>
        	</table>
        </header>
        
        <div class="container">
	        <aside class="leftMenu">
	           	<ul>
			   		<li><a href="#" style="font-size:23px; margin-top:20px;"><b>회원관리</b></a></li>
			    	<li><a href="#">개인회원</a></li>
			    	<li><a href="#">기업회원</a></li>
			    	<li><a href="#">기업인증내역</a></li>
		  			<li><a href="#" style="font-size:23px; margin-top:20px;"><b>신고관리</b></a></li>
			    	<li><a href="#">개인신고내역</a></li>
			    	<li><a href="#">기업신고내역</a></li>
			    	<li><a href="#">신고처리내역</a></li>
			    	<li><a href="#" style="font-size:23px; margin-top:20px;"><b>상품관리</b></a></li>
			    	<li><a href="#">상품사용내역</a></li>
			    	<li><a href="#">결제내역</a></li>
			    	<li><a href="#">환불내역</a></li>
			    	<li><a href="#" style="font-size:23px; margin-top:20px;"><b>광고관리</b></a></li>
			    	<li><a href="#">광고문의내역</a></li>
			    	<li><a href="#">광고처리내역</a></li>
			    	<li><a href="#">광고적용</a></li>
			    	<li><a href="#" style="font-size:23px; margin-top:20px;"><b>이력서관리</b></a></li>
			    	<li><a href="#">이력서 등록현황</a></li>
			   		<li><a href="#" style="font-size:23px; margin-top:20px;"><b>보금자리관리</b></a></li>
			    	<li><a href="#">보금자리 등록현황</a></li>
			    	<li><a href="#">보금자리 심사</a></li>
			    	<li><a href="#" style="font-size:23px; margin-top:20px;"><b>취업박사관리</b></a></li>
			    	<li><a href="#">취업뉴스</a></li>
			    	<li><a href="#">고용복지정책</a></li>
			    	<li><a href="#">자격증정보</a></li>
			    	<li><a href="#">취업박람회/교육일정</a></li>
			    	<li><a href="#" style="font-size:23px; margin-top:20px;"><b>열린마당관리</b></a></li>
			    	<li><a href="#"><b>전국보금자리자랑</b></a></li>
			    	<li><a href="#">&nbsp;보금자리자랑 등록현황</a></li>
			    	<li><a href="#">&nbsp;보금자리자랑 심사</a></li>
			    	<li><a href="#"><b>자유게시판</b></a></li>
			    	<li><a href="#" style="font-size:23px; margin-top:20px;"><b>고객센터관리</b></a></li>
			    	<li><a href="#">묻고답하기</a></li>
			    	<li><a href="#">자주하는질문</a></li>
			    	<li><a href="#">공지사항</a></li>
			    	<li><a href="#">튜토리얼</a></li>
		  		</ul>
	        </aside>
            
            <!— 게시판관리 > 묻고답하기 —>
        	<div class="homeNavi" >
       		 	<label style="font-size:15px;">게시판관리 > Q&A</label>
    	    </div>
        
    	    <!— 기업인증내역 —>
      	  <div class="pageTitle">
      		  	<div class="pageTitle2">
        			<label style="font-size:25px;"><b>묻고답하기</b></label>
        		</div>
     	  </div>
     	  
          <div id="iqTable">
	          <div class="iqTitle" style="font-size:18px">
				 <span><b>문의게시판</b></span>
				 <span class="scrap1">|</span>
	             <span class="scrap2">게시물 총</span>
	             <span class="scrap3">1</span>
	             <span class="scrap4">건</span>
			  </div><br>
		  	
		  	<!— 테이블 —>
           	<table class="iqTable">
           		<tr class="iqTr" style="background-color: #d3d3d3">
           			<th width="5%">선택</th>
           			<th width="5%">번호</th>
           			<th width="10%">작성자</th>
           			<th width="15%">문의유형</th>
           			<th width="45%">제목</th>
           			<th width="10%">등록일</th>
           			<th width="10%">상태</th>
           		</tr>
           		<tr class="iqTr1">
           			<td><input type="checkbox"></td>
           			<td>8</td>
           			<td>관리자</td>
           			<td>입사지원관리</td>
           			<td style="text-align:left; margin-left: 40px">입사지원 취소는 어떻게 해야하나요?</td>
           			<td>20/01/24</td>
           			<td>미답변</td>
           		</tr>
           		<tr class="iqTr1">
           			<td><input type="checkbox"></td>
           			<td>8</td>
           			<td>관리자</td>
           			<td>입사지원관리</td>
           			<td style="text-align:left">입사지원 취소는 어떻게 해야하나요?</td>
           			<td>20/01/24</td>
           			<td>미답변</td>
           		</tr>
           	</table>
            </div>
          </div> 
        
        
        
        
        
        
    </div><!--  wrap영역 End --> -->
</body>
</html>