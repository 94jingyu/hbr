<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
	<<!-- 레프트메뉴바 -->
		<aside class="leftMenu">
	          <div class="left leftblock" id="left1"><br>&nbsp;
	           <label class="myPage" style="font-size:20px;"><b>마이페이지</b></label><br><br>
	           <hr>
	           <br>
	              <ul>
	              <li>
	                  <a href="#" style="font-size:18px;"><b>이력서관리</b></a>
	                  <a href="#" onclick="resumeWrite();" style="font-size:15px;">이력서 등록</a>
	                  <a href="#" onclick="resumeView();" style="font-size:15px;">이력서 현황</a>
	               </li><br>
	               <hr><br>
	               <li>
	               	  <a href="#" style="font-size:18px;"><b>입사지원 관리</b></a>
	                  <a href="#" onclick="applyHistory();" style="font-size:15px;">입사지원목록</a>
	                  <a href="#" onclick="myresumeBusiness();" style="font-size:15px;">내 이력서 열람기업</a>
	                  <a href="#" onclick="" style="font-size:15px;">이력서 열람 제한</a>
	               </li><br>
	             <hr><br>
	             <li>
	             	<a href="#" onclick="recruitBookmark()" style="font-size:18px; color:#2FA599"><b>스크랩보금자리</b></a>
	             </li><br>
	             <hr><br>
	             <li>
	             	<a href="#" onclick="recentView()" style="font-size:18px;"><b>최근 본 보금자리</b></a>
	             </li><br>
	             <hr><br>
	             <li><a href="#" onclick="" style="font-size:18px;"><b>회원정보 관리</b></a>
	                 <a href="#" onclick="" style="font-size:15px;">회원정보수정</a>
	                 <a href="#" onclick="" style="font-size:15px;">회원탈퇴</a>
	             </li><br>
	             <hr><br>
	             <li><a href="#" onclick="myInquiry();" style="font-size:18px;"><b>나의 문의내역</b></a>
	             </li>
	           </ul>
	          </div>
		   </aside><br>
	    
	    
	 <script>
	 
		//페이지 이동
	   function resumeWrite() {
			location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/resumeWrite.jsp";
		}
	   function resumeView() {
			location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/resumeView.jsp";
		}
	  function applyHistory() {
		   location.href = <%= request.getContextPath()%>/views/user_TAEWON/user_applyHistory.jsp";
	   }
	  function recruitBookmark() {
		  location.href = <%= request.getContextPath()%>/views/user_TAEWON/user_recruitBookmark.jsp";
	  }
	  function recentView() {
		  location.href = <%= request.getContextPath()%>/views/user_TAEWON/user_recentView.jsp;
	  }
	  function myInquiry() {
		  location.href = <%= request.getContextPath()%>/views/user_TAEWON/user_inquiryBoard.jsp";
	  	}
	  function myresumeBusiness(){
    	  location.href = "<%=request.getContextPath()%>/res.open";
      }
     </script>
</body>
</html>