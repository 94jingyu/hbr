<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script	src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
* {
	margin: 0;
	padding: 0;
}
/* body {background-color: #fffde7;}  */
#wrap {
	width: 1200px;
	margin: 0 auto;
}

/* 헤더 영역 */
.logo {
	float: left;
	width: 22%;
	height: 130px;
}

.toplogin {
	width: 100%;
	height: 50px;
	padding: 1%;
}

/* 상단메뉴바 */
.topMenu {
	/* display: inline-block; */
	width: 100%;
	height: 50px;
	/* background: #013252; */
	line-height: 30px; /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	vertical-align: middle; /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	text-align: center; /* 글씨 정렬을 가운데로 설정 */
	font-weight: bolder;
	font-size: 20px;
	color: #013252;
}

.topMenu>div {
	float: right;
}

.topMenu>ul {
	float: left;
	margin: 0;
	padding: 0;
	list-style: none;
}

.topMenu>ul>li {
	display: inline-block;
	padding: 0;
}

.topMenu a {
	display: block;
	text-decoration: none;
	padding: 10px 20px;
	color: black;
}

.topMenu a:hover {
	background: #013252;
	color: white;
}

/* 하이퍼텍스트 효과 */
a {
	text-decoration: none;
} /* 하이퍼텍스트 밑줄 효과 없애기 */

/* 광고 영역 */
.ads {
	width: 100%;
	height: 500px;
	clear: both;
}

/* 공고조회 및 로그인 영역 */
.b2_cotainer {
	width: 100%;
	height: 260px;
	background-color: #F0F0F0;
	text-align: center;
}

#selectJob {
	float: left;
	width: 70%;
	height: 250px;
	background-color: #039be5;
	display: inline-block;
}

#login {
	width: 30%;
	height: 250px;
	background-color: #0288d1;
	display: inline-block;
}

/* 일반채용리스트 */
.normalJobList {
	width: 88%;
	font-size: 20px;
	font-weight: bold;
}

#normaljobList {
	font-size: 16px;
	font-weight: bold;
	border: none;
	border-bottom: 3px;
	border-bottom-color: black;
	background-color: lightGray;
	
}


/* 버튼 효과 */
.btns {
	align: center;
}

#loginBtn, #memberJoinBtn, #logoutBtn, #changeInfo, #adminBtn {
	display: inline-block;
	text-align: center;
	background: orangered;
	color: white;
	height: 25px;
	width: 100px;
	border-radius: 5px;
}

#memberJoinBtn, #logoutBtn {
	background: yellowgreen;
}

#loginBtn:hover, #changeInfo:hover, #logoutBtn:hover, #memberJoinBtn:hover,
	#adminBtn:hover {
	cursor: pointer;
}

/* nav */
.nav {
	width: 15%;
	height: 300px;
	border: solid 1px lightgray;
	margin-left: 5px;
}


/* 박다영 스타일 */
/* left 메뉴 */
   .leftblock {
      display: block;
   }
   
   #left2, #left3, #left4 {
      margin-top:10px;
   }
   
   #left1 {
      width: 180px;
      height:850px;
      margin-top:10px;
      align: left;
   }
   
   .myPage {
   	margin-left: 10px;
   }
   
   #left2 {
      height:290px;
   }
   
   #left3 {
      height:260px;
   }
   
   #left4 {
      height:250px;
   }
   
   /* left메뉴 가로선 */
   hr {
      width:90%;
      color:lightgray;
      size:2px;
      margin-left: 5%;
   }
   
   ul {
      list-style:none;
   }

.leftMenu{float: left; width: 20%; height: 580px; margin-left: 30px; margin-top: -28px; text-align: left}

.leftMenu a:hover{
	color: #2FA599;
	font-weight: bold;
}

/* 내용 부분 */
   
.leftMenu>ul {       
   margin: 10;
   padding: 0;
   list-style: none;
}
.leftMenu a {
   display: block;
   text-decoration: none;
   padding: 10px 20px;
   color: black;
}
.left {
      border:2px solid rgb(192, 192, 192);
   }


/* 최근 본 보금자리 작성영역 */
.recentBg {
	width:80%;
	height: 60px;
	background-color: #013252;
	font-size: 20px;
	font-weight:bold;
	color:white;
	line-height: 60px;
	margin-left: 240px;
	text-align: left;
}

#scrapArea {
	width: 80%;
	margin-left: 240px;
}

.scrapBg {
	width:100%;
	height: 60px;
	background-color: #013252;
	font-size: 20px;
	font-weight:bold;
	line-height: 60px;
	color:white;	
}

#scrapTable {
	width: 100%;
	text-align: center;
	border: 1px solid #d3d3d3;
	border-collapse: collapse;
}

#scrapBgArea {
	width:77%;
	height:200px;
	float:center;
	height:200px;
	margin-left: 220px;
}

#tableTr1 {
	background-color: #d3d3d3;
	border-bottom: 1px solid lightgray;
}

tr {
	border-bottom: 1px solid lightgray;
}

.scrap1{color:#7f7f7f;}
.scrap2{font-size:15px}
.scrap3{color:#f00}
.scrap5{{color:#c4c4c4}}


/* footer 영역 */
.footer {border: 1px solid #F0F0F0; width: 1200px; height: 210px; text-align: center; background-color: #afafaf; margin-top: 1000px}
.footerTable{margin: 0 auto; padding: 30px;}

</style>

</head>
<body>
	<%@ include file="../common/E_user_menubar.jsp" %>
	<div id="wrap">

		<hr style="border: solid 2px #013252; width:100%; margin:0 auto"><br>
		<br>
		<!-- 사이트경로 -->
	    <p style="margin-left: 240px; text-align: left">HOME > 마이페이지 > 최근 본 보금자리</p>
		
		<!-- 레프트메뉴바 -->
		<aside class="leftMenu">
             <div class="left leftblock" id="left1"><br>&nbsp;
             <label class="myPage" onclick="userMypage();" style="font-size:20px;"><b>마이페이지</b></label><br><br>
             <hr>
             <br>
                <ul>
                <li>
                      <a href="#" style="font-size:18px;"><b>이력서관리</b></a>
                    <a href="#" onclick="resumeWrite();" style="font-size:15px;">이력서 등록</a>
                    <a href="#" onclick="resumeManage();" style="font-size:15px;">이력서 현황</a>
                 </li><br>
                 <hr><br>
                 <li>
                      <a href="#" style="font-size:18px;"><b>입사지원 관리</b></a>
                    <a href="#" onclick="applyHistory();" style="font-size:15px;">입사지원목록</a>
                    <a href="#" onclick="myresumeBusiness();"style="font-size:15px;">내 이력서 열람기업</a>
                    <a href="#" onclick="resumeLimit();"style="font-size:15px;">이력서 열람 제한</a>
                 </li><br>
               <hr><br>
               <li>
                  <a href="#" onclick="recruitBookmark();" style="font-size:18px;"><b>스크랩보금자리</b></a>
               </li><br>
               <hr><br>
               <li> 
                  <a href="#" onclick="recentViewRecruit();" style="font-size:18px; color: #2FA599; font-weight: bold;"><b>최근 본 보금자리</b></a>
               </li><br>
               <hr><br>
               <li><a href="#" style="font-size:18px;"><b>회원정보 관리</b></a>
                   <a href="#" onclick="changeInfo()" style="font-size:15px;">회원정보수정</a>
                   <a href="#" onclick="del();" style="font-size:15px;">회원탈퇴</a>
               </li><br>
               <hr><br>
               <li><a href="#" onclick="myinquiry();" style="font-size:18px;"><b>나의 문의내역</b></a>
               </li>
             </ul>  
             </div>
         </aside><br>
			
	    <!-- 레프트 메뉴바 End -->
			
		<!-- 최근 본 보금자리 영역 -->
		<div id="recentArea">
			<div class="recentBg">최근 본 보금자리</div><br><br><br>
			
			<div class="" style="font-size:18px; text-align: left">
			  <span><b>최근 본 보금자리</b></span>
			  <span class="recent1">|</span>
              <span class="recent2">총</span>
              <span class="recent3">1</span>
              <span class="recent4">건</span>
              <span class="recent5">/ 20건</span>
			</div><br>
			
			<div id="scrapBgArea">
				<table id="scrapTable">
					<tr id="tableTr1">
						<th class="tableTh" style="width:10%;"></th>
						<th class="tableTh" style="width:20%; height:35px;">근무지</th>
						<th class="tableTh" style="width:40%;">회사명 / 모집내용</th>
						<th class="tableTh" style="width:15%;">급여(원)</th>
						<th class="tableTh" style="width:15%;">등록일</th>
					</tr>
					<tr id="tableTr2" style="height: 100px">
						<td style="border-right:hidden">
            				<img alt="star.png" src="../../static/images/user/star.png" width="25px" height="25px" style="cursor:pointer">
            			</td>
						<td>서울 강남구</td>
						<td style="text-align:left; margin-left: 150px">
							<label style=""><b>(주)덕승재</b></label><br>
							<label>한식남녀 서빙 245만 주5일 220만</label><br>
							<label>10:00 ~16:00</label>
						</td>
						<td>월급 2,400,000</td>
						<td>20/01/29</td>
					</tr>
					<tr id="tableTr2" style="height: 100px">
					<td style="border-right:hidden; align: center">
            				<img alt="star.png" src="../../static/images/user/star.png" width="25px" height="25px" style="cursor:pointer">
            			</td>
						<td>서울 송파구</td>
						<td style="text-align:left; margin-left: 60px">
							<label style=""><b>송도어점 송파점</b></label><br>
							<label>[시간조절가능] 주부환영(음식 서빙 / 조리 보조)</label><br>
							<label>09:30 ~ 16:00  (주 5일)</label>
						</td>
						<td>월급 2,400,000</td>
						<td>20/01/29</td>
					</tr>
					<tr style="height: 100px">
						<td style="border-right:hidden; align: center">
            				<img alt="star.png" src="../../static/images/user/star.png" width="25px" height="25px" style="cursor:pointer">
            			</td>
						<td>서울 송파구</td>
						<td style="text-align:left; margin-left: 60px">
							<label style=""><b>송도어점 송파점</b></label><br>
							<label>[시간조절가능] 주부환영(음식 서빙 / 조리 보조)</label><br>
							<label>09:30 ~ 16:00  (주 5일)</label>
						</td>
						<td>월급 2,400,000</td>
						<td>20/01/29</td>
					</tr>
				</table>
			</div> <!-- 일반채용 리스트 end -->
		</div>
		
	
	
	
	<div class="footer">
			<table class="footerTable">
				<tr>
					<td>홈&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;광고문의&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;제휴문의&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;인재채용&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;이용약관&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>개인정보처리방침</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;고객센터</td>
				</tr>
				<tr>
					<td colspan="7" style="height: 20px;"></td>
				</tr>
				<tr>
					<td colspan="7">고객센터: 1588-9350(평일 09:00 ~ 19:00 토요일 09:00 ~ 15:00)&nbsp;&nbsp;&nbsp;FAX : 02-123-456&nbsp;&nbsp;&nbsp;Email : sunshine@kh.co.kr</td>
				</tr>
				<tr>
					<td colspan="7">서울특별시 강남구 강남구 테헤란로14길 6 해볼래&nbsp;&nbsp;&nbsp;대표 : 윤햇살&nbsp;&nbsp;&nbsp;사업자등록번호 : 110-81-34859</td>
				</tr>
				<tr>
					<td colspan="7">통신판매업 신고번호 : 2020-서울역삼-0287호&nbsp;&nbsp;&nbsp;직업정보제공사업 신고번호 : 서울청 제2020-01호</td>
				</tr>
				<tr>
					<td colspan="7" style="height: 20px;"></td>
				</tr>
				<tr>
					<td colspan="7">Copyright ⓒ Sunshine Corp. All Right Reserved.</td>
				</tr>
			</table>
	    
	    </div> 
	<%-- <%@ include file="../guide/footer.jsp" %> --%>
	</div> <!-- 전체 wrap END -->
	
	
	<script>
	 
		//레프트바 페이지 이동
	    function resumeWrite() {
           location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/resumeWrite.jsp";
           }
         function resumeManage() {
            location.href = "<%=request.getContextPath()%>/res.list";
         }
         function recruitBookmark() {
            location.href = "<%=request.getContextPath()%>/views/user_TAEWON/user_recruitBookmark.jsp";
         }
         function myinquiry(){
            <%-- location.href = "<%=request.getContextPath()%>/select.iq"; --%>
            location.href = "<%=request.getContextPath()%>/views/user_TAEWON/user_MyInquiryHistory.jsp";
         }
         function applyHistory(){
            location.href = "<%=request.getContextPath()%>/selectApplyList.ap";
         }
         function recentViewRecruit(){
            location.href = "<%=request.getContextPath()%>/views/user_TAEWON/user_recentView.jsp";
         }
         function changeInfo() {
            location.href = "<%=request.getContextPath()%>/views/common/E_UpdateInfoPage1.jsp";
         }
         function del() {
            location.href = "<%=request.getContextPath()%>/views/common/E_DeletePage1.jsp";
         }
         function myresumeBusiness(){
          location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/myresumeBusiness.jsp";
         }
         function resumeLimit() {
          location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/resumeLimit.jsp";
         }
         function userMypage() {
           
           location.href = "<%=request.getContextPath()%>/user.my";
        }
	      
     </script>
	
	
	
	
	
	
	
</body>
</html>












