<%@page import="oracle.net.aso.r"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" 
	import="java.util.*, 
			com.kh.hbr.resume.model.vo.*,
			com.kh.hbr.recruit.model.vo.*,
			java.lang.System.*"
%>

<%@ page import="com.kh.hbr.board.jengukBoard.model.vo.Attachment" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import = "com.kh.hbr.resume.model.vo.Resume" %>


<%
	Resume resume = (Resume) session.getAttribute("selectResume");
	Recruit recruit = (Recruit) session.getAttribute("searchRecruit");
	int click = recruit.getClickCount();
	
	ArrayList<Attachment> fileList = (ArrayList<Attachment>) session.getAttribute("fileList");
	Attachment titleImg = fileList.get(0);
	
	/* ArrayList<Resume> list = (ArrayList<Resume>) session.getAttribute("selectResume");
	System.out.println("list size : " + list.size()); */
	
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script	src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
* {
	margin: 0;
	padding: 0;
}

button {
	cursor: pointer;
}

/* body {background-color: #fffde7;}  */
#wrap {
	width: 1200px;
	margin: 0 auto;
}

/* 헤더 영역 */
.logo {
	float: left;
	width: 22%;
	height: 130px;
}

.toplogin {
	width: 100%;
	height: 50px;
	padding: 1%;
}

/* 상단메뉴바 */
.topMenu {
	/* display: inline-block; */
	width: 100%;
	height: 50px;
	/* background: #013252; */
	line-height: 30px; /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	vertical-align: middle; /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	text-align: center; /* 글씨 정렬을 가운데로 설정 */
	font-weight: bolder;
	font-size: 20px;
	color: #013252;
}

.topMenu>div {
	float: right;
}

.topMenu>ul {
	float: left;
	margin: 0;
	padding: 0;
	list-style: none;
}

.topMenu>ul>li {
	display: inline-block;
	padding: 0;
}

.topMenu a {
	display: block;
	text-decoration: none;
	padding: 10px 20px;
	color: black;
}

.topMenu a:hover {
	background: #013252;
	color: white;
}

/* 하이퍼텍스트 효과 */
a {
	text-decoration: none;
} /* 하이퍼텍스트 밑줄 효과 없애기 */

/* 공고조회 및 로그인 영역  */
.b2_cotainer {
	width: 100%;
	height: 260px;
	background-color: #F0F0F0;
	text-align: center;
}

/* 버튼 효과 */
	.btns {
		align:center;
	}

/* 버튼 효과 */
.btns {
	align: center;
}

#loginBtn, #memberJoinBtn, #logoutBtn, #changeInfo, #adminBtn {
	display: inline-block;
	text-align: center;
	background: orangered;
	color: white;
	height: 25px;
	width: 100px;
	border-radius: 5px;
}

#memberJoinBtn, #logoutBtn {
	background: yellowgreen;
}

#loginBtn:hover, #changeInfo:hover, #logoutBtn:hover, #memberJoinBtn:hover,
	#adminBtn:hover {
	cursor: pointer;
}


/* 채용공고 상세페이지 조회*/
/* 가장 큰 DIV */
.jari_register {

height : 100%;
/* margin-top : 20px;  */
padding : 30px;
width: 1100px;
margin: 0 auto;
}
	/* 상단에 패딩 */
.t{
padding-top: 10px;
}
/* 하단에 패딩 */
.b{
padding-bottom: 10px;
}
/* 테이블 글씨 가운데 정렬 */
table{
text-align :center;
}
/* 테이블안에 나만 왼쪽 정렬 */
#align_left{
text-align: left;
padding-left:10px;
}
#align_left1{
text-align: left;
}
.align_left1{
text-align: left;
}
.align_left2{
vertical-align:top;
}
/* 보금자리 미리보기 요약  배경색*/
#table{
background:#ECF0F7;
}
/* 보금자리 미리보기 상세내용 테이블 테두리 */
.line{
border-collapse:collapse;
border :1px solid #afafaf;
}
/* 보금자리 미리보기 상세내용 작성내용 div */
 .content{
margin-left:10px;
margin-top:20px;
width:1050px;
border:1px solid green;
}

/* 뒤로가기 버튼 */
.backButton {
	float: right;
	margin-right: 10px;
	margin-top: 25px;
	background: #013252;
	border: 0px;
	color: white;
	height: 35px;
	width: 70px;
	text-size: 15px;
}

/* 입사 지원하기 버튼 */
.applyBtnArea {
	margin-right: 550px;
	
}

.applyBtn {
	width: 140px;
	height: 50px;
	background-color: #013252;
	border: none;
	color: white;
	font-weight: bold;
	font-size: 18px;
	align: center;
	margin: 0 auto;
}

.applyBtn2 {
	width: 140px;
	height: 50px;
	background-color: #013252;
	border: none;
	color: white;
	font-weight: bold;
	font-size: 18px;
	align: center;
	margin: 0 auto;
}


/* 스크랩 북마크 */
.bookmark {
	width: 100px;
	height: 40px;
	font-weight: bold;
	font-size: 17px;
	color: #666666;
	border: none;
	background-color: #FFFFFF;
	cursor: pointer;
	display:none;
}

/* 신고하기 버튼 */
.reportBtn {
	width: 80px;
	height: 30px;
	font-weight: bold;
	color: white;
	text-align: center;
	background-color: #BA4949;
	border: none;
	border-radius: 7px;
	margin: 0 auto;
	line-height: 30px;
	cursor: pointer;
	display:none;
}

/* 신고팝업 영역 */
.reportPage {
	width: 350px;
	height: 400px;
	border: 2px solid gray;
	text-align: left;
	margin: 0 auto;
	padding: 20px;
	display:none;
}

/* 모달 적용 */
#reportPage {
	width:900px;
	display:none;
}

#reportPage {
	position: relative;
	width: 350px;
	height: 100%;
	z-index: 1;
	background: rgba(0, 0, 0, 0.8);
	margin: 0 auto;
}
 /* 모달창에 관련된 div 설정창 */
#reportPage .reportPage {
	
	height:100%;
	margin:20px auto;
	padding:30px 30px;
	background:#ffffff;
	border:4px solid darkgray;
}

/* 모달레이어 */
.reportLayer {
	position:fixed;
	top:0;
	left:0;
	width:100%;
	height:100%;
	background: rgba(0, 0, 0, 0.8);
	z-index:-1;
}

/* 신고, 취소버튼 */
.reportSubmit {
	width:70px;
	height: 30px;
	background: #BA4949;
	font-weight: bold;
	color: white;
	border: none;
}

.reportCancel {
	width:70px;
	height: 30px;
	background: lightgray;
	font-weight: bold;
	color: gray;
	border: none;
}

/* 이력서 조회/선택 팝업 */

.resumeSelectArea {
	width:100%;
	/* height: 300px; */
	/* border: 1px solid red; */
	align: center;
	text-align: left;
	/* padding: 30px; */
}

.resultSelectInner {
	width: 500px;
	/* height: 340px; */
	padding: 30px;
	border: 4px solid darkgray;
	margin: 0 auto;
}

.resumeChoiceBtn {
	width: 100px;
	height: 40px;
	border: none;
	background-color: #013252;
	color: white;
	font-size: 15px;
	font-weight: bold;
	margin: 0 auto;
	align: center;
}

.resumeCancelBtn {
	width: 100px;
	height: 40px;
	border: none;
	background-color: #7D7777;
	font-size: 15px;
	font-weight: bold;
	margin: 0 auto;
	align: center;
	color: darkgray;	
}

/* 이력서 선택팝업 모달 */
.resumeSelectArea {
	width: 900px;
	display:none;
}
.resumeSelectArea{
	position: relative;
	width: 450px;
	height: 100%;
	z-index: 1;
	background: rgba(0, 0, 0, 0.8);
	margin: 0 auto;
}

/* 모달창에 관련된 div 설정창 */
.resumeSelectArea .resultSelectInner {
	width: 100%; /*sds*/
	height:100%;
	margin:20px auto;
	padding:30px 30px;
	background:#ffffff;
	border:4px solid darkgray;
}

/* 모달레이어 */
.ApplyResumeLayer {
	position:fixed;
	top:0;
	left:0;
	width:100%;
	height:100%;
	background: rgba(0, 0, 0, 0.8);
	z-index:-1;
}


</style>

</head>
<body>
<%@ include file="../common/E_user_menubar.jsp" %>
	
<%
	String managerPhone1 =recruit.getManagerPhone().substring(0,3);
	String managerPhone2 =recruit.getManagerPhone().substring(3,7);
	String managerPhone3 =recruit.getManagerPhone().substring(7,11);
	
	String address[] =recruit.getDetailArea().split(" ");
	String address1[] =recruit.getDetailArea().split("-");
	
	String status = recruit.getStatus();
	
%>
	
	

	<!-- 전체영역 -->
	<div id="wrap">
		
		<form action="<%= request.getContextPath()%>/insert.ap?num=<%= recruit.getRecruitNo() %>" name="" method="post">
		
		<hr style="border: solid 2px #013252; width:100%; margin:0 auto"><br>
		<br>
		
		<!-- 채용정보 미리보기 -->
		<p style="text-align:left">HOME > 채용정보</p><br><br><br>
	    	<div>
    		 	<table style="width:100%">
    		 		<tr>
    		 			<td style="margin-right:900px; color: #013252; font-size: 20px; text-align:left; padding-left:20px">
		    		 		<b>이 보금자리의 현재 조회수 : <b style="color:#BA4949;"><%= recruit.getClickCount() %></b></b>
    		 			</td>
    		 			<td>
    		 				<button class="bookmark" name="">
    		 					<img alt="star.png" src="../../static/images/user/star.png" width="20px" height="20px;">&nbsp;&nbsp;스크랩
    		 				</button>
    		 			</td>
    		 			<td>
    		 				<div class="reportBtn" name="">신고</div>
    		 			</td>
    		 		</tr>
    		 	</table><br>
    		 	
	    		 <!-- 신고하기 팝업페이지 : 모달 -->
	    		 <div id="reportPage">
		    		 <div class="reportPage">
		    		 	<div class="reportInner">
			    		 	<div style="font-size: 20px; font-weight:bold">
			    		 		<img src="../../static/images/user/siren.png" width="30px" height="30px">
			    		 		<b>보금자리 신고</b></div><br>
			    		 	<hr style="border: 1px solid #d3d3d3"><br>
			    		 	<div>
			    		 		<b>신고대상 보금자리 정보</b><br><br>
			    		 		<ul style="margin-left: 20px">
				    		 		<li><div><b>공고번호</b> : <span><%= recruit.getRecruitNo() %></span></div></li>
				    		 		<li><div><b>공고제목</b> : <span><%= recruit.getRecruitTitle() %></span></div></li>
			    		 		</ul>
			    		 	</div><br><br>
			    		 	<hr style="border: 1px solid #d3d3d3"><br>
			    		 	<div><b>신고사유 선택</b><br>
			    		 		<div style="width:317px; height:90px; border:1px solid #d3d3d3; align:center; padding:15px; margin-top:10px">
			    		 			<input type="radio" id="ad" name="reportReason"><label for="ad">&nbsp;&nbsp;&nbsp;광고(성인광고 포함)</label><br>
			    		 			<input type="radio" id="fuck" name="reportReason"><label for="fuck">&nbsp;&nbsp;&nbsp;욕설</label><br>
			    		 			<input type="radio" id="porn" name="reportReason"><label for="porn">&nbsp;&nbsp;&nbsp;음란</label><br>
			    		 			<input type="radio" id="etc" name="reportReason"><label for="etc">&nbsp;&nbsp;&nbsp;기타</label>
			    		 			<input type="text" class="etcText" style="width:240px"><br>
			    		 		</div>
			    		 	</div><br>
			    		 	<div align="center" style="">
				    		 	<button style="" class="reportSubmit" name="asdsad">신고하기</button>
				    		 	<button style="width:70px; height: 30px" class="reportCancel" name="asdsad">취소하기</button>
			    		 	</div>
		    		 	</div><br>
		    		 </div>
		    		 
		    		 <!-- 모달용 레이어 -->
		    		 <div class="reportLayer"></div>
	    		 </div><!-- 신고하기 팝업 End -->
	    		 
	    		 <div id="table">
	    		 <form action="">
	    		 	<table>
	    		 		<tr>
	    		 			<td colspan="8" width="100px" height="100px" id="align_left"><h3><%=recruit.getCompanyName() %></h3>
	    		 			<h2><%=recruit.getRecruitTitle() %></h2></td>
	    		 			
	    		 			<td width="200px" height="100px">
	    		 			<!-- <img alt="duck.png" src="../../static/images/register/duck.png" width="150px" height="80px" style="margin-left: auto; margin-right: auto; display: block";> -->
	    		 			<img class="contentImg" width="156px" height="60px" style="border: 2px solid gray; padding:2px" alt="" src="<%=request.getContextPath()%>/thumbnail_uploadFiles/<%=titleImg.getChangeName()%>">
	    		 			</td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="100px" height="100px"><img alt="money.png" src="../../static/images/register/money.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="120px" height="100px" id="align_left1"><h4><%=recruit.getSalaryType() %><br> <%= recruit.getSalary() %> 원</h4></td>
	    		 			<td width="100px" height="100px"><img alt="calendar.png" src="../../static/images/register/calendar.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="120px" height="100px" id="align_left"><h4>요일<br><%=recruit.getJobDate() %></h4></td>
	    		 			<td width="100px" height="100px"><img alt="clock.png" src="../../static/images/register/clock.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="120px" height="100px" id="align_left1"><h4>시간<br><%=recruit.getJobTime() %></h4></td>
	    		 			<td width="90px" height="100px"><img alt="placeholder.png" src="../../static/images/register/placeholder.png" width="60px" height="60px" style="margin-left: auto; margin-right: auto; display: block";></td>
	    		 			<td width="140px" height="100px" id="align_left1"><h4>지역<a href="#" style="color:red;"> 지도></a><br><%=address[0] %> <%=address[1] %></h4></td>
	    		 			<td width="200px" height="100px" id="align_left"><h4><%=recruit.getCompanyName() %><br>사업내용<br><%=recruit.getBusinessContent() %></h4></td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="110px" height="50px"><h3>근무조건</h3></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="110px" height="50px"><h3>자격조건</h3></td>
	    		 			<td width="110px" height="50px"></td>
	    		 			<td width="90px" height="50px"></td>
	    		 			<td width="90px" height="50px"></td>
	    		 			<td rowspan="2" width="200px" height="100px"><br><br><h3>마감시간</h3><h4></h4></td>
	    		 		</tr>
	    		 		
	    		 		<% 
	    		 			String CareerChoice = recruit.getCareerChoice();
							String EduYn = recruit.getEduYn();
	    		 		%>
	    		 		
	    		 		<tr>
	    		 			<td width="110px" height="50px" class="align_left2"><b>근무유형</b></td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><%=recruit.getJobType() %></td>
	    		 			<td width="110px" height="50px" class="align_left2"><b>근무기간</b></td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><%=recruit.getJobTime() %></td>
	    		 			<td width="110px" height="50px" class="align_left2"><b>경력</b></td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><% if(CareerChoice.equals("경력")){ %><%=CareerChoice%> <%=recruit.getCareerPeriod()%>년<% }else{ %><%=recruit.getCareerChoice()%><%} %></td>
	    		 			<td width="90px" height="50px" class="align_left2"><b>학  력</b></td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><% if(EduYn.equals("선택")){ %><%=recruit.getEducation()%><% }else{ %><%=EduYn%><%} %></td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="110px" height="50px" class="align_left2"><b>근무요일</b></td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><%=recruit.getJobDate() %></td>
	    		 			<td width="110px" height="50px" class="align_left2"><b>근무직종</b></td>
	    		 			<td width="110px" height="50px" class="align_left1 align_left2"><%=recruit.getRecruitMajor() %></td>
	    		 			<td width="110px" height="50px" class="align_left2"><b>우대/가능</b></td>
	    		 			
	    		 			<% 
	    		 			   String preference = recruit.getPreference();
	    		 			   String welfare = recruit.getWelfare(); 
	    		 			   String detailText = recruit.getDetailText();
	    		 			%>
	    		 			
	    		 			<td rowspan ="2" width="110px" height="50px" class="align_left1 align_left2"><% if(preference==null){ %>-<% }else{ %><%=recruit.getPreference()%><%} %></td>
	    		 			<td width="90px" height="50px" class="align_left2"><b>복리후생</b></td>
	    		 			<td rowspan ="2" width="110px" height="50px" class="align_left1 align_left2"><% if(welfare==null){ %>-<% }else{ %><%=recruit.getWelfare()%><%} %></td>
	    		 			<td rowspan="2" width="200px" height="100px"><h2 style="color:red;"><%=recruit.getExpirationDate()%></h2><br><br></td>
	    		 		</tr>
	    		 		<tr>
	    		 			<td width="110px" height="50px" class="align_left2"><b>급여</b></td>
	    		 			<td colspan="3" width="110px" height="50px" class="align_left1 align_left2"><b><%=recruit.getSalaryType() %></b> : <%=recruit.getSalary() %> 원</td>
	    		 			

	    		 			
	    		 		</tr>
	    		 	</table>
	    		 </form>
	    		</div><!-- 하늘색 요약 테이블 div 끝 -->
	    		<br><br>
	    		
	    		<!-- 입사지원 시 입력서 선택팝업 시작 -->
	    		<form action="">
	    		<div class="resumeSelectArea">
	    			<div class="resultSelectInner">
	    				<div style="font-weight: bold; font-size: 20px">
	    					<img src="../../static/images/user/apply.png" width="30px" height="30px"">
	    					<p>입사지원하기</p>
	    				</div><br>
	    				<hr style="height: 4px; background: lightgray; border:none"><br><br>
	    				<div style="font-weight: bold; font-size: 18px;">지원공고<br>
	    					<div style="width:350px; height:20px; color:gray">
	    						<p><%= recruit.getRecruitTitle() %></p>
	    					</div>
	    				</div><br><br>
	    				<div style="font-weight: bold; font-size: 18px;">기업명<br>
	    					<div style="width:350px; height:20px; color:gray">
	    						<%= recruit.getCompanyName() %>
	    					</div>
	    				</div><br><br>
	    				<hr style="height: 2px; background: lightgray; border:none"><br>
	    				<div><p style="font-weight: bold; font-size: 16px;">이력서선택</p><br>
	    					
	    					
	    					<div style="margin-bottom: 10px">
		    					<div>
		    						<input type="radio" name="radio" id="radio1" value="1" checked>&nbsp;&nbsp;
		    						<label for="radio1" id="radio1Lab"></label>
		    						<input id="hidden1" type="hidden" name="resumeNo">
			    				</div>
		    					<div>
		    						<input type="radio" name="radio" id="radio2" value="2">&nbsp;&nbsp;
		    						<label for="radio2" id="radio2Lab"></label>
		    						<input id="hidden2" type="hidden" name="resumeNo">
		    					</div>
		    					<div>
		    						<input type="radio" name="radio" id="radio3" value="3">&nbsp;&nbsp;
		    						<label for="radio3" id="radio3Lab"></label>
		    						<input id="hidden3" type="hidden" name="resumeNo">
		    					</div>
	    					</div>
	    					
	    					
	    				</div><br>
	    				<hr style="height: 4px; background: lightgray; border:none"><br>
	    				<p style="font-size: 14px; color:gray">작성하신 이력서 내역이 없으면 입사지원이 불가능합니다.<br>
	    					이력서 작성을 먼저 진행해주세요.
	    				</p><br><br>
	    				<div style="align: center">
		    				<button type="button" class="resumeChoiceBtn" style="margin-left: 120px" onclick="apply()">지원하기</button> <!-- "/insert.ap" 서블릿 연결 -->
		    				<button class="resumeCancelBtn" style="margin: 0 auto">취소하기</button>
	    				</div>
	    			</div>
					
					<!-- 모달용 레이어 -->
		    		<div class="ApplyResumeLayer"></div>
	    		</div>
	    		</form>
	    	
	    	
	    	
	    	<%-- 	
	    		<script>
	    			
	    			//채용공소 상세보기에서 입사지원 - 공고명 recruitTitle 불러오기
	    			$(function(){
	    				
	    				$ajax({
	    					url: "<%=request.getContextPath()%>/selectResume.rs",
	    					data: ,
	    					type: "post",
	    					success: function(data) {
	    				});
	    				
	    			});
	    			
	    		
	    		
	    		</script>
	    		 --%>
	    		
	    		
	    		<!-- 지원하기 버튼 -->
	    		<div class="applyBtnArea">
					
					<%-- <% if(list.size() <= 0) { %>	    			
	    			<button type="button" id="applyBtn2" class="applyBtn2" onclick="firstApplyBtn2()">지원하기</button>
					<% } else {  %> --%>
					<button type="button" id="applyBtn" class="applyBtn" onclick="firstApplyBtn()">지원하기</button>
					<%-- <% } %> --%>
	    		</div><br><!-- 지원하기 버튼 End -->
	    		
	    		
	    		
	    		<%-- <%
	    			} else { %>
	    			<script>
	    			location.href="views/common/E_LoginPage.jsp";
	    			</script>
	    		<%
	    			}
	    		%> --%>
	    		 
	    		 
	    		 
	    		<!-- 상세모집 내용 회사 정보 근무 지역  tap부분  -->
	    		 <div>
	 				<table class="line">
	 					<tr id="table">
	 						<td width="400px" height="60px"  class="line1" onclick=""><h3 style="cursor:pointer">상세모집내용</h3></td>
	 						<td width="400px" height="60px"  class="line2" style="border:1px solid #afafaf;"><h3 style="cursor:pointer">회사정보</h3></td>
	 						<td width="400px" height="60px"  class="line3"><h3 style="cursor:pointer">근무지역</h3></td>
	 					</tr>
	 					<tr>
	 						<td id="align_left" colspan="3" class="line recruitContent">
	 						
	 								<br>
	 								<h4>[모집내용]</h4>
	 								<p>- 모집직종  : <%=recruit.getRecruitMajor() %><br>
	 								<p>- 근무유형 :  <%=recruit.getJobType()%><br>
	 								<p>- 근무요일  :  <%=recruit.getJobDate()%><br>
	 								<p>- 근무시간  : <%=recruit.getJobTime()%><br>
	 								<p>- 급여  : <%=recruit.getSalaryType()%> <%=recruit.getSalary()%><br>
	 								</p>
	 								<h4>[우대·가능]</h4>
	 									<p><% if(preference==null){ %>-<% }else{ %><%=recruit.getPreference()%><%} %></p>
	 								<h4>[복리후생]</h4>
	 									<p><% if(welfare==null){ %>-<% }else{ %><%=recruit.getWelfare()%><%} %></p>
	 								<h3>* 전화 연락시 "보금자리 구인정보 보고 전화 드렸어요"라고 말씀해주세요.</h3>
	 								<br>
	 								<h4><% if(detailText==null){ %>-<% }else{ %><%=recruit.getDetailText()%><%} %></h4>
	 								<br>
	 							
	 							<br>
	 						</td>
	 					</tr>
	 					<tr>
	 						<td id="align_left" colspan="3" class="line CompanyContent" style="display: none;">
	 						
	 								<table>
	 									<tr>
	 										<td rowspan="2" width="300px" style="text-align: left; padding-left:20px;">
	 										<h2> 회사정보</h2>
	 										<img class="contentImg" width="200px" height="80px" alt="" src="<%=request.getContextPath()%>/thumbnail_uploadFiles/<%=titleImg.getChangeName()%>">
	 										<br><h3><%=recruit.getCompanyName()%></h3>
	 										<p>[회사주소]<br>
	 										<%=address1[0]%><br>
	 										<%=address1[1]%><br>
	 										[사업내용]<br>
	 										<%=recruit.getBusinessContent()%></p>
	 										</td>
	 										<td rowspan="2" width="420px" style="text-align: left; padding-left:20px;">
	 										<h2>채용담당자 정보</h2>
	 										<br>
	 										<h3>[채용담장자] <%=recruit.getManagerName()%></h3>
	 										<h3>[대 표 번 호] <%=recruit.getCompanyPhone()%></h3>
	 										<h3>[담장자전화] <%=managerPhone1%>-<%=managerPhone2%>-<%=managerPhone3%></h3>
	 										<h3>[ 이 메 일  ] <%=recruit.getManagerEmail()%></h3>
	 										</td>  
	 										<td width="380px" height="140px"  style="text-align: left; padding-left:20px;">
	 										<img alt="phone.png" src="../../static/images/register/phone.png" width="30px" height="30px" style="float: left"><h3>&nbsp;&nbsp;전화문의 할 경우</h3><br>
	 										<label style="font-size: smail">"보금자리에서 보고 전화드렸어요."<br>&nbsp;라고 하시면빠른 문의가 가능합니다. </label></h4>
	 										</td>
	 									</tr>
	 									<tr>
	 										<td width="380px" height="140px"  style="text-align: left; padding-left:20px;">
	 										<img alt="alert.png" src="../../static/images/register/alert.png" width="30px" height="30px" style="float: left"><h3>&nbsp;&nbsp;주의사항</h3><br>
	 										<label style="font-size: smail"> 통장 / 신분증 / 비밀번호 요구에는<br>절대 응하지 마세요. <br> 사기나 범죄에 이용될 수 있습니다. </label></h4>
	 										</td>
	 									</tr>
	 								</table>
	 							
	 							<br>
	 						</td>
	 					</tr>
	 					<tr>
	 						<td id="align_left" colspan="3" class="line AreaContent" style="display: none;" >
	 						
	 								<table>
	 									 <tr>
	 									 	<td width="550px" height="50px" style="text-align: left;"><%=recruit.getDetailArea()%> </td>
	 									 	<td width="600px" height="50px"><img alt="alert.png" src="../../static/images/register/alert.png" width="20px" height="20px">&nbsp;&nbsp;근무지 위치를 나타내며 호사 소재지와 일치하지 않을 수 있습니다.</td>
	 									 </tr>
	 									<tr>
	 										<td colspan="2"><img alt="map.png" src="../../static/images/register/map.png" width="100%" height="400">
	 									</tr>	 
	 								</table>
	 							<br>
	 						</td>
	 					</tr>
	 					<table>
	 						<tr>
        						<td width="500px"></td>
        						<td width="100px" height="40px"><button class="backButton" style="width:120px; height:50px; font-size: 18px; font-weight: bold" onclick="back();">
        							<b>목록으로</b></button>
        						</td>
        					</tr>
	 					</table>
	 				</table>
	 			
	 		</div><!-- 공고 상세보기내용 tap부분 끝 -->
	 		
	    	
	    	
	 		<div><!-- 공간띄우기용 div -->
	    	<br><br><br>
	    	</div><!-- 공간띄우기 끝~! -->
            <section class=""></section>
		</div>
		
		</form>
		
		<br><br><br>
		<%@ include file="../guide/footer.jsp" %>
	</div> <!-- 전체 wrap영역 End -->
	
	
	<script>
	
	//신고하기 팝업 모달
	$(".reportBtn").click(function(){
		$("#reportPage").attr("style", "display:block");
	});
	$(".reportCancel").click(function(){
		$("#reportPage").attr("style", "display:none");
	});
    
	//신고하기
	$(".reportSubmit").click(function() {
		var report = confirm("신고하시겠습니까?");
		
		if(report) {
			alert("신고완료");
			location.replace();
		} else {
			location.replace();
		}
	});
	
	//입사지원 팝업 모달
	$(".applyBtn").click(function(){
		$(".resumeSelectArea").attr("style", "display:block");
	});
	
	$(".resumeCancelBtn").click(function(){
		$(".resumeSelectArea").attr("style", "display:none");
	});

	
	/* //이력서 보유x회원 : 입사지원 제한	
	function firstApplyBtn2() {
		alert("작성하신 이력서가 없습니다.\n입사지원을 원하시면 이력서 작성을 먼저 진행해주세요.");
	} */
	
	
	
	
	
	//입사지원 이력서 선택
	function apply(){
		
		var apply = confirm("선택하신 기업에 입사지원하시겠습니까?");
		
		if(apply) {
			alert("입사지원신청을 완료하였습니다.\n입사지원내역은 마이페이지에서 확인가능합니다.")
			
			//라디오 버튼체크 확인
			var applyCheck = $("input[name=radio]:checked").val();
			var resumeNo;
			var recruitNo = <%= recruit.getRecruitNo()%>;
			
			console.log("resumeNo: " + resumeNo);
			console.log("recruitNo : " + recruitNo);
			
			switch(applyCheck) {
			case "1" : resumeNo = $("#hidden1").val(); break;
			case "2" : resumeNo = $("#hidden2").val(); break;
			case "3" : resumeNo = $("#hidden3").val(); break;
			}

			/* 서블릿 요청 시 변수 지정 */ 
			
			location.href = "<%= request.getContextPath()%>/insert.ap?resumeNo=" + resumeNo + "&recruitNo=" + recruitNo;
			
		} else {
			
			location.replace("user_apply.jsp");
		}
	};
		
	<%-- //입사지원버튼 클릭 : 입사지원신청
	function apply() {
		location.href = "<%= request.getContextPath()%>/insert.ap?num=" + num;
		location.href = "<%= request.getContextPath()%>/selectOneSearch.rc?num=" + num;
	} --%>
	
	
	//이력서 불러오기 ajax
	$(function(){
		
		$.ajax({
			url: "<%=request.getContextPath()%>/selectResume.rs",
			data: {memberNo:"<%= ((Member)session.getAttribute("loginUser")).getMemberNo()%>"},
			type: "post",
			success: function(data) {
				
				/* console.log(decodeURIComponent(data[0].resumeTitle));
				console.log(data[1].resumeTitle); */
				
				for(var key in data) {
					
					switch(key) {
						case "0" : $("#radio1Lab").text(decodeURIComponent(data[0].resumeTitle));
									$("#hidden1").val(data[0].resumeNo); break;
						case "1" : $("#radio2Lab").text(decodeURIComponent(data[1].resumeTitle));
								   	$("#hidden2").val(data[1].resumeNo); break;
						case "2" : $("#radio3Lab").text(decodeURIComponent(data[2].resumeTitle));
								   	$("#hidden3").val(data[2].resumeNo); break;
					};
					
				}
						var a = $("input[type=hidden]").eq(0).val();
						console.log(a);
						
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	
	
	<%-- //sadsadsad
	$(function() {
		
		if()
		
		$.ajax({
			url: "<%=request.getContentLength()%>"/
			
						
		});
		
	}); --%>
	
	
	
	
	
	//공고내용 hide and show
	$(function(){
    	$(".line2").click(function(){
    		$(".CompanyContent").show();
    		$(".recruitContent").hide();
    		$(".AreaContent").hide();
    	});
    });
    $(function(){
    	$(".line3").click(function(){
    		$(".CompanyContent").hide();
    		$(".recruitContent").hide();
    		$(".AreaContent").show();
    	});
    });
    $(function(){
    	$(".line1").click(function(){
    		$(".CompanyContent").hide();
    		$(".recruitContent").show();
    		$(".AreaContent").hide();
    	});
    });
	
	
	
	
	
	
	//뒤로가기
	function back() {
		histroy.go(-1);
	}
	
	
	
	</script>
</body>
</html>












