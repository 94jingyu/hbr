<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page
	import="java.util.ArrayList, com.kh.hbr.payment.model.vo.ItemUse, com.kh.hbr.payment.model.vo.PageInfo"
 %>

<%
	 ArrayList<ItemUse> useList = (ArrayList<ItemUse>)request.getAttribute("useList");

	 PageInfo pi = (PageInfo) request.getAttribute("pi");
	 int listCount = pi.getListCount();
	 int currentPage = pi.getCurrentPage();
	 int maxPage = pi.getMaxPage();
	 int startPage = pi.getStartPage();
	 int endPage = pi.getEndPage();
%>    
    
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	#whole {
		width: 1200px;
		margin: 0 auto;
	}
	
	#mainArea {
		width: 955px;
		float: left;
	}
	
	.default {
		font-weight: 900;
		height: 50px;
	}
	
	#navi {
		font-size: 13px;
		height: 50px;
		margin-left: 50px;
		line-height: 60px;
		border-bottom: 1px solid black;
	}
	
	#naviLab {
		margin-left: 740px;
	}
	
	#pageTitle {
		height: 80px;
		font-size: 25px;
		margin-left: 50px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#itemList2 {
		margin-top : 20px;
		margin-left: 50px;
		width: 900px;
	}
	
	
	#searchArea	{
		margin-top: 15px;
		width: 100%;
		height: 150px;
		padding-top: 15px;
		border: 2px solid #afafaf;
		margin: 0 auto;
	}
	
	#upperArea {
		margin: 0 auto;
		width: 95%;
		height: 40px;
		border-bottom: 1px solid black;
	}
	
	#upperLab {
		margin-left: 10px;
		font-size: 20px;
		font-weight: bold;
	}
	
	#downArea {
		margin: 0 auto;
		width: 960px;
		height: 100px;
	}
	
	.downLab {
		font-weight: bold;
	}
	
	#downDiv1 {
		margin-left: 30px;
		margin-top: 20px;
		width: 130px;
		height: 80px;
		float: left;
	}
	
	.downSelect {
		margin-top: 15px;
	}
	
	#downDiv2 {
		margin-top: 20px;
		width: 220px;
		height: 80px;
		margin-left: 70px;
		float: left;
	}
	
	#downDiv3 {
		margin-top: 20px;
		width: 260px;
		height: 80px;
		margin-left: 30px;
		float: left;
	}
	
	#downDiv4 {
		margin-top: 30px;
		width: 80px;
		height: 50px;
		margin-left: 30px;
		background: red;
		float: left;
	}
	
	#searchBtn {
		font-size: 18px;
		font-weight: bold;
		width: 100%;
		height: 100%;
	}
	
	
	#underListTabArea {
		width: 100%;
		margin-top: 30px;
	}
	
	#resultLab {
		font-size: 15px;
		font-weight: bold;
	}
	
	#underListTab {
		width: 100%;
		border-collapse: collapse;
		text-align: center;
		margin-top: 20px;
	}
	
	#tabTr1 {
		border-top: 1px solid #afafaf;
		border-bottom: 1px solid #afafaf;
		background: #f0f0f0;
	}
	
	#underListTab td {
		height: 40px;
		border-bottom: 1px solid #afafaf;
	}
	
	#underListTab th {
		font-weight: 900;
	}
	
	
	/* 페이징 공용 css */
	#pagingArea > button {
		border-style: none;
		background: none;
		font-size: 18px;
		font-weight: bold;
		cursor: pointer;
		margin-top: 20px;
	}
	
	.former {
		margin-right: 10px;
	}
	
	.next {
		margin-left: 10px;
	}
	
	/* 페이징 공용 css end */
	
</style>
</head>
<body>
	<%@ include file="../guide/admin_menubar.jsp" %>

	<!-- 전체 영역 -->
	<div id="whole">

		<!-- 컨텐츠 영역 -->
		<div id="mainArea">

			<div id="navi" class="default"><label id="naviLab">상품관리 > 상품사용내역</label></div>
			<div id="pageTitle" class="default">상품사용내역</div>
			
			<div id="itemList2">
				<div id="underListArea">
					<div id="underListTabArea">
						<div id="searchResultArea">
							<div id="resultLab">
								<label style="font-size:20px;">사용내역</label> 
								<label style="font-size:19px; font-weight:normal;">| </label>
								<label style="color:red; font-size:18px;">5</label><label>건</label>
							</div>
						</div>
					
						<table id="underListTab">
							<tr id="tabTr1">
								<th class="tabTh" style="width:10%; height:35px;">번호</th>
								<th class="tabTh" style="width:10%;">상품분류</th>
								<th class="tabTh" style="width:40%;">적용대상</th>
								<th class="tabTh" style="width:10%;">종류</th>
								<th class="tabTh" style="width:10%;">개수</th>
								<th class="tabTh" style="width:10%;">승인일자</th>
								<th class="tabTh" style="width:10%;">적용일자</th>
								
							</tr>
							<% 
								int index = 0;
							
								if(useList != null && useList.size() != 0) {
									for(int i = 0; i < useList.size(); i++) {
									ItemUse iu = (ItemUse) useList.get(i);
									
									String name = "";
									if(iu.getProductName().length() > 3) {
										name = iu.getProductName().substring(0, 3);
									}else {
										name = iu.getProductName().substring(0, 2);
									}
									
									String target = "";
									
									if(iu.getTargetKind().equals("1")) {
										target = "채용공고";
									}else {
										target = "이력서";
									}
									
									int num = (listCount - index) - 
									((currentPage - 1) * pi.getLimit()); 
							%>
									<tr>
										<td><%=num%></td>
										<td><%=name%></td>
										
										<% if(iu.getRecruitTitle().length() >= 20) { %>
										<td><%=iu.getRecruitTitle().substring(0, 20) + "..."%></td>
										<% } else { %>
										<td><%=iu.getRecruitTitle()%></td>	
											
										<% }%>
										
										<td ><%=target%></td>
										<td><%=iu.getUseUnit()%></td>
										<td><%=iu.getUseDate()%></td>
										<td><%=iu.getApplyDate()%></td>
									</tr>
							<% 
									index++;
									} 
								}else {
							%>
									<tr>
										<td colspan="7">보유한 아이템이 없습니다.</td>
									</tr>
							<%	
								}
							%>
						</table>
					</div>
				</div>
			</div>
			
			<div id="pagingArea" align="center">
				<button onclick="location.href='<%=request.getContextPath()%>/loadItemUse?currentPage=1'"><<</button>
			
				<% if(currentPage <= 1) {%>
					<button disabled class="former"><</button>
				<% } else {%>
					<button class="former" onclick="location.href='<%=request.getContextPath()%>/loadItemUse?currentPage=<%=currentPage - 1%>'"><</button>
				<% } %>
						
				<% for(int p = startPage; p <= endPage; p++) { 
					if(p == currentPage) {
				%>
						<button disabled style="color:red; cursor:default;"><%=p%></button>
				<%	} else {%>
						<button onclick="location.href='<%=request.getContextPath()%>/loadItemUse?currentPage=<%=p%>'"><%=p%></button>
				<%  } %>
				<% } %>
						
				<% if(currentPage >= maxPage) {%>
					<button disabled class="next">></button>
				<% } else {%>
					<button class="next" onclick="location.href='<%=request.getContextPath()%>/loadItemUse?currentPage=<%=currentPage + 1%>'">></button>
				<% } %>
						
				<button onclick="location.href='<%=request.getContextPath()%>/loadItemUse?currentPage=<%=maxPage%>'">>></button>				
				<br>
				<br>
				<br>
				<br>
				<br>                             
			</div>	
			
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	

</body>
</html>