<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@
	page import="
				com.kh.hbr.board.faq.model.vo.Faq
				, java.util.ArrayList
				, com.kh.hbr.payment.model.vo.PageInfo"
%>

<%
	ArrayList<Faq> faqList = (ArrayList<Faq>) request.getAttribute("faqList");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
	int value = (int) request.getAttribute("memberKind");
%>
    

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>


	#whole {
		width: 1200px;
		margin: 0 auto;
	}
	
	#mainArea {
		width: 80%;
		margin-left: 220px;
	}
	
	#navi {
		font-size: 15px;
		height: 50px;
		margin-left: 30px;
		line-height: 50px;
	}
	
	#pageTitle {
		width: 95%;
		height: 80px;
		line-height: 80px;
		margin-left: 30px;
		background: #013252;
	}
	
	#titleLab {
		font-size: 30px;
		color: white;
		font-weight: bold;
		margin-left: 30px;
	}
	
	#memberKind {
		width: 95%;
		margin-left: 30px;
		margin-top: 20px;
	}
	
	#personal {
		width: 49%;
		height: 40px;
		display: inline-block;
		text-align: center;
		border: 2px solid #7d7d7d;
		line-height: 40px;
		font-weight: bold;
	}
	
	#business {
		width: 49%;
		display: inline-block;
		text-align: center;
		border: 2px solid #7d7d7d;
		height: 40px;
		line-height: 40px;
		font-weight: bold;
	}
	
	#tableArea {
		width: 95%;
		margin-left: 30px;
		margin-top: 15px;
	}
	
	#faqTable {
		width: 100%;
		border-collapse: collapse;

	}
	
	#faqTable tr {
		height: 50px;
		line-height: 50px;
		border: 1px solid black;
	}
	
	#pagingArea > button {
		border-style: none;
		background: none;
		font-size: 18px;
		font-weight: bold;
		cursor: pointer;
	}
	
	.former {
		margin-right: 10px;
	}
	
	.next {
		margin-left: 10px;
	}
	
	#moklok {
		width: 50px;
		height: 30px;
		border-style: none;
		color: white;
		background: #013252;
	}
	
	
</style>
</head>
<body>

	<%@ include file="../common/E_user_menubar.jsp" %></div>
	<hr style="border: solid 2px #013252; width:1200px; margin:0 auto;"><br>
	<%@ include file="faq_leftMenu.jsp" %>
		
	<!-- 전체 영역 -->
	<div id="whole">
		<!-- 컨텐츠 영역 -->
		<div id="mainArea">

			<div id="navi" class="default">
				<div style="margin-left:10px;">HOME > 고객센터 > 자주묻는질문</div>
			</div>
			<div id="pageTitle" class="default">
				<div id="titleLab">자주묻는질문</div>
			</div>
			
			<div id="memberKind">
				<div id="personal">개인</div>
				<div id="business">기업</div>
			</div>
			
			<div id="tableArea">
				<table id="faqTable">
					<tbody>
						<% for(int i = 0; i < faqList.size(); i++) {
								Faq faq = faqList.get(i);						
						%>
						<tr>
							<td width="80%" style="margin-left: 5px;">&nbsp;&nbsp;<b>Q.</b>&nbsp;<%=faq.getContent1()%></td>
							<td width="5%" style="text-align:center;"><img src="/h/static/images/faq/downarrow.png" width="20px" height="20px"></td>
						</tr>
						<tr>
							<td colspan="2" style="margin-left: 5px;">&nbsp;&nbsp;<b>A.</b>&nbsp;<%=faq.getContent2()%></td>
						</tr>
						<% } %>
					</tbody>
				</table>
			</div>
			
			<div style="margin-top: 15px; margin-left: 50px;"><button id="moklok">목록</button></div>
			
			<script>
				$("#moklok").click(function(){
					location.href="<%=request.getContextPath()%>/loadFront.faq";		
				});
			</script>
			
			<div id="pagingArea" align="center">
				<button onclick="location.href='<%=request.getContextPath()%>/searchFrontFaq?currentPage=1&value=<%=value%>'"><<</button>
			
				<% if(currentPage <= 1) {%>
					<button disabled class="former"><</button>
				<% } else {%>
					<button class="former" onclick="location.href='<%=request.getContextPath()%>/searchFrontFaq?currentPage=<%=currentPage - 1%>&value=<%=value%>'"><</button>
				<% } %>
				
				<% for(int p = startPage; p <= endPage; p++) { 
					if(p == currentPage) {
				%>
						<button disabled style="color:red; cursor:default;"><%=p%></button>
				<%	} else {%>
						<button onclick="location.href='<%=request.getContextPath()%>/searchFrontFaq?currentPage=<%=p%>&value=<%=value%>'"><%=p%></button>
				<%  } %>
				<% } %>
				
				<% if(currentPage >= maxPage) {%>
					<button disabled class="next">></button>
				<% } else {%>
					<button class="next" onclick="location.href='<%=request.getContextPath()%>/searchFrontFaq?currentPage=<%=currentPage + 1%>&value=<%=value%>'">></button>
				<% } %>
				
				<button onclick="location.href='<%=request.getContextPath()%>/searchFrontFaq?currentPage=<%=maxPage%>&value=<%=value%>'">>></button>				
			</div>
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
	
	<script>
	
		/* 버튼에 css연결 - 개인 */
		$("#personal").mouseenter(function(){
			$(this).css({"background":"#ecf0f7", "color":"#013252", "cursor":"pointer"});
		}).mouseout(function(){
			$(this).css({"background":"#ffffff", "color":"black", "cursor":"default"});
		}).click(function(){
			$(this).attr("value","1");
			location.href="<%=request.getContextPath()%>/searchFrontFaq?value=" + $(this).attr("value"); 
		});
		
		/* 버튼에 css연결 - 기업*/
		$("#business").mouseenter(function(){
			$(this).css({"background":"#ecf0f7", "color":"#013252", "cursor":"pointer"});
		}).mouseout(function(){
			$(this).css({"background":"#ffffff", "color":"black", "cursor":"default"});
		}).click(function(){
			$(this).attr("value","2");
			location.href="<%=request.getContextPath()%>/searchFrontFaq?value=" + $(this).attr("value"); 
		});
		
		//클릭시 영역 설정에대한 script
		//기본은 onload되었을 때 answer의 tr을 숨겨줌 
		$(function(){
			
			$("#faqTable tr").each(function(index, item){
				if(index % 2 == 1) {
					$(this).hide();
				}else {
					$(this).click(function(){
						$("#faqTable tr").eq(index + 1).toggle();
						
						var row = $("#faqTable tr").eq(index + 1);
						
						if(row.css("display") === "table-row") {
							$(this).children().eq(1).html('<img src="/h/static/images/faq/uparrow.png" width="20px" height="20px">');
						}else {
							$(this).children().eq(1).html('<img src="/h/static/images/faq/downarrow.png" width="20px" height="20px">');
						}
					});
				}
				
			});
			
			/*개인 / 기업 버튼에 색칠해주기*/
			var searchValue = <%=value%>;
			
			if(searchValue === 1) {
				$("#personal").css({"background":"#ecf0f7", "color":"#013252"});
			}else if(searchValue === 2){
				$("#business").css({"background":"#ecf0f7", "color":"#013252"});
			}
		});
		
	</script>		

</body>
</html>