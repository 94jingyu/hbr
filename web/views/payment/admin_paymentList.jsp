<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="com.kh.hbr.payment.model.vo.PageInfo
				,com.kh.hbr.payment.model.vo.Purchase 
				,java.util.ArrayList
				,java.text.DecimalFormat"
%>    

<%
	ArrayList<Purchase> purList = (ArrayList<Purchase>) request.getAttribute("purList");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
	int limit = pi.getLimit();
%>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="views/payment/datepicker.css" />
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	#whole {
		width: 1200px;
		margin: 0 auto;
		
	}
	
	#mainArea {
		width: 955px;
		float: left;
	}
	
	.default {
		font-weight: 900;
		height: 50px;
	}
	
	#navi {
		font-size: 13px;
		height: 50px;
		margin-left: 50px;
		line-height: 60px;
		border-bottom: 1px solid black;
	}
	
	#naviLab {
		margin-left: 770px;
	}
	
	#pageTitle {
		height: 80px;
		font-size: 25px;
		margin-left: 50px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#itemList2 {
		margin-top : 20px;
		margin-left: 50px;
		width: 900px;
	}
	
	
	#searchArea	{
		margin-top: 15px;
		width: 50%;
		height: 150px;
		padding-top: 15px;
		border: 2px solid #afafaf;
	}
	
	#upperArea {
		margin: 0 auto;
		width: 95%;
		height: 40px;
		border-bottom: 1px solid black;
	}
	
	#upperLab {
		margin-left: 10px;
		font-size: 20px;
		font-weight: bold;
	}
	
	#downArea {
		margin: 0 auto;
		width: 960px;
		height: 100px;
	}
	
	.downLab {
		font-weight: bold;
	}
	
	.downSelect {
		margin-top: 15px;
	}
	
	#downDiv3 {
		margin-top: 20px;
		width: 260px;
		height: 80px;
		margin-left: 30px;
		float: left;
	}
	
	#downDiv4 {
		margin-top: 30px;
		width: 80px;
		height: 50px;
		margin-left: 40px;
		background: red;
		float: left;
	}
	
	#searchBtn {
		font-size: 18px;
		font-weight: bold;
		width: 100%;
		height: 100%;
	}
	
	
	#underListTabArea {
		width: 100%;
		margin-top: 30px;
	}
	
	#resultLab {
		font-size: 15px;
		font-weight: bold;
	}
	
	#underListTab {
		width: 100%;
		border-collapse: collapse;
		text-align: center;
		margin-top: 20px;
	}
	
	#tabTr1 {
		border-top: 1px solid #afafaf;
		border-bottom: 1px solid #afafaf;
		background: #f0f0f0;
	}
	
	#underListTab td {
		height: 40px;
		border-bottom: 1px solid #afafaf;
	}
	
	#underListTab th {
		font-weight: 900;
	}
	
	#paging {
		margin-top: 15px;
	}
	
	#paging > button {
		border-style: none;
		background: none;
		font-size: 18px;
		font-weight: bold;
		cursor: pointer;
	}
	
	.former {
		margin-right: 10px;
	}
	
	.next {
		margin-left: 10px;
	}
	
</style>
</head>
<body>
	<%@ include file="../guide/admin_menubar.jsp" %>
	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>  
<script src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>

	<!-- 전체 영역 -->
	<div id="whole">

		<!-- 컨텐츠 영역 -->
		<div id="mainArea">

			<div id="navi" class="default"><label id="naviLab">상품관리 > 결제내역</label></div>
			<div id="pageTitle" class="default">결제내역</div>
			
			<div id="itemList2">
			
			<div id="searchArea">
				<div id="upperArea">
					<div id="upperLab">검색조건</div>
				</div>
				
				<div id="downArea">
				
					<div id="downDiv3">
					<div id="downLab3" class="downLab">조건 입력</div>
						<div class="downSelect">
							<select id="condition" style="width:80px; height:25px; font-size:14px;" >
								<option>아이디</option>
							</select>
							<div style="float:right; line-height:100%;">
							<input id="searchValue" type="text" style="height:20px; font-size:14px;">
							</div>
						</div>
					</div>
					
					<div id="downDiv4">
						<button id="searchBtn">검색</button>					
					</div>
				</div>
			</div>
			
				<div id="underListArea">
					<div id="underListTabArea">
						<div id="searchResultArea">
							<div id="resultLab">
								<label style="font-size:20px;">결제내역</label> 
								<label style="font-size:19px; font-weight:normal;">| </label>
								<label style="color:red; font-size:18px;"><%=listCount%></label><label>건</label>
							</div>
						</div>
					
						<table id="underListTab">
							<tr id="tabTr1">
								<th class="tabTh" style="width:5%; height:35px;">번호</th>
								<th class="tabTh" style="width:10%;">결제번호</th>
								<th class="tabTh" style="width:10%;">아이디</th>
								<th class="tabTh" style="width:10%;">구매자</th>
								<th class="tabTh" style="width:10%;">상품명</th>
								<th class="tabTh" style="width:10%;">개수</th>
								<th class="tabTh" style="width:10%;">결제금액</th>
								<th class="tabTh" style="width:10%;">결제수단</th>
								<th class="tabTh" style="width:10%;">결제일</th>
								
							</tr>
							
							<% 
							int index = 0;
							String name = "";
							for(Purchase p : purList) {						
								int num = (listCount - index) - 
										((currentPage - 1) * limit); 
							%>
							<tr>
								<td><%=num%></td>
								<td><%=p.getPurchaseNo()%></td>
								<td><%=p.getbMemberId()%></td>
								<td><%=p.getbMemberName()%></td>
								
								<% 
									if(p.getProductName().length() > 3) {
										name = p.getProductName().substring(0,3);
									}else {
										name = p.getProductName().substring(0,2);
									}
								%>
												
								<td><%=name%></td>
								<td><%=p.getSalesUnit()%></td>
								
								<%
									DecimalFormat pf = new DecimalFormat("#,###");
								%>
								
								<td><%=pf.format(p.getPrice())%></td>
								<td>카드</td>
								<td><%=p.getPurchaseDate()%></td>
							</tr>	
							<% 
								index++;
							}
							%>
						</table>
						
						<div id="paging" align="center">
							<!-- 첫 페이지 버튼성정 -->
							<button onclick="location.href='<%=request.getContextPath()%>/adminPaymentList.pay?currentPage=1'"><<</button>
						
							<!-- 이전 페이지 버튼설정 -->
							<% if(currentPage <= 1) {%>
								<button disabled class="former"><</button>
							<% } else { %>
								<button class="former" onclick="location.href='<%=request.getContextPath()%>/adminPaymentList.pay?currentPage=<%=currentPage - 1%>'"><</button>								
							<% } %>
							
							<!-- 숫자 페이지 버튼 설정 -->
							<% 
								for(int i = startPage; i <= endPage; i++) { 
									if(i == currentPage) {
							%>
										<button disabled style="color:red; cursor:default;"><%=i%></button>
							<%		} else {%>
										<button onclick="location.href='<%=request.getContextPath()%>/adminPaymentList.pay?currentPage=<%=i%>'"><%=i%></button>
							<% 
									}	
								} 
							%>
							
							<!-- 다음페이지 버튼 설정 -->
							<% if(currentPage >= maxPage) { %>
								<button disabled class="next">></button>
							<% } else { %>
								<button class="next" onclick="location.href='<%=request.getContextPath()%>/adminPaymentList.pay?currentPage=<%=currentPage + 1%>'">></button>
							<% } %>
							
							<button onclick="location.href='<%=request.getContextPath()%>/adminPaymentList.pay?currentPage=<%=maxPage%>'">>></button>
						</div>
					</div>
				</div>
			</div>
			
			
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	
	<script>
		$(function(){
			//검색조건 시작날짜의 설정사항
			$.datepicker.setDefaults($.datepicker.regional['ko']); 
	        $("#startDate").datepicker({
	        	nextText: '다음 달',
	            prevText: '이전 달', 
	            dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
	            dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'], 
	            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	            monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	            dateFormat: "y/mm/dd",
	            maxDate: 0,
	 
	        });
	        
	        $("#endDate").datepicker({
	        	nextText: '다음 달',
	            prevText: '이전 달', 
	            dayNames: ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'],
	            dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'], 
	            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	            monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	            dateFormat: "y/mm/dd",
	            maxDate: 0,
	            
	        });
	        
	        $("#searchBtn").click(function() {
	        	var condition = $("#condition").children(":selected").val();
	        	var searchValue = $("#searchValue").val();
	        	
	        	if(searchValue.length <= 0) {
	        		alert("검색조건은 한 글자 이상 입력해야합니다.");
	        	}else {
		        	location.href="<%=request.getContextPath()%>/searchAdminPaymentList.pay?condition=" 
	        			+ condition + "&searchValue=" + searchValue;
	        	}
	        });
		});	
	
	</script>
	
	

</body>
</html>