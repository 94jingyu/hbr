<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	
	<h2><a href="payment_leftMenu.jsp">레프트메뉴</a></h2>
	<h2><a href="payment.jsp">상품/구매페이지</a></h2>
	<h2><a href="paymentGold.jsp">구매-금도끼</a></h2>
	<h2><a href="paymentSilver.jsp">구매-은도끼</a></h2>
	<h2><a href="paymentSteel.jsp">구매-쇠도끼</a></h2>
	<h2><a href="paymentTwinkle.jsp">구매-반짝</a></h2>
	<h2><a href="paymentResume.jsp">구매-이력서</a></h2>
	<h2><a href="itemList.jsp">상품보유내역</a></h2>
	<h2><a href="itemUseList.jsp">상품사용내역</a></h2>
	<h2><a href="paymentList.jsp">상품결제내역</a></h2>
	<h2><a href="<%=request.getContextPath()%>/adminPaymentList.pay">관리자결제내역</a></h2>
	<h2><a href="admin_itemUseList.jsp">관리자아이템사용내역</a></h2>
	<h2><a href="<%=request.getContextPath()%>/adminRefundList.pay">관리자환불처리내역</a></h2>
	<h2><a href="<%=request.getContextPath()%>/loadList.faq">관리자FAQ리스트</a></h2>
	<h2><a href="<%=request.getContextPath()%>/loadFront.faq">일반FAQ리스트</a></h2>
	

</body>
</html>