<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import = "java.util.ArrayList, com.kh.hbr.payment.model.vo.Refund, com.kh.hbr.payment.model.vo.PageInfo"%>
    
<%
ArrayList<Refund> refundList = (ArrayList<Refund>) request.getAttribute("refundList");
PageInfo pi = (PageInfo) request.getAttribute("pi");
int listCount = pi.getListCount();
int currentPage = pi.getCurrentPage();
int maxPage = pi.getMaxPage();
int startPage = pi.getStartPage();
int endPage = pi.getEndPage();
int limit = pi.getLimit();
%>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	#whole {
		width: 1200px;
		margin: 0 auto;
		
	}
	
	#mainArea {
		width: 955px;
		float: left;
	}
	
	.default {
		font-weight: 900;
		height: 50px;
	}
	
	#navi {
		font-size: 13px;
		height: 50px;
		margin-left: 50px;
		line-height: 60px;
		border-bottom: 1px solid black;
	}
	
	#naviLab {
		margin-left: 770px;
	}
	
	#pageTitle {
		height: 80px;
		font-size: 25px;
		margin-left: 50px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#itemList2 {
		margin-top : 20px;
		margin-left: 50px;
		width: 900px;
	}
	
	
	#searchArea	{
		margin-top: 15px;
		width: 100%;
		height: 150px;
		padding-top: 15px;
		border: 2px solid #afafaf;
		margin: 0 auto;
	}
	
	#upperArea {
		margin: 0 auto;
		width: 95%;
		height: 40px;
		border-bottom: 1px solid black;
	}
	
	#upperLab {
		margin-left: 10px;
		font-size: 20px;
		font-weight: bold;
	}
	
	#downArea {
		margin: 0 auto;
		width: 960px;
		height: 100px;
	}
	
	.downLab {
		font-weight: bold;
	}
	
	#underListTabArea {
		width: 100%;
		margin-top: 30px;
	}
	
	#resultLab {
		font-size: 15px;
		font-weight: bold;
	}
	
	#search {
		width: 150px;
		height: 30px;
		margin-left: 610px;
	}
	
	#underListTab {
		width: 100%;
		border-collapse: collapse;
		text-align: center;
		margin-top: 20px;
	}
	
	#tabTr1 {
		border-top: 1px solid #afafaf;
		border-bottom: 1px solid #afafaf;
		background: #f0f0f0;
	}
	
	#underListTab td {
		height: 40px;
		border-bottom: 1px solid #afafaf;
	}
	
	#underListTab th {
		font-weight: 900;
	}
	
	.refundBtn {
		width: 50px;
		height: 30px;
		background: #013252;
		color: white;
		border: 1px solid white;
		border-radius:5px;
		background: red;
		line-height: 30px;
	}
	
	 /* The Modal (background) */
	.modal {
		display: none; /* Hidden by default */
		position: fixed; /* Stay in place */
		z-index: 1; /* Sit on top */
		left: 0;
 		top: 0;
		width: 100%; /* Full width */
		height: 100%; /* Full height */
		overflow: auto; /* Enable scroll if needed */
		background-color: rgb(0,0,0); /* Fallback color */
		background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
    
        /* Modal Content/Box */
	.modal-content {
		background-color: #fefefe;
		margin: 0 auto; /* 15% from the top and centered */
		margin-top: 200px;
		padding: 20px;
		border: 1px solid #888;
		width: 500px; /* Could be more or less, depending on screen size */ 
		height: 400px;                         
	}	
        
	#refuseReason {
		margin-top: 10px;
	}
	
	#closeBtn {
		width: 80px;
		height: 40px;
		color: white;
		border: 1px solid white;
		border-radius:5px;
		background: red;
		margin-top: 10px;
	}
		
	#finalRefund {
		width: 80px;
		height: 40px;
		background: #013252;
		color: white;
		border: 1px solid white;
		border-radius:5px;
		margin-left: 165px;
		margin-top: 10px;
	}
	
	#paging {
		margin-top: 15px;
	}
	
	#paging > button {
		border-style: none;
		background: none;
		font-size: 18px;
		font-weight: bold;
		cursor: pointer;
	}
	
	.former {
		margin-right: 10px;
	}
	
	.next {
		margin-left: 10px;
	}
	
	#introLab {
		width: 900px;
		margin-top: 20px;
		margin-bottom: 20px;
		padding-top: 10px;
		padding-bottom: 10px;
		border: 1px solid black;
		margin-left: 50px;
	}
	
	#ulArea {
		margin-top: 5px;
		margin-left: 40px;
	}
	
</style>
</head>
<body>
	<%@ include file="../guide/admin_menubar.jsp" %>

	<!-- 전체 영역 -->
	<div id="whole">

		<!-- 컨텐츠 영역 -->
		<div id="mainArea">

			<div id="navi" class="default"><label id="naviLab">상품관리 > 환불내역</label></div>
			<div id="pageTitle" class="default">환불내역</div>
			<div id="introLab">
				<label style="font-size: 18px; font-weight: bold;">&nbsp;&nbsp;환불관련 주의사항</label>
				<div id="ulArea">
					<ul>
						<li>상품의 구매일자가 현재 날짜로부터 7일 이내인지 한번 더 확인후에 환불처리를 실시합니다.</li>
						<li>같은 아이디에서 상습적, 악의적인 환불 경향이 보일 경우 반려 처리를 실시합니다.</li>
					</ul>
				</div>
			</div>
			
			<div id="itemList2">
			
				<div id="underListArea">
					<div id="underListTabArea">
						<div id="searchResultArea">
							<div id="resultLab">
								<label style="font-size:20px;">환불내역</label> 
								<label style="font-size:19px; font-weight:normal;">| </label>
								<label style="color:red; font-size:18px;"><%=refundList.size()%></label><label>건</label>
							</div>
						</div>
						
					<div id="tableArea">
						<table id="underListTab">
							<tr id="tabTr1">
								<th class="tabTh" style="width:5%; height:35px;">번호</th>
								<th class="tabTh" style="width:10%;">환불번호</th>
								<th class="tabTh" style="width:10%;">구매자</th>
								<th class="tabTh" style="width:10%;">상품명</th>
								<th class="tabTh" style="width:20%;">환불사유</th>
								<th class="tabTh" style="width:10%;">신청일자</th>
								<th class="tabTh" style="width:10%;">처리결과</th>
								<th class="tabTh" style="width:10%;">처리일자</th>
								<th class="tabTh" style="width:10%;">환불처리</th>
							</tr>
							
							<% 
								int num = refundList.size();
								for(int i = 0; i < refundList.size(); i++) { 
									Refund ref = refundList.get(i);
							%>							
							<tr>
								<td><%=num%></td>
								<td><%=ref.getRefundNo()%></td>
								<td><%=ref.getMemberId()%></td>
								<td><%=ref.getProductName()%></td>
								<td><%=ref.getRefundReason()%></td>
								<td><%=ref.getRefundApplyDate()%></td>
								<%
									if(ref.getRefundResult() != null) {
										String result = "";
										switch(ref.getRefundResult()) {
											case "Y" : result = "환불완료"; break;
											case "N" : result = "반려"; break;
										}
								%>
										<td><%=result%></td>
								<%
									}else {
								%>
										<td>-</td>
								<% } %>
								<%
									if(ref.getResultDate() == null) {
								%>
										<td>-</td>
								<%
									}else {
								%>
									<td><%=ref.getResultDate()%></td>	
								<% } %>

								<td>
								<% 	
									if(ref.getRefundResult() == null) {
								%>
										<button class="refundBtn">환불</button>
								<%	
									}
								%>
								</td>							
							</tr>
							<% 
								num--;
								} 
							%>
						</table>
						</div>
						
						<div id="paging" align="center">
						<button onclick="location.href='<%=request.getContextPath()%>/adminRefundList.pay?currentPage=1'"><<</button>
						
						<% if(currentPage <= 1) {%>
						<button disabled class="former"><</button>
						<% } else { %>
						<button class="former" onclick="location.href='<%=request.getContextPath()%>/adminRefundList.pay?currentPage=<%=currentPage - 1%>'"><</button>
						<% } %>
						
						<% for(int i = startPage; i <= endPage; i++) {
								if(i == currentPage) {
						%>
									<button disabled style="color:red; cursor:default;"><%= i %></button>
						<%		
								}else {
						%>
									<button onclick="location.href='<%=request.getContextPath()%>/adminPaymentList.pay?currentPage=<%=i%>'"><%=i%></button>
						<% 
								}
						   } 
						%>
						
						<% if(currentPage >= maxPage) { %>
							<button disabled class="next">></button>
						<% } else { %>
							<button class="next" onclick="location.href='<%=request.getContextPath()%>/adminPaymentList.pay?currentPage=<%=currentPage + 1%>'">></button>
						<%}%>
						
						<button onclick="location.href='<%=request.getContextPath()%>/adminPaymentList.pay?currentPage=<%=maxPage%>'">>></button>						   
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="myModal" class="modal">
      <!-- Modal content -->
      <div class="modal-content">
                <div style="text-align:center; font-size: 35px; font-weight: bold;">
                	환불처리
                </div>
                <div style="margin-top: 10px;">
                <label style="color: red; fond-size: bold; font-size: 18px;">*주의사항</label>
                </div>
                <div style="margin-top: 10px; margin-left: 15px;">
                	<ul style="font-size: 16px; line-height: 25px;">
                		<li>환불은 신중하게 처리 부탁드립니다.</li>
                		<li>환불반려시 꼭 사유를 작성해주시기 바랍니다.</li>
                	</ul>	
                </div>
                <div style="width: 90%; height: 180px; border: 1px solid black; margin: 0 auto; margin-top: 15px;">
					<div style="font-size: 18px; margin-left: 10px; margin-top: 10px;">환불처리선택</div>
					<div style="margin-left: 15px; margin-top: 10px;">
					<input type="radio" name="refund" value="1" id="agree">&nbsp;<label for="agree" style="color:red; font-weight:bold;">승인</label></div>               
					<div style="margin-left: 15px; margin-top: 10px;">
					<input type="radio" name="refund" value="2" id="refuse">&nbsp;<label for="refuse" style="color:#013252; font-weight:bold;">반려</label><br>
					<textarea id="refuseReason" rows="4" cols="55" maxlength="100" disabled></textarea>
					</div>          
                </div>
            <div>
            <button id="finalRefund" class="reBtn">환불처리</button>&nbsp;<button id="closeBtn" class="reBtn">닫기</button></div>
      </div>
 
    </div>
    
	<script>
	
	$(function(){
		
		//사용할 환불번호를 담을 변수를선언
		var refundNo;
		
		$("input[name='refund']").click(function(){
			if($("input:radio:checked").val() === "2") {
				$("#refuseReason").attr("disabled", false);
			}else {
				$("#refuseReason").attr("disabled", true);
			}
		});
		
		$(".refundBtn").click(function(){
	    	$('#myModal').show();
	    	refundNo = $(this).parent().parent().children().eq(1).text();
		});
		
		$("#closeBtn").click(function(){
			$("#myModal").hide();
		});
		
		$("#search").change(function(){
				
				console.log($("#search").children(":selected").val());
			
			$.ajax({
				url: "<%=request.getContextPath()%>/adminRefundList.pay",
				type: "POST",
				data: {
					former: $("#search").children(":selected").val()
				}
			});
		});
		
		//최종환불처리
		$("#finalRefund").click(function(){
			
			//조회해올 imp_uid 변수 선언
			var req_imp_uid;
			var myItemNo;
			var purchaseNo;
			
			//라디오버튼의 값에 따라 승인 / 반려에 대한 처리를 다르게 해줌.
			var result = $("input[name='refund']:checked").val();
			
			if(result === "1") {
				//환불승인
				
				//요청한 req_imp_uid 얻어오기
				$.ajax({
					url: "<%=request.getContextPath()%>/impUid.pay",
					type: "POST",
					data: {refundNo:refundNo},
					success: function(data) {
						req_imp_uid = data.approveNo;
						myItemNo = data.myItemNo;
						purchaseNo = data.purchaseNo;
						
						//받는데 성공할 경우 토큰을 받아옴
						//토큰받아오기
						$.ajax({
							url: "<%=request.getContextPath()%>/token.get",
							type: "POST",
							success: function(data) {
								token = data;
								
								if(token != null) {
									//토큰을 얻는데 성공하였을 경우 환불을 요청
									
									//환불신청 ajax
									//성공할 경우 같은 서블릿에서 처리결과 처리일자 update처리
									
									$.ajax({
										url: "<%=request.getContextPath()%>/refund.pay",
										type: "POST",
										data: {
											url: "https://api.iamport.kr/payments/cancel",
											accessToken: token,
											imp_uid: req_imp_uid,
											refundNo: refundNo,
											myItemNo: myItemNo,
											purchaseNo: purchaseNo
										},
										success: function(data) {
											alert("환불처리가 완료되었습니다.");
											$("myModal").hide();
										},
										error: function(error) {
											console.log(error);
										}
									});
									
								}
							},
							error: function(error) {
								console.log(error);
							}
						});
					},
					
					error: function(error) {
						console.log(error);
					}
				});
				 
			} else if(result === "2") {
				
				var refuseReason = $("#refuseReason").val();
				
				//반려 클릭 시 반려에대한 내용 업데이트 처리.
				$.ajax({
					url: "<%=request.getContextPath()%>/refuseRefund.pay",
					type: "POST",
					data: {refundNo:refundNo, refuseReason:refuseReason},
					success: function(data) {
						$("myModal").hide();	
						alert("반려가 완료되었습니다.");
					},error: function(error) {
						console.log(error);
					}
				});
				
			} else {
				alert("결과를 선택해주세요.");
			}	

		});
		
	});
	
	</script>

</body>
</html>