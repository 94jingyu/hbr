<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.hbr.member.model.vo.Member"%>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	#whole {
		width: 1200px;
		margin: 0 auto;
	}
	
	#mainArea {
		width: 955px;
		float: left;
	}
	
	.default {
		font-weight: 900;
		height: 50px;
	}
	
	#navi {
		font-size: 13px;
		height: 50px;
		margin-left: 50px;
		line-height: 60px;
		border-bottom: 1px solid black;
	}
	
	#naviLab {
		margin-left: 750px;
	}
	
	#pageTitle {
		height: 80px;
		font-size: 25px;
		margin-left: 50px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#itemList2 {
		margin-top : 20px;
		margin-left: 50px;
		width: 900px;
	}
	
	#underListTabArea {
		width: 100%;
		margin-top: 30px;
	}
	
	#resultLab {
		font-size: 15px;
		font-weight: bold;
	}
	
	#underListTab {
		width: 100%;
		border-collapse: collapse;
		text-align: center;
		margin-top: 20px;
	}
	
	#tabTr1 {
		border-top: 1px solid #afafaf;
		border-bottom: 1px solid #afafaf;
		background: #f0f0f0;
	}
	
	#underListTab td {
		height: 40px;
		border-bottom: 1px solid #afafaf;
	}
	
	#underListTab th {
		font-weight: 900;
	}
	
	#btnArea {
		float: right;
		margin-top: 20px;
	}
	
	#writeBtn {
		width: 60px;
		height: 30px;
		background: #013252;
		color: white;
		border: 1px solid white;
		border-radius:5px;
		line-height: 30px;
	}
	
	#deleteBtn {
		width: 60px;
		height: 30px;
		background: #d9d5d5;
		border: 1px solid white;
		color: white;
		border-radius:5px;
		line-height: 30px;
	}
	
	#writeArea {
		width: 90%;
		height: 750px;
		background: #f6f3f3;
		margin-top: 30px;
		margin-left: 50px;
		overflow: hidden;
	}
	
	#titleArea {
		font-size: 18px;
		font-weight: bold;
		width: 100%;
		margin-top: 30px;
		overflow: hidden;
	}
	
	#titleLab {
		margin-left: 70px;
		width: 50px;
		height: 20px;
		float: left;
	}

	#titleInput {
		margin-left: 130px;
		line-height: 0px;
	}
	
	#faqTitle {
		width: 650px;
		height: 30px;
		border: 1px solid black;
		border-radius: 5px;
		padding-left: 10px;
	}
	
	#selectArea {
		font-size: 18px;
		font-weight: bold;
		width: 100%;
		margin-top: 20px;
		overflow: hidden;
	}
	
	#selectLab {
		margin-left: 33px;
		float: left;
	}
	
	#selectBox {
		margin-left: 130px;
	}
	
	#selectOption {
		border: 1px solid black;
		border-radius: 5px;
		width: 100px;
		height: 30px;
	}
	
	#questionArea {
		font-size: 18px;
		font-weight: bold;
		width: 100%;
		margin-top: 20px;
		overflow: hidden;
	}
	
	#questionLab {
		margin-left: 33px;
		float: left;
	}
	
	#questionText {
		margin-left: 130px;
	}
	
	#questionTextArea {
		border: 1px solid black;
		border-radius: 5px;
		resize: none;
		padding-top: 5px;
		padding-left: 10px;
	}
	
	#answerArea {
		font-size: 18px;
		font-weight: bold;
		width: 100%;
		margin-top: 20px;
		overflow: hidden;		
	}
	
	#answerLab {
		margin-left: 33px;
		float: left;
	}
	
	#answerText {
		margin-left: 130px;	
	}
	
	#answerTextArea {
		border: 1px solid black;
		border-radius: 5px;
		resize: none;
		padding-top: 5px;
		padding-left: 10px;
	}
	
	#btnArea {
		margin-top: 20px;
		margin-right: 360px;
	}
	
	#write {
		width: 60px;
		height: 30px;
		background: #013252;
		color: white;
		border: 1px solid white;
		border-radius:5px;
		line-height: 30px;
	}
	
	#back {
		width: 60px;
		height: 30px;
		background: #7d7777;
		border: 1px solid white;
		color: white;
		border-radius:5px;
		line-height: 30px;
	}
	
</style>
</head>
<body>
	<%@ include file="../guide/admin_menubar.jsp" %>

	<%
		Member loginUser = (Member) session.getAttribute("loginUser");
	%>

	<!-- 전체 영역 -->
	<div id="whole">

		<!-- 컨텐츠 영역 -->
		<div id="mainArea">
			<div id="navi" class="default"><label id="naviLab">자주하는질문 > 글쓰기</label></div>
			<div id="pageTitle" class="default">자주하는질문 글쓰기</div>
			
			<div id="writeArea">
				<form action="<%=request.getContextPath()%>/insert.faq" method="post">
				<div id="titleArea">
					<div id="titleLab">제목 : </div>
					<div id="titleInput"><input type="text" maxlength="40" id="faqTitle" name="faqTitle"></div>
				</div>
				
				<input type="hidden" name="bMemberNo" value="<%=loginUser.getMemberNo()%>">
				
				<div id="selectArea">
					<div id="selectLab">구분선택 : </div>
					<div id="selectBox">
						<select id="selectOption" name="selectOption">
							<option value="0">구분선택</option>
							<option value="1">개인</option>
							<option value="2">기업</option>
						</select>
					</div>
				</div>
				
				<div id="questionArea">
					<div id="questionLab">질문내용 : </div>
					<div id="questionText">
						<textarea cols="90" rows="10" id="questionTextArea" name="questionTextArea"></textarea>
					</div>
				</div>
				
				<div id="answerArea">
					<div id="answerLab">답변내용 : </div>
					<div id="answerText">
						<textarea cols="90" rows="20" id="answerTextArea" name="answerTextArea"></textarea>
					</div>
				</div>
				
				<div id="btnArea">
					<button id="write">글쓰기</button>&nbsp;<button type="button" id="back" onclick="history.go(-1);">뒤로</button>
				</div>
				</form>
			</div>
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	
	<script>
		/* $("#write").click(function(){
			//클릭시 콘솔에 값을 확인하는 script문
			console.log($("input[name=bMemberNo]").val());
			console.log($("#faqTitle").val());			
			console.log($("#selectOption option:selected").val());
			console.log($("#questionTextArea").val());
			console.log($("#answerTextArea").val());
		}); */
	</script>
	

</body>
</html>