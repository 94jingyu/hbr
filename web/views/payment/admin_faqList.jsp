<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page
	import="java.util.ArrayList, com.kh.hbr.board.faq.model.vo.Faq, com.kh.hbr.payment.model.vo.PageInfo"
 %>

<%
	ArrayList<Faq> faqList = (ArrayList<Faq>) request.getAttribute("faqList");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
%>       
    
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	#whole {
		width: 1200px;
		margin: 0 auto;
		
	}
	
	#mainArea {
		width: 955px;
		float: left;
	}
	
	.default {
		font-weight: 900;
		height: 50px;
	}
	
	#navi {
		font-size: 13px;
		height: 50px;
		margin-left: 50px;
		line-height: 60px;
		border-bottom: 1px solid black;
	}
	
	#naviLab {
		margin-left: 770px;
	}
	
	#pageTitle {
		height: 80px;
		font-size: 25px;
		margin-left: 50px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#itemList2 {
		margin-top : 20px;
		margin-left: 50px;
		width: 900px;
	}
	
	#underListTabArea {
		width: 100%;
		margin-top: 30px;
	}
	
	#resultLab {
		font-size: 15px;
		font-weight: bold;
	}
	
	#underListTab {
		width: 100%;
		border-collapse: collapse;
		text-align: center;
		margin-top: 20px;
	}
	
	#tabTr1 {
		border-top: 1px solid #afafaf;
		border-bottom: 1px solid #afafaf;
		background: #f0f0f0;
	}
	
	#underListTab td {
		height: 40px;
		border-bottom: 1px solid #afafaf;
	}
	
	#underListTab th {
		font-weight: 900;
	}
	
	.pagingWrap {
		width: 80%;
		margin: 0 auto;
		margin-top: 15px;
	}

	/* 페이징 공용 css */
	.pagingArea {
		width: 80%;
		margin: 0 auto;
	}
	
	.pagingArea > button {
		border-style: none;
		background: none;
		font-size: 18px;
		font-weight: bold;
		cursor: pointer;
	}
	
	/* 페이징 공용 css end */
	
	#btnArea {
		margin-top: 10px;
		float:right;
	}
	
	#writeBtn {
		width: 60px;
		height: 30px;
		background: #013252;
		color: white;
		border: 1px solid white;
		border-radius:5px;
		line-height: 30px;
	}
	
	#deleteBtn {
		width: 60px;
		height: 30px;
		background: #d9d5d5;
		border: 1px solid white;
		color: white;
		border-radius:5px;
		line-height: 30px;
	}
	
</style>
</head>
<body>
	<%@ include file="../guide/admin_menubar.jsp" %>

	<!-- 전체 영역 -->
	<div id="whole">

		<!-- 컨텐츠 영역 -->
		<div id="mainArea">

			<div id="navi" class="default"><label id="naviLab">자주하는질문 > 목록</label></div>
			<div id="pageTitle" class="default">자주하는질문 목록</div>
			
			<div id="itemList2">
 				<div id="underListArea">
					<div id="underListTabArea">
						<div id="searchResultArea">
							<div id="resultLab">
								<label style="font-size:20px;">질문목록</label> 
								<label style="font-size:19px; font-weight:normal;">| </label>
								<label style="color:red; font-size:18px;"><%=pi.getListCount()%></label><label>건</label>
							</div>
						</div>
					
						<table id="underListTab">
							<tr id="tabTr1">
								<th class="tabTh" style="width:10%; height:35px;">선택</th>
								<th class="tabTh" style="width:10%;">글번호</th>
								<th class="tabTh" style="width:10%;">구분</th>
								<th class="tabTh" style="width:50%;">제목</th>
								<th class="tabTh" style="width:10%;">조회수</th>
								<th class="tabTh" style="width:10%;">등록일</th>
							</tr>
							<% for(Faq f : faqList) {
								if(f.getMemberKind().equals("1")) {
									f.setMemberKind("개인");
								}else {
									f.setMemberKind("기업");
								}
							%>
							<tr>
								<td><input type="checkbox" name="faqCheck"></td>							
								<td><%=f.getBoardNo()%></td>							
								<td><%=f.getMemberKind()%></td>							
								<td><%=f.getTitle()%></td>							
								<td><%=f.getbCount()%></td>	
								<td><%=f.getEnrollDate()%></td>							
							</tr>
							<% } %>
						</table>
					</div>
				</div>
			</div>
			
			<div id="btnArea">
				<button id="writeBtn" onclick="location.href='views/payment/admin_writeFaq.jsp'">글쓰기</button>&nbsp;<button id="deleteBtn" type="button">글삭제</button>
			</div>
			
			<div class="pagingWrap">
				<div class="pagingArea" align="center">
				<button onclick="location.href='<%=request.getContextPath()%>/loadList.faq?currentPage=1'"><<</button>			
				
				<% if(currentPage <= 1) {%>
					<button disabled><</button>
				<%} else { %>
					<button onclick="location.href='<%=request.getContextPath()%>/loadList.faq?currentPage=<%=currentPage - 1%>'"><</button>
				<% } %>
				
				<% for(int p = startPage; p <= endPage; p++) { 
					if(p == currentPage) {
				%>
						<button disabled><%=p%></button>
				<%		
					}else {
				%>
					<button onclick="location.href='<%=request.getContextPath()%>/loadList.faq?currentPage=<%=p%>'"><%=p%></button>
				<%		
					}
				   } 
				%>
				
				<% if(currentPage >= maxPage) { %>
					<button disabled>></button>
				<% } else {%>
					<button onclick="location.href='<%=request.getContextPath()%>/loadList.faq?currentPage=<%=currentPage + 1%>'">></button>
				<% } %>
				
				<button onclick="location.href='<%=request.getContextPath()%>/loadList.faq?currentPage=<%=maxPage%>'">>></button>
				</div>
			</div>

		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	
	<script>
		$(function(){
			$("#underListTab tr").each(function(index){
				if(index !== 0) {
					
					$(this).children().each(function(index){
						if(index !== 0 && index % 3 == 0) {
							$(this).mouseenter(function(){
								$(this).parent().css({"cursor":"pointer"});
							}).mouseout(function(){
								$(this).parent().css({"cursor":"default"});
							}).click(function(){
								var num = $(this).parent().children().eq(1).text();
								location.href="<%=request.getContextPath()%>/loadDetail.faq?num=" + num;
							});
						}	
					});
				}
			});
			

			$("#deleteBtn").click(function(){
				var checkLength = $("input[name=faqCheck]:checked").length;
				var deleteNo = "";
				
				if(checkLength <= 0) {
					alert("최소 하나의 게시물은 선택해야합니다.");
				}else {
					var result = confirm("정말 해당 게시물을 삭제하시겠습니까?");
					
					if(result) {
						
						$("input[name=faqCheck]:checked").each(function(){
							deleteNo += $(this).parent().parent().children().eq(1).text() + "#";
						});
						
						var url = "<%=request.getContextPath()%>/delete.faq?deleteNo=" + encodeURIComponent(deleteNo);
						
						location.href=url;
					}
				}
			});
			
		});
		
	</script>
</body>
</html>