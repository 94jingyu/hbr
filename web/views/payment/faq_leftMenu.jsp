<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>

	/* 레프트메뉴 CSS */
	#wrap {
		width: 1200px;
		margin: 0 auto;
	}
   
	#leftMenu {
   		float: left; 
		width: 200px;
		margin-left: 10px; 
		border : 2px solid rgb(192, 192, 192);
    }
    
    #menu1, #menu2, #menu3, #menu4, #menu5, #menu6
     {
    	width: 100%;
    	height: 80px;


    }
    
    #menu1Lab, #menu2Lab, #menu3Lab, #menu4Lab, #menu5Lab, #menu6Lab 
    {
    	margin: 0 auto;
    	width: 80%;
    	line-height: 80px;
    	font-size: 18px;
    	font-weight: bold;
    	border-bottom: 1px solid rgb(192, 192, 192);
    }
    
     .leftStyle:hover {
     	cursor: pointer;
     	color: #2FA599;
     }
    
	
</style>
</head>
<body>
	<div id="wrap">
        	
		<div id="leftMenu">
			<div id="menu1">
				<div id="menu1Lab"style="font-size: 20px;">고객센터</div>
			</div>
			<div id="menu2" class="leftStyle">
				<div id="menu2Lab">공지사항</div>
			</div>
			<div id="menu3" class="leftStyle">
				<div id="menu3Lab">묻고답하기</div>
			</div>
			<div id="menu4" class="leftStyle">
				<div id="menu4Lab" onclick="location.href='<%=request.getContextPath()%>/loadFront.faq'">자주묻는질문</div>			
			</div>
		</div>
	</div>
</body>
</html>