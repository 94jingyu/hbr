<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@
	page import="com.kh.hbr.member.model.vo.Member"
 %>

<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://service.iamport.kr/js/iamport.payment-1.1.5.js"></script>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>

	#whole {
		width: 1200px;
		margin: 0 auto;
		
	}
	
	#mainArea {
		width: 80%;
		float: left;
	}
	
	.default {
		font-weight: 900;
		height: 50px;
	}
	
	#navi {
		font-size: 15px;
		height: 80px;
		margin-left: 30px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#pageTitle {
		height: 80px;
		font-size: 30px;
		margin-left: 30px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#caution {
		margin-top: 20px;
		margin-left: 50px;
	}
	
	#caution li, table li {
	 	margin-left: 30px;
	}
	
	/* 은도끼 영역 CSS */
	
	#itemArea2 {
		margin-top : 30px;
		margin-left: 30px;
		border: 1px solid black;
		width: 940px;
		height: 350px;
	}
	
	#silverLabel {
		font-size: 30px;
		font-weight: bold;
		margin-top: 10px;
		margin-left: 20px;
		margin-bottom: 10px;
	}
	
	.silverDetail {
		width: 280px;
		height: 260px;
		margin: 0 auto;
		float: left;
		border: 1px solid black;
		text-align: left;
	}
	
	#silverDetail1 {
		margin-left: 20px;
		margin-right: 20px;
	}
	
	#silverInner {
		width: 250px;
		height: 160px;
		border: 3px solid yellow;
		margin: 0 auto;
		margin-top: 25px;
	}
	
	#silverDetail2 {
		margin-right: 20px;
	}
	
	
	#silverImg {
		width: 120px;
		height: 80px;
		margin: 0 auto;
		margin-top: 5px;
	}
	
	.silverLabel {
		text-align:left;
		font-size: 20px;
		font-weight: bold;
		margin-left: 20px;
		margin-top: 5px;
		margin-bottom: 5px;
	}
	
	#silverLabel2 {
		font-size: 20px;
		margin-left: 20px;
	}
	
	#silverLabel3 {
		margin-top: 5px;
		margin-left: 20px;
	}
	
	#silverBtn {
		background: #013252;
		color: white;
		border-style: none;
		width: 80px;
		height: 30px;
		font-weight: bold;
		margin-top: 20px;
		margin-left: 190px;
	}
	
	#desList {
		margin-top: 10px;
		margin-left: 35px;
	}
	
	#desList > ul {
		line-height: 30px;
	}
	
	#price{
		width: 250px;
		height: 180px;
		margin: 0 auto;
		margin-top: 30px;
	}
	
	#priceTable {
		width: 100%;
		height: 100%;
		text-align: center;
		border-collapse: collapse;
	}

	#priceTable th {
		background: lightgrey;
	}	
	
	#priceTable th, td {
		border: 1px solid black;
	}
	
	#selectProductArea {
		background: #f6f3f3;
		margin-top: 30px;
		margin-left: 30px;
		padding-top: 15px;
		width: 940px;
		height: 300px;
	}
	
	#selectProTabDiv {
		margin: 0 auto;
		width: 800px;
		height: 200px;
	}
	
	#selectProTab {
		width: 100%;
		height: 100%;
		border-collapse: collapse;
	}
	
	#selectProTab, #selectProTab td {
		border: 1px solid #989898;
	}
	
	 #card {
		margin-left: 20px;
	}
	
	.tabTd2 {
		padding-left: 20px;
	}
	
	.tabTd1 {
		width: 120px;
		font-weight: bold;
		background: #013252;
		color: white;
	}
	
	#selectProLab {
		height: 35px;
		font-weight: bold;
		font-size: 30px;
		margin-bottom: 20px;
		margin-left: 20px;
	}
	
	#buyBtn {
		background: #013252;
		color: white;
		height: 50px;
		width: 100px;
		margin-left: 320px;
		border-style: none;
	}
	
	#backBtn {
		background: #2FA599;
		color: white;
		height: 50px;
		width: 100px;
		border-style: none;
		margin-left: 30px;
	}
	
	#proPrice {
		font-size: 22px;
		font-weight: bold;
		color: red;
	}
	
	#firstLab {
		margin-top: 80px;
		font-size: 25px;
		font-weight: bold;
		text-align: center;
	}
	
</style>
</head>
<body>
	<%@ include file="../guide/business_menubar.jsp" %>
	<%@ include file="payment_leftMenu.jsp" %>
	
	<% 
		if(bloginUser == null) {
	%>
		<script>
		alert("기업회원으로 로그인 해주세요.");
		location.href = "../common/E_LoginPage.jsp";
		</script>
	<%		
		}
	%>

	<!-- 전체 영역 -->
	<div id="whole">

		<!-- 컨텐츠 영역 -->
		<div id="mainArea">

			<div id="navi" class="default">HOME > 상품 결제관리 > 상품구매</div>
			<div id="pageTitle" class="default">상품소개/구매</div>
			<div id="caution">
				<ul>
					<li>모든 상품에는 구매후 사용기한이 있습니다. 사용기한내에 사용하지 않아 소멸된 상품은 환불이 불가합니다.</li>
					<li>구매후 사용한 상품에 대해서는 환불이 불가합니다.</li>
				</ul>
			</div>

			<!-- 금도끼 영역 -->
			<div id="itemArea2">

				<div id="silverLabel">은도끼 : 가장 핫한 광고는 바로 나!</div>
				
				<!-- 안에 3개의 div중 첫번째 -->
				<div class="silverDetail" id="silverDetail1">
					<div class="silverLabel" id="silverLab">상품이미지(예시)</div>
					<div id="silverInner">
						<div id="silverImg">
							<img src="../../static/images/payment/mcdonald.jpg" width="120px" height="80px">
						</div>
						<div id="silverLabel2">Mcdonald's 맥도날드</div>
						<div id="silverLabel3">햄버거 조리 크루 모집</div>
					</div>
				</div>


				<!-- 안에 3개의 div중 두번째 -->
				<div class="silverDetail" id="silverDetail2">
					<div class="silverLabel">상품설명</div>
					<div id="desList">
						<ul>
							<li>메인 페이지 중단에 노출</li>
							<li>뚜렷한 로고</li>
							<li>마우스 접근 시 테두리 강조</li>
							<li>알아보기 쉬운 텍스트</li>
							<li>광고효과 높은 최적의 사이즈</li>
						</ul>
					</div>
				</div>

				<!-- 안에 3개의 div중 세번째 -->
				<div class="silverDetail" id="silverDetail3">
					<div class="silverLabel">상품가격</div>
					<div id="price">
						<table id="priceTable">
							<tr>
								<th>선택</th>
								<th>개수</th>
								<th>가격</th>
								<th>기한</th>
							</tr>
							<tr>
								<td>
									<input type="radio" name="product" value="S1"> 
								</td>
								<td>1개</td>
								<td>3만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>
									<input type="radio" name="product" value="S2"> 
								</td>
								<td>5개</td>
								<td>15만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>
									<input type="radio" name="product" value="S3"> 
								</td>
								<td>10개</td>
								<td>27.5만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>
									<input type="radio" name="product" value="S4"> 
								</td>
								<td>30개</td>
								<td>81만원</td>
								<td>50일</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		
			
			<div id="selectProductArea">
			
				 <div id="selectProLab">결제정보</div>
				 
				 <div id="firstLab">아직 선택한 상품이 없습니다. 상품을 먼저 선택해주세요.</div>
				 <div id="selectProTabDiv">
				 	<table id="selectProTab" style="display:none;">
				 		<tr>
				 			<td class="tabTd1" align="center">상품금액</td>
				 			<td class="tabTd2"></td>
				 		</tr>
				 		<tr>
				 			<td class="tabTd1" align="center">선택상품</td>
				 			<td class="tabTd2"></td>
				 		</tr>
				 		<tr>
				 			<td class="tabTd1" align="center">결제금액</td>
				 			<td class="tabTd2" id="proPrice"></td>
				 		</tr>
				 		<tr>
				 			<td class="tabTd1" align="center">결제수단</td>
				 			<td><input type="radio" id="card" checked>&nbsp;신용카드</td>
				 		</tr>
				 	</table>
				 </div>
			</div>
			<br>
			<button id="backBtn" onclick="location.href='payment.jsp'">뒤로</button> <button id="buyBtn">구매하기</button>
			<br>
			<br>
			<br>
		</div>
	</div>
	
	<script>
		$(function(){
			var radio;
			var productCode = "";
			
			$("input[name=product]").change(function(){
				$("#firstLab").detach();
				$("#selectProTab").show();
				
				var num = $(this).val();
				productCode = num;
				console.log(productCode);
				var price;
				var expDate = "은도끼 - 메인페이지 중단 노출상품 (사용기간 : 결제 완료 후 30일 이내)";
			
				switch(num){
					case "S1" : price = "30,000 원"; break;
					case "S2" : price = "150,000 원"; break;
					case "S3" : price = "275,000 원"; break;
					case "S4" : price = "810,000 원"; expDate = "은도끼 - 메인페이지 중단 노출상품 (사용기간 : 결제 완료 후 50일 이내)"; break;
				}
				
				$("#selectProTab").children().children().children().eq(1).text(price);
				$("#selectProTab").children().children().children().eq(3).text(expDate);
				$("#selectProTab").children().children().children().eq(5).text(price);
			});
			
			$("#buyBtn").click(function(){
				
				var productName = $(".tabTd2").eq(1).text().substring(0,18);
				var productPrice = $(".tabTd2").eq(2).text();
				var payCheck = false;
				
				var IMP = window.IMP; //window.IMP 변수 선언

				IMP.init('imp44207208');

				IMP.request_pay({
					pg : 'uplus',
					pay_method : 'card',
					merchant_uid : 'merchant_' + new Date().getTime(),
					name : productName,
					//amount는 추후에 수정 현재는 100으로 설정
					//productPrice 위에 변수 사용
					amount : 100,
					
					<%
						if(bloginUser.getManagerEmail() != null) {
					%>
						buyer_email : '<%=bloginUser.getManagerEmail()%>',
					<%
						}else {
					%>		
						buyer_email : '',
					<%
						}
					%>
					
					buyer_name : '<%=bloginUser.getManagerName()%>',
					buyer_tel : '<%=bloginUser.getManagerPhone()%>'
					
				}, function(rsp) {
					if ( rsp.success ) {
						var msg = '결제가 완료되었습니다.';
						
						alert(msg);
						
						payCheck = true;
						if(payCheck) {
							//결제완료시 컨트롤러로 넘겨야하는내용
							//제품코드와 회원번호를 넘기면됨
							$.ajax({
								url: "<%=request.getContextPath()%>/buyProduct.pay",
								type: "get",
								data: {
									bMemberNo: "<%=bloginUser.getBmemberNo()%>",
									productCode: productCode,
									approveNo: rsp.imp_uid
								},
								success: function(data) {
									console.log(data);
								},
								error: function(error) {
									console.log(error);
								},
							});
						}
						
					} else {
						var msg = '결제에 실패하였습니다.';
						msg += '에러내용 : ' + rsp.error_msg;
						alert(msg);
					}
				});
			}); 
			
		});
	</script>

</body>
</html>