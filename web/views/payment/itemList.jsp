<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page
	import="java.util.ArrayList, com.kh.hbr.payment.model.vo.MyItem, com.kh.hbr.payment.model.vo.PageInfo"
 %>
    
<%
	 ArrayList<MyItem> itemList = (ArrayList<MyItem>)request.getAttribute("itemList");
	 PageInfo pi = (PageInfo) request.getAttribute("pi");
	 int listCount = pi.getListCount();
	 int currentPage = pi.getCurrentPage();
	 int maxPage = pi.getMaxPage();
	 int startPage = pi.getStartPage();
	 int endPage = pi.getEndPage();

%>

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	#whole {
		width: 1200px;
		margin: 0 auto;
	}
	
	#mainArea {
		width: 80%;
		float: left;
	}
	
	.default {
		font-weight: 900;
		height: 50px;
	}
	
	#navi {
		font-size: 15px;
		height: 80px;
		margin-left: 30px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#pageTitle {
		height: 80px;
		font-size: 30px;
		margin-left: 30px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#itemList {
		margin-top : 30px;
		margin-left: 30px;
		border: 2px solid #afafaf;
		width: 96%;
		height: 250px;
	}
	
	#itemInner {
		width: 100%;
		height: 220px;
		margin-top: 15px;
	}
	
	#listArea1 {
		width: 400px;
		height: 220px;
		float: left;
		border-right: 1px solid #afafaf;
	}
	
	#listArea2 {
		width: 280px;
		height: 220px;
		float: left;
		border-right: 1px solid #afafaf;
	}
	
	#listArea3 {
		width: 180px;
		height: 220px;
		float: left;
		margin-left: 15px;
	}
	
	#area1Lab {
		width: 300px;
		height: 30px;
		font-size: 21px;
		font-weight: bold;
		margin-top: 20px;
		margin-left: 95px;
	}
	
	.area1Td1 {
		width: 120px;
		text-align: center;
	}
	
	#area1Td2, #area1Td3, #area1Td4{
		font-size: 25px;
	}
	
	#area1Tab {
		width: 380px;
		margin: 0 auto;
		margin-top: 25px;
	}
	
	#area1Tab td {
		border-spacing: 70px 60px;
	}
	
	#area2Lab {
		width: 280px;
		height: 30px;
		font-size: 19px;
		font-weight: bold;
		margin-top: 20px;
		text-align: center;
	}
	
	#tabArea2 {
		width: 100px;
		height: 130px;
		margin-left: 90px;
		margin-top: 20px;
	}
	
	#area2Td1 {
		font-size: 25px;
	}
	
	#area3Lab {
		width: 170px;
		height: 30px;
		font-size: 19px;
		font-weight: bold;
		margin-left:15px;
		margin-top: 20px;
		text-align: center;
	}
	
	#tabArea3 {
		width: 100px;
		height: 160px;
		margin: 0 auto;
		margin-top: 20px;
	}
	
	#area3Td1 {
		font-size: 25px;
	}
	
	#area3Tab {
		text-align: center;
		margin-left: 15px;
	}
	
	#itemList2 {
		margin-top : 30px;
		margin-left: 30px;
		width: 96%;
		height: 250px;
	}
	
	#underLab {
		font-size: 25px;
		font-weight: bold;
	}
	
	#underListTabArea {
		width: 100%;
		margin-top: 15px;
	}
	
	#underListTab {
		width: 100%;
		border-collapse: collapse;
		border-left: 1px solid #afafaf;
		border-right: 1px solid #afafaf;
		border-bottom: 1px solid #afafaf;
		text-align: center;
	}
	
	#tabTr1 {
		border-top: 1px solid #afafaf;
		border-bottom: 1px solid #afafaf;
		background: #f0f0f0;
	}
	
	#underListTab td {
		height: 40px;
		border-bottom: 1px solid #afafaf;
	}
	
	#underListTab th {
		font-weight: 900;
	}
	
	.inlineB {
		display: inline-block;
	}
	
	/* 페이징 공용 css */
	#pagingArea > button {
		border-style: none;
		background: none;
		font-size: 18px;
		font-weight: bold;
		cursor: pointer;
		margin-top: 15px;
	}
	
	.former {
		margin-right: 10px;
	}
	
	.next {
		margin-left: 10px;
	}
	
	/* 페이징 공용 css end */
	
</style>
</head>
<body>
	<%@ include file="../guide/business_menubar.jsp" %>
	<%@ include file="payment_leftMenu.jsp" %>
		
	<script>
		$(function() {
			$("#menu3Lab").css("color", "#2FA599");
			$("#menu3Lab").removeAttr("onclick");
			$("#menu3").css("cursor", "default");
		});
	</script>

	<script>
		$(function(){
			$.ajax({
				url: "<%=request.getContextPath()%>/itemCount.pay",
				dataType: 'json',
				type: "post",
				data: {userNo: <%=bloginUser.getBmemberNo()%>},
				success: function(data){
					
					var gold = data.G;
					var silver = data.S;
					var steel = data.L;
					var star = data.T;
					var resume = data.R;
					
					$("#area1Td2").text('x ' + gold);
					$("#area1Td3").text('x ' + silver);
					$("#area1Td4").text('x ' + steel);

					$("#area2Td1").text('x ' + star);
					
					$("#area3Td1").text('x ' + resume);
				},error: function(error){
					console.log(error);
				}
			});
		});	

	</script>


	<!-- 전체 영역 -->
	<div id="whole">


		<!-- 컨텐츠 영역 -->
		<div id="mainArea">

			<div id="navi" class="default">HOME > 상품 결제관리 > 상품보유현황</div>
			<div id="pageTitle" class="default">상품보유현황</div>
	
			<div id="itemList">
				<div id="itemInner">
				
					<div id="listArea1">
						<div id="area1Lab">메인페이지 전용 상품</div>
						
							<div id="tabArea">
								<table id="area1Tab">
									<tr>
										<td class="area1Td1"><img src="/h/static/images/payment/goldenAxe.png" width="60px" height="60px"></td>
										<td class="area1Td1"><img src="/h/static/images/payment/silverAxe.png" width="60px" height="60px"></td>
										<td class="area1Td1"><img src="/h/static/images/payment/steelAxe.png" width="60px" height="60px"></td>
									</tr>
									<tr>
										<td align="center"><label id="area1Td2"></label><label style="font-size:10px;"> 개</label></td>
										<td align="center"><label id="area1Td3"></label><label style="font-size:10px;"> 개</label></td>
										<td align="center"><label id="area1Td4"></label><label style="font-size:10px;"> 개</label></td>
									</tr>
								</table>
							</div>
							<div id="line"></div>
					</div>
					
					<div id="listArea2">
						<div id="area2Lab">보금자리페이지 전용 상품</div>
						<div id="tabArea2">
							<table id="area2Tab">
								<tr>
									<td class="area1Td1" align="center"><img src="/h/static/images/payment/crown.png"
															width="80px" height="80px"></td>
								</tr>
								<tr>
									<td align="center"><label id="area2Td1"></label><label style="font-size:10px;"> 개</label></td>
								</tr>
							</table>
						</div>
					
					</div>
					
					<div id="listArea3">
						<div id="area3Lab">이력서열람 상품</div>
						<div id="tabArea3">
							<table id="area3Tab">
								<tr>
									<td class="area3Td1" align="center"><img src="/h/static/images/payment/resume.png"
															width="80px" height="100px" style="margin-left: 10px;"></td>
								</tr>
								<tr>
									<td align="center"><label id="area3Td1"></label><label style="font-size:10px;"> 개</label></td>
								</tr>
							</table>
						</div>
					</div>
					
					
				</div>
			</div>
			
			<div id="itemList2">
				<div id="underListArea">
					<div id="underLab" class="inlineB">상품보유내역</div>
					<div id="underLab2" class="inlineB" style="display:inline-block; width:375px; height:30px; line-height:30px;">＊현재 보유하고 있는 아이템 내역이 표시됩니다.</div>
					<div id="underLab3" class="inlineB" style="display:inline-block; width:150px; height:30px; line-height:30px; margin-left: 235px;">
						<select id="itemSearch" style="width: 150px; height: 30px;">
							<option value="0">선택</option>
							<option value="1">전체</option>
							<option value="2">금도끼</option>
							<option value="3">은도끼</option>
							<option value="4">쇠도끼</option>
							<option value="5">반짝</option>
							<option value="6">이력서</option>
						</select>
					</div>
					
					<div id="underListTabArea">
						<table id="underListTab">
							<tr id="tabTr1">
								<th class="tabTh" style="width:5%; height:35px;">번호</th>
								<th class="tabTh" style="width:30%;">상품명</th>
								<th class="tabTh" style="width:10%;">구매개수</th>
								<th class="tabTh" style="width:10%;">남은개수</th>
								<th class="tabTh" style="width:25%;">구매일</th>
								<th class="tabTh" style="width:25%;">유효기간</th>
							</tr>

							<% 
								int index = 0;
							
								if(itemList != null && itemList.size() != 0) {
									for(int i = 0; i < itemList.size(); i++) {
									MyItem item = (MyItem) itemList.get(i);
									int num = (listCount - index) - 
									((currentPage - 1) * pi.getLimit()); 
							%>
									<tr>
										<td><%=num%></td>
										<td><%=item.getProductName()%></td>
										<td><%=item.getProductCount()%></td>
										<td style="font-weight: bold;"><%=item.getMyItemUnit()%></td>
										<td><%=item.getPurchaseDate()%></td>
										<td><%=item.getMyItemExpDate()%></td>
									</tr>
							<% 
									index++;
									} 
								}else {
							%>
									<tr>
										<td colspan="6">보유한 아이템이 없습니다.</td>
									</tr>
							<%	
						
								}
							%>
						</table>
					</div>
					<div id="pagingArea" align="center">
						<button onclick="location.href='<%=request.getContextPath()%>/myItem.load?currentPage=1'"><<</button>
			
						<% if(currentPage <= 1) {%>
							<button disabled class="former"><</button>
						<% } else {%>
							<button class="former" onclick="location.href='<%=request.getContextPath()%>/myItem.load?currentPage=<%=currentPage - 1%>'"><</button>
						<% } %>
						
						<% for(int p = startPage; p <= endPage; p++) { 
							if(p == currentPage) {
						%>
								<button disabled style="color:red; cursor:default;"><%=p%></button>
						<%	} else {%>
								<button onclick="location.href='<%=request.getContextPath()%>/myItem.load?currentPage=<%=p%>'"><%=p%></button>
						<%  } %>
						<% } %>
						
						<% if(currentPage >= maxPage) {%>
							<button disabled class="next">></button>
						<% } else {%>
							<button class="next" onclick="location.href='<%=request.getContextPath()%>/myItem.load?currentPage=<%=currentPage + 1%>'">></button>
						<% } %>
						
						<button onclick="location.href='<%=request.getContextPath()%>/myItem.load?currentPage=<%=maxPage%>'">>></button>				
						<br>
						<br>
						<br>
						<br>
						<br>
					</div>					
					
				</div>
			</div>
		</div>
	</div>
	
	<script>
		$(function(){
			$("#itemSearch").change(function(){
				var value = $("#itemSearch").children(":selected").val();
				
				console.log(value);
				
				if(value !== "0") {
					location.href="<%=request.getContextPath()%>/searchMyItem?value=" + value + "&bNo=" + <%=bloginUser.getBmemberNo()%>;
				}
				
			});		
		});
	</script>

</body>
</html>