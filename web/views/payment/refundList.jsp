<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@
	page import = "java.util.ArrayList
				, com.kh.hbr.payment.model.vo.Refund
				, com.kh.hbr.payment.model.vo.PageInfo"
 %>
    
<%
	ArrayList<Refund> myRefundList = (ArrayList<Refund>) request.getAttribute("refundList");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();

%>
    
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<style>
	#whole {
		width: 1200px;
		margin: 0 auto;
		
	}
	
	#mainArea {
		width: 80%;
		float: left;
	}
	
	.default {
		font-weight: 900;
		height: 50px;
	}
	
	#navi {
		font-size: 15px;
		height: 80px;
		margin-left: 30px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#pageTitle {
		height: 80px;
		font-size: 30px;
		margin-left: 30px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#itemList {
		margin-top : 30px;
		margin-left: 30px;
		border: 2px solid #afafaf;
		width: 940px;
		height: 250px;
	}
	
	#itemList2 {
		margin-top : 15px;
		margin-left: 40px;
		width: 940px;
		height: 250px;
	}
	
	#underLab {
		font-size: 25px;
		font-weight: bold;
	}
	
	#underListTabArea {
		width: 940px;
	}
	
	#underListTab {
		width: 96%;
		border-collapse: collapse;
		border-left: 1px solid #afafaf;
		border-right: 1px solid #afafaf;
		border-bottom: 1px solid #afafaf;
		text-align: center;
		margin-top: 20px;
	}
	
	#tabTr1 {
		border-top: 1px solid #afafaf;
		border-bottom: 1px solid #afafaf;
		background: #f0f0f0;
	}
	
	#underListTab td {
		height: 40px;
		border-bottom: 1px solid #afafaf;
	}
	
	#underListTab th {
		font-weight: 900;
	}
	
	
	#underListTabArea {
		width: 940px;
		margin-top: 10px;
	}
	
	/* 페이징 공용 css */
	#pagingArea > button {
		border-style: none;
		background: none;
		font-size: 18px;
		font-weight: bold;
		cursor: pointer;
		margin-top: 20px;
	}
	
	.former {
		margin-right: 10px;
	}
	
	.next {
		margin-left: 10px;
	}
	
	/* 페이징 공용 css end */
	
	#introLab {
		width: 900px;
		margin-top: 20px;
		margin-bottom: 20px;
		padding-top: 10px;
		padding-bottom: 10px;
		border: 1px solid black;
	}
	
	#ulArea {
		margin-top: 5px;
		margin-left: 40px;
	}
	
	#resultLab {
		font-size: 15px;
		font-weight: bold;
	}

</style>
</head>
<body>
	
	<%@ include file="../guide/business_menubar.jsp" %>
	<%@ include file="payment_leftMenu.jsp" %>
		
	<script>
		$(function() {
			$("#menu6Lab").css("color", "#2FA599");
			$("#menu6Lab").removeAttr("onclick");
			$("#menu6").css("cursor", "default");
		});
	</script>
	
	<!-- 전체 영역 -->
	<div id="whole">

		<!-- 컨텐츠 영역 -->
		<div id="mainArea">

			<div id="navi" class="default">HOME > 상품 결제관리 > 상품결제내역</div>
			<div id="pageTitle" class="default">상품환불내역</div>
			
			
			<div id="itemList2">
			
					<div id="introLab">
						<label style="font-size: 18px; font-weight: bold;">&nbsp;&nbsp;환불관련 안내사항</label>
						<div id="ulArea">
								<ul>
									<li>전자상거래법 17조 1항 청약철회 및 계약해제의 범위에 따라 7일이내의 경우 환불이 가능합니다.</li>
									<li>악의적, 상습적인 환불의 경우 환불이 승인되지 않아 <반려>처리를 당할 수 있습니다.</li>
									<li>환불관련 문의사항은 고객센터와 1:1문의 이용 부탁드립니다.</li>
								</ul>
						</div>
					</div>
					<div id="resultLab">
						<label style="font-size:20px;">상품환불내역</label> 
						<label style="font-size:19px; font-weight:normal;">|</label>
						<label>총</label>
						<label style="color:red; font-size:18px;"><%=pi.getListCount()%></label><label>건</label>
					</div>	
					
					
					<div id="underListTabArea">
						<table id="underListTab">
							<tr id="tabTr1">
								<th class="tabTh" style="width:10%; height:35px;">번호</th>
								<th class="tabTh" style="width:10%;">결제번호</th>
								<th class="tabTh" style="width:15%;">상품명</th>
								<th class="tabTh" style="width:15%;">환불사유</th>
								<th class="tabTh" style="width:10%;">신청일자</th>
								<th class="tabTh" style="width:10%;">처리결과</th>
								<th class="tabTh" style="width:10%;">처리일자</th>
								
							</tr>
							
							<% 
								if(myRefundList != null && myRefundList.size() > 0) {
									int num = myRefundList.size();
									for(int i = 0; i < myRefundList.size(); i++) {
									Refund ref = myRefundList.get(i);
										String name = "";
										if(ref.getProductName().length() > 3) {
											name = ref.getProductName().substring(0, 3);
										}else {
											name = ref.getProductName().substring(0, 2);
										}
							%>
								<tr>
									<td><%=num%></td>
									<td><%=ref.getPurchaseNo()%></td>
									<td><%=name%></td>
									<td><%=ref.getRefundReason()%></td>
									<td><%=ref.getRefundApplyDate()%></td>
									
									<%  String result = ""; 
										if(ref.getRefundResult().equals("Y")) {
											result = "처리완료";
										}else if(ref.getRefundResult().equals("N")) {
											result = "반려";
										}else {
											result = "처리중";
										}
									%>
									<td><%=result%></td>
									
									<td><%=ref.getResultDate()%></td>
									
								</tr>
							<% 
									num--;
									}
								} else { %>
								<tr>
									<td colspan="8">환불요청한 상품이 없습니다.</td>
								</tr>
							
							<% } %>
						</table>
					</div>
					
					<div id="pagingArea" align="center">
						<button onclick="location.href='<%=request.getContextPath()%>/loadItemUse?currentPage=1'"><<</button>
			
						<% if(currentPage <= 1) {%>
							<button disabled class="former"><</button>
						<% } else {%>
							<button class="former" onclick="location.href='<%=request.getContextPath()%>/loadItemUse?currentPage=<%=currentPage - 1%>'"><</button>
						<% } %>
						
						<% for(int p = startPage; p <= endPage; p++) { 
							if(p == currentPage) {
						%>
								<button disabled style="color:red; cursor:default;"><%=p%></button>
						<%	} else {%>
								<button onclick="location.href='<%=request.getContextPath()%>/loadItemUse?currentPage=<%=p%>'"><%=p%></button>
						<%  } %>
						<% } %>
						
						<% if(currentPage >= maxPage) {%>
							<button disabled class="next">></button>
						<% } else {%>
							<button class="next" onclick="location.href='<%=request.getContextPath()%>/loadItemUse?currentPage=<%=currentPage + 1%>'">></button>
						<% } %>
						
						<button onclick="location.href='<%=request.getContextPath()%>/loadItemUse?currentPage=<%=maxPage%>'">>></button>				
					</div>	
			</div>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>		
	</div> <!-- mainarea end -->
	</div> <!-- wholearea end -->
</body>
</html>