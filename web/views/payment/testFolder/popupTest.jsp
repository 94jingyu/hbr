<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	
	<button onclick="showPopup();">클릭하기</button>

	<script>
		function showPopup(){
			//매개변수로 URL, 팝업창 이름, 옵션
			/* 
				width	open되는 팝업 창의 너비를 지정
				height	open되는 팝업창의 높이를 지정
			   	left	open되는 팝업창의 x축 위치를 지정
			   	top	open되는 팝업창의 y축 위치를 지정
	 		   	scrollbars	팝업창의 scrollbar를 Visible 여부를 설정 (scrollbars = no or scrollbars = yes)
			   	location	팝업창의 URL 입력란을 Visible 여부를 설정 (location= no or location= yes)
				toolbars	팝업창의 도구상자를 Visible 여부를 설정 (toolbars= no or toolbars= yes)
				status	팝업창의 상태 표시줄을 Visible 여부를 설정 (status= no or status= yes)
			*/
			var popupX = (window.screen.width / 2) - (200 / 2);
			// 만들 팝업창 좌우 크기의 1/2 만큼 보정값으로 빼주었음

			var popupY= (window.screen.height / 2) - (300 / 2);
			// 만들 팝업창 상하 크기의 1/2 만큼 보정값으로 빼주었음
			
			window.open(
				"paymentList.jsp"
				, "주소검색창"
				, "width=300, height=300,  left=" + popupX + ", top="+ popupY
			);
		};
	</script>
</body>
</html>