<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

</head>
<body>
	
	주소 : <input type="text" id="address" value="" style="width:240px;" readonly><br>
	상세주소 : <input type="text" id="address_etc" value="" style="width:200px;"> &nbsp;
	<button onclick="openDaumZipAddress();">주소찾기</button>

	
	<script>
		function openDaumZipAddress(){
			new daum.Postcode({
				oncomplete:function(data) {
	
					$("#address").val(data.address);
					$("#address_etc").focus();
	
					console.log(data);
				}
			}).open();
			
		}
	</script>

</body>
</html>