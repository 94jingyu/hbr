<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
 <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
</head>
<body>
<h1>토큰테스트</h1>
	
	 <button onclick="cancelPay()">환불하기</button>
	 
	 
	
	<script>

	$(function() {
			
		
		$.ajax({
			url: "<%=request.getContextPath()%>/token.get",
			type: "POST",
			success: function(data) {
				token = data;
				console.log(token);
				
				if(token != null) {
					//토큰을 얻는데 성공하였을 경우 환불을 요청
					
					$.ajax({
						url: "<%=request.getContextPath()%>/refund.pay",
						type: "POST",
						data: {
							url: "https://api.iamport.kr/payments/cancel",
							accessToken: token,
							imp_uid: "imp_978461238315"
						},
						success: function(data) {
							console.log(data);
						},
						error: function(error) {
							console.log(error);
						}
					});
				} 
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	
	
	
  </script>
	
	    
	    
	    
	
	

</body>
</html>