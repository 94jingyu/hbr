<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://service.iamport.kr/js/iamport.payment-1.1.5.js"></script>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	#whole {
		width: 1200px;
		margin: 0 auto;
		
	}
	
	#mainArea {
		width: 80%;
		float: left;
	}
	
	.default {
		font-weight: 900;
		height: 50px;
	}
	
	#navi {
		font-size: 15px;
		height: 80px;
		margin-left: 30px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#pageTitle {
		height: 80px;
		font-size: 30px;
		margin-left: 30px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#caution {
		margin-top: 20px;
		margin-left: 30px;
	}
	
	#caution li, table li {
	 	margin-left: 30px;
	}
	
	#itemArea5 {
		margin-top : 30px;
		margin-left: 30px;
		border: 1px solid black;
		width: 940px;
		height: 350px;
		
	}
	
	#resumeLabel {
		font-size: 30px;
		font-weight: bold;
		margin-top: 10px;
		margin-left: 20px;
		margin-bottom: 10px;
	}
	
	.resumeDetail {
		width: 280px;
		height: 260px;
		margin: 0 auto;
		float: left;
		border: 1px solid black;
		text-align: left;
	}
	
	#resumeDetail1 {
		margin-left: 20px;
		margin-right: 20px;
	}
	
	#resumeInner {
		width: 250px;
		height: 200px;
		margin : 0 auto;
	}
	
	#resumeDetail2 {
		margin-right: 20px;
	}
	
	#resumeImg {
		width: 130px;
		height: 160px;
		margin: 0 auto;
		margin-top: 20px;
	}
	
	.resumeLabel {
		text-align:left;
		font-size: 20px;
		font-weight: bold;
		margin-left: 20px;
		margin-top: 15px;
	}
	
	#resumeBtn {
		background: #013252;
		color: white;
		border-style: none;
		width: 80px;
		height: 30px;
		font-weight: bold;
		margin-top: 20px;
		margin-left: 180px;
	}
	
	#desList {
		margin-top: 10px;
		margin-left: 35px;
	}
	
	#desList > ul {
		line-height: 30px;
	}
	
	#price{
		width: 250px;
		height: 180px;
		margin: 0 auto;
		margin-top: 10px;
	}
	
	#priceTable {
		width: 100%;
		height: 100%;
		text-align: center;
		border-collapse: collapse;
	}

	#priceTable th {
		background: lightgrey;
	}	
	
	#priceTable th, td {
		border: 1px solid black;
	}
	
	#selectProductArea {
		background: #f6f3f3;
		margin-top: 30px;
		margin-left: 30px;
		padding-top: 15px;
		width: 940px;
		height: 300px;
	}
	
	#selectProTabDiv {
		margin: 0 auto;
		width: 800px;
		height: 200px;
	}
	
	#selectProTab {
		width: 100%;
		height: 100%;
		border-collapse: collapse;
	}
	
	#selectProTab, #selectProTab td {
		border: 1px solid #989898;
	}
	
	 #card {
		margin-left: 20px;
	}
	
	.tabTd2 {
		padding-left: 20px;
	}
	
	.tabTd1 {
		width: 120px;
		font-weight: bold;
		background: #013252;
		color: white;
	}
	
	#selectProLab {
		height: 35px;
		font-weight: bold;
		font-size: 30px;
		margin-bottom: 20px;
		margin-left: 20px;
	}
	
	#buyBtn {
		background: #013252;
		color: white;
		height: 50px;
		width: 100px;
		border-style: none;
		margin-left: 320px;
	}
	
	#backBtn {
		background: #2FA599;
		color: white;
		height: 50px;
		width: 100px;
		border-style: none;
		margin-left: 30px;
	}
	
	#proPrice {
		font-size: 22px;
		font-weight: bold;
		color: red;
	}
	
	#firstLab {
		margin-top: 80px;
		font-size: 25px;
		font-weight: bold;
		text-align: center;
	}
	
</style>
</head>
<body>
	<%@ include file="../guide/business_menubar.jsp" %>
	<%@ include file="payment_leftMenu.jsp" %>
	
	<% 
		if(bloginUser == null) {
	%>
		<script>
		alert("기업회원으로 로그인 해주세요.");
		location.href = "../common/E_LoginPage.jsp";
		</script>
	<%		
		}
	%>

	<!-- 전체 영역 -->
	<div id="whole">

		<!-- 컨텐츠 영역 -->
		<div id="mainArea">

			<div id="navi" class="default">HOME > 상품 결제관리 > 상품구매</div>
			<div id="pageTitle" class="default">상품소개/구매</div>
			<div id="caution">
				<ul>
					<li>모든 상품에는 구매후 사용기한이 있습니다. 사용기한내에 사용하지 않아 소멸된 상품은 환불이 불가합니다.</li>
					<li>구매후 사용한 상품에 대해서는 환불이 불가합니다.</li>
				</ul>
			</div>

			<!-- 이력서 영역 -->
			<div id="itemArea5">

				<div id="resumeLabel">이력서열람권 : 우리기업에 맞는 최적인재! 빠르게 스카웃하자!</div>
				
				<!-- 안에 3개의 div중 첫번째 -->
				<div class="resumeDetail" id="resumeDetail1">
					<div class="resumeLabel">상품이미지(예시)</div>
					<div id="resumeInner">
						<div id="resumeImg">
							<img src="../../static/images/payment/resume.png" width="130px" height="160px">
						</div>
					</div>
				</div>


				<!-- 안에 3개의 div중 두번째 -->
				<div class="resumeDetail" id="resumeDetail2">
					<div class="resumeLabel">상품설명</div>
					<div id="desList">
						<ul>
							<li>인재검색시 연락처 확인 가능</li>
							<li>맞춤인재에게 면접제의 가능</li>
							<li>하나의 상품으로 인재열람과<br>
								면접제의를 동시에!
							</li>
						</ul>
					</div>
				</div>

				<!-- 안에 3개의 div중 세번째 -->
				<div class="resumeDetail" id="resumeDetail3">
					<div class="resumeLabel">상품가격</div>
					<div id="price">
						<table id="priceTable">
							<tr>
								<th>선택</th>
								<th>개수</th>
								<th>가격</th>
								<th>기한</th>
							</tr>
							<tr>
								<td>
									<input type="radio" name="product" value="R1"> 
								</td>
								<td>10개</td>
								<td>1000원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>
									<input type="radio" name="product" value="R2"> 
								</td>
								<td>30개</td>
								<td>3000원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>
									<input type="radio" name="product" value="R3"> 
								</td>
								<td>50개</td>
								<td>5000원</td>
								<td>30일</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		
			
			<div id="selectProductArea">
			
				 <div id="selectProLab">결제정보</div>
				 
				 <div id="firstLab">아직 선택한 상품이 없습니다. 상품을 먼저 선택해주세요.</div>
				 <div id="selectProTabDiv">
				 	<table id="selectProTab" style="display:none;">
				 		<tr>
				 			<td class="tabTd1" align="center">상품금액</td>
				 			<td class="tabTd2"></td>
				 		</tr>
				 		<tr>
				 			<td class="tabTd1" align="center">선택상품</td>
				 			<td class="tabTd2"></td>
				 		</tr>
				 		<tr>
				 			<td class="tabTd1" align="center">결제금액</td>
				 			<td class="tabTd2" id="proPrice"></td>
				 		</tr>
				 		<tr>
				 			<td class="tabTd1" align="center">결제수단</td>
				 			<td><input type="radio" id="card" checked>&nbsp;신용카드</td>
				 		</tr>
				 	</table>
				 </div>
			</div>
			<br>
			<button id="backBtn" onclick="location.href='payment.jsp'">뒤로</button><button id="buyBtn">구매하기</button>
			<br>
			<br>
			<br>
		</div>
	</div>
	
	<script>
		$(function(){
			var productCode;
			
			$("input[name=product]").change(function(){
				$("#firstLab").detach();
				$("#selectProTab").show();
				
				var num = $(this).val();
				productCode = num;
				var price;
				var expDate = "이력서 열람권 - 이력서 열람 및 면접제의 가능 상품 (사용기간 : 결제 완료 후 30일 이내)";
			
				switch(num){
					case "R1" : price = "1,000 원"; break;
					case "R2" : price = "3,000 원"; break;
					case "R3" : price = "5,000 원"; break;
				}
				
				$("#selectProTab").children().children().children().eq(1).text(price);
				$("#selectProTab").children().children().children().eq(3).text(expDate);
				$("#selectProTab").children().children().children().eq(5).text(price);
			});
			
			$("#buyBtn").click(function(){
				
				var productName = $(".tabTd2").eq(1).text().substring(0,18);
				var productPrice = $(".tabTd2").eq(2).text();
				var payCheck = false;
				
				var IMP = window.IMP; //window.IMP 변수 선언

				IMP.init('imp44207208');

				IMP.request_pay({
					pg : 'uplus',
					pay_method : 'card',
					merchant_uid : 'merchant_' + new Date().getTime(),
					name : productName,
					//amount는 추후에 수정 현재는 100으로 설정
					//productPrice 위에 변수 사용
					amount : 100,
					
					<%
						if(bloginUser.getManagerEmail() != null) {
					%>
						buyer_email : '<%=bloginUser.getManagerEmail()%>',
					<%
						}else {
					%>		
						buyer_email : '',
					<%
						}
					%>
					
					buyer_name : '<%=bloginUser.getManagerName()%>',
					buyer_tel : '<%=bloginUser.getManagerPhone()%>'
					
				}, function(rsp) {
					if ( rsp.success ) {
						var msg = '결제가 완료되었습니다.';
						
						alert(msg);
						
						payCheck = true;
						if(payCheck) {
							//결제완료시 컨트롤러로 넘겨야하는내용
							//제품코드와 회원번호를 넘기면됨
							$.ajax({
								url: "<%=request.getContextPath()%>/buyProduct.pay",
								type: "get",
								data: {
									bMemberNo: "<%=bloginUser.getBmemberNo()%>",
									productCode: productCode,
									approveNo: rsp.imp_uid
								},
								success: function(data) {
									console.log(data);
								},
								error: function(error) {
									console.log(error);
								},
							});
						}
						
					} else {
						var msg = '결제에 실패하였습니다.';
						msg += '에러내용 : ' + rsp.error_msg;
						alert(msg);
					}
				});
			}); 
			
			
		});
	</script>

</body>
</html>