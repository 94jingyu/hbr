<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@
	page import = "java.util.ArrayList
				, com.kh.hbr.payment.model.vo.Purchase
				, com.kh.hbr.payment.model.vo.PageInfo
				, java.text.DecimalFormat"
 %>
    
<%
	ArrayList<Purchase> myPaymentList = (ArrayList<Purchase>) request.getAttribute("requestPurchase");
	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();

%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<style>
	#whole {
		width: 1200px;
		margin: 0 auto;
		
	}
	
	#mainArea {
		width: 80%;
		float: left;
	}
	
	.default {
		font-weight: 900;
		height: 50px;
	}
	
	#navi {
		font-size: 15px;
		height: 80px;
		margin-left: 30px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#pageTitle {
		height: 80px;
		font-size: 30px;
		margin-left: 30px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#introLab {
		width: 900px;
		margin-top: 20px;
		margin-bottom: 20px;
		padding-top: 10px;
		padding-bottom: 10px;
		border: 1px solid black;
	}
	
	#ulArea {
		margin-top: 5px;
		margin-left: 40px;
	}
	
	#itemList {
		margin-top : 30px;
		margin-left: 30px;
		border: 2px solid #afafaf;
		width: 940px;
		height: 250px;
	}
	
	#itemList2 {
		margin-top : 15px;
		margin-left: 40px;
		width: 940px;
	}
	
	#underLab {
		font-size: 25px;
		font-weight: bold;
	}
	
	#underListTabArea {
		width: 940px;
	}
	
	#underListTab {
		width: 96%;
		border-collapse: collapse;
		border-left: 1px solid #afafaf;
		border-right: 1px solid #afafaf;
		border-bottom: 1px solid #afafaf;
		text-align: center;
		margin-top: 20px;
	}
	
	#tabTr1 {
		border-top: 1px solid #afafaf;
		border-bottom: 1px solid #afafaf;
		background: #f0f0f0;
	}
	
	#underListTab td {
		height: 40px;
		border-bottom: 1px solid #afafaf;
	}
	
	#underListTab th {
		font-weight: 900;
	}
	
	
	#underListTabArea {
		width: 940px;
		margin-top: 10px;
	}
	
	.refundBtn {
		width: 50px;
		height: 30px;
		background: #013252;
		color: white;
		border: 1px solid white;
		border-radius:5px;
		background: red;
		line-height: 30px;
	}
	
	.refundBtn:hover {
		cursor: pointer;
	}
	
	 /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
    
        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 0 auto; /* 15% from the top and centered */
            margin-top: 200px;
            padding: 20px;
            border: 1px solid #888;
            width: 500px; /* Could be more or less, depending on screen size */ 
            height: 450px;                         
        }
        
		#closeBtn {
			width: 80px;
			height: 40px;
			color: white;
			border: 1px solid white;
			border-radius:5px;
			background: red;
		}
		
		#finalRefund {
			width: 80px;
			height: 40px;
			background: #013252;
			color: white;
			border: 1px solid white;
			border-radius:5px;
			margin-left: 165px;
		}
		
		.reBtn {
			margin-top: 20px;
		}
		
	#pagingArea {
		margin-top: 20px;
	}
		
	#pagingArea > button {
		border-style: none;
		background: none;
		font-size: 18px;
		font-weight: bold;
		cursor: pointer;
		margin-top: 15px;
	}
	
	.former {
		margin-right: 10px;
	}
	
	.next {
		margin-left: 10px;
	}
		
	#resultLab {
		font-size: 15px;
		font-weight: bold;
	}


</style>
</head>
<body>
	<%@ include file="../guide/business_menubar.jsp" %>
	<%@ include file="payment_leftMenu.jsp" %>
		
	<script>
		$(function() {
			$("#menu5Lab").css("color", "#2FA599");
			$("#menu5Lab").removeAttr("onclick");
			$("#menu5").css("cursor", "default");
		});
	</script>
	
	<!-- 전체 영역 -->
	<div id="whole">


		<!-- 컨텐츠 영역 -->
		<div id="mainArea">

			<div id="navi" class="default">HOME > 상품 결제관리 > 상품결제내역</div>
			<div id="pageTitle" class="default">상품결제내역</div>
			
			
			<div id="itemList2">
					<div id="introLab">
						<label style="font-size: 18px; font-weight: bold;">&nbsp;&nbsp;결제 및 청약철회 관련 안내사항</label>
						<div id="ulArea">
								<ul>
									<li>청약철회는 구매일로부터 7일이내의 상품만, 사용하지 않은 경우에 가능합니다.</li>
									<li>결제, 환불관련 문의사항은 고객센터와 1:1문의 이용 부탁드립니다. 더욱 자세한 안내가 가능합니다.</li>
								</ul>
						</div>
					</div>
					<div id="resultLab">
						<label style="font-size:20px;">상품결제내역</label> 
						<label style="font-size:19px; font-weight:normal;">|</label>
						<label>총</label>
						<label style="color:red; font-size:18px;"><%=pi.getListCount()%></label><label>건</label>
					</div>	
					<div id="underListTabArea">
						<table id="underListTab">
							<tr id="tabTr1">
								<th class="tabTh" style="width:10%; height:35px;">번호</th>
								<th class="tabTh" style="width:10%;">결제번호</th>
								<th class="tabTh" style="width:15%;">상품명</th>
								<th class="tabTh" style="width:10%;">구매단위</th>
								<th class="tabTh" style="width:15%;">결제금액</th>
								<th class="tabTh" style="width:15%;">결제수단</th>
								<th class="tabTh" style="width:15%;">구매일자</th>
								<th class="tabTh" style="width:10%;">청약철회</th>
							</tr>
							
							<%
								int index = 0;
								if(myPaymentList != null && myPaymentList.size() != 0) {
									String name = "";
									for(int i = 0; i < myPaymentList.size(); i++) {
										Purchase pur = myPaymentList.get(i);
										int num = (listCount - index) - 
										((currentPage - 1) * pi.getLimit()); 
										
										if(pur.getProductName().length() > 3) {
											name = pur.getProductName().substring(0, 3);
										}else {
											name = pur.getProductName().substring(0, 2);
										}
							%>		
								<tr>
									<td><%=num%></td>
									<td><%=pur.getPurchaseNo()%></td>
									<td><%=name%></td>
									<td><%=pur.getSalesUnit()%></td>
									
									<% 
										DecimalFormat df = new DecimalFormat("#,###");
									%>
									
									<td><%=df.format(pur.getPrice())%></td>
									<td>신용카드</td>
									<td><%=pur.getPurchaseDate()%></td>
														
									<% if(pur.getSalesUnit() == pur.getMyItemUnit() && pur.getRefundYn().equals("N")) {%>
									<td><button class="refundBtn">철회</button></td>
									<% } else { %>
									<td></td>
									<% } %>
								</tr>
							<%
										index++;
									}
								}else {
							%>
								<tr>
									<td colspan="8">보유한 아이템이 없습니다.</td>
								</tr>
							
							<% } %>
						</table>
					</div>
					
					<div id="pagingArea" align="center">
						<button onclick="location.href='<%=request.getContextPath()%>/myPaymentList.load?currentPage=1'"><<</button>
			
						<% if(currentPage <= 1) {%>
							<button disabled class="former"><</button>
						<% } else {%>
							<button class="former" onclick="location.href='<%=request.getContextPath()%>/myPaymentList.load?currentPage=<%=currentPage - 1%>'"><</button>
						<% } %>
						
						<% for(int p = startPage; p <= endPage; p++) { 
							if(p == currentPage) {
						%>
								<button disabled style="color:red; cursor:default;"><%=p%></button>
						<%	} else {%>
								<button onclick="location.href='<%=request.getContextPath()%>/myPaymentList.load?currentPage=<%=p%>'"><%=p%></button>
						<%  } %>
						<% } %>
						
						<% if(currentPage >= maxPage) {%>
							<button disabled class="next">></button>
						<% } else {%>
							<button class="next" onclick="location.href='<%=request.getContextPath()%>/myPaymentList.load?currentPage=<%=currentPage + 1%>'">></button>
						<% } %>
						
						<button onclick="location.href='<%=request.getContextPath()%>/myPaymentList.load?currentPage=<%=maxPage%>'">>></button>				
						<br>
						<br>
						<br>
						<br>
						<br>
					</div>	
			</div>
			
		
	</div> <!-- mainarea end -->
	</div> <!-- wholearea end -->
	<br>
	<br>
	<br>
	<br>
	
	<script>
		$(function(){
			$("#underListTab").children().children().each(function(index) {
				if(index > 0) {
					var purchaseDateArr = $(this).children().eq(6).text().split("-");
					var purchaseDate = Number(purchaseDateArr[0] + purchaseDateArr[1] + purchaseDateArr[2]);
					
					var refundDate = purchaseDate + 6;
					
					var now = new Date();
					var year = now.getFullYear();
					var month = (now.getMonth() + 1) > 9 ? '' + now.getMonth() + 1 : '0' + (now.getMonth() + 1);
					var day = now.getDate() > 9 ? '' + now.getDate() : '0' + now.getDate();
					
					var today = Number(year + month + day);
					
					var tdNum = $(this).children().eq(0).text() - 1;
					
					if(refundDate < today) {
						$(this).children().eq(7).html('');	
					}
				}
				
				
			});

		});
		
	</script>
	
	<!-- modal 영역 -->
	
	<div id="myModal" class="modal">
 
      <!-- Modal content -->
      <div class="modal-content">
                <div style="text-align:center; font-size: 35px; font-weight: bold;">
                	환불신청
                </div>
                <div style="margin-top: 10px;">
                <label style="color: red; fond-size: bold; font-size: 18px;">*주의사항</label>
                </div>
                <div style="margin-top: 10px; margin-left: 15px;">
                	<ul style="font-size: 16px; line-height: 25px;">
                		<li>환불은 구매일로부터 7일이내의 상품만 가능합니다.</li>
                		<li>환불은 사용하지 않은 상품만 가능합니다.</li>
                		<li>환불 신청 후 관리자의 승인을 거쳐 환불이 완료됩니다.</li>
                		<li>신청한 환불 내역은 상품결제관리 > 환불내역에서 확인가능합니다.</li>
                	</ul>	
                </div>
                <div style="width: 90%; height: 150px; border: 1px solid black; margin: 0 auto; margin-top: 15px;">
					<div style="font-size: 18px; margin-left: 10px; margin-top: 10px;">환불사유선택</div>
					<div style="margin-left: 15px; margin-top: 10px;">
					<input type="radio" name="refundReason" value="1" id="change">&nbsp;<label for="change">단순변심</label></div>               
					<div style="margin-left: 15px; margin-top: 10px;">
					<input type="radio" name="refundReason" value="2" id="mistake">&nbsp;<label for="mistake">실수로 구매</label></div>     
					<div style="margin-left: 15px; margin-top: 10px;">
					<input type="radio" name="refundReason" value="3" id="guitar">&nbsp;<label for="guitar">기타</label> <input type="text" maxlength="15" id="guitarText" style="width:250px;"disabled>     
					</div>          
                </div>
            <div>
            <button id="finalRefund" class="reBtn">환불신청</button>&nbsp;<button id="closeBtn" class="reBtn" onclick="close_pop();">닫기</button></div>
      </div>
 
    </div>
    
    <script>
		function close_pop() {
	   	 	$('#myModal').hide();
		}

		$(function() {
			
			var refundObject;
			
			$("input:radio").click(function(){
				var check = $(this).val();
				
				if(check === "3") {
					$("#guitarText").attr("disabled", false);
				}else {
					$("#guitarText").attr("disabled", true);
				}
			});
			
			$(".refundBtn").click(function(){
											
	           $('#myModal').show();
	            
	           $.ajax({
	            	url: "<%=request.getContextPath()%>/loadOneMyPayment.pay",
	            	type: "POST",
	            	data: {
	            		purchaseNo: $(this).parent().parent().children().eq(1).text()
	            	},
	            	success: function(data) {
	            		refundObject = data[0];
	            	},
	            	error: function(error) {
	            		
	            	}
	           });
	           
			});
			
			$("#finalRefund").click(function(){
				
				var refundReason = "";
				
				if($("input:radio:checked").val() === "3") {
					refundReason = $("input:radio:checked").val() + "$" + $("#guitarText").val();
				}else {
					refundReason = $("input:radio:checked").val();
				}
				
				 $.ajax({
					url: "<%=request.getContextPath()%>/reqRefund.pay",
					type: "POST",
					data: {
						myItemNo: refundObject.myItemNo,
						refundReason: refundReason,
						purchaseNo: refundObject.purchaseNo,
						approveNo: refundObject.approveNo,
						bMemberNo: "<%=bloginUser.getBmemberNo()%>"
					},
					success: function(data) {
						alert("환불신청이 완료되었습니다.");
					},
					error: function(error){
						console.log(error);
					},
					complete: function() {
						$("#myModal").hide();
					}
				});
				
			});
			
		});
    </script>
</body>
</html>