<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>

	/* 레프트메뉴 CSS */
	#wrap {
		width: 1200px;
		margin: 0 auto;
	}
   
	#leftMenu {
   		float: left; 
		width: 200px;
		margin-left: 10px; 
		margin-top: 30px;
		border : 2px solid rgb(192, 192, 192);
    }
    
    #menu1, #menu2, #menu3, #menu4, #menu5, #menu6
     {
    	width: 100%;
    	height: 80px;


    }
    
    #menu1Lab, #menu2Lab, #menu3Lab, #menu4Lab, #menu5Lab, #menu6Lab 
    {
    	margin: 0 auto;
    	width: 80%;
    	line-height: 80px;
    	font-size: 18px;
    	font-weight: bold;
    	border-bottom: 1px solid rgb(192, 192, 192);
    }
    
     .leftStyle:hover {
     	cursor: pointer;
     	color: #2FA599;
     }
    
	
</style>
</head>
<body>
	<div id="wrap">
        	
		<div id="leftMenu">
			<div id="menu1">
				<div id="menu1Lab"style="font-size: 20px;">상품결제·관리</div>
			</div>
			<div id="menu2" class="leftStyle">
				<div id="menu2Lab" onclick="location.href='/h/views/payment/payment.jsp';">상품소개/구매</div>
			</div>
			<div id="menu3" class="leftStyle">
				<div id="menu3Lab" onclick="location.href='<%=request.getContextPath()%>/myItem.load'">상품보유현황</div>
			</div>
			<div id="menu4" class="leftStyle">
				<div id="menu4Lab" onclick="location.href='<%=request.getContextPath()%>/loadItemUse'">상품사용내역</div>			
			</div>
			<div id="menu5" class="leftStyle" style="border-style: none;" onclick="location.href='<%=request.getContextPath()%>/myPaymentList.load'">
				<div id="menu5Lab">상품결제내역</div>				
			</div>
			<div id="menu6" class="leftStyle" style="border-style: none;" onclick="location.href='<%=request.getContextPath()%>/refundList.pay'">
				<div id="menu6Lab">상품환불내역</div>				
			</div>
		</div>
	</div>
</body>
</html>