<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@
	page import="com.kh.hbr.member.model.vo.Member"
%>    

<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://service.iamport.kr/js/iamport.payment-1.1.5.js"></script>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	#whole {
		width: 1200px;
		margin: 0 auto;
		
	}
	
	#mainArea {
		width: 80%;
		float: left;
	}
	
	.default {
		font-weight: 900;
		height: 50px;
	}
	
	#navi {
		font-size: 15px;
		height: 80px;
		margin-left: 30px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#pageTitle {
		height: 80px;
		font-size: 30px;
		margin-left: 30px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#caution {
		margin-top: 20px;
		margin-left: 30px;
	}
	
	#caution li, table li {
	 	margin-left: 30px;
	}
	
	#itemArea3 {
		margin-top : 30px;
		margin-left: 30px;
		border: 1px solid black;
		width: 940px;
		height: 350px;
	}
	
	#steelLabel {
		font-size: 30px;
		font-weight: bold;
		margin-top: 10px;
		margin-left: 20px;
		margin-bottom: 10px;
	}
	
	.steelDetail {
		width: 280px;
		height: 260px;
		margin: 0 auto;
		float: left;
		border: 1px solid black;
		text-align: left;
	}
	
	#steelDetail1 {
		margin-left: 20px;
		margin-right: 20px;
	}
	
	#steelInner {
		width: 230px;
		height: 150px;
		border: 1px solid black;
		margin: 0 auto;
		margin-top: 25px;
	}
	
	#steelDetail2 {
		margin-right: 20px;
	}
	
	.steelLabel {
		text-align:left;
		font-size: 20px;
		font-weight: bold;
		margin-left: 20px;
		margin-top: 5px;
		margin-bottom: 5px;
	}
	
	#steelLabel1 {
		font-size: 20px;
		margin-top: 20px;
		margin-left: 20px;
	}
	
	#steelLabel2 {
		margin-top: 10px;
		margin-left: 20px;
	}
	
	#steelLabel3 {
		margin-top: 5px;
		margin-left: 20px;
	}
	
	#steelBtn {
		background: #013252;
		color: white;
		border-style: none;
		width: 80px;
		height: 30px;
		font-weight: bold;
		margin-top: 20px;
		margin-left: 180px;
	}
	
	#desList {
		margin-top: 10px;
		margin-left: 40px;
	}
	
	#desList > ul {
		line-height: 30px;
	}
	
	#price{
		width: 250px;
		height: 180px;
		margin: 0 auto;
		margin-top: 30px;
	}
	
	#priceTable {
		width: 100%;
		height: 100%;
		text-align: center;
		border-collapse: collapse;
	}

	#priceTable th {
		background: lightgrey;
	}	
	
	#priceTable th, td {
		border: 1px solid black;
	}
	
	#selectProductArea {
		background: #f6f3f3;
		margin-top: 30px;
		margin-left: 30px;
		padding-top: 15px;
		width: 940px;
		height: 300px;
	}
	
	#selectProTabDiv {
		margin: 0 auto;
		width: 800px;
		height: 200px;
	}
	
	#selectProTab {
		width: 100%;
		height: 100%;
		border-collapse: collapse;
	}
	
	#selectProTab, #selectProTab td {
		border: 1px solid #989898;
	}
	
	 #card {
		margin-left: 20px;
	}
	
	.tabTd2 {
		padding-left: 20px;
	}
	
	.tabTd1 {
		width: 120px;
		font-weight: bold;
		background: #013252;
		color: white;
	}
	
	#selectProLab {
		height: 35px;
		font-weight: bold;
		font-size: 30px;
		margin-bottom: 20px;
		margin-left: 20px;
	}
	
	#buyBtn {
		background: #013252;
		color: white;
		height: 50px;
		width: 100px;
		border-style: none;
		margin-left: 320px;
	}
	
	#backBtn {
		background: #2FA599;
		color: white;
		height: 50px;
		width: 100px;
		border-style: none;
		margin-left: 30px;
	}
	
	#proPrice {
		font-size: 22px;
		font-weight: bold;
		color: red;
	}
	
	#firstLab {
		margin-top: 80px;
		font-size: 25px;
		font-weight: bold;
		text-align: center;
	}
	
</style>
</head>
<body>
	<%@ include file="../guide/business_menubar.jsp" %>
	<%@ include file="payment_leftMenu.jsp" %>

	<% 
		if(bloginUser == null) {
	%>
		<script>
		alert("기업회원으로 로그인 해주세요.");
		location.href = "../common/E_LoginPage.jsp";
		</script>
	<%		
		}
	%>

	<!-- 전체 영역 -->
	<div id="whole">

		<!-- 컨텐츠 영역 -->
		<div id="mainArea">

			<div id="navi" class="default">HOME > 상품 결제관리 > 상품구매</div>
			<div id="pageTitle" class="default">상품소개/구매</div>
			<div id="caution">
				<ul>
					<li>모든 상품에는 구매후 사용기한이 있습니다. 사용기한내에 사용하지 않아 소멸된 상품은 환불이 불가합니다.</li>
					<li>구매후 사용한 상품에 대해서는 환불이 불가합니다.</li>
				</ul>
			</div>

			<!-- 금도끼 영역 -->
			<div id="itemArea3">

				<div id="steelLabel">쇠도끼 : 알짜배기 광고는 바로 나!</div>
				
				<!-- 안에 3개의 div중 첫번째 -->
				<div class="steelDetail" id="steelDetail1">
					<div class="steelLabel" id="steelLab">상품이미지</div>
					<div id="steelInner">
						<div id="steelLabel1">(주)경비나라</div>
						<div id="steelLabel2">역삼역 KH정보교육원 <br>경비채용</div>
						<div id="steelLabel3">서울시 강남구</div>
					</div>
				</div>


				<!-- 안에 3개의 div중 두번째 -->
				<div class="steelDetail" id="steelDetail2">
					<div class="steelLabel">상품설명</div>
					<div id="desList">
						<ul>
							<li>메인페이지 하단에 노출</li>
							<li>저렴한 가격 높은 만족도</li>
							<li>또렷한 기업명</li>
							<li>필요한 채용정보만 쏙쏙!</li>
							<li>로고 없는 은도끼!</li>
						</ul>
					</div>
				</div>

				<!-- 안에 3개의 div중 세번째 -->
				<div class="steelDetail" id="steelDetail3">
					<div class="steelLabel">상품가격</div>
					<div id="price">
						<table id="priceTable">
							<tr>
								<th>선택</th>
								<th>개수</th>
								<th>가격</th>
								<th>기한</th>
							</tr>
							<tr>
								<td>
									<input type="radio" name="product" value="L1"> 
								</td>
								<td>1개</td>
								<td>2만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>
									<input type="radio" name="product" value="L2"> 
								</td>
								<td>5개</td>
								<td>10만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>
									<input type="radio" name="product" value="L3"> 
								</td>
								<td>10개</td>
								<td>19만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>
									<input type="radio" name="product" value="L4"> 
								</td>
								<td>30개</td>
								<td>54만원</td>
								<td>50일</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		
			
			<div id="selectProductArea">
			
				 <div id="selectProLab">결제정보</div>
				 
				 <div id="firstLab">아직 선택한 상품이 없습니다. 상품을 먼저 선택해주세요.</div>
				 <div id="selectProTabDiv">
				 	<table id="selectProTab" style="display:none;">
				 		<tr>
				 			<td class="tabTd1" align="center">상품금액</td>
				 			<td class="tabTd2"></td>
				 		</tr>
				 		<tr>
				 			<td class="tabTd1" align="center">선택상품</td>
				 			<td class="tabTd2"></td>
				 		</tr>
				 		<tr>
				 			<td class="tabTd1" align="center">결제금액</td>
				 			<td class="tabTd2" id="proPrice"></td>
				 		</tr>
				 		<tr>
				 			<td class="tabTd1" align="center">결제수단</td>
				 			<td><input type="radio" id="card" checked>&nbsp;신용카드</td>
				 		</tr>
				 	</table>
				 </div>
			</div>
			<br>
			<button id="backBtn" onclick="location.href='payment.jsp'">뒤로</button><button id="buyBtn">구매하기</button>
			<br>
			<br>
			<br>
		</div>
	</div>
	
	<script>
		$(function(){
			var productCode = "";
			
			$("input[name=product]").change(function(){
				$("#firstLab").detach();
				$("#selectProTab").show();
				
				var num = $(this).val();
				productCode = num;
				var price;
				var expDate = "쇠도끼 - 메인페이지 하단 노출상품 (사용기간 : 결제 완료 후 30일 이내)";
			
				switch(num){
					case "L1" : price = "20,000 원"; break;
					case "L2" : price = "100,000 원"; break;
					case "L3" : price = "190,000 원"; break;
					case "L4" : price = "540,000 원"; expDate = "쇠도끼 - 메인페이지 하단 노출상품 (사용기간 : 결제 완료 후 50일 이내)"; break;
				}
				
				$("#selectProTab").children().children().children().eq(1).text(price);
				$("#selectProTab").children().children().children().eq(3).text(expDate);
				$("#selectProTab").children().children().children().eq(5).text(price);
			});
			
			$("#buyBtn").click(function(){
				
				var productName = $(".tabTd2").eq(1).text().substring(0,18);
				var productPrice = $(".tabTd2").eq(2).text();
				var payCheck = false;
				
				var IMP = window.IMP; //window.IMP 변수 선언

				IMP.init('imp44207208');

				IMP.request_pay({
					pg : 'uplus',
					pay_method : 'card',
					merchant_uid : 'merchant_' + new Date().getTime(),
					name : productName,
					//amount는 추후에 수정 현재는 100으로 설정
					//productPrice 위에 변수 사용
					amount : 100,
					
					<%
						if(bloginUser.getManagerEmail() != null) {
					%>
						buyer_email : '<%=bloginUser.getManagerEmail()%>',
					<%
						}else {
					%>		
						buyer_email : '',
					<%
						}
					%>
					
					buyer_name : '<%=bloginUser.getManagerName()%>',
					buyer_tel : '<%=bloginUser.getManagerPhone()%>'
					
				}, function(rsp) {
					if ( rsp.success ) {
						var msg = '결제가 완료되었습니다.';
						
						alert(msg);
						
						payCheck = true;
						if(payCheck) {
							//결제완료시 컨트롤러로 넘겨야하는내용
							//제품코드와 회원번호를 넘기면됨
							$.ajax({
								url: "<%=request.getContextPath()%>/buyProduct.pay",
								type: "get",
								data: {
									bMemberNo: "<%=bloginUser.getBmemberNo()%>",
									productCode: productCode,
									approveNo: rsp.imp_uid
								},
								success: function(data) {
									console.log(data);
								},
								error: function(error) {
									console.log(error);
								},
							});
						}
						
					} else {
						var msg = '결제에 실패하였습니다.';
						msg += '에러내용 : ' + rsp.error_msg;
						alert(msg);
					}
				});
			}); 
		});
	</script>

</body>
</html>