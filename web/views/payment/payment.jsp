<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@
page import = "com.kh.hbr.member.model.vo.Business"
%>    
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>

	#whole {
		width: 1200px;
		margin: 0 auto;
	}
	
	#mainArea {
		width: 80%;
		float: left;
	}
	
	.default {
		font-weight: 900;
		height: 50px;
	}
	
	#navi {
		font-size: 15px;
		height: 80px;
		margin-left: 30px;
		line-height: 80px;
		border-bottom: 1px solid black;
		font-weight: bold;
	}
	
	#pageTitle {
		height: 80px;
		font-size: 30px;
		margin-left: 30px;
		line-height: 80px;
		border-bottom: 1px solid black;
	}
	
	#caution {
		margin-top: 20px;
		margin-left: 50px;
	}
	
	#caution li, table li {
	 	margin-left: 0px;
	}
	
	/* 금도끼 영역 CSS */
	
	#itemArea1 {
		margin-top : 30px;
		margin-left: 30px;
		border: 1px solid black;
		width: 940px;
		height: 350px;
	}
	
	#goldLabel {
		font-size: 30px;
		font-weight: bold;
		margin-top: 10px;
		margin-left: 20px;
		margin-bottom: 10px;
	}
	
	.goldDetail {
		width: 280px;
		height: 260px;
		margin: 0 auto;
		float: left;
		border: 1px solid black;
		text-align: left;
	}
	
	#goldDetail1 {
		margin-left: 25px;
		margin-right: 20px;
	}
	
	#goldInner {
		width: 250px;
		height: 200px;
		border: 5px solid yellow;
		margin : 0 auto;
	}
	
	#goldDetail2 {
		margin-right: 20px;
	}
	
	
	#goldImg {
		width: 160px;
		height: 100px;
		margin: 0 auto;
		margin-top: 20px;
	}
	
	.goldLabel {
		text-align:left;
		font-size: 20px;
		font-weight: bold;
		margin-left: 20px;
		margin-top: 5px;
		margin-bottom: 5px;
	}
	
	#goldLabel2 {
		font-size: 20px;
		font-weight: bold;
		margin-top: 10px;
		margin-left: 20px;
	}
	
	#goldLabel3 {
		margin-top: 5px;
		margin-left: 20px;
	}
	
	#desList {
		margin-top: 10px;
		margin-left: 27px;
	}
	
	#desList > ul {
		line-height: 30px;
	}
	
	#price{
		width: 200px;
		height: 150px;
		margin: 0 auto;
		margin-top: 10px;
	}
	
	#priceTable {
		width: 200px;
		height: 150px;
		text-align: center;
		border-collapse: collapse;
	}

	#priceTable th {
		background: lightgrey;
	}	
	
	#priceTable th, td {
		border: 1px solid black;
	}
	
	#goldBtn {
		background: #013252;
		color: white;
		border-style: none;
		width: 80px;
		height: 30px;
		font-weight: bold;
		margin-top: 20px;
		margin-left: 180px;
	}
	
	/* 은도끼 영역 CSS */
	
	#itemArea2 {
		margin-top : 30px;
		margin-left: 30px;
		border: 1px solid black;
		width: 940px;
		height: 350px;
	}
	
	#silverLabel {
		font-size: 30px;
		font-weight: bold;
		margin-top: 10px;
		margin-left: 20px;
		margin-bottom: 10px;
	}
	
	.silverDetail {
		width: 280px;
		height: 260px;
		margin: 0 auto;
		float: left;
		border: 1px solid black;
		text-align: left;
	}
	
	#silverDetail1 {
		margin-left: 20px;
		margin-right: 20px;
	}
	
	#silverInner {
		width: 250px;
		height: 160px;
		border: 3px solid yellow;
		margin: 0 auto;
		margin-top: 25px;
	}
	
	#silverDetail2 {
		margin-right: 20px;
	}
	
	#desListSil {
		margin-top: 10px;
		margin-left: 40px;
	}
	
	#desListSil > ul {
		line-height: 30px;
	}
	
	
	#silverImg {
		width: 120px;
		height: 80px;
		margin: 0 auto;
		margin-top: 5px;
	}
	
	.silverLabel {
		text-align:left;
		font-size: 20px;
		font-weight: bold;
		margin-left: 20px;
		margin-top: 5px;
		margin-bottom: 5px;
	}
	
	#silverLabel2 {
		font-size: 20px;
		margin-left: 20px;
	}
	
	#silverLabel3 {
		margin-top: 5px;
		margin-left: 20px;
	}
	
	#silverBtn {
		background: #013252;
		color: white;
		border-style: none;
		width: 80px;
		height: 30px;
		font-weight: bold;
		margin-top: 20px;
		margin-left: 180px;
	}
	
	/* 쇠도끼 영역 CSS */
	
	#itemArea3 {
		margin-top : 30px;
		margin-left: 30px;
		border: 1px solid black;
		width: 940px;
		height: 350px;
	}
	
	#steelLabel {
		font-size: 30px;
		font-weight: bold;
		margin-top: 10px;
		margin-left: 20px;
		margin-bottom: 10px;
	}
	
	.steelDetail {
		width: 280px;
		height: 260px;
		margin: 0 auto;
		float: left;
		border: 1px solid black;
		text-align: left;
	}
	
	#desListSteel {
		margin-top: 10px;
		margin-left: 40px;
	}
	
	#desListSteel > ul {
		line-height: 30px;
	}
	
	#steelDetail1 {
		margin-left: 20px;
		margin-right: 20px;
	}
	
	#steelInner {
		width: 230px;
		height: 150px;
		border: 1px solid black;
		margin: 0 auto;
		margin-top: 25px;
	}
	
	#steelDetail2 {
		margin-right: 20px;
	}
	
	.steelLabel {
		text-align:left;
		font-size: 20px;
		font-weight: bold;
		margin-left: 20px;
		margin-top: 5px;
		margin-bottom: 5px;
	}
	
	#steelLabel1 {
		font-size: 20px;
		margin-top: 20px;
		margin-left: 20px;
	}
	
	#steelLabel2 {
		margin-top: 10px;
		margin-left: 20px;
	}
	
	#steelLabel3 {
		margin-top: 5px;
		margin-left: 20px;
	}
	
	#steelBtn {
		background: #013252;
		color: white;
		border-style: none;
		width: 80px;
		height: 30px;
		font-weight: bold;
		margin-top: 20px;
		margin-left: 180px;
	}
	
	/* 반짝 영역 CSS */
	
	#itemArea4 {
		margin-top : 30px;
		margin-left: 30px;
		border: 1px solid black;
		width: 940px;
		height: 350px;
	}
	
	#twinkleLabel {
		font-size: 30px;
		font-weight: bold;
		margin-top: 10px;
		margin-left: 20px;
		margin-bottom: 10px;
	}
	
	.twinkleDetail {
		width: 280px;
		height: 260px;
		margin: 0 auto;
		float: left;
		border: 1px solid black;
		text-align: left;
	}
	
	#twinkleDetail1 {
		margin-left: 20px;
		margin-right: 20px;
	}
	
	#twinkleInner {
		width: 250px;
		height: 200px;
		border: 5px solid yellow;
		margin : 0 auto;
	}
	
	#twinkleDetail2 {
		margin-right: 20px;
	}
	
	#twinkleImg {
		width: 130px;
		height: 130px;
		margin: 0 auto;
	}
	
	.twinkleLabel {
		text-align:left;
		font-size: 20px;
		font-weight: bold;
		margin-left: 20px;
		margin-top: 5px;
		margin-bottom: 5px;
	}
	
	#twinkleLabel2 {
		font-size: 20px;
		font-weight: bold;
		margin-top: 10px;
		margin-left: 20px;
	}
	
	#twinkleLabel3 {
		margin-top: 5px;
		margin-left: 20px;
	}
	
	#twinkleBtn {
		background: #013252;
		color: white;
		border-style: none;
		width: 80px;
		height: 30px;
		font-weight: bold;
		margin-top: 20px;
		margin-left: 180px;
	}
		
	/* 이려서 영역 CSS */
	
	#itemArea5 {
		margin-top : 30px;
		margin-left: 30px;
		border: 1px solid black;
		width: 940px;
		height: 350px;
	}
	
	#resumeLabel {
		font-size: 30px;
		font-weight: bold;
		margin-top: 10px;
		margin-left: 20px;
		margin-bottom: 10px;
	}
	
	.resumeDetail {
		width: 280px;
		height: 260px;
		margin: 0 auto;
		float: left;
		border: 1px solid black;
		text-align: left;
	}
	
	#resumeDetail1 {
		margin-left: 20px;
		margin-right: 20px;
	}
	
	#resumeInner {
		width: 250px;
		height: 200px;
		margin : 0 auto;
	}
	
	#resumeDetail2 {
		margin-right: 20px;
	}
	
	#resumeImg {
		width: 120px;
		height: 130px;
		margin: 0 auto;
		margin-top: 20px;
	}
	
	.resumeLabel {
		text-align:left;
		font-size: 20px;
		font-weight: bold;
		margin-left: 20px;
		margin-top: 5px;
		margin-bottom: 5px;
	}
	
	#resumeBtn {
		background: #013252;
		color: white;
		border-style: none;
		width: 80px;
		height: 30px;
		font-weight: bold;
		margin-top: 20px;
		margin-left: 180px;
	}
	
	#desListResume {
		margin-top: 10px;
		margin-left: 40px;
	}
		
	#desListResume > ul {
		line-height: 30px;
	}
	
</style>
</head>
<body>
	<%@ include file="../guide/business_menubar.jsp" %>
	<%@ include file="payment_leftMenu.jsp" %>
	
	<script>
		$(function() {
			$("#menu2Lab").css("color", "#2FA599");
			$("#menu2Lab").removeAttr("onclick");
			$("#menu2").css("cursor", "default");
		});
	</script>

	<% 
		if(bloginUser == null) {
	%>
		<script>
		alert("기업회원으로 로그인 해주세요.");
		
		$(function(){
			location.href = "../common/E_LoginPage.jsp";
		});
		</script>
	<%		
		}
	%>
	
	<!-- 전체 영역 -->
	<div id="whole">
		
		<!-- 컨텐츠 영역 -->
		<div id="mainArea">
		
			<div id="navi" class="default">HOME > 상품 결제관리 > 상품소개/구매</div>
			<div id="pageTitle" class="default">상품소개/구매</div>
			<div id="caution">
				<ul>
					<li>모든 상품에는 구매후 사용기한이 있습니다. 사용기한내에 사용하지 않아 소멸된 상품은 환불이 불가합니다.</li>
					<li>구매후 사용한 상품에 대해서는 환불이 불가합니다.</li>
				</ul>
			</div>

			<!-- 금도끼 영역 -->
			<div id="itemArea1">
			
				<div id="goldLabel">금도끼 : 확실한 광고 효과 직접 경험해보세요!</div>
				
				<!-- 안에 3개의 div중 첫번째 -->
				<div class="goldDetail" id="goldDetail1">
					<div class="goldLabel">상품이미지</div>
					<div id="goldInner">
						<div id="goldImg">
							<img src="../../static/images/user/logo.png" width="160px" height="100px">
						</div>
						<div id="goldLabel2">haebolae Company</div>
						<div id="goldLabel3">프론트엔드 개발자 모집</div>
					</div>
				</div>

				
				<!-- 안에 3개의 div중 두번째 -->
				<div class="goldDetail" id="goldDetail2">
					<div class="goldLabel">상품설명</div>
					<div id="desList">
						<ul>
							<li>메인 페이지 최상단에 노출</li>
							<li>한눈에 들어오는 최적화된 사이즈</li>
							<li>마우스 접근 시 확대</li>
							<li>마우스 접근 시 테두리 강조</li>
							<li>텍스트 최상 크기</li>
							<li>강조된 로고 사이즈</li>
						</ul>
					</div>
				</div>
								
				<!-- 안에 3개의 div중 세번째 -->
				<div class="goldDetail" id="goldDetail3">
					<div class="goldLabel">상품가격</div>
					<div id="price">
						<table id="priceTable">
							<tr>
								<th>개수</th>
								<th>가격</th>
								<th>기한</th>
							</tr>
							<tr>
								<td>1개</td>
								<td>4만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>5개</td>
								<td>20만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>10개</td>
								<td>38만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>30개</td>
								<td>108만원</td>
								<td>50일</td>
							</tr>
						</table>
					</div>
					<button id="goldBtn">구매하기</button>
				</div>
			</div>
			
			<!-- 은도끼 영역 -->
			<div id="itemArea2">
			
				<div id="silverLabel">은도끼 : 가장 핫한 광고는 바로 나!</div>
				
				<!-- 안에 3개의 div중 첫번째 -->
				<div class="silverDetail" id="silverDetail1">
					<div class="silverLabel" id="silverLab">상품이미지</div>
					<div id="silverInner">
						<div id="silverImg">
							<img src="../../static/images/payment/mcdonald.jpg" width="120px" height="80px">
						</div>
						<div id="silverLabel2">Mcdonald's 맥도날드</div>
						<div id="silverLabel3">햄버거 조리 크루 모집</div>
					</div>
				</div>

				
				<!-- 안에 3개의 div중 두번째 -->
				<div class="silverDetail" id="silverDetail2">
					<div class="silverLabel">상품설명</div>
					<div id="desListSil">
						<ul>
							<li>메인 페이지 중단에 노출</li>
							<li>뚜렷한 로고</li>
							<li>마우스 접근 시 테두리 강조</li>
							<li>알아보기 쉬운 텍스트</li>
							<li>광고효과 높은 최적의 사이즈</li>
						</ul>
					</div>
				</div>
								
				<!-- 안에 3개의 div중 세번째 -->
				<div class="silverDetail" id="silverDetail3">
					<div class="silverLabel">상품가격</div>
					<div id="price">
						<table id="priceTable">
							<tr>
								<th>개수</th>
								<th>가격</th>
								<th>기한</th>
							</tr>
							<tr>
								<td>1개</td>
								<td>3만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>5개</td>
								<td>15만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>10개</td>
								<td>27.5만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>30개</td>
								<td>81만원</td>
								<td>50일</td>
							</tr>
						</table>
					</div>
					<button id="silverBtn" onclick="location.href='paymentSilver.jsp'">구매하기</button>
				</div>
			</div>
			
			<!-- 쇠도끼 영역 -->
			<div id="itemArea3">
			
				<div id="steelLabel">쇠도끼 : 알짜배기 광고는 바로 나!</div>
				
				<!-- 안에 3개의 div중 첫번째 -->
				<div class="steelDetail" id="steelDetail1">
					<div class="steelLabel" id="steelLab">상품이미지</div>
					<div id="steelInner">
						<div id="steelLabel1">(주)경비나라</div>
						<div id="steelLabel2">역삼역 KH정보교육원 <br>경비채용</div>
						<div id="steelLabel3">서울시 강남구</div>
					</div>
				</div>

				
				<!-- 안에 3개의 div중 두번째 -->
				<div class="steelDetail" id="steelDetail2">
					<div class="steelLabel">상품설명</div>
					<div id="desListSteel">
						<ul>
							<li>메인페이지 하단에 노출</li>
							<li>저렴한 가격 높은 만족도</li>
							<li>또렷한 기업명</li>
							<li>필요한 채용정보만 쏙쏙!</li>
							<li>로고 없는 은도끼!</li>
						</ul>
					</div>
				</div>
								
				<!-- 안에 3개의 div중 세번째 -->
				<div class="steelDetail" id="steelDetail3">
					<div class="steelLabel">상품가격</div>
					<div id="price">
						<table id="priceTable">
							<tr>
								<th>개수</th>
								<th>가격</th>
								<th>기한</th>
							</tr>
							<tr>
								<td>1개</td>
								<td>2만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>5개</td>
								<td>10만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>10개</td>
								<td>19만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>30개</td>
								<td>54만원</td>
								<td>50일</td>
							</tr>
						</table>
					</div>
					<button id="steelBtn" onclick="location.href='paymentSteel.jsp'">구매하기</button>
				</div>
			</div>
			
			<!-- 반짝 영역 -->
			<div id="itemArea4">
			
				<div id="twinkleLabel">반짝 : 일자리를 찾는 모든인재들에게 기업을 홍보하세요!</div>
				
				<!-- 안에 3개의 div중 첫번째 -->
				<div class="twinkleDetail" id="twinkleDetail1">
					<div class="twinkleLabel">상품이미지</div>
					<div id="twinkleInner">
						<div id="twinkleImg">
							<img src="../../static/images/payment/samsung.png" width="120px" height="130px">
						</div>
						<div id="twinkleLabel2">삼성인력정보</div>
						<div id="twinkleLabel3">총무/경영관리원 모집</div>
					</div>
				</div>

				
				<!-- 안에 3개의 div중 두번째 -->
				<div class="twinkleDetail" id="twinkleDetail2">
					<div class="twinkleLabel">상품설명</div>
					<div id="desList">
						<ul>
							<li>채용 페이지 상단에 노출</li>
							<li>한눈에 들어오는 최적화된 사이즈</li>
							<li>마우스 접근 시 확대</li>
							<li>마우스 접근 시 테두리 강조</li>
							<li>텍스트 최상 크기</li>
							<li>강조된 로고 사이즈</li>
						</ul>
					</div>
				</div>
								
				<!-- 안에 3개의 div중 세번째 -->
				<div class="twinkleDetail" id="twinkleDetail3">
					<div class="twinkleLabel">상품가격</div>
					<div id="price">
						<table id="priceTable">
							<tr>
								<th>개수</th>
								<th>가격</th>
								<th>기한</th>
							</tr>
							<tr>
								<td>1개</td>
								<td>1만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>5개</td>
								<td>5만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>10개</td>
								<td>9.5만원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>30개</td>
								<td>27만원</td>
								<td>50일</td>
							</tr>
						</table>
					</div>
					<button id="twinkleBtn" onclick="location.href='paymentTwinkle.jsp'">구매하기</button>
				</div>
			</div>
			
			<!-- 이력서 영역 -->
			<div id="itemArea5">
			
				<div id="resumeLabel">이력서열람권 : 우리기업에 맞는 최적인재! 빠르게 스카웃하자!</div>
				
				<!-- 안에 3개의 div중 첫번째 -->
				<div class="resumeDetail" id="resumeDetail1">
					<div class="resumeLabel">상품이미지</div>
					<div id="resumeInner">
						<div id="resumeImg">
							<img src="../../static/images/payment/resume.png" width="120px" height="150px">
						</div>
					</div>
				</div>

				
				<!-- 안에 3개의 div중 두번째 -->
				<div class="resumeDetail" id="resumeDetail2">
					<div class="resumeLabel">상품설명</div>
					<div id="desListResume">
						<ul>
							<li>인재검색시 연락처 확인 가능</li>
							<li>맞춤인재에게 면접제의 가능</li>
							<li>하나의 상품으로 인재열람과<br>
								면접제의를 동시에!
							</li>
						</ul>
					</div>
				</div>
								
				<!-- 안에 3개의 div중 세번째 -->
				<div class="resumeDetail" id="resumeDetail3">
					<div class="resumeLabel">상품가격</div>
					<div id="price">
						<table id="priceTable">
							<tr>
								<th>개수</th>
								<th>가격</th>
								<th>기한</th>
							</tr>
							<tr>
								<td>10개</td>
								<td>1000원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>30개</td>
								<td>3000원</td>
								<td>30일</td>
							</tr>
							<tr>
								<td>50개</td>
								<td>5000원</td>
								<td>30일</td>
							</tr>
						</table>
					</div>
					<button id="resumeBtn" onclick="location.href='paymentResume.jsp'">구매하기</button>
				</div>
			</div>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
					
		</div>

		
	</div>
	<script>
		$(function(){
			$("#goldBtn").click(function(){
				location.href = "<%= request.getContextPath() %>/views/payment/paymentGold.jsp";
			});
		});
	</script>
	
</body>
</html>