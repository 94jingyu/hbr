<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%

%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script	src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
/* td {
    border: 1px solid #444444;
 } */

* {
   text-align:left;
   margin: 0;  
   padding: 0;
}

button {
   cursor:pointer;
}

/* body {background-color: #fffde7;} */
#wrap {
   width: 1200px;
   margin: 0 auto;
}

/* 헤더 영역 */
.logo {
   float: left;
   width: 22%;
   height: 130px;
}

.toplogin {
   width: 100%;
   height: 50px;
   padding: 1%;
} 

}

/* 상단메뉴바 */
.topMenu {
   /* display: inline-block; */
   width: 100%;
   height: 50px;
   /* background: #013252; */
   line-height: 30px; /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
   vertical-align: middle; /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
   text-align: center; /* 글씨 정렬을 가운데로 설정 */
   font-weight: bolder;
   font-size: 20px;
   color: #013252;
}

.topMenu>div {
   float: right;
}

.topMenu>ul {
   float: left;
   margin: 0;
   padding: 0;
   list-style: none;
}

.topMenu>ul>li {
   display: inline-block;
   padding: 0;
}

.topMenu a {
   display: block;
   text-decoration: none;
   padding: 10px 20px;
   color: black; 
}

.topMenu a:hover {
   background: #013252;
   color: white;
}

/* 하이퍼텍스트 효과 */
a {
   text-decoration: none;
} /* 하이퍼텍스트 밑줄 효과 없애기 */

/* 광고 영역 */
.ads {
   width: 100%;
   height: 500px;
   clear: both;
}

/* 공고조회 및 로그인 영역 */
.b2_cotainer {
   width: 100%;
   height: 260px;
   background-color: #F0F0F0;
   text-align: center;
}

#selectJob {
   float: left;
   width: 70%;
   height: 250px;
   background-color: #039be5;
   display: inline-block;
}

#login {
   width: 30%;
   height: 250px;
   background-color: #0288d1;
   display: inline-block;
}

.fsearch_wp {
   width: 100%;
   border: 1px solid #e1e1e1;
   border-bottom: none;
   font-size: 14px;
   box-sizing: border-box;
}

/* 일반채용리스트 */
.normalJobList {
   width: 88%;
   font-size: 20px;
   font-weight: bold;
}

#normaljobList {
   font-size: 16px;
   font-weight: bold;
   border: none;
   border-bottom: 3px;
   border-bottom-color: black;
   background-color: lightGray;
   
}

/* footer 영역 */
footer {
   width: 100%;
   height: 200px;
   background-color: #ffb300;
}

/* 버튼 효과 */
.btns {
   align: center;
}

#loginBtn, #memberJoinBtn, #logoutBtn, #changeInfo, #adminBtn {
   display: inline-block;
   text-align: center;
   background: orangered;
   color: white;
   height: 25px;
   width: 100px;
   border-radius: 5px;
}

#memberJoinBtn, #logoutBtn {
   background: yellowgreen;
}

#loginBtn:hover, #changeInfo:hover, #logoutBtn:hover, #memberJoinBtn:hover,
   #adminBtn:hover {
   cursor: pointer;
}

/* nav */
.nav {
   width: 15%;
   height: 300px;
   border: solid 1px lightgray;
   margin-left: 5px;
}

/* 박다영 스타일 */
/* left 메뉴 */
   .leftblock {
      display: block;
   }
   
   #left2, #left3, #left4 {
      margin-top:10px;
   }
   
   #left1 {
      width: 180px;
      height:900px;
      margin-top:10px;
      align: left;
   }
   
   .myPage {
      margin-left: 10px;
   }
   
   #left2 {
      height:290px;
   }
   
   #left3 {
      height:260px;
   }
   
   #left4 {
      height:250px;
   }
   
   /* left메뉴 가로선 */
   hr {
      width:90%;
      color:lightgray;
      size:2px;
      margin-left: 5%;
   }
   
   ul {
      list-style:none;
   }

.leftMenu{float: left; width: 20%; height: 580px; margin-left: 30px; margin-top: 10px;}
   
   /* 내용 부분 */
   /* section{width: 75%; height: 1500px; background-color: #ffca28;} */
   
   .leftMenu>ul {       
       margin: 10;
       padding: 0;
       list-style: none;
    }
    .leftMenu a {
       display: block;
       text-decoration: none;
       padding: 10px 20px;
       color: black;
     }
.left {
      border:2px solid rgb(192, 192, 192);
   }
#applyList{
text-align:center;}
/* .inquiryType {
   display: inline-block;
} */

.myInquiry {
   width: 80%;
   height: 60px;
   margin-left: 240px;
   background-color: #013252;
   font-size: 20px;
   color: white;
   line-height: 60px; 
}


#inquiry {
   display: inline-block;
}

#period {
   display: inline-block;
   width: 50px;
   height: 25px;
   border: 1px solid gray;
   background-color: lightgray;
   /* line-height: 60px; */
}

/* * #inquiryTable {
   
   border-collapse: collapse;
   height: px;
   
} */

#qTable , #qTable{
   width: 100px;
   height: 40px;
   background-color: #013252;
   font-weight: bolder;
   font-size: 14px;
   color: white;
   border: none;
}

#applyTable {
   width: 94%;
   text-align: center;
   border: 1px solid lightgray;
   border-collapse: collapse;
}
#applyTable1{
   width: 100%;
   text-align: center;
   border: 1px solid lightgray;
   border-collapse: collapse;
}

.iqTr{
   background-color: lightgray;
   font-weight: bold;
}

.iqTr2{
   border-bottom: 1px solid lightgray;
}


/* 모달관련 */
/* Modal background */
.searchModal {
   display: none; /* Hidden by default */
   position: fixed; /* Stay in place */
   z-index: 10; /* Sit on top */
   left: 0;
   top: 0;
   width: 100%; /* Full width */
   height: 100%; /* Full height */
   overflow: auto; /* Enable scroll if needed */
   background-color: rgb(0,0,0); /* Fallback color */
   background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
   border: 10px solid black;
}

/* Modal content */
.search-modal-content {
   background-color: #fefefe;
   margin: 15% auto; /* 15% from the top and centered */
   padding: 20px;
   border: 1px solid #888;
   width: 75%; /* Could be more or less, depending on screen size */
   height: 380px;
}

</style>


<script>
   $(document).ready(function() {
      $('.slider').bxSlider();
   });
</script>
</head>
<body>
   <div id="wrap">
      <!-- 로고 영역 -->
      <!-- <div class="logo">
         <img alt="logo.png" src="../../static/images/user/logo.png"
            width="250px" height="100%"
            style="margin-left: auto; margin-right: auto; display: block">
      </div>

      상단 로그정보 영역
      <div class="toplogin">
         <div class="btns" align="right">
            <div id="loginBtn" onclick="login();">로그인</div>
            <div id="memberJoinBtn" onclick="memberJoin();">회원가입</div>
         </div>
      </div>

      상단 메뉴바
      <nav class="topMenu" style="color:#013252">
         <ul>
            <li><a href="#">채용정보</a></li>
            <li><a href="#">인재정보</a></li>
            <li><a href="#">열린마당</a></li>
            <li><a href="#">취업박사</a></li>
            <li><a href="#">고객센터</a></li>
         </ul>
         <div>
            <a href="#">이력서등록</a>
         </div>
         <div>
            <a href="#" style="color:white; background-color:#013252">마이페이지</a>
         </div>
      </nav><br>
      <hr style="border: solid 2px #013252; width:100%; margin:0 auto"><br>
      
      <aside class="leftMenu">
              <div class="left leftblock" id="left1"><br>&nbsp;
              <label class="myPage" style="font-size:20px;"><b>마이페이지</b></label><br><br>
              <hr>
              <br>
                 <ul>
                 <li>
                       <a href="#" style="font-size:18px;"><b>이력서관리</b></a>
                     <a href="#" style="font-size:15px;">이력서 등록</a>
                     <a href="#" style="font-size:15px;">이력서 현황</a>
                  </li><br>
                  <hr><br>
                  <li>
                       <a href="#" style="font-size:18px;"><b>입사지원 관리</b></a>
                     <a href="#" style="font-size:15px;">입사지원목록</a>
                     <a href="#" style="font-size:15px;">내 이력서 열람기업</a>
                     <a href="#" style="font-size:15px; font-weight: bold; color: #2fa599">이력서 열람 제한</a>
                  </li><br>
                <hr><br>
                <li>
                   <a href="#" style="font-size:18px;"><b>스크랩보금자리</b></a>
                </li><br>
                <hr><br>
                <li>
                   <a href="#" style="font-size:18px;"><b>최근 본 보금자리</b></a>
                </li><br>
                <hr><br>
                <li><a href="#" style="font-size:18px;"><b>회원정보 관리</b></a>
                    <a href="#" style="font-size:15px;">회원정보수정</a>
                    <a href="#" style="font-size:15px;">회원탈퇴</a>
                </li><br>
                <hr><br>
                <li><a href="#" style="font-size:18px;"><b>나의 문의내역</b></a>
                </li>
              </ul>
              </div>
         </aside><br> -->
         <%@ include file="../common/E_user_menubar.jsp" %> 
         <hr style="border: solid 2px #013252; width:100%; margin:0 auto"><br>
      
         <%@ include file="../user_JinHyeok/leftBar.jsp" %><br>
         
     <%--  <form class="resume_form" action="<%= request.getContextPath() %>/limit.res"  name="" method="post" > --%>
         <div class="myInquiry" style="font-weight: bold">입사지원 관리</div><br>
         <table>
            <tr>
               <td style="font-weight: bold">열람 제한 목록 |</td>
               <td style="font-weight: bold">총 0건</td>
            </tr>
         </table><br><br>
            
         <table id="inquiryTable">
            <tr>
               <td colspan="2" width="85px" height="60px">
               <img src="../../static/images/user/lock.png" width="80px" height="80px"></td>
               <td><p style="font-size:14px">
                     • <b>내 이력서의 열람을 원하지 않는 기업(회원)</b>이 있을 경우,<열람 제한 신청>버튼을 클릭해주세요<br>
                     • 열람 제한 설정된 기업은 이력서를 열람할 수 없습니다.<br>
                     • 열람 제한 설정 후, 다시 해지를 원하실 경우에는 <b><열람 제한 해제></b>버튼을 클릭해주세요.
                  </p>
               </td>
            </tr>
         </table><br><br>
         
         
         <table width="700px" height="80px" style="border:1px solid lightgray;">
            <tr>
            <td width="30px"></td>
            <td style="font-size:16px; font-weight:bold;">검색조건</td>
            <td width="40px"></td>
               <td>
                  <label style="font-size:14px" >기업명</label>&nbsp;&nbsp;
                  <input type="text" placeholder="검색할 기업명 입력" style="width:340px; height:25px">
               </td>
               <td>
                  <button id="qTable" style="font-weight: bold; text-align:center;" >검색</button>
               </td>
            </tr> 
         </table><br><br><br>
         
         
            
         <!-- 모달시작 -->
         <div id="modal" class="searchModal" style="margin-left:240px;">
            <div class="search-modal-content">
                  <table style="width:100%">
                     <tr>
                     <td colspan="3" style="font-weight: bold"><h3>이력서 열람제한 설정</h3></td>
                  </tr>
                  </table>
                  <br>
                  <table>
                      <tr>
                     <td width="85px" height="60px">
                     <img src="../../static/images/user/lock.png" width="80px" height="80px"></td>
                     <td colspan="4"><p style="font-size:14px">
                           • <b>내 이력서의 열람을 원하지 않는 기업(회원)</b>이 있을 경우,<열람 제한 신청>버튼을 클릭해주세요<br>
                        </p>
                     </td>
                  </tr>
                   
                      <tr>
                  
                  <td style="font-size:16px; font-weight:bold; height:80px; ">검색조건</td>
                  <td width="40px"></td>
                     <td colspan="1">
                        <label style="font-size:14px" >기업명</label>&nbsp;&nbsp;
                        <input type="text" placeholder="검색할 기업명 입력" style="width:340px; height:25px" name="company_name">&nbsp;&nbsp;&nbsp;&nbsp;
                        <button id="qTable1" style="font-weight: bold; text-align:center; width:80px; height:30px; background:#013252; color:white;" >검색</button>
                     </td>
                     <td>
                        
                     </td>
                  </tr>
                  </table> <br>
                  
                  <table style="width:100%;">
                  <tr class="iqTr">
                     <td id="applyList" width="8%" height="40px">선택</td>
                     <td id="applyList" width="14%">등록일</td>
                     <td id="applyList" width="14%">기업명</td>
                     <td id="applyList" width="45%">채용중인 공고</td>
                     <td id="applyList" width="25%">내 이력서 열람여부</td>
                  </tr>
                  
                  
                  <tr class="iqTr2" height="70">
                     <td id="applyList"><input type="checkbox"></td>
                     <td id="applyList">20/01/27</td>
                     <td id="applyList"></td>
                     <td id="applyList"></td>
                     <td id="applyList"></td>
                  </tr>
                  
                  
               </table>
                  <table style="width:100%;">
                     <tr>
                        <td width="189px"></td>
                        <td width="189px"></td>
                        <td width="189px"></td>
                        <td width="40px">
                        
                        <td width="80px"><button type="button" id="limit_view" style="text-align:center; width:50px; height:30px; background:#013252; color:white; float:right;">적용</button>
                                    <button type="button" id="jdj_modal_close5" style="width:50px; height:30px; background:gray; float:right; color:white; text-align:center;">취소</button></td> 
                     </tr>
                  </table>
               </div>
         </div><!-- 우대  모달 div 끝  -->
         
         
         <div style="border:1px solid lightgay; margin-left:270px" align="center">
         <table id="applyTable">
            <tr class="iqTr">
               <td id="applyList" width="8%" height="40px">선택</td>
               <td id="applyList" width="14%">등록일</td>
               <td id="applyList" width="14%">기업명</td>
               <td id="applyList" width="45%">채용중인 공고</td>
               <td id="applyList" width="25%">내 이력서 열람여부</td>
            </tr>
            <tr class="iqTr2" height="70">
               <td id="applyList"><input type="checkbox"></td>
               <td id="applyList">20/01/27</td>
               <td id="applyList">(주)덕승재</td>
               <td id="applyList">한정식 남녀 서빙 245만 주5일</td>
               <td id="applyList">미열람</td>
               
            </tr>
            <tr class="iqTr2" height="70">
               <td id="applyList"><input type="checkbox"></td>
               <td id="applyList">20/02/02</td>
               <td id="applyList">태원상사</td>
               <td id="applyList">힘들어요. 체력 좋으신 분만</td>
               <td id="applyList">열람</td>
               
            </tr>
            <tr height="70">
               <td id="applyList"><input type="checkbox"></td>
               <td id="applyList">20/02/14</td>
               <td id="applyList">KH정뵤교육</td>
               <td id="applyList">환경미화 해주실 분 구합니다.</td>
               <td id="applyList">열람</td>
               
            </tr>
         </table><br>
         </div><br>
         <!-- </form> -->
         <!-- 페이지 버튼 담을 영역 -->
     <%--  <div class="div-btnArea">
      
      <!-- 처음으로 -->
      <button class="btn-page" onclick="location.href='<%=request.getContextPath()%>/selectPaymentList?currentPage=1'"><<</button>
      <!-- 이전으로 -->
      <%if(pageInfo.getCurrentPage() == 1){%>
         <button disabled class="btn-page"><</button>
      <%}else{%>
         <button class="btn-page" onclick="location.href='<%=request.getContextPath()%>/selectPaymentList?currentPage=<%=pageInfo.getCurrentPage() - 5%>'"><</button>
      <%}%>
      
      
      
      <!-- 페이지 버튼들 -->
      <%for(int i = pageInfo.getStartPage(); i <= pageInfo.getMaxPage(); i++ ){ %>
      
      <%if(pageInfo.getCurrentPage() == i){ %>
      <button class="btn-page" style="background:gray;" onclick="location.href='<%=request.getContextPath()%>/selectPaymentList?currentPage=<%=i%>'"><%=i%></button>
      <%}else{ %>
      <button class="btn-page" onclick="location.href='<%=request.getContextPath()%>/selectPaymentList?currentPage=<%=i%>'"><big><b><%=i%></b></big></button>
      <%} %>
      
      
      <%} %>
      
      <!-- //페이지 버튼들 -->
       
       
       
      
      <!-- 다음으로 -->
      <%if(pageInfo.getCurrentPage() == pageInfo.getMaxPage()){%>
         <button disabled class="btn-page">></button>
      <%}else{%>
         <button class="btn-page" onclick="location.href='<%=request.getContextPath()%>/selectPaymentList?currentPage=<%=pageInfo.getCurrentPage() + 5%>'">></button>
      <%}%>
      <!-- 끝으로 -->
      <button class="btn-page" onclick="location.href='<%=request.getContextPath()%>/selectPaymentList?currentPage=<%=pageInfo.getMaxPage()%>'">>></button>
      
      
      </div>
      <!-- //페이징 버튼 담을 영역 --> --%>
      
         
      
            <br><br>
            <div align="right" style="text-align:right; margin-right: 55px">
               <button style="width:160px; height:40px; text-align:center; background-color: #013252; color:white; font-size:14px">열람 제한 신청</button>
               <button style="width:160px; height:40px; text-align:center;background-color: #013252; color:white; font-size:14px">열람 제한 해제</button>
            </div>
            <br><br><br><br><br><br><br><br><br><br><br><br><br>
            
            
            
         <%-- <div class="pagingArea" align="center">
            <button onclick="location.href='<%= request.getContextPath()%>/selectList.bo?currentPage=1'"><<</button>
            
            <% if (currentPage <= 1) { %>
            <button disabled><</button>
            <% } else { %>
             <button onclick="location.href='<%= request.getContextPath()%>/selectList.bo?currentPage=<%= currentPage - 1%>'"><</button>
             <% } %>
             
             
             <% for (int p = startPage; p <= endPage; p++) {
                   if(p == currentPage) {
             %>
                   <button disabled><%= p %></button>
             <%
                } else {
             %>
                   <button onclick="location.href='<%= request.getContextPath() %>/selectList.bo?currentPage=<%= p %>'"><%= p %></button>
             <%
                }
            }
             %>
             
             
         <% if (currentPage >= maxPage) { %>
         <button disabled>></button>
         <% } else { %>
         <button onclick="location.href='<%= request.getContextPath() %>/selectList.bo?currentPage=<%= currentPage + 1 %>'">></button>
         <% } %>
             
             
             <button onclick="location.href='<%= request.getContextPath()%>/selectList.bo?currentPage=<%= maxPage %>'">>></button>
         </div> --%>
      
   
      <!-- footer영역 -->
      <!-- <footer>Footer</footer> -->
   </div>
   
   <script>
      
   
      $("#qTable").click(function(){
         $(".searchModal").attr("style", "display:block");
      });
      
      $("#jdj_modal_close5").click(function(){
         $(".searchModal").attr("style", "display:none");
      });
       
      <%-- $("#limit_view").click(function(){
          $(".resume_form").attr("action" , "<%= request.getContextPath() %>/limit.list");
          $(".resume_form").submit();
       }); --%>
      
   </script>
   
   
</body>
</html>










