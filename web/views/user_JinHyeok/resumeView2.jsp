<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>  
 	<style>
 	/*css reset*/
	body,h1,h2,h3,h4,h5,h6,p,div,header,main,footer,section,article,nav,ul,li,form,fieldset,legend,label,p,address,table,dl,dt,dd,input,select,textarea,button,figure,figcaption,table,th,td,tr{margin:0;padding:0;}
        
ul,li{list-style:none}
a{text-decoration:none;color:#222}
a:hover{color:#2698cb}
address{font-style:normal}
form,input,button{vertical-align:middle} 
button{cursor:pointer;border:none}
img,fieldset,select{border:none}
input{border:none;outline:none}
.tableBack{background-color:#ECF0F7;font-weight:bold;}
.wrap {width: 1200px; margin: 0 auto;}
/* 헤더 영역 */
* { 
	margin: 0;
	padding: 0;
}
/* body {background-color: #fffde7;} */
#wrap {
	width: 1200px;
	margin: 0 auto;
}

/* 헤더 영역 */
.logo {
	float: left;
	width: 22%;
	height: 130px;
}

.toplogin {
	width: 100%;
	height: 50px;
	padding: 1%;
}

/* 상단메뉴바 */
.topMenu {
	/* display: inline-block; */
	width: 100%;
	height: 50px;
	/* background: #013252; */
	line-height: 30px; /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	vertical-align: middle; /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	text-align: center; /* 글씨 정렬을 가운데로 설정 */
	font-weight: bolder;
	font-size: 20px;
	color: #013252;
}

.topMenu>div {
	float: right;
}

.topMenu>ul {
	float: left;
	margin: 0;
	padding: 0;
	list-style: none;
}

.topMenu>ul>li {
	display: inline-block;
	padding: 0;
}

.topMenu a {
	display: block;
	text-decoration: none;
	padding: 10px 20px;
	color: black;
}

.topMenu a:hover {
	background: #013252;
	color: white;
}

/*main*/
#main{width:1100px;margin:0 auto}
#main legend{padding:20px 0 20px 0;font-size:22px;color:#00548b;font-weight:bold}
.resume_title{margin-bottom:20px}
.resume_title label{font-weight:bold;font-size:15px;display:block;margin-bottom:10px}
.resume_title input{border:1px solid #eee;padding:10px;}

/*기본정보*/
.basic_info{margin-bottom:20px}
.basic_info .basic_info_title{font-weight:bold;font-size:15px;display:block;margin-bottom:10px}
.basic_info .basic_info_contents{background:#ecf0f7;padding:4%;position: relative}
.basic_info .basic_info_contents p{padding:10px}
.basic_info .basic_info_contents .uphoto{padding:0}
.basic_info .basic_info_contents label{font-size:15px;margin-bottom:10px;}
.basic_info .basic_info_contents input{padding:15px;}

.uname label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:50px}
.uname input{background:#e8e9ee}
.b_day label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px}
.email label{margin-right:60px}
.phone_num label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:35px}
.phone_num input{margin-right:10px;background:#e8e9ee}
.phone_num a{text-decoration:underline}
.uaddress label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:50px}
.uaddress .uaddress_01{margin-bottom:10px}
.uaddress .remain_addy{display:block;margin-left:110px}
.uphoto{border:5px solid #fff;position:absolute;left:900px;top:35px;}
.uphoto label{cursor:pointer;}
.uphoto input{position:absolute;left:-9999px;top:-9999px;}

/*학력사항*/
.edu_bg_contents{background:#ecf0f7;}
.edu_bg_title_01{font-weight:bold;font-size:15px;display:inline-block;margin-bottom:10px}
.edu_bg_title_02{font-size:11px}
.highest_edu{border-bottom:2px solid #acacac;padding:4% 2%;}
.highest_edu ul{overflow:hidden;width:99%;margin:0 auto}
.highest_edu li{width:18%;float:left;height:40px;border:1px solid #ddd;background:#fff;margin:0 3%;text-align:center;line-height:2.6;font-size:15px;font-weight:bold}

/*고등학교 정보 입력*/
.high_sch_info{clear:both;padding:4%;border-bottom:2px solid #acacac}
.high_sch_info_title{font-weight:bold;font-size:15px;}
.high_sch_info label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.high_sch_info input{padding:15px;color:#8b7575}
.high_sch_info select{padding:15px;height:50px;color:#8b7575}
.high_sch_info p{margin-bottom:20px}
.high_sch_info .high_name_title{margin-right:35px}
.high_major{width:372px;color:#8b7575;}
.high_sch_info .high_select{vertical-align:bottom}

/*대학·대학원 정보 입력*/
.uni_info{padding:4%;margin-bottom:20px}
.uni_info label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.uni_info input{padding:15px;color:#8b7575}
.uni_info select{padding:15px;color:#8b7575;width:372px;}
.uni_info p{margin-bottom:20px}
.uni_info_title{font-weight:bold;font-size:15px;margin-bottom:10px}
.uni_info .sel_uni{margin-right:50px}
.uni_info .uni_name{margin-right:35px}
.uni_info .uni_area,.uni_info .uni_major{margin-right:50px}
.uni_info .uni_major_select{height:45px;vertical-align:bottom;padding:14px;}
.uni_info .uni_major_input{display:block;margin:10px 0 0 110px}
.uni_info .uni_select{vertical-align:bottom;width:auto;height:50px;}
.uni_info .uni_credit_label{background:none;padding:0;margin-right:75px}
.uni_info .credit_limit_select{width:168px;height:45px;vertical-align:bottom}

/*경력사항*/
.Career_details{margin-bottom:20px}
.Career_details_title{font-weight:bold;font-size:15px;margin-bottom:10px}
.Career_details_contents{background:#ecf0f7;}
.Career_details_inpo{padding:4%}
.Career_details label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.Career_details input{padding:15px;color:#8b7575}
.Career_details select{padding:15px;color:#8b7575;width:372px;}
.Career_details p{margin-bottom:20px;}
.Career_position,.Career_type{position:relative}
.Career_details_inpo a{text-decoration:underline;position:absolute;left:430px;top:11px;font-weight:bold}
.sel_career{border-bottom:2px solid #acacac;padding:4% 2%;}
.sel_career ul{width:60%;margin:0 auto;overflow:hidden;}
.sel_career li{width:40%;margin:3%;background:#fff;float:left;text-align:center;height:40px;border:1px solid #ddd;line-height:2.6;font-size:15px;font-weight:bold}
.Career_details .cor_name{margin-right:35px}
.Career_details .term_office_input{width:145px;}
.Career_details .job_type{margin-right:50px}
.Career_details .sel_position{margin-right:15px}

/*자기소개서*/
.self_intro{margin-bottom:20px}
.self_intro_title{margin-bottom:10px}
.self_intro_title_01{font-weight:bold;font-size:15px;margin-bottom:10px}
.self_intro_title_02{font-size:11px}
.self_intro_table{border:1px solid #eee;margin-bottom:10px}
.self_intro_table caption,.self_intro_table .re_title,.self_intro_table .re_contents{position:absolute;left:-9999px;top:-9999px}
.re_title_area{border-bottom:1px solid #eee;padding:13px;font-size:15px}
.re_contents_area{border:none;padding:10px;}
.letter_count{font-weight:bold;font-size:16px;}
.letter_count .letter_red{color:#ff0000}

/*희망조건 선택*/
.de_condition{margin-bottom:20px}
.de_condition_title{font-weight:bold;font-size:15px;margin-bottom:10px}
.de_condition_contents{background:#ecf0f7;padding:4%}
.de_condition_contents label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:45px;font-size:15px}
.de_condition_contents .int_job{padding-right:80px;background:url(../images/essential_3_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.de_condition_contents select{padding:15px;color:#8b7575;margin-right:30px;width:200px;}
.de_condition_contents p{margin-bottom:20px;}

/*이력서 공개여부*/
.resume_public{margin-bottom:20px}
.resume_public_title{font-weight:bold;font-size:15px;margin-bottom:10px}
.resume_public_contents{background:#ecf0f7;padding:4%}
.resume_public_contents label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.resume_public_radio{margin-left:15px}
.resume_public_contents p{font-size:15px;font-weight:bold;margin:5px 0 0 160px}

.resume_check{padding:10px}
.resume_check_ch{width:410px;margin:0 auto;margin-bottom:20px}
.resume_check_ch span{font-weight:bold}
.resume_date{width:280px;margin:0 auto}
.resume_date_date,.resume_date_user{font-weight:bold}

/*버튼*/
.sub_btn{}
#backButton{background: #013252;
		color: white;
		border-style: none;
		width: 180px;
		height: 60px;
		font-weight: bold;
		margin-top: 20px;
		margin-left: 450px;
		font-size:18px;
		}
		#phone_view{ 
		background: #013252;
		color: white;
		border-style: none;
		width: 90px;
		height: 40px;
		font-weight: bold;
		
		font-size:14px;
		}

		#interview{
		background: #013252;
		color: white;
		border-style: none;
		width: 90px;
		height: 40px;
		font-weight: bold;
		
		font-size:14px;
		}
		#scrap{ 
		background: #013252;
		color: white;
		border-style: none;
		width: 90px;
		height: 40px;
		font-weight: bold;
		
		font-size:14px;
		}
}

	
    </style>
    
</head>
<body>
    <div id="wrap">
		<!-- 로고 영역 -->
		<div class="logo">
			<img alt="logo.png" src="../../static/images/user/logo.png"
				width="250px" height="100%"
				style="margin-left: auto; margin-right: auto; display: block";>
		</div>

		<!-- 상단 로그정보 영역 -->
		<div class="toplogin">
			<div class="btns" align="right">
				<div id="loginBtn" onclick="login();">로그인</div>
				
			</div>
		</div>

		<!-- 상단 메뉴바 -->
		<nav class="topMenu" style="color:#013252">
			<ul>
				<li><a href="#">채용정보</a></li>
				<li><a href="#">인재정보</a></li>
				<li><a href="#">열린마당</a></li>
				<li><a href="#">취업박사</a></li>
				<li><a href="#">고객센터</a></li>
			</ul>
			<div>
				<a href="#">이력서등록</a>
			</div>
			<div>
				<a href="#">마이페이지</a>
			</div>
		</nav><br>
		<hr style="border: solid 2px #013252">
        </header>
        <main id="main">
            <form class="resume_form" action="" name="" method="post">
                <fieldset>
                    <legend>이력서등록</legend>
                    <div class="form_wrap">
                        <div class="resume_title">
                            <label for="_title">현실에 안주하지 않고 늘 발전하는 리순신입니다..</label>
                        </div>
                        <div class="basic_info">
                            <div class="basic_info_title">기본정보</div>
                            <div class="basic_info_contents">
                                <table style="font-size: 18px; font-weight:bold">
                                    <tr>
                                        <td colspan="2" width="90px" height="50px" style="text-align:center">이순신</td>
                                        <td width="60px" style="text-align:center">|</td>
                                        <td width="150px">1992년(29)세</td>
                                        <td width="60px" style="text-align:center">|</td>
                                        <td width="40px">남</td>
                                        <td width="60px"></td>
                                        <td width="60px"></td>
                                        <td width="60px"></td>
                                        <td width="60px"></td>
                                        <td width="60px"></td>
                                        <td width="60px"></td>
                                        <td width="180px" rowspan="3"><img src="../../static/images/user/photo_icon_03.png"></td>
                                        <td width="130px"><button id="phone_view">연락처 확인</button></td>
                                    </tr>

                                    <tr>
                                        <td width="20px" height="50px"></td>
                                        <td width="40px"><img alt="logo.png" src="../../static/images/user/msg.png" width="30px" height="30px"></td>
                                        <td colspan="3">soon_ee1592@naver.com</td>
                                        <td width="40px"><img alt="logo.png" src="../../static/images/user/tel.png" width="30px" height="30px"></td>
                                        <td colspan="3">010-9210-5061</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><button id="interview">면접 제의</button></td>
                                        
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td height="60px"><img alt="logo.png" src="../../static/images/user/home.png" width="30px" height="30px"></td>
                                        <td colspan="10">경기도 군포시 번영로 403 가야아파트 509동 303호</td>   
                                        <td><button id="scrap">스크랩</button></td>
                                    </tr>

                                </table>
                        
                            </div>
                        </div>
                        <br><br>
                        
                        <div id="tableDiv">
                            <form action="">
                                <table>
                                    <tr>
                                        <td><h3>학력사항&nbsp;&nbsp;</h3></td>
                                        <td><h4>최종학력&nbsp;&nbsp;|&nbsp;&nbsp;총 &nbsp;<a style="color:red">1</a>건</h3></td>
                                        
                                    </tr>
                                </table>
                                <br>
                                <table style="text-align:center;">
                                     <tr class="tableBack">
                                         <th width="400px" height="50px">재학기간</th>
                                         <th width="200px">구분</th>
                                         <th width="400px">학교명(소재지)</th>
                                         <th width="400px">전공</th>
                                         <th width="200px">학점</th>
                                     </tr>
                                     <tr>
                                        <td width="400px" height="50px">1979.03 ~ 1982.02</td>
                                        <td>중퇴</td>
                                        <td>경희대학교</td>
                                        <td>경영학과</td>
                                        <td>4.0/4.5</td>
                                     </tr>
                                     <tr>
                                        <td width="400px" height="50px">1979.03 ~ 1982.02</td>
                                        <td>졸업</td>
                                        <td>군포고등학교</td>
                                        <td>문과</td>
                                        <td>-</td>
                                     </tr>
                                </table>
                                
                            </form>
                            <br><br>


                            <table>
                                <tr>
                                    <td><h3>경력사항&nbsp;&nbsp;</h3></td>
                                    <td><h4>|&nbsp;&nbsp;총 &nbsp;<a style="color:red">1</a>년</h3></td>
                                    
                                </tr>
                            </table>
                            <br>
                            <table style="text-align:center;">
                                <tr class="tableBack">
                                    <th width="400px" height="50px">재학기간</th>
                                    <th width="200px">구분</th>
                                    <th width="400px">학교명(소재지)</th>
                                    <th width="400px">전공</th>
                                    <th width="200px">학점</th>
                                </tr>
                                <tr>
                                   <td width="400px" height="50px">1979.03 ~ 1982.02</td>
                                   <td>중퇴</td>
                                   <td>경희대학교</td>
                                   <td>경영학과</td>
                                   <td>4.0/4.5</td>
                                </tr>
                                <tr>
                                   <td width="400px" height="50px">1979.03 ~ 1982.02</td>
                                   <td>졸업</td>
                                   <td>군포고등학교</td>
                                   <td>문과</td>
                                   <td>-</td>
                                </tr>
                           </table>
                           <br><br>

                           <table>
                            <tr>
                                <td><h3>자기소개서&nbsp;&nbsp;</h3></td>
                            </tr>
                        </table>
						<br>
                        <table class="self_intro_table">
                            <caption>자기소개서</caption>
                            <tbody>
                                <tr>
                                    <th class="re_title">제목</th>
                                    <td class="re_title_area">
                                        <input type="text" placeholder="자소서 제목" size="150" />
                                    </td>
                                </tr>
                                <tr>
                                    <th class="re_contents">내용</th>
                                    <td>
                                        <textarea class="re_contents_area" name="" id="" cols="151" rows="20" placeholder="자소서 내용"></textarea>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="resume_check">
                            <div class="resume_check_ch">
                                <span>위에 모든 기재 사항은 사실과 다름없음을 확인합니다.</span>
                            </div>
                            <p class="resume_date">
                                <span>작성일</span>
                                <span class="resume_date_date">2020년 1월 24일</span> |
                                <span>작성자</span>
                                <span class="resume_date_user">ㅇㅇ</span>
                            </p>
                        </div>
                        <div class="sub_btn">
                            <button id="backButton">뒤로</button>
                             
                        </div>
                    </div>
                </fieldset>
            </form>
        </main>
        <footer></footer>
    </div>
    <br>
    <br>
    <br>
    
</body>
</html>