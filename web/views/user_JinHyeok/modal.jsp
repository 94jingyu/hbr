<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.hbr.member.model.vo.Member"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
/*css reset*/

.tableBack66{background-color:#ECF0F7;font-weight:bold;}





/*main*/
#main{width:1100px;margin:0 auto}
#main legend{padding:20px 0 20px 0;font-size:22px;color:#00548b;font-weight:bold}
.resume_title66{margin-bottom:20px}
.resume_title66 label{font-weight:bold;font-size:15px;display:block;margin-bottom:10px}
.resume_titl66e input{border:1px solid #eee;padding:10px;}

/*기본정보*/
.basic_info66{margin-bottom:20px}
.basic_info66 .basic_info_title66{font-weight:bold;font-size:15px;display:block;margin-bottom:10px}
.basic_info66 .basic_info_contents66{background:#ecf0f7;padding:4%;position: relative}
.basic_info66 .basic_info_contents66 p{padding:10px}
.basic_info66 .basic_info_contents66 .uphoto{padding:0}
.basic_info66 .basic_info_contents66 label{font-size:15px;margin-bottom:10px;}
.basic_info66 .basic_info_contents66 input{padding:15px;}

.uname66 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:50px}
.uname66 input{background:#e8e9ee}
.b_day66 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px}
.email66 label{margin-right:60px}
.phone_num66 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:35px}
.phone_num66 input{margin-right:10px;background:#e8e9ee}
.phone_num66 a{text-decoration:underline}
.uaddress66 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:50px}
.uaddress66 .uaddress_01{margin-bottom:10px}
.uaddress66 .remain_addy{display:block;margin-left:110px}
.uphoto66{border:5px solid #fff;position:absolute;left:900px;top:35px;}
.uphoto66 label{cursor:pointer;}
.uphoto66 input{position:absolute;left:-9999px;top:-9999px;}

/*학력사항*/
.edu_bg_contents66{background:#ecf0f7;}
.edu_bg_title_0166{font-weight:bold;font-size:15px;display:inline-block;margin-bottom:10px}
.edu_bg_title_0266{font-size:11px}
.highest_edu66{border-bottom:2px solid #acacac;padding:4% 2%;}
.highest_edu66 ul{overflow:hidden;width:99%;margin:0 auto}
.highest_edu66 li{width:18%;float:left;height:40px;border:1px solid #ddd;background:#fff;margin:0 3%;text-align:center;line-height:2.6;font-size:15px;font-weight:bold}


/*경력사항*/
.Career_details66margin-bottom:20px}
.Career_details_title66{font-weight:bold;font-size:15px;margin-bottom:10px}
.Career_details_contents66{background:#ecf0f7;}
.Career_details_inpo66{padding:4%}
.Career_details66 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.Career_details66 input{padding:15px;color:#8b7575}
.Career_details66 select{padding:15px;color:#8b7575;width:372px;}
.Career_details66 p{margin-bottom:20px;}
.Career_position66,.Career_type66{position:relative}
.Career_details_inpo66 a{text-decoration:underline;position:absolute;left:430px;top:11px;font-weight:bold}
.sel_career66{border-bottom:2px solid #acacac;padding:4% 2%;}
.sel_career66 ul{width:60%;margin:0 auto;overflow:hidden;}
.sel_career66 li{width:40%;margin:3%;background:#fff;float:left;text-align:center;height:40px;border:1px solid #ddd;line-height:2.6;font-size:15px;font-weight:bold}
.Career_details66 .cor_name66{margin-right:35px}
.Career_details66 .term_office_input66{width:145px;}
.Career_details66 .job_type66{margin-right:50px}
.Career_details66 .sel_position66{margin-right:15px}

/*자기소개서*/
.self_intro66{margin-bottom:20px}
.self_intro_title66{margin-bottom:10px}
.self_intro_title_0166{font-weight:bold;font-size:15px;margin-bottom:10px}
.self_intro_title_0266{font-size:11px}
.self_intro_table66{border:1px solid #eee;margin-bottom:10px}

.self_intro_table66 caption,.self_intro_table66 .re_title66,.self_intro_table66 .re_contents66{position:absolute;left:-9999px;top:-9999px}
.re_title_area66{border-bottom:1px solid #000000;padding:15px;font-size:15px}
.re_contents_area66{border:none;padding:15px;}
.terms_desired166{border-bottom:1px solid #000000;padding:15px;font-size:15px}
.terms_desired266{border:none;padding:15px;}
.letter_count66{font-weight:bold;font-size:16px;}
.letter_count66 .letter_red66{color:#ff0000}

/*희망조건 선택*/
.de_condition66{margin-bottom:20px}
.de_condition_title66{font-weight:bold;font-size:15px;margin-bottom:10px}
.de_condition_contents66{background:#ecf0f7;padding:4%}
.de_condition_contents66 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:45px;font-size:15px}
.de_condition_contents66 .int_job{padding-right:80px;background:url(../images/essential_3_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.de_condition_contents66 select{padding:15px;color:#8b7575;margin-right:30px;width:200px;}
.de_condition_contents66 p{margin-bottom:20px;}

/*이력서 공개여부*/
.resume_public66{margin-bottom:20px}
.resume_public_title66{font-weight:bold;font-size:15px;margin-bottom:10px}
.resume_public_contents66{background:#ecf0f7;padding:4%}
.resume_public_contents66 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.resume_public_radio66{margin-left:15px}
.resume_public_contents66 p{font-size:15px;font-weight:bold;margin:5px 0 0 160px}

.resume_check66{padding:10px}
.resume_check_ch66{width:410px;margin:0 auto;margin-bottom:20px}
.resume_check_ch66 span{font-weight:bold}
.resume_date66{width:280px;margin:0 auto}
.resume_date_date66,.resume_date_user66{font-weight:bold}

/*버튼*/
.sub_btn66{}

#backButton66{background: #013252;
		color: white;
		border-style: none;
		width: 180px;
		height: 60px;
		font-weight: bold;
		margin-top: 20px;
		margin-left: 450px;
		font-size:18px;}
</style>
</head>
<body>
	<div id="modal" class="searchModal" style="margin-left:240px;">
            <div class="search-modal-content">
                  	     <fieldset style="text-align:left">
                    <legend>이력서등록</legend>
                    <div class="form_wrap66">
                        <div class="resume_title66">
                        <label for="_title" id="title5"></label>
                        
                        
                        </div>
                        <div class="basic_info66">
                            <div class="basic_info_title66">기본정보</div>
                            <div class="basic_info_contents66">
                                <table style="font-size: 18px; font-weight:bold">
                                    <tr class="table_info66">
                                        <td colspan="3" width="90px" height="50px" style="text-align:center"></td>
                                        <td width="60px" style="text-align:center">|</td>
                                       
                                      	
                                        <td width="150px" id="birth"><p id="b1"></p></td>
                                     
                                        <td width="60px" style="text-align:center">|</td>
                                       
                                      
                                        <td width="40px" style="text-align:center" id="gender"><p id="b2"></p></td>
                                        
                                        <td width="60px"></td>
                                        <td width="60px"></td>
                                        <td width="60px"></td>
                                        <td width="60px"></td>
                                       	<td width="60px"></td>
                                       	<td width="60px"></td>
                                       	
                                       	<td width="130px" rowspan="3"><img src="../../static/images/user/photo_icon_03.png"></td>
                                       	                             
                                    </tr>

                                    <tr>
                                        <td width="20px" height="50px"></td>
                                        <td width="40px"><img alt="logo.png" src="../../static/images/user/msg.png" width="30px" height="30px" ></td>
                                        
                                        
                                        <td colspan="3"><p id="b3"></p></td>
                                        
                                        <td width="40px"><img alt="logo.png" src="../../static/images/user/tel.png" width="30px" height="30px" style="margin-left:17px"></td>
                                        <td colspan="3"><p id="b4"></p></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td height="60px"><img alt="logo.png" src="../../static/images/user/home.png" width="30px" height="30px"></td>
                                        <td colspan="9"><p id="b5"></p></td>   
                                        <td></td>
                                        <td></td>
                                    </tr>

                                </table>
                        
                            </div>
                        </div>
                        <br><br>
                        
                        <div id="tableDiv66">
                             
                                <table>
                                    <tr>
                                        <td><h3>학력사항&nbsp;&nbsp;</h3></td>
                                        <td><h4>최종학력&nbsp;&nbsp;|&nbsp;&nbsp;총 &nbsp;<a style="color:red"></a>건</h3></td>
                                        
                                    </tr>
                                </table>
                                <br>
                                <table style="text-align:center; border:1px Ridge #dcdcdc">
                                     <tr style="border:1px solid black" class="tableBack66">
                                         <th width="400px" height="50px">재학기간</th>
                                         <th width="200px">구분</th>
                                         <th width="400px">학교명(소재지)</th>
                                         <th width="400px">전공</th>
                                         <th width="200px">학점</th>
                                     </tr>
                                      <tr style="border:1px solid black"class="tableShit66" >
                                      	
                                      	
                                        <td width="400px" height="50px"><p id="b6"></p> ~ <p id="b7"></p></td>
                                        
                                        <td><p id="b8"></p></td>
                                        <td><p id="b9"></p></td>
                                        <td><p id="b10"></p></td>
                                        <td>-</td>
                                     </tr>
                                    
                                    
                                     <tr style="border:1px solid black" class="tableShit66">
                                    
                                     	
                                        <td width="400px" height="50px"><p id="b11"></p> ~ <p id="b12"></p></td>
                                       
                                        <td><p id="b13"></p></td>
                                        <td><p id="b14"></p></td>
                                        <td><p id="b15"></p></td>
                                       
                                        
                                        <td><p id="b16"></p>/<p id="b17"></p></td>
                                        
                                     </tr>
                                     
                                </table>
                                
                           
                            <br><br>


                            <table>
                                <tr>
                                    <td><h3>경력사항&nbsp;&nbsp;</h3></td>
                                	
                                	<td><h4>|&nbsp;&nbsp;신입</h4></td>
                                	
                                    
                                    
                                    <td><h4>|&nbsp;&nbsp;총 &nbsp;</h4><p style="color:red" id="b18"></p><h4>년</h4></td>
                                    
                                </tr>
                            </table>
                            <br>
                            	
                            <table style="text-align:center; border:1px Ridge #dcdcdc;">
                                <tr class="tableBack66">
                                    <th width="350px" height="50px">근무기간</th>
                                    <th width="250px">회사명</th>
                                    <th width="400px">부서 / 직급 · 직책</th>
                                    <th width="400px">직종</th>
                                    <th width="200px">근무지역</th>
                                </tr>
                                <tr>
                                   
                                   
                                   <td width="35px" height="50px" style="border-bottom:1px Ridge #dcdcdc;"><p id="b19"></p> ~ <p id="b20"></p></td>
                                  
                                   <td style="border-bottom:1px Ridge #dcdcdc;"><p id="b21"></p></td>
                                   <td style="border-bottom:1px Ridge #dcdcdc;"><p id="b22"></p> / <p id="b23"></p></td>
                                   <td style="border-bottom:1px Ridge #dcdcdc;"><p id="b24"></p></td>
                                   <td style="border-bottom:1px Ridge #dcdcdc;"><p id="b25"></p></td>
                                </tr>
                                <tr>
                                	<td colspan="1" style="height:70px; align:center"><div style="margin-left:70px; text-align:center; width:100px; height:40px; border:1.5px solid green; border-radius: 14px; line-height:35px;">당담업무</div></td>
                                	<td style="text-align:left"><p style="font-size:18px"><p id="b26"></p></p></td>
                                </tr>
                                
                           </table>
                           <br><br>

                           <table>
                            <tr>
                                <td><h3>자기소개서&nbsp;&nbsp;</h3></td>
                            </tr>
                        </table>
						<br>
                        <table class="self_intro_table66" style="background-color:#ECF0F7">
                            <caption>자기소개서</caption>
                            <tbody>
                                <tr> 
                                    <th class="re_title66">제목</th>
                                    <td class="re_title_area66">
                                    	
                                        <h3 style="font-weight:bold;" id="27"></h3>
                                      
                                    </td>
                                </tr>
                                <tr>
                                    <th class="re_contents66">내용</th>
                                    <td class="re_contents_area66" height="500px" width="1100px" align="left" valign="top">
                                    
                                        <p id="b28"></p>
                                    
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br><br><br>
                        <table>
                            <tr>
                                <td><h3>희망조건&nbsp;&nbsp;</h3></td>
                            </tr>
                        </table>
						<br>
                        <table style="border: 1px solid #7D7777" width="1100">

                            <tbody>
                                <tr>
                                    <td class="terms_desired166"><div style="text-align:center; width:100px; height:40px; border:1.5px solid green; border-radius: 14px; line-height:35px;">관심지역</div></td>
                                    
                                    <td class="terms_desired166" width="850px"><p id="b29"></p></td>
                                    
                                </tr>
                                <tr>
                                    <td class="terms_desired266"><div style="text-align:center; width:100px; height:40px; border:1.5px solid green; border-radius: 14px; line-height:35px;">관심직종</div></td>
                                    <td class="terms_desired266"><p id="b30"></p></td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="resume_check66">
                            <div class="resume_check_ch66">
                                <span>위에 모든 기재 사항은 사실과 다름없음을 확인합니다.</span>
                            </div>
                            <p class="resume_date66">
                                <span>작성일</span>
                                <span class="resume_date_date66" id="b31" ></span> |
                                <span>작성자</span>
                                <span class="resume_date_user66" id="b32"></span>
                            </p>
                        </div>
                        <div class="sub_btn66">
                            <button id="backButton">뒤로</button> 
                        </div>
                    </div> 
                </fieldset>
               </div>
         </div>

</body>
</html>