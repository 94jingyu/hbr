<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.hbr.member.model.vo.Member"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
/*css reset*/

.tableBack66{background-color:#ECF0F7;font-weight:bold;}





/*main*/
#main{width:1100px;margin:0 auto}
#main legend{padding:20px 0 20px 0;font-size:22px;color:#00548b;font-weight:bold}
.resume_title66{margin-bottom:20px}
.resume_title66 label{font-weight:bold;font-size:15px;display:block;margin-bottom:10px}
.resume_titl66e input{border:1px solid #eee;padding:10px;}

/*기본정보*/
.basic_info66{margin-bottom:20px}
.basic_info66 .basic_info_title66{font-weight:bold;font-size:15px;display:block;margin-bottom:10px}
.basic_info66 .basic_info_contents66{background:#ecf0f7;padding:4%;position: relative}
.basic_info66 .basic_info_contents66 p{padding:10px}
.basic_info66 .basic_info_contents66 .uphoto{padding:0}
.basic_info66 .basic_info_contents66 label{font-size:15px;margin-bottom:10px;}
.basic_info66 .basic_info_contents66 input{padding:15px;}

.uname66 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:50px}
.uname66 input{background:#e8e9ee}
.b_day66 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px}
.email66 label{margin-right:60px}
.phone_num66 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:35px}
.phone_num66 input{margin-right:10px;background:#e8e9ee}
.phone_num66 a{text-decoration:underline}
.uaddress66 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:50px}
.uaddress66 .uaddress_01{margin-bottom:10px}
.uaddress66 .remain_addy{display:block;margin-left:110px}
.uphoto66{border:5px solid #fff;position:absolute;left:900px;top:35px;}
.uphoto66 label{cursor:pointer;}
.uphoto66 input{position:absolute;left:-9999px;top:-9999px;}

/*학력사항*/
.edu_bg_contents66{background:#ecf0f7;}
.edu_bg_title_0166{font-weight:bold;font-size:15px;display:inline-block;margin-bottom:10px}
.edu_bg_title_0266{font-size:11px}
.highest_edu66{border-bottom:2px solid #acacac;padding:4% 2%;}
.highest_edu66 ul{overflow:hidden;width:99%;margin:0 auto}
.highest_edu66 li{width:18%;float:left;height:40px;border:1px solid #ddd;background:#fff;margin:0 3%;text-align:center;line-height:2.6;font-size:15px;font-weight:bold}


/*경력사항*/
.Career_details66margin-bottom:20px}
.Career_details_title66{font-weight:bold;font-size:15px;margin-bottom:10px}
.Career_details_contents66{background:#ecf0f7;}
.Career_details_inpo66{padding:4%}
.Career_details66 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.Career_details66 input{padding:15px;color:#8b7575}
.Career_details66 select{padding:15px;color:#8b7575;width:372px;}
.Career_details66 p{margin-bottom:20px;}
.Career_position66,.Career_type66{position:relative}
.Career_details_inpo66 a{text-decoration:underline;position:absolute;left:430px;top:11px;font-weight:bold}
.sel_career66{border-bottom:2px solid #acacac;padding:4% 2%;}
.sel_career66 ul{width:60%;margin:0 auto;overflow:hidden;}
.sel_career66 li{width:40%;margin:3%;background:#fff;float:left;text-align:center;height:40px;border:1px solid #ddd;line-height:2.6;font-size:15px;font-weight:bold}
.Career_details66 .cor_name66{margin-right:35px}
.Career_details66 .term_office_input66{width:145px;}
.Career_details66 .job_type66{margin-right:50px}
.Career_details66 .sel_position66{margin-right:15px}

/*자기소개서*/
.self_intro66{margin-bottom:20px}
.self_intro_title66{margin-bottom:10px}
.self_intro_title_0166{font-weight:bold;font-size:15px;margin-bottom:10px}
.self_intro_title_0266{font-size:11px}
.self_intro_table66{border:1px solid #eee;margin-bottom:10px}

.self_intro_table66 caption,.self_intro_table66 .re_title66,.self_intro_table66 .re_contents66{position:absolute;left:-9999px;top:-9999px}
.re_title_area66{border-bottom:1px solid #000000;padding:15px;font-size:15px}
.re_contents_area66{border:none;padding:15px;}
.terms_desired166{border-bottom:1px solid #000000;padding:15px;font-size:15px}
.terms_desired266{border:none;padding:15px;}
.letter_count66{font-weight:bold;font-size:16px;}
.letter_count66 .letter_red66{color:#ff0000}

/*희망조건 선택*/
.de_condition66{margin-bottom:20px}
.de_condition_title66{font-weight:bold;font-size:15px;margin-bottom:10px}
.de_condition_contents66{background:#ecf0f7;padding:4%}
.de_condition_contents66 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:45px;font-size:15px}
.de_condition_contents66 .int_job{padding-right:80px;background:url(../images/essential_3_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.de_condition_contents66 select{padding:15px;color:#8b7575;margin-right:30px;width:200px;}
.de_condition_contents66 p{margin-bottom:20px;}

/*이력서 공개여부*/
.resume_public66{margin-bottom:20px}
.resume_public_title66{font-weight:bold;font-size:15px;margin-bottom:10px}
.resume_public_contents66{background:#ecf0f7;padding:4%}
.resume_public_contents66 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.resume_public_radio66{margin-left:15px}
.resume_public_contents66 p{font-size:15px;font-weight:bold;margin:5px 0 0 160px}

.resume_check66{padding:10px}
.resume_check_ch66{width:480px;margin:0 auto;margin-bottom:20px}
.resume_check_ch66 span{font-weight:bold}
.resume_date66{width:340px;margin:0 auto}
.resume_date_date66,.resume_date_user66{font-weight:bold}

/*버튼*/
.sub_btn66{}

#backButton66{background: #013252;
		color: white;
		border-style: none;
		width: 180px;
		height: 60px;
		font-weight: bold;
		margin-top: 20px;
		margin-left: 450px;
		font-size:18px;}
	/* td{
		border:1px solid black;	} */
.searchModal {
   display: none; /* Hidden by default */
   position: fixed; /* Stay in place */
   z-index: 10; /* Sit on top */
   left: 0;
   top: 0;
   width: 100%; /* Full width */
   height: 100%; /* Full height */
   overflow: auto; /* Enable scroll if needed */
   background-color: rgb(0,0,0); /* Fallback color */
   background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
   border: 10px solid black;
}

/* Modal content */
.search-modal-content {
   background-color: #fefefe;
   margin: 15% auto; /* 15% from the top and centered */
   padding: 20px;
   border: 1px solid #888;
   width: 1200px; /* Could be more or less, depending on screen size */
   height: 2500px;
}

body,h1,h2,h3,h4,h5,h6,p,div,header,main,footer,section,article,nav,ul,li,form,fieldset,legend,label,p,address,table,dl,dt,dd,input,select,textarea,button,figure,figcaption,table,th,td,tr{margin:0;padding:0;}
 
.sch1:hover{
   background: #2FA599;
   font-weight: bold;
}
.sch2:hover{
   background: #2FA599;
   font-weight: bold;
}
.sch3:hover{
   background: #2FA599;
   font-weight: bold;
}
.sch4:hover{
   background: #2FA599;
   font-weight: bold;
}
.newcareer:hover{
   background: #2FA599;
      font-weight: bold;
}
.oldcareer:hover{
   background: #2FA599;
   font-weight: bold;
}
        
ul,li{list-style:none}
a{text-decoration:none;color:#222}
a:hover{color:#2698cb}
address{font-style:normal}
form,input,button{vertical-align:middle}
button{cursor:pointer;border:none}
img,fieldset,select{border:none}
input{border:none;outline:none}
.wrap {width: 1200px; margin: 0 auto;}
/* 헤더 영역 */
header{height:137px;border-bottom:2px solid #013252}
.logo{float:left;width:22%;height:130px;}
.toplogin{width: 100%; height: 50px; padding: 1%;}
   
/* 상단메뉴바 */
* {
   text-align:left;
   margin: 0;
   padding: 0;
}
/* body {background-color: #fffde7;} */
#wrap {
   width: 1200px;
   margin: 0 auto;
}

/* 헤더 영역 */
.logo {
   float: left;
   width: 22%;
   height: 130px;
}

.toplogin {
   width: 100%;
   height: 50px;
   padding: 1%;
}

/* 상단메뉴바 */
.topMenu {
   /* display: inline-block; */
   width: 100%;
   height: 50px;
   /* background: #013252; */
   line-height: 30px; /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
   vertical-align: middle; /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
   text-align: center; /* 글씨 정렬을 가운데로 설정 */
   font-weight: bolder;
   font-size: 20px;
   color: #013252;
}

.topMenu>div {
   float: right;
}

.topMenu>ul {
   float: left;
   margin: 0;
   padding: 0;
   list-style: none;
}

.topMenu>ul>li {
   display: inline-block;
   padding: 0;
}

.topMenu a {
   display: block;
   text-decoration: none;
   padding: 10px 20px;
   color: black;
}

.topMenu a:hover {
   background: #013252;
   color: white;
}
.btns {
   float:right;
}

 #loginBtn, #memberJoinBtn, #logoutBtn, #changeInfo, #adminBtn {
   display: inline-block;
   text-align: center;
   
   
   height: 25px;
   width: 100px;
   border-radius: 5px;
   
   
}



/*main*/
#main{width:1100px;margin:0 auto}
#main legend{padding:20px 0 20px 0;font-size:22px;color:#00548b;font-weight:bold}
.resume_title{margin-bottom:20px}
.resume_title label{font-weight:bold;font-size:15px;display:block;margin-bottom:10px}
.resume_title input{border:1px solid #eee;padding:10px;}

/*기본정보*/
.basic_info{margin-bottom:20px}
.basic_info .basic_info_title{font-weight:bold;font-size:15px;display:block;margin-bottom:10px}
.basic_info .basic_info_contents{background:#ecf0f7;padding:4%;position: relative}
.basic_info .basic_info_contents p{padding:10px}
.basic_info .basic_info_contents .uphoto{padding:0}
.basic_info .basic_info_contents label{font-size:15px;margin-bottom:10px;}
.basic_info .basic_info_contents input{padding:15px;}
.gender label{padding-right:25px;background:url(../../static/images/user/essential_icon.png) no-repeat bottom right;margin-right:50px}

.uname label{padding-right:25px;background:url(../../static/images/user/essential_icon.png) no-repeat bottom right;margin-right:50px}
.uname input{background:#e8e9ee}
.b_day label{padding-right:25px;background:url(../../static/images/user/essential_icon.png) no-repeat bottom right;margin-right:22px}
.email label{margin-right:60px}
.phone_num label{padding-right:25px;background:url(../../static/images/user/essential_icon.png) no-repeat bottom right;margin-right:35px}
.phone_num input{margin-right:10px;background:#e8e9ee}
.phone_num a{text-decoration:underline}
.uaddress label{padding-right:25px;background:url(../../static/images/user/essential_icon.png) no-repeat bottom right;margin-right:50px}
.uaddress .uaddress_01{margin-bottom:10px}
.uaddress .remain_addy{display:block;margin-left:110px}
.uphoto{border:5px solid #fff;position:absolute;left:900px;top:35px;}
.uphoto label{cursor:pointer;}
.uphoto input{position:absolute;left:-9999px;top:-9999px;}

/*학력사항*/
.edu_bg_contents{background:#ecf0f7;}
.edu_bg_title_01{font-weight:bold;font-size:15px;display:inline-block;margin-bottom:10px}
.edu_bg_title_02{font-size:11px}
.highest_edu{border-bottom:2px solid #acacac;padding:4% 2%;}
.highest_edu ul{overflow:hidden;width:99%;margin:0 auto}
.highest_edu li{width:18%;float:left;height:40px;border:1px solid #ddd;background:#fff;margin:0 3%;text-align:center;line-height:2.6;font-size:15px;font-weight:bold}

/*고등학교 정보 입력*/
.high_sch_info{clear:both;padding:4%;border-bottom:2px solid #acacac}
.high_sch_info_title{font-weight:bold;font-size:15px;}
.high_sch_info label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.high_sch_info input{padding:15px;color:#8b7575;}
.high_sch_info select{padding:15px;height:50px;color:#8b7575}
.high_sch_info p{margin-bottom:20px}
.high_sch_info .high_name_title{margin-right:35px}
.high_major{width:372px;color:#8b7575;}
.high_sch_info .high_select{vertical-align:bottom}

/*고등학교 정보 입력*/
.high_sch_info{clear:both;padding:4%;border-bottom:2px solid #acacac}
.high_sch_info_title{font-weight:bold;font-size:15px;}
.high_sch_info label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.high_sch_info input{padding:15px;color:#8b7575}
.high_sch_info select{padding:15px;height:50px;color:#8b7575}
.high_sch_info p{margin-bottom:20px}
.high_sch_info .high_name_title{margin-right:35px}
.high_major{width:372px;color:#8b7575;}
.high_sch_info .high_select{vertical-align:bottom}

/*고등학교 정보 입력*/
.high_sch_info{clear:both;padding:4%;border-bottom:2px solid #acacac}
.high_sch_info_title{font-weight:bold;font-size:15px;}
.high_sch_info label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.high_sch_info input{padding:15px;color:#8b7575}
.high_sch_info select{padding:15px;height:50px;color:#8b7575}
.high_sch_info p{margin-bottom:20px}
.high_sch_info .high_name_title{margin-right:35px}
.high_major{width:372px;color:#8b7575;}
.high_sch_info .high_select{vertical-align:bottom}
/*초등학교 정보 입력*/
.high_sch_info1{clear:both;padding:4%;border-bottom:2px solid #acacac}
.high_sch_info_title1{font-weight:bold;font-size:15px;}
.high_sch_info1 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.high_sch_info1 input{padding:15px;color:#8b7575}
.high_sch_info1 select{padding:15px;height:50px;color:#8b7575}
.high_sch_info1 p{margin-bottom:20px}
.high_sch_info1 .high_name_title1{margin-right:35px}
.high_major1{width:372px;color:#8b7575;}
.high_sch_info1 .high_select1{vertical-align:bottom}

.high_sch_info2{clear:both;padding:4%;border-bottom:2px solid #acacac}
.high_sch_info_title2{font-weight:bold;font-size:15px;}
.high_sch_info2 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.high_sch_info2 input{padding:15px;color:#8b7575}
.high_sch_info2 select{padding:15px;height:50px;color:#8b7575}
.high_sch_info2 p{margin-bottom:20px}
.high_sch_info2 .high_name_title2{margin-right:35px}
.high_major2{width:372px;color:#8b7575;}
.high_sch_info2 .high_select2{vertical-align:bottom}

.high_sch_info3{clear:both;padding:4%;border-bottom:2px solid #acacac}
.high_sch_info_title3{font-weight:bold;font-size:15px;}
.high_sch_info3 label{padding-right:25px;background:url(../images/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.high_sch_info3 input{padding:15px;color:#8b7575}
.high_sch_info3 select{padding:15px;height:50px;color:#8b7575}
.high_sch_info3 p{margin-bottom:20px}
.high_sch_info3 .high_name_title1{margin-right:35px}
.high_major3{width:372px;color:#8b7575;}
.high_sch_info3 .high_select3{vertical-align:bottom}

/*대학·대학원 정보 입력*/
.uni_info{padding:4%;margin-bottom:20px}
.uni_info label{padding-right:25px;background:url(../../static/images/user/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.uni_info input{padding:15px;color:#8b7575}
.uni_info select{padding:15px;color:#8b7575;width:348px;}
.uni_info p{margin-bottom:20px}
.uni_info_title{font-weight:bold;font-size:15px;margin-bottom:10px}
.uni_info .sel_uni{margin-right:50px}
.uni_info .uni_name{margin-right:35px}
.uni_info .uni_area,.uni_info .uni_major{margin-right:50px}
.uni_info .uni_major_select{height:45px;vertical-align:bottom;padding:14px;}
.uni_info .uni_major_input{display:block;margin:10px 0 0 110px}
.uni_info .uni_select{vertical-align:bottom;width:auto;height:50px;}
.uni_info .uni_credit_label{background:none;padding:0;margin-right:75px}
.uni_info .credit_limit_select{width:168px;height:45px;vertical-align:bottom}

/*경력사항*/
.Career_details{margin-bottom:20px}
.Career_details_title{font-weight:bold;font-size:15px;margin-bottom:10px}
.Career_details_contents{background:#ecf0f7;}
.Career_details_inpo{padding:4%}
.Career_details label{padding-right:25px;background:url(../../static/images/user/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.Career_details input{padding:15px;color:#8b7575}
.Career_details select{padding:15px;color:#8b7575;width:372px;}
.Career_details p{margin-bottom:20px;}
.Career_position,.Career_type{position:relative}
.Career_details_inpo a{text-decoration:underline;position:absolute;left:430px;top:11px;font-weight:bold}
.sel_career{border-bottom:2px solid #acacac;padding:4% 2%;}
.sel_career ul{width:60%;margin:0 auto;overflow:hidden;}
.sel_career li{width:40%;margin:3%;background:#fff;float:left;text-align:center;height:40px;border:1px solid #ddd;line-height:2.6;font-size:15px;font-weight:bold}
.Career_details .cor_name{margin-right:35px}
.Career_details .term_office_input{width:145px;}
.Career_details .job_type{margin-right:50px}
.Career_details .sel_position{margin-right:15px}

/*자기소개서*/
.self_intro{margin-bottom:20px}
.self_intro_title{margin-bottom:10px}
.self_intro_title_01{font-weight:bold;font-size:15px;margin-bottom:10px}
.self_intro_title_02{font-size:11px}
.self_intro_table{border:1px solid #eee;margin-bottom:10px}
.self_intro_table caption,.self_intro_table .re_title,.self_intro_table .re_contents{position:absolute;left:-9999px;top:-9999px}
.re_title_area{border-bottom:1px solid #eee;padding:13px;font-size:15px}
.re_contents_area{border:none;padding:10px;}
.letter_count{font-weight:bold;font-size:16px;}
.letter_count .letter_red{color:#ff0000}

/*희망조건 선택*/
.de_condition{margin-bottom:20px}
.de_condition_title{font-weight:bold;font-size:15px;margin-bottom:10px}
.de_condition_contents{background:#ecf0f7;padding:4%}
.de_condition_contents label{padding-right:25px;background:url(../../static/images/user/essential_icon.png) no-repeat bottom right;margin-right:45px;font-size:15px}
.de_condition_contents .int_job{padding-right:80px;background:url(../../static/images/user/essential_3_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.de_condition_contents select{padding:15px;color:#8b7575;margin-right:30px;width:200px;}
.de_condition_contents p{margin-bottom:20px;}

/*이력서 공개여부*/
.resume_public{margin-bottom:20px}
.resume_public_title{font-weight:bold;font-size:15px;margin-bottom:10px}
.resume_public_contents{background:#ecf0f7;padding:4%}
.resume_public_contents label{padding-right:25px;background:url(../../static/images/user/essential_icon.png) no-repeat bottom right;margin-right:22px;font-size:15px}
.resume_public_radio{margin-left:15px}
.resume_public_contents p{font-size:15px;font-weight:bold;margin:5px 0 0 160px}

.resume_check{padding:10px}
.resume_check_ch{width:410px;margin:0 auto;margin-bottom:20px}
.resume_check_ch span{font-weight:bold}
.resume_date{width:320px;margin:0 auto}
.resume_date_date,.resume_date_user{font-weight:bold}
#resume_view{ 
      background: #013252;
      color: white;
      border-style: none;
      width: 180px;
      height: 60px;
      font-weight: bold;
      margin-top: 20px;
      margin-left: 260px;
      font-size:18px;
      }

#resume_save{
      background: #013252;
      color: white;
      border-style: none;
      width: 180px;
      height: 60px;
      font-weight: bold;
      margin-top: 20px;
      margin-left: 20px;
      font-size:18px;
}
#resume_commit{
      background: #2FA599;
      color: white;
      border-style: none;
      width: 180px;
      height: 60px;
      font-weight: bold;
      margin-top: 20px;
      margin-left: 20px;
      font-size:18px;

}
.addressBtn{
width:120px; height:34px; font-size:15px; margin: 2px; background: #666666; color:#C2C2C2; border: none; padding:5px;
}
footer{margin:0 auto; text-align:center;}
   footer > span {padding: 20px;}
/*버튼*/
.sub_btn{}
</style>
</head>
<body>
   <div id="wrap">
      <!-- 로고 영역 -->
      <%@ include file="../common/E_user_menubar.jsp" %>
      <hr style="border: solid 2px #013252; width:100%; margin:0 auto"><br>
      <div id="modal" class="searchModal" style="margin-left:240px;">
            <div class="search-modal-content">
            <br>  <br>  <br>  <br>  <br>  <br>  <br>  <br>
                  	     <fieldset style="text-align:left">
                    <legend>이력서등록</legend>
                    <div class="form_wrap66">
                        <div class="resume_title66">
                        <label for="_title" id="title5"></label>
                        
                        
                        </div>
                        <div class="basic_info66">
                            <div class="basic_info_title66">기본정보</div>
                            <div class="basic_info_contents66">
                                <table style="font-size: 18px; font-weight:bold">
                                    <tr class="table_info66">
                                        <td colspan="3" width="90px" height="50px" style="text-align:center"><%= loginUser.getMemberName()%></td>
                                        <td width="60px" style="text-align:center">|</td>
                                       
                                      	
                                        <td width="150px" id="birth"><p id="b1" style="text-align:center"></p></td>
                                     
                                        <td width="60px" style="text-align:center" style="text-align:center">|</td>
                                       
                                      
                                        <td width="40px" style="text-align:center" id="gender"><p id="b2" style="text-align:center"></p></td>
                                        
                                        <td width="60px" style="text-align:center"></td>
                                        <td width="60px" style="text-align:center"></td>
                                        <td width="60px" style="text-align:center"></td>
                                        <td width="60px" style="text-align:center"></td>
                                       	<td width="60px" style="text-align:center"></td>
                                       	<td width="60px" style="text-align:center"></td>
                                       	<td width="130px" rowspan="3" style="text-align:center"><img width="125px" height="163px" src="../../static/images/user/photo_icon_03.png" id="userimg"></td>
                                       	                             
                                    </tr>

                                    <tr>
                                        <td width="20px" height="50px" style="text-align:center"></td>
                                        <td width="40px" style="text-align:center"><img alt="logo.png" src="../../static/images/user/msg.png" width="30px" height="30px" ></td>
                                        
                                        
                                        <td colspan="3" style="text-align:center"><p id="b3" style="text-align:center"></p></td>
                                        
                                        <td width="40px" style="text-align:center"><img alt="logo.png" src="../../static/images/user/tel.png" width="30px" height="30px" style="margin-left:17px"></td>
                                        <td colspan="3" style="text-align:center"><%=loginUser.getPhone() %></td>
                                        <td style="text-align:center"></td>
                                        <td style="text-align:center"></td>
                                        <td style="text-align:center"></td>
                                        <td style="text-align:center"></td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td height="60px" style="text-align:center"><img alt="logo.png" src="../../static/images/user/home.png" width="30px" height="30px"></td>
                                        <td colspan="9" style="text-align:center"><p id="b5" style="text-align:center"></p></td>   
                                        <td></td>
                                        <td></td>
                                    </tr>

                                </table>
                        
                            </div>
                        </div>
                        <br><br>
                        
                        <div id="tableDiv66">
                             
                                <table>
                                    <tr>
                                        <td><h3>학력사항&nbsp;&nbsp;</h3></td>
                                        <td><h4>최종학력&nbsp;&nbsp;|&nbsp;&nbsp;총 &nbsp;<a style="color:red"></a>건</h3></td>
                                        
                                    </tr>
                                </table>
                                <br>
                                <table style="text-align:center; border:1px Ridge #dcdcdc">
                                     <tr style="border:1px solid black" class="tableBack66">
                                         <th width="390px" height="50px" style="text-align:center">재학기간</th>
                                         <th width="190px" style="text-align:center">구분</th>
                                         <th width="370px" style="text-align:center">학교명(소재지)</th>
                                         <th width="370px" style="text-align:center">전공</th>
                                         <th width="280px" style="text-align:center">학점</th>
                                     </tr>
                                      <tr style="border:1px solid black"class="tableShit66" style="text-align:center">
                                      	
                                      	
                                        <td width="400px" height="50px" style="text-align:center"><p id="b6" style="text-align:center"></p></td>
                                        
                                        <td style="text-align:center"><p id="b8" style="text-align:center"></p></td>
                                        <td style="text-align:center"><p id="b9" style="text-align:center"></p></td>
                                        <td style="text-align:center"><p id="b10" style="text-align:center"></p></td>
                                        <td style="text-align:center">-</td>
                                     </tr>
                                    
                                    
                                     <tr style="border:1px solid black" class="tableShit66" style="text-align:center">
                                    
                                     	
                                        <td width="400px" height="50px" style="text-align:center"><p id="b11" style="text-align:center"></p></td>
                                       
                                        <td style="text-align:center"><p id="b13" style="text-align:center"></p></td>
                                        <td style="text-align:center"><p id="b14" style="text-align:center"></p></td>
                                        <td style="text-align:center"><p id="b15" style="text-align:center"></p></td>
                                        <td style="text-align:center"><p id="b16" style="text-align:center"></p></td>
                                        
                                     </tr>
                                     
                                </table>
                                
                           
                            <br><br>


                            <table>
                                <tr>
                                    <td><h3>경력사항&nbsp;</h3></td>
                                	<td><h4>|&nbsp;경력&nbsp;</h4></td>
                                    <td><h4>|&nbsp;총 &nbsp;</h4></td><td><p style="color:red" id="b18"></p><h4>년</h4></td>
                                </tr>
                            </table>
                            <br>
                            	
                            <table style="text-align:center; border:1px Ridge #dcdcdc;">
                                <tr class="tableBack66" style="text-align:center">
                                    <th width="350px" height="50px" style="text-align:center">근무기간</th>
                                    <th width="250px" style="text-align:center">회사명</th>
                                    <th width="400px" style="text-align:center">부서 / 직급 · 직책</th>
                                    <th width="400px" style="text-align:center">직종</th>
                                    <th width="200px" style="text-align:center">근무지역</th>
                                </tr>
                                <tr>
                                   
                                   
                                   <td width="35px" height="50px" style="border-bottom:1px Ridge #dcdcdc; text-align:center"><p style="text-align:center" id="b19"></p></td>
                                  
                                   <td style="border-bottom:1px Ridge #dcdcdc; text-align:center"><p id="b21" style="text-align:center"></p></td>
                                   <td style="border-bottom:1px Ridge #dcdcdc; text-align:center"><p id="b22" style="text-align:center"></p></td>
                                   <td style="border-bottom:1px Ridge #dcdcdc; text-align:center"><p id="b24" style="text-align:center"></p></td>
                                   <td style="border-bottom:1px Ridge #dcdcdc; text-align:center"><p id="b25" style="text-align:center"></p></td>
                                </tr>
                                <tr>
                                	<td colspan="1" style="text-align:center; height:70px; align:center"><div style="margin-left:70px; text-align:center; width:100px; height:40px; border:1.5px solid green; border-radius: 14px; line-height:35px;">당담업무</div></td>
                                	<td style="text-align:left "><p style="font-size:18px"><p id="b26"></p></td>
                                </tr>
                                
                           </table>
                           <br><br>

                           <table>
                            <tr>
                                <td><h3>자기소개서&nbsp;&nbsp;</h3></td>
                            </tr>
                        </table>
						<br>
                        <table class="self_intro_table66" style="background-color:#ECF0F7">
                            <caption>자기소개서</caption>
                            <tbody>
                                <tr> 
                                    <th class="re_title66">제목</th>
                                    <td class="re_title_area66">
                                    	
                                        <h3 style="font-weight:bold;" id="b27" ></h3>
                                      
                                    </td>
                                </tr>
                                <tr>
                                    <th class="re_contents66">내용</th>
                                    <td class="re_contents_area66" height="500px" width="1100px" align="left" valign="top">
                                    
                                        <p id="b28"></p>
                                    
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br><br><br>
                        <table>
                            <tr>
                                <td><h3>희망조건&nbsp;&nbsp;</h3></td>
                            </tr>
                        </table>
						<br> 
                        <table style="border: 1px solid #7D7777" width="1100">

                            <tbody>
                                <tr>
                                    <td class="terms_desired166"><div style="text-align:center; width:100px; height:40px; border:1.5px solid green; border-radius: 14px; line-height:35px;">관심지역</div></td>
                                    
                                    <td class="terms_desired166" width="850px"><p id="b29"></p></td>
                                    
                                </tr>
                                <tr>
                                    <td class="terms_desired266"><div style="text-align:center; width:100px; height:40px; border:1.5px solid green; border-radius: 14px; line-height:35px;">관심직종</div></td>
                                    <td class="terms_desired266"><p id="b30"></p></td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="resume_check66">
                            <div class="resume_check_ch66">
                                <span>위에 모든 기재 사항은 사실과 다름없음을 확인합니다.</span>
                            </div>
                            <p class="resume_date66">
                                <span>작성일</span>
                                <span id="time-result"class="resume_date_date66"></span> |
                                <span>작성자</span>
                                <span class="resume_date_user66" id="b32"><%= loginUser.getMemberName()%></span>
                            </p>
                        </div>
                        <div class="sub_btn66">
                            <button id="backButton66" style="text-align:center; align:center;">뒤로</button> 
                        </div>
                    </div> 
                </fieldset>
               </div>
         </div>
        <main id="main">
        
            <form class="resume_form" action="<%= request.getContextPath() %>/insert.res"  name="" method="post" encType="multipart/form-data">
                <fieldset>
                    <legend>이력서등록</legend>
                    <div class="form_wrap">
                        <div class="resume_title">
                            <label for="_title">이력서 제목</label>
                            <input type="text" name="resume_title" id="_title" size="150" placeholder="제목은 최대 60자까지 입력 가능합니다." />
                        </div>
                        <div class="basic_info">
                            <div class="basic_info_title">기본정보</div>
                            <div class="basic_info_contents">
                                <p class="uname">
                                    <label for="uname">이름</label>
                                    <!--★★★이름 자동입력 바꾸기-->
                                    <input type="text" name="rname" id="uname" placeholder="이순신" size="45" value="<%= loginUser.getMemberName()%>" readonly/>
                                </p>
                                <p class="uphoto">
                                    <label for="uphoto"><img id="rphoto" width="125px" height="163px"src="../../static/images/user/photo_icon_03.png" alt="사진추가"></label>
                                    <input type="file" name="rphoto" id="uphoto"onchange="loadImg(this, 1);" />
                                </p>

                                <p class="b_day">
                                    <label for="b_day">생년월일</label>
                                    <input type="text" name="birth_date" id="b_day" placeholder="예시)19920423" size="45" />
                                </p>
                                <p class="email">
                                    <label for="uemail">이메일</label>
                                    <input type="text" name="remail" id="uemail" size="45" />
                                </p>
                                <p class="phone_num">
                                    <label for="phone_num">휴대폰</label>
                                    <!--★★★폰번호 자동입력 바꾸기-->
                                    <input type="text" name="rphone" id="phone_num"  size="45" value="<%= loginUser.getPhone()%>" readonly />
                                    <a href="#" onclick="changeInfo()">회원정보수정</a>
                                </p>
                                <p class="uaddress">
                                    <label for="uaddress">주소</label>
                                    <input type="text" class="uaddress_01" name="raddress1" id="uaddress"  value=""placeholder="주소 입력" size="45" />
                                 
                                    <button type="button" class="uaddress_01" name="raddress1" onclick="openDaumZipAddress();">주소검색</button>
                                    
                                    <input type="text" class="remain_addy" name="raddress2" id="remain_addy" value="" size="70" />
                                    
                                </p>
                                <p class="gender">
                                	 <label for="resume_public">성별</label>
                                <input type="radio" id="resume_public" class="gender"name="gender" value="남"> 남성
                                <input type="radio" id="resume_public" class="gender"name="gender" value="여"class="resume_public_radio"> 여성
                                </p>
                            </div>
                        </div>
   <script>
    function loadImg(value, num) {
      if(value.files && value.files[0]) {
         var reader = new FileReader();
         
         reader.onload = function(e) {
            console.log(e.target.result);
            switch(num) {
               case 1 : $("#rphoto").attr("src", e.target.result); $("#image").attr("src", e.target.result); break;
               
            }
            
         } 
         
         reader.readAsDataURL(value.files[0]);
      }
   }
            
    </script>
                        
                        <div class="edu_bg">
                           <div class="edu_bg_title">
                               <span class="edu_bg_title_01">학력사항</span>
                               <span class="edu_bg_title_02">최종학력을 선택해 주세요.</span>
                           </div>
                           <div class="edu_bg_contents">
                               <div class="highest_edu">
                                    <ul id="school_Kind">
                                        <li class="sch1" id="sch0">초등학교졸업</li>
                                        <li class="sch2" id="sch0">중학교졸업</li>
                                        <li class="sch3" id="sch0">고등학교졸업</li>
                                        <li class="sch4" id="sch0" value="4">대학(원) 이상 졸업</li>
                                    </ul>
                                </div>
                                <div class="uni_info" id="school1">
                                    <p class="high_sch_info_title">초등학교 정보 입력</p>
                                    <p class="">
                                        <label for="high_name" class="uni_name">학교명</label>
                                        <input type="text" name="sch_name" class="high_name" id="sch_name1" placeholder="학교명 입력" size="45px" />
                                    </p>
                                    
                                    
                                    <p>
                                        <label for="uni_period">재학기간</label>
                                        <input type="date" name="sch_enroll_date" id="uni_period" />
                                        <select name="" id="" class="uni_select">
                                            <option value="입학">입학</option>
                                        </select> ~
                                        <input name="sch_graduate_date" type="date" class="uni_end_date" />
                                        <select  id="" class="uni_select" name="graduate_type">
                                        	<option value="">선택</option>
                                            <option value="졸업">졸업</option>
                                            <option value="자퇴">자퇴</option>
                                        </select>
                                    </p>
                                </div>
                                
                                <div class="uni_info" id="school2">
                                    
                                    <p class="high_sch_info_title">중학교 정보 입력</p>
                                    <p class="">
                                        <label for="high_name" class="uni_name">학교명</label>
                                        <input type="text" name="sch_name" class="high_name" id="sch_name2" placeholder="학교명 입력" size="45px" />
                                    </p>
                                    
                                    
                                    <p>
                                        <label for="uni_period">재학기간</label>
                                        <input type="date" name="sch_enroll_date" id="uni_period" />
                                        <select name="" id="" class="uni_select">
                                            <option value="입학">입학</option>
                                        </select> ~
                                        <input name="sch_graduate_date" type="date" class="uni_end_date" />
                                        <select  id="" class="uni_select" name="graduate_type">
                                        	<option value="">선택</option>
                                            <option value="졸업">졸업</option>
                                            <option value="자퇴">자퇴</option>
                                        </select>
                                    </p>
                                </div>
                                
                                
                                <div class="uni_info" id="school3">
                                    <p class="high_sch_info_title">고등학교 정보 입력</p>
                                    <p class="">
                                        <label for="high_name" class="uni_name">학교명</label>
                                        <input type="text" name="sch_name" class="high_name" id="sch_name3" placeholder="학교명 입력" size="45px" />
                                    </p>
                                    
                                    <p>
                                        <label for="high_major">전공계열</label>
                                        <select class="high_major" id="high_major1" name="major">
                                            <option value="volvo">전공계열 선택</option>
                                            <option value="문과계열">문과계열</option>
                                            <option value="이과계열">이과계열</option>
                                            <option value="전문(실업계)">전문(실업계)</option>
                                            <option value="예체능계">예체능계</option>
                                            <option value="특성화/마이스터고">특성화/마이스터고</option>
                                            <option value="특수목적고">특수목적고</option>
                                        </select>
                                    </p>
                                    <p>
                                        <label for="uni_period">재학기간</label>
                                        <input type="date" name="sch_enroll_date" id="uni_period" />
                                        <select name="" id="" class="uni_select">
                                            <option value="입학">입학</option>
                                        </select> ~
                                        <input name="sch_graduate_date" type="date" class="uni_end_date" />
                                        <select  id="" class="uni_select" name="graduate_type">
                                        	<option value="">선택</option>
                                            <option value="졸업">졸업</option>
                                            <option value="자퇴">자퇴</option>
                                        </select>
                                    </p>
                                </div>
                                
                                
                                
                                <div class="uni_info" id="school4">
                                   <p class="high_sch_info_title">고등학교 정보 입력</p>
                                    <p class="">
                                        <label for="high_name" class="uni_name">학교명</label>
                                        <input type="text" name="sch_name" class="high_name" id="sch_name4" placeholder="학교명 입력" size="45px" />
                                    </p>
                                    
                                    <p>
                                        <label for="high_major">전공계열</label>
                                        <select class="high_major" id="high_major2" name="major">
                                            <option value="volvo">전공계열 선택</option>
                                            <option value="문과계열">문과계열</option>
                                            <option value="이과계열">이과계열</option>
                                            <option value="전문(실업계)">전문(실업계)</option>
                                            <option value="예체능계">예체능계</option>
                                            <option value="특성화/마이스터고">특성화/마이스터고</option>
                                            <option value="특수목적고">특수목적고</option>
                                        </select>
                                    </p>
                                    
                                    
                                    <p>
                                        <label for="uni_period">재학기간</label>
                                        <input type="date" name="sch_enroll_date" id="sch_period" class="sch_start_date"/>
                                        <select name="sch_enroll_date" id="" class="uni_select">
                                            <option value="">입학</option>
                                        </select> ~
                                        <input name="sch_graduate_date" type="date" class="uni_end_date" id="sch_end_date"/>
                                        <select  id="sch_select" class="uni_select" name="graduate_type">
                                        	<option value="">선택</option>
                                            <option value="졸업">졸업</option>
                                            <option value="자퇴">자퇴</option>
                                        </select>
                                    </p>
                                    
                                    
                                    <br>
                                    <p class="uni_info_title">대학·대학원 정보 입력</p>
                                    <p>
                                        <label class="sel_uni" for="sel_uni">대학</label>
                                        <select name="uni_type" id="sel_uni">
                                        <option value="volvo">선택</option>
                                           <option value="대학교(2,3년)">대학교(2,3년)</option>
                                            <option value="대학교(4년)">대학교(4년)</option>
                                            <option value="대학원(석사)">대학원(석사)</option>
                                            <option value="대학원(박사)">대학원(박사)</option>
                                        </select>
                                    </p>
                                    <p>
                                        <label for="uni_name" class="uni_name">학교명</label>
                                        <input name="uni_name" type="text" id="uni_name" size="45" />
                                    </p>
                                    <p>
                                        <label for="uni_area" class="uni_area">지역</label>
                                        <select name="uni_area" id="uni_area">
                                              <option value="volvo">지역 선택</option>
                                               <option value="서울">서울</option>
                                                <option value="경기">경기</option>
                                                <option value="인천">인천</option>
                                                <option value="부산">부산</option>
                                                <option value="대구">대구</option>
                                                <option value="광주">광주</option>
                                                <option value="대전">대전</option>
                                                <option value="울산">울산</option>
                                                <option value="세종">세종</option>
                                                <option value="강원">강원</option>
                                                <option value="경남">경남</option>
                                                <option value="경북">경북</option>
                                                <option value="전남">전남</option>
                                                <option value="전북">전북</option>
                                                <option value="충남">충남</option>
                                                <option value="충북">충북</option>
                                                <option value="제주">제주</option>
                                        </select>
                                    </p>
                                    <p>
                                        <label for="uni_major" class="uni_major">전공</label>
                                        <input type="text" id="uni_major1" placeholder="주전공" readonly />
                                        <select class="uni_major_select" name="uni_major" id="uni_major">
                                            
                                               <option value="volvo">전공계열 선택</option>
                                               <option value="어문학">어문학</option>
                                                <option value="영어/영문">영어/영문</option>
                                                <option value="중어/중문">중어/중문</option>
                                              <option value="일어/일문">일어/일문</option>
                                                <option value="국어/국문">국어/국문</option>
                                                <option value="인문과학">인문과학</option>
                                                <option value="사회과학">사회과학</option>
                                                <option value="상경계열">상경계열</option>
                                                <option value="경제/경영">경제/경영</option>
                                                <option value="회계학">회계학</option>
                                                <option value="법학계열">법학계열</option>
                                                <option value="사범계열">사범계열</option>
                                                <option value="종교학">종교학</option>
                                                <option value="생할과학">생할과학</option>
                                                <option value="예/체능">예/체능</option>
                                                <option value="자연과학계열">자연과학계열</option>
                                                <option value="농수산/해양/축산">농수산/해양/축산</option>
                                                <option value="수학/통계학">수학/통계학</option>
                                                <option value="물리/천문/기상학">물리/천문/기상학</option>
                                                <option value="화학/생물">화학/생물</option>
                                                <option value="공학계열">공학계열</option>
                                                <option value="전기/전자/정보통신공학">전기/전자/정보통신공학</option>
                                                <option value="컴퓨터/시스템 공학">컴퓨터/시스템 공학</option>
                                                <option value="금속/비금속공학">금속/비금속공학</option>
                                                <option value="생명/화학/환경/바이오">생명/화학/환경/바이오</option>
                                                <option value="도시/토목/건축공학">도시/토목/건축공학</option>
                                                <option value="에너지/원자력공학">에너지/원자력공학</option>
                                                <option value="산업/자동차/우주공학">산업/자동차/우주공학</option>
                                                <option value="기계/조선/항공공학">기계/조선/항공공학</option>
                                                <option value="신소재/재료/섬유공학">신소재/재료/섬유공학</option>
                                                <option value="식품/유전/안전공학">식품/유전/안전공학</option>
                                                
                                                
                                                
                                        </select>
                                        <input type="text" name="uni_major_name" id="uni_major_name" class="uni_major_input" placeholder="전공학과 입력" size="45" />
                                    </p>
                                    <p>
                                        <label for="uni_period">재학기간</label>
                                        <input name="uni_enroll_date" type="date" id="uni_period" class="uni_start_date"/>
                                        <select id="uni_select1" class="uni_select" name="uni_enroll_type">
                                            <option value="입학">입학</option>
                                            <option value="편입">편입</option>
                                        </select> ~
                                        
                                        <input name="uni_graduate_date" type="date" class="uni_end_date" id="uni_end_date" />
                                        <select id="uni_select2" class="uni_select" name="uni_graduate_type">
                                        <option value="">선택</option>
                                            <option value="졸업">졸업</option>
                                            <option value="재학중">재학중</option>
                                            <option value="휴학중">휴학중</option>
                                            <option value="수료">수료</option>
                                            <option value="중퇴">중퇴</option>
                                            <option value="자퇴">자퇴</option>
                                            <option value="졸업예정">졸업예정</option>
                                        </select>
                                    </p>
                                    <p>
                                        <label class="uni_credit_label" for="uni_credit">학점</label>
                                        <input type="text" name="uni_score" id="uni_credit" class="uni_credit" placeholder="학점입력" />
                                        <select name="uni_score_choice"class="credit_limit_select" id="credit_limit">
                                           <option value="volvo">기준학점선택</option>
                                           <option value="4.0">4.0</option>
                                           <option value="4.3">4.3</option>
                                           <option value="4.5">4.5</option>
                                           <option value="5.0">5.0</option>
                                           <option value="7.0">7.0</option>
                                           <option value="100">100</option>
                                            
                                        </select>
                                    </p>
                                    
                                </div>
                           </div>
                        </div>
                        <br><br>
                        <div class="Career_details">
                                <div class="Career_details_title">경력사항</div>
                                <div class="Career_details_contents">
                                    <div class="sel_career">
                                        <ul>
                                            <li class="newcareer">신입</li>
                                            <li class="oldcareer">경력</li>
                                        </ul>
                                    </div>
                                    <div class="Career_details_inpo" id="career1">
                                        <p>
                                            <label class="cor_name" for="cor_name">회사명</label>
                                            <input type="text" name="company_name" id="cor_name" placeholder="회사명입력" size="45" />
                                        </p>
                                        <p>
                                            <label for="term_office">재직기간</label>
                                            <input type="date" class="term_office_input" name="company_in_year" id="company_in_year" /> ~
                                            <input type="date" class="term_office_input" name="company_out_date" id="company_out_date" />
                                        </p>
                                        <p class="Career_position">
                                            <label for="sel_position" class="sel_position">직급 직책</label>
                                            <input type="text" name="job_level" id="sel_position" placeholder="직급 직책 선택" size="50">
                                            <!-- <a href="#">선택</a> -->
                                            <input type="text" name="career_year" class="" id="careeryear" /> 년차
                                        </p>
                                        <p class="Career_type">
                                            <label for="job_type" class="job_type">직종</label>
                                            <input type="text" id="career_kind" name="career_kind" placeholder="직종 선택" size="50">
                                            <!-- <a href="#">선택</a> -->
                                        </p>
                                        <p>
                                            <label for="work_place">근무지역</label>
                                            <select name="career_area" id="work_place">
                                               <option value="volvo" disabled>근무지역 선택</option>
                                               <option value="">선택</option>
                                                <option value="서울">서울</option>
                                                <option value="경기">경기</option>
                                                <option value="인천">인천</option>
                                                <option value="부산">부산</option>
                                                <option value="대구">대구</option>
                                                <option value="광주">광주</option>
                                                <option value="대전">대전</option>
                                                <option value="울산">울산</option>
                                                <option value="세종">세종</option>
                                                <option value="강원">강원</option>
                                                <option value="경남">경남</option>
                                                <option value="경북">경북</option>
                                                <option value="전남">전남</option>
                                                <option value="전북">전북</option>
                                                <option value="충남">충남</option>
                                                <option value="충북">충북</option>
                                                <option value="제주">제주</option>
                                            </select>
                                        </p>
                                        <p>
                                            <label for="_dep">근무부서</label>
                                            <input type="text" name="career_dept" id="_dep" class="_dep" placeholder="근무부서입력" size="45" />
                                        </p>
                                        <p>
                                            <label for="assign_task">담당업무</label>
                                            <input type="text" name="task" id="assign_task" class="assign_task" placeholder="담당업무입력" size="45" />
                                        </p>
                                    </div>
                                </div>
                        </div>
                        <br><br>
                        <div class="self_intro">
                                <div class="self_intro_title">
                                    <span class="self_intro_title_01">자기소개서</span>
                                    <span class="self_intro_title_02">자기소개를 통해 본인을 표현하세요.</span>
                                </div>
                                
                                <table class="self_intro_table">
                                    <caption>자기소개서</caption>
                                    <tbody>
                                        <tr>
                                            <th class="re_title" >제목</th>
                                            <td class="re_title_area">
                                                <input name="introduce_title" id="ititle" type="text" placeholder="자소서 제목" size="150">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="re_contents">내용</th>
                                            <td>
                                                <textarea name="introduce_content" class="re_contents_area" id="icontent" cols="151" rows="20" placeholder="자소서 내용"></textarea>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                                
                                <div class="letter_count">
                                    <span>총 글자수</span>
                                    <span class="letter_red">0 </span>/
                                    <span>1000 </span>자 /<span class="letter_red1">0</span>byte
                                </div>
                                
                        </div>
                        <br><br>
                        <div class="de_condition">
                            <div class="de_condition_title">
                                   희망조건 선택
                            </div>
                            <div class="de_condition_contents">
                                <p>
                                    <label for="interest_area">관심근무지역</label>
                                    <select name="interest_area" id="interest_area">
                                                 <option value="volvo">관심근무지역 선택</option>
                                              <option value="서울">서울</option>
                                                <option value="경기">경기</option>
                                                <option value="인천">인천</option>
                                                <option value="부산">부산</option>
                                                <option value="대구">대구</option>
                                                <option value="광주">광주</option>
                                                <option value="대전">대전</option>
                                                <option value="울산">울산</option>
                                                <option value="세종">세종</option>
                                                <option value="강원">강원</option>
                                                <option value="경남">경남</option>
                                                <option value="경북">경북</option>
                                                <option value="전남">전남</option>
                                                <option value="전북">전북</option>
                                                <option value="충남">충남</option>
                                                <option value="충북">충북</option>
                                                <option value="제주">제주</option>
                                    </select>
                                    
                                </p>
                                <p>
                                    <label class="int_job" for="">관심직종</label>
                                    <select name="interest_job1" id="interest_job1">
                                       <option value="volvo">관심직종 선택</option>
                                        <option value="요리/서빙">요리/서빙</option>
                                        <option value="간호/의료">간호/의료</option>
                                        <option value="생산/건설">생산/건설</option>
                                        <option value="사무/경리">사무/경리</option>
                                        <option value="운전/배달">운전/배달</option>
                                        <option value="상담/영업">상담/영업</option>
                                        <option value="매장관리">매장관리</option>
                                        <option value="교사/강사">교사/강사</option>
                                        <option value="일반/기타">일반/기타</option>
                                    </select>
                                    <select name="interest_job2" id="interest_job2">
                                        <option value="volvo">관심직종 선택</option>
                                        <option value="요리/서빙">요리/서빙</option>
                                        <option value="간호/의료">간호/의료</option>
                                        <option value="생산/건설">생산/건설</option>
                                        <option value="사무/경리">사무/경리</option>
                                        <option value="운전/배달">운전/배달</option>
                                        <option value="상담/영업">상담/영업</option>
                                        <option value="매장관리">매장관리</option>
                                        <option value="교사/강사">교사/강사</option>
                                        <option value="일반/기타">일반/기타</option>
                                    </select>
                                    <select name="interest_job3" id="interest_job3">
                                        <option value="volvo">관심직종 선택</option>
                                        <option value="요리/서빙">요리/서빙</option>
                                        <option value="간호/의료">간호/의료</option>
                                        <option value="생산/건설">생산/건설</option>
                                        <option value="사무/경리">사무/경리</option>
                                        <option value="운전/배달">운전/배달</option>
                                        <option value="상담/영업">상담/영업</option>
                                        <option value="매장관리">매장관리</option>
                                        <option value="교사/강사">교사/강사</option>
                                        <option value="일반/기타">일반/기타</option>
                                    </select>
                                </p>
                            </div>
                        </div>
                        <br><br>
                        <div class="resume_public">
                            <div class="resume_public_title">
                                	이력서 공개여부
                            </div>
                            <div class="resume_public_contents">
                                <label for="resume_public">이력서 공개여부</label>
                                <input type="radio" id="resume_public" name="resume_open_yn" value="Y"> 공개
                                <input type="radio" id="resume_public" name="resume_open_yn" value="N"class="resume_public_radio"> 비공개   
                                <p>※이력서를 공개하시면 입사지원을 하지 않아도 인사담당자에게 직접 연락받을 수 있습니다.</p>
                            </div>
                        </div>
                        <div class="resume_check">
                            <div class="resume_check_ch">
                                <input type="checkbox" id="resume_check_ch" name="resume_check_ch" />
                                <span>위에 모든 기재 사항은 사실과 다름없음을 확인합니다.</span>
                            </div>
                            <p class="resume_date">
                                <span>작성일</span>
                                <span id="time-result"class="resume_date_date"></span> |
                                <span>작성자</span>
                                <span class="resume_date_user"><%= loginUser.getMemberName()%></span>
                            </p>
                        </div> 
                        
                        <div id="school_kind_area">
                        <input type="radio" id="schradio1" name="schradio" class="schradio5" value="초등학교">
                        <input type="radio" id="schradio2" name="schradio" class="schradio5" value="중학교">
                       <input type="radio" id="schradio3" name="schradio" class="schradio5" value="고등학교">
                        <input type="radio" id="schradio4" name="schradio" class="schradio5" value="대학교">
                       </div>
                       
                       <div id="career_kind_area">
                        <input type="radio" id="careerradio1" class="careerradio4" name="careerradio" value="N">
                        <input type="radio" id="careerradio2" class="careerradio4" name="careerradio" value="Y">
                       </div>
                       
                        
                        <div class="sub_btn">
                            <input type="button" id="resume_view" style="text-align:center;" value="이력서 미리보기">
                            <input type="button" id="resume_save" style="text-align:center;" value="이력서 임시저장">
                            <input type="button" id="resume_commit" style="text-align:center;" value="이력서 등록">
                        </div>
                    </div>
                </fieldset>
            </form>
        </main>
        <footer><br><br></footer>
    </div>
    
   
    
    
     <script>
          $(function(){
           /* $("#resume_view").click(function(event){
            	

                    
                    event.preventDefault();
                    // Get form
                    var form = $('.resume_form')[0];

            	    // Create an FormData object 
                    var data = new FormData(form);
            	    
                    $("#resume_view").prop("disabled", true);
                    
                    $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        url: "/res.preveal",
                        data: data,
                        processData: false,
                        contentType: false,
                        cache: false,
                        timeout: 600000,
                        success: function (data) {
                        	alert("complete");
                            $("#resume_view").prop("disabled", false);
                            console.log(data);
                            $("#_title").html("value", "#title");
                        },
                        error: function (e) {
                            console.log("ERROR : ", e);
                            $("#resume_view").prop("disabled", false);
                            console.log(data);
                            alert("fail");
                        }
                    });

                }); */
                    
              	 
            
            
            $("#school_kind_area").hide();
            $("#career_kind_area").hide();
            
         $(".sch1").click(function(){
            $(".sch2").css('background','#FFFFFF');
            $(".sch3").css('background','#FFFFFF');
            $(".sch4").css('background','#FFFFFF');
             $("#schradio1").click();
             $(".sch1").css('background','#2FA599');
         });         
            $(".sch2").click(function(){
               $(".sch1").css('background','#FFFFFF');
            $(".sch3").css('background','#FFFFFF');
            $(".sch4").css('background','#FFFFFF');
              $("#schradio2").click();
              $(".sch2").css('background','#2FA599');
            }); 
           $(".sch3").click(function(){
              $(".sch1").css('background','#FFFFFF');
            $(".sch2").css('background','#FFFFFF');
            $(".sch4").css('background','#FFFFFF');
              $("#schradio3").click();
              $(".sch3").css('background','#2FA599');
          });
         $(".sch4").click(function(){
            $(".sch1").css('background','#FFFFFF');
            $(".sch2").css('background','#FFFFFF');
            $(".sch3").css('background','#FFFFFF');
            $("#schradio4").click();
            $(".sch4").css('background','#2FA599');
         });
         
         
         $(".newcareer").click(function(){
            $(".oldcareer").css('background','#FFFFFF');
            $("#careerradio1").click();
            $(".newcareer").css('background','#2FA599');
         });
         
         $(".oldcareer").click(function(){
            $(".newcareer").css('background','#FFFFFF');
            $("#careerradio2").click();
            $(".oldcareer").css('background','#2FA599');
            
         });
         
         
         $("#resume_commit").click(function(){
            var title = $("#_title").val();
            var b_day = $("#b_day").val();
            var uaddress = $("#uaddress").val();
            var remain_addy = $("#remain_addy").val();
            var schradio5 = $("input[name=schradio]:checked").val();
            var resume_check_ch = $("input[name=resume_check_ch]:checked").val();
            var careerradio4 = $("input[name=careerradio]:checked").val();
            var interest_job1 = $("#interest_job1").val();
            var interest_job2 = $("#interest_job2").val();
            var interest_job3 = $("#interest_job3").val();
            var interest_job4 = interest_job1 + interest_job2 + interest_job3;
            var interest_area = $("#interest_area").val();
            var resume_public = $("input[name=resume_open_yn]:checked").val();
            
            var careeryear = $("#careeryear").val();
            var uni_credit = $("#uni_credit").val();
            var credit_limit = $("#credit_limit").val();
            
            var sch_name1 =$("#sch_name1").val();
            var sch_name2=$("#sch_name2").val();
            var sch_name3 =$("#sch_name3").val();
            var sch_name4 =$("#sch_name4").val();
            
            
            var sch_name4 =$("#sch_name4").val();
            
            var sch_name = sch_name1 + sch_name2 + sch_name3 + sch_name4;
            
            var major1 = $("#high_major1").val();
            var major2 = $("#high_major2").val();
            var major3 = $("#high_major3").val();
            var major4 = $("#high_major4").val();
            
            var sch_major = major1 + major2 + major3 + major4;
            
            var uni_name = $("#uni_name").val();
            var uni_area = $("#uni_area").val();
            var uni_major = $("#uni_major").val();
            var uni_major_name= $("#uni_major_name").val();
           
            /* var uni_select1 = $("#uni_select1").val();
            var uni_select2 = $("#uni_select2").val(); */
            var uni_select= $(".uni_select").val();
            
            
            var graduate = $("")
            var cor_name = $("#cor_name").val();
            var sel_position = $("#sel_position").val();
            var careeryear = $("#careeryear").val();
            var career_kind = $("#career_kind").val();
            var work_place = $("#work_place").val();
            var _dep = $("#_dep").val();
            var assign_task =$("#assign_task").val();
            
            
           
            if(title === "") {
               alert("이력서 제목을 입력해주세요");
            
            } else if(b_day === "") {
               alert("생일을 입력해주세요");
               
            } else if(uaddress === "" && remain_addy === "") {  
               alert("주소를 입력해주세요.");
            } else if(schradio5 === undefined) {
               alert("학력사항을 선택해주세요");
            } else if(sch_name ===""){
               alert("학교명을 입력해주세요");
            } else if(schradio5 === "고등학교" && major1 === null){
               alert("전공계열을 입력해주세요");
            } else if(schradio5 === "대학교" && major2 === null){
               alert("전공계열을 입력해주세요");
            } else if(schradio5 === "대학교" && uni_name ===""){
               alert("학교(대학)명을 입력해주세요");
            } else if(schradio5 === "대학교" && uni_area === null){
               alert("대학지역을 선택해주세요");
            } else if(schradio5 === "대학교" && uni_major === null){
               alert("전공계열을 선택해주세요");
            } else if(schradio5 === "대학교" && uni_major_name === ""){
               alert("전공학과를  입력해주세요");
            
               
            
               
            } else if(careerradio4 === undefined){
               alert("경력사항을 선택해주세요");
            } else if(careerradio4 ==="Y" && cor_name === ""){   
               alert("회사이름을 입력해주세요");
            } else if(careerradio4 ==="Y" &&sel_position === ""){
               alert("직급 직책을 입력해주세요");
            } else if(careerradio4 ==="Y" &&careeryear === ""){
               alert("년차를 입력해주세요");
            } else if(careerradio4 ==="Y" &&career_kind === ""){
               alert("직종을 입력해주세요");
            } else if(careerradio4 ==="Y" &&work_place === null){
               alert("근무지역을 입력해주세요");
            } else if(careerradio4 ==="Y" &&_dep === ""){
               alert("근무부서를 입력해주세요");
            } else if(careerradio4 ==="Y" && assign_task === ""){
               alert("당담업무을 입력해주세요");
            } else if(interest_area === null) {
               alert("관심근무지역을 선택해주세요");
            } else if(interest_job4 === 0) {
               alert("관심직종을 선택해주세요");
            } else if(resume_public === undefined) {
               alert("이력서  공개여부를 선택해주세요.");
            } else if(resume_check_ch === undefined) {
               alert("약관에 동의해주세요.");
            } else {
               $(".resume_form").attr("action" , "<%= request.getContextPath() %>/insert.res");
               $(".resume_form").submit();
            }
         });
         
         
         $("#resume_save").click(function(){
            $(".resume_form").attr("action" , "<%= request.getContextPath() %>/res.sav");
            $(".resume_form").submit();
         });
         });
         
         
         
            
      </script>
      <script>
      
      $("#resume_view").click(function(){
    	  
    	  
    	  
   		var gender = $(".gender").val();
   		var email = $("#uemail").val();
      var title = $("#_title").val();
      var b_day = $("#b_day").val();
      var uaddress = $("#uaddress").val();
      var remain_addy = $("#remain_addy").val();
      
      var adrress = uaddress + remain_addy 
      var schradio5 = $("input[name=schradio]:checked").val();
      var resume_check_ch = $("input[name=resume_check_ch]:checked").val();
      var careerradio4 = $("input[name=careerradio]:checked").val();
      var interest_job1 = $("#interest_job1").val();
      var interest_job2 = $("#interest_job2").val();
      var interest_job3 = $("#interest_job3").val();
      
      var interest_area = $("#interest_area").val();
      var resume_public = $("input[name=resume_open_yn]:checked").val();
      var sch_select = $("#sch_select").val();
      var careeryear = $("#careeryear").val();
      var uni_credit = $("#uni_credit").val();
      var credit_limit = $("#credit_limit").val();
      
      var score = uni_credit + "/" + credit_limit;
      var sch_name1 =$("#sch_name1").val();
      var sch_name2=$("#sch_name2").val();
      var sch_name3 =$("#sch_name3").val();
      var sch_name4 =$("#sch_name4").val();
      
      var sch_name = sch_name1 + sch_name2 + sch_name3 + sch_name4;
      
      
      
      var major2 = $("#high_major2").val();
      
     console.log(interest_job1);
      
      /* if(interest_job1.equals("null")){
    	  interest_job1="";
      }
      if(interest_job2.equals("null")){
    	  interest_job2="";
      }
      if(interest_job3.equals(null)){
    	  interest_job3="";
      } */
      var interest_job4 = interest_job1 + "/" + interest_job2 +"/" + interest_job3;
      
      
      
      
      var uni_name = $("#uni_name").val();
      var uni_area = $("#uni_area").val();
      var uni_major = $("#uni_major").val();
      var uni_major_name= $("#uni_major_name").val();
      /* var uni_select1 = $("#uni_select1").val();
      var uni_select2 = $("#uni_select2").val(); */
      var uni_select= $(".uni_select").val();
      
      var sch_start_date = $(".sch_start_date");
      var sch_end_date = $("#sch_end_date");
      
      var sch_date = sch_start_date + "~" +sch_end_date
      var uni_select2 =$("#uni_select2").val();
      
      var uni_end_date =$("#uni_end_date").val();
      var uni_start_date =$(".uni_start_date").val();
      
      var uni_date = uni_start_date + "~" + uni_end_date;
      
      var graduate = $("")
      var cor_name = $("#cor_name").val();
      var sel_position = $("#sel_position").val();
      var careeryear = $("#careeryear").val();
      var career_kind = $("#career_kind").val();
      var work_place = $("#work_place").val();
      var _dep = $("#_dep").val();
      var assign_task =$("#assign_task").val();
      var icontent =$("#icontent").val();
      var ititle =$("#ititle").val();
      var company_out_date = $("#company_out_date").val();
      var company_in_year = $("#company_in_year").val();
      var company_date = company_out_date + "~" + company_in_year;
     /*  $("label[for = 'title' ].text(title); */
    
      var dept = _dep + "/" + sel_position;
      var img = 
     
      document.getElementById("title5").innerHTML = title;
      document.getElementById("b1").innerHTML = b_day;
      document.getElementById("b2").innerHTML = gender;
      document.getElementById("b3").innerHTML = email;
      /* document.getElementById("b4").innerHTML = title; */
      document.getElementById("b5").innerHTML = adrress;
      document.getElementById("b6").innerHTML = sch_date;
      /*document.getElementById("b7").innerHTML = 졸업날짜;*/
      document.getElementById("b8").innerHTML = sch_select;
      document.getElementById("b9").innerHTML = sch_name;
      document.getElementById("b10").innerHTML = major2;
      document.getElementById("b11").innerHTML = uni_date;
      /* document.getElementById("b12").innerHTML = 졸업날짜; */
      document.getElementById("b13").innerHTML = uni_select2;
      document.getElementById("b14").innerHTML = uni_name;
      document.getElementById("b15").innerHTML = uni_major_name;
      document.getElementById("b16").innerHTML =  score;
     /*  document.getElementById("b17").innerHTML = credit_limit; */
      document.getElementById("b18").innerHTML = careeryear;
      document.getElementById("b19").innerHTML = company_date;
      /*document.getElementById("b20").innerHTML = 퇴사연도;*/
      document.getElementById("b21").innerHTML = cor_name;
      document.getElementById("b22").innerHTML = dept;
      /* document.getElementById("b23").innerHTML = sel_position; */
      document.getElementById("b24").innerHTML = career_kind;
      document.getElementById("b25").innerHTML = work_place;
      document.getElementById("b26").innerHTML = assign_task;
      document.getElementById("b27").innerHTML = ititle;
      document.getElementById("b28").innerHTML = icontent;
      document.getElementById("b29").innerHTML = interest_area;
      document.getElementById("b30").innerHTML = interest_job4;
     /*  document.getElementById("b31").innerHTML = title; */
     /*  document.getElementById("b32").innerHTML = title; */
     
     
     
    /*  $("#userimg").attr("src",  $("#rhoto").attr("src")); */

     
	/* var a = jQuery('#image').attr("src");
	jQuery("#uphoto").attr("src", a);*/

  
      $(".searchModal").attr("style", "display:block");
      
      $("#backButton66").click(function(){
          $(".searchModal").attr("style", "display:none");
       });
      
      });
      </script>
    <script>
       $(function(){
           
          $("#school1").hide();
         $("#school2").hide();
         $("#school3").hide();
         $("#school4").hide();
         $("#career1").hide();
          
          
          $(".sch1").click(function(){
             $("#school1").show();
             $("#school2").hide();
             $("#school3").hide();
             $("#school4").hide();
             
          });
          $(".sch2").click(function(){
             $("#school2").show();
             $("#school1").hide();
             $("#school3").hide();
             $("#school4").hide();
          });
          $(".sch3").click(function(){
             $("#school3").show();
             $("#school1").hide();
             $("#school2").hide();
             $("#school4").hide();
          });
          $(".sch4").click(function(){
             $("#school4").show();
             $("#school1").hide();
             $("#school2").hide();
             $("#school3").hide();
          });
          $(".newcareer").click(function(){
             $("#career1").hide();
          });
          
          $(".oldcareer").click(function(){
             $("#career1").show();
          });
          
          
         
       });   
       
       var d = new Date();
       
       var currentDate = d.getFullYear() + "년 " + (d.getMonth() + 1) + "월 " + d.getDate() + "일 ";
       
       
       var result = document.getElementById("time-result");
       
       
       result.innerHTML = currentDate;
       
       
    </script>
    
    <script>
    
    $("select option[value*='volvo']").prop('disabled',true);
    
    
     function openDaumZipAddress(){
      new daum.Postcode({
         oncomplete:function(data) {

            $("#uaddress").val(data.address);
            $("#remain_addy").focus();

            console.log(data);
         }
      }).open();
      
   }
   </script>
   
   
   <script>
   $('.re_contents_area').keyup(function (e){
      var content = $(this).val();
      
      $('.letter_red').html(content.length);
      
      
      if(content.length > 1000){
         alert("최대 1000자 까지 입력 가능합니다.");
         $(this).val(content.Substring(0,1000));
         $('.letter_red').html("1000");
      }
      
      
      var stringByteLength = 0;
      
      for(var i=0; i<content.length; i++) {
          if(escape(content.charAt(i)).length >= 4)
              stringByteLength += 3;
          else if(escape(content.charAt(i)) == "%A7")
              stringByteLength += 3;
          else
              if(escape(content.charAt(i)) != "%0D")
                  stringByteLength++;
      }
      
      $('.letter_red1').html(stringByteLength);
      
   });
   
   function changeInfo() {
         location.href = "<%=request.getContextPath()%>/views/common/E_UpdateInfoPage1.jsp";
      }
    </script>
  <script>
  /* $("#resume_view").click(function(){
     $(".searchModal").attr("style", "display:block");
  });
  
  $("#jdj_modal_close5").click(function(){
     $(".searchModal").attr("style", "display:none");
  });*/
  </script>
  
    
</body>
</html>