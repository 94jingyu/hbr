<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	.leftMenu a:hover{
   color: #2FA599;
   font-weight: bold;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head> 
<body>
	<aside class="leftMenu">
	           <div class="left leftblock" id="left1"><br>&nbsp;
	           <label class="myPage" onclick="userMypage();" style="font-size:20px;"><b>마이페이지</b></label><br><br>
	           <hr>
	           <br>
	              <ul>
	              <li>
	               	  <a href="#" style="font-size:18px;"><b>이력서관리</b></a>
	                  <a href="#" onclick="resumeWrite();" style="font-size:15px;">이력서 등록</a>
	                  <a href="#" id="jinhyeok1" onclick="resumeManage();" style="font-size:15px;">이력서 현황</a>
	               </li><br>
	               <hr><br>
	               <li>
	               	  <a href="#" style="font-size:18px;"><b>입사지원 관리</b></a>
	                  <a href="#" onclick="applyHistory();" style="font-size:15px;">입사지원목록</a>
	                  <a href="#" onclick="myresumeBusiness();"style="font-size:15px;">내 이력서 열람기업</a>
	                  <a href="#" onclick="resumeLimit();"style="font-size:15px;">이력서 열람 제한</a>
	               </li><br>
	             <hr><br>
	             <li>
	             	<a href="#" onclick="recruitBookmark();" style="font-size:18px;"><b>스크랩보금자리</b></a>
	             </li><br>
	             <hr><br>
	             <li> 
	             	<a href="#" onclick="recentViewRecruit();" style="font-size:18px;"><b>최근 본 보금자리</b></a>
	             </li><br>
	             <hr><br>
	             <li><a href="#" style="font-size:18px;"><b>회원정보 관리</b></a>
	                 <a href="#" onclick="changeInfo()" style="font-size:15px;">회원정보수정</a>
	                 <a href="#" onclick="del();" style="font-size:15px;">회원탈퇴</a>
	             </li><br>
	             <hr><br>
	             <li><a href="#" onclick="myinquiry();" style="font-size:18px;"><b>나의 문의내역</b></a>
	             </li>
	           </ul>  
	           </div>
	      </aside>
	       
	      <script>
	      function resumeWrite() {
	  		location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/resumeWrite.jsp";
	  		}
	      function resumeManage() {
	    	  location.href = "<%=request.getContextPath()%>/res.list";
	      }
	      function recruitBookmark() {
	    	  location.href = "<%=request.getContextPath()%>/views/user_TAEWON/user_recruitBookmark.jsp";
	      }
	      function myinquiry(){
	    	  <%-- location.href = "<%=request.getContextPath()%>/select.iq"; --%>
	    	  location.href = "<%=request.getContextPath()%>/views/user_TAEWON/user_MyInquiryHistory.jsp";
	      }
	      function applyHistory(){
	    	  location.href = "<%=request.getContextPath()%>/selectApplyList.ap";
	      }
	      function recentViewRecruit(){
	    	  location.href = "<%=request.getContextPath()%>/views/user_TAEWON/user_recentView.jsp";
	      }
	      function changeInfo() {
	      	location.href = "<%=request.getContextPath()%>/views/common/E_UpdateInfoPage1.jsp";
	      }
	      function del() {
	      	location.href = "<%=request.getContextPath()%>/views/common/E_DeletePage1.jsp";
	      }
	      function myresumeBusiness(){
	    	  location.href = "<%=request.getContextPath()%>/res.open";
	      }
	      function resumeLimit() {
	    	  location.href = "<%=request.getContextPath()%>/views/common/resumeLimit.jsp"
	      }
	      function userMypage() {
	  		
	  		location.href = "<%=request.getContextPath()%>/user.my";
	  	}
	      </script>
</body>
</html>