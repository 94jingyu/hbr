<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import = "com.kh.hbr.resume.model.vo.Resume,java.util.*"%>
<%
  ArrayList<Resume> list=(ArrayList<Resume>) session.getAttribute("resumeManage2");
 System.out.println("list : " + list);
 
%>
<% 
  	
	for(Resume r:list){
	if(r.getResume_title()==null){
		r.setResume_title("제목이 없습니다.");
	}
}
	%>
<!DOCTYPE html>         
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	<style>  
	* {
	text-align:left;
	margin: 0;
	padding: 0;
}



/* body {background-color: #fffde7;} */
#wrap {
	width: 1200px;
	margin: 0 auto;
}

/* 헤더 영역 */
.logo {
	float: left;
	width: 22%;
	height: 130px;
}

.toplogin {
	width: 100%;
	height: 50px;
	padding: 1%;
}

/* 상단메뉴바 */
.topMenu {
	/* display: inline-block; */
	width: 100%;
	height: 50px;
	/* background: #013252; */
	line-height: 30px; /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	vertical-align: middle; /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	text-align: center; /* 글씨 정렬을 가운데로 설정 */
	font-weight: bolder;
	font-size: 20px;
	color: #013252;
}

.topMenu>div {
	float: right;
}

.topMenu>ul {
	float: left;
	margin: 0;
	padding: 0;
	list-style: none;
}

.topMenu>ul>li {
	display: inline-block;
	padding: 0;
}

.topMenu a {
	display: block;
	text-decoration: none;
	padding: 10px 20px;
	color: black;
}

.topMenu a:hover {
	background: #013252;
	color: white;
}

/* 하이퍼텍스트 효과 */
a {
	text-decoration: none;
} /* 하이퍼텍스트 밑줄 효과 없애기 */

/* 광고 영역 */
.ads {
	width: 100%;
	height: 500px;
	clear: both;
}

/* 공고조회 및 로그인 영역 */
.b2_cotainer {
	width: 100%;
	height: 260px;
	background-color: #F0F0F0;
	text-align: center;
}

#selectJob {
	float: left;
	width: 70%;
	height: 250px;
	background-color: #039be5;
	display: inline-block;
}

#login {
	width: 30%;
	height: 250px;
	background-color: #0288d1;
	display: inline-block;
}

.fsearch_wp {
	width: 100%;
	border: 1px solid #e1e1e1;
	border-bottom: none;
	font-size: 14px;
	box-sizing: border-box;
}

/* 일반채용리스트 */
.normalJobList {
	width: 88%;
	font-size: 20px;
	font-weight: bold;
}

#normaljobList {
	font-size: 16px;
	font-weight: bold;
	border: none;
	border-bottom: 3px;
	border-bottom-color: black;
	background-color: lightGray;
	
}

/* footer 영역 */
footer {
	width: 100%;
	height: 200px;
	background-color: #ffb300;
}

/* 버튼 효과 */
.btns {
	float:right;
}

 #loginBtn, #memberJoinBtn, #logoutBtn, #changeInfo, #adminBtn {
	display: inline-block;
	text-align: center;
	
	
	height: 25px;
	width: 100px;
	border-radius: 5px;
	
	
}



/* nav */
.nav {
	width: 15%;
	height: 300px;
	border: solid 1px lightgray;
	margin-left: 5px;
}

/* 박다영 스타일 */
/* left 메뉴 */
   .leftblock {
      display: block;
   }
   
   #left2, #left3, #left4 {
      margin-top:10px;
   }
   
   #left1 {
      width: 180px;
      height:900px;
      margin-top:10px;
      align: left;
   }
   
   .myPage {
   	margin-left: 10px;
   }
   
   #left2 {
      height:290px;
   }
   
   #left3 {
      height:260px;
   }
   
   #left4 {
      height:250px;
   }
   
   /* left메뉴 가로선 */
   hr {
      width:90%;
      color:lightgray;
      size:2px;
      margin-left: 5%;
   }
   
   ul {
      list-style:none;
   }

.leftMenu{float: left; width: 20%; height: 580px; margin-left: 30px; margin-top: 10px;}
   
   /* 내용 부분 */
   /* section{width: 75%; height: 1500px; background-color: #ffca28;} */
   
   .leftMenu>ul {       
       margin: 10;
       padding: 0;
       list-style: none;
    }
    .leftMenu a {
       display: block;
       text-decoration: none;
       padding: 10px 20px;
       color: black;
     }
.left {
      border:2px solid rgb(192, 192, 192);
   }
	.wrap{width:1200px;margin:0 auto}
        
/*header*/
.mypage_wrap{width:930px;float:right}
.mypage_root{padding:0 0 20px 0}
.header_top{background:#013252;padding:20px;margin-bottom:20px}
.header_top p{color:#fff;font-size:20px;font-weight:bold}

/*main*/
/*사용자 정보*/
.user_info{background:#ecf0f7;padding:20px;overflow:hidden;margin-bottom:10px}
.user_img{padding:10px;float:left}

.user_info_detail{float:left;padding:10px;font-weight:bold;}
.user_info_detail li{padding:10px 10px 0 0;}
.user_info_detail .u_name_box{width:120px;border-bottom:2px solid #000;text-align:center;padding:0 0 4px 0;margin-bottom:10px}
.u_name_box .u_name{font-size:30px;color:#013252;}

.user_career_info{float:left;font-weight:bold;}
.user_career_info ul{overflow:hidden}
.user_career_info li{float:left;padding:10px 15px}
.user_career_info span{font-size:50px;color:#013252;}
.user_career_info .job_app span,.user_career_info .scr_cur span{display:block;text-align:center}
.user_career_info p{font-size:20px;text-align:center;padding-top:15px}
.user_career_info .resume_limit,.user_career_info .not_limit{font-size:30px;color:#c4c4c4}

/*입사지원현황*/
.current_job_app{clear:both;margin-bottom:10px;overflow:hidden}
.current_job_app_title{font-weight:bold;font-size:18px;padding:10px;float:left}
.job_app_border{color:#7f7f7f;}
.job_app_modify,.job_app_red,.job_app_limit{font-size:15px}
.job_app_red{color:#f00}
.job_app_limit{color:#c4c4c4}

.more_btn{float:right;padding:10px}
.more_btn a{font-weight:bold}

.current_job_app_contents{clear:both}
.current_job_app_contents table{border:1px solid #ddd;border-collapse:collapse}
.current_job_app_contents caption{position:absolute;left:-9999px;top:-9999px}
.current_job_app_contents thead{background:#ecf0f7}
.current_job_app_contents thead td{padding:10px}
.current_job_app_contents tbody td{padding:15px;border-bottom:1px solid #ddd}

/*이력서등록현황*/
.regist_sta{overflow:hidden;font-weight:bold;margin-bottom:10px}
.regist_sta_title{font-size:18px;padding:10px;float:left}
.regist_sta_contents{clear:both}
.regist_sta_border{color:#7f7f7f;}
.regist_sta_red{color:#f00;font-size:22px}
.regist_sta_modify,.regist_sta_limit{font-size:15px}
.regist_sta_limit{color:#c4c4c4}

/* .regist_sta_contents table{border:1px solid #ddd;} */
.regist_sta_contents caption{position:absolute;left:-9999px;top:-9999px}
.regist_sta_contents td{padding:15px;border-bottom:1px solid #ddd;border-top:1px solid #ddd}
.re_post{color:#f00;font-size:18px;}
.resume_title{padding:5px 0}
.resume_date{font-size:14px;font-weight:normal;padding:5px 0}
.resume_border{font-size:30px;color:#7f7f7f}

/*이력서등록버튼*/
.reg_btn{background:#ecf0f7;text-align:center;padding:15px;font-weight:bold;font-size:18px;height:25px;margin-bottom:20px}
.reg_btn a{display:block;height:100%;}
.resume_pre{color:#013252;}
.resume_reg{color:#76c0bc;}
.li{
	margin-left:5px;
}
#jinhyeok1{
color:#2FA599;
font-weight: bold;
}
	</style>
</head>
<body>
<div id="wrap">
		<!-- 로고 영역 -->
		<%@ include file="../common/E_user_menubar.jsp" %>
		<!-- <div class="logo">
			<img alt="logo.png" src="../../static/images/user/logo.png"
				width="250px" height="100%"
				style="margin-left: auto; margin-right: auto; display: block">
		</div>

		상단 로그정보 영역
		<div class="toplogin">
			<div class="btns" align="right">
				<div id="loginBtn" onclick="login();">로그인</div>
				<div id="memberJoinBtn" onclick="memberJoin();">회원가입</div>
			</div>
		</div>

		상단 메뉴바
		<nav class="topMenu" style="color:#013252">
			<ul>
				<li><a href="#">채용정보</a></li>
				<li><a href="#">인재정보</a></li>
				<li><a href="#">열린마당</a></li>
				<li><a href="#">취업박사</a></li>
				<li><a href="#">고객센터</a></li>
			</ul>
			<div>
				<a href="#">이력서등록</a>
			</div>
			<div>
				<a href="#" style="color:white; background-color:#013252">마이페이지</a>
			</div>
		</nav><br> -->
		<hr style="border: solid 2px #013252; width:100%; margin:0 auto"><br>
		
		<%@ include file="../user_JinHyeok/leftBar.jsp" %><br>
			
    <div class="mypage_wrap"> 
    
        <header> 
            <div class="mypage_root">
                <span>HOME</span>
                <span>> 마이페이지</span>
            </div>
            <div class="header_top">
                <p>이력서 등록 현황</p>
            </div>
            <br>
            <div style="text-align: left" class="li">
            
				<li class="li">이력서는 <b>최대 3개</b>까지 작성하여 등록 가능합니다.</li>
				<li class="li">대표 이력서 설정은 <b>1개</b>의 이력서만 가능합니다.</li>
				<li class="li">미완성 이력서는 이력서 등록이 완료되지 않은 상태에서 저장된 이력서입니다.</li>
				<li class="li">미완성 이력서로 입사지원 및 이력서 공개는 불가합니다.</li>
				<li class="li">"완성하기" 버튼을 클릭하여, 필수 항목을 모두 입력 후 등록을 완료해야 입사지원 및 이력서 공개가 가능합니다.</li>
			
			</div><br>
            
        </header> 
        <main>
            
            
            <div class="regist_sta">
                <div class="regist_sta_title">
                    <span>이력서등록현황 </span>
                    <span class="regist_sta_border">|</span>
                    <%int ctn=0;
                        for(Resume r:list){
								if(r.getDelete_yn().equals("N")){
									ctn++;
								}
                        }
                        	%>
                    <span class="regist_sta_red"><%= ctn%></span>
                    <span class="regist_sta_modify">건 / </span>
                    <span class="regist_sta_limit">3건</span>
                </div>
                
                
                <div class="regist_sta_contents">
                    <table cellpadding="0" cellspacing="0" width="">
                        <caption>이력서등록현황</caption>
                        <colgroup>
                            <col width="130">
                            <col width="580">
                            <col width="120">
                            <col width="5">
                            <col width="65">
                            <col width="65">
                            <col width="65">
                            <col width="125">
                            
                        </colgroup>
                        <br>
                         <tbody>
                         <tr class="hiddentable"> 
                                <td class="re_post" align="left">대표</td>
                                
                                <td class="resume_title1">
                                   
                                    <p class="resume_title" ></p>
                                    <p class="resume_date"></p>
                                       <p class="resumeno"></p>
                                </td>
                                <td align="center">비공개</td>
                                <td align="center" class="resume_border">|</td>
                                <td align="center" ><p>보기</p></td>
                                <td align="center" >수정</td>
                                <td align="center" >삭제</td>
                                <td align="center" >대표설정</td>
                            </tr>
                         
                         <%for (int i = list.size()-1; i>=0 ; i--){ %>
                               <% Resume r = list.get(i); %>
                        <% if(r.getDelete_yn().equals("N")){ %>
                           <% if (r.getComplete_yn().equals("Y")){ %>
                            <tr> 
                               <%if(r.getRepresent_yn().equals("Y")){ %>
                               
                                <td class="re_post" align="left">대표</td>
                                <% }else{ %>
                                <td class="re_post" align="left">일반</td>
                                <%} %>
                                <td class="resume_title1">
                                   
                                    <p class="resume_title" style="cursor:pointer" onclick="resumeView('<%=r.getResume_no() %>')"><%=r.getResume_title() %></p>
                                    <p class="resume_date"><%=r.getResume_modify_date() %></p>
                                    <p class="resumeno"><%=r.getResume_no() %></p>
                                    
                                </td>
                                <% System.out.println(r.getResume_open_yn()); %>
                                 <%if(r.getResume_open_yn().equals("N")){ %>
                                <td align="center" style="text-align:center;">비공개</td>
                                <% }else{ %>
                                <td align="center" style="text-align:center;">공개</td>
                                <% } %>
                                <td align="center" class="resume_border">|</td>
                                <td align="center" style="cursor:pointer" onclick="resumeView('<%=r.getResume_no() %>')"><div style="text-align:center; width:55px; height:25px; border:1px solid #013252; border-radius: 14px; line-height:25px;">보기</div></td>
                                <td align="center" style="cursor:pointer" onclick="resumeUpdate('<%=r.getResume_no() %>')"><div style="text-align:center; width:55px; height:25px; border:1px solid #013252; border-radius: 14px; line-height:25px;">수정</div></td>
                                <td align="center" style="cursor:pointer" onclick="resumeDelete('<%=r.getResume_no() %>')"><div style="text-align:center; width:55px; height:25px; border:1px solid #013252; border-radius: 14px; line-height:25px;">삭제</div></td>
                                <td align="center" style="cursor:pointer"  onclick="resumeRepre('<%=r.getResume_no() %>')"><div style="text-align:center; width:80px; height:25px; border:1px solid #013252; border-radius: 14px; line-height:25px;">대표설정</div></td>
                            </tr>
                            <% }else { %>
                               <tr>
               
                                <td class="re_post" align="left" >미완성</td>
                                <td class="resume_title1" onclick="resumeView('<%=r.getResume_no() %>')">
                                    <p class="resume_title" style="cursor:pointer" ><%=r.getResume_title() %></p>
                                    <p class="resume_date"><%=r.getResume_modify_date() %></p>
                                </td>
                                <td align="center"></td>
                                <td align="center" colspan="2" style="text-align:center; cursor:pointer" onclick="resumeDelete('<%=r.getResume_no() %>')"><div style="text-align:center; width:55px; height:35px; border:1px solid #013252; border-radius: 14px; line-height:35px;">삭제</div></td>
                                <td align="center" style="text-align:center; cursor:pointer;" class="resume_border" >|</td>
                                <td align="center" style="cursor:pointer" colspan="2"><div style="text-align:center; width:80px; height:35px; border:1px solid #013252; border-radius: 14px; line-height:35px;" onclick="resumeUpdate('<%=r.getResume_no()%>')">완성하기</div></td>
                                
                            </tr>
                            <% } %>
                            
                            <% }else{ %>
                            
                            <% } %>
                            
                     <%
                        }
                     %> 
                     
                           
                        </tbody>
                    </table>
                </div>
            </div>
    		<br>
            <div class="reg_btn" onclick="resumeWrite3()">
                <a href="#" style="text-align:center">
                    <span class="resume_pret">이력서 </span>
                    <span class="resume_reg">등록하기</span>
                </a>
            </div>
        </main>
    </div>
    </div>
   <script>
    $(function(){
    	$(".resumeno").hide();
    	$(".hiddentable").hide();
    });
    
    </script> 
	<script>
   		function resumeView(num){
		console.log(num);
		location.href = "<%=request.getContextPath()%>/res.detail?num=" + num;
	}
   		function resumeUpdate(num){
   		console.log(num);
   		location.href = "<%=request.getContextPath()%>/res.UpView?num=" + num;
   	}
   		function resumeDelete(num){
   			if (confirm("정말 삭제하시겠습니까??") == true){    //확인
   				location.href = "<%=request.getContextPath()%>/res.del?num=" + num;
	    	}else{   //취소
	    	    return;
	    	}
   		<%-- console.log(num);
   		location.href = "<%=request.getContextPath()%>/res.del?num=" + num; --%>
   		}
   		function resumeRepresent(num){
   			console.log(num);
   			location.href = "<%=request.getContextPath()%>/represent.y?num=" + num;
   		}
   		function resumeNomal(num){
   			console.log(num);
   			location.href = "<%=request.getContextPath()%>/represent.n?num=" + num;
   		}
   		function resumeWrite3(){
   					if(<%=ctn%> >= 3){
					alert("등록된 이력서가 3개 이상입니다.");
					}else{
					location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/resumeWrite.jsp";
					}
			}
   		function resumeRepre(num){
   			
   			location.href = "<%=request.getContextPath()%>/represent.res?num=" + num;
   		}
   		
   		
    </script>
    
</body>
</html>