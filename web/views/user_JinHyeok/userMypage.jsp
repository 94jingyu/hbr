<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ page import = "com.kh.hbr.resume.model.vo.Resume" %>
<%@ page import = "com.kh.hbr.member.model.vo.Member" %>
<%@ page import = "com.kh.hbr.apply.model.vo.*" %>
<%@ page import = "java.util.*" %>
 
<%
	Member m = (Member) session.getAttribute("resumeMember");
	ArrayList<Resume> list=(ArrayList<Resume>) session.getAttribute("resumeList");
	ArrayList<Apply> alist  = (ArrayList<Apply>) session.getAttribute("alist");

	/* String managerPhone1 =recruit.getManagerPhone().substring(0,3);
	String managerPhone2 =recruit.getManagerPhone().substring(3,7);
	String managerPhone3 =recruit.getManagerPhone().substring(7,11); */
%> 
<% 
  	
	for(Resume r:list){
	if(r.getResume_title()==null){
		r.setResume_title("제목이 없습니다.");
	}
}
	%>
<!DOCTYPE html>         
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	<style>  
	* {
	text-align:left;
	margin: 0;
	padding: 0;
}

/* body {background-color: #fffde7;} */
#wrap {
	width: 1200px;
	margin: 0 auto;
}

/* 헤더 영역 */
.logo {
	float: left;
	width: 22%;
	height: 130px;
}

/* .toplogin {
	width: 100%;
	height: 50px;
	padding: 1%;
}
 */
/* 상단메뉴바 */
.topMenu {
	/* display: inline-block; */
	width: 100%;
	height: 50px;
	/* background: #013252; */
	line-height: 30px; /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	vertical-align: middle; /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	text-align: center; /* 글씨 정렬을 가운데로 설정 */
	font-weight: bolder;
	font-size: 20px;
	color: #013252;
}

.topMenu>div {
	float: right;
}

.topMenu>ul {
	float: left;
	margin: 0;
	padding: 0;
	list-style: none;
}

.topMenu>ul>li {
	display: inline-block;
	padding: 0;
}

.topMenu a {
	display: block;
	text-decoration: none;
	padding: 10px 20px;
	color: black;
}

.topMenu a:hover {
	background: #013252;
	color: white;
}

/* 하이퍼텍스트 효과 */
a {
	text-decoration: none;
} /* 하이퍼텍스트 밑줄 효과 없애기 */

/* 광고 영역 */
.ads {
	width: 100%;
	height: 500px;
	clear: both;
}

/* 공고조회 및 로그인 영역 */
.b2_cotainer {
	width: 100%;
	height: 260px;
	background-color: #F0F0F0;
	text-align: center;
}

#selectJob {
	float: left;
	width: 70%;
	height: 250px;
	background-color: #039be5;
	display: inline-block;
}

#login {
	width: 30%;
	height: 250px;
	background-color: #0288d1;
	display: inline-block;
}

.fsearch_wp {
	width: 100%;
	border: 1px solid #e1e1e1;
	border-bottom: none;
	font-size: 14px;
	box-sizing: border-box;
}

/* 일반채용리스트 */
.normalJobList {
	width: 88%;
	font-size: 20px;
	font-weight: bold;
}

#normaljobList {
	font-size: 16px;
	font-weight: bold;
	border: none;
	border-bottom: 3px;
	border-bottom-color: black;
	background-color: lightGray;
	
}

/* footer 영역 */
footer {
	width: 100%;
	height: 200px;
	background-color: #ffb300;
}

/* 버튼 효과 */
.btns {
	float:right;
}

 #loginBtn, #memberJoinBtn, #logoutBtn, #changeInfo, #adminBtn {
	display: inline-block;
	text-align: center;
	
	
	height: 25px;
	width: 100px;
	border-radius: 5px;
	
	
}



/* nav */
.nav {
	width: 15%;
	height: 300px;
	border: solid 1px lightgray;
	margin-left: 5px;
}

/* 박다영 스타일 */
/* left 메뉴 */
   .leftblock {
      display: block;
   }
   
   #left2, #left3, #left4 {
      margin-top:10px;
   }
   
   #left1 {
      width: 180px;
      height:900px;
      margin-top:10px;
      align: left;
   }
   
   .myPage {
   	margin-left: 10px;
   }
   
   #left2 {
      height:290px;
   }
   
   #left3 {
      height:260px;
   }
   
   #left4 {
      height:250px;
   }
   
   /* left메뉴 가로선 */
   hr {
      width:90%;
      color:lightgray;
      size:2px;
      margin-left: 5%;
   }
   
   ul {
      list-style:none;
   }

.leftMenu{float: left; width: 20%; height: 580px; margin-left: 30px; margin-top: 10px;}
   
   /* 내용 부분 */
   /* section{width: 75%; height: 1500px; background-color: #ffca28;} */
   
   .leftMenu>ul {       
       margin: 10;
       padding: 0;
       list-style: none;
    }
    .leftMenu a {
       display: block;
       text-decoration: none;
       padding: 10px 20px;
       color: black;
     }
.left {
      border:2px solid rgb(192, 192, 192);
   }
	.wrap{width:1200px;margin:0 auto}
        
/*header*/

.mypage_wrap{width:930px;float:right;}
.mypage_root{padding:0 0 20px 0}
.header_top{background:#013252;padding:20px;margin-bottom:20px}
.header_top p{color:#fff;font-size:20px;font-weight:bold}

/*main*/
/*사용자 정보*/
.user_info{background:#ecf0f7;padding:20px;overflow:hidden;margin-bottom:10px}
.user_img{padding:10px;float:left}

.user_info_detail{float:left;padding:10px;font-weight:bold;}
.user_info_detail li{padding:10px 10px 0 0;}
.user_info_detail .u_name_box{width:150px;border-bottom:2px solid #000;text-align:center;padding:0 0 4px 0;margin-bottom:10px}
.u_name_box .u_name{font-size:30px;color:#013252;}

.user_career_info{float:left;font-weight:bold;}
.user_career_info ul{overflow:hidden}
.user_career_info li{float:left;padding:10px 15px}
.user_career_info span{font-size:50px;color:#013252;}
.user_career_info .job_app span,.user_career_info .scr_cur span{display:block;text-align:center}
.user_career_info p{font-size:20px;text-align:center;padding-top:15px}
.user_career_info .resume_limit,.user_career_info .not_limit{font-size:30px;color:#c4c4c4}

/*입사지원현황*/
.current_job_app{clear:both;margin-bottom:10px;overflow:hidden}
.current_job_app_title{font-weight:bold;font-size:18px;padding:10px;float:left}
.job_app_border{color:#7f7f7f;}
.job_app_modify,.job_app_red,.job_app_limit{font-size:15px}
.job_app_red{color:#f00}
.job_app_limit{color:#c4c4c4}

.more_btn{float:right;padding:10px}
.more_btn a{font-weight:bold}

.current_job_app_contents{clear:both}
.current_job_app_contents table{/* border:1px solid #ddd; */border-collapse:collapse}
.current_job_app_contents caption{position:absolute;left:-9999px;top:-9999px}
.current_job_app_contents thead{background:#ecf0f7}
.current_job_app_contents thead td{padding:10px}
.current_job_app_contents tbody td{padding:15px;border-bottom:1px solid #ddd}

/*이력서등록현황*/
.regist_sta{overflow:hidden;font-weight:bold;margin-bottom:10px}
.regist_sta_title{font-size:18px;padding:10px;float:left}
.regist_sta_contents{clear:both} 
.regist_sta_border{color:#7f7f7f;}
.regist_sta_red{color:#f00;font-size:15px}
.regist_sta_modify,.regist_sta_limit{font-size:15px}
.regist_sta_limit{color:#c4c4c4}

/* .regist_sta_contents table{border:1px solid #ddd;} */
.regist_sta_contents caption{position:absolute;left:-9999px;top:-9999px}
.regist_sta_contents td{padding:15px;  border-bottom:1px solid #ddd;border-top:1px solid #ddd }
.re_post{color:#f00;font-size:18px;}
.resume_title{padding:5px 0}
.resume_date{font-size:14px;font-weight:normal;padding:5px 0}
.resume_border{font-size:30px;color:#7f7f7f}

/*이력서등록버튼*/
.reg_btn{background:#ecf0f7;text-align:center;padding:15px;font-weight:bold;font-size:18px;height:25px;margin-bottom:20px}
.reg_btn a{display:block;height:100%;}
.resume_pre{color:#013252;}
.resume_reg{color:#76c0bc;}

	</style>
</head>
<body>
<div id="wrap">
		<!-- 로고 영역 -->
		<%@ include file="../common/E_user_menubar.jsp" %>
	
		<hr style="border: solid 2px #013252; width:100%; margin:0 auto"><br>
		
		<%@ include file="../user_JinHyeok/leftBar.jsp" %><br>
			
    <div class="mypage_wrap">
    
        <header>
            <div class="mypage_root">
                <span>HOME</span>
                <span>> 마이페이지</span>
            </div>
            <div class="header_top">
                <p>마이페이지</p>
            </div>
        </header> 
        <main>
            <div class="user_info">
                <div class="user_img">
                    <img src="../../static/images/user//u_img.png" alt="나의 사진">
                </div>
                <div class="user_info_detail">
                    <ul>
                        <li class="u_name_box">
                            <span class="u_name"><%=loginUser.getMemberName() %></span>
                            <span class="uname_modify">님</span>
                        </li>
                        <li class="u_email_box">
                            <span>이메일</span>
                            <span><%=loginUser.getEmail() %></span>
                        </li>
                        <li class="u_mobile_box">
                            <span>휴대전화</span>
                            <span><%=loginUser.getPhone() %></span>
                        </li>
                    </ul>
                </div>
                <div class="user_career_info">
                    <ul>
                        <li class="resume_cur">
						<%int ctn=0;
                        for(Resume r:list){
								if(r.getDelete_yn().equals("N")){
									ctn++;
								}
                        }
                        	%>
                        	
                        <%int ctn1=0;
                        for(Apply a : alist) {
								
									ctn1++;
								}
                        
                        	%>
                            <span class="resume_cut"><%=ctn%></span>
                            
                            <span class="resume_limit">/ 3</span>
                            <p>이력서</p>
                        </li>
                        <li class="job_app">
                            <span class="job_app_cut"><%= ctn1 %></span>
                            <p>입사지원</p>
                        </li>
                        <li class="scr_cur">
                            <span class="scr_cut">0</span>
                            <p>스크랩</p>
                        </li>
                        <li class="re_notice">
                            <span class="not_cut">0</span>
                            <span class="not_limit">/ 100</span>
                            <p>최근본공고</p>
                        </li>
                    </ul>
                </div>
            </div>
            <br>
            <div class="current_job_app">
                <div class="current_job_app_title">
                    <span>입사지원현황 </span>
                    <span class="job_app_border">|</span>
                    <span class="job_app_modify">총</span>
                    <span class="job_app_red"><%= ctn1 %></span>
                    <span class="job_app_modify">건</span>
                    <span class="job_app_limit">/ 5건</span>
                </div>
                <div class="more_btn">
                    <a href="#">더보기</a>
                </div>
                <div class="current_job_app_contents">
                    <table cellpadding="0" cellspacing="0" width="">
                        <caption>입사지원현황</caption>
                        <colgroup>
                            <col width="170">
                            <col width="160">
                            <col width="700">
                            <col width="170">
                        </colgroup>
                        <br>
                        <% if(ctn1==0){ %>
                        <% }else{ %>
                        <thead>
                            <tr style="text-align:center">
                                <td style="text-align:center" align="center">지원일</td>
                                <td style="text-align:center" style="text-align:center"align="center">회사명</td>
                                <td style="text-align:center" align="center">모집내용</td>
                                <td style="text-align:center" align="center">마감일</td>
                            </tr>
                        </thead>
                        <% for (Apply a : alist) { %>
                        <tbody>
                            <tr>
                                <td style="text-align:center" align="center"><%= a.getApplyEnrollDate() %></td>
                                <td style="text-align:center" align="center"><%= a.getCompanyName() %></td>
                                <td style="text-align:center" align="center"><%= a.getRecruitTitle() %></td>
                                <td style="text-align:center" align="center"><%= a.getExpirationDate() %></td>
                            </tr>
                        </tbody>
                        <% } %>
                        <% } %>
                    </table>
                </div>
            </div>
            <br>
            <div class="regist_sta">
                <div class="regist_sta_title">
                    <span>이력서등록현황 </span>
                    <span class="regist_sta_border">|</span>
                    <span class="regist_sta_red"><%=ctn%></span>
                    <span class="regist_sta_modify">건 / </span>
                    <span class="regist_sta_limit">3건</span>
                </div>
                <div class="more_btn">
                    <a href="#">더보기</a>
                </div>
                
                <div class="regist_sta_contents">
                    <table cellpadding="0" cellspacing="0" width="">
                        <caption>이력서등록현황</caption>
                        <colgroup>
                            <col width="130">
                            <col width="865">
                            <col width="5">
                            <col width="200">
                        </colgroup>
                        <br>
                        <tbody>
                        <%for (int i = list.size()-1; i>=0 ; i--){ %>
                            	<% Resume r = list.get(i); %>
                        	<% if(r.getDelete_yn().equals("N")){ %>
                            <% if (r.getComplete_yn().equals("Y")){ %>
                            <tr>
                            	<%if(r.getRepresent_yn().equals("Y")){ %>
                                <td class="re_post" align="left">대표</td>
                                <% }else{ %>
                                <td class="re_post" align="left">일반</td>
                                <% } %>
                                <% }else{ %>
                                <td class="re_post" align="left">미완성</td>
                                <%} %>
                                
                                <td class="resume_title1" <%-- onclick="resumeView('<%=r.getResume_no() %>')" --%>>
                                	
                                    <p class="resume_title" style="cursor:pointer" onclick="resumeView('<%=r.getResume_no() %>')"><%=r.getResume_title() %></p>
                                    <p class="resume_date"><%=r.getResume_modify_date() %></p>
                                    
                                </td>
                                <td align="center" class="resume_border">|</td>
                                <td align="center"><a href="#" onclick="resumeUpdate('<%=r.getResume_no() %>')">수정하기</a></td>
                                <td><p class="resumeno" id="resume_no1"><%=r.getResume_no() %></p></td>
                            </tr>
                            <% }else{ %>
                            
                            <% } %>
							<%
								}
							%>
                           
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="reg_btn" onclick="resumeWrite2()">
                <a href="#" style="text-align:center">
                    <span class="resume_pret">이력서 </span>
                    <span class="resume_reg">등록하기</span>
                </a>
            </div>
        </main>
    </div>
    </div>
    
    
    <script>
    $(function(){
    	$(".resumeno").hide();
    	
    });
    
    </script>  
							<script>
							function resumeView(num){
								console.log(num);
								
								location.href = "<%=request.getContextPath()%>/res.detail?num=" + num;
								
							}
   							 function resumeUpdate(num){
   			 					console.log(num);
   								location.href = "<%=request.getContextPath()%>/res.UpView?num=" + num;
   							 }
   			 				function resumeWrite2(){
   			 					if(<%=ctn%> >= 3){
   			 					alert("등록된 이력서가 3개 이상입니다.");
   			 					}else{
   			 					location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/resumeWrite.jsp";
   			 					}
   			 				}
   			 				
   			 				function resumeWrite(){
			 					if(<%=ctn%> >= 3){
			 					alert("등록된 이력서가 3개 이상입니다.");
			 					}else{
			 					location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/resumeWrite.jsp";
			 					}
			 				}
   			 
   			
   			 				function nvl(str, defaultStr){
   			 	         
   			 	        	if(typeof str == "undefined" || str == null || str == "")
   			 	            str = defaultStr ;
   			 	         
   			 	        	return str ;
   			 	    		}
    
    						</script>
    
</body>
</html>