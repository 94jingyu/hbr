<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	#wrap {width: 1200px; margin: 0 auto; height:100px; top:20%; margin-top:50px;}
	#whiteBox {width:720px; height:850px; padding: 40px; border: 1px solid #C4C4C4; margin: 0 auto; background: white;}
	.checkBox {width: 17px; height: 17px;}
	a {text-decoration: none; color: black;}
	textArea {color: #646464; width: 100%;}	
	.innerTable {width: 100%; border: 1px solid #646464;; border-collapse: collapse; }
 	.agreeBtn {margin: 0 auto; width: 120px;}
	#backBtn {float: left; width:100px; height:50px; font-size:17px; background-color: transparent !important; 
		background-image: none !important; border-color: transparent; border: none; margin: 2px;}
	#agreeBtn {width:150px; height:50px; font-size:20px; background: #013252; color: white; padding: 5px; margin: 2px; 
		border-radius:10px; border: 1px; outline: 1px;} 

	#agreeBtn, #backBtn:hover{cursor:pointer;}
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
<script>
    function agree(){ 
    	var ch1 = $('input:checkbox[id="essential1"]').is(":checked");
    	var ch2 = $('input:checkbox[id="essential2"]').is(":checked");

    	if(ch1 == true && ch2 == true) {
	    	location.href = "<%=request.getContextPath()%>/views/common/E_business_JoinPage2.jsp";
    	} else {
    		alert("이용약관에 동의해야합니다.");
    	}
    };
    
    function allCheck(){
    	var all = document.getElementsByName("all")
    	var list = document.getElementsByName("subject");

    	if(all[0].checked) {
    		for(var i = 0; i < list.length; i++){
    			list[i].checked = true;
    		}				
    	} else {
    		for(var i = 0; i < list.length; i++){
    			list[i].checked = false;
    		}
    	}
    }
    
   function back() {
	   history.back();
   }
    
    
</script>
</head>
<body>
	<div id="wrap">
	
	<!-- login area -->
	<div class="loginArea">
		<h1 id="loginTitle" align="center">기업 회원가입</h1>
		<br>
		<br>
		<div id=whiteBox> <!-- whiteBox -->
			<form id="loginForm" action="" method="post">
	 			<table>
					<tr>
						<td colspan="2"><h3>약관동의</h3></td>
					</tr>
					<tr>
						<td>이용약관, 개인정보 수집 및 이용, 마케팅·이벤트정보 수신(선택) <b>모두 동의</b>합니다.</td>
						<td style="text-align:right;">
							<label for="checkBox1">모두 동의</label>
							<input type="checkBox" id="checkBox1" class="checkBox" name="all"  onclick="allCheck(this.checked);">
						</td>
					</tr>
					<tr>
						<td colspan="2" height="10px;"><hr><br><br></td>
					</tr>
					<tr>
						<td><h4>이용약관에 동의(필수)</h4></td>
						<td style="text-align:right;" width="100px;">
							<label for="checkBox2" name="subject">동의</label>
							<input type="checkBox" id="essential1" class="checkBox essential" name="subject">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<textarea name=argree cols=100 rows=10 style="overflow-x:hidden;overflow-y:auto" readonly>
제 1 조 [목적]
본 약관은 종합미디어그룹 (주)미디어윌(이하 "회사")이 운영하는 서비스를 이용함에 있어 회사와 이용고객(이하 "회원")간의 이용조건 및 제반 절차, 권리, 의무 및 책임사항, 기타 필요한 사항을 규정함을 목적으로 합니다.
※ 「PC통신, 무선 등을 이용하는 서비스에 대해서도 그 성질에 반하지 않는 한 이 약관을 준용합니다.」

제 2 조 [용어의 정의]
(1) 이 약관에서 사용하는 용어의 정의는 다음과 같습니다.
① "미디어윌 통합사이트”(이하 "사이트"라 한다)란 "회사"에 회원 등록한 이용자가 다양한 정보와 서비스를 제공받을 수 있도록 컴퓨터, 스마트폰 등 정보통신설비를 이용하여 설정한 가상의 영업장 또는 회사가 운영하는 온라인 및 모바일 웹사이트, 앱 등의 온라인서비스를 의미합니다.
※ 미디어윌 통합사이트 현황 (2019년 8월 27일 현재) : 벼룩시장, 부동산써브

② "미디어 통합회원"(이하 "회원"이라 한다)이란 "회사"에 개인정보를 제공하여 회원등록을 하고 회원 ID를 부여 받은 자 또는 그 회원전체를 의미하며, 회원등록 시 부여 받은 ID로 미디어윌 통합사이트에 자유롭게 접속할 수 있습니다.
③ "서비스"라 함은 "회사"가 운영하는 "사이트"를 통해 "회원"이 등록하는 게시물 등을 각각의 목적에 맞게 분류 가공, 집계하여 정보를 제공하는 서비스와 기타 관련된 모든 부대 서비스를 말합니다.
④ "회원"이라 함은 "회사"가 운영하는 "사이트"를 통하여 본 약관에 동의하고, "회사"와 이용계약을 체결하여 "아이디(ID)"를 부여 받은 개인과 기업 및 단체를 말합니다.
⑤ "아이디(ID)"라 함은 회원의 식별 및 서비스 이용을 위하여 회원의 신청에 따라 회사가 회원별로 부여하는 고유한 문자와 숫자의 조합을 말합니다.
⑥ "비밀번호"라 함은 "아이디(ID)"로 식별되는 회원의 본인 여부를 검증하기 위하여 회원이 설정하여 회사에 등록한 고유의 문자와 숫자의 조합을 말합니다.
⑦ "비회원"이라 함은 "회원"에 가입하지 않고 "회사"가 제공하는 서비스를 이용하는 자를 말합니다.
⑧ "해지"라 함은 회사 또는 회원이 이용계약을 해약하는 것을 말합니다.
(2) 이 약관에서 사용하는 용어 중 제1항에서 정하지 아니한 것은 관계 법령 및 서비스별 안내, (주)미디어윌 이용약관에서 정하는 바에 따르며 그 외에는 일반 관례에 따릅니다.

제 3 조 [약관의 게시와 개정]
① "회사"는 이 약관의 내용을 "회원"이 쉽게 알 수 있도록 "사이트" 시작 화면에 게시합니다.
② "회사"는 "약관의 규제에관한 법률", "정보통신망 이용촉진 및 정보보호 등에 관한법률(이하 "정보통신망법")" 등 관련법을 위배하지 않는 범위에서 이 약관을 개정할 수 있습니다.
③ "회사"가 약관을 개정할 경우에는 적용일자 및 개정사유를 명시하여 현행약관과 함께 제1항의 방식에 따라 그 개정약관의 적용일자 7일 전부터 적용일자 전일까지 공지합니다. 다만, "회원"에게 불리한 약관의 개정의 경우에는 공지 외에 일정기간 서비스 내 전자우편, 로그인 시 동의창 등의 전자적 수단을 통해 따로 30일전에 통지하도록 합니다.
④ "회사"가 전항에 따라 개정약관을 공지 또는 통지하면서 이용자에게 7일 기간 내에 의사표시를 하지 않으면 의사표시가 표명된 것으로 본다는 뜻을 명확하게 공지 또는 통지하였음에도 "회원"이 명시적으로 거부의사를 표명하지 아니한 경우 개정약관에 동의한 것으로 봅니다.
⑤ "회원"이 개정약관의 내용에 동의하지 않는 경우 회사는 개정 약관의 내용을 적용 할 수 없으며, 이 경우 "회원"은 이용계약을 해지할 수 있습니다. 다만, 기존 약관을 적용할 수 없는 특별한 사정이 있는 경우에는 회사는 이용계약을 해지할 수 있습니다.
⑥ "회사"는 "서비스" 운영에 따라 "사이트" 별 특별 조항을 생성하거나 기존 약관을 개정할 수 있으며, 이 경우 해당 "사이트"를 통하여 본 조 제 ①항의 방식에 따라 "회원"에게 공지 또는 통보 후 약관을 적용할 수 있습니다.
							</textarea>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="10px;"><br><br></td>
					</tr>
					
					
					<tr>
						<td><h4>개인정보 수집 이용 동의(필수)</h4></td>
						<td style="text-align:right;" width="100px;">
							<label for="checkBox2">동의</label>
							<input type="checkBox" id="essential2" class="checkBox essential" name="subject">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table class="innerTable" border="1">
								<tr>
									<th height="30px;">이용목적</th>
									<th>개인정보의 항목</th>
									<th>보유 및 이용기간</th>
								</tr>
								<tr>
									<td width="40%" height="100px;">통합회원 가입시 본인 확인 및 부정이용방지</td>
									<td width="35%">아이디, 비밀번호, 이름, 이메일, 휴대전화 번호</td>
									<td width="25%"><b>회원 탈퇴까지 또는 고객 요청시</b></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<small>* 필수 수집 정보는 서비스 이용에 필요한 최소한의 정보이며, 동의를 해야만 서비스를 이용할 수 있습니다.</small>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="10px;"><br><br></td>
					</tr>
					
					
					<tr>
						<td><h4>마케팅·이벤트정보 수신 동의(선택)</h4></td>
						<td style="text-align:right;" width="100px;">
							<label for="checkBox2">동의</label>
							<input type="checkBox" id="checkBox2" class="checkBox" name="subject">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table class="innerTable" border="1">
								<tr>
									<th height="30px;">이용목적</th>
									<th>개인정보의 항목</th>
									<th>보유 및 이용기간</th>
								</tr>
								<tr>
									<td width="40%" height="100px;">이용자에게 최적화된 서비스 제공(회원 맞춤 서비스)신규 서비스 및 상품 개발을 위한 서비스 이용현황 통계/분석 해볼래 보금자리 등의 이벤트 기획 간행물 발송, 다양한 정보와 이벤트 소식 제공(이메일, 전화, 문자)</td>
									<td width="35%">이름, 상호(기업)명, 휴대폰 번호, 이메일</td>
									<td width="25%"><b>회원 탈퇴까지 또는 고객요청에 따라 개인정보 이용동의 철회 요청시까지</b></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<small>* 마케팅·이벤트정보 수신 동의(선택)는 동의하지 않아도 회원가입은 가능하나,  다양한 혜택 안내가 제한될 수 있습니다.</small>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="10px;"><br><br></td>
					</tr>
				</table>	
			</form>
			<div><button id=backBtn onclick="back();"><b>이전으로</b></button></div>
			<div class=agreeBtn><button id=agreeBtn onclick="agree();"><b>확인</b></button></div>
		</div> <!-- whiteBox -->
		<br><br><br>
		<footer>
		<table >
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div><!-- /login area -->
	</div>
</body>
</html>