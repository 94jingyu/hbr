<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.kh.hbr.member.model.vo.Business, java.util.*, com.kh.hbr.board.jengukBoard.model.vo.*"%>
<% 
	ArrayList<Business> list = (ArrayList<Business>) request.getAttribute("list");

	PageInfo pi = (PageInfo) request.getAttribute("pi");
	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
	int limit = pi.getLimit();
%>	
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>기업승인</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
* {margin: 0; padding: 0;}
#wrap {width: 1200px; height: 100%; margin: 0 auto;}

/* ============= 헤더 영역 ============= */
header {width: 100%; height: 100px; background: #013252;}
.logo {font-size: 45px; color: white;}
#title {margin-left: 10px;}

/* 사이트 바로가기 td id명 */
#goSite {font-size: 18px;}

/* =========== 좌측메뉴 및 내용 ============ */
.leftMenu {float: left; width: 20%; height: 1800px; background-color: #ECF0F7; border-right: 3px solid #CDCDCD}
.leftMenu {/* border-right: hidden; */ border-right: 3px solid #CDCDCD}
.leftMenu>ul {margin: 0; padding: 0; list-style: none;}
.leftMenu a {display: block; text-decoration: none; padding: 10px 20px; color: black;}
.leftMenu a:hover {background: darkgray; color: white;}

/* ========== HOME > 회원관리 > 기업인증내역 ========== */
.homeNavi {margin-top: 50px; float: right;}

/* ========== 기업인증내역 - 위아래 선 ========== */
.pageTitle {border-top: 3px solid rgb(200, 200, 200); border-bottom: 3px solid rgb(200, 200, 200);
	width: 930px; height: 80px; float: right; margin-top: 30px; margin-bottom: 90px;}

/* ========== 기업인증내역 -> 제목 ========== */
.pageTitle2 {padding: 23px;}

/* ============= 테이블 영역 ============= */

/* 기업인증내역 테이블 div class */
.Request {width: 850px;	margin-left: 30px; margin-top:50px;}

/* 기업인증내역 테이블 클래스명 */
.table {width: 840px; height: 170px; text-align: center; border-collapse: collapse;}

.table th {
	background: rgb(210, 210, 210);
	height: 40px;
	border: 1.5px solid rgb(180, 180, 180);
}

.table td {
	height: 110px;
	border: 1.5px solid rgb(180, 180, 180);
}

/* 테이블 제목 div class - 총 3건 */
.total {
	width: 100px;
	height: 30px;
	float: left;
}

/* 테이블 제목 label ID - 총 3건 */
#tableTitle {
	float: left;
	margin-bottom: 10px;
}

/* select - 전체, 기업명, 아이디, 미승인, 승인완료, 반려 */
.selectbox {
	width: 110px;
	height: 30px;
	float: right;
	margin-top: 8px;
}

/* 검색할 키워드 입력 div class (text상자) */
.searchKey {
	width: 180px;
	height: 50px;
	float: right;
}

/* 검색할 키워드 입력 input class (text상자) */
.searchKeyword {
	float: right;
	position: relative;
	width: 180px;
	height: 35px;
	mix-blend-mode: normal;
	border: 3px solid #013252;
	box-sizing: border-box;
}

/* 검색버튼 div 클래스명 */
.searchB {
	width: 70px;
	height: 40px;
	float: right;
}

/* 검색버튼 */
.searchButton {
	background: #013252;
	border: 0px;
	color: white;
	height: 35px;
	width: 70px;
	text-size: 15px;
}

/* 진규 추가 */
.searchBox {
	width: 840px;
	background: #d2d2d2;
	margin-left: 30px;
}

.searchTable {
	margin: 0 auto;
	padding: 30px;
	padding-left: 20px;
}

#searchBtn {
	width: 90px;
	height: 138px;
	font-size: 15px;
	background: #013252;
	color: white;
	padding: 5px;
	border-radius: 1px;
	border: 1px;
	outline: 1px;
}

#searchBtn:hover {cursor: pointer;}

.searchSelect {
	width: 120px;
	height: 40px;
	font-size: 20px;
}

.searchTable td {
	padding: 3px;
}
</style>
</head>
<body>
	<div id="wrap">
	<%@ include file="../guide/admin_menubar.jsp" %>
	
		<div class="homeNavi">
			<label style="font-size: 15px;">HOME > 회원관리 > 기업회원 목록</label>
		</div>

		<div class="pageTitle">
			<div class="pageTitle2">
				<label style="font-size: 25px;"><b>기업회원 목록</b></label>
			</div>
			<br>
			<div class="searchBox">
				<form id="searchForm" action="<%= request.getContextPath()%>/searchBmember.me" method="get">
					<table class="searchTable">
						<tr>
							<td rowspan="3"
								style="width: 150px; height: 80px; text-align: center"><b
								style="font-size: 20px">검색조건</b></td>
							<td style="width: 90px;">회원찾기</td>
							<td style="width: 120px;">
								<select class="searchSelect" name="searchCondition" style="width: 120px; height: 40px; font-size: 15px;">
										<option selected value="findAll">선택</option>
										<option value="findId">아이디</option>
										<option value="findName">담당자명</option>
										<option value="findCname">기업명</option>
										<option value="findCert">인증상태</option>
								</select>
							</td>
							<td style="width: 250px;">
							<input type="search" id="searchText" name="searchValue" placeholder="검색할 단어 입력" style="width: 250px; height: 38px; font-size: 15px;"></td>
							<td rowspan="3" style="width: 90px; text-align: center">
								<button type="submit" id="searchBtn">검색</button>
							</td>
						</tr>
						<tr>
							<td>기업인증</td>
							<td colspan="2"><select class="searchSelect" name="companyCert"
								style="width: 380px; height: 40px; font-size: 15px;">
									<option selected value="">인증여부</option>
									<option value="">대기</option>
									<option value="">인증</option>
									<option value="">반려</option>
							</select></td>
						</tr>
						<tr>
							<td>회원상태</td>
							<td colspan="2"><select class="searchSelect" name="memberStatus"
								style="width: 380px; height: 40px; font-size: 15px;">
									<option selected value="">구분 선택</option>
									<option value="Y">이용</option>
									<option value="N">탈퇴</option>
							</select></td>
						</tr>
					</table>
				</form>
			</div>
		
		<% if(list != null) {%>    	
     	<!--   테이블 -->
            <div class="Request">
            	<table class="table">
            		<caption>
            		<div class="searchB">
		   				</div>
            			<div class="total">
            				<label id="tableTitle" style="font-size:18px;"><b>총 <label style="color:orange;"><%= list.size() %></label>건</b></label>
            			</div>
            			
		   				<div class="searchKey">
		   				</div>
		   				<div class="selectbox">
		   				</div>
            		</caption>
            		<tbody>
            		<tr>
            			<th style="border-right:hidden;" width="60">번호</th>
            			<th style="border-right:hidden;" width="100">아이디</th>
            			<th style="border-right:hidden;" width="100">기업명</th>
            			<th style="border-right:hidden;" width="100">담당자명</th>
            			<th style="border-right:hidden;" width="100">가입일</th>
            			<th style="border-right:hidden;" width="100">기업인증</th>
            			<th style="border-right:hidden;" width="80">처리일</th>
            			<th style="" width="80">상태</th>
            		</tr>
            		<% int i = list.size() + 1; %>
            		<% for(Business b : list) { %>
              		<% i--; %>
            		<tr>
            			<td style="border-right:hidden;">
            				<label><%= i %></label>
            			</td>
            			<td style="border-right:hidden;">
            				<label><%= b.getBmemberId() %></label>
            			</td>
            			<td style="border-right:hidden;">
            				<label><%= b.getCompanyName() %></label>
            			</td>
            			<td style="border-right:hidden;">
            				<label><%= b.getManagerName() %></label>
            			</td>
            			<td style="border-right:hidden;">
            				<label><%= b.getBenrollDate() %></label>
            			</td>
            			<td style="border-right:hidden;">
            				<label><%= b.getCertStatus() %></label>
            			</td>
            			<td style="border-right:hidden;">
            				<label><%= b.getCertDate() %></label>
            			</td>
            			<td>
            				<% if(b.getBstatus().equals("Y")) {%>
            				<label>이용</label>
            				<% } else { %>
            				<label>탈퇴</label>
            				<% } %>
            			</td>
            		</tr>
            		<% } %>
            		</tbody>
            	</table>
       			<br><br>
            </div>
        </div>  
        </div>
     	<% } %> 
     	
     	
     <%-- 페이지 처리 --%>
     <div class="pagingArea" align="center">
        <button onclick="location.href='<%=request.getContextPath()%>/selectJenguk.tn?currentPage=1'"><<</button>
        <% if(currentPage <= 1) { %>
        <button disabled><</button>
        <% } else { %>
        <button onclick="location.href='<%=request.getContextPath()%>/selectJenguk.tn?currentPage=<%=currentPage - 1%>'"><</button>
        <% } %>
        
        <% for(int p = startPage; p <= endPage; p++) { 
        	  if(p == currentPage){
        %>
        		<button disabled><%= p %></button>
        <%
        	  } else {
        %>
        		<button onclick="location.href='<%=request.getContextPath()%>/selectJenguk.tn?currentPage=<%=p%>'"><%= p %></button>
        <% 
        	  }
           }
        %>
      
      	
      	<% if(currentPage >= maxPage) { %>
      	<button disabled>></button>
      	<% } else { %>
      	<button onclick="location.href='<%=request.getContextPath()%>/selectJenguk.tn?currentPage=<%=currentPage + 1%>'">></button>
      	<% } %>
      	
      	<button onclick="location.href='<%=request.getContextPath()%>/selectJenguk.tn?currentPage=<%=maxPage%>'">>></button>
     	
     	
</body>
</html>