<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	#wrap {width: 1200px; margin: 0 auto; height:100px; top:20%; margin-top:50px;}
	#whiteBox {width:720px; height:670px; padding: 40px; border: 1px solid #C4C4C4; margin: 0 auto; background: white;}
	.checkBox {width: 17px; height: 17px;}
	a {text-decoration: none; color: black;}
	textArea {color: #646464;}	
	
	
	.btn {width:120px; height:44px; font-size:15px; margin: 2px; background: #666666; color:#C2C2C2; border: none;}
	.btn2 {width:180px; height:50px; font-size:20px; color: white; padding: 5px; margin: 2px; border-radius:10px; border: 1px; outline: 1px;}
 	.agreeBtn {margin: 0 auto; width: 120px;}
	.backBtn {float: left; width:150px; height:50px; font-size:20px; background: #666666; color: white; padding: 5px; margin: 2px; 
		border-radius:10px; border: 1px; outline: 1px;} 
	.agreeBtn {float: left; width:150px; height:50px; font-size:20px; background: #013252; color: white; padding: 5px; margin: 2px; 
		border-radius:10px; border: 1px; outline: 1px;} 
	.btn, .btn2:hover{cursor:pointer;}

	#agreeBtn, #backBtn:hover{cursor:pointer;}
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
<script>
    function next(){ 
       	var ch1 = $('input:checkbox[id="essential1"]').is(":checked");
       	var ch2 = $('input:checkbox[id="essential2"]').is(":checked");
       	var ch3 = $('input:checkbox[id="essential3"]').is(":checked");

       	if(ch1 == true && ch2 == true && ch3 == true) {
	    	location.href = "<%=request.getContextPath()%>/views/common/E_DeletePage2.jsp";
       	} else {
       		alert("모두 동의해야합니다.");
       	}
    };
    
    function allCheck(){
    	var all = document.getElementsByName("all")
    	var list = document.getElementsByName("subject");

    	if(all[0].checked) {
    		for(var i = 0; i < list.length; i++){
    			list[i].checked = true;
    		}				
    	} else {
    		for(var i = 0; i < list.length; i++){
    			list[i].checked = false;
    		}
    	}
    }
    	
</script>
</head>
<body>
	<div id="wrap">
	
	<!-- login area -->
	<div class="loginArea">
		<h1 id="loginTitle" align="center">회원탈퇴</h1>
		<br>
		<br>
		<div id=whiteBox> <!-- whiteBox -->
			<form id="loginForm" action="" method="post">
	 			<table style="margin: 0 auto">
		 			<tr>
	 					<td colspan="5"><p style="font-size: 20px; font-weight: bold; margin-bottom: 5px;">회원탈퇴 안내</p></td>
	 				</tr>
					<tr>
						<td><p style="margin-bottom: 5px;" >* 회원탈퇴에 대한 안내를 읽고, 동의를 진행해주세요.</td>
						<td style="text-align:right;">
							<label for="checkBox1">모두 동의</label>
							<input type="checkBox" id="checkBox1" class="checkBox" name="all"  onclick="allCheck(this.checked);">
						</td>
					</tr>
					<tr>
						<td colspan="2" height="10px;"><hr style="border: 2px solid #013252;"><br><br></td>
					</tr>
					<tr>
						<td><h3>아이디 재사용 및 복구 불가 안내</h3></td>
						<td style="text-align:right;" width="300px;">
							<label for="checkBox2">동의</label>
							<input type="checkBox" id="essential1" class="checkBox" name="subject">
						</td>
					</tr>
					<tr>
						<td><br></td>
					</tr>
					<tr>
						<td colspan="2">
							<p style="color:red">- 탈퇴 진행 시 아이디 재사용이나 복구가 불가능합니다.</p>
							<p>- 신중히 선택하신 후 결정해주세요.</p>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="10px;"><br><hr><br></td>
					</tr>
					<tr>
						<td><h3>개인정보 삭제 안내</h3></td>
						<td style="text-align:right;" width="100px;">
							<label for="checkBox2">동의</label>
							<input type="checkBox" id="essential2" class="checkBox" name="subject">
						</td>
					</tr>
					<tr>
						<td><br></td>
					</tr>
					<tr>
						<td colspan="2">
							<p>- 회원탈퇴 즉시 개인정보는 1년간 보관되며 각 서비스별로 아래와 같이 처리됩니다. </p>
							<p>- 스크랩 및 맞춤정보, 이력서, 기타 아이디와 연계된 사적인 영역의 정보와 게시물 삭제</p>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="10px;"><br><hr><br></td>
					</tr>
					<tr>
						<td><h3>보관 데이터 안내</h3></td>
						<td style="text-align:right;" width="100px;">
							<label for="checkBox2">동의</label>
							<input type="checkBox" id="essential3" class="checkBox" name="subject">
						</td>
					</tr>
					<tr>
						<td><br></td>
					</tr>
					<tr>
						<td colspan="2">
							<p>탈퇴 후에는 회원 정보 삭제로 인해 작성자 본인을 확인할 수 없으므로 게시글을 임의로 삭제할 수 없으며 그 외 보관 데이터는 다음과 같습니다.</p><br>
							<p>- 부적합 정보 및 불량 이용, 징계에 관한 기록</p>
							<p>- 서비스 이용 시 결제에 관한 기록</p>
							<p>- 타인과 함께 사용하는 공적인 영역(게시판)의 게시물과 덧글</p>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="10px;"><br></td>
					</tr>
				</table>	
			</form>
			<br><br>
			<div style="width:400px; margin: 0 auto">
				<button class="btn2 backBtn" style="margin-right:40px" onclick="back();">취소</button>
				<button class="btn2 agreeBtn" onclick="next();"><b>다음 단계</b></button>
			</div>
		</div> <!-- whiteBox -->
		<br><br><br>
		<footer>
		<table >
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div><!-- /login area -->
	</div>
</body>
</html>