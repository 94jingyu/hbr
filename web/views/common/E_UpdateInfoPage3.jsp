<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.hbr.member.model.vo.Member"%>
<%
	Member loginUser = (Member) session.getAttribute("loginUser");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	#wrap {width: 1200px; margin: 0 auto; height:100px; top:20%; margin-top:50px;}
	#whiteBox {width:720px; height:100%; padding: 40px; border: 1px solid #C4C4C4; margin: 0 auto; background: white;}
	a {text-decoration: none; color: black;}
	input {width:410px; height:40px; font-size:15px; margin: 2px;}
	p {margin: 2px}
	#tel1 {width:154px; height:42px; font-size:15px; margin: 2px;}
	.btn {width:120px; height:44px; font-size:15px; margin: 2px; background: #666666; color:#C2C2C2; border: none;}
	.btn2 {width:180px; height:50px; font-size:20px; color: white; padding: 5px; margin: 2px; border-radius:10px; border: 1px; outline: 1px;}
	.btnDiv{width:120px; text-align: center; display:table-cell;vertical-align:middle;}
 	.agreeBtn {margin: 0 auto;}
	#email_select {width:413px; height:42px; font-size:15px; margin: 2px;}
	th {background: #f5f6f7}
	
	.backBtn {float: left; width:150px; height:50px; font-size:20px; background: #666666; color: white; padding: 5px; margin: 2px; 
		border-radius:10px; border: 1px; outline: 1px;} 
	.agreeBtn {float: left; width:150px; height:50px; font-size:20px; background: #013252; color: white; padding: 5px; margin: 2px; 
		border-radius:10px; border: 1px; outline: 1px;} 
	.btn, .btn2:hover{cursor:pointer;}
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
<script>
	function back() {
		   history.back();
	 }
	
	// 비밀번호 유효성 검사
  	$(function(){
  		var pwdCheck = /([a-zA-Z0-9].*[!,@,#,$])|([!,@,#,$].*[a-zA-Z0-9])/;
  		
  		$("#alert-pwd-success").hide();
		$("#alert-pwd-danger").hide();
		$("#alert-success").hide();
		$("#alert-danger").hide();
		
		$("input").change(function(){
			var memberPwd = $("#memberPwd").val();
			var memberPwd2 = $("#memberPwd2").val();
			
			// 비밀번호 유효성 검사
			if(memberPwd != "") {
				if(pwdCheck.test(memberPwd)) {
					$("#alert-pwd-success").show();
					$("#alert-pwd-danger").hide();
				} else {
					$("#alert-pwd-success").hide();
					$("#alert-pwd-danger").show();
				}
			} else {
				$("#alert-pwd-success").hide();
				$("#alert-pwd-danger").hide();
			}
			
			// 비밀번호 재입력  검사
			if(memberPwd != "" && memberPwd2 != "") {
				if(memberPwd == memberPwd2) {
					$("#alert-success").show();
					$("#alert-danger").hide();
					$("#sumit").removeAttr("disabled");
				} else {
					$("#alert-success").hide(); 
					$("#alert-danger").show(); 
					$("#submit").attr("disabled", "disabled");
				}
			} else {
				$("#alert-success").hide();
				$("#alert-danger").hide();
			}
			
		}); 
		
 	});
	
	// 이메일 select
	$(function(){
		$("#email_select").change(function(){
			if($("#email_select option:selected").text() == "직접 입력"){
				var value = ""; 
			} else {
				value = $("#email_select option:selected").text(); // 이외 메일 주소 선택일 경우 selectbox에서 선택한 값
			   	$("#email2").val(value);                           // 값 입력됨
			}
		});
	});
	 
	
	// 핸드폰 문자 발송 (from 용훈이형)
	$(function(){
		$("#sendSms").click(function(){
			alert("인증번호가 발송되었습니다.");
			
			var tel1 = $("#tel1").val();
			var tel2 = $("#tel2").val();
			var phone = tel1 + tel2;
			//console.log(phone);

			var randomNum = "초기값"; //유저에게 보낸 문자의 랜덤 숫자를 저장하기 위한 전역변수
			
			$.ajax({
				url: "/h/sendSms.api",
			    data: {
		    		phone: phone
		    	},
			    type: "post",
			    success: function(data) {
				    randomNum = data; //유저에게 문자로 보낸 랜덤 값을 그대로 리턴 받아서 전역변수로 선언한 randomNum에 넣어줬음
				    console.log("data : " + data);
			    },
			    error: function(error) {
			        console.log(error);
			    } 
			});
			
			// 인증번호 일치 여부 확인
			$("#checkSmsNumber").click(function(){
				var userSms = $("#checkSms").val(); // 회원가입 폼에서 유저가 문자를 보고 입력한 숫자를 가져옴
				// console.log('userSms : ' + userSms);
				// console.log('randomNum1 : ' + randomNum);
				
			    if(userSms == randomNum){ 			//인증번호가 같은 경우 행동
				  	alert("인증성공!");   
			    } else {		   					//인증번호가 다른 경우 행동
				  	alert("인증실패!");   
			    }
			});
				
		});
	});
	
	// 재전송 버튼
	$(function(){
		$("#sendSms2").click(function(){
			alert("인증번호가 발송되었습니다.");
			
			var tel1 = $("#tel1").val();
			var tel2 = $("#tel2").val();
			var phone = tel1 + tel2;
			//console.log(phone);

			var randomNum = "초기값"; 
			
			$.ajax({
				url: "/h/sendSms.api",
			    data: {
		    		phone: phone
		    	},
			    type: "post",
			    success: function(data) {
				    randomNum = data; //유저에게 문자로 보낸 랜덤 값을 그대로 리턴 받아서 전역변수로 선언한 randomNum에 넣어줬음
				    console.log("data : " + data);
			    },
			    error: function(error) {
			        console.log(error);
			    } 
			});
			
			// 인증번호 일치 여부 확인
			$("#checkSmsNumber").click(function(){
				var userSms = $("#checkSms").val(); // 회원가입 폼에서 유저가 문자를 보고 입력한 숫자를 가져옴
				console.log('userSms : ' + userSms);
				console.log('randomNum1 : ' + randomNum);
				
			    if(userSms == randomNum){ 			//인증번호가 같은 경우 행동
				  	alert("인증성공!");   
			    } else {		   					//인증번호가 다른 경우 행동
				  	alert("인증실패!");   
			    }
			});
				
		});
	});
	
</script>
</head>
<body>
	<div id="wrap">
	
	<!-- login area -->
	<div class="joinArea">
		<h1 id="joinTitle" align="center">회원정보 수정</h1>
		<br>
		<br>
		<div id=whiteBox> <!-- whiteBox -->
			<form id="joinForm" action="<%=request.getContextPath() %>/updateMember.me" method="post">
	 			<table class="outerTable"> 
	 				<tr>
	 					<td colspan="5"><p style="font-size: 20px; font-weight: bold; margin-bottom: 2px;">아이디·비밀번호 설정</p><hr style="border: 2px solid #013252;"></td>
	 				</tr>
					<tr>
						<td colspan="5"><br></td>
					</tr>
					<tr>
						<td>아이디</td>
						<td colspan="3">
							<p><%= loginUser.getMemberId()%></p>
							<!-- 숨겨진 input -->
							<input type="text" name="memberId" value="<%= loginUser.getMemberId()%>" hidden><!-- 숨겨진 input -->
						</td>
						<td>
						</td>
					</tr>
					<tr>
						<td>비밀번호(필수)</td>
						<td colspan="3">
							<input type="password" name="memberPwd" id="memberPwd" placeholder="영문,숫자,특수문자(!@#$만 허용)를 혼용하여 8~16자 입력">
						</td>
					</tr>
					<!-- 숨겨진 tr -->
					<tr>
						<td></td>
						<td colspan="3">
							<div class="alert alert-pwd-success" id="alert-pwd-success" style="color:green">올바른 비밀번호입니다.</div> 
							<div class="alert alert-pwd-danger" id="alert-pwd-danger" style="color:red">잘못된 비밀번호입니다.</div>
						</td>
					</tr><!-- 숨겨진 tr -->
					<tr>
						<td>비멀번호 재입력(필수)</td>
						<td colspan="3">
							<input type="password" name="memberPwd2" id="memberPwd2" placeholder="비밀번호 재확인">
						</td>
					</tr>
					<!-- 숨겨진 tr -->
					<tr>
						<td></td>
						<td colspan="3">
							<div class="alert alert-success" id="alert-success" style="color:green">비밀번호가 일치합니다.</div> 
							<div class="alert alert-danger" id="alert-danger" style="color:red">비밀번호가 일치하지 않습니다.</div>
						</td>
					</tr><!-- 숨겨진 tr -->
					
					<tr>
						<td colspan="5"><br><br><br></td>
					</tr>
			
			
	 				<tr>
	 					<td colspan="5"><p style="font-size: 20px; font-weight: bold; margin-bottom: 2px;">회원정보 입력</p><hr style="border: 2px solid #013252;"></td>
	 				</tr>
	 				<tr>
						<td colspan="5"><br></td>
					</tr>
					<tr>
						<td>이름(필수)</td>
						<td colspan="4">
							<input type="text" name="memberName" placeholder="이름 입력" value="<%= loginUser.getMemberName()%>">
						</td>
					</tr>
					<tr>
						<td>휴대폰(필수)</td>
						<td style="width: 100px;">
							<select id=tel1 name="tel1">
								<option selected value="010">010</option>
								<option value="011">011</option>
								<option value="016">016</option>
								<option value="017">017</option>
								<option value="018">018</option>
								<option value="019">019</option>
							</select>
						</td>
						<td colspan="2" >
							<input type="text" maxlength="8" name="tel2" id="tel2" style="width:250px;">
						</td>
						<td>
							<div class="btn btnDiv" id="sendSms">인증받기</div>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="3">
							<input type="text" name="sendNumber" id="checkSms" placeholder="인증번호 입력" style="width:285px; float:left; margin-right: 3px;">
							<div class="btn btnDiv" id="checkSmsNumber">확인</div>	
						</td>
						<td colspan="2">
							<div class="btn btnDiv" id="sendSms2" style="background:#AFAFAF; color:black">재전송</div>
						</td>
					</tr>
					
					<tr>
						<td>이메일(선택)</td>
						<td><input type="text" name="email1" id="email1" style="width:150px;" ></td>
						<td>@</td>
						<td><input type="text" name="email2" id="email2" style="width:232px;"></td>
					</tr>
					<tr>
						<td></td>
						<td colspan="3">
							<select id="email_select">
								<option selected value="0">직접 입력</option>
								<option value="naver.com">naver.com</option>
								<option value="hanmail.net">hanmail.net</option>
								<option value="daum.net">daum.net</option>
								<option value="gmail.com">gmail.com</option>
								<option value="nate.com">nate.com</option>
								<option value="dreamwiz.com">dreamwiz.com</option>
								<option value="hotmail.com">hotmail.com</option>
								<option value="korea.com">korea.com</option>
								<option value="lycos.com">lycos.com</option>
								<option value="yahoo.com">yahoo.com</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="10px;"><br><br></td>
					</tr>
					<tr>
						<td colspan="2"><h4>마케팅·이벤트정보 수신 동의(선택)</h4></td>
						<td colspan="3" style="text-align:right;" width="100px;">
							<label for="checkBox2">동의</label>
							<input type="checkBox" id="checkBox2" class="checkBox" style="width: 15px; height: 15px;">
						</td>
					</tr>
					<tr>
						<td colspan="6">
							<table id="innerTable" border=1 bordercolor="#ddddddd" style="border-collapse: collapse; padding: 5px" >
								<tr class="inserLine">
									<th style="padding: 5px" height="30px;">이용목적</th>
									<th style="padding: 5px" >개인정보의 항목</th>
									<th style="padding: 5px" >보유 및 이용기간</th>
								</tr>
								<tr class="inserLine">
									<td width="40%" height="100px;" style="padding: 5px">이용자에게 최적화된 서비스 제공(회원 맞춤 서비스)신규 서비스 및 상품 개발을 위한 서비스 이용현황 통계/분석 해볼래 보금자리 등의 이벤트 기획 간행물 발송, 다양한 정보와 이벤트 소식 제공(이메일, 전화, 문자)</td>
									<td width="35%" style="padding: 5px">이름, 상호(기업)명, 휴대폰 번호, 이메일</td>
									<td width="25%" style="padding: 5px"><b>회원 탈퇴까지 또는 고객요청에 따라 개인정보 이용동의 철회 요청시까지</b></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan=5>
							<br><br><br>
							<div style="width:400px; margin: 0 auto">
								<button type="button" class="btn2 backBtn" style="margin-right:40px" onclick="back();">뒤로가기</button>
								<button class="btn2 agreeBtn" onclick="agree();"><b>수정완료</b></button>
							</div>
						</td>
					</tr>
				</table>
			</form>
		</div> <!-- whiteBox -->
		<br><br><br>
		<footer>
		<table >
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div><!-- /login area -->
	</div>
</body>
</html>