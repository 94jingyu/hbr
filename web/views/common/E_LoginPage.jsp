<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	#wrap {width: 1200px; margin: 0 auto; height:100px; top:20%; margin-top:50px;}
	#whiteBox {width:460px; height:100%; padding-top: 0; padding-left: 40px; padding-right: 40px; padding-bottom: 40px; border: 1px solid #C4C4C4; margin: 0 auto; background: white;}
	.tap{width: 268px; height: 50px; text-align: center; font-size: 20px; padding-top: 20px; margin-bottom: 40px; border: 1px solid #C4C4C4; bordre-bottom: 0;}
	.tap1{float:left; margin-left:-40px; border-bottom: 0; font-weight: bold;}
	.tap2{float:right; margin-right:-40px; background: #EFEFEF}
	.tap:hover{cursor:pointer;}
	input {width:400px; height:40px; font-size:15px; margin: 2px;}
	#loginBtn {width:405px; height:50px; font-size:20px; background: #013252; color: white; padding: 5px; margin: 2px; border-radius:10px; border: 1px; outline: 1px;}
	a {text-decoration: none; color: black;}
	#loginBtn, .btn:hover{cursor:pointer;}
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
<script>
    function Join(){ 
    	location.href = "<%=request.getContextPath()%>/views/common/E_JoinPage.jsp";
    };
    function FindId(){ 
    	location.href = "<%=request.getContextPath()%>/views/common/E_Find_Id1.jsp";
    };
    function FindPwd(){ 
    	location.href = "<%=request.getContextPath()%>/views/common/E_Find_Pwd1.jsp";
    };
    
    function user_tap() {
    	var tap1 = document.getElementById("user_tap");
    	tap1.style.backgroundColor = "white";
     	tap1.style.borderBottom = 0;
    	tap1.style.fontWeight = "bold";

    	var tap2 = document.getElementById("business_tap");
    	tap2.style.backgroundColor = "#EFEFEF";
    	tap2.style.border = "1px solid #C4C4C4";
    	tap2.style.fontWeight= "normal";
    	
    	$("#loginType").val("0");
    	
    };
    
    function business_tap() {
    	var tap1 = document.getElementById("user_tap");
    	tap1.style.backgroundColor = "#EFEFEF";
    	tap1.style.border = "1px solid #C4C4C4";
    	tap1.style.fontWeight= "normal";
    	
    	var tap2 = document.getElementById("business_tap");
    	tap2.style.backgroundColor = "white";
     	tap2.style.borderBottom = 0;
    	tap2.style.fontWeight = "bold";
    	
    	$("#loginType").val("1");
    };  
    
    
</script>
</head>
<body>
	<div id="wrap">
	
	<!-- login area -->
	<div class="loginArea">
		<h1 id="loginTitle" align="center">로그인</h1>
		<br>
		<div id="loginContext" align="center">보금자리 회원이 아니면, 지금 <a class="btn JoinBtn" onclick="Join();"><b>회원가입</b></a>을 해주세요.</div>
		<br>
		<div id=whiteBox> <!-- whiteBox -->
		<div class="tap tap1" id="user_tap" onclick="user_tap();">개인회원</div>
		<div class="tap tap2" id="business_tap" onclick="business_tap();">기업회원</div>
			<form id="loginForm" action="<%= request.getContextPath() %>/login.me" method="post">
	 			<table style="margin: 0 auto">
					<tr>
						<td colspan="5"><input type="text" name="memberId" placeholder="아이디를 입력하세요"></td>
					</tr>
					<tr>
						<td colspan="5"><input type="password" name="memberPwd" placeholder="비밀번호를 입력하세요"></td>
					</tr>
					<tr>
						<td colspan="5"><button id="loginBtn" >로그인</button></td>
					</tr>
					<tr>
					<td colspan="5" height="20px;"></td>
					</tr>
					<tr>
						<td width="45px;"></td>
						<td><a class="btn" onclick="FindId();">아이디 찾기</a></td>
						<td><a class="btn" onclick="FindPwd()">비밀번호 찾기</a></td>
						<td><a class="btn JoinBtn" onclick="Join();"><b>회원가입</b></a></td>
						<td width="45px;"></td>
					</tr>
				</table>
				<!-- 숨겨진 input -->
				<input type="text" name="loginType" id="loginType" value="0" hidden>	
			</form>
		</div> <!-- whiteBox -->
		<br><br><br>
		<footer>
		<table>
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div><!-- /login area -->
	</div>
	
</body>
</html>