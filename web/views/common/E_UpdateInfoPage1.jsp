<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	#wrap {width: 1200px; margin: 0 auto; height:100px; top:20%; margin-top:50px;}
	#whiteBox {width:500px; height:70px; padding: 50px; padding-top:50px; padding-right: 20px; border: 1px solid #C4C4C4; margin: 0 auto; background: white;}
	input {width:400px; height:40px; font-size:15px; margin: 2px;}
	.nextBtn {width:50px; height:50px; font-size:20px; background: #259ADA; color: white; padding: 5px; margin: 2px; padding-left: 8px; border-radius:100px; border: 1px; outline: 1px;}
	a {text-decoration: none; color: black;}
	.nextBtn, .btn:hover{cursor:pointer;}
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
<script>
    function next(){ 
    	location.href = "<%=request.getContextPath()%>/views/common/E_UpdateInfoPage2.jsp";
    };
    
    function del() {
    	location.href = "<%=request.getContextPath()%>/views/common/E_DeletePage1.jsp";
    };
    

</script>
</head>
<body>
	<div id="wrap">
	
	<!-- area -->
	<div class="Area">
		<h1 id="Title" align="center">회원정보 관리</h1>
		<br>

		<div id=whiteBox> <!-- whiteBox -->
			<form form id="updateForm" action="<%= request.getContextPath() %>/views/common/E_UpdateInfoPage2.jsp" method="post">
	 			<table>
					<tr>
						<td rowspan="2">
							<img alt="logo.png" src="../../static/images/common/user.png" 
								width="60px" height="60px" style="margin-left: auto; margin-right: 20px; display: inline-block";>
						</td>
						<td><h2>회원정보 수정</h2></td>
						<td rowspan="2"><button class="btn nextBtn" style="margin-left:200px"><b style="font-size: 25px;" onclick="next();"> 〉</b></button></td>
					</tr>
					<tr>
						<td colspan="2"><p >연락처, 이메일 등을 수정할 수 있습니다.</p></td>
					</tr>
					<tr>
					</tr>
					<tr>
						<td colspan="5" height="20px;"></td>
					</tr>
				</table>
			</form>	
		</div> <!-- whiteBox -->
		<br>
		<div id="loginContext" align="center" style="width:1120px">더 이상 서비스를 이용하지 않을 경우 <a class="btn" style="text-decoration: underline; margin-right:3px;" onclick="del();" ><b>회원탈퇴</b></a>하기</div>
		<br>
		<br><br><br>
		<footer>
		<table >
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div><!-- /login area -->
	</div>
</body>
</html>