<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	#wrap {width: 1200px; margin: 0 auto; height:100px; top:20%; margin-top:50px;}
	#whiteBox {width:720px; height:100%; padding: 40px; border: 1px solid #C4C4C4; margin: 0 auto; background: white;}
	a {text-decoration: none; color: black;}
	input {width:410px; height:40px; font-size:15px; margin: 2px;}
	#tel1 {width:154px; height:42px; font-size:15px; margin: 2px;}
	.btn {width:120px; height:44px; font-size:15px; margin: 2px; background: #666666; color:#C2C2C2; border: none; text-align: center;}
	.btnDiv{width:120px; text-align: center; display:table-cell;vertical-align:middle;}
	#email_select {width:413px; height:42px; font-size:15px; margin: 2px;}
	
	textArea {color: #646464; width: 100%;}	
 	.agreeBtn {margin: 0 auto; width: 120px;}
	#backBtn {float: left; width:90px; height:50px; font-size:17px; background-color: transparent !important; 
		background-image: none !important; border-color: transparent; border: none; margin: 2px;}
	#agreeBtn {width:150px; height:50px; font-size:20px; background: #013252; color: white; padding: 5px; margin: 2px; 
		border-radius:10px; border: 1px; outline: 1px;} 
	.btn, #agreeBtn, #backBtn:hover{cursor:pointer;}
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
	<script>
	$(function(){
		// 최종 확인버튼
		$("#agreeBtn").click(function(){
			var memberId = $("#memberId").val();
			var memberPwd = $("#memberPwd").val();
			var memberPwd2 = $("#memberPwd2").val();
			var memberName = $("#memberName").val();
			var tel2 = $("#tel2").val();

			if(memberId === "") {
				alert("아이디를 입력해주세요.");
			} else if(memberPwd === "") {
				alert("비밀번호를 입력해주세요.");
			} else if(memberName === "") {
				alert("이름를 입력해주세요.");
			} else if(tel2 === "") {
				alert("휴대폰 번호를 입력해주세요.");
			} else {
				$("#joinForm").submit();
			}
		});
	});
			
	
	function back() {
		history.back();
	}

	
	// 아이디 중복 및 유효성 검사
 	$(function(){
 		$("#idCheck").click(function(){
 			// 아이디 유효성 검사
 			var idCheck = /^[a-zA-Z0-9]{4,12}$/;
 			var memberId = $("#memberId").val();
 			console.log(memberId);
 			
 			if(memberId !== "") {
 				// 아이디 유효성이 맞을 때		
 	 			if(idCheck.test(memberId)) {
 		 			// 아이디 중복검사
 	 	 			$.ajax({
 	 	 				url: "/h/idCheck.me",
 	 	 				type: "post",
 	 	 				data: {memberId: memberId},
 	 	 				success: function(data) {
 	 	 					if(data === "fail") {
 	 	 						alert("아이디가 중복됩니다.");
 	 	 					} else {
 	 	 						alert("사용 가능합니다.");
 	 	 					}
 	 	 				},
 	 	 				error: function(error){
 	 	 					console.log(error);
 	 	 				}
 	 	 			});
 	 			// 유효성에 어긋날 때	
 				} else {
 					alert("사용 불가능한 아이디입니다.");
 				}
 			} else {
 				alert("아이디를 입력해주세요.");
 			}
 			
 		});
 		
 	});		
 	
  	// 비밀번호 유효성 검사
  	$(function(){
  		var pwdCheck = /([a-zA-Z0-9].*[!,@,#,$])|([!,@,#,$].*[a-zA-Z0-9])/;
  		
  		$("#alert-pwd-success").hide();
		$("#alert-pwd-danger").hide();
		$("#alert-success").hide();
		$("#alert-danger").hide();
		
		$("input").change(function(){
			var memberPwd = $("#memberPwd").val();
			var memberPwd2 = $("#memberPwd2").val();
			
			// 비밀번호 유효성 검사
			if(memberPwd != "") {
				if(pwdCheck.test(memberPwd)) {
					$("#alert-pwd-success").show();
					$("#alert-pwd-danger").hide();
				} else {
					$("#alert-pwd-success").hide();
					$("#alert-pwd-danger").show();
				}
			} else {
				$("#alert-pwd-success").hide();
				$("#alert-pwd-danger").hide();
			}
			
			// 비밀번호 재입력  검사
			if(memberPwd != "" && memberPwd2 != "") {
				if(memberPwd == memberPwd2) {
					$("#alert-success").show();
					$("#alert-danger").hide();
					$("#sumit").removeAttr("disabled");
				} else {
					$("#alert-success").hide(); 
					$("#alert-danger").show(); 
					$("#submit").attr("disabled", "disabled");
				}
			} else {
				$("#alert-success").hide();
				$("#alert-danger").hide();
			}
			
		}); 
		
 	});
	
	// 이메일 select
	$(function(){
		$("#email_select").change(function(){
			if($("#email_select option:selected").text() == "직접 입력"){
				var value = ""; 
			} else {
				value = $("#email_select option:selected").text(); // 이외 메일 주소 선택일 경우 selectbox에서 선택한 값
			   	$("#email2").val(value);                           // 값 입력됨
			}
		});
	});
	 
	
	// 핸드폰 문자 발송 (from 용훈이형)
	$(function(){
		$("#sendSms").click(function(){
			alert("인증번호가 발송되었습니다.");
			
			var tel1 = $("#tel1").val();
			var tel2 = $("#tel2").val();
			var phone = tel1 + tel2;
			//console.log(phone);

			var randomNum = "초기값"; //유저에게 보낸 문자의 랜덤 숫자를 저장하기 위한 전역변수
			
			$.ajax({
				url: "/h/sendSms.api",
			    data: {
		    		phone: phone
		    	},
			    type: "post",
			    success: function(data) {
				    randomNum = data; //유저에게 문자로 보낸 랜덤 값을 그대로 리턴 받아서 전역변수로 선언한 randomNum에 넣어줬음
				    console.log("data : " + data);
			    },
			    error: function(error) {
			        console.log(error);
			    } 
			});
			
			// 인증번호 일치 여부 확인
			$("#checkSmsNumber").click(function(){
				var userSms = $("#checkSms").val(); // 회원가입 폼에서 유저가 문자를 보고 입력한 숫자를 가져옴
				// console.log('userSms : ' + userSms);
				// console.log('randomNum1 : ' + randomNum);
				
			    if(userSms == randomNum){ 			//인증번호가 같은 경우 행동
				  	alert("인증성공!");   
			    } else {		   					//인증번호가 다른 경우 행동
				  	alert("인증실패!");   
			    }
			});
				
		});
	});
	
	// 재전송 버튼
	$(function(){
		$("#sendSms2").click(function(){
			alert("인증번호가 발송되었습니다.");
			
			var tel1 = $("#tel1").val();
			var tel2 = $("#tel2").val();
			var phone = tel1 + tel2;
			//console.log(phone);

			var randomNum = "초기값"; 
			
			$.ajax({
				url: "/h/sendSms.api",
			    data: {
		    		phone: phone
		    	},
			    type: "post",
			    success: function(data) {
				    randomNum = data; //유저에게 문자로 보낸 랜덤 값을 그대로 리턴 받아서 전역변수로 선언한 randomNum에 넣어줬음
				    console.log("data : " + data);
			    },
			    error: function(error) {
			        console.log(error);
			    } 
			});
			
			// 인증번호 일치 여부 확인
			$("#checkSmsNumber").click(function(){
				var userSms = $("#checkSms").val(); // 회원가입 폼에서 유저가 문자를 보고 입력한 숫자를 가져옴
				console.log('userSms : ' + userSms);
				console.log('randomNum1 : ' + randomNum);
				
			    if(userSms == randomNum){ 			//인증번호가 같은 경우 행동
				  	alert("인증성공!");   
			    } else {		   					//인증번호가 다른 경우 행동
				  	alert("인증실패!");   
			    }
			});
				
		});
	});
	
</script>
</head>
<body>
	<div id="wrap">
	
	<!-- login area -->
	<div class="joinArea">
		<h1 id="joinTitle" align="center">개인 회원가입</h1>
		<br>
		<br>
		<div id=whiteBox> <!-- whiteBox -->
			<form id="joinForm" action="<%=request.getContextPath() %>/insertMember.me" method="post">
	 			<table>
	 				<tr>
	 					<td colspan="5"><p style="font-size: 20px; font-weight: bold; margin-bottom: 2px;">아이디·비밀번호 설정</p><hr style="border: 2px solid #013252;"></td>
	 				</tr>
					<tr>
						<td colspan="5"><br></td>
					</tr>
					<tr>
						<td>아이디(필수)</td>
						<td colspan="3">
							<input type="text" name="memberId" id="memberId" placeholder="4~15자리의 영문 소문자, 숫자">
						</td>
						<td>
							<div class="btn btnDiv" id="idCheck">중복확인</div>
						</td>
					</tr>
					<tr>
						<td>비밀번호(필수)</td>
						<td colspan="3">
							<input type="password" name="memberPwd" id="memberPwd" placeholder="영문,숫자,특수문자(!@#$만 허용)를 혼용하여 8~16자 입력">
						</td>
					</tr>
					<!-- 숨겨진 tr -->
					<tr>
						<td></td>
						<td colspan="3">
							<div class="alert alert-pwd-success" id="alert-pwd-success" style="color:green">올바른 비밀번호입니다.</div> 
							<div class="alert alert-pwd-danger" id="alert-pwd-danger" style="color:red">잘못된 비밀번호입니다.</div>
						</td>
					</tr><!-- 숨겨진 tr -->
					<tr>
						<td>비멀번호 재입력(필수)</td>
						<td colspan="3">
							<input type="password" name="memberPwd2" id="memberPwd2" placeholder="비밀번호 재확인">
						</td>
					</tr>
					<!-- 숨겨진 tr -->
					<tr>
						<td></td>
						<td colspan="3">
							<div class="alert alert-success" id="alert-success" style="color:green">비밀번호가 일치합니다.</div> 
							<div class="alert alert-danger" id="alert-danger" style="color:red">비밀번호가 일치하지 않습니다.</div>
						</td>
					</tr><!-- 숨겨진 tr -->
					
					<tr>
						<td colspan="5"><br><br><br></td>
					</tr>
			
			
	 				<tr>
	 					<td colspan="5"><p style="font-size: 20px; font-weight: bold; margin-bottom: 2px;">회원정보 입력</p><hr style="border: 2px solid #013252;"></td>
	 				</tr>
	 				<tr>
						<td colspan="5"><br></td>
					</tr>
					<tr>
						<td>이름(필수)</td>
						<td colspan="4">
							<input type="text" id="memberName" name="memberName" placeholder="이름 입력">
						</td>
					</tr>
					<tr>
						<td>휴대폰(필수)</td>
						<td style="width: 100px;">
							<select id=tel1 name="tel1">
								<option selected value="010">010</option>
								<option value="011">011</option>
								<option value="016">016</option>
								<option value="017">017</option>
								<option value="018">018</option>
								<option value="019">019</option>
							</select>
						</td>
						<td colspan="2" >
							<input type="text" maxlength="8" name="tel2" id="tel2" style="width:250px;">
						</td>
						<td>
							<div class="btn btnDiv" id="sendSms">인증받기</div>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="3">
							<input type="text" name="sendNumber" id="checkSms" placeholder="인증번호 입력" style="width:285px; float:left; margin-right: 3px;">
							<div class="btn btnDiv" id="checkSmsNumber">확인</div>	
						</td>
						<td colspan="2">
							<div class="btn btnDiv" id="sendSms2" style="background:#AFAFAF; color:black">재전송</div>
						</td>
					</tr>
					
					<tr>
						<td>이메일(선택)</td>
						<td><input type="text" name="email1" id="email1" style="width:150px;"></td>
						<td>@</td>
						<td><input type="text" name="email2" id="email2" style="width:232px;"></td>
					</tr>
					<tr>
						<td></td>
						<td colspan="3">
							<select id="email_select">
								<option selected value="0">직접 입력</option>
								<option value="naver.com">naver.com</option>
								<option value="hanmail.net">hanmail.net</option>
								<option value="daum.net">daum.net</option>
								<option value="gmail.com">gmail.com</option>
								<option value="nate.com">nate.com</option>
								<option value="dreamwiz.com">dreamwiz.com</option>
								<option value="hotmail.com">hotmail.com</option>
								<option value="korea.com">korea.com</option>
								<option value="lycos.com">lycos.com</option>
								<option value="yahoo.com">yahoo.com</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="10px;"><br><br></td>
					</tr>
				</table>
				<!-- 숨겨진 input -->
				<input type="text" name="loginType" id="loginType" value="0" hidden>	
 			<div><button type="button" id="backBtn" onclick="back();"><b>이전으로</b></button></div>
			<div class=agreeBtn><button type="button" id="agreeBtn"><b>확인</b></button></div>
			</form>
			<br>
		</div> <!-- whiteBox -->
		<br><br><br>
		<footer>
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div><!-- /login area -->
	</div>
	
</body>
</html>