<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	#wrap {margin-left: 100px; width: 1200px; margin: 0 auto; height:100px; top:20%; margin-top:50px;}
	#whiteBoxLeft {display: inline-block; margin-left: 17%; width:290px; height:370px; padding: 50px; border: 1px solid #C4C4C4; background: white;}
	#whiteBoxRight {display: inline-block;  width:290px; height:370px; padding: 50px; border: 1px solid #C4C4C4; background: white;}
	.join{margin-top: 50px; text-align:center}
	a {text-decoration: none; color: black;}
	footer{clear:both; margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
	#whiteBoxLeft, #whiteBoxRight:hover{cursor:pointer;}
</style>
<script>
	function userJoin(){
		location.href = "<%=request.getContextPath()%>/views/common/E_user_JoinPage1.jsp";
	}

	function businessJoin() {
		location.href = "<%=request.getContextPath()%>/views/common/E_business_JoinPage1.jsp";
	}
</script>
</head>
<body>
	<div id="wrap">
	
	<!-- login area -->
	<div class="loginArea">
		<h1 id="loginTitle" align="center">회원가입</h1>
		<br>
		<div id="loginContext" align="center">회원가입을 하시면 더 많은 서비스를 이용할 수 있습니다.</div>
		<br><br>
		<div id=whiteBoxLeft onclick="userJoin();"> <!-- whiteBox -->
 			<div class="user join">
				<img alt="" src="../../static/images/common/user.png" 
					width="75px" height="80px" style="margin-left: auto; margin-right: auto; display: block";>
				<br>
				<h2>개인 회원가입</h2>
				<br>
				<p>일자리를 구하거나<br>구직정보를 얻고 싶은 경우에는<br>개인회원으로 가입해주세요</p>
			</div>
		</div> <!-- whiteBox -->
		<div id=whiteBoxRight> <!-- whiteBox -->

				<div class="business join" onclick="businessJoin();"> 
				<img alt="" src="../../static/images/common/business.png" 
					width="75px" height="80px" style="margin-left: auto; margin-right: auto; display: block";>
				<br>
				<h2>기업 회원가입</h2><br>
				<p>직원을 구하거나<br>기업 광고를 등록하려는 경우에는<br>기업 회원으로 가입해주세요</p>
			</div> 
			
		</div> <!-- whiteBox -->
		<br><br><br>
		<footer>
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div><!-- /login area -->
	</div>
</body>
</html>