<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	#wrap {width: 1200px; margin: 0 auto; height:100px; top:20%; margin-top:50px;}
	#whiteBox {width:700px; height:300px; padding: 40px; border: 1px solid #C4C4C4; margin: 0 auto; background: white;}
	input {width:400px; height:40px; font-size:15px; margin: 2px;}
	#insertBtn {width:200px; height:50px; font-size:20px; background: #013252; color: white; padding: 5px; margin: 2px; border-radius:10px; border: 1px; outline: 1px;}
	#goHome {width:200px; height:50px; font-size:20px; background: white; color: black; padding: 5px; margin: 2px; border-radius:10px; border: 1px solid black;}
	a {text-decoration: none; color: black;}
	#insertBtn, #goHome, .btn:hover{cursor:pointer;}
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
<script>
    function product(){ 
    	location.href = "<%=request.getContextPath()%>/myItem.load";
    };
    function insertBtn(){ 
    	location.href = "<%=request.getContextPath()%>/views/boguem_dj/DJ_boguem_register.jsp";
    };
    function goHome(){ 
    	location.href = "<%=request.getContextPath()%>/views/guide/user_mainPage.jsp";
    };
</script>
</head>
<body>
	<div id="wrap">
	
	<!-- login area -->
	<div class="loginArea">
		<h1 id="loginTitle" align="center">회원가입이 완료되었습니다.</h1>
		<br>
		<div id="loginContext" align="center">보금자리 <a class="btn JoinBtn" onclick="product();"><b>이력서 열람권</b></a>이 발급되었습니다.</div>
		<br>
		<div id=whiteBox> <!-- whiteBox -->
			<img alt="logo.png" class="btn" src="../../static/images/common/product.png" 
				width="100px" height="150px" style="margin-left: auto; margin-right: auto; display: block"
				onclick="product();">
			<br>
			<p align="center">현재 <b style="color:#669DF5">20,125명</b>의 구직자가 회원님의 일자리를 기다리고 있습니다.<br> 
			지금 바로 보금자리를 등록하고 직원 채용을 시작해보세요!<p>
			<br>
			<div align="center">
				<button id="insertBtn" onclick="insertBtn();">보금자리 등록</button>
				<button id="goHome" onclick="goHome()">보금자리 홈</button>
			</div>		

		</div> <!-- whiteBox -->
		<br><br><br>
		<footer>
		<table >
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div><!-- /login area -->
	</div>
</body>
</html>