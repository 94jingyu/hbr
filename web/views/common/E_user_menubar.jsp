<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.hbr.member.model.vo.Member, com.kh.hbr.member.model.vo.Business"%>
<%
	//System.out.println(session.getAttribute("loginType"));
	int loginType = 99;
	Member loginUser = null;
	Business bloginUser = null;

	if(session.getAttribute("loginType") != null){
		loginType = (Integer) session.getAttribute("loginType");  
	}
	
	if(loginType == 0) {
		loginUser = (Member) session.getAttribute("loginUser");
		/* System.out.println(loginType); */
	
	} else if(loginType == 1){
		bloginUser = (Business) session.getAttribute("loginUser");
		/* System.out.println(loginType); */
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>


<style>
	* {margin: 0; padding: 0;}
	#wrap {width: 1200px; margin: 0 auto;}
	
	/* 헤더 영역 */
	.logo {float: left; width: 21%; height: 130px; padding-top: 10px;}
	
	.toplogin {width: 100%; height: 20px; padding-top: 15px; /* border: 1px solid black; */}
	
	/* 상단메뉴바 */
 	.topMenu {
    	float: right;
    	width: 940px;
    	height: 75px;
    	line-height: 30px;          /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	    vertical-align: middle;     /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	    text-align: center;         /* 글씨 정렬을 가운데로 설정 */  
  	}
  	.topMenu>div {float: right; vertical-align: middle;}
  	.topMenu>ul {
   		float: left;
    	margin: 0;
    	padding: 0;
    	list-style: none;
  	}
  	.topMenu>ul>li {
    	display: inline-block;
    	
  	}
  	.topMenu a {
   		display: block;
    	text-decoration: none;
    	color: black;
    	font-size: 17px;
    	padding: 20px;
    	font-size: 20px;
  	}
  	.topMenu a:hover {background:#013252; color:white; } 
  
	
	/* 하이퍼텍스트 효과 */
	a {text-decoration: none;}      /* 하이퍼텍스트 밑줄 효과 없애기 */
	
	/* 버튼 효과 */
	.btns {align:right;}
 	#loginBtn, #memberJoinBtn{
		display:inline-block;
		text-align:center;
		width:80px;
		border-radius:5px;
	} 
	
	#loginBtn:hover, #memberJoinBtn:hover{background:#ECF0F7;}
	
	/* Dropdown */
	
	.dropdown {
	  display: inline-block;
	  position: relative;
	}
	
	.dd-button {
	  display: inline-block;
	  border: 0px solid gray;
	  border-radius: 4px;
	  padding-right: 25px;
	  padding-left: 10px;
	  margin-right: 10px;
	  background-color: #ffffff;
	  cursor: pointer;
	  white-space: nowrap;
	  color: #013252;
	}
	
	.dd-button:after {
	  content: '';
	  position: absolute;
	  top: 50%;
	  right: 15px;
	  transform: translateY(-50%);
	  width: 0; 
	  height: 0; 
	  border-left: 5px solid transparent;
	  border-right: 5px solid transparent;
	  border-top: 5px solid black;
	}
	
	.dd-button:hover {
	  background-color: #ECF0F7;
	}
	
	
	.dd-input {
	  display: none;
	}
	
	.dd-menu {
	  position: absolute;
	  top: 100%;
	  border: 1px solid #ccc;
	  border-radius: 4px;
	  padding: 0;
	  margin: 2px 0 0 0;
	  box-shadow: 0 0 6px 0 rgba(0,0,0,0.1);
	  background-color: #ffffff;
	  list-style-type: none;
	}
	
	.dd-input + .dd-menu {
	  display: none;
	} 
	
	.dd-input:checked + .dd-menu {
	  display: block;
	} 
	
	.dd-menu li {
	  padding: 10px 20px;
	  cursor: pointer;
	  white-space: nowrap;
	}
	
	.dd-menu li:hover {
	  background-color: #f6f6f6;
	}
	
	.dd-menu li a {
	  display: block;
	  margin: -10px -20px;
	  padding: 10px 20px;
	}
	
	.dd-menu li.divider{
	  padding: 0;
	  border-bottom: 1px solid #cccccc;
	}

	
	
</style>
<script>
    function login(){ 
    	location.href = "<%=request.getContextPath()%>/views/common/E_LoginPage.jsp";
    };
    
    function memberJoin() {
    	location.href = "<%=request.getContextPath()%>/views/common/E_JoinPage.jsp";
    }
    
    function find(){ 
    	location.href = "<%=request.getContextPath()%>/views/common/E_Find_Id1.jsp";
    };
    
    function changeInfo() {
    	location.href = "<%=request.getContextPath()%>/views/common/E_UpdateInfoPage1.jsp";
    }
    
    
    function changeInfo2() {
    	location.href = "<%=request.getContextPath()%>/views/common/E_business_UpdateInfoPage1.jsp";
    }
    
    function goAdmin() {
    	location.href = "<%=request.getContextPath()%>/views/guide/admin_mainPage.jsp";
    }
    
    
	function logout() {
		var check = window.confirm("로그아웃 하시겠습니까?");
		
		if(check) {
			location.href = "<%=request.getContextPath()%>/logout.me";
		}
	}
    
	function searchRecruit() {
		<%-- location.href = "<%=request.getContextPath()%>/views/user_TAEWON/user_searchRecruit.jsp"; --%>
	      location.href = "<%= request.getContextPath()%>/selectListSearch.rc";
	}
	

	function applicantSearch() {
		location.href = "<%=request.getContextPath()%>/views/business_DY/DY_business_ApplicantSearch.jsp";
	}
	
	
	function openField() {
		location.href = "<%=request.getContextPath()%>/selectJenguk.tn";
	
	
	
	}
	
	
	function resumeWrite() {
		location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/resumeWrite.jsp";
	}
	function userMypage() {
		
		location.href = "<%=request.getContextPath()%>/user.my";
	}
	
	
	function businessWrite() {
		location.href = "<%=request.getContextPath()%>/views/boguem_dj/DJ_boguem_register.jsp";
	}
	function businessMypage() {
		location.href = "<%=request.getContextPath()%>/views/business_DY/DY_business_MyPage.jsp";
	}
    
    
</script>
</head>
<body>
	<div id="wrap">
        <!-- 로고 영역 -->
        <div class="logo">
        	<a href="<%=request.getContextPath()%>/views/guide/user_mainPage.jsp"><img alt="logo.png" src="<%=request.getContextPath()%>/static/images/user/logo.png" width="250px" height="100%" style="margin-left: auto; margin-right: auto; display: block"></a>
        </div>
        
		<!-- 상단 로그정보 영역 -->
        <div class="toplogin" align="right">
        <% if(loginUser == null && bloginUser == null) {%>
        	<div class="btns">
				<div id="loginBtn" class="btn" onclick="login();">로그인</div>
				<div id="memberJoinBtn" class="btn" onclick="memberJoin();">회원가입</div>
			</div>
        </div>
        <div style="height: 45px"></div>
        <% } else { %>
        <div id="userInfo">
        	<table style="padding:0; padding-bottom:40px; float:right" >
        		<tr>
        			<td>
						<label class="dropdown">
							  <div class="dd-button" style="display:inline-block">
								  <%if(loginType == 0) {%>
								  <b><%=loginUser.getMemberName() %></b>님
								  <% } else if(loginType == 1) {  %>
								  <b><%=bloginUser.getManagerName() %></b>님
								  <% } %>
							  </div>
							  <input type="checkbox" class="dd-input" id="test">
							  <ul class="dd-menu">
							      <% if(loginType == 0) {
									      if(!loginUser.getMemberId().equals("admin")){ %>   <!-- 느낌표 주의하기 -->
									      	<li onclick="changeInfo();">회원정보 수정</li>
									      <% } else { %>
									      	<li onclick="goAdmin();">관리자 메뉴가기</li>
										  <% }
									 } else { %>	
								     <li onclick="changeInfo2();">회원정보 수정</li>
								  <% } %>						    	
						    	  <li onclick="logout();">로그아웃</li>
							  </ul>
						</label>			
        			</td>
        			
        		</tr>
        	</table>
		</div>
	<% } %>
      
        
        <!-- 상단 메뉴바 -->
        <nav class="topMenu">
		  	<ul>
		   		<li><a href="#" onclick="searchRecruit();" >채용정보</a></li>
		    	<li><a href="#" onclick="applicantSearch();" >인재정보</a></li>
		    	<li><a href="#" onclick="openField();" >열린마당</a></li>
		    	<li><a href="#" onclick="alert('기능 구현중입니다.')" >취업박사</a></li>
		  	  	<li><a href="#" onclick="" >고객센터</a></li>
		  	</ul>
		  	  <% if(loginUser == null && bloginUser == null) {%>
   		  	  <div onclick="resumeWrite();"><a href="#">이력서등록</a></div>
 		  	  <div onclick="userMypage();"><a href="#">마이페이지</a></div>
		  	  <% } else {	
			  	  	 if(loginType == 0) {
			  	  	 	if(loginUser.getMemberId().equals("admin")) {%>
			   		  	  <div onclick="goAdmin();"><a href="#">관리자 메뉴가기</a></div>
			  	  	 	<% } else { %>
			   		  	  <div onclick="resumeWrite();"><a href="#">이력서등록</a></div>
			   		  	  <div onclick="userMypage();"><a href="#">마이페이지</a></div>
			  	  	 	<% } %>
				  <% } else if(loginType == 1) {  %>
	   		  	  <div onclick="businessWrite();"><a href ="#">보금자리등록</a></div>
	   		  	  <div onclick="businessMypage();"><a href="#">마이페이지</a></div>
				  <% } 
		  	  }%>
		</nav>