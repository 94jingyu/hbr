<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	#wrap {width: 1200px; margin: 0 auto; height:100px; top:20%; margin-top:50px;}
	.tap{width: 268px; height: 50px; text-align: center; font-size: 20px; padding-top: 20px; margin-bottom: 40px; border: 1px solid #C4C4C4; bordre-bottom: 0;}
	.tap1{float:left; margin-left:-40px; border-bottom: 0; font-weight: bold;}
	.tap2{float:right; margin-right:-40px; background: #EFEFEF}
	.btn, .tap:hover{cursor:pointer;}
	#whiteBox {width:460px; height:400px; padding-top: 0; padding-left: 40px; padding-right: 40px; padding-bottom: 40px; border: 1px solid #C4C4C4; margin: 0 auto; background: white;}
	input{height:45px; font-size:15px; margin: 1px;} 
	select{width: 80px; height:47px; font-size:15px; margin: 1px;}
	button{width: 90px; height: 50px; margin: 1px; font-size: 15px;}
	
	#checkBtn {width:445px; height:50px; font-size:20px; background: #013252; color: white; padding: 5px; margin: 2px; border-radius:10px; border: 1px; outline: 1px;}
	a {text-decoration: none; color: black;}
	#checkBtn, .JoinBtn:hover{cursor:pointer;}
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
<script>
    function check(){ 
    	/* alert("전송완료"); */
    	$("#findForm").submit();
    	
    };
    
    function user_tap() {
    	var tap1 = document.getElementById("user_tap");
    	tap1.style.backgroundColor = "white";
     	tap1.style.borderBottom = 0;
    	tap1.style.fontWeight = "bold";

    	var tap2 = document.getElementById("business_tap");
    	tap2.style.backgroundColor = "#EFEFEF";
    	tap2.style.border = "1px solid #C4C4C4";
    	tap2.style.fontWeight= "normal";
    	
    	$("#loginType").val("0");
    	
    };
    
    function business_tap() {
    	var tap1 = document.getElementById("user_tap");
    	tap1.style.backgroundColor = "#EFEFEF";
    	tap1.style.border = "1px solid #C4C4C4";
    	tap1.style.fontWeight= "normal";
    	
    	var tap2 = document.getElementById("business_tap");
    	tap2.style.backgroundColor = "white";
     	tap2.style.borderBottom = 0;
    	tap2.style.fontWeight = "bold";
    	
    	// input 초기화
    	$("input").val("");
    	$("#loginType").val("1");
    };  
    
 	// 핸드폰 문자 발송 (from 용훈이형)
	$(function(){
		$("#sendSms").click(function(){
			alert("인증번호가 발송되었습니다.");
			
			var tel1 = $("#tel1").val();
			var tel2 = $("#tel2").val();
			var phone = tel1 + tel2;
			//console.log(phone);

			var randomNum = "초기값"; //유저에게 보낸 문자의 랜덤 숫자를 저장하기 위한 전역변수
			
			$.ajax({
				url: "/h/sendSms.api",
			    data: {
		    		phone: phone
		    	},
			    type: "post",
			    success: function(data) {
				    randomNum = data; //유저에게 문자로 보낸 랜덤 값을 그대로 리턴 받아서 전역변수로 선언한 randomNum에 넣어줬음
				    console.log("data : " + data);
			    },
			    error: function(error) {
			        console.log(error);
			    } 
			});
			
			// 인증번호 일치 여부 확인
			$("#checkSmsNumber").click(function(){
				var userSms = $("#checkSms").val(); // 회원가입 폼에서 유저가 문자를 보고 입력한 숫자를 가져옴
				// console.log('userSms : ' + userSms);
				// console.log('randomNum1 : ' + randomNum);
				
			    if(userSms == randomNum){ 			//인증번호가 같은 경우 행동
				  	alert("인증성공!");   
			    } else {		   					//인증번호가 다른 경우 행동
				  	alert("인증실패!");   
			    }
			});
				
		});
	});
	
    
</script>
</head>
<body>
	<div id="wrap">
	
	<!-- login area -->
	<div class="loginArea">
		<h1 id="loginTitle" align="center">아이디 찾기</h1>
		<br>
		<br>
		<div id=whiteBox> <!-- whiteBox -->
		<div class="tap tap1" id="user_tap" onclick="user_tap();">개인회원</div>
		<div class="tap tap2" id="business_tap" onclick="business_tap();">기업회원</div>
			<form id="findForm" action="<%=request.getContextPath()%>/findId.me" method="post">
	 			<table>
	 				<tr>
	 					<td colspan="5"><p style="font-size: 20px; font-weight: bold; margin-bottom: 2px;">회원정보로 찾기</p><hr style="border: 2px solid #013252;"><br></td>
	 					<!-- 숨겨진 input -->
						<input type="text" name="loginType" id="loginType" value="0" hidden>
						<!-- /숨겨진 input -->	
	 				</tr>
					<tr>
						<td>이름</td>
						<td colspan="4">
							<input type="text" name="memberName" placeholder="이름을 입력해주세요" style="width:355px;">
						</td>
					</tr>
					<tr>
						<td>휴대폰 번호</td>
						<td>
							<select id="tel1" name="tel1">
								<option selected value="010">010</option>
								<option value="011">011</option>
								<option value="016">016</option>
								<option value="017">017</option>
								<option value="018">018</option>
								<option value="019">019</option>
							</select>
						<td colspan="2" >
							<input type="text" maxlength="8" id="tel2" name="tel2" style="width:176px; height: 45px;">
						</td>
						<td>
							<button type="button" id="sendSms" class="btn" style="background: #666666; color: #AFAFAF; border: 0">인증받기</button>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="4">
							<input type="text" id="checkSms" name="sendNumber" placeholder="인증번호 입력" style="width:160px;">
							<button type="button" id="checkSmsNumber" class="btn" style="background: #666666; color: #AFAFAF; border: 0">확인</button>	
							<button type="button" id="sendSms2" class="btn" style="background:#AFAFAF; color:black; border: 0">재전송</button>
						</td>
					</tr>
					<tr>
						<td colspan="5"><br></td>
					</tr>
					<tr>
						<td colspan="5">
							<button id="checkBtn" value="확인" onclick="check();">확인</button>
						</td>
					</tr>
				</table>	
			</form>
		</div> <!-- whiteBox -->
		<br><br><br>
		<footer>
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div><!-- /login area -->
	</div>
</body>
</html>