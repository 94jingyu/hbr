<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.hbr.member.model.vo.Business"%>
<%
	Business bloginUser = (Business) session.getAttribute("loginUser");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>기업인증확인</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	a {text-decoration: none; color: black;}
	input {width:410px; height:40px; font-size:15px; margin: 2px;}
	textArea {color: #646464;}	
	
	/* 화면 전체 영역 */
	#wrap {
		width: 1200px; 
		margin: 0 auto; 
		height:100px; 
		top:20%; 
		margin-top:50px;
	}
	
	/* 내용 입력 영역 */
	#whiteBox {
		width:700px; 
		height:100%; 
		padding: 40px; 
		border: 1px solid #C4C4C4; 
		margin: 0 auto; 
		background: white;
	}
	.btn {width:120px; height:44px; font-size:15px; margin: 2px; background: #666666; color:#C2C2C2; border: none;}
	.btnDiv{width:120px; text-align: center; display:table-cell;vertical-align:middle;}
	/* 사업자 등록번호 text 박스 */
	.license {
		width: 128px;
	}
	
	/* 휴대폰번호/팩스 앞번호 */
	#tel, #pax {
		width:140px; 
		height:42px; 
		font-size:15px; 
		margin: 2px;
	}
	
	/* 사업자등록번호 조회 버튼 */
	.inquiryBtn {
		width:120px; 
		height:44px; 
		font-size:15px; 
		margin: 2px; 
		background: #666666; 
		color:#C2C2C2; 
		border: none;
	}
	
	/* 승인 버튼 */
	#agreeBtn {
		width:150px; 
		height:50px; 
		font-size:20px; 
		background: #013252; 
		color: white; 
		padding: 5px; 
		margin: 2px; 
		border-radius:10px; 
		border: 1px; 
		outline: 1px;
	} 
	
	.btn, #agreeBtn, #backBtn:hover{cursor:pointer;}
	
	/* 반려 버튼 */
	#closeBtn {
		width:150px; 
		height:50px; 
		font-size:20px; 
		background: rgb(192, 192, 192); 
		color: white; 
		/* padding: 5px; 
		margin: 2px;  */
		border-radius:10px; 
		border: 1px; 
		outline: 1px;
	} 
	
	#closeBtn:hover {
		cursor:pointer;
	}
	
	/* 밑줄(구분선) */
	hr {
		margin-top: 15px;
		margin-bottom: -10px;
	}
	
	/* td 간격 */
	td {
		padding:6px;
	}
	
	/* 기업 정보 form ID */
	#ApproveForm {
		margin-top: -30px;
	}
	
	/* 이전으로 버튼 */
	#backBtn {float: left; width:100px; height:50px; font-size:17px; background-color: transparent !important; 
		background-image: none !important; border-color: transparent; border: none; margin: 2px;}
	
	/* 승인, 반려 버튼 위치 가운데로 조정 */
	#buttons {
		text-align:center;
	}
	
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
<script>
	// 사업자등록번호 조회
    $(function(){
		$("#inquiry").click(function(){
 			var companyName = $("#companyName").val(); 
 			
 			var licensee1 = $(".licensee1").val();
 			var licensee2 = $(".licensee2").val();
 			var licensee3 = $(".licensee3").val();
 			var companyNo = licensee1 + licensee2 + licensee3
 			
 			//console.log(companyName);
 			//console.log(companyNo);
  			
 			if(companyNo != null) {
 				var url = "http://www.ftc.go.kr/bizCommPop.do?wrkr_no="+companyNo;
 				window.open(url, "bizCommPop", "width=750, height=700;");
			} else {
				alert("사용자등록번호를 입력해주세요.");
			} 
 			
 		});
	}); 
	
	// 회사 주소 조회
    function openDaumZipAddress(){
		new daum.Postcode({
			oncomplete:function(data) {

				$("#address").val(data.address);
				$("#address_etc").focus();

				console.log(data);
			}
		}).open();
		
	}
	
</script>
</head>
<body>
	<div id="wrap">
	
	<!-- 기업 정보 -->
	<div class="ApproveArea">
		<h1 id="ApproveTitle" align="center">사업자정보 수정</h1>
		<br>
		<br>
		<div id=whiteBox> <!-- whiteBox -->
			<br>
			<form id="ApproveForm" action="<%=request.getContextPath() %>/updateCompany.me" method="post">
	 			<table>
	 				<tr>
						<td colspan="5"><h3>사업자정보</h3><hr style="border: 1.5px solid #013252; background:#013252;"></td>
						<!-- 숨겨진 input -->
						<input type="text" name="memberId" value="<%= bloginUser.getBmemberId()%>" hidden>
						<input type="text" name="companyYN" value="9" hidden>
						<!-- /숨겨진 input -->
					</tr>
					<tr>
						<td colspan="5"><br></td>
					</tr>
					<tr>
						<td>기업구분</td>
						<td colspan="3">
							<%if(bloginUser.getCompanyType() == "개인") {%>
							<input type="radio" name="companyType" value="개인" id="individual" style="width:15px; height:15px;" checked><label for="individual">&nbsp;개인 사업자</label>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="companyType" value="기업" id="corporation" style="width:15px; height:15px;"><label for="corporation">&nbsp;법인 사업자</label>
							<%} else { %>
							<input type="radio" name="companyType" value="개인" id="individual" style="width:15px; height:15px;" ><label for="individual">&nbsp;개인 사업자</label>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="companyType" value="기업" id="corporation" style="width:15px; height:15px;" checked><label for="corporation">&nbsp;법인 사업자</label>
							<%} %>
						</td>
					</tr>
					<tr>
						<td style="height: 50px;">기업/상호명</td>
						<td colspan="3">
							<input type="text" name="companyName" id="companyName" value="<%= bloginUser.getCompanyName()%>" placeholder=" 기업/상호명 입력">
						</td>
					</tr>
					
					<tr>
						<td>사업자등록번호</td>
						<td colspan="3">
							<input type="text" maxlength="3" name="licensee1" class="license license1" value="<%= bloginUser.getCompanyNo().substring(0, 3)%>">
							<input type="text" maxlength="2" name="licensee2" class="license license2" value="<%= bloginUser.getCompanyNo().substring(4, 6)%>">
							<input type="text" maxlength="5" name="licensee3" class="license license3"value="<%= bloginUser.getCompanyNo().substring(7, 12)%>">
						</td>
						<td>
							<div class="btn btnDiv" id="inquiry">조회</div>
						</td>
					</tr>
					<tr>
						<td>대표자명</td>
						<td colspan="4">
							<input type="text" name="CeoName" value="<%= bloginUser.getCeoName()%>" placeholder=" 대표자명 입력">
						</td>
					</tr>
					<tr>
						<td>회사연락처</td>
						<td id="tel">
							<input type="text" name="tel1" style="width:110px;">
							<label>&nbsp; -</label>
						</td>
						<td colspan="1" >
							<input type="text" name="tel2" style="width:253px;">
						</td>

					</tr>
					<tr>
						<td>회사주소</td>
						<td colspan="3">
							<input type="text" id="address" name="address1">
						</td>
						<td>
							<div class="btn btnDiv" onclick="openDaumZipAddress();">주소검색</div>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="3">
							<input type="text" id="address_etc" name="address2">
						</td>
					</tr>
					<tr>
						<td>팩스번호(선택)</td>
						<td id="pax">
							<input type="text" name="fax1" style="width:110px;">
							<label>&nbsp; -</label>
						</td>
						<td colspan="1" >
							<input type="text" name="fax2" style="width:253px;">
						</td>
					</tr>
					<tr>
						<td>홈페이지(선택)</td>
						<td colspan="4">
							<input type="url" name="homepage" placeholder=" http://">
						</td>
					</tr>
					<tr>
						<td colspan="2" height="10px;"><br><br></td>
					</tr>
					<tr>
					</tr>
					<tr>
						<td colspan="5" id="buttons">
							<button type="reset" id=closeBtn ><b>취소</b></button>&nbsp;&nbsp;&nbsp;
							<button id=agreeBtn><b>수정완료</b></button>
						</td>
					</tr>
				</table>	
			</form>
		
		</div> <!-- whiteBox -->
		<br><br><br>
		<footer>
		<table >
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div><!-- /기업정보-->
	</div>
</body>
</html>