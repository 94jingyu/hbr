<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.hbr.member.model.vo.Member, com.kh.hbr.member.model.vo.Business"%>
<%
	//System.out.println(session.getAttribute("loginType"));
	int loginType = 99;
	Member loginUser = null;
	Business bloginUser = null;

	if(session.getAttribute("loginType") != null){
		loginType = (Integer) session.getAttribute("loginType");  
	}
	
	if(loginType == 0) {
		loginUser = (Member) session.getAttribute("loginUser");
		System.out.println(loginType);
	
	} else if(loginType == 1){
		bloginUser = (Business) session.getAttribute("loginUser");
		System.out.println(loginType);
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	#wrap {width: 1200px; margin: 0 auto; height:100px; top:20%; margin-top:50px;}
	#whiteBox {width:520px; height:100%; padding: 30px; border: 1px solid #C4C4C4; margin: 0 auto; background: white;}
	.checkBox {width: 17px; height: 17px;}
	a {text-decoration: none; color: black;}
	textArea {color: #646464;}	
	
	
	.btn {width:120px; height:44px; font-size:15px; margin: 2px; background: #666666; color:#C2C2C2; border: none;}
	.btn2 {width:180px; height:50px; font-size:20px; color: white; padding: 5px; margin: 2px; border-radius:10px; border: 1px; outline: 1px;}
 	.delBtn {margin: 0 auto; width: 120px;}
	.backBtn {float: left; width:150px; height:50px; font-size:20px; background: #666666; color: white; padding: 5px; margin: 2px; 
		border-radius:10px; border: 1px; outline: 1px;} 
	.delBtn {float: left; width:150px; height:50px; font-size:20px; background: #013252; color: white; padding: 5px; margin: 2px; 
		border-radius:10px; border: 1px; outline: 1px;} 
	.btn, .btn2:hover{cursor:pointer;}
	
	.etcInput{margin-left: 10px; width: 250px; background-color: lightgray; readOnly:true}
	
	#agreeBtn, #backBtn:hover{cursor:pointer;}
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
<script>
     $(function(){
     	$(".delBtn").click(function(){
	    	if (confirm("정말 삭제하시겠습니까??") == true){    //확인
	    	    document.form.submit();
	    	}else{   //취소
	    	    return;
	    	}
    	}); 

    	var sw = true; // 전역변수로 sw를 설정
    
	    $("#etcBox").change(function(){
	    	console.log(sw);
	    	if(sw){
				$('#etcBox').attr("checked", false);
				$('#etcInput').attr("readonly", false).css("background", "white");
				
				sw = false;
			} else {
				$('#etcBox').attr("checked", true);
				$('#etcInput').attr("readonly", true).css("background", "lightgray");
	
				sw = true;
			}
	    });
     }); 
     

</script>
</head>
<body>
	<div id="wrap">
	
	<!-- login area -->
	<div class="loginArea">
		<h1 id="loginTitle" align="center">회원탈퇴</h1>
		<br>
		<br>
		<div id=whiteBox> <!-- whiteBox -->
			<%if(loginType == 0) {%>
			<form id="loginForm" action="<%= request.getContextPath() %>/deleteMember.me" method="post">
	 		<%} else { %>
			<form id="loginForm" action="<%= request.getContextPath() %>/deleteBmember.me" method="post">
	 		<%} %>
	 			<table style="margin: 0 auto; width: 500px;">
		 			<tr>
	 					<td colspan="5"><p style="font-size: 20px; font-weight: bold; margin-bottom: 5px;">회원탈퇴 안내</p></td>
	 				</tr>
					<tr>
						<td colspan="4">
							<p style="margin-bottom: 5px;">* 탈퇴 후 언제든 재가입을 통해 서비스를 이용하실 수 있습니다.</p>
						</td>
					</tr>
					<tr>
						<td colspan="5" height="10px;"><hr style="border: 2px solid #013252;"><br></td>
					</tr>
					<tr>
						<td style="width:80px;">
							<p>아이디</p>
						</td>
						<td>
						<%if(loginType == 0) {%>
							<p> <%= loginUser.getMemberId()%></p>
							<!-- 숨겨진 input -->
							<input type="text" name="memberId" value="<%= loginUser.getMemberId()%>" hidden><!-- 숨겨진 input -->
						<%} else { %>
							<p> <%= bloginUser.getBmemberId()%></p>
							<!-- 숨겨진 input -->
							<input type="text" name="bmemberId" value="<%= bloginUser.getBmemberId()%>" hidden><!-- 숨겨진 input -->
						<%} %>	
						</td>
						<td colspan="4"></td>
					</tr>
					<tr>
				 		<td colspan="5"><br><br></td>
					</tr>
		 			<tr>
	 					<td colspan="5"><p style="font-size: 20px; font-weight: bold; margin-bottom: 5px;">탈퇴 사유</p></td>
	 				</tr>
					<tr>
						<td colspan="4">
							<p style="margin-bottom: 5px;">* 중복 선택이 가능합니다.</p>
						</td>
					</tr>
					<tr>
						<td colspan="5" height="10px;"><hr style="border: 2px solid #013252;"><br></td>
					</tr>
			
					<tr>
						<td colspan="5">
							<input type="checkBox" id="checkBox2" class="checkBox" name="reason" value="서비스 이용불편">
							<label for="checkBox2">서비스 이용불편</label>
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<input type="checkBox" id="checkBox2" class="checkBox" name="reason" value="원하는 정보부족">
							<label for="checkBox2">원하는 정보부족</label>
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<input type="checkBox" id="checkBox2" class="checkBox" name="reason" value="고객응대에 대한 서비스 불만족">
							<label for="checkBox2">고객응대에 대한 서비스 불만족</label>
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<input type="checkBox" id="etcBox" class="checkBox">
							<label for="checkBox2">기타</label><input type="text" id="etcInput" class="etcInput" name="reason" readonly>
						</td>
					</tr>
					<tr>
						<td colspan="5" height="10px;"><br><br></td>
					</tr>
					
					<tr>
						<td colspan="5">
							<button type="button" class="btn2 backBtn" style="margin-left: 85px; margin-right:40px" onclick="back();">취소</button>
							<button type="submit" class="btn2 delBtn"><b>탈퇴하기</b></button>
						</td>
					</tr>
				</table>	
			</form>
		</div> <!-- whiteBox -->
		<table >
		<br><br><br>
		<footer>
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div><!-- /login area -->
	</div>
</body>
</html>