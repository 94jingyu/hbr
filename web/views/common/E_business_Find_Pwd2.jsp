<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	#wrap {width: 1200px; margin: 0 auto; height:100px; top:20%; margin-top:50px;}
	.tap{width: 268px; height: 50px; text-align: center; font-size: 20px; padding-top: 20px; margin-bottom: 40px; border: 1px solid #C4C4C4; bordre-bottom: 0;}
	.tap1{float:left; margin-left:-40px; background: #EFEFEF;}
	.tap2{float:right; margin-right:-40px; border-bottom: 0; font-weight: bold;}
	.tap:hover{cursor:pointer;}
	#whiteBox {width:460px; height:350px; padding-top: 0; padding-left: 40px; padding-right: 40px; padding-bottom: 40px; border: 1px solid #C4C4C4; margin: 0 auto; background: white;}
	input{width: 335px; height:45px; font-size:15px; margin: 1px; margin-left: 10px;} 
	select{width: 80px; height:47px; font-size:15px; margin: 1px;}
	button{width: 90px; height: 50px; margin: 1px; font-size: 15px;}
	
	#checkBtn {width:450px; height:50px; font-size:20px; background: #013252; color: white; padding: 5px; margin: 2px; border-radius:10px; border: 1px; outline: 1px;}
	a {text-decoration: none; color: black;}
	#checkBtn, .JoinBtn:hover{cursor:pointer;}
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
<script>
    function check(){ 
    	alert("비밀번호가 변경되었습니다.");
    };
    
    function user_tap() {
    	location.href = "<%=request.getContextPath()%>/views/common/E_user_Find_Id.jsp";
    };
    function business_tap() {
    	location.href = "<%=request.getContextPath()%>/views/common/E_business_Find_Id.jsp";
    };
    
</script>
</head>
<body>
	<div id="wrap">
	
	<!-- login area -->
	<div class="loginArea">
		<h1 id="loginTitle" align="center">비밀번호 찾기</h1>
		<br>
		<br>
		<div id=whiteBox> <!-- whiteBox -->
		<div class="tap tap1" onclick="user_tap();">개인회원</div>
		<div class="tap tap2" onclick="business_tap();">기업회원</div>
			<form id="loginForm" action="" method="post">
	 			<table>
	 				<tr>
	 					<td colspan="5"><p style="font-size: 20px; font-weight: bold; margin-bottom: 2px;">새 비밀번호 변경</p><hr style="border: 2px solid #013252;"><br></td>
	 				</tr>
					<tr>
						<td>새 비밀번호</td>
						<td colspan="4">
							<input type="password" name="userPwd1" placeholder="영문, 숫자 또는 특수문자 포함 8~20자">
						</td>
					</tr>
	 				</tr>
					<tr>
						<td>비밀번호 확인</td>
						<td colspan="4">
							<input type="password" name="userPwd2" placeholder="비밀번호 재입력">
						</td>
					</tr>
					
					<tr>
						<td colspan="5"><br></td>
					</tr>
					<tr>
						<td colspan="5"><button id="checkBtn" onclick="check();">확인</button></td>
					</tr>
				</table>	
			</form>
		</div> <!-- whiteBox -->
		<br><br><br>
		<footer>
		<table >
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div><!-- /login area -->
	</div>
</body>
</html>