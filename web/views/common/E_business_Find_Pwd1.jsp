<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	#wrap {width: 1200px; margin: 0 auto; height:100px; top:20%; margin-top:50px;}
	.tap{width: 268px; height: 50px; text-align: center; font-size: 20px; padding-top: 20px; margin-bottom: 40px; border: 1px solid #C4C4C4; bordre-bottom: 0;}
	.tap1{float:left; margin-left:-40px; background: #EFEFEF}
	.tap2{float:right; margin-right:-40px; border-bottom: 0; font-weight: bold;}
	.tap:hover{cursor:pointer;}
	#whiteBox {width:460px; height:450px; padding-top: 0; padding-left: 40px; padding-right: 40px; padding-bottom: 40px; border: 1px solid #C4C4C4; margin: 0 auto; background: white;}
	input{height:45px; font-size:15px; margin: 1px;} 
	select{width: 80px; height:47px; font-size:15px; margin: 1px;}
	button{width: 90px; height: 50px; margin: 1px; font-size: 15px;}
	
	#loginBtn {width:445px; height:50px; font-size:20px; background: #013252; color: white; padding: 5px; margin: 2px; border-radius:10px; border: 1px; outline: 1px;}
	a {text-decoration: none; color: black;}
	#loginBtn, .JoinBtn:hover{cursor:pointer;}
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
<script>
    function check(){ 

    	
    location.href = "<%=request.getContextPath()%>/views/common/E_business_Find_Pwd2.jsp";
    };
    
    function user_tap() {
    	location.href = "<%=request.getContextPath()%>/views/common/E_user_Find_Pwd.jsp";
    };
    function business_tap() {
    	location.href = "<%=request.getContextPath()%>/views/common/E_Find_Pwd2.jsp";
    };
    
    
	//submit하기 전 기업인증신청 유효성 검사
	function confirm(){ 
	
	
};
    
</script>
</head>
<body>
	<div id="wrap">
	
	<!-- login area -->
	<div class="loginArea">
		<h1 id="loginTitle" align="center">비밀번호 찾기</h1>
		<br>
		<br>
		<div id=whiteBox> <!-- whiteBox -->
		<div class="tap tap1" onclick="user_tap();">개인회원</div>
		<div class="tap tap2" onclick="business_tap();">기업회원</div>
			<form id="loginForm" action="<%=request.getContextPath()%>/views/common/E_business_Find_Pwd2.jsp" method="post">
	 			<table>
	 				<tr>
	 					<td colspan="5"><p style="font-size: 20px; font-weight: bold; margin-bottom: 2px;">회원정보로 찾기</p><hr style="border: 2px solid #013252;"><br></td>
	 				</tr>
					<tr>
						<td>아이디</td>
						<td colspan="4">
							<input type="text"name="userId" placeholder="아이디를 입력해주세요" style="width:355px;">
						</td>
					</tr>
					<tr>
						<td>이름</td>
						<td colspan="4">
							<input type="text" name="userName" placeholder="이름을 입력해주세요" style="width:355px;">
						</td>
					</tr>
					<tr>
						<td>휴대폰 번호</td>
						<td>
							<select id=tel1>
								<option checked>010</option>
								<option>011</option>
								<option>016</option>
								<option>017</option>
								<option>018</option>
								<option>019</option>
							</select>
						<td colspan="2" >
							<input type="tel" name="phone" style="width:178px; height: 45px;">
						</td>
						<td>
							<button class="btn" onclick="" style="background: #666666; color: #AFAFAF; border: 0">인증받기</button>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="4">
							<input type="text" name="sendNumber" placeholder="인증번호 입력" style="width:160px;">
							<button class="btn" onlick=""  style="background: #666666; color: #AFAFAF; border: 0">확인</button>	
							<button class="btn" onlick="" style="background:#AFAFAF; color:black; border: 0">재전송</button>
						</td>
					</tr>
					<tr>
						<td colspan="5"><br></td>
					</tr>
					<tr>
						<td colspan="5"><button id="loginBtn" onclick="check();">확인</button></td>
					</tr>
				</table>	
			</form>
		</div> <!-- whiteBox -->
		<br><br><br>
		<footer>
		<table >
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div><!-- /login area -->
	</div>
</body>
</html>