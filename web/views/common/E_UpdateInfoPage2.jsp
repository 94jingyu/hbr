<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.hbr.member.model.vo.Member"%>
<%
	Member loginUser = (Member) session.getAttribute("loginUser");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	#wrap {width: 1200px; margin: 0 auto; height:100px; top:20%; margin-top:50px;}
	#whiteBox {width:480px; height:200px; padding: 50px; padding-top:50px; padding-right: 20px; border: 1px solid #C4C4C4; margin: 0 auto; background: white;}
	input {width:350px; height:40px; font-size:15px; margin: 2px; margin-left: 30px;}
	.nextBtn {width:50px; height:50px; font-size:20px; background: #259ADA; color: white; padding: 5px; margin: 2px; padding-left: 8px; border-radius:100px; border: 1px; outline: 1px;}
	a {text-decoration: none; color: black;}
	.nextBtn, .btn:hover{cursor:pointer;}
	.btn {width:180px; height:50px; font-size:20px; color: white; padding: 5px; margin: 2px; border-radius:10px; border: 1px; outline: 1px;}
	.IdBtn{background: #afafaf; margin-left: 30px;}
	.loginBtn{background: #013252; margin-right:0}
	
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
<script>
<%--     function check(){ 
    	location.href = "<%=request.getContextPath()%>/checkPwd.me";
    }; --%>
    
    function back() {
 	   history.back();
    }

</script>
</head>
<body>
	<div id="wrap">
	
	<!-- area -->
	<div class="Area">
		<h1 id="Title" align="center">회원정보 확인</h1>
		<br><br>
		<div id="loginContext" align="center">회원 님의 개인정보 보호를 위해 비밀번호를 다시 한번 확인합니다.</div>
		<br>

		<div id=whiteBox> <!-- whiteBox -->
			<form id="loginForm" action="<%=request.getContextPath()%>/checkPwd.me" method="post">
	 			<table>
					<tr>
						<td colspan="2">아이디</td>
						<td colspan="4">
							<p style="font-size:15px; margin: 2px; margin-left: 30px;"><%= loginUser.getMemberId()%></p>
							<!-- 숨겨진 input -->
							<input type="text" name="memberId" value="<%= loginUser.getMemberId()%>" hidden>
							<input type="text" name="loginType" id="loginType" value="0" hidden>
							<!-- /숨겨진 input -->
						</td>
					</tr>
					<tr>
						<td colspan="6"><br></td>
					</tr>
					<tr>
						<td colspan="2">비밀번호</td>
						<td colspan="4">
							<input type="password" name="memberPwd" placeholder="비밀번호를 입력해주세요">
						</td>
					</tr>
				</table>	
				<br><br>
				<button type="button" class="btn IdBtn" style="margin-right:10px" onclick="back();">뒤로가기</button>
				<button class="btn loginBtn" onclick="check();">확인</button>
			</form>
		</div> <!-- whiteBox -->

		<br><br><br>
		<footer>
		<table >
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div><!-- /login area -->
	</div>
</body>
</html>