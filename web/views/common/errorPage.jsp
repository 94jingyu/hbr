<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String msg = (String) request.getAttribute("msg");
	String failCode = (String) request.getAttribute("failCode");
%>    
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
	
	<% if(msg != null) { %> 
	<h1 align="center"><%= msg %></h1>
	<% } else { %>
	<script>
		$(function() {
			var failCode = "<%= failCode %>";
			var alertMessage = "";
			var movePath = "";
			
			switch(failCode) {
			case "failPassword" : 
				alertMessage = "비밀번호가 틀렸습니다."; 
				movePath = "/h/views/common/E_UpdateInfoPage2.jsp";
				break;
			case "failSelectJenguk" : 
				alertMessage = "조회실패!"; 
				movePath = "/h/views/common/E_UpdateInfoPage2.jsp";
				break;	
			} 
			alert(alertMessage);
			location.href = movePath;
			
		});
	</script>
	<% } %>
</body>
</html>