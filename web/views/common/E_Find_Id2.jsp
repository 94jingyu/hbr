<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.hbr.member.model.vo.Member, com.kh.hbr.member.model.vo.Business"%>
<%
	//System.out.println(session.getAttribute("loginType"));
	int loginType = 99;
	Member loginUser = null;
	Business bloginUser = null;

	if(session.getAttribute("loginType") != null){
		loginType = (Integer) session.getAttribute("loginType");  
	}
	
	if(loginType == 0) {
		loginUser = (Member) session.getAttribute("loginUser");
		/* System.out.println(loginType); */
	
	} else if(loginType == 1){
		bloginUser = (Business) session.getAttribute("loginUser");
		/* System.out.println(loginType); */
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	#wrap {width: 1200px; margin: 0 auto; height:100px; top:20%; margin-top:50px;}
	.tap{width: 268px; height: 50px; text-align: center; font-size: 20px; padding-top: 20px; margin-bottom: 40px; border: 1px solid #C4C4C4; bordre-bottom: 0;}
	<% if(loginType == 0) {%>
		.tap1{float:left; margin-left:-40px; border-bottom: 0; font-weight: bold;}
		.tap2{float:right; margin-right:-40px; background: #EFEFEF}
	<% } else if(loginType == 1) {  %>
		.tap1{float:left; margin-left:-40px; background: #EFEFEF}
		.tap2{float:right; margin-right:-40px; border-bottom: 0; font-weight: bold;}
	<% } %>
	.tap:hover{cursor:pointer;}
	#whiteBox {width:460px; height:100%; padding-top: 0; padding-left: 40px; padding-right: 40px; padding-bottom: 40px; border: 1px solid #C4C4C4; margin: 0 auto; background: white;}
	select{width: 80px; height:47px; font-size:15px; margin: 1px;}
	button{width: 90px; height: 50px; margin: 1px; font-size: 15px;}
	.btn {width:180px; height:50px; font-size:20px; color: white; padding: 5px; margin: 2px; border-radius:10px; border: 1px; outline: 1px;}
	.IdBtn{background: #afafaf;}
	.loginBtn{background: #013252; margin-right:0}
	a {text-decoration: none; color: black;}
	.btn:hover{cursor:pointer;}
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
<script>
    function findPwd() { 
    	location.href = "<%=request.getContextPath()%>/views/common/E_Find_Pwd1.jsp";
    };

    function login() { 
    	location.href = "<%=request.getContextPath()%>/views/common/E_LoginPage.jsp";
    };
    
    function user_tap() {
    	var tap1 = document.getElementById("user_tap");
    	tap1.style.backgroundColor = "white";
     	tap1.style.borderBottom = 0;
    	tap1.style.fontWeight = "bold";

    	var tap2 = document.getElementById("business_tap");
    	tap2.style.backgroundColor = "#EFEFEF";
    	tap2.style.border = "1px solid #C4C4C4";
    	tap2.style.fontWeight= "normal";
    	
    };
    
    function business_tap() {
    	location.href = "<%=request.getContextPath()%>/views/common/E_Find_Id1.jsp";
    };  
    
</script>
</head>
<body>
	<div id="wrap">
	
	<!-- login area -->
	<div class="loginArea">
		<h1 id="loginTitle" align="center">아이디 찾기</h1>
		<br>
		<br>
		<div id=whiteBox> <!-- whiteBox -->
		<div class="tap tap1" id="user_tap" onclick="user_tap();">개인회원</div>
		<div class="tap tap2" id="business_tap" onclick="business_tap();">기업회원</div>
			<form id="loginForm" action="" method="post">
	 			<table style="margin: 0 auto; width: 450px;">
	 				<tr>
	 					<td colspan="5"><p style="width: 370px; font-size: 20px; font-weight: bold; margin-bottom: 2px;">아이디 찾기 결과</p><hr style="border: 2px solid #013252;"></td>
	 				</tr>
					<tr>
						<td colspan="5">
							<div style="height: 50px; background: #C4C4C4; padding: 5px;">
								<p style="font-size: 15px; padding-top:5px; margin-bottom: 5px;">- 입력하신 정보와 일치하는 아이디 목록입니다.</p>
								<!-- <p style="font-size: 15px;">- 개인정보 보호를 위해 아이디 끝자리는 *로 표시했으며, 확인을 <br>&nbsp;&nbsp;원하는 아이디를 선택하신 후 이메일로 받아보실 수 있습니다.</p> -->
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="5" style="border:1px solid #afafaf;" height="50px;">
							<input type="radio" id="userId" name="userId" style="margin: 10px;">
							<label for="userId" style="font-size: 20px;">
							<% if(loginType == 0) {%>
								<%= loginUser.getMemberId()%>
							<% } else if(loginType == 1) {  %>
								<%= bloginUser.getBmemberId()%>
							<% } %>
							</label>
						</td>
					</tr>
					<tr>
						<td colspan="5"><br></td>
					</tr>
					<tr>
						<td align ="center">
							<button type="button" class="btn IdBtn" onclick="findPwd();">비밀번호 찾기</button>
							<button type="button" class="btn loginBtn" onclick="login();">로그인</button>
						</td>
					</tr>
				</table>	
			</form>
		</div> <!-- whiteBox -->
		<br><br><br>
		<footer>
		<table >
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div><!-- /login area -->
	</div>
</body>
</html>