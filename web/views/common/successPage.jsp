<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String successCode = (String) request.getAttribute("successCode"); 
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
	
</script>
</head>
<body>
	<script>
		$(function() {
			var successCode = "<%= successCode%>";
			var alertMessage = "";
			var movePath = "";
			
			switch(successCode) {
			case "insertMember" : 
				alertMessage = "회원가입 성공!"; 
				movePath = "/h/views/guide/user_mainPage.jsp";
				break;
			case "insertBmember" :
				alertMessage = "회원가입 성공!"; 
				movePath = "/h/views/common/E_business_JoinPage3.jsp";
				break;
			case "updateMember" :
				alertMessage = "회원수정 성공!"; 
				movePath = "/h/views/common/E_UpdateInfoPage4.jsp";
				break;
			case "deleteMember" :
				alertMessage = "회원탈퇴 성공!"; 
				movePath = "/h/logout.me";
				break;
 			case "deleteJenguk" :
				alertMessage = "게시글 삭제 성공!"; 
				movePath = "/h/selectJenguk.tn";
				break; 
			} 
			alert(alertMessage);
			location.href = movePath;
			
		});
	</script>
</body>
</html>