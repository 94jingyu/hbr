<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	/* body {background-color: #fffde7;} */
	#wrap {width: 1200px; margin: 0 auto;}
	
	/* ============= 헤더 영역 ============= */
	header {width: 100%; height: 100px; background: #013252;}
	.logo {font-size:45px; color: white;}
	#title {margin-left:10px;}
	
	/* 사이트 바로가기 td id명 */
	#goSite {
		font-size: 18px;
	}
	/* 좌측메뉴 및 내용 */
	.leftMenu{float: left; width: 20%; height: 720px; background-color: #ECF0F7; border-right: 3px solid #CDCDCD; border-right:hidden;}
	section{float: right; width: 100%; height: 720px; border-right: 1px solid #CDCDCD; border-bottom: 1px solid #CDCDCD; /* background-color: #039be5; */}
	
	.leftMenu>ul {    	
		margin: 0;
    	padding: 0;
    	list-style: none;
    }
    .leftMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: black;
  	}
  	td>a {text-decoration: none; color: white;}
	.leftMenu a:hover {background: darkgray; color: white;}
	.btn:hover{cursor: pointer;}
	
	.pageTitle {/* border-top: 3px solid rgb(200, 200, 200); */ border-bottom: 3px solid rgb(200, 200, 200);
		width: 930px; height: 80px; float: right; margin-top: 30px; margin-bottom: 90px;}
	
	.pageTitle2 {padding: 23px;}
	.boardList {padding-top: 50px; padding-left: 50px; margin: 10px;}
	
	.admin{display:inline-block; border:0; width: 400px; height: 290px; border: 1px solid black; margin: 0 auto;}
	.admin2{display:inline-block; border:0; width: 400px; height: 230px; border: 1px solid black; margin: 0 auto;}
	#adminTd{width: 200px; height: 50px; border-color: #013252; background: #013252; color: white; font-size: 20px; font-weight: bold;
		padding: 10px;}
	#straight{width: 200px; text-align:right; border-color: #013252; background: #013252; color: white; font-size: 15px; font-weight: bold;
		padding: 10px;}
	.table1{border-collapse: collapse; border-spacing:0px;}
	.table2{border-collapse: collapse; border-spacing:0px; background: #ECF0F7;}
	.td1{width: 200px; height: 50px; text-align: center; font-size: 20px; font-weight: bold; padding: 10px; }
	.td1-1{width: 200px; height: 20px; text-align: center; font-size: 20px; font-weight: bold; padding: 10px; }
	.td2{border-right: 1px solid black; height: 140px; text-align: center; font-size: 20px; font-weight: bold;}
	.td3{border-right: 1px solid black; padding-bottom: 30px; text-align: center; font-size: 40px; font-weight: bold; }
</style>
</head>
<body>
<script>
	
	//기업인증내역
	function companyApprove() {
		location.href = "<%=request.getContextPath()%>/selectApprove.ap";
	}
	
	// 개인회원 목록
	function userList() {
		location.href = "<%=request.getContextPath()%>/searchMember.me?searchCondition=findAll&searchValue=&memberStatus=";
	}
	// 기업회원 목록
	function businessList() {
		location.href = "<%=request.getContextPath()%>/searchBmember.me?searchCondition=findAll&searchValue=&memberStatus=";
	}
	// 보금자리 등록 심사
	function registerApprove(){
		location.href = "<%=request.getContextPath()%>/approvedList.ad";
	}
	//보금자리 등록 현황
	function listApproved(){
		location.href = "<%=request.getContextPath()%>/listApproved.ad";
	}											
	
	
</script>
	<div id="wrap">
        <header class="logo">
        	<table width="100%;" border="0px;" style="padding: 20px;">
        		<tr>
        			<td width="65%"><label id="title">보금자리</label></td>
        			<td id="goSite" width="320">
        				<a class="btn" href="<%=request.getContextPath()%>/views/guide/user_mainPage.jsp"><img id="arrow" alt="admin_arrow.png" src="<%=request.getContextPath()%>/static/images/admin/admin_arrow.png" 
						width="25px" height="25px">사이트 바로가기</a>
						&nbsp;&nbsp;&nbsp;
						<img alt="admin_account.png" src="<%=request.getContextPath()%>/static/images/admin/admin_account.png" 
						width="25px" height="25px">
						<a><label style="font-size: 18px;">관리자 님</label></a>
        			</td>
        			<td width="40" style="float:right;">
        				<a class="btn" href="<%=request.getContextPath()%>/views/guide/admin_mainPage.jsp"><img alt="admin_home.png" src="<%=request.getContextPath()%>/static/images/admin/admin_home.png" 
						width="45px" height="45px">
        			</td>
        		</tr>
        	</table>
        </header>
        
        <div class="container">
	         <aside class="leftMenu">
	           	<ul>
			   		<li><a href="#" style="font-size:23px; margin-top:20px;"><b>회원관리</b></a></li>
			    	<li><a href="#" onclick="userList();">개인회원</a></li>
			    	<li><a href="#" onclick="businessList();">기업회원</a></li>
			    	<li><a href="#" onclick="companyApprove();">기업인증내역</a></li>
			    	<li><a href="#" style="font-size:23px; margin-top:20px;"><b>상품관리</b></a></li>
			    	<li><a href="<%=request.getContextPath()%>/loadAdminItemUse">상품사용내역</a></li>
			    	<li><a href="<%=request.getContextPath()%>/adminPaymentList.pay">결제내역</a></li>
			    	<li><a href="<%=request.getContextPath()%>/adminRefundList.pay">환불내역</a></li>
			   		<li><a href="#" style="font-size:23px; margin-top:20px;"><b>보금자리관리</b></a></li>
			    	<li><a href="#" onclick="listApproved()">보금자리 등록현황</a></li>
			    	<li><a href="#" onclick="registerApprove()">보금자리 심사</a></li>
			    	<li><a href="#" style="font-size:23px; margin-top:20px;"><b>고객센터관리</b></a></li>
			    	<li><a href="<%=request.getContextPath()%>/loadList.faq">자주하는질문</a></li>
		  		</ul>
	        </aside>
            
         
        </div> 
    </div>
</body>
</html>