<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>

	/* footer 영역 */
	.footer {border: 1px solid #F0F0F0; width: 1200px; height: 210px; text-align: center; background-color: #afafaf;}
	.footerTable{margin: 0 auto; padding: 30px;}
</style>
</head>
<body>
		    <div class="footer">
			<table class="footerTable">
				<tr>
					<td>홈&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;광고문의&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;제휴문의&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;인재채용&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;이용약관&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>개인정보처리방침</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;고객센터</td>
				</tr>
				<tr>
					<td colspan="7" style="height: 20px;"></td>
				</tr>
				<tr>
					<td colspan="7">고객센터: 1588-9350(평일 09:00 ~ 19:00 토요일 09:00 ~ 15:00)&nbsp;&nbsp;&nbsp;FAX : 02-123-456&nbsp;&nbsp;&nbsp;Email : sunshine@kh.co.kr</td>
				</tr>
				<tr>
					<td colspan="7">서울특별시 강남구 강남구 테헤란로14길 6 해볼래&nbsp;&nbsp;&nbsp;대표 : 윤햇살&nbsp;&nbsp;&nbsp;사업자등록번호 : 110-81-34859</td>
				</tr>
				<tr>
					<td colspan="7">통신판매업 신고번호 : 2020-서울역삼-0287호&nbsp;&nbsp;&nbsp;직업정보제공사업 신고번호 : 서울청 제2020-01호</td>
				</tr>
				<tr>
					<td colspan="7" style="height: 20px;"></td>
				</tr>
				<tr>
					<td colspan="7">Copyright ⓒ Sunshine Corp. All Right Reserved.</td>
				</tr>
			</table>
	    
	    </div> 
</body>
</html>