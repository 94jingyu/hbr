<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.hbr.member.model.vo.Member, com.kh.hbr.member.model.vo.Business"%>
<%
	//System.out.println(session.getAttribute("loginType"));
	int loginType = 99;
	Member loginUser = null;
	Business bloginUser = null;

	if(session.getAttribute("loginType") != null){
		loginType = (Integer) session.getAttribute("loginType");  
	}
	
	if(loginType == 0) {
		loginUser = (Member) session.getAttribute("loginUser");
	
	} else if(loginType == 1){
		bloginUser = (Business) session.getAttribute("loginUser");
	}
	
	
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>test</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	#wrap {width: 1200px; margin: 0 auto;}
	
	/* 상단 로그인 영역 */
	.btns {
		align:center;
	}
	
	#loginBtn, #memberJoinBtn, #logoutBtn, #changeInfo, #adminBtn{
		display:inline-block;
		text-align:center;
		background:orangered;
		color:white;
		height:25px;
		width:100px;
		border-radius:5px;
	}
	
	#memberJoinBtn, #logoutBtn {
		background:yellowgreen;
	}
	
	#loginBtn:hover, #changeInfo:hover, #logoutBtn:hover, #memberJoinBtn:hover, #adminBtn:hover {
		cursor:pointer;
	}
	
	/* 로그인 후 */
	.topAfterLogin, .toplogin {
		margin-top: 30px;
	}
	
	/* 헤더 영역 */
	header {width: 100%; height: 150px;}
	
	/* 상단메뉴바 */
	.topMenu {
    	display: inline-block;
    	width: 100%;
    	height: 50px;
    	background: #013252;
    	line-height: 30px;          /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	    vertical-align: middle;     /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	    text-align: center;         /* 글씨 정렬을 가운데로 설정 */  
	    margin-top: 18px;	/* *****로고랑 메뉴바 사이 여백 설정 ***** */
	    font-weight: bold;
  	}
  	
  	.topMenu>div { float: right; }
  	.topMenu>ul {
   		float: left;
    	margin: 0;
    	padding: 0;
    	list-style: none;
  	}
  	
  	.topMenu>ul>li {
    	display: inline-block;
    	padding: 0;
  	}
  	
  	.topMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: white;
  	}
  	
  	.topMenu a:hover { 
  		color:#2FA599;
   		background: #013252; 
   	}
	
	/* 하이터텍스트 효과 */
	a {text-decoration: none; /* color:white; */}      /* 하이퍼텍스트 밑줄 효과 없애기 */
	
	
		/* Dropdown */
	
	.dropdown {
	  display: inline-block;
	  position: relative;
	}
	
	.dd-button {
	  display: inline-block;
	  border: 0;
	  border-radius: 4px;
	  padding-right: 25px;
	  padding-left: 10px;
	  margin-right: 10px;
	  background-color: #ffffff;
	  cursor: pointer;
	  white-space: nowrap;
	  color: #013252;
	}
	
	.dd-button:after {
	  content: '';
	  position: absolute;
	  top: 50%;
	  right: 15px;
	  transform: translateY(-50%);
	  width: 0; 
	  height: 0; 
	  border-left: 5px solid transparent;
	  border-right: 5px solid transparent;
	  border-top: 5px solid black;
	}
	
	.dd-button:hover {
	  background-color: #ECF0F7;
	}
	
	
	.dd-input {
	  display: none;
	}
	
	.dd-menu {
	  position: absolute;
	  top: 100%;
	  border: 1px solid #ccc;
	  border-radius: 4px;
	  padding: 0;
	  margin: 2px 0 0 0;
	  box-shadow: 0 0 6px 0 rgba(0,0,0,0.1);
	  background-color: #ffffff;
	  list-style-type: none;
	}
	
	.dd-input + .dd-menu {
	  display: none;
	} 
	
	.dd-input:checked + .dd-menu {
	  display: block;
	} 
	
	.dd-menu li {
	  padding: 10px 20px;
	  cursor: pointer;
	  white-space: nowrap;
	}
	
	.dd-menu li:hover {
	  background-color: #f6f6f6;
	}
	
	.dd-menu li a {
	  display: block;
	  margin: -10px -20px;
	  padding: 10px 20px;
	}
	
	.dd-menu li.divider{
	  padding: 0;
	  border-bottom: 1px solid #cccccc;
	}
	
	
</style>
<script>
	/* 회원가입, 로그인 등등 */
	function memberJoin() {
		location.href = "<%=request.getContextPath()%>/views/common/E_JoinPage.jsp";
	}

	function login() {
		location.href = "<%=request.getContextPath()%>/views/common/E_LoginPage.jsp";
	}
	
	function logout() {
		var check = window.confirm("로그아웃 하시겠습니까?");
		
		if(check) {
			location.href = "<%=request.getContextPath()%>/logout.me";
		}
	}
	
	function changeInfo() {
		location.href = "<%=request.getContextPath()%>/views/common/E_UpdateInfoPage1.jsp";
	}
	
	/* 네비게이션 메뉴 */
	function home() {
		location.href = "<%=request.getContextPath()%>/views/guide/user_mainPage.jsp";
	}
	
	function bogeumManage() {
		var num = <%=bloginUser.getBmemberNo()%>
		location.href = "<%=request.getContextPath()%>/selectList.bg?num="+num;
	}
	
	function resumeManage() {
		location.href = "<%=request.getContextPath()%>/views/business_DY/DY_business_ResumeManage_list.jsp";
	}
	
	function paymentManage() {
		location.href = "<%=request.getContextPath()%>/views/payment/payment.jsp";
	}
	
	function applicantSearch() {
		location.href = "<%=request.getContextPath()%>/applicantSearch.bs";
	}
	
	function boguemRegister() {
	      var status = "<%=bloginUser.getCertStatus()%>";
	      console.log(status);

	      if(status == "null" || status == "N") {
	         alert("기업인증이 되어야 보금자리 등록을 하실 수 있습니다.");
	      } else {
	         location.href = "<%=request.getContextPath()%>/views/boguem_dj/DJ_boguem_register.jsp";
	      }
	      
	   }
	
	function mypage() {
		
		var bmemberNo = <%=bloginUser.getBmemberNo()%>;
		
		console.log(bmemberNo);
		
		location.href = "<%=request.getContextPath()%>/companyMypage.bs?bmemberNo="+bmemberNo;
	}
	
	//보금자리 로고 클릭시 홈으로 이동
	$(function() {
		$("#bogeumLogo").click(function() {
			location.href = "<%=request.getContextPath()%>/views/guide/user_mainPage.jsp";
		});
	});
	
</script>
</head>
<body>
	<div id="wrap">
		<!-- 상단 로그정보창 -->
		
		<!-- 상단 로그정보 영역 -->
        <div class="toplogin" align="right">
        <% if(loginUser == null && bloginUser == null) {%>
        	<div class="btns">
				<div id="loginBtn" class="btn" onclick="login();">로그인</div>
				<div id="memberJoinBtn" class="btn" onclick="memberJoin();">회원가입</div>
			</div>
        </div>
        <div style="height: 45px"></div>
        <% } else { %>
        <div id="userInfo">
        	<table style="padding:0; padding-bottom:40px; float:right;">
        		<tr>
        			<td style="border:0;">
						<label class="dropdown">
							  <div class="dd-button" style="display:inline-block;" >
								  <%if(loginType == 0) {%>
								  <b><%=loginUser.getMemberName() %></b>님
								  <% } else if(loginType == 1) {  %>
								  <b><%=bloginUser.getManagerName() %></b>님
								  <% } %>
							  </div>
							  <input type="checkbox" class="dd-input" id="test">
							  <ul class="dd-menu">
							      <% if(loginType == 0) {
									      if(!loginUser.getMemberId().equals("admin")){ %>   <!-- 느낌표 주의하기 -->
									      	<li onclick="changeInfo();">회원정보 수정</li>
									      <% } else { %>
									      	<li onclick="goAdmin();">관리자 메뉴가기</li>
										  <% }
									 } else { %>	
								     <li onclick="changeInfo();">회원정보 수정</li>
								  <% } %>						    	
						    	  <li onclick="logout();">로그아웃</li>
							  </ul>
						</label>			
        			</td>
        			
        		</tr>
        	</table>
		</div>
	<% } %>
        
      	<!-- 상단로고 -->
        <header class="logo"><img id="bogeumLogo" alt="logo.png" src="/h/static/images/user/logo.png" width="280px" height="85%" style="margin-left: auto; margin-right: auto; display: block; cursor:pointer;"></header>
        
        <!-- 상단 메뉴바 -->
        <nav class="topMenu">
		  	<ul>
		   		<li><a style="margin-left:35px; cursor:pointer;" onclick="home();">홈</a></li>
		    	<li><a onclick="bogeumManage();" style="cursor:pointer;">보금자리 관리</a></li>
		    	<li><a onclick="resumeManage();" style="cursor:pointer;">인재 관리</a></li>
		    	<li><a onclick="paymentManage();" style="cursor:pointer;">상품·결제 관리</a></li>
		  	  <li><a onclick="applicantSearch();" style="cursor:pointer;">인재 검색</a></li>
		  	</ul>
		  	<div><a style="margin-right:30px; cursor:pointer;" onclick="boguemRegister();">보금자리 등록</a></div>
		  	<div><a onclick="mypage();" style="cursor:pointer;">마이페이지</a></div>
		</nav>
    </div>
</body>
</html>