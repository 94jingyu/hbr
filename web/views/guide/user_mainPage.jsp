<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>

<style>	
	/* 하이퍼텍스트 효과 */
	a {text-decoration: none;}      /* 하이퍼텍스트 밑줄 효과 없애기 */
	
	/* 광고 영역 */
	.ads {width: 100%; height: 450px; clear: both;}
	.ads > .bx-wrapper {border: 0; box-shadow: none;}
	.bx-default-pager {visibility:hidden;  /* display:none; */}
	
	
	/* 공고조회 및 로그인 영역 */
	.b2_cotainer{width: 1198px; height: 275px; background-color: #F0F0F0; text-align: center; border: 1px solid #afafaf;}
	#selectJob {float: left; width: 800px; height: 250px; margin: 12px; margin-right: 0; /* background-color: #039be5; */ display: inline-block;}
	#login {float: right; width: 374px; height: 255px; margin: 12px; margin-left: 0px; background-color: #AFAFAF; display: inline-block;}
	#jiyuk {text-align:left; float:left; padding: 20px; width: 210px; height: 215px; background: white; border-right: 2px solid #AFAFAF;}
	#upjong {text-align:left; float:left; padding: 20px; width: 500px; height: 215px; background: white;}
	
	.job{float:left; width: 60px; height: 60px; /* border: 1px solid black; */}
	.text{float:left; width: 102px; height: 60px; /* border: 1px solid black; */ text-align: center; font-size:18px;}
	.jobimg{width:60px; height:auto;}
	.job:hover{/* background: #f0f0f0; */ cursor:pointer;}
	
	/* 로그인 영역 */
	.login {width: 165px; height: 114px; background-color: white; border: 1px solid #AFAFAF; padding: 10px; padding-top: 0px; padding-bottom: 80px; maring: 0;}
	.loginh {width: 165px; height: 114px; background-color: white; border: 1px solid #AFAFAF; padding: 10px; padding-top: 0px; padding-bottom: 80px; maring: 0;}
	.login > b{font-size: 20px;}
	.loginBtn{width: 375px; height: 45px; background-color: #013252; color: white; clear:both; border-radius:5px; padding-top: 15px;}
	.join{width: 130px; height: 45px; text-align:right;}	
	.search{width: 185px; height: 45px; text-align:center;}	
	.logInfo:hover{background:#ECF0F7;}
	.enroll:hover{text-decoration:underline;}
	.login:hover{background:#ECF0F7;}
	
	/* 기업공고 그리드 영역 */
	.section {text-align:left; overflow: hidden; padding: 20px; width: 1158px; border: 1px solid #AFAFAF; border-top: 0px; border-bottom: 0px;}
	.item1 {width: 265px; margin: 0; height: 240px; padding: 10px; /* background-color: #ff9800; */ border: 1px solid #66B2FF; float: left;}
	.item2 {width: 265px; margin: 0; height: 210px; padding: 10px; /* background-color: #ff9800; */ border: 1px solid #AFAFAF; float: left;}
	.item3 {width: 265px; margin: 0; height: 150px; padding: 10px; /* background-color: #ff9800; */ border: 1px solid #AFAFAF; float: left;}
	.item_gold{width: 265px; height: 110px;}
	.item_silver{width: 265px; height: 90px;}
	.bName {font-size:17px; margin-bottom: 5px;}
	.bTitle{font-size: 22px; margin-bottom: 10px;}
	.bPeriods{font-size: 15px; color: gray; float: left;}
	.bScrap{float: right;}
	.scrap_img{width: 20px; height: 20px;}
	.item1:hover{background: #f4c447; border-color: #f4c447;
	transform: scale(1.2);
	-webkit-transform: scale(1.2);
	-moz-transform: scale(1.2);
	-ms-transform: scale(1.2);
	-o-transform: scale(1.2);
	}
	.item2:hover{background: #f0f0f0;
	}
	.item3:hover{background: #f0f0f0;}
	
	
	/* 아이템 제목(금도끼/은도끼/쇠도끼) */
	#item_title{clear: both; text-align: left; margin-top: 10px; margin-bottom: 5px; font-size: 25px; width: 1150px; height: 50px; /* background-color: #ff9800; */}
	.item_ax{width: 40px; height: 40px; padding-left: 5px; padding-right: 5px; position: relative; top: 5px;}
	
	
	/* footer 영역 */
	.footer {border: 1px solid #F0F0F0; width: 1200px; height: 210px; text-align: center; background-color: #afafaf;}
	table{margin: 0 auto; padding: 30px;}
	
	
	/* 버튼 효과 */
	.btns {
		align:center;
	}
	.bx-prev, .bx-next{font-size: 0}
	.btn {cursor:pointer;}
	
</style>
</head>
<body>
<%@ include file="../common/E_user_menubar.jsp" %>
        
        <!-- 광고 영역 -->
        <div class="ads">
         	<div class="slider">
		    	<div><img alt="" src="../../static/images/admin/sample1.png" width="100%"height="450px" border="none"></div>
		    	<div><img alt="" src="../../static/images/admin/sample2.png" width="100%"height="450px" border="none"></div>
		    	<div><img alt="" src="../../static/images/admin/sample3.png" width="100%"height="450px" border="none"></div>
		  	</div> 	  	
        </div>
        

		<!-- 업직종 및 로그인 영역 -->        
		<div class=b2_cotainer>
        	<div id="selectJob">
        		<div id="jiyuk">
        			<h2>지역별</h2>
        				<br>
        				<p style="font-size:18px;">서울 · 경기 · 인천 · 부산<br>
        				       대구 · 광주 · 대전 · 울산<br>
        				       강원 · 경남 · 경북 · 전남<br>
        				       전북 · 충남 · 충북 · 세종<br>
        				       제주 · 전국</p> 
        		</div>
        		<div id="upjong">
        			<div style="margin-bottom: 10px;" ><h2>업·직종별</h2></div>
					<div class="job job01">
						<img class="jobimg" alt="요리/서빙" src="../../static/images/common/chef.png">
					</div>        		
					<div class="job text job02"><br>요리/서빙</div>        		
					
					<div class="job job03">
						<img class="jobimg" alt="간호/의료" src="../../static/images/common/nurse.png">
					</div>        		
					<div class="job text job04"><br>간호/의료</div>        		
					<div class="job job05">
						<img class="jobimg" alt="생산/기술/건설" src="../../static/images/common/architect2.png">
					</div>        		
					<div class="job text job06"><br>생산/기술</div>        		
					<div class="job job07">
						<img class="jobimg" alt="사무/경리" src="../../static/images/common/accountant.png">
					</div>        		
					<div class="job text job08"><br>사무/경리</div>        		
					<div class="job job09">
						<img class="jobimg" alt="운전/배달" src="../../static/images/common/architect.png">
					</div>        		
					<div class="job text job10"><br>운전/배달</div>        		
					<div class="job job11">
						<img class="jobimg" alt="상담/영업" src="../../static/images/common/secretary.png">
					</div>        		
					<div class="job text job12"><br>상담/영업</div>        		
					<div class="job job13">
						<img class="jobimg" alt="매장관리" src="../../static/images/common/broker.png">
					</div>        		
					<div class="job text job14"><br>매장관리</div>        		
					<div class="job job15">
						<img class="jobimg" alt="교사/강사" src="../../static/images/common/teacher.png">
					</div>        		
					<div class="job text job16"><br>교사/강사</div>        		
					<div class="job job17">
						<img class="jobimg" alt="일반서비스/기타" src="../../static/images/common/astronaut.png">
					</div>        		
					<div class="job text job18"><br>서비스/기타</div>        		
        		</div>
        	
        	</div>
        	
        	<% if(loginUser == null && bloginUser ==null) { %>
	        	<div id="login">
	        	<div class="btn hover login u_login" style="float:left;" onclick="login();"><br><br><br><b>개인회원</b><br>로그인</div>
	        	<div class="btn hover login b_login" style="float:right;" onclick="login();"><br><br><br><b>기업회원</b><br>로그인</div>
	        	<div class="btn loginBtn" style="text-align: left;">
	        	<div class="btn login2 join" style="float:left" onclick="memberJoin();"><b>회원가입</b></div>
	        	<div class="btn login2 search" style="float:right;" onclick="find();">ID/PW 찾기</div>
	        	</div>
	        <% } else if(loginType == 0) {	
		        	  if(loginUser.getMemberId().equals("admin")) {%>
				   		<div id="login">
			        	<div class="loginh u_login" style="float:left; text-align: left; width: 355px; height: 175px;">
							<table style="width: 100%;">
								<tr>
									<td colspan="4" style="font-size: 20px;"><b><%=loginUser.getMemberName() %></b> 님
										<button type="button" class="btn "style="width: 70px; border: 1px solid black; border-radius: 5px; height: 25px;" onclick="logout();">로그아웃</button>
									</td>
								</tr>
								<tr><td style="height: 20px;"></td></tr>
								<tr>
									<td colspan="4">휴 먹고 살기 힘들다</td>
								</tr>
								<tr><td style="height: 10px;"></td></tr>
								<tr>
									<td colspan="4" class="btn enroll" style="font-size: 24px; font-weight: bold; color: #013252">살려줘</td>
								</tr>
								<tr><td style="height: 25px;"></td></tr>
								<tr>
									<td colspan="2" class="btn logInfo" style="width: 200px; text-align: center; border: 1px solid black; border-radius: 5px; height: 50px; font-size: 17px;">출근 관리</td>
									<td colspan="2" class="btn logInfo" style="width: 200px; text-align: center; border: 1px solid black; border-radius: 5px; height: 50px; font-size: 17px;">퇴근 관리</td>
								</tr>
							</table>
			        	</div>
			        	</div>
				  	  
				  	  <% } else { %>
				   		<div id="login">
			        	<div class="loginh u_login" style="float:left; text-align: left; width: 355px; height: 175px;">
							<table style="width: 100%;">
								<tr>
									<td colspan="4" style="font-size: 20px;"><b><%=loginUser.getMemberName() %></b> 님
										<button type="button" class="btn "style="width: 70px; border: 1px solid black; border-radius: 5px; height: 25px;" onclick="logout();">로그아웃</button>
									</td>
								</tr>
								<tr><td style="height: 20px;"></td></tr>
								<tr>
									<td colspan="4">일자리를 빠르고 쉽게 지원하려면</td>
								</tr>
								<tr><td style="height: 10px;"></td></tr>
								<tr>
									<td colspan="4" class="btn enroll" style="font-size: 24px; font-weight: bold; color: #013252" onclick="resumeWrite();">이력서 등록</td>
								</tr>
								<tr><td style="height: 25px;"></td></tr>
								<tr>
									<td colspan="2" class="btn logInfo" style="width: 200px; text-align: center; border: 1px solid black; border-radius: 5px; height: 50px; font-size: 17px;" onclick="resumeManage();">이력서 관리</td>
									<td colspan="2" class="btn logInfo" style="width: 200px; text-align: center; border: 1px solid black; border-radius: 5px; height: 50px; font-size: 17px;" onclick="applyHistory();">입사지원 관리</td>
								</tr>
							</table>
			        	</div>
			        	</div>
				  	  <% } %>
	        
	        	
	        	<% } else { %>
	        	<div id="login">
	        	<div class="loginh u_login" style="float:left; text-align: left; width: 355px; height: 175px;">
					<table style="width: 100%;">
						<tr>
							<td colspan="4" style="font-size: 20px;"><b><%=bloginUser.getManagerName() %></b> 님
								<button type="button" class="btn"  style="width: 70px; border: 1px solid black; border-radius: 5px; height: 25px;" onclick="logout();">로그아웃</button>
							</td>
						</tr>
						<tr><td style="height: 20px;"></td></tr>
						<tr>
							<td colspan="4">인재를 빠르고 쉽게 채용하려면</td>
						</tr>
						<tr><td style="height: 10px;"></td></tr>
						<tr>
							<td colspan="4" class="btn enroll" style="font-size: 24px; font-weight: bold; color: #013252" onclick="businessWrite();">보금자리 등록</td>
						</tr>
						<tr><td style="height: 25px;"></td></tr>
						<tr>
							<td colspan="2" class="btn logInfo" style="width: 200px; text-align: center; border: 1px solid black; border-radius: 5px; height: 50px; font-size: 17px;" onclick="bogeumManage();">보금자리 관리</td>
							<td colspan="2" class="btn logInfo" style="width: 200px; text-align: center; border: 1px solid black; border-radius: 5px; height: 50px; font-size: 17px;" onclick="applicantManage();">지원자 관리</td>
						</tr>
					</table>
	        	</div>
	        	</div>
	        	<% } %>
	        
	        
        	</div>
		</div>
		
		
		<!-- 적용 상품 그리드  -->
        <div class="section">
    
        	<!-- 금도끼 채용공고 -->
            <div id="item_title" style="padding-top:30px;">
            	<img class="item_ax" alt="" src="../../static/images/common/gold_ax.png">
            	<b style="color:#2FA599">금도끼</b> <b>채용공고</b>
            </div>
            	
            	<!-- 첫번째 영역 -->
            	<div id="goldenAxe">
            	
	     		<div class="item1" style="clear:both;">
	            	<img class="item_gold" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" src="/h/static/images/common/star_em.png">
	        		</div>
	        		<input type="hidden" name="recruitNo">
	      		</div>
	      		
	            <div  class="item1">
	            	<img class="item_gold" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
	            <div  class="item1">
	            	<img class="item_gold" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
	            <div  class="item1">
	            	<img class="item_gold" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
	            <div  class="item1">
	            	<img class="item_gold" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
		        <div  class="item1">
	            	<img class="item_gold" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
	            <div  class="item1">
	            	<img class="item_gold" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
		        <div  class="item1">
	            	<img class="item_gold" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
		        <div  class="item1">
	            	<img class="item_gold" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
         	    <div  class="item1">
	            	<img class="item_gold" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
		        <div  class="item1">
	            	<img class="item_gold" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
         	    <div  class="item1">
	            	<img class="item_gold" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
	         </div>   
	            
	        <!-- 금도끼 상품 적용된 채용공고 불러오기 -->
	        <script>
	        	$(function(){
	        		
	        		$(".item1, .item2").mouseover(function(){
	        			var a = $(this).children().eq(4).children();
	        			
	        			if(a.attr("src") === "/h/static/images/common/star.png") {
	        				a.attr("src", "/h/static/images/common/borderStar.png");
	        			}
	        			
	        		}).mouseout(function(){
	        			var a = $(this).children().eq(4).children();
	        			
	        			if(a.attr("src") === "/h/static/images/common/borderStar.png") {
	        				a.attr("src", "/h/static/images/common/star.png");
	        			}
	        		});
	        		
	        		
	        		//onload시 ajax로 적용아이템 목록 가져오기
	        		$.ajax({
	        			url: "<%=request.getContextPath()%>/loadUserMainPageItem",
	        			type: "POST",
	        			success: function(data) {
	        				
	        				//ajax 통신 성공여부 확인
	        				var goldArray = data[0];
	        				var silverArray = data[1];
	        				var steelArray = data[2];
	        				
	        				var goldItem = new Array();
	        				var silverItem = new Array();
	        				var steelItem = new Array();
	        				
	        				//data에 담긴 금도끼 상품 arrayList를 변수에 삽입
	        				for(var i = 0; i < goldArray.length; i++) {
								goldItem.push(goldArray[i]);
	        				}
	        				
							//상품이 하나이상 담겼을때 해당 페이지 구역에 순차적으로 insert	        				
	        				if(goldItem !== null) {
	        					for(var i = 0; i < goldItem.length; i++) {
				        			var advertise = $("#goldenAxe").children().eq(i).children();
				        			var filePath = goldItem[i].filePath;
									var fileName = goldItem[i].changeName;
									var title = goldItem[i].recruitTitle;
									var companyName = goldItem[i].companyName;
									var recruitNo = goldItem[i].target;
									
									//즐겨찾기 img가져오기
									var goldStar = advertise.eq(4).children();
									
									//String type의 date를 가져와서 substr함수를 이용 date의 yy, mm, dd의형태로 분리
									var date = goldItem[i].stringExpirationDate;
									//console.log(date.substr(0, 4));
									//console.log(date.substr(5, 2));
									//console.log(date.substr(8, 2));
									
									//분리된 yy, mm, dd를 기반으로 date 생성
									var expDate = new Date(date.substr(0, 4), date.substr(5, 2) - 1, date.substr(8, 2));
									
									//차이를 계산해줄 현재시간을 생성
									var nowDate = new Date();
									
									//두 날짜를 시간으로 변환하여 시간차이를 계산하고 시간을 일로 변경하여 보여줌
									var diffTime = Math.ceil((expDate.getTime() - nowDate.getTime()) / (1000 * 60 * 60 * 24));
									
									if(fileName === undefined) {
										advertise.eq(0).attr("src", "<%=request.getContextPath()%>/thumbnail_uploadFiles/notready.png");
									}else {
										advertise.eq(0).attr("src", "<%=request.getContextPath()%>/thumbnail_uploadFiles/" + fileName);
									}
									
									advertise.eq(1).text(companyName);
									advertise.eq(2).text(title);
									advertise.eq(3).text("D-" + diffTime );
									advertise.eq(5).val(recruitNo);
									
									//로그인유저의 회원정보를 가져오기
									
									<%
										if(loginUser != null) {
									%>
											memberNo = <%=loginUser.getMemberNo()%>;	
											//로그인한 유저가 있다면
											//즐겨찾기여부에따라 별 표시에 대한 ajax
											$.ajax({
												url: "<%=request.getContextPath()%>/loadFavStarYN",
												type: "POST",
												data: {
													memberNo: memberNo,
													recruitNo: recruitNo,
													kind: "1"
												},
												async: false,
												success: function(data) {
													var yn = data;
													
													if(yn === "Y") {
														goldStar.attr("src", "/h/static/images/common/star.png");
													}else {
														goldStar.attr("src", "/h/static/images/common/star_em.png");
													}
													
												},
												error: function(error) {
													console.log(error);
												}
											});
										
									<%
										}else {
									%>		
										//로그인하지 않았을때는 별모양을 무조건 회색으로 표시
											goldStar.attr("src", "/h/static/images/common/star_em.png");
									<%		
										}
									
									%>
	        					}
	        				}
							
							
							
	        				//data에 담긴 은도끼 상품 arrayList를 변수에 삽입
	        				for(var i = 0; i < silverArray.length; i++) {
								silverItem.push(silverArray[i]);
	        				}
	        				
							//상품이 하나이상 담겼을때 해당 페이지 구역에 순차적으로 insert	        				
	        				if(silverItem !== null) {
								for(var p = 0; p < silverItem.length; p++) {
									
		        					var advertise = $("#silverAxe").children().eq(p).children();
		        					var filePath = silverItem[p].filePath;
									var fileName = silverItem[p].changeName;
									var title = silverItem[p].recruitTitle;
									var companyName = silverItem[p].companyName;
									var recruitNo = silverItem[p].target;
									
									
									//String type의 date를 가져와서 substr함수를 이용 date의 yy, mm, dd의형태로 분리
									var date = silverItem[p].stringExpirationDate;
									//console.log(date.substr(0, 4));
									//console.log(date.substr(5, 2));
									//console.log(date.substr(8, 2));
									
									//분리된 yy, mm, dd를 기반으로 date 생성
									var expDate = new Date(date.substr(0, 4), date.substr(5, 2) - 1, date.substr(8, 2));
									
									//차이를 계산해줄 현재시간을 생성
									var nowDate = new Date();
									
									//두 날짜를 시간으로 변환하여 시간차이를 계산하고 시간을 일로 변경하여 보여줌
									var diffTime = Math.ceil((expDate.getTime() - nowDate.getTime()) / (1000 * 60 * 60 * 24));
									
									if(fileName === undefined) {
										advertise.eq(0).attr("src", "<%=request.getContextPath()%>/thumbnail_uploadFiles/notready.png");
									}else {
										advertise.eq(0).attr("src", "<%=request.getContextPath()%>/thumbnail_uploadFiles/" + fileName);
									}
									
									advertise.eq(1).text(companyName);
									advertise.eq(2).text(title);
									advertise.eq(3).text("D-" + diffTime );
									advertise.eq(5).val(recruitNo);
									
									//즐겨찾기 img가져오기
									var silverStar = advertise.eq(4).children();
									
									//로그인유저의 회원정보를 가져오기
									var memberNo;
									
									<%
										if(loginUser != null) {
									%>
											memberNo = <%=loginUser.getMemberNo()%>;	
											//번호
											//console.log(memberNo);
											//type : number
											//console.log(typeof(memberNo));
											//안담길경우 undefined
											//console.log(memberNo == undefined);
											
											//즐겨찾기여부에따라 별 표시에 대한 ajax
											$.ajax({
												url: "<%=request.getContextPath()%>/loadFavStarYN",
												type: "POST",
												data: {
													memberNo: memberNo,
													recruitNo: recruitNo,
													kind: "1"
												},
												async: false,
												success: function(data) {
													
													var yn = data;
													
													if(yn === "Y") {
														silverStar.attr("src", "/h/static/images/common/star.png");
													} 
													
												},
												error: function(error) {
													console.log(error);
												}
											});
										
									<%
										}else {
									%>		
										//로그인하지 않았을때는 별모양을 무조건 회색으로 표시
											silverStar.attr("src", "/h/static/images/common/star_em.png");
									<%		
										}
									
									%>
								} 
	        				}
	        					
	        				//data에 담긴 쇠도끼 상품 arrayList를 변수에 삽입
	        				for(var i = 0; i < steelArray.length; i++) {
								steelItem.push(steelArray[i]);
	        				}
	        				
							//상품이 하나이상 담겼을때 해당 페이지 구역에 순차적으로 insert	        				
	        				if(steelItem !== null) {
								for(var p = 0; p < steelItem.length; p++) {
			        				var advertise = $("#steelAxe").children().eq(p).children();
									var title = steelItem[p].recruitTitle;
									var companyName = steelItem[p].companyName;
									var recruitNo = steelItem[p].target;
									
									//String type의 date를 가져와서 substr함수를 이용 date의 yy, mm, dd의형태로 분리
									var date = steelItem[p].stringExpirationDate;
									//console.log(date.substr(0, 4));
									//console.log(date.substr(5, 2));
									//console.log(date.substr(8, 2));
									
									//분리된 yy, mm, dd를 기반으로 date 생성
									var expDate = new Date(date.substr(0, 4), date.substr(5, 2) - 1, date.substr(8, 2));
									
									//차이를 계산해줄 현재시간을 생성
									var nowDate = new Date();
									
									//두 날짜를 시간으로 변환하여 시간차이를 계산하고 시간을 일로 변경하여 보여줌
									var diffTime = Math.ceil((expDate.getTime() - nowDate.getTime()) / (1000 * 60 * 60 * 24));
									
									advertise.eq(1).text(companyName);
									advertise.eq(2).text(title);
									advertise.eq(3).text("D-" + diffTime);
									advertise.eq(5).val(recruitNo);
									
									//즐겨찾기 img가져오기
									var steelStar = advertise.eq(4).children();
									//로그인유저의 회원정보를 가져오기
									var memberNo;
									
									<%
										if(loginUser != null) {
									%>
											memberNo = <%=loginUser.getMemberNo()%>;	
											//번호
											//console.log(memberNo);
											//type : number
											//console.log(typeof(memberNo));
											//안담길경우 undefined
											//console.log(memberNo == undefined);
											
											//즐겨찾기여부에따라 별 표시에 대한 ajax
											$.ajax({
												url: "<%=request.getContextPath()%>/loadFavStarYN",
												type: "POST",
												data: {
													memberNo: memberNo,
													recruitNo: recruitNo,
				        							kind:"1"
												},
												async: false,
												success: function(data) {
													
													var yn = data;
													
													if(yn === "Y") {
														steelStar.attr("src", "/h/static/images/common/star.png");
													} 
													
												},
												error: function(error) {
													console.log(error);
												}
											});
										
									<%
										}else {
									%>		
										//로그인하지 않았을때는 별모양을 무조건 회색으로 표시
											steelStar.attr("src", "/h/static/images/common/star_em.png");
									<%		
										}
									
									%>
								}
	        				}	

	        			},
	        			error: function(error){
	        				console.log(error);
	        			}
	        		});
	        		
	        		//즐겨찾기 별표 클릭시 동작할 이벤트
	        		$(".scrap_img").click(function(){
	        			
	        			//로그인이 된 경우에만 동작하도록함.
	        			<% if(loginUser == null) {%>
	        				alert("로그인을 먼저 진행해주세요.");
	        			<% } else { %> 
	        				
	        				//즐겨찾기 설정 ajax
	        				
	        				var check = $(this).parent().parent().children().eq(5).val();
	        				if(check !== "") {
	        					var favArea = $(this);
		        				var favRecruitNo = $(this).parent().parent().children().eq(5).val();
		        				var favMemberNo = <%= loginUser.getMemberNo() %>;
		        				$.ajax({
		        					url: "<%=request.getContextPath()%>/handleFavStar",
		        					data: 
		        						{
		        							favRecruitNo:favRecruitNo,
		        							favMemberNo:favMemberNo,
		        							kind:"1"
		        						},
		        					type: "POST",
		        					success: function(data) {
										if(data === "Y") {
											favArea.attr("src", "/h/static/images/common/star.png");
											alert("즐겨찾기가 설정되었습니다.");
										}else {
											favArea.attr("src", "/h/static/images/common/star_em.png");
											alert("즐겨찾기가 해제되었습니다.");
										}        						
		        					},
		        					error: function(error) {
		        						console.log(error);
		        					}
		        				}); 
	        				}else {
	        					alert("광고준비중입니다.");
	        				}
	        			
	        			<% } %>
	        		}).mouseover(function(){
	        			$(this).css("cursor", "pointer");
	        		}).mouseout(function(){
	        			$(this).css("cursor", "default");	        			
	        		});
	        		
	        		
	        		
	        	});
	        
	        </script>

			<!-- 은도끼 채용공고 -->	
        	<div id="item_title" style="padding-top:100px;">
            	<img class="item_ax" alt="" src="../../static/images/common/silver_ax.png">
        		<b style="color:#2FA599">은도끼</b> <b>채용공고</b>
        	</div>
			<div id="silverAxe">
	     		<div  class="item2" style="clear:both;">
	            	<img class="item_silver" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	        		</div>
	        		<input type="hidden" name="recruitNo">
	      		</div>
	            <div  class="item2">
	            	<img class="item_silver" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
	            <div  class="item2">
	            	<img class="item_silver" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
	            <div  class="item2">
	            	<img class="item_silver" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
	            <div  class="item2">
	            	<img class="item_silver" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
		        <div  class="item2">
	            	<img class="item_silver" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
	            <div  class="item2">
	            	<img class="item_silver" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
		        <div  class="item2">
	            	<img class="item_silver" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
		        <div  class="item2">
	            	<img class="item_silver" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
         	    <div  class="item2">
	            	<img class="item_silver" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
		        <div  class="item2">
	            	<img class="item_silver" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
         	    <div  class="item2">
	            	<img class="item_silver" alt="" src="/h/static/images/common/ready.jpg">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
	        </div>
			
			
			<!-- 쇠도끼 채용공고 -->		
            <div id="item_title" style="padding-top:100px;">
            	<img class="item_ax" alt="" src="../../static/images/common/brons_ax.png">
            	<b style="color:#2FA599">쇠도끼</b> <b>채용공고</b>
           	</div>
           	
			<div id="steelAxe">			
	     		<div  class="item3" style="clear:both;">
	     			<input type="hidden" name="noneVisibility">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	        		</div>
	        		<input type="hidden" name="recruitNo">
	      		</div>
	            <div  class="item3">
	            	<input type="hidden" name="noneVisibility">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
	            <div  class="item3">
	            	<input type="hidden" name="noneVisibility">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
	            <div  class="item3">
	            	<input type="hidden" name="noneVisibility">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
	            <div  class="item3">
	            	<input type="hidden" name="noneVisibility">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
		        <div  class="item3">
		        	<input type="hidden" name="noneVisibility">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
	            <div  class="item3">
	            	<input type="hidden" name="noneVisibility">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
		        <div  class="item3">
		        	<input type="hidden" name="noneVisibility">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
		        <div  class="item3">
		        	<input type="hidden" name="noneVisibility">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
         	    <div  class="item3">
         	    	<input type="hidden" name="noneVisibility">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
		        <div  class="item3">
		        	<input type="hidden" name="noneVisibility">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
	            	<div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
         	    <div  class="item3">
         	    	<input type="hidden" name="noneVisibility">
	            	<div class="bName">광고주</div>
	            	<div class="bTitle">광고주를 모십니다.</div>
	            	<div class="bPeriods">-</div>
         	        <div class="bScrap">
		            	<img class="scrap_img" alt="" src="/h/static/images/common/star_em.png">
	            	</div>
	            	<input type="hidden" name="recruitNo">
	            </div>
	            
	           </div> 
		</div>
		

		<div style="height:100px; border: 1px solid #afafaf; border-top: 0px; border-bottom: 0px;"> </div>
	    <!-- footer영역 -->
	    <div class="footer">
			<table>
				<tr>
					<td>홈&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;광고문의&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;제휴문의&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;인재채용&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;이용약관&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>개인정보처리방침</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;고객센터</td>
				</tr>
				<tr>
					<td colspan="7" style="height: 20px;"></td>
				</tr>
				<tr>
					<td colspan="7">고객센터: 1588-9350(평일 09:00 ~ 19:00 토요일 09:00 ~ 15:00)&nbsp;&nbsp;&nbsp;FAX : 02-123-456&nbsp;&nbsp;&nbsp;Email : sunshine@kh.co.kr</td>
				</tr>
				<tr>
					<td colspan="7">서울특별시 강남구 강남구 테헤란로14길 6 해볼래&nbsp;&nbsp;&nbsp;대표 : 윤햇살&nbsp;&nbsp;&nbsp;사업자등록번호 : 110-81-34859</td>
				</tr>
				<tr>
					<td colspan="7">통신판매업 신고번호 : 2020-서울역삼-0287호&nbsp;&nbsp;&nbsp;직업정보제공사업 신고번호 : 서울청 제2020-01호</td>
				</tr>
				<tr>
					<td colspan="7" style="height: 20px;"></td>
				</tr>
				<tr>
					<td colspan="7">Copyright ⓒ Sunshine Corp. All Right Reserved.</td>
				</tr>
			</table>
	    
	    </div> 
	    
	    
	    
<script>
	$(function(){
	 	$(document).ready(function () {
		    $('.slider').bxSlider({
		        auto: true, // 자동으로 애니메이션 시작
		        speed: 500,  // 애니메이션 속도
		        pause: 3500  // 애니메이션 유지 시간 (1000은 1초)
		        //mode: 'horizontal', // 슬라이드 모드 ('fade', 'horizontal', 'vertical' 이 있음)
		        //autoControls: true, // 시작 및 중지버튼 보여짐
		        //pager: true, // 페이지 표시 보여짐
		        //captions: true, // 이미지 위에 텍스트를 넣을 수 있음
		    });
		}); 
	});
	
	function logout() {
		var check = window.confirm("로그아웃 하시겠습니까?");
		
		if(check) {
			location.href = "<%=request.getContextPath()%>/logout.me";
		}
	}
	
	
	function resumeWrite() {
		location.href = "<%=request.getContextPath()%>/views/user_JinHyeok/resumeWrite.jsp";
	}
	
	function resumeManage() {
  		location.href = "<%=request.getContextPath()%>/res.list";
    }
	
	function applyHistory() {
		location.href = "<%= request.getContextPath()%>/views/user_TAEWON/user_applyHistory.jsp";
	}
	
	function businessWrite() {
		location.href = "<%=request.getContextPath()%>/views/boguem_dj/DJ_boguem_register.jsp";
	}
	
	<%if(bloginUser != null) { %>
 	function bogeumManage() {
		var num = <%=bloginUser.getBmemberNo()%>;
		location.href = "<%=request.getContextPath()%>/selectList.bg?num="+num;
	}
	
	function applicantManage() {
		var bnum = <%=bloginUser.getBmemberNo()%>;
		location.href="<%=request.getContextPath()%>/applicantManage.bs?bmemberNo=" + bnum;
	}
	<% } %>
	
</script>	    
    
</body>
</html>