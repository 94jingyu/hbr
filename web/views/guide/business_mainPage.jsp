<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>test</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	/* body {background-color: #fffde7;} */
	#wrap {width: 1200px; margin: 0 auto;}
	
	/* 상단 로그인 영역 */
	.btns {
		align:center;
	}
	#loginBtn, #memberJoinBtn, #logoutBtn, #changeInfo, #adminBtn{
		display:inline-block;
		text-align:center;
		background:orangered;
		color:white;
		height:25px;
		width:100px;
		border-radius:5px;
	}
	#memberJoinBtn, #logoutBtn {
		background:yellowgreen;
	}
	#loginBtn:hover, #changeInfo:hover, #logoutBtn:hover, #memberJoinBtn:hover, #adminBtn:hover {
		cursor:pointer;
	}
	
	/* 헤더 영역 */
	header {width: 100%; height: 150px; /* background: #b2defe; */}
	
	/* 상단메뉴바 */
	.topMenu {
    	display: inline-block;
    	width: 100%;
    	height: 50px;
    	background: #013252;
    	line-height: 30px;          /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	    vertical-align: middle;     /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	    text-align: center;         /* 글씨 정렬을 가운데로 설정 */  
  	}
  	.topMenu>div { float: right; }
  	.topMenu>ul {
   		float: left;
    	margin: 0;
    	padding: 0;
    	list-style: none;
  	}
  	.topMenu>ul>li {
    	display: inline-block;
    	padding: 0;
  	}
  	.topMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: white;
  	}
  	.topMenu a:hover { background:darkgray; color:orangered; }
	
	
	/* 하이터텍스트 효과 */
	a {text-decoration: none; color:white;}      /* 하이퍼텍스트 밑줄 효과 없애기 */
	
	/* 기업 및 담당자 정보 영역 */
	#information1 {float: left; width: 70%; height: 250px; background-color: #039be5;}
	#information2 {width: 100%; height: 250px; background-color: #0288d1;}
	.leftMenu{float: left; width: 20%; height: 800px; background-color: #ff9800;}
	section{width: 100%; height: 800px; background-color: #ffca28;}
	
	.leftMenu>ul {    	
		margin: 0;
    	padding: 0;
    	list-style: none;
    }
    .leftMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: white;
  	}
	.leftMenu a:hover { background:darkgray; color:orangered; }
	
	/* footer 영역 */
	footer {width: 100%; height: 200px; background-color: #ffb300;}
</style>
</head>
<body>
	<div id="wrap">
		<!-- 상단 로그정보창 -->
	    <div class="toplogin">
        	<div class="btns" align="right">
				<div id="loginBtn" onclick="login();">로그인</div>
				<div id="memberJoinBtn" onclick="memberJoin();">회원가입</div>
			</div>
        </div>
        
      	<!-- 상단로고 -->
        <header class="logo"><img alt="logo.png" src="../../static/images/user/logo.png" width="280px" height="100%" style="margin-left: auto; margin-right: auto; display: block";></header>
        
        <!-- 상단 메뉴바 -->
        <nav class="topMenu">
		  	<ul>
		   		<li><a href="#">홈</a></li>
		    	<li><a href="#">보금자리 관리</a></li>
		    	<li><a href="#">이력서 관리</a></li>
		    	<li><a href="#">상품·결제관리</a></li>
		  	  <li><a href="#">인재검색</a></li>
		  	</ul>
		  	<div><a href="#">보금자리 등록</a></div>
		  	<div><a href="#">마이페이지</a></div>
		</nav>
        
        <!--  기업정보 -->
        <div class="container">
            <div id="information1" class="b1_container">기업정보</div>
            <div id="information2" class="b1_container">담당자정보</div>
		</div>
		
		<!-- 좌측 메뉴바 -->
        <aside class="leftMenu">좌측 메뉴
           	<ul>
		   		<li><a href="#">홈</a></li>
		    	<li><a href="#">보금자리 관리</a></li>
		    	<li><a href="#">이력서 관리</a></li>
		    	<li><a href="#">상품·결제관리</a></li>
	  	  		<li><a href="#">인재검색</a></li>
	  		</ul>
        </aside>
	    
	    <!-- section 영역 -->
	    <div>   
            <section class="b1_container">내용</section>
        </div> 

     
        <!-- footer 영역 --> 
        <footer>Footer</footer>
    </div>
</body>
</html>