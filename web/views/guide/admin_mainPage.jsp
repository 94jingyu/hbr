<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	/* body {background-color: #fffde7;} */
	#wrap {width: 1200px; margin: 0 auto;}
	
	.pageTitle {/* border-top: 3px solid rgb(200, 200, 200); */ border-bottom: 3px solid rgb(200, 200, 200);
		width: 930px; height: 80px; float: right; margin-top: 30px; margin-bottom: 90px;}
	
	.pageTitle2 {padding: 23px;}
	.boardList {padding-top: 50px; padding-left: 50px; margin: 10px;}
	
	.admin{display:inline-block; border:0; width: 400px; height: 292px; border: 1px solid black; margin: 0 auto;}
	.admin2{display:inline-block; border:0; width: 400px; height: 275px; border: 1px solid black; margin: 0 auto;}
	.admin3{display:inline-block; border:0; width: 808px; height: 275px; border: 1px solid black; margin: 0 auto;}
	.empty{display:inline-block; border:0; width: 808px; height: 75px; margin: 0 auto;}
	#adminTd{width: 200px; height: 50px; border-color: #013252; background: #013252; color: white; font-size: 25px; font-weight: bold;
		padding: 10px;}
	#straight{width: 200px; text-align:right; border-color: #013252; background: #013252; color: white; font-size: 15px; font-weight: bold;
		padding: 10px;}
	.table1{border-collapse: collapse; border-spacing:0px;}
	.table2{border-collapse: collapse; border-spacing:0px; background: #ECF0F7;}
	.table3{border-collapse: collapse; border-spacing:0px;}
	.td1{width: 200px; height: 63px; text-align: center; font-size: 20px; font-weight: bold; padding: 10px; padding-bottom: 0}
	.td1-1{width: 200px; height: 30px; text-align: center; font-size: 40px; font-weight: bold; padding: 10px; padding-top: 0}
	.td2{border-right: 1px solid black; height: 140px; text-align: center; font-size: 20px; font-weight: bold;}
	.td3{border-right: 1px solid black; padding-bottom: 30px; text-align: center; font-size: 40px; font-weight: bold;}
	.ta3-td1{width: 405px; height: 50px; border-color: #013252; background: #013252; color: white; font-size: 25px; font-weight: bold;
		padding: 10px;}
	.ta3-td2{width: 405px; text-align:right; border-color: #013252; background: #013252; color: white; font-size: 15px; font-weight: bold;
		padding: 10px;}
	.ta3-td3{font-size: 20px; height: 50px;}
	.ta3-td3-1{text-align:center;}
</style>
</head>
<body>
	<div id="wrap">
		<%@ include file="../guide/admin_menubar.jsp" %>
  
            <div class="section">
                <div class="pageTitle">
	      		  	<div class="pageTitle2">
    	    			<label style="font-size:25px;"><b>업무요약</b></label>
       		 		</div>

	        		<div class="boardList">
		        		<div class="admin">
		        			<table class="table1">
								<tr>
									<td id="adminTd">미승인 현황</td>
									<td id="straight">바로가기
										<img id="arrow" alt="admin_arrow.png" src="../../static/images/admin/admin_arrow.png" 
										width="15px" height="15px">
									</td>
								</tr>		        				
								<tr>
									<td class="td2">보금자리<br>미승인</td>
									<td class="td2">기업인증<br>미승인</td>
								</tr>		        				
								<tr>
									<td class="td3">25</td>
									<td class="td3">25</td>
								</tr>		        				
		        			</table>
		        		</div>


		        		<div class="admin">
		        			<table class="table1">
								<tr>
									<td id="adminTd">QNA</td>
									<td id="straight">바로가기
										<img id="arrow" alt="admin_arrow.png" src="../../static/images/admin/admin_arrow.png" 
										width="15px" height="15px">
									</td>
								</tr>		        				
								<tr>
									<td class="td2">미처리 문의</td>
									<td class="td2">오늘 등록된<br>QNA</td>
								</tr>		        				
								<tr>
									<td class="td3">33</td>
									<td class="td3">25</td>
								</tr>		        				
		        			</table>
		        		</div>
						
					<div class="empty"><!-- 여백을 위한 div --></div>
						
						
		        		<div class="admin2">
		        			<table class="table2" >
								<tr>
									<td rowspan="2" class="td1 icon">
										<img alt="growth.png" src="../../static/images/admin/growth.png" 
										width="70px" height="80px">
									</td>
									<td class="td1">방문자수</td>
								</tr>		
								<tr>
									<td class="td1-1">33</td>
								<tr>
									<td rowspan="2" class="td1">
										<img alt="support.png" src="../../static/images/admin/support.png" 
										width="70px" height="80px">
									</td>
									<td class="td1">가입자수</td>
								</tr>		
								<tr>
									<td class="td1-1">4</td>
		        			</table>
		        		</div>
		        		
		        		<div class="admin2">
		        			<table class="table2" >
								<tr>
									<td rowspan="2" class="td1">
										<img alt="resume.png" src="../../static/images/admin/resume.png" 
										width="70px" height="80px">
									</td>
									<td class="td1">등록된 보금자리</td>
								</tr>		
								<tr>
									<td class="td1-1">33</td>
								<tr>
									<td rowspan="2" class="td1">
										<img alt="certificate.png" src="../../static/images/admin/certificate.png" 
										width="70px" height="80px">
									</td>
									<td class="td1">등록된 이력서</td>
								</tr>		
								<tr>
									<td class="td1-1">4</td>
		        			</table>
		        		</div>
		        		
		        	<div class="empty"><!-- 여백을 위한 div --></div>
				        <div class="admin3">
		        			<table class="table3">
								<tr>
									<td colspan="2" class="ta3-td1">공지사항</td>
									<td colspan="2" class="ta3-td2">바로가기
										<img id="arrow" alt="admin_arrow.png" src="../../static/images/admin/admin_arrow.png" 
										width="15px" height="15px">
									</td>
								</tr>		        				
								<tr>
									<td class="ta3-td3 ta3-td3-1" style="width:100px;">[필독]</td>
									<td colspan="2" class="ta3-td3" style="width: 9000px; padding-left: 10px">설날 연휴 영업시간 공지</td>
									<td class="ta3-td3 ta3-td3-1" style="width: 200px; text-align:right; padding-right: 20px;">20/01/25</td>
								</tr>		        				
								<tr>
									<td class="ta3-td3 ta3-td3-1" style="width:100px;">[필독]</td>
									<td colspan="2" class="ta3-td3" style="width: 900px; padding-left: 10px">설날 연휴 영업시간 공지</td>
									<td class="ta3-td3 ta3-td3-1" style="width: 200px; text-align:right; padding-right: 20px;">20/01/25</td>
								</tr>		        				
								<tr>
									<td class="ta3-td3 ta3-td3-1" style="width:100px;">[안내]</td>
									<td colspan="2" class="ta3-td3" style="width: 900px; padding-left: 10px">이력서 열람 제한 기능에 대해 알려드립니다.</td>
									<td class="ta3-td3 ta3-td3-1" style="width: 200px; text-align:right; padding-right: 20px;">20/01/18</td>
								</tr>		        				
								<tr>
									<td class="ta3-td3 ta3-td3-1" style="width:100px;">[안내]</td>
									<td colspan="2" class="ta3-td3" style="width: 900px; padding-left: 10px">[안내] 광고 상품에 대해 알려드립니다.</td>
									<td class="ta3-td3 ta3-td3-1" style="width: 200px; text-align:right; padding-right: 20px;">20/01/18</td>
								</tr>		        				
		        			</table>
		        		</div>


	        		</div>

        		</div>
        		
        	
        		
        		
        		
            </div>
            
         

        </div> 
    </div>
</body>
</html>