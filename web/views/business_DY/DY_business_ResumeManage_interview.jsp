<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>인재관리</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}

	#wrap {width: 1200px; margin: 0 auto;}
	
	/* 상단 로그인 영역 */
	.topAfterLogin {
		margin-top: 30px;
	}
	
	/* 헤더 영역 */
	header {width: 100%; height: 150px; }
	
	/* 상단메뉴바 */
	.topMenu {
    	display: inline-block;
    	width: 100%;
    	height: 50px;
    	background: #013252;
    	line-height: 30px;          /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	    vertical-align: middle;     /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	    text-align: center;         /* 글씨 정렬을 가운데로 설정 */  
	    margin-top: 18px;	/* *****로고랑 메뉴바 사이 여백 설정 ***** */
	    font-weight: bold;
  	}
  	
  	.topMenu>div { float: right; }
  	
  	.topMenu>ul {
   		float: left;
    	margin: 0;
    	padding: 0;
    	list-style: none;
  	}
  	.topMenu>ul>li {
    	display: inline-block;
    	padding: 0;
  	}
  	
  	.topMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: white;
  	}
  	
  	/* 상단메뉴바 마우스 클릭 시 효과 */
  	.topMenu a:hover { 
		color:#2FA599;
   		background: rgb(210, 210, 210);
  	}
  	
	/* ========== HOME > 이력서관리 > 면접제의 인재정보 ========== */
	.homeNavi {
		margin-top: 45px;
	}
	
	/* ========== 면접제의 인재정보 - 위아래 선 ========== */
	.pageTitle {
		border-top: 3px solid rgb(192, 192, 192);
		border-bottom: 3px solid rgb(192, 192, 192);
		width:930px;
		height: 80px;
		float:right;
		margin-top: 30px;
		margin-bottom: 30px;
	}
	
	/* ========== 면접제의 인재정보 -> 제목 ========== */
	.pageTitle2 {
		padding: 23px;
	}

	/* ================ 면접제의 영역 ================ */
	
	/* 북마크 이미지 */
	#interviewImg {
		margin-left:15px;
	}
	
	/* 면접제의인재 안내  */
	#interviewInfo {
		width:850px;
		height:100px;
		margin-top:170px;
	}
	
	/* 면접제의 인재정보 테이블 div class */
	.interviewTable {
		width:850px;
		height:1000px;
		margin-left:300px;
		margin-top: 60px;
	}
	
	/* 면접제의 테이블 클래스명 */
	 .jariInfo {
    	 width: 840px;
         height: 170px;
      	 text-align: center;
         border-collapse: collapse;
   	}

  	 .jariInfo th {
         background: rgb(210, 210, 210);
         height: 40px;
         border: 1.5px solid rgb(180, 180, 180);
    }   
   
     .jariInfo td {
         border: 1.5px solid rgb(180, 180, 180);
         height: 110px;
    }

    
     /* 테이블 - 이력서 요약 */
    #tableContent {
    	text-align:left;
    	margin-left:25px;
    }

    /* 검색할 키워드 입력 (text) */
    .searchKeyword {
    	float:right;
    	position: relative;
    	width: 180px;
    	height: 35px;
    	mix-blend-mode: normal;
    	border: 3px solid #013252;
    	box-sizing: border-box;
   
    }
    
    /* 검색버튼 */
    .searchButton {
		background: #013252;
		border: 0px;
		color: white;
		height: 35px;
		width: 70px;
		text-size: 15px;
	}
    
    /* 테이블 제목 - 총 3명 */
    #tableTitle {
    	float:left;
    	margin-bottom: 10px;
    }
	
	/* 총 3명 */
	.total {
		width:100px;
		height:30px;
		float: left;
	}
	
	/* select - 전체, 이용중, 이용종료 */
	.selectbox {
		width:110px;
		height:30px;
		float: right;
		margin-top:8px;
	}
	
	/* 검색할 키워드 입력 */
	.searchKey {
		width:180px;
		height:50px;
		float: right;
	}
	
	/* 검색버튼 div 클래스명 */
	.searchB {
		width:70px;
		height:40px;
		float: right;
	}
	
	/* 삭제버튼 */
	.tableButton {
		float: right;
		margin-right: 10px;
		margin-top: 25px;
		background: #013252;
		border: 0px;
		color: white;
		height: 35px;
		width: 70px;
		text-size: 15px;
	}

	/* ================ 좌측 메뉴바 ================ */
	
	.left {
		border:2px solid rgb(192, 192, 192);
	}
	
	/* left 메뉴 aside 클래스 */
	.leftMenu {
		float: left; 
		width: 20%; 
		height: 580px; 
		margin-left: 30px; 
		margin-top: 10px;
	}
	
    .leftMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: black;
  	}
	
	/* left 메뉴  */
	.leftblock {
		display: block;
	}
	
	/* 이력서 관리 */
	#left1 {
		margin-top: 40px;
		width:200px;
		height:335px;
	}

	/* left메뉴 가로선 */
	hr {
		width:85%;
		color:lightgray;
		size:2px;
		margin-left: 5%;
	}
	
	/* 블렛 삭제 */
	ul {
		list-style:none;
	}


</style>
<script>
	/* 이력서 열람내역 */
	function list() {
		location.href = "<%=request.getContextPath()%>/views/business_DY/DY_business_ResumeManage_list.jsp";
	}
	
	/* 면접제의 인재정보 */
	function interview() {
		location.href = "<%=request.getContextPath()%>/views/business_DY/DY_business_ResumeManage_interview.jsp";
	}
	
	/* 스크랩 인재정보 */
	function bookmark() {
		location.href = "<%=request.getContextPath()%>/views/business_DY/DY_business_ResumeManage_bookmark.jsp";
	}
	
</script>
</head>
<body>
	<div id="wrap">
		<%@ include file="../guide/business_menubar.jsp" %>
        
		<!-- 좌측 메뉴바 -->
        <aside class="leftMenu">
        	<div class="left leftblock" id="left1">
        	<br>&nbsp;&nbsp;
        	<label style="font-size:20px;"><b>인재 관리</b></label>
        	<br><br>
        	<hr>
        	<br>
           	<ul>
		   		<li><a href="#" style="font-size:18px;" onclick="list();"><b>이력서 열람내역</b></a>
		   		</li>
		   		<br>
		   		<hr>
		   		<br>
		    	<li><a href="#" style="font-size:18px; color:#2FA599;" onclick="interview();"><b>면접제의 인재정보</b></a>
		    	</li>
		    	<br>
		    	<hr>
		    	<br>
		    	<li><a href="#" style="font-size:18px;" onclick="bookmark();"><b>스크랩 인재정보</b></a>
		    	</li>
	  		</ul>
	  		</div>
        </aside>
        
        <!-- home > 이력서관리 > 면접제의 인재정보 -->
        <div class="homeNavi" >
        	<label style="font-size:15px;">HOME > 인재관리 > 면접제의 인재정보</label>
        </div>
        
        <!-- 면접제의 인재정보내역 -->
        <div class="pageTitle">
        	<div class="pageTitle2">
        		<label style="font-size:25px;"><b>면접제의 인재정보</b></label>
        	</div>
        </div>
        
        <table id="interviewInfo">
				<tr>
					<td colspan="2" width="85px" height="60px">
					<img src="/h/static/images/business/interview.png" width="80px" height="80px" id="interviewImg"></td>
					<td>
						<p style="font-size:18px; padding:5px; margin-left:15px;">
							• 이력서 열람상품을 구매하시면 해당 인재의 상세정보를 확인하실 수 있습니다.</p>
						<p style="font-size:18px; padding:5px; margin-left:15px;"> 
							• 최근 3개월 동안 면접제의한 내역만 확인 가능합니다.
						</p>
					</td>
				</tr>
		</table>
        
            <!-- 테이블 -->
            <div class="interviewTable">
            	<table class="jariInfo">
            		<caption>
            		<div class="searchB">
		   					<button class="searchButton"><b>검색</b></button>
		   				</div>
            			<div class="total">
            				<label id="tableTitle" style="font-size:18px;"><b>총 <label style="color:orange;">3</label>명</b></label>
            			</div>
            			
		   				<div class="searchKey">
		   					<input type="text" class="searchKeyword" name="searchKeyword" placeholder=" 검색할 키워드 입력">
		   				</div>
		   				<div class="selectbox">
		   						<select name="resumeOpen" style="font-size:15px;">
		   							<option value="total">전체</option>
		   							<option value="ing">이용중</option>
		   							<option value="end">이용종료</option>
		   						</select>
		   				</div>
            		</caption>
            		<tbody>
            		<tr>
            			<th width="60" style="border-right:hidden;">선택</th>
            			<th width="50" style="border-right:hidden;"></th>
            			<th width="80" style="border-right:hidden;">이름</th>
            			<th width="260" style="border-right:hidden;">이력서 요약</th>
            			<th width="90" style="border-right:hidden;">열람상태</th>
            			<th width="110" style="border-right:hidden;">면접제의일</th>
            			<th width="180">열람기간</th>
            		</tr>
            		<tr>
            			<td style="border-right:hidden;">
            			<input type="checkbox" name="choice">
            			</td>
            			<td style="border-right:hidden;">
            				<img alt="star.png" src="/h/static/images/user/star.png" width="25px" height="25px">
            			</td>
            			<td style="border-right:hidden;">
            				<a style="font-size:18px;"><b>박다영</b></a><br>
            				<a style="font-size:12px;">여 / 50세</a>
            			</td>
            			<td id="tableContent" style="border-right:hidden; text-align:">
            				<a id="tableContent" style="font-size:15px;"><b>예쁘고 착하고 웃음이 많습니다.</b></a><br>
            				<a id="tableContent" style="font-size:12px;">직종 : 교사/강사/어린이집/유치원</a><br>
            				<a id="tableContent" style="font-size:12px;">지역 : 경기도 수원</a><br>
           
            			</td>
            			<td style="border-right:hidden;">
            				<label style="font-size:15px;">이용중</label>
      
            			</td>
            			<td style="border-right:hidden;">
            				<label>20/01/20</label>
            			</td>
            			<td>
            				<label>20/01/20</label><label> ~ </label><label>20/02/19</label>
            			</td>
            		</tr>
            		<tr>
            			<td style="border-right:hidden;">
            			<input type="checkbox" name="choice">
            			</td>
            			<td style="border-right:hidden;">
            				<img alt="star.png" src="/h/static/images/user/star.png" width="25px" height="25px">
            			</td>
            			<td style="border-right:hidden;">
            				<a style="font-size:18px;"><b>윤관</b></a><br>
            				<a style="font-size:12px;">남 / 50세</a>
            			</td>
            			<td id="tableContent" style="border-right:hidden;">
            				<a id="tableContent" style="font-size:15px;"><b>물류업에 경력이 많습니다.</b></a><br>
            				<a id="tableContent" style="font-size:12px;">직종 : 운전/배달/화물/중장비/특수차</a><br>
            				<a id="tableContent" style="font-size:12px;">지역 : 경기도 분당</a><br>
           
            			</td>
            			<td style="border-right:hidden;">
            				<label style="font-size:15px;">이용중</label>
      
            			</td>
            			<td style="border-right:hidden;">
            				<label>20/01/20</label>
            			</td>
            			<td>
            				<label>20/01/20</label><label> ~ </label><label>20/02/19</label>
            			</td>
            		</tr>
            		<tr>
            			<td style="border-right:hidden;">
            			<input type="checkbox" name="choice">
            			</td>
            			<td style="border-right:hidden;">
            				<img alt="star.png" src="/h/static/images/user/star.png" width="25px" height="25px">
            			</td>
            			<td style="border-right:hidden;">
            				<a style="font-size:18px;"><b>이진규</b></a><br>
            				<a style="font-size:12px;">남 / 50세</a>
            			</td>
            			<td id="tableContent" style="border-right:hidden; text-align:">
            				<a id="tableContent" style="font-size:15px;"><b>바르고 성실합니다.</b></a><br>
            				<a id="tableContent" style="font-size:12px;">직종 : 일반서비스/기타/경비/보안</a><br>
            				<a id="tableContent" style="font-size:12px;">지역 : 경기도 의정부</a><br>
           
            			</td>
            			<td style="border-right:hidden;">
            				<label style="font-size:15px;">이용중</label>
      
            			</td>
            			<td style="border-right:hidden;">
            				<label>20/01/20</label>
            			</td>
            			<td>
            				<label>20/01/20</label><label> ~ </label><label>20/02/19</label>
            			</td>
            		</tr>
            		</tbody>
            	</table>
            	<button class="tableButton tableButton1"><b>삭제</b></button>
            </div>
         
        <!-- footer 영역 --> 
		<!-- <footer>Footer</footer> -->
    </div>
</body>
</html>