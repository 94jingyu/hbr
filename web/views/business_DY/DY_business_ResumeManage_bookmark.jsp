<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,com.kh.hbr.member.model.vo.*,com.kh.hbr.approve.model.vo.*"%>
<% 
	ArrayList<Bookmark> list = (ArrayList<Bookmark>) session.getAttribute("list");

	System.out.println("스크랩인재정보 list : " + list);
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>인재 관리</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}

	#wrap {width: 1200px; margin: 0 auto;}
	
	/* 상단 로그인 영역 */
	.topAfterLogin {
		margin-top: 30px;
	}
	
	/* 헤더 영역 */
	header {width: 100%; height: 150px; }
	
	/* 상단메뉴바 */
	.topMenu {
    	display: inline-block;
    	width: 100%;
    	height: 50px;
    	background: #013252;
    	line-height: 30px;          /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	    vertical-align: middle;     /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	    text-align: center;         /* 글씨 정렬을 가운데로 설정 */  
	    margin-top: 18px;	/* *****로고랑 메뉴바 사이 여백 설정 ***** */
	    font-weight: bold;
  	}
  	
  	.topMenu>div { float: right; }
  	
  	.topMenu>ul {
   		float: left;
    	margin: 0;
    	padding: 0;
    	list-style: none;
  	}
  	.topMenu>ul>li {
    	display: inline-block;
    	padding: 0;
  	}
  	
  	.topMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: white;
  	}
  	
  	/* 상단메뉴바 마우스 클릭 시 효과 */
  	.topMenu a:hover { 
		color:#2FA599;
   		background: rgb(210, 210, 210);
  	}
  	
	/* ========== HOME > 이력서관리 > 스크랩 인재정보 ========== */
	.homeNavi {
		margin-top: 45px;
	}
	
	/* ========== 스크랩 인재정보 - 위아래 선 ========== */
	.pageTitle {
		border-top: 3px solid rgb(192, 192, 192);
		border-bottom: 3px solid rgb(192, 192, 192);
		width:930px;
		height: 80px;
		float:right;
		margin-top: 30px;
		margin-bottom: 30px;
	}
	
	/* ========== 스크랩 인재정보 -> 제목 ========== */
	.pageTitle2 {
		padding: 23px;
	}

	/* ================ 이력서 열람내역 영역 ================ */
	
	/* 북마크 이미지 */
	#bookmarkImg {
		margin-left:15px;
	}
	
	/* 스크랩인재 안내  */
	#bookmarkInfo {
		width:850px;
		height:100px;
		margin-top:170px;
	}
	
	/* 스크랩인재정보 테이블 div class */
	.bookmarkTable {
		width:850px;
		height:1000px;
		margin-left:300px;
		margin-top: 60px;
	}
	
	/* 스크랩인재정보 테이블 클래스명 */
	 .jariInfo {
    	 width: 840px;
         height: 170px;
      	 text-align: center;
         border-collapse: collapse;
   	}

  	 .jariInfo th {
         background: rgb(210, 210, 210);
         height: 40px;
         border: 1.5px solid rgb(180, 180, 180);
    }   
   
     .jariInfo td {
         border: 1.5px solid rgb(180, 180, 180);
         height: 110px;
    }

    
     /* 테이블 - 이력서 요약 */
    #tableContent {
    	text-align:left;
    	margin-left:25px;
    }

    /* 검색할 키워드 입력 (text) */
    .searchKeyword {
    	float:right;
    	position: relative;
    	width: 180px;
    	height: 35px;
    	mix-blend-mode: normal;
    	border: 3px solid #013252;
    	box-sizing: border-box;
   
    }
    
    /* 검색버튼 */
    .searchButton {
		background: #013252;
		border: 0px;
		color: white;
		height: 35px;
		width: 70px;
		text-size: 15px;
	}
    
    /* 테이블 제목 - 총 3명 */
    #tableTitle {
    	float:left;
    	margin-bottom: 10px;
    }
	
	/* 총 3명 */
	.total {
		width:100px;
		height:40px;
		float: left;
	}
	
	/* select - 전체, 이용중, 이용종료 */
	.selectbox {
		width:110px;
		height:30px;
		float: right;
		margin-top:8px;
	}
	
	/* 검색할 키워드 입력 */
	.searchKey {
		width:180px;
		height:50px;
		float: right;
	}
	
	/* 검색버튼 div 클래스명 */
	.searchB {
		width:70px;
		height:40px;
		float: right;
	}
	
	/* 삭제버튼 */
	.tableButton {
		float: right;
		margin-right: 10px;
		margin-top: 25px;
		background: #013252;
		border: 0px;
		color: white;
		height: 35px;
		width: 70px;
		text-size: 15px;
	}

	/* ================ 좌측 메뉴바 ================ */
	
	.left {
		border:2px solid rgb(192, 192, 192);
	}
	
	/* left 메뉴 aside 클래스 */
	.leftMenu {
		float: left; 
		width: 20%; 
		height: 580px; 
		margin-left: 30px; 
		margin-top: 10px;
	}
	
    .leftMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: black;
  	}
	
	/* left 메뉴  */
	.leftblock {
		display: block;
	}
	
	/* 이력서 관리 */
	#left1 {
		margin-top: 40px;
		width:200px;
		height:335px;
	}

	/* left메뉴 가로선 */
	hr {
		width:85%;
		color:lightgray;
		size:2px;
		margin-left: 5%;
	}
	
	/* 블렛 삭제 */
	ul {
		list-style:none;
	}
	
	/* =========== 검색조건 =========== */
	 
	/* 검색조건 테이블 ID */
	#searchBox {
    	margin-right:130px;
    	height:180px;
    	width:700px;
    }
   	
   	/* 검색 버튼 */
   	#searchBtn {
		background: #013252;
		border: 0px;
		color: white;
		height: 40px;
		width: 70px;
		font-size: 15px;
	}
	/* 페이징 관련 */
	#paging {
		margin-top: 35px;
	}
	
	#paging > button {
		border-style: none;
		background: none;
		font-size: 18px;
		/* font-weight: bold; */
		cursor: pointer;
	}
	
	.former {
		margin-right: 10px;
	}
	
	.next {
		margin-left: 10px;
	}

</style>
<script>
	/* 이력서 열람내역 */
	function list() {
		location.href = "<%=request.getContextPath()%>/views/business_DY/DY_business_ResumeManage_list.jsp";
	}
	
	/* 면접제의 인재정보 */
	function interview() {
		location.href = "<%=request.getContextPath()%>/views/business_DY/DY_business_ResumeManage_interview.jsp";
	}
	
	/* 스크랩 인재정보 */
	function bookmark() {
		location.href = "<%=request.getContextPath()%>/views/business_DY/DY_business_ResumeManage_bookmark.jsp";
	}
	
</script>
</head>
<body>
	<div id="wrap">
	<%@ include file="../guide/business_menubar.jsp" %>       
        
		<!-- 좌측 메뉴바 -->
        <aside class="leftMenu">
        	<div class="left leftblock" id="left1">
        	<br>&nbsp;&nbsp;
        	<label style="font-size:20px;"><b>인재 관리</b></label>
        	<br><br>
        	<hr>
        	<br>
           	<ul>
		   		<li><a href="#" style="font-size:18px;" onclick="list();"><b>이력서 열람내역</b></a>
		   		</li>
		   		<br>
		   		<hr>
		   		<br>
		    	<li><a href="#" style="font-size:18px;" onclick="interview();"><b>면접제의 인재정보</b></a>
		    	</li>
		    	<br>
		    	<hr>
		    	<br>
		    	<li><a href="#" style="font-size:18px; color:#2FA599;" onclick="bookmark();"><b>스크랩 인재정보</b></a>
		    	</li>
	  		</ul>
	  		</div>
        </aside>
        
        <!-- home > 이력서관리 > 이력서 열람내역 -->
        <div class="homeNavi" >
        	<label style="font-size:15px;">HOME > 인재관리 > 스크랩 인재정보</label>
        </div>
        
        <!-- 이력서 열람내역 -->
        <div class="pageTitle">
        	<div class="pageTitle2">
        		<label style="font-size:25px;"><b>스크랩 인재정보</b></label>
        	</div>
        </div>
        
        <table id="bookmarkInfo">
				<tr>
					<td colspan="2" width="85px" height="60px">
					<img src="/h/static/images/business/bookmark.png" width="85px" height="85px" id="bookmarkImg"></td>
					<td>
						<p style="font-size:18px; padding:5px; margin-left:15px;">
							• 이력서 열람상품을 구매하시면 해당 인재의 상세정보를 확인하실 수 있습니다.</p>
						<p style="font-size:18px; padding:5px; margin-left:15px;"> 
							• 최근 3개월 동안 스크랩한 내역만 확인 가능합니다.
						</p>
					</td>
				</tr>
		</table>
		<br>
		<br>
		
		<!-- 검색 조건 -->
			 <div align="right">
				<table id="searchBox" style="border:1px solid rgb(180, 180, 180); background:#ECF0F7;">
					<tr>
						<td width="250px" rowspan="4" style="text-align:center">
							<p style="font-size:18px; font-weight: bold;">검색조건</p>
						</td>
					</tr>
					<tr>
						<td width="600px" height="70px">
							<label><b>열람상태</b></label>&nbsp; &nbsp;
							<input type="radio" name="business" value="individual" id="individual" style="width:15px; height:15px;"><label for="individual">&nbsp;전체</label>
							&nbsp; &nbsp;
							<input type="radio" name="business" value="individual" id="individual" style="width:15px; height:15px;"><label for="individual">&nbsp;이용중</label>
							&nbsp; &nbsp;
							<input type="radio" name="business" value="individual" id="individual" style="width:15px; height:15px;"><label for="individual">&nbsp;이용만료</label>			
						</td>
					</tr> 
					<tr>
						<td width="600px" height="40px">
							<label style="margin-right:15px; margin-left:15px;"><b>이름</b></label> &nbsp;&nbsp;
							<input type="text" id="searchText" value="" placeholder="검색할 이름 입력" style="width: 250px; height: 38px; font-size: 15px;">
							<button id="searchBtn">검색</button>
							<br>
							<label>&nbsp;</label>
						</td>
					</tr>
				</table>
				</div>
        
            <!-- 테이블 -->
            <div class="bookmarkTable">
            	<table class="jariInfo">
            		<caption>
            			<div class="total">
            				<label id="tableTitle" style="font-size:18px;"><b>총 <label style="color:orange;"><%=list.size() %></label>명</b></label>
            			</div>
            		</caption>
            		<tbody>
            		<tr>
            			<th width="80" style="border-right:hidden;">선택</th>
            			<th width="80" style="border-right:hidden;">이름</th>
            			<th width="260" style="border-right:hidden;">이력서 정보</th>
            			<th width="90" style="border-right:hidden;">열람상태</th>
            			<th width="110">스크랩일</th>
            		</tr>
            		<% for(Bookmark b : list) { %>
            	
            		<tr>
            			<td style="border-right:hidden;">
            			<input type="checkbox" name="choice">
            			</td>
            			<td style="border-right:hidden;">
            				<a style="font-size:18px;"><b><%=b.getMemberName() %></b></a><br>
            			</td>
            			<td id="tableContent" style="border-right:hidden; text-align:">
            				<a id="tableContent" style="font-size:15px;"><b><%=b.getResumeTitle() %></b></a><br>
            				<a id="tableContent" style="font-size:12px;">관심직종 : <%=b.getApplyField() %></a><br>
            				<a id="tableContent" style="font-size:12px;">관심지역 : <%=b.getInterestArea() %></a><br>
           
            			</td>
            			<td style="border-right:hidden;">
            				<label style="font-size:15px;">이용중</label>
      
            			</td>
            			<td>
            				<label><%=b.getBookmarkDate() %></label>
            			</td>
            		</tr>
            		<%} %>
            		</tbody>
            	</table>
            	<!-- 페이지 처리 -->
           		<div id="paging" align="center">
							<!-- 첫 페이지 버튼설정 -->
							<button><<</button>
							<button><</button>
							
							<button style="color:orange; cursor:default;">&nbsp;&nbsp;1</button>
	
							<button disabled class="next">></button>			
							<button>>></button>
				</div>
            	<button class="tableButton tableButton1"><b>삭제</b></button>
            </div>
         	 <br><br>
        <!-- footer 영역 --> 
		<!-- <footer>Footer</footer> -->
    </div>
</body>
</html>