<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,com.kh.hbr.member.model.vo.*,com.kh.hbr.approve.model.vo.*"%>
<%
	ArrayList<ApplicantManage> acList = (ArrayList<ApplicantManage>) session.getAttribute("applicantList");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>지원자 관리</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	#wrap {width: 1200px; margin: 0 auto;}
	
	/* 상단 로그인 영역 */
	.topAfterLogin {
		margin-top: 30px;
	}
	
	/* 헤더 영역 */
	header {width: 100%; height: 150px; }
	
	/* 상단메뉴바 */
	.topMenu {
    	display: inline-block;
    	width: 100%;
    	height: 50px;
    	background: #013252;
    	line-height: 30px;          /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	    vertical-align: middle;     /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	    text-align: center;         /* 글씨 정렬을 가운데로 설정 */  
	    margin-top: 18px;	/* *****로고랑 메뉴바 사이 여백 설정 ***** */
	    font-weight: bold;
  	}
  	
  	.topMenu>div { float: right; }
  	
  	.topMenu>ul {
   		float: left;
    	margin: 0;
    	padding: 0;
    	list-style: none;
  	}
  	.topMenu>ul>li {
    	display: inline-block;
    	padding: 0;
  	}
  	
  	.topMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: white;
  	}
  	
  	/* 상단메뉴바 마우스 클릭 시 효과 */
  	.topMenu a:hover { 
		color:#2FA599;
   		background: rgb(210, 210, 210);
  	}
  	
	/* ========== HOME > 보금자리 관리 > 지원자 관리 ========== */
	.homeNavi {
		margin-top: 45px;
	}
	
	/* ========== 지원자 관리 - 위아래 선 ========== */
	.pageTitle {
		border-top: 3px solid rgb(192, 192, 192);
		border-bottom: 3px solid rgb(192, 192, 192);
		width:930px;
		height: 80px;
		float:right;
		margin-top: 30px;
		margin-bottom: 30px;
	}
	
	/* ========== 지원자 관리-> 제목 ========== */
	.pageTitle2 {
		padding: 23px;
	}

	/* =========== 이미지, 지원자 관리 안내 =========== */
	
	/* 지원자 관리 안내 table ID */
	#manageInfo {
		width:850px;
		height:100px;
		margin-top:170px;
		margin-bottom:50px;
	}
	
	/* 지원자관리 이미지 */
	#manageImg {
		margin-left:20px;
	}
	
	/* =========== 검색조건 =========== */
	 
	/* 검색조건 테이블 ID */
	#searchBox {
    	margin-right:80px;
    }
    
	 /* 1주일, 1개월, 2개월 */
   	#period {
   		width:60px;
   		height:35px;
   		background: white;
   		border:1px solid rgb(180, 180, 180);
   	}
   	
   	/* 검색 버튼 */
   	#searchBtn {
		background: #013252;
		border: 0px;
		color: white;
		height: 40px;
		width: 70px;
		font-size: 15px;
	}
	
	/* =========== 지원자 관리 테이블 =========== */
	
	/* 테이블 div 클래스명 */
	.applicantTable {
		width:910px;
		height:1000px;
		margin-left:290px;
		margin-top: 15px;
	}
	
	/* 테이블 클래스명 */
	 .applicantInfo {
    	 width: 900px;
         height: 170px;
      	 text-align: center;
         border-collapse: collapse;
   	}

  	 .applicantInfo th {
         background: rgb(210, 210, 210);
         height: 40px;
         border: 1.5px solid rgb(180, 180, 180);
    }   
   
     .applicantInfo td {
         border: 1.5px solid rgb(180, 180, 180);
         height: 110px;
    }

     /* 테이블 - 지원자정보 */
    #tableContent {
    	text-align:left;
    	margin-left:25px;
    }
    
    /* 총 3명 div 클래스명*/
	.total {
		width:100px;
		height:30px;
		float: left;
	}
	
    /* 테이블 제목 - 총 3명 */
    #tableTitle {
    	float:left;
    	margin-bottom: 10px;
    }
    
    /* 보금자리명 */	
    #bogumTitle {
    	text-align:left;
    }
  
    /* 공고상태 */
    #bogumState {
    	border: 1.3px solid navy;
		border-radius:8px;
		padding: 5px;
    }
   
	/* 삭제버튼 */
	.tableButton {
		float: right;
		margin-right: 10px;
		margin-top: 25px;
		background: #013252;
		border: 0px;
		color: white;
		height: 35px;
		width: 70px;
		text-size: 15px;
	}

	/* ================ 좌측 메뉴바 ================ */
	
	.left {
		border:2px solid rgb(192, 192, 192);
	}
	
	/* left 메뉴 aside 클래스 */
	.leftMenu {
		float: left; 
		width: 20%; 
		height: 580px; 
		margin-left: 30px; 
		margin-top: 10px;
	}
	
    .leftMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: black;
  	}
	
	/* left 메뉴  */
	.leftblock {
		display: block;
	}
	
	/* 지원자 관리 */
	#left1 {
		margin-top: 40px;
		width:200px;
		/* left 메뉴 높이 조절 */
		height:255px;
	}

	/* left메뉴 가로선 */
	hr {
		width:85%;
		color:lightgray;
		size:2px;
		margin-left: 5%;
	}
	
	/* 블렛 삭제 */
	ul {
		list-style:none;
	}
	
	/* 페이징 관련 */
	#paging {
		margin-top: 35px;
	}
	
	#paging > button {
		border-style: none;
		background: none;
		font-size: 18px;
		/* font-weight: bold; */
		cursor: pointer;
	}
	
	.former {
		margin-right: 10px;
	}
	
	.next {
		margin-left: 10px;
	}

</style>
</head>
<body>
	<div id="wrap">
	<%@ include file="../guide/business_menubar.jsp" %>
	<script>
	//보금자리 현황으로 이동
	function bogeumManage() {
   		 var num = <%=bloginUser.getBmemberNo()%>
    	 location.href = "<%=request.getContextPath()%>/selectList.bg?num="+num;
	 }
	
	//지원자 관리 클릭 - 지원자관리 서블릿으로 이동
	function applicantManage() {	
		var bnum = <%=bloginUser.getBmemberNo()%>
		location.href="<%=request.getContextPath()%>/applicantManage.bs?bmemberNo=" + bnum;
	}
	
	//이력서 상세보기
	$(function() {
		var a = $("tbody").children().each(function(index) {
			
			//입사지원번호, 이력서번호, 열람여부 가져오기
			if(index != 0) {
				$(this).children().eq(5).click(function() {
					var rnum = $(this).parent().eq(0).children("#resumeNo").text();
					var anum = $(this).parent().eq(0).children("#applyNo").text();
					var openYN = $(this).parent().eq(0).children("#openYN").text();
					
					/* console.log(rnum);
					console.log(anum);
					console.log(openYN); */
					
					location.href = "<%=request.getContextPath()%>/updateOpenYN.bs?resumeNo="+rnum+"&applyNo="+anum;
	
					if(openYN === "N") {
						location.href = "<%=request.getContextPath()%>/updateOpenYN.bs?resumeNo="+rnum+"&applyNo="+anum;
					} else {
						location.href = "<%=request.getContextPath()%>/resumeDetail.bs?resumeNo="+rnum+"&applyNo="+anum;
					}
				});
			}
		});
	}); 
	
	</script>
        
		<!-- 좌측 메뉴바 -->
        <aside class="leftMenu">
        	<div class="left leftblock" id="left1">
        	<br>&nbsp;&nbsp;
        	<label style="font-size:20px;"><b>보금자리 관리</b></label>
        	<br><br>
        	<hr color="lightgray" size="1">
        	<br>
           	<ul>
		   		<li><a href="#" style="font-size:18px;" onclick="bogeumManage();"><b>보금자리 현황</b></a>
		   		</li>
		   		<br>
		   		<hr color="lightgray" size="1">
		   		<br>
		    	<li><a href="#" style="font-size:18px; color:#2FA599;" onclick="applicantManage();"><b>지원자 관리</b></a>
		    	</li>
	  		</ul>
	  		</div>
        </aside>
        
        <!-- home > 보금자리 관리 > 지원자 관리 -->
        <div class="homeNavi" >
        	<label style="font-size:15px;">HOME > 보금자리 관리 > 지원자 관리</label>
        </div>
        
        <!-- 지원자 관리 -->
        <div class="pageTitle">
        	<div class="pageTitle2">
        		<label style="font-size:25px;"><b>지원자 관리</b></label>
        	</div>
        </div>
        
        <!-- 지원자 관리 안내 -->
        <table id="manageInfo">
				<tr>
					<td colspan="2" width="85px" height="60px">
					<img src="/h/static/images/business/process.png" width="80px" height="80px" id="manageImg"></td>
					<td>
						<p style="font-size:18px; padding:5px; margin-left:15px;">
							• 지원자가 입사지원을 취소한 경우 이력서를 확인하실 수 없습니다.</p>
						<p style="font-size:18px; padding:5px; margin-left:15px;"> 
							• 최근 2개월 이내의 지원자 내역을 확인하실 수 있습니다.
						</p>
						<p style="font-size:18px; padding:5px; margin-left:15px;"> 
							• 채용 외의 목적으로 이력서를 열람하는 경우 관련법에 의해 처벌받을 수 있습니다.
						</p>
					</td>
				</tr>
		</table>
			
			<!-- 검색 조건 -->
			 <div align="right">
				<table id="searchBox" style="border:1px solid rgb(180, 180, 180); background:#ECF0F7;">
					<tr>
						<td colspan="2" rowspan="4" width="150px" style="text-align:center">
							<p style="font-size:18px; font-weight: bold;">검색조건</p>
							<p style="font-size:12px;">(최근 2개월)</p>
						</td>
						<td id="periodTD" colspan="4" width="600px" height="80px">
							<label><b>조회기간</b></label>
							&nbsp;
							<button id="period" style="font-size:12px">1주일</button>
							<button id="period" style="font-size:12px">1개월</button>
							<button id="period" style="font-size:12px">2개월</button>
							&nbsp; &nbsp;
							<input type="date" style="width:120px; height:30px;">
							 - 
							<input type="date" style="width:120px; height:30px;">
						</td>
					</tr>
					<tr>
						<td width="600px" height="30px">
							<label><b>열람여부</b></label>&nbsp; &nbsp;
							<input type="radio" name="business" value="individual" id="individual" style="width:15px; height:15px;"><label for="individual">&nbsp;전체</label>
							&nbsp; &nbsp;
							<input type="radio" name="business" value="individual" id="individual" style="width:15px; height:15px;"><label for="individual">&nbsp;열람</label>
							&nbsp; &nbsp;
							<input type="radio" name="business" value="individual" id="individual" style="width:15px; height:15px;"><label for="individual">&nbsp;미열람</label>
			
						</td>
					</tr> 
					<tr>
						<td width="600px" height="70px">
							&nbsp;
							<label><b>상태</b></label>&nbsp; &nbsp; &nbsp; &nbsp;
							<input type="radio" name="business" value="individual" id="individual" style="width:15px; height:15px;"><label for="individual">&nbsp;전체</label>
							&nbsp; &nbsp;
							<input type="radio" name="business" value="individual" id="individual" style="width:15px; height:15px;"><label for="individual">&nbsp;게재중</label>
							&nbsp; &nbsp;
							<input type="radio" name="business" value="individual" id="individual" style="width:15px; height:15px;"><label for="individual">&nbsp;대기중</label>
							&nbsp; &nbsp;
							<input type="radio" name="business" value="individual" id="individual" style="width:15px; height:15px;"><label for="individual">&nbsp;마감</label>
						</td>
					</tr> 
					<tr>
						<td width="600px" height="40px">
							<label><b>검색조건</b></label> &nbsp;
							<input type="text" id="searchText" value="" placeholder="검색할 단어 입력" style="width: 250px; height: 38px; font-size: 15px;">
							<button id="searchBtn">검색</button>
							<br>
							<label>&nbsp;</label>
						</td>
					</tr>
					
				</table><br><br><br>
				</div>
        
            <!-- 테이블 -->
            <div class="applicantTable">
            	<table class="applicantInfo">
            		<caption>
            			<div class="total">
            				<!-- 총 몇명 -->
            				<label id="tableTitle" style="font-size:18px;"><b>총 <label style="color:orange;"><%=acList.size()%></label>명</b></label>
            			</div>
            		</caption>
            		<thead>
            		<tr>
            			<th width="60" style="border-right:hidden;">선택</th>
            			<th width="210" style="border-right:hidden;">보금자리명</th>
            			<th width="210" style="border-right:hidden;">지원자정보</th>
            			<th width="80" style="border-right:hidden;">상태</th>
            			<th width="80" style="border-right:hidden;">지원일</th>
            			<th width="80">열람여부</th>
            		</tr>
            		</thead>           		
            		<% 
            				for(ApplicantManage ac : acList) {
            					String state = ac.getStatus();
            					String openYN = ac.getOpenYN();
            		
            		%>
            		<tbody>
            		<tr>
            			<td id="resumeNo" hidden><%=ac.getResumeNo() %></td>
            			<td id="applyNo" hidden><%=ac.getApplyNo() %></td>
            			<td id="openYN" hidden><%=ac.getOpenYN() %></td>
            			<td style="border-right:hidden;">
            				<input type="checkbox" name="choice">           			
            			</td>
            			<td id="bogumTitle" style="border-right:hidden;">
            				<!-- 보금자리 제목  -->
            				<label style="font-size:15px;"><%=ac.getRecruitTitle()%></label>
            			</td>
            			<td id="tableContent" style="border-right:hidden; text-align:">
            				<!-- 지원자 이름 -->
            				<label id="tableContent" style="font-size:18px; cursor:pointer;"><b><u><%=ac.getMemberName() %></u></b></label>
            				<br>            	
            				<!-- 이력서 제목 -->
            				<label id="tableContent" style="font-size:15px;"><%=ac.getResumeTitle() %></label><br>           			
            			</td>
            			<td style="border-right:hidden;">
            				<!-- 보금자리 상태 -> 심사대기/APPROVED(게재중)/END(마감)/REJECTED(반려)-->
            				<% if(state.equals("심사대기")) { %>
            				<label id="bogumState" style="font-size:15px;">대기</label>
            				<% } else if(state.equals("APPROVED")) {%>
            				<label id="bogumState" style="font-size:15px;">승인</label>
            				<% } else if(state.equals("REJECTED")) {%>
            				<label id="bogumState" style="font-size:15px;">반려</label>
            				<% } else if(state.equals("END")) { %>
            				<label id="bogumState" style="font-size:15px;">마감</label>
            				<% } %>
            			</td>
            			<td style="border-right:hidden;">
            				<!-- 지원일 -->
            				<label style="font-size:15px;"><%=ac.getApplyEnrollDate()%></label>
            			</td>
            			<td>
            				<!-- 열람여부 -->
            				<%if(openYN.equals("Y")) {%>
            				<label style="font-size:15px;">열람</label>
            				<% } else {%>
            				<label style="font-size:15px;">미열람</label>
            				<% } %>
            			</td>
            		</tr>
            		<%}            		        			      
					%>        
            		</tbody>
            	</table>
            	<!-- 페이지 처리 -->
           		<div id="paging" align="center">
							<!-- 첫 페이지 버튼설정 -->
							<button><<</button>
							<button><</button>
							
							<button style="color:orange; cursor:default;">&nbsp;&nbsp;1</button>
	
							<button disabled class="next">></button>			
							<button>>></button>
				</div>
            	<button class="tableButton tableButton1"><b>삭제</b></button>
            </div>
         
        <!-- footer 영역 --> 
		<!-- <footer>Footer</footer> -->
    </div>
</body>
</html>