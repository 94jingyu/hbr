<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import = "com.kh.hbr.resume.model.vo.Resume" %>
<%@ page import = "com.kh.hbr.resume.model.vo.Career" %>
<%@ page import = "com.kh.hbr.resume.model.vo.Education" %>
<%@ page import = "com.kh.hbr.member.model.vo.Member" %>
<%@ page import = "java.util.*" %>
<%@ page import="com.kh.hbr.board.jengukBoard.model.vo.Attachment" %>
<%
	Resume r = (Resume) session.getAttribute("resume");
	Career c = (Career) session.getAttribute("career");
	Education e = (Education) session.getAttribute("edu");
	Member m = (Member) session.getAttribute("member");
	ArrayList<Attachment> attlist = (ArrayList<Attachment>) session.getAttribute("attlist");
	Attachment titleImg = attlist.get(0);
	
	String name = m.getMemberName();
	String name2 = name.substring(0, 1);
	
%>

<!DOCTYPE html>
<html>
<head> 
<meta charset="UTF-8">
<title>Insert title here</title>   
 <style>
body, h1, h2, h3, h4, h5, h6, p, div, header, main, footer, section,
	article, nav, ul, li, form, fieldset, legend, label, p, address, table,
	dl, dt, dd, input, select, textarea, button, figure, figcaption, table,
	th, td, tr {
	margin: 0;
	padding: 0;
}

ul, li {
	list-style: none
}

a {
	text-decoration: none;
	color: #222
}

a:hover {
	color: #2698cb
}

address {
	font-style: normal
}

form, input, button {
	vertical-align: middle
}

button {
	cursor: pointer;
	border: none
}

img, fieldset, select {
	border: none
}

input {
	border: none;
	outline: none
}

.tableBack {
	background-color: #ECF0F7;
	font-weight: bold;
}

.wrap {
	width: 1200px;
	margin: 0 auto;
}
/* 헤더 영역 */
* {
	margin: 0;
	padding: 0;
}
/* body {background-color: #fffde7;} */
#wrap {
	width: 1200px;
	margin: 0 auto;
}

/* 헤더 영역 */
.toplogin {
	width: 100%;
	height: 50px;
	padding: 1%;
}

/*main*/
#main {
	width: 1100px;
	margin: 0 auto;
	margin-top: 200px;
	margin-left: 60px;
}

#main legend {
	padding: 20px 0 20px 0;
	font-size: 22px;
	color: #00548b;
	font-weight: bold
}

.resume_title {
	margin-bottom: 20px
}

.resume_title label {
	font-weight: bold;
	font-size: 15px;
	display: block;
	margin-bottom: 10px
}

.resume_title input {
	border: 1px solid #eee;
	padding: 10px;
}

/*기본정보*/
.basic_info {
	margin-bottom: 20px
}

.basic_info .basic_info_title {
	font-weight: bold;
	font-size: 15px;
	display: block;
	margin-bottom: 10px
}

.basic_info .basic_info_contents {
	background: #ecf0f7;
	padding: 4%;
	position: relative
}

.basic_info .basic_info_contents p {
	padding: 10px
}

.basic_info .basic_info_contents .uphoto {
	padding: 0
}

.basic_info .basic_info_contents label {
	font-size: 15px;
	margin-bottom: 10px;
}

.basic_info .basic_info_contents input {
	padding: 15px;
}

.uname label {
	padding-right: 25px;
	background: url(../images/essential_icon.png) no-repeat bottom right;
	margin-right: 50px
}

.uname input {
	background: #e8e9ee
}

.b_day label {
	padding-right: 25px;
	background: url(../images/essential_icon.png) no-repeat bottom right;
	margin-right: 22px
}

.email label {
	margin-right: 60px
}

.phone_num label {
	padding-right: 25px;
	background: url(../images/essential_icon.png) no-repeat bottom right;
	margin-right: 35px
}

.phone_num input {
	margin-right: 10px;
	background: #e8e9ee
}

.phone_num a {
	text-decoration: underline
}

.uaddress label {
	padding-right: 25px;
	background: url(../images/essential_icon.png) no-repeat bottom right;
	margin-right: 50px
}

.uaddress .uaddress_01 {
	margin-bottom: 10px
}

.uaddress .remain_addy {
	display: block;
	margin-left: 110px
}

.uphoto {
	border: 5px solid #fff;
	position: absolute;
	left: 900px;
	top: 35px;
}

.uphoto label {
	cursor: pointer;
}

.uphoto input {
	position: absolute;
	left: -9999px;
	top: -9999px;
}

/*학력사항*/
.edu_bg_contents {
	background: #ecf0f7;
}

.edu_bg_title_01 {
	font-weight: bold;
	font-size: 15px;
	display: inline-block;
	margin-bottom: 10px
}

.edu_bg_title_02 {
	font-size: 11px
}

.highest_edu {
	border-bottom: 2px solid #acacac;
	padding: 4% 2%;
}

.highest_edu ul {
	overflow: hidden;
	width: 99%;
	margin: 0 auto
}

.highest_edu li {
	width: 18%;
	float: left;
	height: 40px;
	border: 1px solid #ddd;
	background: #fff;
	margin: 0 3%;
	text-align: center;
	line-height: 2.6;
	font-size: 15px;
	font-weight: bold
}

/*고등학교 정보 입력*/
.high_sch_info {
	clear: both;
	padding: 4%;
	border-bottom: 2px solid #acacac
}

.high_sch_info_title {
	font-weight: bold;
	font-size: 15px;
}

.high_sch_info label {
	padding-right: 25px;
	background: url(../images/essential_icon.png) no-repeat bottom right;
	margin-right: 22px;
	font-size: 15px
}

.high_sch_info input {
	padding: 15px;
	color: #8b7575
}

.high_sch_info select {
	padding: 15px;
	height: 50px;
	color: #8b7575
}

.high_sch_info p {
	margin-bottom: 20px
}

.high_sch_info .high_name_title {
	margin-right: 35px
}

.high_major {
	width: 372px;
	color: #8b7575;
}

.high_sch_info .high_select {
	vertical-align: bottom
}

/*대학·대학원 정보 입력*/
.uni_info {
	padding: 4%;
	margin-bottom: 20px
}

.uni_info label {
	padding-right: 25px;
	background: url(../images/essential_icon.png) no-repeat bottom right;
	margin-right: 22px;
	font-size: 15px
}

.uni_info input {
	padding: 15px;
	color: #8b7575
}

.uni_info select {
	padding: 15px;
	color: #8b7575;
	width: 372px;
}

.uni_info p {
	margin-bottom: 20px
}

.uni_info_title {
	font-weight: bold;
	font-size: 15px;
	margin-bottom: 10px
}

.uni_info .sel_uni {
	margin-right: 50px
}

.uni_info .uni_name {
	margin-right: 35px
}

.uni_info .uni_area, .uni_info .uni_major {
	margin-right: 50px
}

.uni_info .uni_major_select {
	height: 45px;
	vertical-align: bottom;
	padding: 14px;
}

.uni_info .uni_major_input {
	display: block;
	margin: 10px 0 0 110px
}

.uni_info .uni_select {
	vertical-align: bottom;
	width: auto;
	height: 50px;
}

.uni_info .uni_credit_label {
	background: none;
	padding: 0;
	margin-right: 75px
}

.uni_info .credit_limit_select {
	width: 168px;
	height: 45px;
	vertical-align: bottom
}

/*경력사항*/
.Career_details {
	margin-bottom: 20px
}

.Career_details_title {
	font-weight: bold;
	font-size: 15px;
	margin-bottom: 10px
}

.Career_details_contents {
	background: #ecf0f7;
}

.Career_details_inpo {
	padding: 4%
}

.Career_details label {
	padding-right: 25px;
	background: url(../images/essential_icon.png) no-repeat bottom right;
	margin-right: 22px;
	font-size: 15px
}

.Career_details input {
	padding: 15px;
	color: #8b7575
}

.Career_details select {
	padding: 15px;
	color: #8b7575;
	width: 372px;
}

.Career_details p {
	margin-bottom: 20px;
}

.Career_position, .Career_type {
	position: relative
}

.Career_details_inpo a {
	text-decoration: underline;
	position: absolute;
	left: 430px;
	top: 11px;
	font-weight: bold
}

.sel_career {
	border-bottom: 2px solid #acacac;
	padding: 4% 2%;
}

.sel_career ul {
	width: 60%;
	margin: 0 auto;
	overflow: hidden;
}

.sel_career li {
	width: 40%;
	margin: 3%;
	background: #fff;
	float: left;
	text-align: center;
	height: 40px;
	border: 1px solid #ddd;
	line-height: 2.6;
	font-size: 15px;
	font-weight: bold
}

.Career_details .cor_name {
	margin-right: 35px
}

.Career_details .term_office_input {
	width: 145px;
}

.Career_details .job_type {
	margin-right: 50px
}

.Career_details .sel_position {
	margin-right: 15px
}

/*자기소개서*/
.self_intro {
	margin-bottom: 20px
}

.self_intro_title {
	margin-bottom: 10px
}

.self_intro_title_01 {
	font-weight: bold;
	font-size: 15px;
	margin-bottom: 10px
}

.self_intro_title_02 {
	font-size: 11px
}

.self_intro_table {
	border: 1px solid #eee;
	margin-bottom: 10px
}

.self_intro_table caption, .self_intro_table .re_title,
	.self_intro_table .re_contents {
	position: absolute;
	left: -9999px;
	top: -9999px
}

.re_title_area {
	border-bottom: 1px solid #000000;
	padding: 15px;
	font-size: 15px
}

.re_contents_area {
	border: none;
	padding: 15px;
}

.terms_desired1 {
	border-bottom: 1px solid #000000;
	padding: 15px;
	font-size: 15px
}

.terms_desired2 {
	border: none;
	padding: 15px;
}

.letter_count {
	font-weight: bold;
	font-size: 16px;
}

.letter_count .letter_red {
	color: #ff0000
}

/*희망조건 선택*/
.de_condition {
	margin-bottom: 20px
}

.de_condition_title {
	font-weight: bold;
	font-size: 15px;
	margin-bottom: 10px
}

.de_condition_contents {
	background: #ecf0f7;
	padding: 4%
}

.de_condition_contents label {
	padding-right: 25px;
	background: url(../images/essential_icon.png) no-repeat bottom right;
	margin-right: 45px;
	font-size: 15px
}

.de_condition_contents .int_job {
	padding-right: 80px;
	background: url(../images/essential_3_icon.png) no-repeat bottom right;
	margin-right: 22px;
	font-size: 15px
}

.de_condition_contents select {
	padding: 15px;
	color: #8b7575;
	margin-right: 30px;
	width: 200px;
}

.de_condition_contents p {
	margin-bottom: 20px;
}

/*이력서 공개여부*/
.resume_public {
	margin-bottom: 20px
}

.resume_public_title {
	font-weight: bold;
	font-size: 15px;
	margin-bottom: 10px
}

.resume_public_contents {
	background: #ecf0f7;
	padding: 4%
}

.resume_public_contents label {
	padding-right: 25px;
	background: url(../images/essential_icon.png) no-repeat bottom right;
	margin-right: 22px;
	font-size: 15px
}

.resume_public_radio {
	margin-left: 15px
}

.resume_public_contents p {
	font-size: 15px;
	font-weight: bold;
	margin: 5px 0 0 160px
}

.resume_check {
	padding: 50px
}

.resume_check_ch {
	width: 410px;
	margin: 0 auto;
	margin-bottom: 20px
}

.resume_check_ch span {
	font-weight: bold
}

.resume_date {
	width: 280px;
	margin: 0 auto
}

.resume_date_date, .resume_date_user {
	font-weight: bold
}

/*버튼*/
.sub_btn {
	
}

#backButton {
	background: #013252;
	color: white;
	border-style: none;
	width: 180px;
	height: 60px;
	font-weight: bold;
	margin-top: 20px;
	margin-left: 450px;
	font-size: 18px;
}

	/* 면접제의, 스크랩 버튼 div */
	#ibBtn {
		width:115px;
		height:95px;
		margin-left:60px;
		margin-right:80px;
	}
	
	/* 면접제의, 스크랩 버튼 */
	#interviewBtn, #bookmarkBtn {
		text-align:center;
		width:120px; 
		height:40px; 
		font-size:16px; 
		background: #013252; 
		color: white; 
		padding: 5px; 
		margin: 2px; 
		border: 1px; 
		outline: 1px;
	}
	
	/* 스크랩버튼 */
	#bookmarkBtn {
		margin-top:15px;
	}
	
	/* Modal background */
	.searchModal {
		display: none; /* Hidden by default */
		position: fixed; /* Stay in place */
		z-index: 10; /* Sit on top */
		left: 0;
		top: 0;
		width: 100%; /* Full width */
		height: 100%; /* Full height */
		overflow: auto; /* Enable scroll if needed */
		background-color: rgb(0,0,0); /* Fallback color */
		background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	
	/* Modal content */
	.search-modal-content {
		background-color: #fefefe;
		margin: 15% auto; /* 15% from the top and centered */
		padding: 20px;
		border: 1px solid #888;
		width: 550px; /* Could be more or less, depending on screen size */
		height: 380px;
	}
	
	/* 확인 버튼 */
	#confirmBtn {
		width:150px; 
		height:50px; 
		font-size:20px; 
		background: #013252; 
		color: white; 
		padding: 5px; 
		margin: 2px; 
		border-radius:10px; 
		border: 1px; 
		outline: 1px;
	} 
	
	#confirmBtn:hover {
		cursor:pointer;
	}
	
	/* 취소 버튼 */
	#closeBtn {
		width:150px; 
		height:50px; 
		font-size:20px; 
		background: rgb(192, 192, 192); 
		color: white; 
		/* padding: 5px; 
		margin: 2px;  */
		border-radius:10px; 
		border: 1px; 
		outline: 1px;
	} 
	
	#closeBtn:hover {
		cursor:pointer;
	}
	
</style>
    
</head>
<body>
<%@ include file="../guide/business_menubar.jsp" %>
<script>
	//상세보기 뒤로가기
	$(function() {
		$("#backButton").click(function() {
			location.href = "<%=request.getContextPath()%>/applicantSearch.bs";
		});
	});
	
	//연락처 확인 버튼
	$(function() {
		$("#interviewBtn").click(function() {
			$("#modal").show();
		});
	});
	
	//모달 취소 버튼
	$(function() {
		$("#closeBtn").click(function() {
			$('.searchModal').hide();
		});
	});
	
	//모달 확인 버튼
	$(function() {
		$("#confirmBtn").click(function() {
			alert("열람권이 사용되었습니다.");
			
			var rNum = $("#rNum").children().children().children().eq(0).text();
			
			location.href="<%=request.getContextPath()%>/useProductDetail.bs?rNum=" + rNum;
			$('.searchModal').hide();
		});
	});
	
	$(function() {
 		$("#bookmarkBtn").click(function() {
 			alert("연락처 확인 후 스크랩이 가능합니다.");
 		});
 	});
</script>
    <div id="wrap">
        <main id="main">
        
        <!-- Modal content -->
		<div id="modal" class="searchModal">
			<div class="search-modal-content">
				<table id="table">
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<label style="font-size:20px;"><b>아이템 사용</b></label>
								<hr style="border: 1.5px solid #013252; background: #013252; margin-bottom: 20px; margin-top:18px;" width="550px;">
							</td>
						</tr>

						<tr>
							<td>
								<label style="font-size:18px;">보유 열람권 개수 :&nbsp;&nbsp;<label style="color:orange; font-weight:bold;">3</label> 개</label>
								<br><br>
								<label style="text-align:left;">* 이력서 1개당 열람권 1개가 차감됩니다.</label><br>
								<label style="text-align:left;">* 사용 버튼을 누르면 해당 이력서를 열람할 수 있습니다.</label><br>
								<label style="text-align:left;">* 30일 동안 이력서 열람이 가능합니다.</label><br>
								<label style="text-align:left;">* 이력서 열람 후에는 면접제의와 이력서 스크랩이 가능합니다.</label><br>
								<label style="text-align:left;">* 사용된 아이템은 <label style="color:orange;">환불이 불가</label>하오니 신중히 사용해주시기 바랍니다.</label>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td style="text-align:center;" id="buttons">
								<button id=confirmBtn type="button"><b>사용</b></button>
								<button id=closeBtn type="button"><b>취소</b></button>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
				</table>
			</div>
		</div> <!-- Modal 영역 끝 -->
		
                <fieldset style="text-align:left">
                    <legend>이력서 상세보기</legend>
                    <div class="form_wrap">
                        <div class="resume_title">
                        <%if(r.getResume_title()==null){ %>
                        <label for="_title">-</label>
                        <%}else{ %>
                        <label for="_title"><%= r.getResume_title() %></label>
                            <%} %>
                        </div>
                        <div class="basic_info">
                            <div class="basic_info_title"><h3>기본정보</h3></div>
                            <div class="basic_info_contents">
                                <table style="font-size: 18px; font-weight:bold">
                                    <tr class="table_info">
                                        <td colspan="3" width="120px" height="50px" style="text-align:center"><%= name2%>**</td>
                                        <td width="60px" style="text-align:center">|</td>
                                        <% if (r.getBirth_date()==null){%>
                                      	<td width="100px" style="text-align:center">-</td>
                                      	<% }else{ %>
                                        <td width="100px" style="text-align:center">********</td>
                                        <%} %>
                                        <td width="60px" style="text-align:center">|</td>
                                        <%if (r.getGender() == null){ %>
                                        <td width="40px" style="text-align:center">-</td>
                                        <%}else{ %>
                                        <td width="40px" style="text-align:center">**</td>
                                        <%} %>
                                        <td width="60px"></td>
                                        <td width="60px"></td>
                                        <td width="60px"></td>
                                        <td width="60px"></td>
                                       	<td width="60px"></td>
                                       	<td width="60px"></td>
                                       	<%if(titleImg.getChangeName()==null){ %>
                                       	<td width="150px" rowspan="3"><img src="../../static/images/user/photo_icon_03.png"></td>
                                       	<% }else{ %>
                                        <td width="130px" rowspan="3"><img src="<%=request.getContextPath()%>/thumbnail_uploadFiles/<%=titleImg.getChangeName()%>" width="125px" height="163px"></td>
  										<% } %>      
  										<td rowspan="3">
                                    		<div id="ibBtn">
                                    		<button type="button" id="interviewBtn">연락처 확인</button>
                                    		<br>
                                        	<button type="button" id="bookmarkBtn">스크랩</button>
                                        	</div>
                                    	</td>                            
                                    </tr>

                                    <tr>
                                        <td width="20px" height="50px"></td>
                                        <td width="40px"><img alt="logo.png" src="../../static/images/user/msg.png" width="30px" height="30px" ></td>
                                        <%if (r.getRemail() ==null){ %>
                                        <td colspan="3">-</td>
                                        <%}else{ %>
                                        <td colspan="3">***************</td>
                                        <% } %>
                                        <td width="40px"><img alt="logo.png" src="../../static/images/user/tel.png" width="30px" height="30px" style="margin-left:17px"></td>
                                        <td colspan="3">***********</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td height="60px"><img alt="logo.png" src="../../static/images/user/home.png" width="30px" height="30px"></td>
                                        <td colspan="9">***************</td>   
                                        <td></td>
                                        <td></td>
                                    </tr>

                                </table>
                        
                            </div>
                        </div>
                        <br><br>
                        
                        <div id="tableDiv">
                             
                                <table id="rNum">
                                    <tr>
                                    	<td style="display:none;"><%=r.getResume_no() %></td>
                                        <td><h3>학력사항&nbsp;&nbsp;</h3></td>
                                        <td><h4>최종학력&nbsp;&nbsp;|&nbsp;&nbsp;총 &nbsp;<a style="color:red">1</a>건</h3></td>
                                        
                                    </tr>
                                </table>
                                <br>
                                <table style="text-align:center; border:1px Ridge #dcdcdc">
                                     <tr style="border:1px solid black" class="tableBack">
                                         <th width="400px" height="50px">재학기간</th>
                                         <th width="200px">구분</th>
                                         <th width="400px">학교명(소재지)</th>
                                         <th width="400px">전공</th>
                                         <th width="200px">학점</th>
                                     </tr>
                                      <tr style="border:1px solid black"class="tableShit" >
                                        <td width="400px" height="50px"><%= e.getSchool_enroll_date() %> ~ <%= e.getSchool_graduate_date() %></td>
                                        <td><%= e.getGraduate_type() %></td>
                                        <td><%= e.getSchool_name() %></td>
                                        <td><%= e.getMajor() %></td>
                                        <td>-</td>
                                     </tr>
                                    <% if(e.getEdu_choice() == null){ %>
                                    <% }else{ %>
                                    <% if(e.getEdu_choice().equals("대학교")) {%>
                                     <tr style="border:1px solid black" class="tableShit">
                                        <td width="400px" height="50px"><%= e.getUni_enroll_date()%> ~ <%= e.getUni_graduate_date() %></td>
                                        <td><%= e.getUni_graduate_type() %></td>
                                        <td><%= e.getUni_name() %></td>
                                        <td><%= e.getUni_major_name() %></td>
                                        <td><%= e.getUni_score() %>/<%= e.getUni_score_choice() %></td>
                                     </tr>
                                     <% }else{ %>
                                     <% } %>
                                     <%} %>
                                </table>
                                
                           
                            <br><br>


                            <table>
                                <tr>
                                    <td><h3>경력사항&nbsp;&nbsp;</h3></td>
                                	<%if(r.getCareer_yn()==null){ %>
                                	<td><h4>|&nbsp;&nbsp;신입</h4></td>
                                	<% }else{ %>
                                    <% if(r.getCareer_yn().equals("N")) { %>
                                    <td><h4>|&nbsp;&nbsp;신입</h4></td>
                                    <% }else{ %>
                                    <td><h4>|&nbsp;&nbsp;총 &nbsp;<a style="color:red"><%= c.getCareer_year() %></a>년</h4></td>
                                    <% }  %>
                                    <% } %>
                                </tr>
                            </table>
                            <br>
                            	<%if(r.getCareer_yn()==null){ %>
                            	<% }else{ %>
                                <% if(r.getCareer_yn().equals("Y")) {%>
                            <table style="text-align:center; border:1px Ridge #dcdcdc;">
                                <tr class="tableBack">
                                    <th width="350px" height="50px">근무기간</th>
                                    <th width="250px">회사명</th>
                                    <th width="400px">부서 / 직급 · 직책</th>
                                    <th width="400px">직종</th>
                                    <th width="200px">근무지역</th>
                                </tr>
                                <tr>
                                   <td width="35px" height="50px" style="border-bottom:1px Ridge #dcdcdc;"><%= c.getCompany_in_year() %> ~ <%= c.getCompany_out_date() %></td>
                                   <td style="border-bottom:1px Ridge #dcdcdc;"><%= c.getCompany_name() %></td>
                                   <td style="border-bottom:1px Ridge #dcdcdc;"><%= c.getCareer_dept() %> / <%= c.getJob_level() %></td>
                                   <td style="border-bottom:1px Ridge #dcdcdc;"><%= c.getCareer_kind() %></td>
                                   <td style="border-bottom:1px Ridge #dcdcdc;"><%= c.getCareer_area() %></td>
                                </tr>
                                <tr>
                                	<td colspan="1" style="height:70px; align:center"><div style="margin-left:70px; text-align:center; width:100px; height:40px; border:1.5px solid green; border-radius: 14px; line-height:35px;">당담업무</div></td>
                                	<td style="text-align:left"><p style="font-size:18px"><%= c.getTask() %></p></td>
                                </tr>
                                <% }else{ %>
                                <%} %>
                                <%} %>
                           </table>
                           <br><br>

                           <table>
                            <tr>
                                <td><h3>자기소개서&nbsp;&nbsp;</h3></td>
                            </tr>
                        </table>
						<br>
                        <table class="self_intro_table" style="background-color:#ECF0F7">
                            <caption>자기소개서</caption>
                            <tbody>
                                <tr> 
                                    <th class="re_title">제목</th>
                                    <td class="re_title_area">
                                    	<%if (r.getIntroducetitle()==null){ %>
                                    	<h3 style="font-weight:bold;">-</h3>
                                    	<%}else{ %>
                                        <h3 style="font-weight:bold;"><%= r.getIntroducetitle()%></h3>
                                        <%} %>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="re_contents">내용</th>
                                    <td class="re_contents_area" height="500px" width="1100px" align="left" valign="top">
                                    <%if (r.getIntroducecontent()==null){ %>
                                    <p>-</p>
                                    <%}else{ %>
                                        <p><%= r.getIntroducecontent() %> </p>
                                    <%} %>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br><br><br>
                        <table>
                            <tr>
                                <td><h3>희망조건&nbsp;&nbsp;</h3></td>
                            </tr>
                        </table>
						<br>
                        <table style="border: 1px solid #7D7777" width="1100">

                            <tbody>
                                <tr>
                                    <td class="terms_desired1"><div style="text-align:center; width:100px; height:40px; border:1.5px solid green; border-radius: 14px; line-height:35px;">관심지역</div></td>
                                    <%if (r.getInterestarea()==null){%>
                                    <td class="terms_desired1" width="850px">-</td>
                                    <%}else{ %>
                                    <td class="terms_desired1" width="850px"><%= r.getInterestarea() %></td>
                                    <%} %>
                                </tr>
                                <tr>
                                    <td class="terms_desired2"><div style="text-align:center; width:100px; height:40px; border:1.5px solid green; border-radius: 14px; line-height:35px;">관심직종</div></td>
                                    <td class="terms_desired2"></td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="resume_check">
                            <div class="resume_check_ch">
                                <span>위에 모든 기재 사항은 사실과 다름없음을 확인합니다.</span>
                            </div>
                            <p class="resume_date">
                                <span>작성일</span>
                                <span class="resume_date_date"><%= r.getResume_enroll_date() %></span> |
                                <span>작성자</span>
                                <span class="resume_date_user"><%= m.getMemberName() %></span>
                            </p>
                        </div>
                        <div class="sub_btn">
                            <button id="backButton">뒤로</button>
                        </div>
                    </div>
                </fieldset>
        </main>
        <footer></footer>
    </div>
    <br>
    <br>
    <br>
	<script>
	function isEmpty(value){

	    if(value == null || value.length === 0) {

	           return "";

	     } else{

	            return value;

	     }

	}
	</script>    
	
</body>
</html>