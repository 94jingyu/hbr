<%@page import="com.kh.hbr.member.controller.bSearchMemberServlet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,com.kh.hbr.member.model.vo.*"%>
<%
	ArrayList<Business> list = (ArrayList<Business>) request.getAttribute("list");

	System.out.println("상세보기 해줄 기업의 정보 : " + list);
	
	Business bs = (Business) list.get(0);

	String bMemberID = bs.getBmemberId();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>기업인증확인</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	a {text-decoration: none; color: black;}
	input {width:410px; height:40px; font-size:15px; margin: 2px;}
	textArea {color: #646464;}	
	
	/* 화면 전체 영역 */
	#wrap {
		width: 1200px; 
		margin: 0 auto; 
		height:100px; 
		top:20%; 
		margin-top:50px;
	}
	
	/* 내용 입력 영역 */
	#whiteBox {
		width:650px; 
		height:740px; 
		padding: 40px; 
		border: 1px solid #C4C4C4; 
		margin: 0 auto; 
		background: white;
	}
	
	/* 승인 버튼 */
	#agreeBtn {
		width:150px; 
		height:50px; 
		font-size:20px; 
		background: #013252; 
		color: white; 
		padding: 5px; 
		margin: 2px; 
		border-radius:10px; 
		border: 1px; 
		outline: 1px;
	} 
	
	#agreeBtn:hover {cursor:pointer;}
	
	/* 반려 버튼 */
	#rejectBtn {
		width:150px; 
		height:50px; 
		font-size:20px; 
		background: rgb(192, 192, 192); 
		color: white; 
		/* padding: 5px; 
		margin: 2px;  */
		border-radius:10px; 
		border: 1px; 
		outline: 1px;
	} 
	
	#rejectBtn:hover {cursor:pointer;}
	
	/* 기업인증 전 이전으로 버튼 */
	#backBtn {float: left; width:100px; height:50px; font-size:17px; background-color: transparent !important; 
		background-image: none !important; border-color: transparent; border: none; margin: 2px;}
		
	/* 기업인증 후 이전으로 버튼 */
	#backBtn2 {
		width:150px; 
		height:50px; 
		font-size:20px; 
		background: #013252; 
		color: white; 
		padding: 5px; 
		margin: 2px; 
		border-radius:10px; 
		border: 1px; 
		outline: 1px;
	}
	
	#backBtn2:hover {cursor:pointer;}
	
	/* 밑줄(구분선) */
	#detailHr {
		width: 630px;
		margin-top: 15px;
		margin-bottom: -10px;
	}
	
	/* td 간격 */
	/* td {padding:12px;} */
	
	/* 기업 정보 form ID */
	#ApproveForm {margin-top: -30px;}
	
	/* 승인, 반려 버튼 위치 가운데로 조정 */
	#buttons {text-align:center;}
	
	/* 기업 정보들 */
	#cNO {margin-left: 60px;}
	
	#cType {margin-left: 110px;}
	
	#cName {margin-left: 90px;}
	
	#ceoName {margin-left: 110px;}
	
	#cPhone {margin-left: 95px;}
	
	#cAddress {margin-left: 110px;}
	
	#fax {margin-left: 80px;}
	
	#homepage {margin-left: 80px;}
	
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
	
	/* Modal background */
	.searchModal {
		display: none; /* Hidden by default */
		position: fixed; /* Stay in place */
		z-index: 10; /* Sit on top */
		left: 0;
		top: 0;
		width: 100%; /* Full width */
		height: 100%; /* Full height */
		overflow: auto; /* Enable scroll if needed */
		background-color: rgb(0,0,0); /* Fallback color */
		background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	 
	/* Modal content */
	.search-modal-content {
		background-color: #fefefe;
		margin: 15% auto; /* 15% from the top and centered */
		padding: 20px;
		border: 1px solid #888;
		width: 30%; /* Could be more or less, depending on screen size */
		height: 500px;
	}
	
	/* 확인 버튼 */
	#confirmBtn {
		width:150px; 
		height:50px; 
		font-size:20px; 
		background: #013252; 
		color: white; 
		padding: 5px; 
		margin: 2px; 
		border-radius:10px; 
		border: 1px; 
		outline: 1px;
	} 
	
	#confirmBtn:hover {
		cursor:pointer;
	}
	
	/* 취소 버튼 */
	#closeBtn {
		width:150px; 
		height:50px; 
		font-size:20px; 
		background: rgb(192, 192, 192); 
		color: white; 
		/* padding: 5px; 
		margin: 2px;  */
		border-radius:10px; 
		border: 1px; 
		outline: 1px;
	} 
	
	#closeBtn:hover {
		cursor:pointer;
	}
</style>
<script>

	//관리자 기업인증내역 목록으로 돌아가기
	function backApproveInfo() {
		location.href = "<%=request.getContextPath()%>/selectApprove.ap";
	}
	
	//승인 버튼 클릭
	function agree(){ 
		
		var check = window.confirm("해당 기업의 인증을 승인하시겠습니까?");
		
		if(check) {
			//hidden 태그의 value 값을 id에 담아줌
			var id = $("#bMemberID").val();
				
			//승인시 DB 업데이트하는 서블릿으로 이동
			location.href="<%=request.getContextPath()%>/certifyApprove.ap?bMemberId=" + id;
		}
	};
	
	//반려 버튼 클릭
	function reject() {
		$("#modal").show();		
	}
	
	//Modal 확인 버튼 클릭
	function modalConfirm() {
		var reason = $("#reason").val();
		
		if(reason === "") {
			alert("반려사유를 작성해주세요.");
		
		} else {
			
			alert("해당 기업의 인증신청이 반려되었습니다.");
			
			//hidden 태그의 value 값을 id에 담아줌
			var id = $("#bMemberID").val();
				
			//반려시 DB 업데이트하는 서블릿으로 이동 (id, reason 전달)
			location.href="<%=request.getContextPath()%>/certifyReject.ap?bMemberId="+id+"&rejectReason="+reason;
		}
	}
	
	//Modal 닫기 버튼 클릭
	function closeModal() {
		$('.searchModal').hide();
	}
	
</script>
</head>
<body>
	<div id="wrap">
		<!-- Modal content -->
		<div id="modal" class="searchModal">
			<div class="search-modal-content">
				<table id="table">
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td style="text-align:center;">
								<label style="font-size:80px; color:orange;"><b>!</b></label>
							</td>
						</tr>
						<tr>
							<td style="text-align:center;">
								<label style="font-size:20px;"><b>해당 기업의 인증을 반려하시겠습니까?</b></label>
								<hr style="border: 1.5px solid #013252; background: #013252; margin-bottom: 20px; margin-top:18px;" width="550px;">
							</td>
						</tr>
						<tr>
							<td style="text-align:center;">
							<textarea id="reason" style="width:520px; height:150px; font-size:18px; margin-top:10px;" maxlength="150" spellcheck="false" placeholder="반려사유를 반드시 작성해주세요."></textarea>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td style="text-align:center;" id="buttons">
								<button id=confirmBtn type="button" onclick="modalConfirm();"><b>확인</b></button>
								<button id=closeBtn type="button" onclick="closeModal();"><b>취소</b></button>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
				</table>
			</div>
		</div>

		<!-- 기업 정보 -->
		<div class="ApproveArea">
			<h1 id="ApproveTitle" align="center">기업인증확인</h1>
			<br> <br>
			<div id=whiteBox>
				<!-- whiteBox -->
				<br> <br>
				<form id="ApproveForm" action="" method="post">
					<table id="table">
						<tr>
							<td colspan="8"><h3 style="font-size: 19px;">기업정보</h3>
								<hr id="detailHr" style="border: 1.5px solid #013252; background: #013252; margin-bottom: 20px;"></td>
							<!-- 기업회원 아이디를 hidden으로 숨겨놓음 -->
							<input type="hidden" id="bMemberID" value="<%=bMemberID%>">
						</tr>
						<% for(Business b : list) { 
						
						String companyType = b.getCompanyType();
						String homepage = b.getHomepage();
						String fax = b.getFax();
						String[] address = b.getCompanyAddress().split("-");
						String companyAddress = address[0] + " " + address[1];
						String certStatus = b.getCertStatus();						
					%>
						<tr>
							<td height="50"><label>사업자 등록번호 <label
									style="color: red;">*</label></label> <label id="cNO"><b><%= b.getCompanyNo() %></b></label>
							</td>
						</tr>
						<tr>
							<td height="50"><label>기업구분 <label
									style="color: red;">*</label></label> <% if(companyType.equals("개인")) { %>
								<label id="cType"><b>개인사업자</b></label> <% } else {%> <label
								id="cType"><b>법인사업자</b></label> <% } %></td>
						</tr>
						<tr>
							<td height="50"><label>기업/상호명 <label
									style="color: red;">*</label></label> <label id="cName"><b><%=b.getCompanyName() %></b></label>
							</td>
						</tr>
						<tr>
							<td height="50"><label>대표자명 <label
									style="color: red;">*</label></label> <label id="ceoName"><b><%=b.getCeoName() %></b></label>
							</td>
						</tr>
						<tr>
							<td height="50"><label>회사연락처 <label
									style="color: red;">*</label></label> <label id="cPhone"><b><%=b.getCompanyPhone() %></b></label>
							</td>
						</tr>
						<tr>
							<td height="50"><label>회사주소 <label
									style="color: red;">*</label></label> <label id="cAddress"><b><%=companyAddress%></b></label>
							</td>
						</tr>
						<tr>
							<td height="50"><label>팩스번호(선택)</label> <% if(fax == null) {%>
								<label id="fax"><b>없음</b></label> <% } else {%> <label id="fax"><b><%=b.getFax() %></b></label>
								<%} %></td>
						</tr>
						<tr>
							<td height="50"><label>홈페이지(선택)</label> <% if(homepage == null) { %>
								<label id="homepage"><b>없음</b></label> <% } else {%> <label
								id="homepage"><b><%=b.getHomepage() %></b></label> <%} %></td>
						</tr>
						<tr>
							<td height="80"></td>
						</tr>
						<% if(certStatus == null) { %>
						<tr>
							<td><a id=backBtn style="cursor:pointer;" onclick="backApproveInfo();"><b> <
										&nbsp; 이전으로</b></a></td>
						</tr>
						<tr>
							<td height="100" id="buttons">
								<button type="button" id="agreeBtn" onclick="agree();">
									<b>승인</b>
								</button>
								<button type="button" id="rejectBtn" onclick="reject();">
									<b>반려</b>
								</button>
							</td>
						</tr>
						<% } else if(certStatus.equals("Y") || certStatus.equals("N")){ %>
						<tr>
							<td height="100" id="buttons">
								<button type="button" id="backBtn2" onclick="backApproveInfo();">
									<b>이전으로</b>
								</button>
							</td>
						</tr>
						<% } %>
					<% } %>
					</table>
				</form>
			</div>
			<!-- whiteBox -->
			<br>
			<br>
			<br>
			<footer>
				<table>
					<span><a href="">홈</a></span>
					<span><a href="">이용약관</a></span>
					<span><a href="">개인정보처리방침</a></span>
					<span><a href="">ⓒHBR.Co. Lth</a></span>
					</footer>
					</div>
					<!-- /기업정보-->
					</div>
</body>
</html>