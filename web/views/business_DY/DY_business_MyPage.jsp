<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,com.kh.hbr.member.model.vo.*,com.kh.hbr.approve.model.vo.*,com.kh.hbr.board.jengukBoard.model.vo.Attachment"%>
<%-- <% 
	//기업인증 신청여부에 따른 기업인증현황 문구 변경
	/* String cPhone = ((Business) (request.getSession().getAttribute("loginUser"))).getCompanyPhone(); */

	//마이페이지 기업정보 
	String cName = ((Business) (request.getSession().getAttribute("loginUser"))).getCompanyName();
	//주소에서 상세주소와의 구분자 '-'를 없애주고 띄어쓰기를 넣어줌
	String cAddress[] = ((Business) (request.getSession().getAttribute("loginUser"))).getCompanyAddress().split("-");
	String address = cAddress[0] + " " + cAddress[1];
	
	//마이페이지 담당자정보
	String mName = ((Business) (request.getSession().getAttribute("loginUser"))).getManagerName();
	String mPhone = ((Business) (request.getSession().getAttribute("loginUser"))).getManagerPhone();
	String mEmail = ((Business) (request.getSession().getAttribute("loginUser"))).getManagerEmail();
%> --%>
<%
	ArrayList<Mypage> itemList = (ArrayList<Mypage>) session.getAttribute("itemList");
	ArrayList<Mypage> recruitTotalList = (ArrayList) session.getAttribute("recruitTotalList");
	ArrayList<Mypage> recruitInfoList = (ArrayList) session.getAttribute("recruitInfoList");
	ArrayList<Mypage> certStatusList = (ArrayList) session.getAttribute("certStatusList");
	ArrayList fileList = (ArrayList) session.getAttribute("fileList");
	
/* 	System.out.println("itemList : " + itemList);
	System.out.println("recruitTotalList : " + recruitTotalList);
	System.out.println("recruitInfoList : " + recruitInfoList);
	System.out.println("certStatusList : " + certStatusList);
	System.out.println("fileList : " + fileList); */
	
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>마이페이지</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}

	#wrap {width: 1200px; margin: 0 auto;}
	
	/* 상단 로그인 영역 */
	.topAfterLogin {margin-top: 30px;}
	
	/* 헤더 영역 */
	header {width: 100%; height: 150px; }
	
	/* 상단메뉴바 */
	.topMenu {
    	display: inline-block;
    	width: 100%;
    	height: 50px;
    	background: #013252;
    	line-height: 30px;          /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	    vertical-align: middle;     /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	    text-align: center;         /* 글씨 정렬을 가운데로 설정 */  
	    margin-top: 18px;	/* *****로고랑 메뉴바 사이 여백 설정 ***** */
	    font-weight: bold;
  	}
  	
  	.topMenu>div { float: right; }
  	
  	.topMenu>ul {
   		float: left;
    	margin: 0;
    	padding: 0;
    	list-style: none;
  	}
  	.topMenu>ul>li {
    	display: inline-block;
    	padding: 0;
  	}
  	
  	.topMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: white;
  	}
  	
  	/* 상단메뉴바 마우스 클릭 시 효과 */
  	.topMenu a:hover { 
		color:#2FA599;
   		background: rgb(210, 210, 210);
  	}
  	
  	/* ================ 기업정보 / 담당자정보 ================ */
	#info1, #info2, .left, .BogumInfo, #kiupLogo {
		border:2px solid rgb(192, 192, 192);
	}
	
	/* 기업정보, 담당자정보 동일 div2 클래스 */
  	.b1_container {
		display:inline-block;
	}
	
	/* 기업 정보 */
	#info1 {
		width:780px;
		height:210px;
		float: left;
		margin-left: 30px;
		margin-top: 20px;
	}
	
	/* 기업 로고 사진 */
	#kiupLogo {
		width:280px;
		height:130px;
		float:left;
		margin-left: 5%;
		margin-top: 5%;
	}
	
	/* 기업명/기업주소/기업정보수정 버튼 영역 */
	#kiupEdit {
		width:420px;
		height:180px;
		float:right;
		margin-right: 1%;
		margin-top: 1%;
	}
	
	/* 기업정보수정 버튼 */
	#kiupEditButton {
	     height: 40px;
	     width: 130px;
		 border: 1px solid rgb(180, 180, 180);
		 background: white;
		 margin-left: 67%;
		 margin-top: 1%;
	}
	
	/* 담당자 정보 */
	#info2 {
		width:340px;
		height:210px;
		margin-left: 10px;
		margin-top: 20px;
		text-align: center;
	}
	
	/* ========== 게재중 공고 / 심사대기중 공고 / 하기 버튼 ========== */
	
	/* 게재중공고, 심사대기중 공고 div class1 */
	.BogumInfo {
		width:880px;
		height:210px;
		float: left;
		margin-left: 15px;
		margin-top: 10px;
		color: black;
	}

	/* 게재중 공고, 심사대기중 공고 div class2 */
  	.bogumIng, .bogumWaiting, .bogumSubmit {
		display: inline-block;
  		margin-top:60px;
  	}
  	
  	/* 게재중 공고 */
  	.bogumIng {
  		width:180px;
		height:100px;
		margin-inline-start: 50px;
  	}
  	
  	/* 심사대기중 공고 */
  	.bogumWaiting {
  		width:180px;
		height:100px;
		margin-inline-start: 50px;
  	}
  	
  	/* 보금자리 등록하기 버튼 div class */
  	.bogumSubmit {
  		width:130px;
		height:130px;
		float:right;
		margin-right: -30%;
		margin-top:40px;
  	}
  	
  	/* 보금자리 등록하기 버튼 */
	.bogumR {
		background: #013252;
		border: 0px;
		color: white;
		height: 120px;
		width: 120px;
		text-size: 15px;
		border-radius:100%;
	}

	/* ================ 최근 등록한 보금자리 내역 영역 ================ */
	
	/* 최근 등록한 보금자리 div class */
	.BogumInfo2 {
		width:880px;
		height:210px;
		float: left;
		margin-left: 15px;
		margin-top: 10px;
		color: black;
	}
	
	/* 보금자리 내역 테이블 div class */
	.BogumInfo3 {
		float: left;
		margin-left: 35px;
		margin-top: -120px;
	}
	
	/* 보금자리 내역 테이블 클래스명 */
	 .jariInfo {
    	 width: 840px;
         height: 170px;
      	 text-align: center;
         border-collapse: collapse;
   	}

  	 .jariInfo th {
         background: rgb(210, 210, 210);
         height: 40px;
    }   
   
     .jariInfo th, td {border: 1.5px solid rgb(180, 180, 180);}
    
     /* 테이블 내용 td id */
    #tableContent {
    	text-align:left;
    	margin-left:25px;
    }
    
    /* 더보기 > */
    .more {
    	float:right;
		margin-right: 20px;
    }
    
    /* 상태 - 심사대기 */
    #simsaWaiting {
    	border: 1.3px solid orange;
		border-radius:8px;
		padding: 5px;
    }
    
    /* 상태 - 마감 */
    #simsaEnd {
    	border: 1.3px solid navy;
		border-radius:8px;
		padding: 5px;
    }
    
    /* 상태 - 게재중 */
    #simsaApproved {
    	border: 1.3px solid #2FA599;
		border-radius:8px;
		padding: 5px;
    }
    
    /* 상태 - 반려 */
    #simsaRejected {
    	border: 1.3px solid hotpink;
		border-radius:8px;
		padding: 5px;
    }
	
	/* 수정, 마감버튼 동일 클래스 */
	.tableButton {
		margin-left: 690px;
		margin-top: 25px;
		background: #013252;
		border: 0px;
		color: white;
		height: 35px;
		width: 70px;
		text-size: 15px;
	}
	
	/* 마감버튼 */
	.tableButton2 {margin-left: 5px;}
	
	/* 새로운 보금자리를 등록해보세요 */
	#saero {margin-left: 230px;}
	
	/* 일자리를 빨리 등록할수록~ */
	#saero2 {margin-left: 170px;}
	
	/* 보금자리 등록하기 버튼 */
	.bogumSubmit2 {
	     height: 40px;
	     width: 170px;
		 border: 1px solid rgb(180, 180, 180);
		 background: white;
		 margin-left: 320px;
	}
	
	/* ================ 좌측 메뉴바 ================ */
	
	/* left 메뉴 aside 클래스 */
	.leftMenu {
		float: left; 
		width: 20%; 
		height: 580px; 
		margin-left: 30px; 
		margin-top: 10px;
	}
	
    .leftMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: black;
  	}
	
	/* left 메뉴 상품보유현황, 기업인증현황, 인재검색, 고객센터 동일 클래스 */
	.leftblock {display: block;}
	
	/* 기업인증현황, 인재검색, 고객센터 */
	.left2, #left3, #left4 {margin-top:10px;}
	
	/* 상품보유현황 */
	#left1 {height:640px;}
	
	/* ******** 기업인증현황 ********* */
	/* 기업인증 신청전 */
	#beforeApply {height:290px;}
	/* 기업인증 심사중 */
	#simsaIng {height:310px;}
	/* 기업인증 승인 */
	#approve {height:310px;}
	/* 기업인증 반려 */
	#reject {height:315px;}
	
	/* 인재검색 */
	#left3 {height:260px;}
	
	/* 고객센터 */
	#left4 {height:250px;}
	
	/* left메뉴 가로선 */
	#leftHr {
		width:90%;
		color:lightgray;
		size:2px;
		margin-left: 5%;
	}
	
	/* 블렛 삭제 */
	ul {list-style:none;}
	
	a {text-decoration: none;}  
	
	/* 보금자리 제목 ... 처리 */
	.recruitTitle {
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
		width: 320px;
		height: 23px;
		display: inline-block;
	}
	
	/* footer 영역 */
	.footer {border: 1px solid #afafaf; width: 1200px; height: 210px; text-align: center; background-color: #afafaf; margin-top:30px;}
	.footer td {border: 1px solid #afafaf;}
	table{margin: 0 auto; padding: 30px;}
	
	/* Modal background */
	.searchModal {
		display: none; /* Hidden by default */
		position: fixed; /* Stay in place */
		z-index: 10; /* Sit on top */
		left: 0;
		top: 0;
		width: 100%; /* Full width */
		height: 100%; /* Full height */
		overflow: auto; /* Enable scroll if needed */
		background-color: rgb(0,0,0); /* Fallback color */
		background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	
	/* Modal content */
	.search-modal-content {
		background-color: #fefefe;
		margin: 15% auto; /* 15% from the top and centered */
		padding: 20px;
		border: 1px solid #888;
		width: 400px; /* Could be more or less, depending on screen size */
		height: 520px;
	}
	
	/* 확인 버튼 */
	#confirmBtn {
		width:120px; 
		height:40px; 
		font-size:16px; 
		background: #013252; 
		color: white; 
		padding: 5px; 
		margin: 2px; 
		border-radius:10px; 
		border: 1px; 
		outline: 1px;
	} 
	
	#confirmBtn:hover {
		cursor:pointer;
	}
	
	/* 취소 버튼 */
	#closeBtn {
		width:120px; 
		height:40px; 
		font-size:16px; 
		background: rgb(192, 192, 192); 
		color: white; 
		/* padding: 5px; 
		margin: 2px;  */
		border-radius:10px; 
		border: 1px; 
		outline: 1px;
	} 
	
	#closeBtn:hover {
		cursor:pointer;
	}

</style>
<script>
	/* 기업정보수정 버튼 클릭 */
	$(function() {
		$("#kiupEditButton").click(function() {
			location.href = "<%=request.getContextPath()%>/views/common/E_business_UpdateInfoPage1.jsp";
		});
	});
	
	/* 보금자리 등록버튼 클릭 (인증신청 전) */
	$(function() {
		$("#beforeCertify").click(function() {
			alert("기업인증 승인 후 보금자리 등록이 가능합니다.");
		});
	});
	
	/* 보금자리 등록버튼 클릭 (인증신청 후 - 심사중) */
	$(function() {
		$("#afterCertify").click(function() {
			alert("기업인증 심사중입니다. 승인 후 보금자리 등록이 가능합니다.");
		});
	});
	
	/* 보금자리 등록버튼 클릭 (인증신청 후 - 승인) */
	$(function() {
		$("#boguemRegister").click(function() {
			location.href = "<%=request.getContextPath()%>/views/boguem_dj/DJ_boguem_register.jsp";
		});
	});
	
	$(function() {
		$("#boguemRegister2").click(function() {
			location.href = "<%=request.getContextPath()%>/views/boguem_dj/DJ_boguem_register.jsp";
		});
	});
	
	
	/* 보금자리 등록버튼 클릭 (인증신청 후 - 반려) */
	$(function() {
		$("#unableToRegister").click(function() {
			alert("기업인증 승인 후 보금자리 등록이 가능합니다.");
		});
	});
	
	/* 최근 등록한 보금자리 더보기  */
	$(function() {
		$("#moreBogumList").click(function() {
			location.href = "<%=request.getContextPath()%>/views/boguem_dj/DJ_boguem_register_manage.jsp";
		});
	});
	
	/* 최근 등록한 보금자리 상세보기 */
	$(function(){
      var a = $("tbody").children().each(function(index){
         
         if(index != 0) {
            $(this).children().eq(2).click(function(){
               var rnum = $(this).parent().children().eq(1).text();
               //전체 공백을 제거해줌
               var afterRnum = rnum.replace(/(\s*)/g,"");
  
               location.href="<%=request.getContextPath()%>/registerView.bg?num=" + afterRnum;
            });
         }
      });
   });
	
	/* 상품구매 바로가기 */
	$(function() {
		$("#buyProduct").click(function() {
			location.href="<%=request.getContextPath()%>/views/payment/payment.jsp";
		});
	});
	
	/* 인증신청 바로가기 */
	$(function() {
		$("#approveRequest").click(function() {
			location.href = "<%=request.getContextPath()%>/bringInfo.ap";
		});
	});
	
	/* 재신청 바로가기 */
	$(function() {
		$("#certifyReapply").click(function() {
			location.href = "<%=request.getContextPath()%>/reapplyCompanyInfo.ap";
		});
	});
	
	/* 인재검색 바로가기 */
	$(function() {
		$("#applicantSearch").click(function() {
			location.href = "<%=request.getContextPath()%>/applicantSearch.bs";
		});
	});
	
	/* 기업정보 카메라 이미지 클릭 */
	$(function() {
		$("#camera").click(function(){
			$("#modal").show();	
		});
	});
	
	/* Modal 취소 버튼 클릭  */
	$(function() {
		$("#closeBtn").click(function() {
			$('.searchModal').hide();
		});
	});
	
</script>
</head>
<body>
	<div id="wrap">
	<%@ include file="../guide/business_menubar.jsp" %>
	<%
		//마이페이지 기업정보 
		String companyName = bloginUser.getCompanyName();
		/* String cAddress = bloginUser.getCompanyAddress(); */
			
		//마이페이지 담당자정보
		String managerName = bloginUser.getManagerName();
		String managerPhone = bloginUser.getManagerPhone();
		String managerEmail = bloginUser.getManagerEmail();
		
		//기업인증 신청여부에 따른 기업인증현황 문구 변경
		/* String companyPhone = bloginUser.getCompanyPhone(); */
		
		//기업인증상태 - 대기중(null)/승인(Y)/반려(N)
		/* String certStatus = bloginUser.getCertStatus(); */
		
		//현재 유저가 가지고 있는 보금자리 개수
		Mypage my = (Mypage) recruitTotalList.get(0);
		
		int waiting = my.getStatusWaiting();
		int approved = my.getStatusApproved();
		int end = my.getStatusEnd();
		int rejected = my.getStatusRejected();
		
		//기업인증현황에 필요한 정보
		Mypage my2 = (Mypage) certStatusList.get(0);
		
		String cAddress = my2.getCompanyAddress();
		String companyPhone = my2.getCompanyPhone();
		String certStatus = my2.getCertStatus();
		
		System.out.println("마이페이지 certStatus : " + certStatus);
		System.out.println("마이페이지 companyPhone : " + companyPhone);
		
	%>   
        <!-- 기업정보 -->
        <div class="container">
            <div id="info1" class="b1_container">
 				<div id="kiupLogo">	
 					<%if(fileList.isEmpty()) {%>
 						<img id="camera" src="/h/static/images/business/ccLogo.png" width="275px" height="125px" style="cursor:pointer; padding:2px;">
 					<% } else {
 						Attachment titleImg = (Attachment) fileList.get(0);
 					%>
 						<img id="camera" src="/h/thumbnail_uploadFiles/<%=titleImg.getChangeName()%>" width="275px" height="125px" style="cursor:pointer; padding:2px;">
 					<% } %>
 				</div>
 				<div id="kiupEdit">
 				<button id="kiupEditButton" style="font-size:15px; color:black; cursor:pointer;"><b>기업정보수정</b></button>
 				<br>
 				<!-- 기업인증 신청을 안했을경우 companyAddress 값이 null이기 때문에 조건문을 써줘야 NullPointerException이 나지 않음 -->
 				<% if(cAddress == null) { %>
 				<br>
 				<label style="font-size:28px; margin-left:5px;"><b><%=companyName%></b></label>
 				<%} else if(cAddress != null) { 
 					String address[] = cAddress.split("-");
 					String companyAddress = address[0] + " " + address[1];
 				%>
 				<label style="font-size:25px; margin-left:5px;"><b><%=companyName%></b></label>
 				<br><br>
 				<label style="font-size:18px; margin-left:5px;"><%=companyAddress%></label>
 				<%} %>
 				</div>
    	 	<!-- Modal content -->
    	<form id="logoForm" action="<%= request.getContextPath() %>/mypageLogo.bs" method="post" encType="multipart/form-data">
		<div id="modal" class="searchModal">
			<div class="search-modal-content">
				<table id="table">
						<thead>
						<tr>
							<td style="border:hidden;">
								<label style="font-size:18px;"><b>로고등록</b></label>				
								<hr style="border: 1.5px solid #013252; background: #013252; margin-bottom: 20px; margin-top:18px;" width="335px;">
							</td>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>
 							<img id="companyLogo"  src="/h/static/images/business/ccLogo.png" width="260px" height="110px" style="cursor:pointer; padding:30px; margin-left:7px; margin-top:7px;">
 							</td>
 						</tr>	
 						<tr>
							<td style="border:hidden;">&nbsp;</td>
						</tr>	
						<tr>
							<td style="border:hidden; font-size:14px;"><b style="color:orange;">10MB</b>이하의 이미지 파일(JPG, PNG, BMP)만 등록 <br>가능합니다.</td>
						</tr>
						<tr>
							<td style="border:hidden;">&nbsp;</td>
						</tr>
 						<tr>
 							<td style="border:hidden;">
 							<input type="file" name="cLogo" id="cLogo" onchange="loadImg(this, 1);">
 							</td>
						</tr>
						<tr>
							<td style="border:hidden;">&nbsp;</td>
						</tr>	
						<tr>
							<td style="border:hidden;">&nbsp;</td>
						</tr>
						<tr>
							<td style="text-align:center; border:hidden;" id="buttons">
								<button id="confirmBtn" type="submit"><b>등록</b></button>
								<button id="closeBtn" type="button"><b>취소</b></button>
							</td>
						</tr>
						<tr>
							<td style="border:hidden;">&nbsp;</td>
						</tr>
						</tbody>
				</table>
			</div>
		</div>
		</form>
            </div>
            <!-- 로고관련 스크립트 -->
    	 	<script type="text/javascript">
    	 	
    	 			function loadImg(value, num) {
    					if(value.files && value.files[0]) {
    						var reader = new FileReader();
    						
    						reader.onload = function(e) {
    							console.log(e.target.result);
    							switch(num) {
    								case 1 : $("#companyLogo").attr("src", e.target.result); break;    								
    							}   							
    						}  						
    						reader.readAsDataURL(value.files[0]);
    					}
    				}
    	 	</script>

		<!-- 담당자 정보 -->
            <div id="info2" class="b1_container">
            	<label id="certStatus" hidden><%=certStatus %></label>
            	<br><br>
  				<label style="font-size:18px;"> <b>채용담당자&nbsp;&nbsp;<label style="font-size:25px;"><%=managerName%></label>&nbsp; 님</b></label>
  				<br><br>
  				<label id="phone" style="font-size:25px;"><%=managerPhone%></label>
  				<br><br>
  				<% if(managerEmail.equals("@")) { %>
  					<label style="font-size:18px;">-</label>
  				<% } else { %>
  					<label style="font-size:18px;"><%=managerEmail%></label>
  				<% } %>
            </div>
		</div>
		
		<!-- 좌측 메뉴바 -->
        <aside class="leftMenu">
        	<div class="left leftblock" id="left1">
        	<br>&nbsp; <label style="font-size:20px;"><b>상품보유현황</b></label>
        	<br><br>
        	<hr id="leftHr">
        	<br>
        	<!-- 상품보유현황 -->
        	<% for (Mypage mp : itemList) { %>
           	<ul>
		   		<li><a style="font-size:18px;"><b>메인페이지 전용 상품</b></a>
		   			<a style="font-size:16px;">금도끼 : <label style="color:#2FA599;"><%=mp.getGoldTotal() %></label>개</a>
		   			<a style="font-size:16px;">은도끼 : <label style="color:#2FA599;"><%=mp.getSilverTotal() %></label>개</a>
		   			<a style="font-size:16px;">쇠도끼 : <label style="color:#2FA599;"><%=mp.getIronTotal() %></label>개</a>
		   		</li>
		   		<br>
		   		<hr id="leftHr">
		   		<br>
		    	<li><a style="font-size:18px;"><b>채용페이지 전용 상품</b></a>
		    		<a style="font-size:16px;">반짝 : <label style="color:#2FA599;"><%=mp.getStarTotal() %></label>개</a>
		    	</li>
		    	<br>
		    	<hr id="leftHr">
		    	<br>
		    	<li><a style="font-size:18px;"><b>이력서 열람권</b></a>
		    		<a style="font-size:16px;">남은 건수 : <label style="color:#2FA599;"><%=mp.getResumeTotal() %></label>건</a>
		    		<br>
		    		<br>
		    		<a id="buyProduct" href="#" style="color:navy; font-size:15px;"><b>상품구매 바로가기</b></a>
		    	</li>
	  		</ul>
	  		<% } %>
	  		</div>
	  		<!-- 기업인증현황 -->
        	<!-- 기업인증 신청전 -->
        	<% if(companyPhone == null) {%>
        	<div class="left left2 leftblock" id="beforeApply">
        	<br>&nbsp; <label style="font-size:20px;"><b>기업인증현황</b></label>
        	<br><br>
        	<hr id="leftHr">
        	<br>
           	<ul>
		   		<li>&nbsp;
		   		<img alt="research.png" src="/h/static/images/business/research.png" width="50px" height="50px" style="margin-left:60px">
		   		<a style="font-size:18px;"><b>기업인증</b>&nbsp; 전 입니다.</a>
		   		<br>
		    		<a id="approveRequest" href="#" style="color:navy; font-size:15px;"><b>인증신청 바로가기</b></a>
		    	</li>
	  		</ul>
	  		<!-- 기업인증 신청후 (심사중) -->
	  		<% } else if(certStatus == null) {%>
	  		<div class="left left2 leftblock" id="simsaIng">
        	<br>&nbsp; <label style="font-size:20px;"><b>기업인증현황</b></label>
        	<br><br>
        	<hr id="leftHr">
        	<br>
	  		<ul>
		   		<li>&nbsp;
		   		<img alt="research.png" src="/h/static/images/business/research.png" width="50px" height="50px" style="margin-left:80px">
		   		<a style="font-size:18px;"><b>기업인증 심사중</b>입니다.</a><br>
		   		<a style="font-size:15px;">인증 관련 문의는 <b>고객센터</b>로 문의 부탁드립니다.</a>
		    	</li>
	  		</ul>
	  		<!-- 기업인증 신청후 (승인) -->
	  		<% } else if(certStatus.equals("Y")) {%>
	  		<div class="left left2 leftblock" id="approve">
        	<br>&nbsp; <label style="font-size:20px;"><b>기업인증현황</b></label>
        	<br><br>
        	<hr id="leftHr">
        	<br>
	  		<ul>
		   		<li>&nbsp;
		   		<img alt="research.png" src="/h/static/images/business/research.png" width="50px" height="50px" style="margin-left:80px">
		   		<a style="font-size:18px;">기업인증이 <b>완료</b>되었습니다.</a><br>
		   		<a style="font-size:15px;">보금자리 등록이 가능합니다.</a>
		    	</li>
	  		</ul>
	  		<!-- 기업인증 신청후 (반려) -->
	  		<% } else if(certStatus.equals("N")) {%>
	  		<div class="left left2 leftblock" id="reject">
        	<br>&nbsp; <label style="font-size:20px;"><b>기업인증현황</b></label>
        	<br><br>
        	<hr id="leftHr">
        	<br>
	  		<ul>
		   		<li>&nbsp;
		   		<img alt="research.png" src="/h/static/images/business/research.png" width="50px" height="50px" style="margin-left:80px">
		   		<a style="font-size:18px;">기업인증이 <b>반려</b>되었습니다.</a><br>
		    		<a id="certifyReapply" href="#" style="color:navy; font-size:15px;"><b>재신청 바로가기</b></a>
		    	</li>
	  		</ul>
	  		<% } %>
	  		</div>
	  		<!-- 인재검색 -->
	  		<div class="left leftblock" id="left3">
        	<br>&nbsp; <label style="font-size:20px;"><b>인재검색</b></label>
        	<br><br>
        	<hr id="leftHr">
        	<br>
           	<ul>
		   		<li>
		   		<a style="font-size:18px;">나에게 딱 맞는 직원을<br>
		   						직접 찾아보세요.</a>
		   		<br>
		    		<a id="applicantSearch" style="color:navy; cursor:pointer;"><b>인재검색 바로가기</b></a>
		    	</li>
	  		</ul>
	  		</div>
	  		<!-- 고객센터 -->
	  		<div class="left leftblock" id="left4">
        	<br>&nbsp; <label style="font-size:20px;"><b>고객센터</b></label>
        	<br><br>
        	<hr id="leftHr">
        	<br>
           	<ul>
		   		<li>
		   		<a style="font-size:30px;"><b>1588-9350</b></a>
		   			<br>
		    		<a style="color:black; font-size:15px;">평일 : 09:00 ~ 18:00
		    		<br>(토/일요일 및 공휴일 휴무)</a>
		    	</li>
	  		</ul>
	  		<br><br><br><br><br><br><br><br><br>
	  		</div>
	  		<!-- footer영역 -->
	    	<div class="footer">
			<table>
				<tr>
					<td>홈&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;광고문의&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;제휴문의&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;인재채용&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;이용약관&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>개인정보처리방침</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;고객센터</td>
				</tr>
				<tr>
					<td colspan="7" style="height: 20px;"></td>
				</tr>
				<tr>
					<td colspan="7">고객센터: 1588-9350(평일 09:00 ~ 19:00 토요일 09:00 ~ 15:00)&nbsp;&nbsp;&nbsp;FAX : 02-123-456&nbsp;&nbsp;&nbsp;Email : sunshine@kh.co.kr</td>
				</tr>
				<tr>
					<td colspan="7">서울특별시 강남구 강남구 테헤란로14길 6 해볼래&nbsp;&nbsp;&nbsp;대표 : 윤햇살&nbsp;&nbsp;&nbsp;사업자등록번호 : 110-81-34859</td>
				</tr>
				<tr>
					<td colspan="7">통신판매업 신고번호 : 2020-서울역삼-0287호&nbsp;&nbsp;&nbsp;직업정보제공사업 신고번호 : 서울청 제2020-01호</td>
				</tr>
				<tr>
					<td colspan="7" style="height: 20px;"></td>
				</tr>
				<tr>
					<td colspan="7">Copyright ⓒ Sunshine Corp. All Right Reserved.</td>
				</tr>
			</table>	    
	    </div> 
        </aside>
        
        <!-- section(내용) 영역 -->
          	<div class="BogumInfo" style="display:table;">
          		<div style="position:absolute; left:50%; display:table-cell; vertical-align:middle; text-align:center;">
 					<div style="position: relative; top:-50%; left:-50%;">
 						<!-- 기업인증 신청전 -->
		   				<% if(companyPhone == null) {%>
		   				<div class="bogumIng">
 							<label style="font-size:20px;"><b>게재중 공고</b></label>
 							<br><br>
 							<label><b style="font-size:35px; color:#013252;">0</b></label>
 						</div>
		   				<div class="bogumWaiting">
		   					<label style="font-size:20px;"><b>심사대기중 공고</b></label>
		   					<br><br>
		   					<label><b style="font-size:35px; color:#013252;">0</b></label>
		   				</div>
		   				<div class="bogumSubmit">
		   					<button class="bogumR" id="beforeCertify"><br><br><label style="font-size:17px;"><b>보금자리<br>등록</b><br><br></label></button>
		   				</div>
		   				<!-- 기업인증 신청후 (심사중) -->
		   				<% } else if(certStatus == null) {%>
		   				<div class="bogumIng">
 							<label style="font-size:20px;"><b>게재중 공고</b></label>
 							<br><br>
 							<label><b style="font-size:35px; color:#013252;">0</b></label>
 						</div>
		   				<div class="bogumWaiting">
		   					<label style="font-size:20px;"><b>심사대기중 공고</b></label>
		   					<br><br>
		   					<label><b style="font-size:35px; color:#013252;">0</b></label>
		   				</div>
		   				<div class="bogumSubmit">
		   					<button class="bogumR" id="afterCertify"><br><br><label style="font-size:17px;"><b>보금자리<br>등록</b><br><br></label></button>
		   				</div>
		   				<!-- 기업인증 신청후 (승인) -->
		   				<% } else if(certStatus.equals("Y")) {
		   					
		   						for(Mypage rt : recruitTotalList) { %>
		   							<div class="bogumIng">
		 							<label style="font-size:20px;"><b>게재중 공고</b></label>
		 							<br><br>
		 							<label><b style="font-size:35px; color:#013252;"><%=rt.getStatusApproved() %></b></label>
		 							</div>
				   					<div class="bogumWaiting">
				   						<label style="font-size:20px;"><b>심사대기중 공고</b></label>
				   						<br><br>
				   						<label><b style="font-size:35px; color:#013252;"><%=rt.getStatusWaiting() %></b></label>
				   					</div>
				   					<div class="bogumSubmit">
				   						<button class="bogumR" id="boguemRegister"><br><br><label style="font-size:17px;"><b>보금자리<br>등록</b><br><br></label></button>
				   					</div>
		   				<% } %>		
		   				<!-- 기업인증 신청후 (반려) -->
		   				<% } else if(certStatus.equals("N")) { %>
		   				<div class="bogumIng">
 							<label style="font-size:20px;"><b>게재중 공고</b></label>
 							<br><br>
 							<label><b style="font-size:35px; color:#013252;">0</b></label>
 						</div>
		   				<div class="bogumWaiting">
		   					<label style="font-size:20px;"><b>심사대기중 공고</b></label>
		   					<br><br>
		   					<label><b style="font-size:35px; color:#013252;">0</b></label>
		   				</div>
		   				<div class="bogumSubmit">
		   					<button class="bogumR" id="unableToRegister"><br><br><label style="font-size:17px;"><b>보금자리<br>등록</b><br><br></label></button>
		   				</div>
		   				<% } %>
		   			</div>
		   		</div>
            </div>
            
            <!-- 최근 등록한 보금자리 내역 -->
            <div class="BogumInfo2">
            	<!-- 기업인증전 / 기업인증후 (심사중) / 기업인증후 (반려) -->
            	<% if(companyPhone == null || certStatus == null || certStatus.equals("N")) {%>
            	<ul>
		   			<li>
		   			<br><br>
		   				<a style="font-size:20px;"><b>&nbsp;&nbsp; 최근 등록한 보금자리</b></a>
		   			</li>
		  		</ul>
		  		<!-- 기업인증후 (승인) -->
            	<% } else if(certStatus.equals("Y")) {%>
            	<ul>
		   			<li>
		   			<br><br>
		   				<a style="font-size:20px;"><b>&nbsp;&nbsp; 최근 등록한 보금자리</b>
		   				<div class="more">
		   				<a id="moreBogumList" style="font-size:16px; color:black; cursor:pointer;"><b>더보기</b></a>
		   				<a id="moreBogumList" style="font-size:23px; color:#2FA599; cursor:pointer;"><b>></b></a>
		   				</div>
						</a>
		   			</li>
		  		</ul>
            	<% } %>
            </div>
            
            <div class="BogumInfo3">
            	<table class="jariInfo">
            		<thead>
            		<tr>
            			<th style="border-right:hidden;" width="100">번호</th>
            			<th style="border-right:hidden;" width="110">등록번호</th>
            			<th style="border-right:hidden;" width="350">보금자리 정보</th>
            			<th style="border-right:hidden;" width="120">상태</th>
            			<th width="110">조회수</th>
            		</tr>
            		</thead>
            		<tbody>
            		<!-- 기업인증전 / 기업인증후 (심사중) / 기업인증후 (반려)-->
            		<% if(companyPhone == null || certStatus == null || certStatus.equals("N")) {%>
            		<tr>
            			<td colspan="5">등록된 보금자리 내역이 없습니다.</td>
            		</tr>
            		<!-- 기업인증후 (승인) -->
            		<% } else if(certStatus.equals("Y")) { 
            				/* 등록한 공고가 없을 경우 */
            				if(waiting == 0 && approved == 0 && end == 0 && rejected == 0) {
            		%>
            					<tr>
                    				<td colspan="5">등록된 보금자리 내역이 없습니다.</td>
                    			</tr>
                    		<!-- 등록한 공고가 있을 경우 -->
            			<% } else {
            				
            					int num = recruitInfoList.size();
            					for(Mypage ri : recruitInfoList) {
            						
            						String status = ri.getStatus();
            						String pName = ri.getProductName();
            			%>
            					<tr height=100>
            						<td style="border-right:hidden;">
            							<label><%=num %></label>
            						</td>
            							
            						<td style="border-right:hidden;">
            							<label id="recruitNo" style="font-size:18px;"><%=ri.getRecruitNo() %></label><br>
            						</td>
            						<td id="tableContent" style="border-right:hidden;">
            							<label class="recruitTitle" id="tableContent" style="font-size:18px; cursor:pointer;"><b><%=ri.getRecruitTitle()%></b></label><br>
            							<label id="tableContent" style="font-size:12px;">게재기간 : <label><%=ri.getEnrollDate() %></label> ~ <label><%=ri.getExpirationDate() %></label></label><br>
            							<label id="tableContent" style="font-size:12px;">업종 : <label><%=ri.getRecruitMajor() %></label></label>
            						</td>
            						<td style="border-right:hidden;">
            								<% if(status.equals("심사대기")) { %>
            									<label id="simsaWaiting" style="font-size:12px;">대기</label>
            								<% } 
            								
            								if(status.equals("APPROVED")) { %>
            									<label id="simsaApproved" style="font-size:12px;">승인</label>
            								<% } 
            								
            								if(status.equals("REJECTED")) { %>
            									<label id="simsaRejected" style="font-size:12px;">반려</label>
            								<% } 
            								
            								if(status.equals("END")) { %>
            									<label id="simsaEnd" style="font-size:12px;">마감</label>
            								<% } %>
            						</td>
            						<td>
            							<label><%=ri.getClickCount() %></label>
            						</td>
            					</tr>
            				<% 
            					num--;
            					} %>
            			<% 
            			
            			} %>
					<% } %>
					</tbody>
            	</table>
            	<!-- 기업인증현황 -->
            	<!-- 기업인증전 -->
            	<% if(companyPhone == null) {%>
            		<ul>
		   			<li>
		   			<br><br><br><br><br>
		   				<a style="font-size:25px; margin-left:190px;"><b>기업인증 후 보금자리 등록이 가능합니다.</b></a>
		   				<br><br>
		   				<a style="font-size:18px; color:rgb(100, 100, 100); margin-left:200px;">'인증신청 바로가기'를 통해 기업인증 신청을 해주세요.</a>
		   			</li>
		  			</ul>
		  		<!-- 기업인증후 (심사중) -->
		  		<% } else if(certStatus == null) { %>
		  			<ul>
		   			<li>
		   			<br><br><br><br><br>
		   				<a style="font-size:25px; margin-left:190px;"><b>기업인증 승인 후 보금자리 등록이 가능합니다.</b></a>
		   				<br><br>
		   				<a style="font-size:18px; color:rgb(100, 100, 100); margin-left:250px;">인증 관련 문의는 고객센터로 문의 부탁드립니다.</a>
		   			</li>
		  			</ul>
		  		<!-- 기업인증후 (승인) -->
				<% } else if(certStatus.equals("Y")) {%>
            		<ul>
		   				<li>
		   				<br><br><br><br><br>
		   					<a id="saero" style="font-size:25px;"><b>새로운 보금자리를 등록해보세요.</b></a>
		   					<br><br>
		   					<a id="saero2" style="font-size:18px; color:rgb(100, 100, 100);">일자리를 빨리 등록할수록 좋은 직원 채용확률이 높아집니다.</a>
		   					<br><br>
		   					<button class="bogumSubmit2" id="boguemRegister2" style="font-size:15px;"><b>보금자리 등록하기</b></button>
		   				</li>
		  			</ul>
		  		<!-- 기업인증후 (반려) -->
				<% } else if(certStatus.equals("N")) {%>
				<ul>
		   			<li>
		   			<br><br><br><br><br>
		   				<a style="font-size:25px; margin-left:190px;"><b>기업인증 승인 후 보금자리 등록이 가능합니다.</b></a>
		   				<br><br>
		   				<a style="font-size:18px; color:rgb(100, 100, 100); margin-left:240px;">인증 관련 문의는 고객센터로 문의 부탁드립니다.</a>
		   			</li>
		  			</ul>
				<% } %>
            </div>
    </div>
</body>
</html>