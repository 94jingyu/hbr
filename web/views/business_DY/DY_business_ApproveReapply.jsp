<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,com.kh.hbr.member.model.vo.*"%>
<%
	ArrayList<Business> list = (ArrayList<Business>) request.getAttribute("list");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>기업인증신청</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	a {text-decoration: none; color: black;}
	input {width:410px; height:40px; font-size:15px; margin: 2px;}
	
	/* 화면 전체 영역 */
	#wrap {
		width: 1200px; 
		margin: 0 auto; 
		height:100px; 
		top:20%; 
		margin-top:50px;
	}
	
	/* 내용 입력 영역 */
	#whiteBox {
		width:720px; 
		height:1150px; 
		padding: 40px; 
		border: 1px solid #C4C4C4; 
		margin: 0 auto; 
		background: white;
	}
	
	/* 사업자 등록번호 text 박스 */
	.license {
		width: 128px;
	}
	
	/* 휴대폰번호 앞자리(010) 선택 */
	#tel, #fax {
		width:140px; 
		height:42px; 
		font-size:15px; 
		margin: 2px;
	}
	
	/* 주소 검색 버튼 */
	.btn {
		width:120px; 
		height:44px; 
		font-size:15px; 
		margin: 2px; 
		background: #666666; 
		color:#C2C2C2; 
		border: none;
	}
	
	/* 이메일 선택 */
	#email_Select {
		width:413px; 
		\height:42px; 
		font-size:15px; 
		margin: 2px;
	}
	
	textArea {color: #646464;}	
	
	/* 확인 버튼 */
	#agreeBtn {
		width:150px; 
		height:50px; 
		font-size:20px; 
		background: #013252; 
		color: white; 
		padding: 5px; 
		margin: 2px; 
		border-radius:10px; 
		border: 1px; 
		outline: 1px;
	} 
	
	#agreeBtn:hover {
		cursor:pointer;
	}
	
	/* 취소 버튼 */
	#closeBtn {
		width:150px; 
		height:50px; 
		font-size:20px; 
		background: rgb(192, 192, 192); 
		color: white; 
		/* padding: 5px; 
		margin: 2px;  */
		border-radius:10px; 
		border: 1px; 
		outline: 1px;
	} 
	
	#closeBtn:hover {
		cursor:pointer;
	}
	
	/* 밑줄(구분선) */
	hr {
		margin-top: 15px;
		margin-bottom: -10px;
	}
	
	/* td 간격 */
	td {
		padding:6px;
	}
	
	/* 기업 인증 내용 입력 form ID */
	#ApproveForm {
		margin-top: -30px;
	}
	
	/* 동의 버튼 */
	.checkBox {
		width: 17px; 
		height: 17px;
	}
	
	/* 확인, 취소 버튼 위치 가운데로 조정 */
	#buttons {
		text-align:center;
	}
	
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
</head>
<body>
<script>	
		//submit하기 전 기업인증신청 유효성 검사
		function confirm(){ 
		
		var ch = $('#checkBox').is(":checked");
		var ceoName = $('#ceoName').val();
		var tel1 = $("#tel").attr("selected", true).val();
		var tel2 = $('#tel2').val();
		var tel = tel1 + tel2;
		var address1 = $('#address').val();
		var address2 = $('#address_etc').val();
		var address = address1 + " " + address2;
		
		//값이 제대로 담기는지 확인
		/* console.log(address);  */
		
		//== null이 아닌 === ""를 써주어야함 (자바스크립트의 자료형은 var 하나이기때문에 자료형과 값 모두를 비교하려면 ===를 써야함)
		//===의 반대는 !==
		if(ceoName === "" || tel2 === "" || address1 === "") {
	
			alert("필수정보를 입력해주세요.");
			
		} else {
			
			if(address2 === "") {
				alert("상세주소를 입력해주세요.");
			} else if(ch != true) {
				alert("이용약관에 동의해주세요.");
			} else {
				$("#ApproveForm").submit();
			}
		}
	};
	
	//취소 버튼 클릭
	function cancel(){ 
    	location.href = "<%=request.getContextPath()%>/views/business_DY/DY_business_MyPage.jsp";
    };

	//주소 검색
	function openDaumZipAddress(){
		new daum.Postcode({
			oncomplete:function(data) {

				$("#address").val(data.address);
				$("#address_etc").focus();

				console.log(data);
			}
		}).open();	
	}
	
	//회사번호, 팩스번호 숫자만 입력하도록 제한
	//숫자가 아닌 정규식
    var replaceNotInt = /[^0-9]/gi;
    
    $(document).ready(function(){
        
        $("#tel2, #fax2").on("focusout", function() {
            var x = $(this).val();
            if (x.length > 0) {
                if (x.match(replaceNotInt)) {
                   x = x.replace(replaceNotInt, "");
                }
                $(this).val(x);
            }
        }).on("keyup", function() {
            $(this).val($(this).val().replace(replaceNotInt, ""));
        });
    });
    
    //대표자 이름에 한글만 입력하도록 제한
    //특수문자 정규식 변수(공백 미포함)
    var replaceChar = /[~!@\#$%^&*\()\-=+_'\;<>0-9\/.\`:\"\\,\[\]?|{}]/gi;
 
    //완성형 아닌 한글 정규식
    var replaceNotFullKorean = /[ㄱ-ㅎㅏ-ㅣ]/gi;
    
    $(document).ready(function(){
        
        $("#ceoName").on("focusout", function() {
            var x = $(this).val();
            if (x.length > 0) {
                if (x.match(replaceChar) || x.match(replaceNotFullKorean)) {
                    x = x.replace(replaceChar, "").replace(replaceNotFullKorean, "");
                }
                $(this).val(x);
            }
            }).on("keyup", function() {
                $(this).val($(this).val().replace(replaceChar, ""));
       });
    });
</script>
	<div id="wrap">
	
	<!-- 기업 인증 신청 내용 입력 -->
	<div class="ApproveArea">

		<h1 id="ApproveTitle" align="center">기업인증신청</h1>
		<br>
		<br>
		<div id=whiteBox> <!-- whiteBox -->
			<br>
			<br>
			<form id="ApproveForm" action="<%=request.getContextPath()%>/reapplyUpdateInfo.ap" method="post">
	 			<table>
	 				<tr>
	 					<td colspan="5">* 구인 이외의 목적으로 이용하는 것을 방지하기 위해 기업 인증을 시행하고 있습니다.</td>
	 				</tr>
	 				<tr>
	 					<td colspan="5">* 승인 기준은 <label style="color:red;">사업자 등록번호의 실재여부와 사업자 정보</label>로 확인합니다.</td>
	 				</tr>
	 				<tr>
	 					<td colspan="5">* 인증기간은 <b>최대 1년</b>으로 1년 경과시 기업인증을 다시 받으셔야 합니다.</td>
	 				</tr>
	 				<tr>
						<td colspan="5"><br></td>
					</tr>
	
					<tr>
						<td colspan="5"><h3>사업자정보 입력</h3><hr style="border: 1.5px solid #013252; background:#013252;"></td>
						
					</tr>
					<tr>
						<td colspan="5"></td>
					</tr>
					<% for (Business b : list){ %>
					<tr>
						<td style="height:50px;">사업자 등록번호</td>
						<td colspan="3" style="height:50px;">
							<label><b><%= b.getCompanyNo() %></b></label>
						</td>
					</tr>
					<tr>
						<td>기업구분</td>
						<td colspan="3">
							<label><b><%= b.getCompanyType() %></b></label>
						</td>
					</tr>
					<tr>
						<td style="height: 50px;">기업/상호명</td>
						<td colspan="3">
							<label><b><%= b.getCompanyName() %></b></label>
						</td>
					</tr>
					<% } %>
					<tr>
						<td>대표자명(필수)</td>
						<td colspan="4">
							<input id="ceoName" type="text" name="ceoName" placeholder=" 대표자명 입력">
						</td>
					</tr>
					<tr>
						<td>회사연락처(필수)</td>
						<td style="width: 100px;">
							<select id="tel" name="regionTel">
								<option selected>선택</option>
								<option>02</option>
								<option>031</option>
								<option>032</option>
								<option>033</option>
								<option>041</option>
								<option>042</option>
								<option>043</option>
								<option>044</option>
								<option>051</option>
								<option>052</option>
								<option>053</option>
								<option>054</option>
								<option>055</option>
								<option>061</option>
								<option>062</option>
								<option>063</option>
								<option>064</option>
							</select>
						<td colspan="2" >
							 <input type="tel" id="tel2" name="telephone" style="width:106px;" maxlength="4">&nbsp;&nbsp;-
                    		 <input type="tel" id="tel3" name="telephone2" style="width:106px; margin-left:10px;" maxlength="4">
						</td>
						
					</tr>
					<tr>
						<td>회사주소(필수)</td>
						<td colspan="3">
							<input type="text" id="address" name="address1">
						</td>
						<td>
							<button type="button" class="btn" onclick="openDaumZipAddress();">주소검색</button>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="3">
							<input type="text" id="address_etc" name="address2" value="">
						</td>
					</tr>
					<tr>
						<td>팩스번호(선택)</td>
						<td style="width: 100px;">
							<select id="fax" name="regionFax">
								<option value="no" selected="selected">선택</option>
								<option>02</option>
								<option>031</option>
								<option>032</option>
								<option>033</option>
								<option>041</option>
								<option>042</option>
								<option>043</option>
								<option>044</option>
								<option>051</option>
								<option>052</option>
								<option>053</option>
								<option>054</option>
								<option>055</option>
								<option>061</option>
								<option>062</option>
								<option>063</option>
								<option>064</option>
							</select>
						<td colspan="2" >
							 <input type="tel" id="fax2" name="fax" style="width:106px;" maxlength="4">&nbsp;&nbsp;-
                     		 <input type="tel" id="fax3" name="fax2" style="width:106px; margin-left:10px;" maxlength="4">
						</td>
				
					</tr>
					<tr>
						<td>홈페이지(선택)</td>
						<td colspan="4">
							<input type="text" name="homepage" placeholder=" http://">
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">
							<label style="color:navy;"><b>[개인정보 수집 및 이용안내]</b></label>
						
						</td>
					</tr>
					<tr>
						<td colspan="3">• 수집 및 이용목적 : 서비스 신청에 따른 문의사항 안내를 위해</td>
					</tr>
					<tr>
						<td colspan="3">• 수집하는 개인정보 항목 : 사업자등록번호, 대표자명, 회사주소, 팩스번호</td>
					</tr>
					<tr>
						<td colspan="3">• 보유 및 이용기간 : 서비스 신청 후 1년까지</td>
					</tr>
					<!-- <tr>
						<td colspan="5"><br></td>
					</tr> -->
					<tr>
					<td style="text-align:left;" colspan="3">
							<input type="checkbox" id="checkBox" class="checkBox">
							<label for="checkBox"><b>위의 개인정보 수집 및 이용에 동의합니다.</b></label>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="10px;"><br><br></td>
					</tr>
					<tr>
						<td colspan="5" id="buttons">
							<button id=agreeBtn type="button" onclick="confirm();"><b>확인</b></button>
							<button id=closeBtn type="button" onclick="cancel();"><b>취소</b></button>
						</td>
					</tr>
				</table>	
			</form>
		
		</div> <!-- whiteBox -->
		<br><br><br>
		<footer>
		<table>
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
			</table>
		</footer>
	</div><!-- /기업인증 신청 내용 입력-->
	</div>
</body>
</html>