<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="com.kh.hbr.member.model.vo.*"%>
<%
	Business bMember = (Business) session.getAttribute("loginUser");

%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>기업인증신청 완료</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	body {background-color: #F5F6F7;}
	a {text-decoration: none; color: black;}
	
	/* 화면 전체 영역 */
	#wrap {width: 1200px; margin: 0 auto; height:100px; top:20%; margin-top:50px;}
	
	/* 내용입력된 영역 */
	#whiteBox {width:700px; height:300px; padding: 40px; border: 1px solid #C4C4C4; margin: 0 auto; background: white;}
	
	/* 확인버튼 */
	#cofirmBtn {
		width:150px; 
		height:50px; 
		font-size:20px; 
		background: #013252; 
		color: white; 
		padding: 5px; 
		margin: 2px; 
		border-radius:10px; 
		border: 1px; 
		outline: 1px;
	}
	
	#cofirmBtn:hover{cursor:pointer;}
	
	/* 기업인증 완료 check 이미지 */
	#check {
		margin-bottom:-10px;
	}
	
	footer{margin:0 auto; text-align:center;}
	footer > span {padding: 20px;}
</style>
<script>
    function cofirmBtn(){ 
    	location.href = "<%=request.getContextPath()%>/companyMypage.bs";
    };
</script>
</head>
<body>
	<div id="wrap">
	
	<div class="SuccessArea">
		<!-- 테이블 화면 가운데 위치로 조정 -->
		<br><br><br>
		<table width="550" border="0" style="margin:0 auto;">
			<tr>
				<td>
					<img id="check" alt="check.png" src="/h/static/images/business/check.png" 
						width="45px" height="45px">
					<label style="font-size:30px;"><b>&nbsp; 기업인증 신청이 완료되었습니다.</b></label>
				</td>
			</tr>
		</table>
		<br><br>
		<div id=whiteBox> <!-- whiteBox -->
			<br><br>
			<img alt="home_check.png" src="/h/static/images/business/home_check.png" 
				width="100px" height="100px" style="margin-left: auto; margin-right: auto; display: block; margin-top:-10px;">
			<br>
			<br>
			<p align="center" style="font-size:16px;"><b>영업일 7일 이내 심사 후 마이페이지 인증 현황에서 확인 가능합니다.</b><p>
			<br>
			<br>
			<div align="center">
				<button id="cofirmBtn" onclick="cofirmBtn();"><b>확인</b></button>
			</div>		

		</div> <!-- whiteBox -->
		<br><br><br>
		<footer>
		<table >
			<span><a href="">홈</a></span>
			<span><a href="">이용약관</a></span>
			<span><a href="">개인정보처리방침</a></span>
			<span><a href="">ⓒHBR.Co. Lth</a></span>
		</footer>
	</div>
	</div>
</body>
</html>