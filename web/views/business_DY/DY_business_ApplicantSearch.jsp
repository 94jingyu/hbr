<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,com.kh.hbr.member.model.vo.*,com.kh.hbr.approve.model.vo.*"%>
    
<%
	ArrayList<Applicant> apList = (ArrayList<Applicant>) session.getAttribute("applicantList");

	PageInfo pi = (PageInfo) session.getAttribute("pi");

	int listCount = pi.getListCount();
	int currentPage = pi.getCurrentPage();
	int maxPage = pi.getMaxPage();
	int startPage = pi.getStartPage();
	int endPage = pi.getEndPage();
	int limit = pi.getLimit();

	/* System.out.println("인재검색 내역 : " + apList);
	System.out.println("page : " + pi); */
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>인재검색</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	#wrap {width: 1200px; margin: 0 auto;}
	
	/* 상단 로그인 영역 */
	.topAfterLogin {
		margin-top: 30px;
	}
	
	/* 헤더 영역 */
	header {width: 100%; height: 150px; }
	
	/* 상단메뉴바 */
	.topMenu {
    	display: inline-block;
    	width: 100%;
    	height: 50px;
    	background: #013252;
    	line-height: 30px;          /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	    vertical-align: middle;     /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	    text-align: center;         /* 글씨 정렬을 가운데로 설정 */  
	    margin-top: 18px;	/* *****로고랑 메뉴바 사이 여백 설정 ***** */
	    font-weight: bold;
  	}
  	
  	.topMenu>div { float: right; }
  	
  	.topMenu>ul {
   		float: left;
    	margin: 0;
    	padding: 0;
    	list-style: none;
  	}
  	.topMenu>ul>li {
    	display: inline-block;
    	padding: 0;
  	}
  	
  	.topMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: white;
  	}
  	
  	/* 상단메뉴바 마우스 클릭 시 효과 */
  	.topMenu a:hover { 
		color:#2FA599;
   		background: rgb(210, 210, 210);
  	}
  	
	/* ========== HOME > 인재검색 ========== */
	.homeNavi {
		margin-top: 45px;
		margin-left: 70px;
	}
	
	/* ========== 인재검색 - 위아래 선 ========== */
	.pageTitle {
		border-top: 3px solid rgb(192, 192, 192);
		border-bottom: 3px solid rgb(192, 192, 192);
		width:1050px;
		height:80px;
		margin-top: 30px;
		margin-bottom: 30px;
		margin-left: 70px;
	}
	
	/* ========== 인재검색 -> 제목 ========== */
	.pageTitle2 {
		padding: 23px;
	}

	/* =========== 이미지, 인재검색 안내 =========== */
	
	/* 인재검색 안내 table ID */
	#searchInfo {
		width:950px;
		height:100px;
		margin-top:50px;
		margin-bottom:50px;
	}
	
	/* 인재검색 이미지 */
	#searchImg {
		margin-left:150px;
	}
	
	/* =========== 검색조건 =========== */
   
   /* 검색조건 테이블 클래스명 */
	.searchTable {
		padding: 30px;
		width: 750px;
		background: #d2d2d2;
		margin-left: 215px;
	}
	
	.searchTable td {
		padding: 3px;
	}
	
	/* 검색 버튼 */
	#searchBtn {
		width: 90px;
		height: 90px;
		background: #013252;
		color: white;
		padding: 5px;
		border-radius: 1px;
		border: 1px;
		outline: 1px;
	}
	
	/* select */
	.searchSelect {
		width: 120px;
		height: 40px;
		font-size: 20px;
	}
	
	/* =========== 지원자 정보 (테이블) =========== */
	
	/* 테이블 div 클래스명 */
	.applicantTable {
		width:910px;
		height:1000px;
		margin-left:140px;
		margin-top: 60px;
	}
	
	/* 테이블 클래스명 */
	 .applicantInfo {
    	 width: 900px;
         height: 170px;
      	 text-align: center;
         border-collapse: collapse;
   	}

  	 .applicantInfo th {
         background: rgb(210, 210, 210);
         height: 40px;
         border: 1.5px solid rgb(180, 180, 180);
    }   
   
     .applicantInfo td {
         border: 1.5px solid rgb(180, 180, 180);
         height: 110px;
    }
    
    /* 총 10명 div 클래스명*/
	.total {
		width:100px;
		height:30px;
		float: left;
	}
	
    /* 테이블 제목 - 총 10명 */
    #tableTitle {
    	float:left;
    	margin-bottom: 10px;
    }
    
    /* 지원자이름 */	
    #applicantName {
    	text-align:center;
    }

	  /* 테이블 - 이력서 요약 */
    #tableContent {
    	text-align:left;
    	margin-left:25px;
    }
   
	/* 스크랩버튼 */
	.tableButton {
		float: right;
		margin-right: 10px;
		margin-top: 25px;
		background: #013252;
		border: 0px;
		color: white;
		height: 40px;
		width: 100px;
		font-size: 16px;
	}
	
	/* 페이징 관련 */
	#paging {
		margin-top: 35px;
		margin-right:20px;
	}
	
	#paging > button {
		border-style: none;
		background: none;
		font-size: 18px;
		/* font-weight: bold; */
		cursor: pointer;
	}
	
	.former {
		margin-right: 10px;
	}
	
	.next {
		margin-left: 10px;
	}

</style>
</head>
<body>
<script>
	//총 몇건인지 카운트
	$(document).ready(function() {
		var listNo = <%=apList.size()%>;

		var a = $(".total").children().children().children();
			
		a.text(listNo);
	});
	
	//이력서 상세보기
	$(function(){
      var a = $("tbody").children().each(function(index){
         
         if(index != 0) {
            $(this).children().eq(3).click(function(){
               var rNum = $(this).children().eq(0).text();
               
               console.log(rNum);
  
               location.href="<%=request.getContextPath()%>/applicantDetail.bs?rNum=" + rNum;
            });
         }
      });
   });
	
	//스크랩하기 (체크박스)
	$(function() {
		$("#bookmark").click(function() {
			var deleteApply = $("input[name=certCheck]:checked").length;
			var deleteApplyNo = "";
			
			if(deleteApply <= 0) {
				alert("스크랩을 원하시는 인재를 선택해주세요.");
			} else {
				
				var result = confirm("선택하신 인재를 스크랩하시겠습니까?");
				
				if(result) {
										
					
					$("input[name=certCheck]:checked").each(function(){
						deleteApplyNo = $(this).parent();
						console.log("deleteApplyNo : " + deleteApplyNo);

					});
					
						/* alert("스크랩이 완료되었습니다."); */
						
						
						<%-- location.href = "<%= request.getContextPath()%>/update.ap?deleteApplyNo=" + encodeURIComponent(deleteApplyNo); --%>
				}

			}
		});
	});
	
</script>
	<div id="wrap">
	<%@ include file="../guide/business_menubar.jsp" %>
        
        <!-- home > 인재검색 -->
        <div class="homeNavi" >
        	<label style="font-size:15px;">HOME > 인재검색</label>
        </div>
        
        <!-- 인재검색 -->
        <div class="pageTitle">
        	<div class="pageTitle2">
        		<label style="font-size:25px;"><b>인재검색</b></label>
        	</div>
        </div>
        
		<!-- 인재검색 안내 -->
        <table id="searchInfo">
				<tr>
					<td colspan="2" width="150px" height="60px">
					<img src="/h/static/images/business/recruitment_2.png" width="85px" height="85px" id="searchImg"></td>
					<td>
						<p style="font-size:18px; padding:5px; margin-left:15px;">
							• 이력서 상세 열람을 위해서는 <b>기업인증</b>과 <b>이력서 열람 상품</b>이 필요합니다.</p>
						<p style="font-size:18px; padding:5px; margin-left:15px;"> 
							• 스크랩 내역은 이력서관리 > 스크랩 인재 정보에서 확인 가능합니다.
						</p>
					</td>
				</tr>
		</table>
				
					<!-- 검색조건 -->
					<form id="searchKeyForm" action="<%= request.getContextPath()%>/searchApplicant.bs" method="get">
					<table class="searchTable">
						<tr>
							<td rowspan="3"
								style="width: 180px; height: 80px; text-align: center"><b
								style="font-size: 18px">검색조건</b></td>
							<td style="width: 100px;">근무지역</td>
							<td style="width: 180px;">
							<select class="searchSelect" name="searchRegion" style="width: 180px; height: 40px; font-size: 15px;">
									<option value="all">전국</option>
									<option value="서울">서울</option>
									<option value="경기">경기</option>
									<option value="인천">인천</option>
									<option value="부산">부산</option>
									<option value="대구">대구</option>
									<option value="광주">광주</option>
									<option value="대전">대전</option>
									<option value="울산">울산</option>
									<option value="세종">세종</option>
									<option value="강원">강원</option>
									<option value="경남">경남</option>
									<option value="경북">경북</option>
									<option value="전남">전남</option>
									<option value="전북">전북</option>
									<option value="충남">충남</option>
									<option value="충북">충북</option>
									<option value="제주">제주</option>
							</select></td>
							<td rowspan="3" style="width: 200px; height: 80px; text-align: center">
								<button id="searchBtn" style="font-size:18px;" type="submit">검색</button>
							</td>
						</tr>
						<tr>
							<td style="width: 100px;">관심직종</td>
							<td style="width: 180px;">
							<select name="searchJob" class="searchSelect" style="width: 180px; height: 40px; font-size: 15px;">
									<option value="all">전체</option>
									<option value="요리/서빙">요리/서빙</option>
									<option value="간호/의료">간호/의료</option>
									<option value="생산/건설">생산/건설</option>
									<option value="사무/경리">사무/경리</option>
									<option value="운전/배달">운전/배달</option>
									<option value="상담/영업">상담/영업</option>
									<option value="매장관리">매장관리</option>
									<option value="교사/강사">교사/강사</option>
									<option value="일반/기타">일반/기타</option>		
							</select></td>
						</tr>
					</table>
					</form>
        
            <!-- 테이블 -->
            <div class="applicantTable">
            	<table class="applicantInfo">
            		<caption>
            			<div class="total">
            				<label id="tableTitle" style="font-size:18px;"><b>총 <label id="count" style="color:orange;"></label>건</b></label>
            			</div>
            		</caption>
                    <tr>
            			<th width="80" style="border-right:hidden;">번호</th>
            			<th style="border-right:hidden;" width="70"></th>
            			<th width="120" style="border-right:hidden;">이름</th>
            			<th width="300" style="border-right:hidden;">이력서 요약</th>
            			<th width="150">등록일</th>
            		</tr>
            		<tbody>
            		<% 
            			int index = 0;
            			for(Applicant ac : apList) { 
            			
            				int num = (listCount - index) - ((currentPage - 1) * limit);
            				String name = ac.getMemberName();
            				String name2 = name.substring(0, 1);   
            		%>
            		<tr>
            			<!-- <td style="border-right:hidden;">
            			<input type="checkbox" name="certCheck">
            			</td> -->
            			<td style="border-right:hidden;">
            				<label style="font-size:18px;"><%=num%></label>
            			</td>
            			<td style="border-right:hidden; cursor:pointer;">
            				<img alt="star.png" src="/h/static/images/common/star_em.png" width="25px" height="25px">
            			</td>
            			<td id="bogumTitle" style="border-right:hidden;">
            				<label style="font-size:18px;"><b><%=name2 %>**</b></label><br>
            			</td>
            			<td id="tableContent" style="border-right:hidden; text-align:">
            				<label style="display:none;"><%=ac.getResumeNo() %></label>
            				<label id="tableContent" style="font-size:18px; cursor:pointer; text-decoration: underline;"><b><%=ac.getResumeTitle() %></b></label><br>
            				<label id="tableContent" style="font-size:12px;">관심직종 : <%=ac.getApplyField() %></label><br>
            				<label id="tableContent" style="font-size:12px;">관심지역 : <%=ac.getInterestArea() %></label><br>
            			</td>
            			<td>
            				<label id="bogumState" style="font-size:18px;"><%=ac.getEnrollDate() %></label>
            			</td>
            		</tr>
            		<% 
            				index++;
            			} %>
            		</tbody>
            	</table>
            	<br>
            	<!-- 페이지 처리 -->
           		<div id="paging" align="center">
							<!-- 첫 페이지 버튼설정 -->
							<button onclick="location.href='<%=request.getContextPath()%>/selectApprove.ap?currentPage=1'"><<</button>
						
							<!-- 이전 페이지 버튼설정 -->
							<% if(currentPage <= 1) {%>
								<button disabled class="former"><</button>
							<% } else { %>
								<button class="former" onclick="location.href='<%=request.getContextPath()%>/selectApprove.ap?currentPage=<%=currentPage - 1%>'"><</button>								
							<% } %>
							
							<!-- 숫자 페이지 버튼 설정 -->
							<% 
								for(int i = startPage; i <= endPage; i++) { 
									if(i == currentPage) {
							%>			<!-- 첫 화면 (맨 첫페이지) -->
										<button disabled style="color:orange; cursor:default;"><%= i %></button>
							<%		} else {%>
										<!-- 다음 화면 -->
										<button onclick="location.href='<%=request.getContextPath()%>/selectApprove.ap?currentPage=<%=i%>'"><%=i%></button>
							<% 
									}	
								} 
							%>
							
							<!-- 다음페이지 버튼 설정 -->
							<% if(currentPage >= maxPage) { %>
								<button disabled class="next">></button>
							<% } else { %>
								<button class="next" onclick="location.href='<%=request.getContextPath()%>/selectApprove.ap?currentPage=<%=currentPage + 1%>'">></button>
							<% } %>
							
							<button onclick="location.href='<%=request.getContextPath()%>/selectApprove.ap?currentPage=<%=maxPage%>'">>></button>
						</div>
            	<!-- <button class="tableButton tableButton1" id="bookmark"><b>스크랩</b></button> -->
            </div>
         
        <!-- footer 영역 --> 
		<!-- <footer>Footer</footer> -->
    </div>
</body>
</html>