<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "com.kh.hbr.member.model.vo.*" %>
<%@ page import = "com.kh.hbr.approve.model.vo.Approve" %>
<%@ page import = "java.util.*" %>
<%	
		ArrayList searchList = (ArrayList) request.getAttribute("searchList");
		
		System.out.println("검색후 jsp searchList : " + searchList);

		/* ArrayList<Business> b = (ArrayList<Business>) list.get(0); */
		/* Business b = (Business) list.get(0);
		Approve a = (Approve) list.get(1); */
	
		/* System.out.println(a);*/
		/* System.out.println("관리자 기업인증내역 테이블 list : " + list); */
	
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>기업승인</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	#wrap {width: 1200px; height: 100%; margin: 0 auto;}
	
	/* ========== HOME > 회원관리 > 기업인증내역 ========== */
	.homeNavi {
		margin-top: 50px;
		float: right;
	}
	
	/* ========== 기업인증내역 - 위아래 선 ========== */
	.pageTitle {
		border-top: 3px solid rgb(200, 200, 200);
		border-bottom: 3px solid rgb(200, 200, 200);
		width:900px;
		height:1500px;
		float:right;
		margin-top: 30px;
		margin-bottom: 90px;
	}
	
	/* ========== 기업인증내역 -> 제목 ========== */
	.pageTitle2 {
		padding: 23px;
	}
	
	/* ============= 테이블 영역 ============= */
	
	/* 기업인증내역 테이블 div class */
	.Request {
		width:850px;
		margin-left:45px;
	}
	
	/* 기업인증내역 테이블 클래스명 */
	 .table {
    	 width: 840px;
         height: 170px;
      	 text-align: center;
         border-collapse: collapse;
   	}
   	
   	 .table th {
         background: rgb(210, 210, 210);
         height: 40px;
         border: 1.5px solid rgb(180, 180, 180);
    }   
    
    .table td {
    	height: 110px;
    	border: 1.5px solid rgb(180, 180, 180);
    }
    
    /* 테이블 제목 div class - 총 3건 */
	.total {
		width:100px;
		height:30px;
		float: left;
	}
	
	/* 테이블 제목 label ID - 총 3건 */
    #tableTitle {
    	float:left;
    	margin-bottom: 10px;
    }
	
	/* select - 전체, 대기중, 승인, 반려 */
	.selectbox {
		width:92px;
		height:30px;
		float: right;
		margin-bottom:12px;
	}

    /* 검색버튼 div 클래스명 */
	.searchB {
		width:70px;
		height:40px;
		float: right;
	}
    
     /* 검색버튼 */
    .searchButton {
		background: #013252;
		border: 0px;
		color: white;
		height: 35px;
		width: 70px;
		text-size: 15px;
	}
	
	/* 반려사유 ... 처리 */
	#rejectReason {
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
		width: 60px;
		height: 20px;
		display: inline-block;
	}
	
	/* Modal background */
	.searchModal {
		display: none; /* Hidden by default */
		position: fixed; /* Stay in place */
		z-index: 10; /* Sit on top */
		left: 0;
		top: 0;
		width: 100%; /* Full width */
		height: 100%; /* Full height */
		overflow: auto; /* Enable scroll if needed */
		background-color: rgb(0,0,0); /* Fallback color */
		background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	 
	/* Modal content */
	.search-modal-content {
		background-color: #fefefe;
		margin: 15% auto; /* 15% from the top and centered */
		padding: 20px;
		border: 1px solid #888;
		width: 30%; /* Could be more or less, depending on screen size */
		height: 380px;
	}
	
	/* 반려사유 Modal 닫기버튼 */
	#closeBtn {
		width:150px; 
		height:50px; 
		font-size:20px; 
		background: rgb(192, 192, 192); 
		color: white; 
		border-radius:10px; 
		border: 1px; 
		outline: 1px;
		margin-top: 13px;
	} 
	
	/* =========== 검색조건 =========== */
	 
	/* 검색조건 테이블 ID */
	#searchBox {
    	margin-right:170px;
    	margin-top:25px;
    }
   	
   	/* 검색 버튼 */
   	#searchBtn {
		background: #013252;
		border: 0px;
		color: white;
		height: 40px;
		width: 70px;
		font-size: 15px;
	}
	
	/* 페이징 관련 */
	#paging {
		margin-top: 35px;
	}
	
	#paging > button {
		border-style: none;
		background: none;
		font-size: 18px;
		/* font-weight: bold; */
		cursor: pointer;
	}
	
	.former {
		margin-right: 10px;
	}
	
	.next {
		margin-left: 10px;
	}
	
</style>
</head>
<body>
<script>
	//총 몇건인지 PageInfo의 listCount를 가져와서 태그 안에 넣어줌
	$(document).ready(function() {
		/* var count = $('#approveList tbody tr').length; */

				var listNo = "<%=searchList.size() / 2%>";
				
				$('#count').html(listNo);

	});
	
	//기업인증내역 상세보기
	$(function(){
      var a = $("tbody").children().each(function(index){
         
         if(index != 0) {
            $(this).children().eq(1).click(function(){
               var id = $(this).text();
               
               /* console.log(id); */
  
               location.href="<%=request.getContextPath()%>/approveDetail.ap?bMemberId=" + id;
            });
         }
      });
   });
	
	//반려사유 상세보기	
	$(function() {
		var a = $("tbody").children().each(function(index) {
			
			if(index != 0) {
				$(this).children().eq(7).click(function() {
					//for문 안에 있는 반려사유 태그의 내용을 reason에 담아줌
					var reason = $(this).children().text();
					
					//상세보기 textarea에 반려사유를 넣어줌
					$("#reason").text(reason);
					$("#modal").show();
				});
			}
		});
	});
	
	//Modal 닫기 버튼 클릭
	function closeModal() {
		$('.searchModal').hide();
	}

	//select 클릭시 
	<%-- $(function() {
		$("#select").change(function(){
			var state = $(this).val();

			console.log(state);

			$("#searchResultForm").submit();
			$("#searchForm").submit();

			if(state == "findTotal") {
				$("#searchResultForm").action="<%=request.getContextPath()%>/searchCertifySort.ap?state="+state;
				$("#searchResultForm").submit();
			} else if(state == "findWaiting") {
				$("#searchResultForm").action="<%=request.getContextPath()%>/searchCertifySort.ap?state="+state;
				$("#searchResultForm").submit();
			} else if(state == "findApproved") {
				$("#searchResultForm").action="<%=request.getContextPath()%>/searchCertifySort.ap?state="+state;
				$("#searchResultForm").submit();
			} else if(state == "findRejected") {
				$("#searchResultForm").action="<%=request.getContextPath()%>/searchCertifySort.ap?state="+state;
				$("#searchResultForm").submit();
			}
		});
	}); --%>
</script>

	<div id="wrap">
		<%@ include file="../guide/admin_menubar.jsp" %>
	    
            <!-- HOME > 회원관리 > 기업인증내역 -->
        	<div class="homeNavi" >
       		 	<label style="font-size:15px;">HOME > 회원관리 > 기업인증내역</label>
    	    </div>
        
    	    <!-- 기업인증내역 -->
      	  <div class="pageTitle">
      		  	<div class="pageTitle2">
        			<label style="font-size:25px;"><b>기업인증내역</b></label>
        		</div>
        		<br>
        		<!-- 검색 조건 -->
			 <div align="right">
			 	<form id="searchKeyForm" action="<%= request.getContextPath()%>/searchCertify.ap" method="get">
				<table id="searchBox" style="border:1px solid rgb(180, 180, 180); background:rgb(210, 210, 210); height:120px; width:600px;">
					<tr>
						<td colspan="2" rowspan="4" width="150px" style="text-align:center">
							<p style="font-size:18px; font-weight: bold;">검색조건</p>
						</td>
					</tr> 
					<tr>
						<td colspan="3" rowspan="4" width="350px">
							<input type="text" id="searchText" name="searchValue" placeholder="검색할 아이디 입력" style="width: 250px; height: 38px; font-size: 15px;">
							<button id="searchBtn" type="submit">검색</button>
							<br>
							<label>&nbsp;</label>
						</td>
					</tr>
				</table><br><br><br>
				</form>
				</div>
				 <!-- 테이블 -->
          	    <div class="Request">
            	<table class="table" id="approveList">
            		<caption>
            			<div class="total">
            				<label id="tableTitle" style="font-size:18px;"><b>총 <label id="count" style="color:orange;"></label>건</b></label>
            			</div>
            			<form id="searchForm" action="<%=request.getContextPath()%>/searchCertifySort.ap" method="get">
		   				<div class="selectbox">
		   						<select id="select" name="searchCondition" style="font-size:15px; height:30px; width:92px;">
		   							<option value="findTotal">전체</option>
		   							<option value="findWaiting">대기중</option>
		   							<option value="findApproved">승인</option>
		   							<option value="findRejected">반려</option>
		   							<option value="findReapply">재신청</option>
		   						</select>
		   				</div>
		   				</form>
            		</caption>
            		<thead>
            		<tr>
            			<th width="55" style="border-right:hidden;">번호</th>
            			<th width="90" style="border-right:hidden;">아이디</th>
            			<th width="120" style="border-right:hidden;">사업자등록번호</th>
            			<th width="90" style="border-right:hidden;">기업명</th>
            			<th width="90" style="border-right:hidden;">신청일</th>
            			<th width="80" style="border-right:hidden;">인증일</th>
            			<th width="80" style="border-right:hidden;">인증상태</th>
            			<th width="90">반려사유</th>
            		</tr>
            		</thead>
            		<form name="searchResultForm" id="searchResultForm" action="<%=request.getContextPath()%>/searchCertifySort.ap" method="get">
            		<tbody>
            		<%    
            			if(searchList.size() >= 1) {
            				
            			int rNum = searchList.size() / 2;
                   		for(int i = 0; i < searchList.size(); i = i + 2) {
            				Business bs = (Business) searchList.get(i);
            				Approve ap = (Approve) searchList.get(i+1);
            				
            				Date approveDate = ap.getApproveDate();
            				String rejectReason = ap.getRejectReason();
            		%>
            		<tr>
            			<td style="border-right:hidden;">
            				<label><%=rNum%></label>
            				<input type="hidden" name="listInfo" value="<%=rNum%>">
            			</td>
            			<td style="border-right:hidden;">
            				<label style="cursor:pointer;"><u><%=bs.getBmemberId() %></u></label>
            				<input type="hidden" name="listInfo" value="<%=bs.getBmemberId()%>">
            			</td>
            			<td style="border-right:hidden;">
            				<label><%=bs.getCompanyNo() %></label>
            				<input type="hidden" name="listInfo" value="<%=bs.getCompanyNo()%>">
            			</td>
            			<td style="border-right:hidden;">
            				<label><%=bs.getCompanyName() %></label>
            				<input type="hidden" name="listInfo" value="<%=bs.getCompanyName()%>">
            			</td>
            			<td style="border-right:hidden;">
            				<label><%=ap.getApplyDate() %></label>
            				<input type="hidden" name="listInfo" value="<%=ap.getApplyDate()%>">
            			</td>
            			<%if(approveDate == null) { %>
            				<td style="border-right:hidden;">
            				<label>-</label>
            				<input type="hidden" name="listInfo" value="-">
    						</td>
            			<% } else { %>
            				<td style="border-right:hidden;">
            				<label><%=approveDate%></label>
            				<input type="hidden" name="listInfo" value="<%=approveDate%>">
    						</td>
            			<% } %>
            			<td style="border-right:hidden;">
            				<label><%=ap.getApproveStatus() %></label>
            				<input type="hidden" name="listInfo" value="<%=ap.getApproveStatus()%>">
            			</td>
            			<%if(rejectReason == null) { %>
            				<td>
            				<label>-</label>
            				<input type="hidden" name="listInfo" value="-">
    						</td>
            			<% } else { %>
            				<td>
            				<label id="rejectReason" style="cursor:pointer;"><u><%=rejectReason%></u></label>
            				<input type="hidden" name="listInfo" value="<%=rejectReason%>">
    						</td>
            			<% } %>
            			<%		rNum--;
                   			}
                   		%>
            			<!-- 검색 결과가 없을 경우 -->
            			<% } else { %>
            				<td colspan="8"><label>검색 결과가 없습니다.</label></td>
            		</tr>
            	 	<% } %>
            		</tbody>
            		</form>
            	</table>
            	<!-- 페이지 처리 -->
           		<div id="paging" align="center">
							<!-- 첫 페이지 버튼설정 -->
							<button><<</button>
							<button><</button>
							
							<button style="color:orange; cursor:default;">&nbsp;&nbsp;1</button>
	
							<button disabled class="next">></button>			
							<button>>></button>
				</div>
            	<br><br><br><br><br><br><br><br><br><br><br>
           		</div>
       		 </div> 
     	  </div>
     	
    <!-- Modal content -->
		<div id="modal" class="searchModal">
			<div class="search-modal-content">
				<table id="table">
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<label style="font-size:20px;"><b>반려사유</b></label>
								<hr style="border: 1.5px solid #013252; background: #013252; margin-bottom: 20px; margin-top:18px;" width="550px;">
							</td>
						</tr>
						<tr>
							<td style="text-align:center;">
							<textarea id="reason" style="width:520px; height:150px; font-size:18px; margin-top:10px;" maxlength="150" spellcheck="false" readonly></textarea>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td style="text-align:center;" id="buttons">
								<button id=closeBtn type="button" onclick="closeModal();"><b>닫기</b></button>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
				</table>
			</div>
		</div>
</body>
</html>