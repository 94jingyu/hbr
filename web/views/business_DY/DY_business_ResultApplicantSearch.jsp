<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*,com.kh.hbr.member.model.vo.*,com.kh.hbr.approve.model.vo.*"%>
<%
	ArrayList<Applicant> apList = (ArrayList<Applicant>) session.getAttribute("searchList");

	System.out.println("인재검색 검색결과 : " + apList);

%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>인재검색</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<style>
	* {margin: 0; padding: 0;}
	#wrap {width: 1200px; margin: 0 auto;}
	
	/* 상단 로그인 영역 */
	.topAfterLogin {
		margin-top: 30px;
	}
	
	/* 헤더 영역 */
	header {width: 100%; height: 150px; }
	
	/* 상단메뉴바 */
	.topMenu {
    	display: inline-block;
    	width: 100%;
    	height: 50px;
    	background: #013252;
    	line-height: 30px;          /* 글씨가 가운데로 오도록 설정하기 위해 한줄의 높이를 30px로 설정 */
	    vertical-align: middle;     /* 세로 정렬을 가운데로 설정(위의 line-height와 같이 설정 필요함) */
	    text-align: center;         /* 글씨 정렬을 가운데로 설정 */  
	    margin-top: 18px;	/* *****로고랑 메뉴바 사이 여백 설정 ***** */
	    font-weight: bold;
  	}
  	
  	.topMenu>div { float: right; }
  	
  	.topMenu>ul {
   		float: left;
    	margin: 0;
    	padding: 0;
    	list-style: none;
  	}
  	.topMenu>ul>li {
    	display: inline-block;
    	padding: 0;
  	}
  	
  	.topMenu a {
   		display: block;
    	text-decoration: none;
    	padding: 10px 20px;
    	color: white;
  	}
  	
  	/* 상단메뉴바 마우스 클릭 시 효과 */
  	.topMenu a:hover { 
		color:#2FA599;
   		background: rgb(210, 210, 210);
  	}
  	
	/* ========== HOME > 인재검색 ========== */
	.homeNavi {
		margin-top: 45px;
		margin-left: 70px;
	}
	
	/* ========== 인재검색 - 위아래 선 ========== */
	.pageTitle {
		border-top: 3px solid rgb(192, 192, 192);
		border-bottom: 3px solid rgb(192, 192, 192);
		width:1050px;
		height:80px;
		margin-top: 30px;
		margin-bottom: 30px;
		margin-left: 70px;
	}
	
	/* ========== 인재검색 -> 제목 ========== */
	.pageTitle2 {
		padding: 23px;
	}

	/* =========== 이미지, 인재검색 안내 =========== */
	
	/* 인재검색 안내 table ID */
	#searchInfo {
		width:950px;
		height:100px;
		margin-top:50px;
		margin-bottom:50px;
	}
	
	/* 인재검색 이미지 */
	#searchImg {
		margin-left:150px;
	}
	
	/* =========== 검색조건 =========== */
   
   /* 검색조건 테이블 클래스명 */
	.searchTable {
		padding: 30px;
		width: 750px;
		background: #d2d2d2;
		margin-left: 215px;
	}
	
	.searchTable td {
		padding: 3px;
	}
	
	/* 검색 버튼 */
	#searchBtn {
		width: 90px;
		height: 90px;
		background: #013252;
		color: white;
		padding: 5px;
		border-radius: 1px;
		border: 1px;
		outline: 1px;
	}
	
	/* select */
	.searchSelect {
		width: 120px;
		height: 40px;
		font-size: 20px;
	}
	
	/* =========== 지원자 정보 (테이블) =========== */
	
	/* 테이블 div 클래스명 */
	.applicantTable {
		width:910px;
		height:1000px;
		margin-left:140px;
		margin-top: 60px;
	}
	
	/* 테이블 클래스명 */
	 .applicantInfo {
    	 width: 900px;
         height: 170px;
      	 text-align: center;
         border-collapse: collapse;
   	}

  	 .applicantInfo th {
         background: rgb(210, 210, 210);
         height: 40px;
         border: 1.5px solid rgb(180, 180, 180);
    }   
   
     .applicantInfo td {
         border: 1.5px solid rgb(180, 180, 180);
         height: 110px;
    }
    
    /* 총 10명 div 클래스명*/
	.total {
		width:100px;
		height:30px;
		float: left;
	}
	
    /* 테이블 제목 - 총 10명 */
    #tableTitle {
    	float:left;
    	margin-bottom: 10px;
    }
    
    /* 지원자이름 */	
    #applicantName {
    	text-align:center;
    }

	  /* 테이블 - 이력서 요약 */
    #tableContent {
    	text-align:left;
    	margin-left:25px;
    }
   
	/* 스크랩버튼 */
	.tableButton {
		float: right;
		margin-right: 10px;
		margin-top: 25px;
		background: #013252;
		border: 0px;
		color: white;
		height: 35px;
		width: 100px;
		text-size: 15px;
	}
	
	/* 페이징 관련 */
	#paging {
		margin-top: 35px;
		margin-right:20px;
	}
	
	#paging > button {
		border-style: none;
		background: none;
		font-size: 18px;
		/* font-weight: bold; */
		cursor: pointer;
	}
	
	.former {
		margin-right: 10px;
	}
	
	.next {
		margin-left: 10px;
	}

</style>
</head>
<body>
<script>
	$(document).ready(function() {
		var listNo = <%=apList.size()%>;

		var a = $(".total").children().children().children();
			
		a.text(listNo);
	});
</script>
	<div id="wrap">
	<%@ include file="../guide/business_menubar.jsp" %>
        
        <!-- home > 인재검색 -->
        <div class="homeNavi" >
        	<label style="font-size:15px;">HOME > 인재검색</label>
        </div>
        
        <!-- 인재검색 -->
        <div class="pageTitle">
        	<div class="pageTitle2">
        		<label style="font-size:25px;"><b>인재검색</b></label>
        	</div>
        </div>
        
		<!-- 인재검색 안내 -->
        <table id="searchInfo">
				<tr>
					<td colspan="2" width="150px" height="60px">
					<img src="/h/static/images/business/recruitment_2.png" width="85px" height="85px" id="searchImg"></td>
					<td>
						<p style="font-size:18px; padding:5px; margin-left:15px;">
							• 이력서 상세 열람을 위해서는 <b>기업인증</b>과 <b>이력서 열람 상품</b>이 필요합니다.</p>
						<p style="font-size:18px; padding:5px; margin-left:15px;"> 
							• 스크랩 내역은 이력서관리 > 스크랩 인재 정보에서 확인 가능합니다.
						</p>
					</td>
				</tr>
		</table>
				
					<!-- 검색조건 -->
					<form id="searchKeyForm" action="<%= request.getContextPath()%>/searchApplicant.bs" method="get">
					<table class="searchTable">
						<tr>
							<td rowspan="3"
								style="width: 180px; height: 80px; text-align: center"><b
								style="font-size: 18px">검색조건</b></td>
							<td style="width: 100px;">근무지역</td>
							<td style="width: 180px;">
							<select class="searchSelect" name="searchRegion" style="width: 180px; height: 40px; font-size: 15px;">
									<option value="all">전국</option>
									<option value="서울">서울</option>
									<option value="경기">경기</option>
									<option value="인천">인천</option>
									<option value="부산">부산</option>
									<option value="대구">대구</option>
									<option value="광주">광주</option>
									<option value="대전">대전</option>
									<option value="울산">울산</option>
									<option value="세종">세종</option>
									<option value="강원">강원</option>
									<option value="경남">경남</option>
									<option value="경북">경북</option>
									<option value="전남">전남</option>
									<option value="전북">전북</option>
									<option value="충남">충남</option>
									<option value="충북">충북</option>
									<option value="제주">제주</option>
							</select></td>
							<td rowspan="3" style="width: 200px; height: 80px; text-align: center">
								<button id="searchBtn" style="font-size:18px;" type="submit">검색</button>
							</td>
						</tr>
						<tr>
							<td style="width: 100px;">관심직종</td>
							<td style="width: 180px;">
							<select name="searchJob" class="searchSelect" style="width: 180px; height: 40px; font-size: 15px;">
									<option value="all">전체</option>
									<option value="요리/서빙">요리/서빙</option>
									<option value="간호/의료">간호/의료</option>
									<option value="생산/건설">생산/건설</option>
									<option value="사무/경리">사무/경리</option>
									<option value="운전/배달">운전/배달</option>
									<option value="상담/영업">상담/영업</option>
									<option value="매장관리">매장관리</option>
									<option value="교사/강사">교사/강사</option>
									<option value="일반/기타">일반/기타</option>		
							</select></td>
						</tr>
					</table>
					</form>
        
            <!-- 테이블 -->
            <div class="applicantTable">
            	<table class="applicantInfo">
            		<caption>
            			<div class="total">
            				<label id="tableTitle" style="font-size:18px;"><b>총 <label id="count" style="color:orange;">dfdf</label>건</b></label>
            			</div>
            		</caption>
            		<tr>
            			<th width="100" style="border-right:hidden;">선택</th>
            			<th width="120" style="border-right:hidden;">번호</th>
            			<th width="120" style="border-right:hidden;">이름</th>
            			<th width="300" style="border-right:hidden;">이력서 요약</th>
            			<th width="150">등록일</th>
            		</tr>
            		<tbody>
            			<% 
            			if(apList.size() >= 1) {
            		
            			int num = apList.size();
            			for(Applicant ac : apList) { 
 
            			%>
            		<tr>
            			<td style="border-right:hidden;">
            			<input type="checkbox" name="choice">
            			</td>
            			<td style="border-right:hidden;">
            				<label style="font-size:18px;"><%=num %></label>
            			</td>
            			<td id="bogumTitle" style="border-right:hidden;">
            				<label style="font-size:18px;"><b><%=ac.getMemberName() %></b></label><br>
            			</td>
            			<td id="tableContent" style="border-right:hidden; text-align:">
            				<label id="tableContent" style="font-size:18px; cursor:pointer; text-decoration: underline;"><b><%=ac.getResumeTitle() %></b></label><br>
            				<label id="tableContent" style="font-size:12px;">관심직종 : <%=ac.getApplyField() %></label><br>
            				<label id="tableContent" style="font-size:12px;">관심지역 : <%=ac.getInterestArea() %></label><br>
            			</td>
            			<td>
            				<label id="bogumState" style="font-size:18px;"><%=ac.getEnrollDate() %></label>
            			</td>
          
            		<% 
      							num--;
            				} 
            		
            			} else {%>
            				<td colspan="8"><label>검색 결과가 없습니다.</label></td>
            		 <% } %>
            		 </tr>
            		</tbody>
            	</table>
            	<br>
            	<button class="tableButton tableButton1"><b>스크랩</b></button>
            </div>
         
        <!-- footer 영역 --> 
		<!-- <footer>Footer</footer> -->
    </div>
</body>
</html>