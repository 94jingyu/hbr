<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>보금자리</h1>
	
	<h3><b>===== 공통 =====</b></h3>
	<h3><a href="views/guide/user_mainPage.jsp">개인 메인페이지_진규</a></h3>
	<h3><a href="views/guide/admin_mainPage.jsp">관리자 메인페이지_진규</a></h3>
	
	<hr>
	
	<h3>----김태원-----</h3>
	<h3><a href="<%= request.getContextPath()%>/selectListSearch.rc">채용정보</a></h3>
	<h3><a href="views/user_TAEWON/user_apply.jsp">입사지원하기 : RecruitDetail</a></h3>
	<h3><a href="<%= request.getContextPath()%>/selectApplyList.ap">입사지원내역</a></h3>
	<h3><a href="views/user_TAEWON/user_MyInquiryHistory.jsp">나의 문의내역</a></h3>
	<h3><a href="views/user_TAEWON/user_Inquiry.jsp">문의하기</a></h3>
	<h3><a href="views/user_TAEWON/admin_Inquiry.jsp">관리자_묻고답하기</a></h3>
	<h3><a href="<%= request.getContextPath()%>/select.iq">문의게시판</a></h3>
	<!-- <h3><a href="views/user_TAEWON/user_inquiryBoard.jsp">문의게시판</a></h3> -->
	<h3><a href="views/user_TAEWON/user_recruitBookmark.jsp">스크랩 보금자리</a></h3>
	<h3><a href="views/user_TAEWON/user_recentView.jsp">최근 본 보금자리</a></h3>
	
	<hr>
	
	<h3><a href="views/payment/paymentIndex.jsp">결제_윤관</a></h3>
	
	<hr>
	
	<h3><a href="views/boguem_dj/DJ_admin_register_screening_detail.jsp">다정_채용공고 상세페이지</a></h3>
	<h3><a href="views/boguem_dj/DJ_boguem_register.jsp">다정_보금자리등록</a></h3>
	<h3><a href="views/boguem_dj/DJ_bogeum_register_view.jsp">다정_register_view</a></h3>
	
	<hr>
	
	<h3><a href="views/user_JinHyeok/userMypage.jsp">유저마이페이지</a></h3>
	
	<h3><b>===== 다영 =====</b></h3>
	<h3><a href="views/business_DY/DY_business_MyPage.jsp">기업_마이페이지</a></h3>
	<h3><a href="views/business_DY/DY_business_ApproveRequest.jsp">기업_기업인증신청</a></h3>
	<h3><a href="views/business_DY/DY_business_ApproveRequestSuccess.jsp">기업_기업인증 신청완료</a></h3>
	<h3><a href="views/business_DY/DY_business_ResumeManage_list.jsp">기업_이력서관리_이력서 열람내역</a></h3>
	<h3><a href="views/business_DY/DY_business_ResumeManage_interview.jsp">기업_이력서관리_면접제의 인재정보</a></h3>
	<h3><a href="views/business_DY/DY_business_ResumeManage_bookmark.jsp">기업_이력서관리_스크랩 인재정보</a></h3>
	<h3><a href="views/business_DY/DY_business_ApplicantManage.jsp">기업_지원자관리</a></h3>
	<h3><a href="views/business_DY/DY_business_ApplicantSearch.jsp">기업_인재검색</a></h3>
	<h3><a href="views/business_DY/DY_admin_BusinessApprove.jsp">관리자_기업인증 신청내역</a></h3>
	<h3><a href="views/business_DY/DY_admin_BusinessApprove_detail.jsp">관리자_기업인증신청 상세보기</a></h3>
	

</body>
</html>