$(function (){
	//결제모듈연결
	
	$("#buyBtn").click(function(){
		
		var productName = $(".tabTd2").eq(1).text().substring(0,18);
		var productPrice = $(".tabTd2").eq(2).text();
		
		
		//1. window.IMP 변수 선언
		var IMP = window.IMP; //window.IMP 변수 선언

		//2. 회원가입 후 생성된 가맹점 식별코드를 이용해서 window.IMP변수를 초기화.
		//가맹점 식별코드는 관리자페이지  로그인 후 시스템설정->내정보에서 확인가능
		//IMP.unit();호추은 최초 1회 이루어져야하며, 페이지 로딩단계에서 미리 호출해두시면 보다 효율적입니다.
		IMP.init('imp44207208'); //init(발급받은 가맹점 식별코드 삽입)

		//3. 결제를 요청
		//결제 창을 띄워야 하는 페이지에서 아래의 IMP.request_pay({파라메타}) 함수를 호출합니다.
		//결제에 필요한 아래의 파라메터만 입력하고 함수를 실행하면 관리자 페이지에서 선택한 PG사의 결제 창이 실행됨.
		//복수 PG를 이용할 경우 PG파라미터의 값만 바꿔넣으면 원하는 PG사의 결제창으로 결제가 진행됨.

		IMP.request_pay({
			pg : 'uplus', //version 1.1.0부터 지원.

			/*
		    'kakao':카카오페이,
		    html5_inicis':이니시스(웹표준결제)
		    'nice':나이스페이
		    'jtnet':제이티넷
		    'uplus':LG유플러스
		    'danal':다날
		    'payco':페이코
		    'syrup':시럽페이
		    'paypal':페이팔
			 */

			pay_method : 'card',
			/*
		    'samsung':삼성페이,
		    'card':신용카드,
		    'trans':실시간계좌이체,
		    'vbank':가상계좌,
		    'phone':휴대폰소액결제
			 */

			merchant_uid : 'merchant_' + new Date().getTime(),
			/*
		    merchant_uid에 경우
		    https://docs.iamport.kr/implementation/payment
		       위에 url에 따라가시면 넣을 수 있는 방법이 있습니다.
		       참고하세요.
		       나중에 포스팅 해볼게요.
			 */

			//productName
			name : productName,
			//결제창에서 보여질 이름

			//productPrice - 나중에 변경하기
			amount : 100,
			//가격 
			
			buyer_email : 'iamport@siot.do',
			buyer_name : '구매자이름',
			buyer_tel : '010-1234-5678',
			buyer_addr : '서울특별시 강남구 삼성동',
			buyer_postcode : '123-456',
			
			/*
		       모바일 결제시,
		       결제가 끝나고 랜딩되는 URL을 지정
		    (카카오페이, 페이코, 다날의 경우는 필요없음. PC와 마찬가지로 callback함수로 결과가 떨어짐)
			 */
		}, function(rsp) {
			if ( rsp.success ) {
				var msg = '결제가 완료되었습니다.';
				msg += '고유ID : ' + rsp.imp_uid;
				msg += '상점 거래ID : ' + rsp.merchant_uid;
				msg += '결제 금액 : ' + rsp.paid_amount;
				msg += '카드 승인번호 : ' + rsp.apply_num;
				
			} else {
				var msg = '결제에 실패하였습니다.';
				msg += '에러내용 : ' + rsp.error_msg;
			}
			
			alert(msg);
		});
	});
});