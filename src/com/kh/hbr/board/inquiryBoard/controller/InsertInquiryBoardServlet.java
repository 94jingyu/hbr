package com.kh.hbr.board.inquiryBoard.controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.board.inquiryBoard.model.service.InquiryBoardService;
import com.kh.hbr.board.inquiryBoard.model.vo.InquiryBoard;
import com.kh.hbr.member.model.vo.Member;



@WebServlet("/insert.iq")
public class InsertInquiryBoardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public InsertInquiryBoardServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/*int memberNo = Integer.parseInt(request.getParameter("MemberNo"));*/
		int memberNo = ((Member) request.getSession().getAttribute("loginUser")).getMemberNo();
		String writerName = request.getParameter("writerName");
		String iqTitle = request.getParameter("iqTitle");
		String category = request.getParameter("Category");
		String iqContent = request.getParameter("iqContent");
//		int pwd = Integer.parseInt(request.getParameter("pwd"));
		String pwd = request.getParameter("pwd");
		
		System.out.println("memberNo : " + memberNo);
		System.out.println("writerName : " + writerName);
		System.out.println("iqTitle : " + iqTitle);
		System.out.println("category : " + category);
		System.out.println("iqContent : " + iqContent);
		System.out.println("pwd : " + pwd);
		
		int pwd2 = Integer.parseInt(pwd);
		
		InquiryBoard iqBoard = new InquiryBoard();
		iqBoard.setMemberNo(memberNo);
		iqBoard.setWriterName(writerName);
		iqBoard.setIqTitle(iqTitle);
		iqBoard.setCategory(category);
		iqBoard.setIqContent(iqContent);
		iqBoard.setPwd(pwd2);
		
		int result = new InquiryBoardService().insertInquiry(iqBoard);
		System.out.println("문의글 insert result : " + result);
		
		String page = "";
		
		if(result > 0) {
			page = "/h/select.iq";
			response.sendRedirect(page);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "문의하기 등록 실패");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
		 
		
	
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
