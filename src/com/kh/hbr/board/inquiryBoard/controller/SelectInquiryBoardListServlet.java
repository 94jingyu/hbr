package com.kh.hbr.board.inquiryBoard.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.board.inquiryBoard.model.service.InquiryBoardService;
import com.kh.hbr.board.inquiryBoard.model.vo.InquiryBoard;

@WebServlet("/select.iq")
public class SelectInquiryBoardListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectInquiryBoardListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ArrayList<InquiryBoard> list = new InquiryBoardService().selectList();
		System.out.println("문의글 select list : " + list);
		
		String page = "";
		if(list != null) {
			page = "views/user_TAEWON/user_inquiryBoard.jsp";
			request.setAttribute("list", list);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "문의게시판 목록조회 실패!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
