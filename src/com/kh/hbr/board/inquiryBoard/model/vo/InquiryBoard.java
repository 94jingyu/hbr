package com.kh.hbr.board.inquiryBoard.model.vo;

import java.sql.Date;

public class InquiryBoard implements java.io.Serializable {

	private int iqNo;
	private String iqTitle;
	private String category;
	private int cid;
	private String iqContent;
	private int memberNo;
	private String writerName;
	private String answerYn;
	private String answerContent;
	private int pwd;
	private Date enrollDate;
	private String boardCode;


	public InquiryBoard() {}


	public InquiryBoard(int iqNo, String iqTitle, String category, int cid, String iqContent, int memberNo,
			String writerName, String answerYn, String answerContent, int pwd, Date enrollDate, String boardCode) {
		super();
		this.iqNo = iqNo;
		this.iqTitle = iqTitle;
		this.category = category;
		this.cid = cid;
		this.iqContent = iqContent;
		this.memberNo = memberNo;
		this.writerName = writerName;
		this.answerYn = answerYn;
		this.answerContent = answerContent;
		this.pwd = pwd;
		this.enrollDate = enrollDate;
		this.boardCode = boardCode;
	}


	public int getIqNo() {
		return iqNo;
	}


	public void setIqNo(int iqNo) {
		this.iqNo = iqNo;
	}


	public String getIqTitle() {
		return iqTitle;
	}


	public void setIqTitle(String iqTitle) {
		this.iqTitle = iqTitle;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public int getCid() {
		return cid;
	}


	public void setCid(int cid) {
		this.cid = cid;
	}


	public String getIqContent() {
		return iqContent;
	}


	public void setIqContent(String iqContent) {
		this.iqContent = iqContent;
	}


	public int getMemberNo() {
		return memberNo;
	}


	public void setMemberNo(int memberNo) {
		this.memberNo = memberNo;
	}


	public String getWriterName() {
		return writerName;
	}


	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}


	public String getAnswerYn() {
		return answerYn;
	}


	public void setAnswerYn(String answerYn) {
		this.answerYn = answerYn;
	}


	public String getAnswerContent() {
		return answerContent;
	}


	public void setAnswerContent(String answerContent) {
		this.answerContent = answerContent;
	}


	public int getPwd() {
		return pwd;
	}


	public void setPwd(int pwd) {
		this.pwd = pwd;
	}


	public Date getEnrollDate() {
		return enrollDate;
	}


	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}


	public String getBoardCode() {
		return boardCode;
	}


	public void setBoardCode(String boardCode) {
		this.boardCode = boardCode;
	}


	@Override
	public String toString() {
		return "InquiryBoard [iqNo=" + iqNo + ", iqTitle=" + iqTitle + ", category=" + category + ", cid=" + cid
				+ ", iqContent=" + iqContent + ", memberNo=" + memberNo + ", writerName=" + writerName + ", answerYn="
				+ answerYn + ", answerContent=" + answerContent + ", pwd=" + pwd + ", enrollDate=" + enrollDate
				+ ", boardCode=" + boardCode + "]";
	}

	
	
	
}
