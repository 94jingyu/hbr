package com.kh.hbr.board.inquiryBoard.model.dao;

import static com.kh.hbr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.kh.hbr.board.inquiryBoard.model.vo.InquiryBoard;

public class InquiryBoardDao {

	private Properties prop = new Properties();
	
	public InquiryBoardDao() {
		
		String fileName = InquiryBoardDao.class.getResource("/sql/board/inquiryBoard-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	//문의글작성 insert
	public int insertInquiry(Connection con, InquiryBoard iqBoard) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("insertInquiry");
		
		try {
			
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, iqBoard.getMemberNo());
			pstmt.setString(2, iqBoard.getWriterName());
			pstmt.setString(3, iqBoard.getIqTitle());
			pstmt.setString(4, iqBoard.getCategory());
			pstmt.setString(5, iqBoard.getIqContent());
			pstmt.setInt(6, iqBoard.getPwd());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return result;
	}


	//문의게시판 select
	public ArrayList<InquiryBoard> selectList(Connection con) {
		
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<InquiryBoard> list = null;
		
		String query = prop.getProperty("selectListInquiry");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				
				InquiryBoard iq = new InquiryBoard();
				iq.setEnrollDate(rset.getDate("ENROLLDATE"));
				iq.setCategory(rset.getString("CATEGORY"));
				iq.setIqTitle(rset.getString("INQUIRY_TITLE"));
				iq.setWriterName(rset.getString("WRITER_NAME"));
				iq.setAnswerYn(rset.getString("ANSWER_YN"));
				
				list.add(iq);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	
	
	
	
}










