package com.kh.hbr.board.inquiryBoard.model.service;

import com.kh.hbr.board.inquiryBoard.model.dao.InquiryBoardDao;
import com.kh.hbr.board.inquiryBoard.model.vo.InquiryBoard;

import static com.kh.hbr.common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.ArrayList;

public class InquiryBoardService {

	
	//문의글 insert
	public int insertInquiry(InquiryBoard iqBoard) {
		
		Connection con = getConnection();
		int result = 0;
		
		result = new InquiryBoardDao().insertInquiry(con, iqBoard);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	
	//문의게시판 select
	public ArrayList<InquiryBoard> selectList() {

		Connection con = getConnection();
		
		ArrayList<InquiryBoard> list = new InquiryBoardDao().selectList(con);
		
		close(con);
		
		return list;
	}

	

	
}
