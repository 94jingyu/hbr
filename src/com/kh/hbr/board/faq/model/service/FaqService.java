package com.kh.hbr.board.faq.model.service;

import static com.kh.hbr.common.JDBCTemplate.close;
import static com.kh.hbr.common.JDBCTemplate.commit;
import static com.kh.hbr.common.JDBCTemplate.getConnection;
import static com.kh.hbr.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;

import com.kh.hbr.board.faq.model.dao.FaqDao;
import com.kh.hbr.board.faq.model.vo.Faq;
import com.kh.hbr.payment.model.vo.PageInfo;

public class FaqService {

	public int insertFaq(Faq faq) {
		int result = 0;
		Connection con = getConnection();
		
		result = new FaqDao().insertFaq(con, faq);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public int getListCount() {
		int result = 0;
		Connection con = getConnection();
		
		result = new FaqDao().getListCount(con);
		
		close(con);
		
		return result;
	}

	public ArrayList<Faq> loadList(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<Faq> faqList = new FaqDao().loadList(con, pi);
		
		close(con);
		
		return faqList;
	}

	public Faq loadDetailFaq(int bNum) {
		Connection con = getConnection();
		
		Faq detailFaq = new FaqDao().loadDetailFaq(con, bNum);
		
		int count = 0;
		
		if(detailFaq != null) {
			count = new FaqDao().updateCount(con, bNum);
			
			if(count > 0) {
				commit(con);
			}else {
				rollback(con);
			}
		}
		
		close(con);
		
		return detailFaq;
	}

	public Faq updateFaq(Faq faq) {
		Connection con = getConnection();
		Faq modifyFaq = null;
		
		int result = new FaqDao().updateFaq(con, faq);
		
		if(result > 0) {
			commit(con);
			modifyFaq = new FaqDao().loadDetailFaq(con, faq.getBoardNo());
			
		}else {
			rollback(con);
		}
		
		close(con);
		
		return modifyFaq;
	}

	public boolean deleteFaq(int[] deleteNo) {
		Connection con = getConnection();
		boolean finalResult = true;
		int result = 0;
		
		for(int i = 0; i < deleteNo.length; i++) {
			
			result = new FaqDao().deleteFaq(con, deleteNo[i]);
			
			if(result > 0) {
				commit(con);
			}else {
				rollback(con);
			}
			
			if(result <= 0) {
				finalResult = false;
			}
		}
		
		return finalResult;
	}

	public int getFrontListCount() {
		Connection con = getConnection();
		
		int result = new FaqDao().getFrontListCount(con);
		
		close(con);
		
		return result;
	}

	public ArrayList<Faq> selectFrontList(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<Faq> faqList = new FaqDao().selectFrontList(con, pi);
		
		close(con);
		
		return faqList;
	}

	public int getFrontSearchFaqListCount(int memberKind) {
		Connection con = getConnection();
		
		int result = new FaqDao().getFrontSearchFaqListCount(con, memberKind);
		
		close(con);
		
		return result;
	}

	public ArrayList<Faq> loadSearchFrontList(PageInfo pi, int memberKind) {
		Connection con = getConnection();
		
		ArrayList<Faq> faqList = new FaqDao().loadSearchFrontList(con, pi, memberKind);
		
		close(con);
		
		return faqList;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
