package com.kh.hbr.board.faq.model.dao;

import static com.kh.hbr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.kh.hbr.board.faq.model.vo.Faq;
import com.kh.hbr.payment.model.vo.PageInfo;

public class FaqDao {
	
	private Properties prop = new Properties();
	
	public FaqDao() {
		String filePath = FaqDao.class.getResource("/sql/board/faq-query.properties").getPath();
		try {
			prop.load(new FileReader(filePath));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int insertFaq(Connection con, Faq faq) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("insertFaq");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, faq.getMemberNo());
			pstmt.setString(2, faq.getTitle());
			pstmt.setString(3, faq.getContent1());
			pstmt.setString(4, faq.getContent2());
			pstmt.setString(5, faq.getMemberKind());
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	public int getListCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;
		String query = prop.getProperty("getListCount");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return result;
	}

	public ArrayList<Faq> loadList(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Faq> loadList = null;
		
		String query = prop.getProperty("loadList");
		
		try {
			pstmt = con.prepareStatement(query);
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			loadList = new ArrayList<Faq>();
			while(rset.next()) {
				Faq faq = new Faq();
				faq.setBoardNo(rset.getInt("BOARD_NO"));
				faq.setMemberKind(rset.getString("MEMBER_KIND"));
				faq.setTitle(rset.getString("TITLE"));
				faq.setbCount(rset.getInt("BCOUNT"));
				faq.setEnrollDate(rset.getDate("ENROLL_DATE"));
				
				loadList.add(faq);
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return loadList;
	}

	public Faq loadDetailFaq(Connection con, int bNum) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Faq detailFaq = null;
		
		String query = prop.getProperty("loadDetailFaq");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, bNum);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				detailFaq = new Faq();
				detailFaq.setBoardNo(rset.getInt("BOARD_NO"));
				detailFaq.setMemberNo(rset.getInt("MEMBER_NO"));
				detailFaq.setTitle(rset.getString("TITLE"));
				detailFaq.setContent1(rset.getString("CONTENT1"));
				detailFaq.setContent2(rset.getString("CONTENT2"));
				detailFaq.setbCount(rset.getInt("BCOUNT"));
				detailFaq.setbSource(rset.getString("BSOURCE"));
				detailFaq.setEnrollDate(rset.getDate("ENROLL_DATE"));
				detailFaq.setModifyDate(rset.getDate("MODIFY_DATE"));
				detailFaq.setDeleteYn(rset.getString("DELETE_YN"));
				detailFaq.setBoardCode(rset.getString("BOARD_CODE"));
				detailFaq.setMemberKind(rset.getString("MEMBER_KIND"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return detailFaq;
	}

	public int updateCount(Connection con, int bNum) {
		int count = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("updateCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, bNum);
			pstmt.setInt(2, bNum);
			count = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return count;
	}

	public int updateFaq(Connection con, Faq faq) {
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("updateFaq");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, faq.getTitle());
			pstmt.setString(2, faq.getContent1());
			pstmt.setString(3, faq.getContent2());
			pstmt.setString(4, faq.getMemberKind());
			pstmt.setInt(5, faq.getBoardNo());
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int deleteFaq(Connection con, int i) {
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("deleteFaq");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, i);
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int getFrontListCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getFrontListCount");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return result;
	}

	public ArrayList<Faq> selectFrontList(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Faq> faqList = null;
		
		String query = prop.getProperty("selectFrontList");
		
		int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
		int endRow = startRow + pi.getLimit() - 1;

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			rset = pstmt.executeQuery();
			
			faqList = new ArrayList<Faq>();
			
			while(rset.next()) {
				Faq faq = new Faq();
				faq.setBoardNo(rset.getInt("BOARD_NO"));
				faq.setMemberNo(rset.getInt("MEMBER_NO"));
				faq.setTitle(rset.getString("TITLE"));
				faq.setContent1(rset.getString("CONTENT1"));
				faq.setContent2(rset.getString("CONTENT2"));
				faq.setbCount(rset.getInt("BCOUNT"));
				faq.setbSource(rset.getString("BSOURCE"));
				faq.setEnrollDate(rset.getDate("ENROLL_DATE"));
				faq.setModifyDate(rset.getDate("MODIFY_DATE"));
				faq.setDeleteYn(rset.getString("DELETE_YN"));
				faq.setBoardCode(rset.getString("BOARD_CODE"));
				faq.setMemberKind(rset.getString("MEMBER_KIND"));
				
				faqList.add(faq);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return faqList;
	}

	public int getFrontSearchFaqListCount(Connection con, int memberKind) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("getFrontSearchFaqListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberKind);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	public ArrayList<Faq> loadSearchFrontList(Connection con, PageInfo pi, int memberKind) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Faq> faqList = null;
		
		String query = prop.getProperty("loadSearchFrontList");
		
		int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
		int endRow = startRow + pi.getLimit() - 1;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberKind);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			rset = pstmt.executeQuery();
			
			faqList = new ArrayList<Faq>();
			
			while(rset.next()) {
				Faq faq = new Faq();
				faq.setBoardNo(rset.getInt("BOARD_NO"));
				faq.setMemberNo(rset.getInt("MEMBER_NO"));
				faq.setTitle(rset.getString("TITLE"));
				faq.setContent1(rset.getString("CONTENT1"));
				faq.setContent2(rset.getString("CONTENT2"));
				faq.setbCount(rset.getInt("BCOUNT"));
				faq.setbSource(rset.getString("BSOURCE"));
				faq.setEnrollDate(rset.getDate("ENROLL_DATE"));
				faq.setModifyDate(rset.getDate("MODIFY_DATE"));
				faq.setDeleteYn(rset.getString("DELETE_YN"));
				faq.setBoardCode(rset.getString("BOARD_CODE"));
				faq.setMemberKind(rset.getString("MEMBER_KIND"));
				
				faqList.add(faq);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return faqList;
	}

}
