package com.kh.hbr.board.faq.model.vo;

import java.sql.Date;

public class Faq implements java.io.Serializable {
	private int boardNo;
	private int memberNo;
	private String title;
	private String content1;
	private String content2;
	private int bCount;
	private String bSource;
	private Date enrollDate;
	private Date modifyDate;
	private String deleteYn;
	private String boardCode;
	private String memberKind;
	
	public Faq() {}

	public Faq(int boardNo, int memberNo, String title, String content1, String content2, int bCount, String bSource,
			Date enrollDate, Date modifyDate, String deleteYn, String boardCode, String memberKind) {
		super();
		this.boardNo = boardNo;
		this.memberNo = memberNo;
		this.title = title;
		this.content1 = content1;
		this.content2 = content2;
		this.bCount = bCount;
		this.bSource = bSource;
		this.enrollDate = enrollDate;
		this.modifyDate = modifyDate;
		this.deleteYn = deleteYn;
		this.boardCode = boardCode;
		this.memberKind = memberKind;
	}

	public String getMemberKind() {
		return memberKind;
	}

	public void setMemberKind(String memberKind) {
		this.memberKind = memberKind;
	}

	public int getBoardNo() {
		return boardNo;
	}

	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}

	public int getMemberNo() {
		return memberNo;
	}

	public void setMemberNo(int memberNo) {
		this.memberNo = memberNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent1() {
		return content1;
	}

	public void setContent1(String content1) {
		this.content1 = content1;
	}

	public String getContent2() {
		return content2;
	}

	public void setContent2(String content2) {
		this.content2 = content2;
	}

	public int getbCount() {
		return bCount;
	}

	public void setbCount(int bCount) {
		this.bCount = bCount;
	}

	public String getbSource() {
		return bSource;
	}

	public void setbSource(String bSource) {
		this.bSource = bSource;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getDeleteYn() {
		return deleteYn;
	}

	public void setDeleteYn(String deleteYn) {
		this.deleteYn = deleteYn;
	}

	public String getBoardCode() {
		return boardCode;
	}

	public void setBoardCode(String boardCode) {
		this.boardCode = boardCode;
	}

	@Override
	public String toString() {
		return "Faq [boardNo=" + boardNo + ", memberNo=" + memberNo + ", title=" + title + ", content1=" + content1
				+ ", content2=" + content2 + ", bCount=" + bCount + ", bSource=" + bSource + ", enrollDate="
				+ enrollDate + ", modifyDate=" + modifyDate + ", deleteYn=" + deleteYn + ", boardCode=" + boardCode
				+ ", memberKind=" + memberKind + "]";
	}
	
}
