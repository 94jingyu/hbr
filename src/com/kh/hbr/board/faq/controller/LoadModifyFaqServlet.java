package com.kh.hbr.board.faq.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.board.faq.model.service.FaqService;
import com.kh.hbr.board.faq.model.vo.Faq;

@WebServlet("/loadModify.faq")
public class LoadModifyFaqServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoadModifyFaqServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int bNo = Integer.parseInt(request.getParameter("bNo"));
		
		Faq detailFaq = new FaqService().loadDetailFaq(bNo);
		
		String page = "";
		if(detailFaq != null) {
			page = "views/payment/admin_faqUpdate.jsp";
			request.setAttribute("detailFaq", detailFaq);
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "FAQ상세조회 실패!!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	
	
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
