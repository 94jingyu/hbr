package com.kh.hbr.board.faq.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.board.faq.model.service.FaqService;
import com.kh.hbr.board.faq.model.vo.Faq;

@WebServlet("/loadDetail.faq")
public class LoadDetailFaqServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoadDetailFaqServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int bNum = Integer.parseInt(request.getParameter("num"));
		
		Faq detailFaq = new FaqService().loadDetailFaq(bNum);
	
		String page = "";
		if(detailFaq != null) {
			page = "views/payment/admin_faqDetail.jsp";
			request.setAttribute("detailFaq", detailFaq);
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "FAQ상세조회 실패!!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
