package com.kh.hbr.board.faq.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.board.faq.model.service.FaqService;
import com.kh.hbr.board.faq.model.vo.Faq;

@WebServlet("/update.faq")
public class UpdateFaqSerlvet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UpdateFaqSerlvet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String title = request.getParameter("faqTitle");
		String question = request.getParameter("questionTextArea");
		String answer = request.getParameter("answerTextArea");
		int bNo = Integer.parseInt(request.getParameter("bNo"));
		String memberKind = request.getParameter("selectOption");
	
		Faq faq = new Faq();
		faq.setTitle(title);
		faq.setContent1(question);
		faq.setContent2(answer);
		faq.setBoardNo(bNo);
		faq.setMemberKind(memberKind);
		
		Faq modifyFaq = new FaqService().updateFaq(faq);
		
		String page = "";
		if(modifyFaq != null) {
			page = "views/payment/admin_faqDetail.jsp";
			request.setAttribute("detailFaq", modifyFaq);
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "수정실패!!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
