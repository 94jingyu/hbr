package com.kh.hbr.board.faq.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.board.faq.model.service.FaqService;
import com.kh.hbr.board.faq.model.vo.Faq;

@WebServlet("/insert.faq")
public class InsertFaqServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public InsertFaqServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String title = request.getParameter("faqTitle");
		String question = request.getParameter("questionTextArea");
		String answer = request.getParameter("answerTextArea");
		int bMemberNo = Integer.parseInt(request.getParameter("bMemberNo"));
		String memberKind = request.getParameter("selectOption");
		
		Faq faq = new Faq();
		faq.setMemberNo(bMemberNo);
		faq.setTitle(title);
		faq.setContent1(question);
		faq.setContent2(answer);
		faq.setMemberKind(memberKind);
		
		int result = new FaqService().insertFaq(faq);
		
		String page = "";
		if(result > 0) {
			page = "loadList.faq";
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "faq 글 등록 실패!");
		}
			request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
