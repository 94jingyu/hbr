package com.kh.hbr.board.faq.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.board.faq.model.service.FaqService;
import com.kh.hbr.board.faq.model.vo.Faq;
import com.kh.hbr.payment.model.vo.PageInfo;

@WebServlet("/searchFrontFaq")
public class SearchFrontFaqServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SearchFrontFaqServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int memberKind = Integer.parseInt(request.getParameter("value"));
		
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		limit = 5;
		
		int listCount = new FaqService().getFrontSearchFaqListCount(memberKind);
	
		maxPage = (int) ((double) listCount / limit + 0.8);
		
		startPage = (((int) ((double) currentPage / 10 + 0.9)) - 1) * 10 + 1;
		endPage = startPage + 10 - 1;
		
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		ArrayList<Faq> faqList = new FaqService().loadSearchFrontList(pi, memberKind);
		
		String page = "";
		
		if(faqList != null) {
			page = "views/payment/searchFaqList.jsp";
			request.setAttribute("pi", pi);
			request.setAttribute("faqList", faqList);
			request.setAttribute("memberKind", memberKind);
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "FAQ조회 실패!!");
		}
			request.getRequestDispatcher(page).forward(request, response);
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
