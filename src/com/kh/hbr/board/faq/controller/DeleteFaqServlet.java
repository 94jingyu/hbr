package com.kh.hbr.board.faq.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.board.faq.model.service.FaqService;

@WebServlet("/delete.faq")
public class DeleteFaqServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DeleteFaqServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String before = request.getParameter("deleteNo");
		String[] noStr = before.split("#");
		int[] deleteNo = new int[noStr.length];
		
		for(int i = 0; i < deleteNo.length; i++) {
			deleteNo[i] = Integer.parseInt(noStr[i]);
		}
		
		boolean result = new FaqService().deleteFaq(deleteNo);
		
		if(result) {
			request.getRequestDispatcher("/loadList.faq").forward(request, response);
		}else {
			request.setAttribute("msg", "게시물 삭제실패!");
			request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
