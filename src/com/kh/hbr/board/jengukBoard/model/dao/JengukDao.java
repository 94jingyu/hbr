package com.kh.hbr.board.jengukBoard.model.dao;

import static com.kh.hbr.common.JDBCTemplate.*;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.board.jengukBoard.model.vo.Jenguk;
import com.kh.hbr.board.jengukBoard.model.vo.Like;
import com.kh.hbr.board.jengukBoard.model.vo.PageInfo;
import com.kh.hbr.board.jengukBoard.model.vo.Reply;
import com.kh.hbr.member.model.dao.MemberDao;
import com.kh.hbr.member.model.vo.Business;

public class JengukDao {
	private Properties prop = new Properties();
	
	public JengukDao() {
		String fileName = MemberDao.class.getResource("/sql/board/jengukBoard-query.properties").getPath();
	
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// 전국보금자리 자랑 등록하기
	public int insertJenguk(Connection con, Jenguk jengukBoard) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertJenguk");
		
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, jengukBoard.getJengukArea());
			pstmt.setString(2, jengukBoard.getCompanyName());
			pstmt.setString(3, jengukBoard.getJobChoice());
			pstmt.setString(4, jengukBoard.getTask());
			pstmt.setInt(5, jengukBoard.getScore());
			pstmt.setString(6, jengukBoard.getJengukTitle());
			pstmt.setString(7, jengukBoard.getJengukContent());
			pstmt.setString(8, jengukBoard.getSalOpenYn());
			pstmt.setString(9, jengukBoard.getSalKind());
			pstmt.setInt(10, jengukBoard.getSalMount());
			pstmt.setString(11, jengukBoard.getMemberId());
		
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}


	public int getListCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		
		int listCount = 0;
		
		String  query = prop.getProperty("listCount");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return listCount;
	}

	
	public int insertThumbnailContent(Connection con, Jenguk jengukBoard) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertThumb");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, jengukBoard.getJengukArea());
			pstmt.setString(2, jengukBoard.getCompanyName());
			pstmt.setString(3, jengukBoard.getJobChoice());
			pstmt.setString(4, jengukBoard.getTask());
			pstmt.setInt(5, jengukBoard.getScore());
			pstmt.setString(6, jengukBoard.getJengukTitle());
			pstmt.setString(7, jengukBoard.getJengukContent());
			pstmt.setString(8, jengukBoard.getSalOpenYn());
			pstmt.setString(9, jengukBoard.getSalKind());
			pstmt.setInt(10, jengukBoard.getSalMount());
			pstmt.setString(11, jengukBoard.getMemberId());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int selectCurrval(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int bid = 0;
		
		String query = prop.getProperty("selectCurrval");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				bid = rset.getInt("currval");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return bid;
	}

	public int insertAttachment(Connection con, Attachment attachment) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertAttachment");

		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, attachment.getBoardNo());
			pstmt.setString(2, attachment.getOriginName());
			pstmt.setString(3, attachment.getChangeName());
			pstmt.setString(4, attachment.getFilePath());
			pstmt.setInt(5, attachment.getFileLevel());
			
			result = pstmt.executeUpdate();
		
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<HashMap<String, Object>> selectThumbnailList(Connection con) {
		Statement stmt = null;
		ArrayList<HashMap<String, Object>> list = null;
		ResultSet rset = null;
		HashMap<String, Object> hmap = null;
		
		String query = prop.getProperty("selectThumbnailMap");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<HashMap<String, Object>>();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				hmap.put("jengukNo", rset.getInt("JENGUK_NO"));
				hmap.put("title", rset.getString("JENGUK_TITLE"));
				hmap.put("content", rset.getString("JENGUK_CONTENT"));
				hmap.put("memberId", rset.getString("MEMBER_ID"));
				hmap.put("boardCount", rset.getInt("BOARD_COUNT"));
				hmap.put("jengukEnrollDate", rset.getString("JENGUK_ENROLL_DATE"));
				hmap.put("attachmentNo", rset.getInt("ATTACHMENT_NO"));
				hmap.put("boardNo", rset.getInt("BOARD_NO"));
				hmap.put("originName", rset.getString("ORIGIN_NAME"));
				hmap.put("changeName", rset.getString("CHANGE_NAME"));
				hmap.put("filePath", rset.getString("FILE_PATH"));
				hmap.put("uploadDate", rset.getDate("UPLOAD_DATE"));

				hmap.put("jengukArea", rset.getString("JENGUK_AREA"));
				hmap.put("companyName", rset.getString("COMPANY_NAME"));
				hmap.put("jobChoice", rset.getString("JOB_CHOICE"));
				hmap.put("task", rset.getString("TASK"));
				hmap.put("score", rset.getInt("SCORE"));
				hmap.put("salKind", rset.getString("SAL_KIND"));
				hmap.put("salMount", rset.getInt("SAL_MOUNT"));
				
				list.add(hmap);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(stmt);
		}
		
		return list;
	}

	public int updateCount(Connection con, int num) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1,  num);
			pstmt.setInt(2,  num);
		
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public HashMap<String, Object> selectThumbnailMap(Connection con, int num) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		HashMap<String, Object> hmap = null;
		Jenguk j = null;
		ArrayList<Attachment> list = null;
		Attachment at = null;
		
		String query = prop.getProperty("selectThumbnailOne");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
		
			rset = pstmt.executeQuery();
			
			hmap = new HashMap<String, Object>();
			list = new ArrayList<Attachment>();
			
			while(rset.next()) {
				j = new Jenguk();
				j.setJengukNo(rset.getInt("JENGUK_NO"));
				j.setJengukTitle(rset.getString("JENGUK_TITLE"));

				// textarea 개행 문자 넣기
				String content = rset.getString("JENGUK_CONTENT");
				content = content.replace("\r\n","<br>");
				j.setJengukContent(content);
				
				j.setMemberId(rset.getString("MEMBER_ID"));
				j.setBoardCount(rset.getInt("BOARD_COUNT"));
				j.setJengukEnrollDate(rset.getDate("JENGUK_ENROLL_DATE"));
				
				j.setJengukArea(rset.getString("JENGUK_AREA"));
				j.setCompanyName(rset.getString("COMPANY_NAME"));
				j.setJobChoice(rset.getString("JOB_CHOICE"));
				j.setTask(rset.getString("TASK"));
				j.setScore(rset.getInt("SCORE"));
				j.setSalOpenYn(rset.getString("SAL_OPEN_YN"));
				j.setSalKind(rset.getString("SAL_KIND"));
				j.setSalMount(rset.getInt("SAL_MOUNT"));
				
				at = new Attachment();
				at.setAttachmentNo(rset.getInt("ATTACHMENT_NO"));
				at.setOriginName(rset.getString("ORIGIN_NAME"));
				at.setChangeName(rset.getString("CHANGE_NAME"));
				at.setFilePath(rset.getString("FILE_PATH"));
				at.setUploadDate(rset.getDate("UPLOAD_DATE"));
				
				
				list.add(at);
			}
			
			hmap.put("jenguk", j);
			hmap.put("fileList", list);
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		
		return hmap;
	}

	// 댓글 등록 insert
	public int insertReply(Connection con, Reply reply) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query= prop.getProperty("insertReply");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, reply.getReplyContent());
			pstmt.setString(2, reply.getMemberId());
			pstmt.setString(3, reply.getBoardCode());
			pstmt.setInt(4, reply.getBoardNo());
			
			result = pstmt.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	// 댓글 조회
	public List<Reply> selectReplyList(Connection con, int boardNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		List<Reply> list = null;
		
		String query = prop.getProperty("selectReplyList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, boardNo);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Reply>();
			
			while(rset.next()) {
				Reply r = new Reply();
				r.setBoardNo(rset.getInt("BOARD_NO"));
				r.setMemberId(rset.getString("MEMBER_ID"));
				r.setReplyContent(rset.getString("REPLY_CONTENT"));
				r.setReplyEnrollDate(rset.getDate("REPLY_ENROLL_DATE"));
				r.setDd(rset.getDate("REPLY_ENROLL_DATE").toString());
				
				list.add(r);
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}
	
	// 전국보금자리자랑 삭제
	public int deleteJenguk(Connection con, Jenguk jenguk) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("deleteJenguk");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1,  jenguk.getMemberId());
			pstmt.setInt(2,  jenguk.getJengukNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	// 좋아요 테이블 생성
	public int insertLike(Connection con, Like like) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query= prop.getProperty("insertLike");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, like.getBoardNo());
			//pstmt.setString(2, like.getMemberId());
			
			result = pstmt.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	
	
	// 좋아요 숫자 증가
	public int plusLike(Connection con, Like like) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("plusLike");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, like.getBoardNo());
			pstmt.setInt(2, like.getBoardNo());
			pstmt.setInt(3, like.getBoardNo());
			pstmt.setInt(4, like.getBoardNo());
			pstmt.setInt(5, like.getBoardNo());
			pstmt.setString(6, like.getLikeId());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	// 좋아요 취소
	public int minusLike(Connection con, Like like) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("minusLike");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, like.getBoardNo());
			pstmt.setInt(2, like.getBoardNo());
			pstmt.setInt(3, like.getBoardNo());
			pstmt.setInt(4, like.getBoardNo());
			pstmt.setInt(5, like.getBoardNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	// 좋아요 숫자 조회
	public Like changedCount(Connection con, Like like) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Like changedCount = null;
		
		String query = prop.getProperty("changedCount"); 
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, like.getBoardNo());
			pstmt.setInt(2, like.getBoardNo());
		
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				changedCount = new Like();
				changedCount.setLikeNo(rset.getInt("LIKE_NO"));
				changedCount.setBoardNo(rset.getInt("BOARD_NO"));
				changedCount.setLikeCount(rset.getInt("LIKE_COUNT"));
				changedCount.setHateCount(rset.getInt("HATE_COUNT"));
				changedCount.setLikeEnrollDate(rset.getDate("LIKE_ENROLL_DATE"));
				changedCount.setLikeId(rset.getString("LIKE_ID"));
				changedCount.setHateId(rset.getString("HATE_ID"));
				changedCount.setBoardCode(rset.getString("BOARD_CODE"));
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);

		}
		return changedCount;
		
	}

	// 싫어요 숫자 증가
	public int plusHate(Connection con, Like hate) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("plusHate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, hate.getBoardNo());
			pstmt.setInt(2, hate.getBoardNo());
			pstmt.setInt(3, hate.getBoardNo());
			pstmt.setInt(4, hate.getBoardNo());
			pstmt.setInt(5, hate.getBoardNo());
			pstmt.setString(6, hate.getHateId());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	// 싫어요 취소
	public int minusHate(Connection con, Like hate) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("minusHate");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1,  hate.getBoardNo());
			pstmt.setInt(2,  hate.getBoardNo());
			pstmt.setInt(3,  hate.getBoardNo());
			pstmt.setInt(4,  hate.getBoardNo());
			pstmt.setInt(5,  hate.getBoardNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	// 리스트 조회
	public ArrayList<HashMap<String, Object>> selectThumbnailList(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;		
		HashMap<String, Object> hmap = null;
		
		String query = prop.getProperty("selectListWithPaging");
		
		try {
			pstmt = con.prepareStatement(query);

			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				hmap.put("jengukNo", rset.getInt("JENGUK_NO"));
				hmap.put("title", rset.getString("JENGUK_TITLE"));
				hmap.put("content", rset.getString("JENGUK_CONTENT"));
				hmap.put("memberId", rset.getString("MEMBER_ID"));
				hmap.put("boardCount", rset.getInt("BOARD_COUNT"));
				String date = rset.getString("JENGUK_ENROLL_DATE");
				String result = date.substring(0, 10);
				hmap.put("jengukEnrollDate", result);
				
				
				hmap.put("attachmentNo", rset.getInt("ATTACHMENT_NO"));
				hmap.put("boardNo", rset.getInt("BOARD_NO"));
				hmap.put("originName", rset.getString("ORIGIN_NAME"));
				hmap.put("changeName", rset.getString("CHANGE_NAME"));
				hmap.put("filePath", rset.getString("FILE_PATH"));
				hmap.put("uploadDate", rset.getDate("UPLOAD_DATE"));

				
				hmap.put("jengukArea", rset.getString("JENGUK_AREA"));
				hmap.put("companyName", rset.getString("COMPANY_NAME"));
				hmap.put("jobChoice", rset.getString("JOB_CHOICE"));
				hmap.put("task", rset.getString("TASK"));
				hmap.put("score", rset.getInt("SCORE"));
				hmap.put("salKind", rset.getString("SAL_KIND"));
				hmap.put("salMount", rset.getInt("SAL_MOUNT"));
				
				list.add(hmap);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}


	// 이전에 좋아요를 누른 기록이 있는지 조회
	public int preLikeCheck(Connection con, Like like) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("preLikeCheck");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1,  like.getLikeId());
			pstmt.setInt(2,  like.getBoardNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	// 좋아요를 누른 아이디를 null로 업데이트
	public int nullLikeId(Connection con, Like like) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("nullLikeId");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, like.getBoardNo());
			pstmt.setString(2, like.getLikeId());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	// 이전에 싫어요를 누른 기록이 있는지 조회
	public int preHateCheck(Connection con, Like hate) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("preHateCheck");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1,  hate.getHateId());
			pstmt.setInt(2,  hate.getBoardNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	
	// 싫어요를 누른 아이디를 null로 업데이트
	public int nullHateId(Connection con, Like hate) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("nullHateId");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1,  hate.getBoardNo());
			pstmt.setString(2,  hate.getHateId());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	// 좋아요가 많은 베스트
	public ArrayList<HashMap<String, Object>> selectBestList(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> bestList = null;		
		HashMap<String, Object> hmap = null;
		
		String query = prop.getProperty("selectBestList");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			bestList = new ArrayList<>();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				hmap.put("jengukNo", rset.getInt("JENGUK_NO"));
				hmap.put("title", rset.getString("JENGUK_TITLE"));
				hmap.put("content", rset.getString("JENGUK_CONTENT"));
				hmap.put("memberId", rset.getString("MEMBER_ID"));
				hmap.put("boardCount", rset.getInt("BOARD_COUNT"));
				
				String date = rset.getString("JENGUK_ENROLL_DATE");
				String result = date.substring(0, 10);
				hmap.put("jengukEnrollDate", result);
				
				//System.out.println(result);
				
//				hmap.put("attachmentNo", rset.getInt("ATTACHMENT_NO"));
//				hmap.put("boardNo", rset.getInt("BOARD_NO"));
//				hmap.put("originName", rset.getString("ORIGIN_NAME"));
//				hmap.put("changeName", rset.getString("CHANGE_NAME"));
//				hmap.put("filePath", rset.getString("FILE_PATH"));
//				hmap.put("uploadDate", rset.getDate("UPLOAD_DATE"));

				hmap.put("jengukArea", rset.getString("JENGUK_AREA"));
				hmap.put("companyName", rset.getString("COMPANY_NAME"));
				hmap.put("jobChoice", rset.getString("JOB_CHOICE"));
				hmap.put("task", rset.getString("TASK"));
				hmap.put("score", rset.getInt("SCORE"));
				hmap.put("salKind", rset.getString("SAL_KIND"));
				hmap.put("salMount", rset.getInt("SAL_MOUNT"));
				
				bestList.add(hmap);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return bestList;
	}

	// 좋아요 베스트 페이징 처리
	public ArrayList<HashMap<String, Object>> selectBestListP(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> bestList = null;		
		HashMap<String, Object> hmap = null;
		
		String query = prop.getProperty("selectBestListP");
		
		try {
			pstmt = con.prepareStatement(query);

			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			bestList = new ArrayList<>();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				hmap.put("jengukNo", rset.getInt("JENGUK_NO"));
				hmap.put("title", rset.getString("JENGUK_TITLE"));
				hmap.put("content", rset.getString("JENGUK_CONTENT"));
				hmap.put("memberId", rset.getString("MEMBER_ID"));
				hmap.put("boardCount", rset.getInt("BOARD_COUNT"));
				
				String date = rset.getString("JENGUK_ENROLL_DATE");
				String result = date.substring(0, 10);
				hmap.put("jengukEnrollDate", result);

				hmap.put("jengukArea", rset.getString("JENGUK_AREA"));
				hmap.put("companyName", rset.getString("COMPANY_NAME"));
				hmap.put("jobChoice", rset.getString("JOB_CHOICE"));
				hmap.put("task", rset.getString("TASK"));
				hmap.put("score", rset.getInt("SCORE"));
				hmap.put("salKind", rset.getString("SAL_KIND"));
				hmap.put("salMount", rset.getInt("SAL_MOUNT"));
				
				bestList.add(hmap);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return bestList;
	}

	// 직종만 선택
	public ArrayList<HashMap<String, Object>> searchJob(Connection con, String job, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;		
		HashMap<String, Object> hmap = null;
		
		String query = prop.getProperty("searchJob");
		
		try {
			pstmt = con.prepareStatement(query);

			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			
			pstmt.setString(1, job);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				hmap.put("jengukNo", rset.getInt("JENGUK_NO"));
				hmap.put("title", rset.getString("JENGUK_TITLE"));
				hmap.put("content", rset.getString("JENGUK_CONTENT"));
				hmap.put("memberId", rset.getString("MEMBER_ID"));
				hmap.put("boardCount", rset.getInt("BOARD_COUNT"));
				
				String date = rset.getString("JENGUK_ENROLL_DATE");
				String result = date.substring(0, 10);
				hmap.put("jengukEnrollDate", result);
				//System.out.println(result);
				
				hmap.put("jengukArea", rset.getString("JENGUK_AREA"));
				hmap.put("companyName", rset.getString("COMPANY_NAME"));
				hmap.put("jobChoice", rset.getString("JOB_CHOICE"));
				hmap.put("task", rset.getString("TASK"));
				hmap.put("score", rset.getInt("SCORE"));
				hmap.put("salKind", rset.getString("SAL_KIND"));
				hmap.put("salMount", rset.getInt("SAL_MOUNT"));
				
				list.add(hmap);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	// 지역만 선택
	public ArrayList<HashMap<String, Object>> searchLocation(Connection con, String location, PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;		
		HashMap<String, Object> hmap = null;
		
		String query = prop.getProperty("searchLocation");
		
		try {
			pstmt = con.prepareStatement(query);

			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			
			pstmt.setString(1, location);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				hmap.put("jengukNo", rset.getInt("JENGUK_NO"));
				hmap.put("title", rset.getString("JENGUK_TITLE"));
				hmap.put("content", rset.getString("JENGUK_CONTENT"));
				hmap.put("memberId", rset.getString("MEMBER_ID"));
				hmap.put("boardCount", rset.getInt("BOARD_COUNT"));
				
				String date = rset.getString("JENGUK_ENROLL_DATE");
				String result = date.substring(0, 10);
				hmap.put("jengukEnrollDate", result);

				hmap.put("jengukArea", rset.getString("JENGUK_AREA"));
				hmap.put("companyName", rset.getString("COMPANY_NAME"));
				hmap.put("jobChoice", rset.getString("JOB_CHOICE"));
				hmap.put("task", rset.getString("TASK"));
				hmap.put("score", rset.getInt("SCORE"));
				hmap.put("salKind", rset.getString("SAL_KIND"));
				hmap.put("salMount", rset.getInt("SAL_MOUNT"));
				
				list.add(hmap);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	// 직종 지역 둘다 선택
	public ArrayList<HashMap<String, Object>> searchLocationJob(Connection con, String location, String job,
			PageInfo pi) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<HashMap<String, Object>> list = null;		
		HashMap<String, Object> hmap = null;
		
		String query = prop.getProperty("searchLocationJob");
		
		try {
			pstmt = con.prepareStatement(query);

			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			
			pstmt.setString(1, location);
			pstmt.setString(2, job);
			pstmt.setInt(3, startRow);
			pstmt.setInt(4, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				hmap = new HashMap<String, Object>();
				hmap.put("jengukNo", rset.getInt("JENGUK_NO"));
				hmap.put("title", rset.getString("JENGUK_TITLE"));
				hmap.put("content", rset.getString("JENGUK_CONTENT"));
				hmap.put("memberId", rset.getString("MEMBER_ID"));
				hmap.put("boardCount", rset.getInt("BOARD_COUNT"));
				
				String date = rset.getString("JENGUK_ENROLL_DATE");
				String result = date.substring(0, 10);
				hmap.put("jengukEnrollDate", result);

				hmap.put("jengukArea", rset.getString("JENGUK_AREA"));
				hmap.put("companyName", rset.getString("COMPANY_NAME"));
				hmap.put("jobChoice", rset.getString("JOB_CHOICE"));
				hmap.put("task", rset.getString("TASK"));
				hmap.put("score", rset.getInt("SCORE"));
				hmap.put("salKind", rset.getString("SAL_KIND"));
				hmap.put("salMount", rset.getInt("SAL_MOUNT"));
				
				list.add(hmap);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}
	

	
}


