package com.kh.hbr.board.jengukBoard.model.service;

import static com.kh.hbr.common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.kh.hbr.board.jengukBoard.model.dao.JengukDao;
import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.board.jengukBoard.model.vo.Jenguk;
import com.kh.hbr.board.jengukBoard.model.vo.Like;
import com.kh.hbr.board.jengukBoard.model.vo.PageInfo;
import com.kh.hbr.board.jengukBoard.model.vo.Reply;

public class JengukService {

	public int insertJenguk(Jenguk jengukBoard) {
		Connection con = getConnection();
		
		int result = new JengukDao().insertJenguk(con, jengukBoard);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public int getListCount() {
		Connection con = getConnection();
		
		int listCount = new JengukDao().getListCount(con);
		
		close(con);
		
		return listCount;
	}

	// 전국보금자리 자랑 글작성
	public int insertThumbnail(Jenguk jengukBoard, ArrayList<Attachment> fileList) {
		Connection con = getConnection();
		
		int result1 = new JengukDao().insertThumbnailContent(con, jengukBoard);  // board에 대한 정보를 insert

		int result2 = 0; // 첨부파일
		int bid = new JengukDao().selectCurrval(con);  // bid는 어떤 게시물을 참조하고 있는지를 나타내는 아이디(게시물 번호)
		if(result1 > 0) {
			if(bid > 0) {
				for(int i = 0; i < fileList.size(); i++) {
					fileList.get(i).setBoardNo((bid));	
					
					// 파일의 인덱스에 따라 다른 파일레벨을 DB에 저장
					if(i == 0) {
						fileList.get(i).setFileLevel(1);
					} else if(i == 1) {
						fileList.get(i).setFileLevel(0);
					}

					result2 += new JengukDao().insertAttachment(con, fileList.get(i));
				}
			}
		} 

		
		// 좋아요 추가
		Like like = new Like();
		like.setBoardNo(bid);
		//like.setMemberId(jengukBoard.getMemberId());
		int result3 = new JengukDao().insertLike(con, like);
		
		int result = 0;
		if(result1 > 0 && result2 == fileList.size() && result3 > 0) { 
			commit(con);
			result = 1;
		} else {
			rollback(con);
			result = 0;
		}
		
		close(con);
		
		return result;
	}

	// 전국보금자리 자랑 전체보기
	public ArrayList<HashMap<String, Object>> selectThumbnailList() {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new JengukDao().selectThumbnailList(con);
		
		close(con);
		
		return list;
	}

	
	// 전국보금자리 자랑 전체보기
	public HashMap<String, Object> selectThumnailMap(int num) {
		Connection con = getConnection();
		int result = new JengukDao().updateCount(con, num);
		
		HashMap<String, Object> hmap = null;
		if(result > 0) {
			commit(con);
			hmap = new JengukDao().selectThumbnailMap(con, num);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		
		return hmap;
	}

	// 전국보금자리 자랑 댓글작성
	public List<Reply> insertReply(Reply reply) {
		Connection con = getConnection();
		List<Reply> replyList = null;
		
		int result = new JengukDao().insertReply(con, reply);
		
		if(result > 0) {
			commit(con);
			replyList = new JengukDao().selectReplyList(con, reply.getBoardNo());
		} else {
			rollback(con);
		}
		close(con);
		
		return replyList;
	}

	// 전국보금자리자랑 삭제
	public int deleteJenguk(Jenguk jenguk) {
		Connection con = getConnection();
		int result = new JengukDao().deleteJenguk(con, jenguk);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	// 좋아요 숫자 증가
	public int plusLike(Like like) {
		Connection con = getConnection();
		int result = new JengukDao().plusLike(con, like);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}

	// 좋아요 숫자 조회
	public Like changedCount(Like like) {
		Connection con = getConnection();
		Like changedCount = new JengukDao().changedCount(con, like);
		
		close(con);
		
		return changedCount;
	}

	// 좋아요 취소
	public int minusLike(Like like) {
		Connection con = getConnection();
		int result = new JengukDao().minusLike(con, like);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}

	// 싫어요 증가
	public int plusHate(Like hate) {
		Connection con = getConnection();
		int result = new JengukDao().plusHate(con, hate);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;	}

	// 싫어요 취소
	public int minusHate(Like hate) {
		Connection con = getConnection();
		int result = new JengukDao().minusHate(con, hate);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}

	// 댓글 조회
	public List<Reply> selectReplyList(Reply reply) {
		Connection con = getConnection();
		
		List<Reply> replyList = new JengukDao().selectReplyList(con, reply.getBoardNo());
		
		close(con);
		
		return replyList;
		
	}

	// 페이징 시도1
	public ArrayList<HashMap<String, Object>> selectThumbnailList(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> list = new JengukDao().selectThumbnailList(con, pi);
		
		close(con);
		
		return list;
	}


	// 이전에 좋아요를 누른 기록이 있는지 조회
	public int preLikeCheck(Like like) {
		Connection con = getConnection();
		int result = new JengukDao().preLikeCheck(con, like);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}

	// 좋아요 누른 아이디를 null로 업데이트
	public int nullLikeId(Like like) {
		Connection con = getConnection();
		int result = new JengukDao().nullLikeId(con, like);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	// 이전에 싫어요를 누른 기록이 있는지 조회
	public int preHateCheck(Like hate) {
		Connection con = getConnection();
		int result = new JengukDao().preHateCheck(con, hate);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}
	
	// 싫어요를 누른 아이디를 null로
	public int nullHateId(Like hate) {
		Connection con = getConnection();
		int result = new JengukDao().nullHateId(con, hate);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	// 좋아요가 많은 리스트
	public ArrayList<HashMap<String, Object>> selectBestList() {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> bestList = new JengukDao().selectBestList(con);
		
		close(con);
		
		return bestList;
	}

	// 좋아요 많은 리스트 페이징 처리
	public ArrayList<HashMap<String, Object>> selectBestListP(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<HashMap<String, Object>> bestList = new JengukDao().selectBestListP(con, pi);
		
		close(con);
		
		return bestList;
	}

	// 직종만 선택
	public ArrayList<HashMap<String, Object>> searchJob(String job, PageInfo pi) {
		Connection con = getConnection();
		ArrayList<HashMap<String, Object>> list = null;
		
		list = new JengukDao().searchJob(con, job, pi);
		
		close(con);
		
		return list;
	}

	// 지역만 선택
	public ArrayList<HashMap<String, Object>> searchLocation(String location, PageInfo pi) {
		Connection con = getConnection();
		ArrayList<HashMap<String, Object>> list = null;	
		
		list = new JengukDao().searchLocation(con, location, pi);
		
		close(con);
		
		return list;
	}

	// 지역 직종 둘다 선택
	public ArrayList<HashMap<String, Object>> searchLocationJob(String location, String job, PageInfo pi) {
		Connection con = getConnection();
		ArrayList<HashMap<String, Object>> list = null;
		
		list = new JengukDao().searchLocationJob(con, location, job, pi);
		
		close(con);
		
		return list;
	}
	
	
}
