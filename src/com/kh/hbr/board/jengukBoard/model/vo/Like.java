package com.kh.hbr.board.jengukBoard.model.vo;

import java.sql.Date;

public class Like implements java.io.Serializable{
	private int likeNo;
	private int boardNo;
	private int likeCount;
	private int hateCount;
	private Date likeEnrollDate;
	private String likeId;
	private String hateId;
	private String boardCode;
	
	public Like() {}

	public Like(int likeNo, int boardNo, int likeCount, int hateCount, Date likeEnrollDate, String likeId, String hateId,
			String boardCode) {
		super();
		this.likeNo = likeNo;
		this.boardNo = boardNo;
		this.likeCount = likeCount;
		this.hateCount = hateCount;
		this.likeEnrollDate = likeEnrollDate;
		this.likeId = likeId;
		this.hateId = hateId;
		this.boardCode = boardCode;
	}

	public int getLikeNo() {
		return likeNo;
	}

	public int getBoardNo() {
		return boardNo;
	}

	public int getLikeCount() {
		return likeCount;
	}

	public int getHateCount() {
		return hateCount;
	}

	public Date getLikeEnrollDate() {
		return likeEnrollDate;
	}


	public String getBoardCode() {
		return boardCode;
	}

	public void setLikeNo(int likeNo) {
		this.likeNo = likeNo;
	}

	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}

	public void setHateCount(int hateCount) {
		this.hateCount = hateCount;
	}

	public void setLikeEnrollDate(Date likeEnrollDate) {
		this.likeEnrollDate = likeEnrollDate;
	}


	public String getLikeId() {
		return likeId;
	}

	public String getHateId() {
		return hateId;
	}

	public void setLikeId(String likeId) {
		this.likeId = likeId;
	}

	public void setHateId(String hateId) {
		this.hateId = hateId;
	}

	public void setBoardCode(String boardCode) {
		this.boardCode = boardCode;
	}

	@Override
	public String toString() {
		return "Like [likeNo=" + likeNo + ", boardNo=" + boardNo + ", likeCount=" + likeCount + ", hateCount="
				+ hateCount + ", likeEnrollDate=" + likeEnrollDate + ", likeId=" + likeId + ", hateId=" + hateId
				+ ", boardCode=" + boardCode + "]";
	}
	
}
