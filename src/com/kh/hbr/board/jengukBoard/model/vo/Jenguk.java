package com.kh.hbr.board.jengukBoard.model.vo;

import java.sql.Date;

public class Jenguk implements java.io.Serializable {
	private int jengukNo;			
	private String jengukArea;		
	private String companyName;		
	private String jobChoice;		
	private String task;			
	private int score;				
	private String jengukTitle;		
	private String jengukContent;	
	private String salOpenYn;		
	private String salKind;			
	private int salMount;			
	private int boardCount;  		// 조회수
	private Date jengukEnrollDate;  // 등록일
	private Date jengukModifyDate;  // 수정일
	private String deleteYn;  		// 삭제여부
	private String approveYn;		// 승인여부
		
	private String memberId;  		// 멤버 아이디 조인
	private int refBid;				// 원글이냐 답글이냐			
	private int replyLevel;			// 댓글뭐여	
	private String boardCode;       // 게시판코드
	
	public Jenguk() {}

	public Jenguk(int jengukNo, String jengukArea, String companyName, String jobChoice, String task, int score,
			String jengukTitle, String jengukContent, String salOpenYn, String salKind, int salMount, int boardCount,
			Date jengukEnrollDate, Date jengukModifyDate, String deleteYn, String approveYn, String memberId,
			int refBid, int replyLevel, String boardCode) {
		super();
		this.jengukNo = jengukNo;
		this.jengukArea = jengukArea;
		this.companyName = companyName;
		this.jobChoice = jobChoice;
		this.task = task;
		this.score = score;
		this.jengukTitle = jengukTitle;
		this.jengukContent = jengukContent;
		this.salOpenYn = salOpenYn;
		this.salKind = salKind;
		this.salMount = salMount;
		this.boardCount = boardCount;
		this.jengukEnrollDate = jengukEnrollDate;
		this.jengukModifyDate = jengukModifyDate;
		this.deleteYn = deleteYn;
		this.approveYn = approveYn;
		this.memberId = memberId;
		this.refBid = refBid;
		this.replyLevel = replyLevel;
		this.boardCode = boardCode;
	}

	public int getJengukNo() {
		return jengukNo;
	}

	public String getJengukArea() {
		return jengukArea;
	}

	public String getCompanyName() {
		return companyName;
	}

	public String getJobChoice() {
		return jobChoice;
	}

	public String getTask() {
		return task;
	}

	public int getScore() {
		return score;
	}

	public String getJengukTitle() {
		return jengukTitle;
	}

	public String getJengukContent() {
		return jengukContent;
	}

	public String getSalOpenYn() {
		return salOpenYn;
	}

	public String getSalKind() {
		return salKind;
	}

	public int getSalMount() {
		return salMount;
	}

	public int getBoardCount() {
		return boardCount;
	}

	public Date getJengukEnrollDate() {
		return jengukEnrollDate;
	}

	public Date getJengukModifyDate() {
		return jengukModifyDate;
	}

	public String getDeleteYn() {
		return deleteYn;
	}

	public String getApproveYn() {
		return approveYn;
	}

	public String getMemberId() {
		return memberId;
	}

	public int getRefBid() {
		return refBid;
	}

	public int getReplyLevel() {
		return replyLevel;
	}

	public String getBoardCode() {
		return boardCode;
	}

	public void setJengukNo(int jengukNo) {
		this.jengukNo = jengukNo;
	}

	public void setJengukArea(String jengukArea) {
		this.jengukArea = jengukArea;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setJobChoice(String jobChoice) {
		this.jobChoice = jobChoice;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void setJengukTitle(String jengukTitle) {
		this.jengukTitle = jengukTitle;
	}

	public void setJengukContent(String jengukContent) {
		this.jengukContent = jengukContent;
	}

	public void setSalOpenYn(String salOpenYn) {
		this.salOpenYn = salOpenYn;
	}

	public void setSalKind(String salKind) {
		this.salKind = salKind;
	}

	public void setSalMount(int salMount) {
		this.salMount = salMount;
	}

	public void setBoardCount(int boardCount) {
		this.boardCount = boardCount;
	}

	public void setJengukEnrollDate(Date jengukEnrollDate) {
		this.jengukEnrollDate = jengukEnrollDate;
	}

	public void setJengukModifyDate(Date jengukModifyDate) {
		this.jengukModifyDate = jengukModifyDate;
	}

	public void setDeleteYn(String deleteYn) {
		this.deleteYn = deleteYn;
	}

	public void setApproveYn(String approveYn) {
		this.approveYn = approveYn;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public void setRefBid(int refBid) {
		this.refBid = refBid;
	}

	public void setReplyLevel(int replyLevel) {
		this.replyLevel = replyLevel;
	}

	public void setBoardCode(String boardCode) {
		this.boardCode = boardCode;
	}

	@Override
	public String toString() {
		return "Jenguk [jengukNo=" + jengukNo + ", jengukArea=" + jengukArea + ", companyName=" + companyName
				+ ", jobChoice=" + jobChoice + ", task=" + task + ", score=" + score + ", jengukTitle=" + jengukTitle
				+ ", jengukContent=" + jengukContent + ", salOpenYn=" + salOpenYn + ", salKind=" + salKind
				+ ", salMount=" + salMount + ", boardCount=" + boardCount + ", jengukEnrollDate=" + jengukEnrollDate
				+ ", jengukModifyDate=" + jengukModifyDate + ", deleteYn=" + deleteYn + ", approveYn=" + approveYn
				+ ", memberId=" + memberId + ", refBid=" + refBid + ", replyLevel=" + replyLevel + ", boardCode="
				+ boardCode + "]";
	}

	
}
