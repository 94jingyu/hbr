package com.kh.hbr.board.jengukBoard.model.vo;

import java.sql.Date;

public class Attachment implements java.io.Serializable{

	private int attachmentNo;
	private int boardNo;
	private int boardKind;
	private String originName;
	private String changeName;
	private String filePath;
	private Date uploadDate;
	private String deleteYn;
	private int fileLevel;
	
	public Attachment() {}

	public Attachment(int attachmentNo, int boardNo, int boardKind, String originName, String changeName,
			String filePath, Date uploadDate, String deleteYn, int fileLevel) {
		super();
		this.attachmentNo = attachmentNo;
		this.boardNo = boardNo;
		this.boardKind = boardKind;
		this.originName = originName;
		this.changeName = changeName;
		this.filePath = filePath;
		this.uploadDate = uploadDate;
		this.deleteYn = deleteYn;
		this.fileLevel = fileLevel;
	}

	public int getAttachmentNo() {
		return attachmentNo;
	}

	public int getBoardNo() {
		return boardNo;
	}

	public int getBoardKind() {
		return boardKind;
	}

	public String getOriginName() {
		return originName;
	}

	public String getChangeName() {
		return changeName;
	}

	public String getFilePath() {
		return filePath;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public String getDeleteYn() {
		return deleteYn;
	}

	public int getFileLevel() {
		return fileLevel;
	}

	public void setAttachmentNo(int attachmentNo) {
		this.attachmentNo = attachmentNo;
	}

	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}

	public void setBoardKind(int boardKind) {
		this.boardKind = boardKind;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public void setDeleteYn(String deleteYn) {
		this.deleteYn = deleteYn;
	}

	public void setFileLevel(int fileLevel) {
		this.fileLevel = fileLevel;
	}

	@Override
	public String toString() {
		return "Attachment [attachmentNo=" + attachmentNo + ", boardNo=" + boardNo + ", boardKind=" + boardKind
				+ ", originName=" + originName + ", changeName=" + changeName + ", filePath=" + filePath
				+ ", uploadDate=" + uploadDate + ", deleteYn=" + deleteYn + ", fileLevel=" + fileLevel + "]";
	}

	
}
