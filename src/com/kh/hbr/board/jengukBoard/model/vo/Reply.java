package com.kh.hbr.board.jengukBoard.model.vo;

import java.sql.Date;

public class Reply implements java.io.Serializable{
	private int replyNo; 
	private String replyContent;
	private Date replyEnrollDate;
	private Date replyModifyDate;
	private String replyStatus;
	private String memberId;
	private String boardCode;
	private int boardNo;
	private String dd;

	public Reply() {}

	public Reply(int replyNo, String replyContent, Date replyEnrollDate, Date replyModifyDate, String replyStatus,
			String memberId, String boardCode, int boardNo, String dd) {
		super();
		this.replyNo = replyNo;
		this.replyContent = replyContent;
		this.replyEnrollDate = replyEnrollDate;
		this.replyModifyDate = replyModifyDate;
		this.replyStatus = replyStatus;
		this.memberId = memberId;
		this.boardCode = boardCode;
		this.boardNo = boardNo;
		this.dd =dd;
	}

	
	
	
	public String getDd() {
		return dd;
	}

	public void setDd(String dd) {
		this.dd = dd;
	}

	public int getReplyNo() {
		return replyNo;
	}

	public String getReplyContent() {
		return replyContent;
	}

	public Date getReplyEnrollDate() {
		return replyEnrollDate;
	}

	public Date getReplyModifyDate() {
		return replyModifyDate;
	}

	public String getReplyStatus() {
		return replyStatus;
	}

	public String getMemberId() {
		return memberId;
	}

	public String getBoardCode() {
		return boardCode;
	}

	public int getBoardNo() {
		return boardNo;
	}

	public void setReplyNo(int replyNo) {
		this.replyNo = replyNo;
	}

	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}

	public void setReplyEnrollDate(Date replyEnrollDate) {
		this.replyEnrollDate = replyEnrollDate;
	}

	public void setReplyModifyDate(Date replyModifyDate) {
		this.replyModifyDate = replyModifyDate;
	}

	public void setReplyStatus(String replyStatus) {
		this.replyStatus = replyStatus;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public void setBoardCode(String boardCode) {
		this.boardCode = boardCode;
	}

	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}

	@Override
	public String toString() {
		return "Reply [replyNo=" + replyNo + ", replyContent=" + replyContent + ", replyEnrollDate=" + replyEnrollDate
				+ ", replyModifyDate=" + replyModifyDate + ", replyStatus=" + replyStatus + ", memberId=" + memberId
				+ ", boardCode=" + boardCode + ", boardNo=" + boardNo + ", dd=" + dd + "]";
	}

	
}
