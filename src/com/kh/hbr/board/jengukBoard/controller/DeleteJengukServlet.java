package com.kh.hbr.board.jengukBoard.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.board.jengukBoard.model.service.JengukService;
import com.kh.hbr.board.jengukBoard.model.vo.Jenguk;

@WebServlet("/deleteJenguk.tn")
public class DeleteJengukServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DeleteJengukServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String writer = request.getParameter("writer");
		int boardNo = Integer.parseInt(request.getParameter("boardNo"));
		
		Jenguk jenguk = new Jenguk();
		jenguk.setMemberId(writer);
		jenguk.setJengukNo(boardNo);
		
		int result = new JengukService().deleteJenguk(jenguk);
		
		String page = "";
		if(result > 0) {
			page = "views/common/successPage.jsp";
			request.setAttribute("successCode", "deleteJenguk");
			//System.out.println("전국보금자리자랑 삭제 성공!");
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "전국보금자리자랑 삭제 실패!");

		}
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
