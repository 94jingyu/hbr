package com.kh.hbr.board.jengukBoard.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kh.hbr.board.jengukBoard.model.service.JengukService;
import com.kh.hbr.board.jengukBoard.model.vo.Reply;

@WebServlet("/selectReply.bo")
public class SelectReplyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectReplyServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int boardNo = Integer.parseInt(request.getParameter("boardNo"));
		//System.out.println("boardNo : " + boardNo);
		
		Reply reply = new Reply();
		reply.setBoardNo(boardNo);
	
		List<Reply> replyList = new JengukService().selectReplyList(reply);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		new Gson().toJson(replyList, response.getWriter());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
