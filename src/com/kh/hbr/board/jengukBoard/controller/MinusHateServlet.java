package com.kh.hbr.board.jengukBoard.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kh.hbr.board.jengukBoard.model.service.JengukService;
import com.kh.hbr.board.jengukBoard.model.vo.Like;

@WebServlet("/minusHate")
public class MinusHateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public MinusHateServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String memberId = request.getParameter("memberId");
		int boardNo = Integer.parseInt(request.getParameter("boardNo"));
	
		Like hate = new Like();
		hate.setMemberId(memberId);
		hate.setBoardNo(boardNo);
		
		int result = new JengukService().minusHate(hate);
		
		if(result > 0) {
			Like changedCount = new JengukService().changedCount(hate);
			int hateCount = changedCount.getHateCount();
			
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			new Gson().toJson(hateCount, response.getWriter());
		}
		
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
