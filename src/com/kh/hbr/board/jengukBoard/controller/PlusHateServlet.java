package com.kh.hbr.board.jengukBoard.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kh.hbr.board.jengukBoard.model.service.JengukService;
import com.kh.hbr.board.jengukBoard.model.vo.Like;

@WebServlet("/plusHate")
public class PlusHateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public PlusHateServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String memberId = request.getParameter("memberId");
		int boardNo = Integer.parseInt(request.getParameter("boardNo"));
	
		Like hate = new Like();
		hate.setHateId(memberId);
		hate.setBoardNo(boardNo);
		
		
		int preHateCheck = new JengukService().preHateCheck(hate);

		
		// 이전에 싫어요를 누르지 않았을때 , 0 
		if(preHateCheck == 0) {
			int result = new JengukService().plusHate(hate);
			
			if(result > 0) {
				Like changedCount = new JengukService().changedCount(hate);
				int HateCount = changedCount.getHateCount();
				
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				
				new Gson().toJson(HateCount, response.getWriter());
			}

		// 이전에 좋아요를 누른적이 있을 때 	
		} else {
			// 좋아요 누른 아이디를 null로 업데이트
			int result1 = new JengukService().nullHateId(hate);
			
			if(result1 > 0) {
				// 좋아요 누른거 감소
				int result2 = new JengukService().minusHate(hate);
				
				if(result2 > 0) {
					Like changedCount = new JengukService().changedCount(hate);
					int hateCount = changedCount.getHateCount();
					
					response.setContentType("application/json");
					response.setCharacterEncoding("UTF-8");
					
					new Gson().toJson(hateCount, response.getWriter());
				}
			}
			
		}
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
