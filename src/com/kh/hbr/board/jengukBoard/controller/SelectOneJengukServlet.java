package com.kh.hbr.board.jengukBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.board.jengukBoard.model.service.JengukService;
import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.board.jengukBoard.model.vo.Jenguk;

/**
 * Servlet implementation class SelectOneJengukServlet
 */
@WebServlet("/selectOne.tn")
public class SelectOneJengukServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectOneJengukServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int num = Integer.parseInt(request.getParameter("num"));
		
		HashMap<String, Object> hmap = new JengukService().selectThumnailMap(num);
		//System.out.println("thumbnail map : " + hmap);
		
		Jenguk j = (Jenguk) hmap.get("jenguk");
		ArrayList<Attachment> fileList = (ArrayList<Attachment>) hmap.get("fileList");
		
		String page = "";
		if(hmap != null) {
			page ="views/user_Lee/E_board_JengukDetail.jsp";
			request.setAttribute("jenguk", j);
			request.setAttribute("fileList", fileList);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "전국보금자리자랑 상세보기 실패!");
		}
	
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
