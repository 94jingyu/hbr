package com.kh.hbr.board.jengukBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.board.jengukBoard.model.service.JengukService;
import com.kh.hbr.board.jengukBoard.model.vo.Jenguk;
import com.kh.hbr.board.jengukBoard.model.vo.PageInfo;

@WebServlet("/selectJenguk.tn")
public class SelectJengukListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectJengukListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int currentPage;		//현재 페이지를 표시할 변수
		int limit;				//한 페이지에 게시글이 몇 개 보여질 것인지 표시
		int maxPage;			//전체 페이지에서 가장 마지막 페이지
		int startPage;			//한 번에 표시될 페이지가 시작할 페이지
		int endPage;			//한 번에 표시될 페이지가 끝나는 페이지
		
		currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		limit = 6;
		
		JengukService js = new JengukService();
		int listCount = js.getListCount();
		
		//System.out.println("listCount : " + listCount);
		
		maxPage = (int) ((double) listCount / limit + 0.9);
		startPage = (((int)((double) currentPage / limit + 0.9)) - 1) * 10 + 1;
		
		endPage = startPage + 10 - 1;
		
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		ArrayList<HashMap<String, Object>> list = new JengukService().selectThumbnailList(pi);
		ArrayList<HashMap<String, Object>> bestList = new JengukService().selectBestList();
		//ArrayList<HashMap<String, Object>> bestList = new JengukService().selectBestListP(pi);
		//System.out.println(list);
		
		String page = "";
		if(list != null) {
			page = "views/user_Lee/E_board_JengukAllList.jsp";
			request.setAttribute("list", list);
			request.setAttribute("bestList", bestList);
			request.setAttribute("pi", pi);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "전국보금자리자랑 조회 실패!");
		}

		request.getRequestDispatcher(page).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
