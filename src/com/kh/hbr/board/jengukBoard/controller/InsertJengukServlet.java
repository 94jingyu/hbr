package com.kh.hbr.board.jengukBoard.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.kh.hbr.board.jengukBoard.model.service.JengukService;
import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.board.jengukBoard.model.vo.Jenguk;
import com.kh.hbr.board.jengukBoard.model.vo.Like;
import com.kh.hbr.common.MyFileRenamePolicy;
import com.kh.hbr.member.model.vo.Member;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

@WebServlet("/insertJenguk.tn")
public class InsertJengukServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public InsertJengukServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(ServletFileUpload.isMultipartContent(request)) {
			// 전송 파일 용량 제한: 500MB로 계산
			int maxSize = 1024 * 1024 * 500; 
		
			String root = request.getSession().getServletContext().getRealPath("/");
			//System.out.println("(InsertJengukServlet) root : " + root);
			
			// 파일 저장경로
			String savePath = root + "thumbnail_uploadFiles/";
			
			MultipartRequest multiRequest = 
					new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());

			ArrayList<String> saveFiles = new ArrayList<String>();
			ArrayList<String> originFiles = new ArrayList<String>();
			
			Enumeration<String> files = multiRequest.getFileNames();
			
			while(files.hasMoreElements()) {
				String name = files.nextElement();
				//System.out.println("(InsertJengukServlet) name : " + name);
				
				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));
			}
			//System.out.println("(InsertJengukServlet) saveFiles : " + saveFiles);
			//System.out.println("(InsertJengukServlet) originFiles : " + originFiles);
		
			
			String location1 = multiRequest.getParameter("location1"); 
			String companyName = multiRequest.getParameter("cName"); 
			String jobChoice = multiRequest.getParameter("jobChoice");
			String task = multiRequest.getParameter("task");
			String salKind = multiRequest.getParameter("sal");
			
			String sal = multiRequest.getParameter("salMount");
			sal.replaceAll(",", "");
			int salMount = Integer.parseInt(sal.replaceAll(",", ""));
			String openYn = multiRequest.getParameter("openYn");
			int score = Integer.parseInt(multiRequest.getParameter("score"));
			String title = multiRequest.getParameter("title");
			String content = multiRequest.getParameter("content");
			String attachment1 = multiRequest.getParameter("attachment1");
			String attachment2 = multiRequest.getParameter("attachment2");
			
			/*System.out.println("jengukArea : " + location1);
			System.out.println("companyName : " + companyName);
			System.out.println("jobChoice : " + jobChoice);
			System.out.println("task : " + task);
			System.out.println("salKind : " + salKind);
			System.out.println("salMount : " + salMount);
			System.out.println("openYn : " + openYn);
			System.out.println("score : " + score);
			System.out.println("title : " + title);
			System.out.println("content : " + content);
			System.out.println("attachment1 : " + attachment1);
			System.out.println("attachment2 : " + attachment2);*/
			
			Jenguk jengukBoard = new Jenguk();
			jengukBoard.setJengukArea(location1);
			jengukBoard.setCompanyName(companyName);
			jengukBoard.setJobChoice(jobChoice);
			jengukBoard.setTask(task);
			jengukBoard.setScore(score);
			jengukBoard.setJengukTitle(title);
			jengukBoard.setJengukContent(content);
			jengukBoard.setSalKind(salKind);
			jengukBoard.setSalMount(salMount);
			
			if(openYn == null) {
				jengukBoard.setSalOpenYn("N");
			} else {
				jengukBoard.setSalOpenYn("Y");
			}
			
			jengukBoard.setMemberId(((Member) request.getSession().getAttribute("loginUser")).getMemberId());
			
			ArrayList<Attachment> fileList = new ArrayList<Attachment>();
			for(int i = originFiles.size() - 1; i >= 0; i--) {
				Attachment at = new Attachment();
				at.setFilePath(savePath);
				at.setOriginName(originFiles.get(i));
				at.setChangeName(saveFiles.get(i));
				
				fileList.add(at);
			}
			
			//System.out.println("jengukBoard : " + jengukBoard);
			//System.out.println("fileList : " + fileList);
			
			int result = new JengukService().insertThumbnail(jengukBoard, fileList);
			
			if(result > 0) {
				response.sendRedirect(request.getContextPath() + "/selectJenguk.tn");
			} else {
				//실패시 저장된 사진 삭제
				for(int i = 0; i < saveFiles.size(); i++) {
					File failedFile = new File(savePath + saveFiles.get(i));
					failedFile.delete();
				}
				
				request.setAttribute("msg", "전국보금자리자랑 등록 실패!");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
			}
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
