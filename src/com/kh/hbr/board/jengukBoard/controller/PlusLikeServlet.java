package com.kh.hbr.board.jengukBoard.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kh.hbr.board.jengukBoard.model.service.JengukService;
import com.kh.hbr.board.jengukBoard.model.vo.Like;

@WebServlet("/plusLike")
public class PlusLikeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public PlusLikeServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String memberId = request.getParameter("memberId");
		int boardNo = Integer.parseInt(request.getParameter("boardNo"));
		
		Like like = new Like();
		like.setLikeId(memberId);
		like.setBoardNo(boardNo);
		
		
		int preLikeCheck = new JengukService().preLikeCheck(like);
		//System.out.println("preLikeCheck : " + preLikeCheck);
		
		
		// 이전에 좋아요를 누르지 않았을때 , 0 
		if(preLikeCheck == 0) {
			int result = new JengukService().plusLike(like);
			//System.out.println("result_preLikeCheck : " + result);
			
			if(result > 0) {
				Like changedCount = new JengukService().changedCount(like);
				int likeCount = changedCount.getLikeCount();
				
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				
				new Gson().toJson(likeCount, response.getWriter());
			}

		// 이전에 좋아요를 누른적이 있을 때 	
		} else {
			// 좋아요 누른 아이디를 null로 업데이트
			int result1 = new JengukService().nullLikeId(like);
			//System.out.println("좋아요 누른 아이디를 null로 업데이트 : " + result1);
			
			if(result1 > 0) {
				// 좋아요 누른거 감소
				int result2 = new JengukService().minusLike(like);
				//System.out.println("좋아요 감소 : " + result2);
				
				if(result2 > 0) {
					Like changedCount = new JengukService().changedCount(like);
					int likeCount = changedCount.getLikeCount();
					
					response.setContentType("application/json");
					response.setCharacterEncoding("UTF-8");
					
					new Gson().toJson(likeCount, response.getWriter());
				}
			}
			
		}
		
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
