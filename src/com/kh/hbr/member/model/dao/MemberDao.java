package com.kh.hbr.member.model.dao;

import static com.kh.hbr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.member.model.vo.Leave;
import com.kh.hbr.member.model.vo.Member;

public class MemberDao {
	
	private Properties prop = new Properties();
	
	public MemberDao() {
		String fileName = MemberDao.class.getResource("/sql/member/member-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// 로그인 확인 메소드
	public Member loginCheck(Connection con, Member requestMember) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Member loginUser = null;
		
		String query = prop.getProperty("loginSelect");  // STATUS = 'Y' 인 것은 탈퇴하지 않은 회원을 의미
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestMember.getMemberId());
			pstmt.setString(2, requestMember.getMemberPwd());
		
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				loginUser = new Member();
				loginUser.setMemberNo(rset.getInt("MEMBER_NO"));
				loginUser.setMemberId(rset.getString("MEMBER_ID"));
				loginUser.setMemberPwd(rset.getString("MEMBER_PWD"));
				loginUser.setMemberName(rset.getString("MEMBER_NAME"));
				loginUser.setPhone(rset.getString("PHONE"));
				loginUser.setEmail(rset.getString("EMAIL"));
				loginUser.setEnrollDate(rset.getDate("ENROLL_DATE"));
				loginUser.setModifyDate(rset.getDate("MODIFY_DATE"));
				loginUser.setStatus(rset.getString("STATUS"));
				loginUser.setPenaltyCount(rset.getInt("PENALTY_COUNT"));
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);

		}
		
		return loginUser;
	}
	// 기업회원 로그인
	public Business bloginCheck(Connection con, Business requestBmember) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Business loginUser = null;
		
		String query = prop.getProperty("bloginSelect");  // STATUS = 'Y' 인 것은 탈퇴하지 않은 회원을 의미
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestBmember.getBmemberId());
			pstmt.setString(2, requestBmember.getBmemberPwd());
		
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				//System.out.println("검색이 됩니다.");
				loginUser = new Business();
				loginUser.setBmemberNo(rset.getInt("BMEMBER_NO"));
				loginUser.setBmemberId(rset.getString("BMEMBER_ID"));
				loginUser.setBmemberPwd(rset.getString("BMEMBER_PWD"));
				loginUser.setCompanyName(rset.getString("COMPANY_NAME"));
				loginUser.setCompanyNo(rset.getString("COMPANY_NO"));
				loginUser.setManagerName(rset.getString("MANAGER_NAME"));
				loginUser.setManagerPhone(rset.getString("MANAGER_PHONE"));
				loginUser.setManagerEmail(rset.getString("MANAGER_EMAIL"));
				loginUser.setCeoName(rset.getString("CEO_NAME"));
				loginUser.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				loginUser.setCompanyAddress(rset.getString("COMPANY_ADDRESS"));
				loginUser.setFax(rset.getString("FAX"));
				loginUser.setHomepage(rset.getString("HOMEPAGE"));
				loginUser.setCertStatus(rset.getString("CERT_STATUS"));
				loginUser.setCertDate(rset.getDate("CERT_DATE"));
				loginUser.setBenrollDate(rset.getDate("BENROLL_DATE"));
				loginUser.setBmodifyDate(rset.getDate("BMODIFY_DATE"));
				loginUser.setBstatus(rset.getString("BSTATUS"));
				loginUser.setBpenaltyCount(rset.getInt("BPENALTY_COUNT"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return loginUser;
	}

	// 개인회원 가입용
	public int insertMember(Connection con, Member requestMember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query =prop.getProperty("insertMember");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, requestMember.getMemberId());
			pstmt.setString(2, requestMember.getMemberPwd());
			pstmt.setString(3, requestMember.getMemberName());
			pstmt.setString(4, requestMember.getPhone());
			pstmt.setString(5, requestMember.getEmail());
		
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	// 기업회원 가입용 메소드
	public int insertBmember(Connection con, Business requestBmember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query =prop.getProperty("insertBmember");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, requestBmember.getBmemberId());
			pstmt.setString(2, requestBmember.getBmemberPwd());
			pstmt.setString(3, requestBmember.getCompanyType());
			pstmt.setString(4, requestBmember.getCompanyName());
			pstmt.setString(5, requestBmember.getCompanyNo());
			pstmt.setString(6, requestBmember.getManagerName());
			pstmt.setString(7, requestBmember.getManagerPhone());
			pstmt.setString(8, requestBmember.getManagerEmail());
		
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	
	
	
	// 중복확인용 메소드
	public int idCheck(Connection con, String memberId) {
		int result = 0;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("idCheck");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, memberId);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	public int bidCheck(Connection con, String bmemberId) {
		int result = 0;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("bidCheck");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, bmemberId);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	// 패스워드 확인용 메소드(회원정보 수정)
	public Member pwdCheck(Connection con, Member requestMember) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Member loginUser = null;
		
		String query = prop.getProperty("loginSelect");  // STATUS = 'Y' 인 것은 탈퇴하지 않은 회원을 의미
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestMember.getMemberId());
			pstmt.setString(2, requestMember.getMemberPwd());
		
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				loginUser = new Member();
				loginUser.setMemberNo(rset.getInt("MEMBER_NO"));
				loginUser.setMemberId(rset.getString("MEMBER_ID"));
				loginUser.setMemberPwd(rset.getString("MEMBER_PWD"));
				loginUser.setMemberName(rset.getString("MEMBER_NAME"));
				loginUser.setPhone(rset.getString("PHONE"));
				loginUser.setEmail(rset.getString("EMAIL"));
				loginUser.setEnrollDate(rset.getDate("ENROLL_DATE"));
				loginUser.setModifyDate(rset.getDate("MODIFY_DATE"));
				loginUser.setStatus(rset.getString("STATUS"));
				loginUser.setPenaltyCount(rset.getInt("PENALTY_COUNT"));
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);

		}
		
		return loginUser;
	}

	// 개인회원 수정
	public int updateMember(Connection con, Member requestMember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateMember");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, requestMember.getMemberPwd());
			pstmt.setString(2, requestMember.getMemberName());
			pstmt.setString(3, requestMember.getPhone());
			pstmt.setString(4, requestMember.getEmail());
			pstmt.setString(5, requestMember.getMemberId());
		
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	// 수정된 회원 조회용 메소드
	public Member selectChangedMember(Connection con, Member requestMember) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Member changedMember = null;
		
		String query = prop.getProperty("changedMember"); 
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestMember.getMemberId());
			//pstmt.setString(2, requestMember.getMemberPwd());
		
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				changedMember = new Member();
				changedMember.setMemberNo(rset.getInt("MEMBER_NO"));
				changedMember.setMemberId(rset.getString("MEMBER_ID"));
				changedMember.setMemberPwd(rset.getString("MEMBER_PWD"));
				changedMember.setMemberName(rset.getString("MEMBER_NAME"));
				changedMember.setPhone(rset.getString("PHONE"));
				changedMember.setEmail(rset.getString("EMAIL"));
				changedMember.setEnrollDate(rset.getDate("ENROLL_DATE"));
				changedMember.setModifyDate(rset.getDate("MODIFY_DATE"));
				changedMember.setStatus(rset.getString("STATUS"));
				changedMember.setPenaltyCount(rset.getInt("PENALTY_COUNT"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);

		}
		return changedMember;
	}

	public int deleteMember(Connection con, Member requestMember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query =prop.getProperty("deleteMember");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, requestMember.getMemberId());
		
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	
	// 탈퇴 테이블 추가
	public int insertLeave(Connection con, Leave leaveMember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query =prop.getProperty("insertLeave");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, leaveMember.getLeaveId());
			pstmt.setString(2, leaveMember.getLeaveReason());
		
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	// 기업회원정보 수정
	public int updateBmember(Connection con, Business requestBmember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateBmember");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, requestBmember.getBmemberPwd());
			pstmt.setString(2, requestBmember.getManagerName());
			pstmt.setString(3, requestBmember.getManagerPhone());
			pstmt.setString(4, requestBmember.getManagerEmail());
			pstmt.setString(5, requestBmember.getBmemberId());
		
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	// 수정확인
	public Business selectChangedBmember(Connection con, Business requestBmember) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Business changedBmember = null;
		
		String query = prop.getProperty("changedBmember"); 
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestBmember.getBmemberId());
		
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				changedBmember = new Business();
				changedBmember.setBmemberNo(rset.getInt("BMEMBER_NO"));
				changedBmember.setBmemberId(rset.getString("BMEMBER_ID"));
				changedBmember.setBmemberPwd(rset.getString("BMEMBER_PWD"));
				changedBmember.setCompanyName(rset.getString("COMPANY_NAME"));
				changedBmember.setCompanyNo(rset.getString("COMPANY_NO"));
				changedBmember.setManagerName(rset.getString("MANAGER_NAME"));
				changedBmember.setManagerPhone(rset.getString("MANAGER_PHONE"));
				changedBmember.setManagerEmail(rset.getString("MANAGER_EMAIL"));
				changedBmember.setCeoName(rset.getString("CEO_NAME"));
				changedBmember.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				changedBmember.setCompanyAddress(rset.getString("COMPANY_ADDRESS"));
				changedBmember.setFax(rset.getString("FAX"));
				changedBmember.setHomepage(rset.getString("HOMEPAGE"));
				changedBmember.setCertStatus(rset.getString("CERT_STATUS"));
				changedBmember.setCertDate(rset.getDate("CERT_DATE"));
				changedBmember.setBenrollDate(rset.getDate("BENROLL_DATE"));
				changedBmember.setBmodifyDate(rset.getDate("BMODIFY_DATE"));
				changedBmember.setBstatus(rset.getString("BSTATUS"));
				changedBmember.setBpenaltyCount(rset.getInt("BPENALTY_COUNT"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);

		}
		return changedBmember;
	}

	// 기업회원 탈퇴
	public int deleteBmember(Connection con, Business requestBmember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query =prop.getProperty("deleteBmember");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, requestBmember.getBmemberId());
		
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
		
	}

	// 사업자정보수정
	public int updateCompany(Connection con, Business requestBmember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateCompany");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, requestBmember.getCompanyType());
			pstmt.setString(2, requestBmember.getCompanyName());
			pstmt.setString(3, requestBmember.getCompanyNo());
			pstmt.setString(4, requestBmember.getCeoName());
			pstmt.setString(5, requestBmember.getCompanyPhone());
			pstmt.setString(6, requestBmember.getCompanyAddress());
			pstmt.setString(7, requestBmember.getFax());
			pstmt.setString(8, requestBmember.getHomepage());
			pstmt.setString(9, requestBmember.getBmemberId());
		
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public Member findId(Connection con, Member requestMember) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Member loginUser = null;
		
		String query = prop.getProperty("findId");  // STATUS = 'Y' 인 것은 탈퇴하지 않은 회원을 의미
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestMember.getMemberName());
			pstmt.setString(2, requestMember.getPhone());
		
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				loginUser = new Member();
				loginUser.setMemberNo(rset.getInt("MEMBER_NO"));
				loginUser.setMemberId(rset.getString("MEMBER_ID"));
				loginUser.setMemberPwd(rset.getString("MEMBER_PWD"));
				loginUser.setMemberName(rset.getString("MEMBER_NAME"));
				loginUser.setPhone(rset.getString("PHONE"));
				loginUser.setEmail(rset.getString("EMAIL"));
				loginUser.setEnrollDate(rset.getDate("ENROLL_DATE"));
				loginUser.setModifyDate(rset.getDate("MODIFY_DATE"));
				loginUser.setStatus(rset.getString("STATUS"));
				loginUser.setPenaltyCount(rset.getInt("PENALTY_COUNT"));
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);

		}
		
		return loginUser;
	}

	public Business bfindId(Connection con, Business requestBmember) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Business loginUser = null;
		
		String query = prop.getProperty("bfindId");  // STATUS = 'Y' 인 것은 탈퇴하지 않은 회원을 의미
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestBmember.getManagerName());
			pstmt.setString(2, requestBmember.getManagerPhone());
		
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				//System.out.println("검색이 됩니다.");
				loginUser = new Business();
				loginUser.setBmemberNo(rset.getInt("BMEMBER_NO"));
				loginUser.setBmemberId(rset.getString("BMEMBER_ID"));
				loginUser.setBmemberPwd(rset.getString("BMEMBER_PWD"));
				loginUser.setCompanyName(rset.getString("COMPANY_NAME"));
				loginUser.setCompanyNo(rset.getString("COMPANY_NO"));
				loginUser.setManagerName(rset.getString("MANAGER_NAME"));
				loginUser.setManagerPhone(rset.getString("MANAGER_PHONE"));
				loginUser.setManagerEmail(rset.getString("MANAGER_EMAIL"));
				loginUser.setCeoName(rset.getString("CEO_NAME"));
				loginUser.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				loginUser.setCompanyAddress(rset.getString("COMPANY_ADDRESS"));
				loginUser.setFax(rset.getString("FAX"));
				loginUser.setHomepage(rset.getString("HOMEPAGE"));
				loginUser.setCertStatus(rset.getString("CERT_STATUS"));
				loginUser.setCertDate(rset.getDate("CERT_DATE"));
				loginUser.setBenrollDate(rset.getDate("BENROLL_DATE"));
				loginUser.setBmodifyDate(rset.getDate("BMODIFY_DATE"));
				loginUser.setBstatus(rset.getString("BSTATUS"));
				loginUser.setBpenaltyCount(rset.getInt("BPENALTY_COUNT"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return loginUser;
	}

	// 개인회원 비밀번호 수정
	public int updatePwd(Connection con, Member requestMember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updatePwd");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, requestMember.getMemberPwd());
			pstmt.setString(2, requestMember.getMemberId());
		
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	// 기업회원 새 비밀번호 
	public int bupdatePwd(Connection con, Business requestBmember) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("bupdatePwd");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, requestBmember.getBmemberPwd());
			pstmt.setString(2, requestBmember.getBmemberId());
		
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	// 아이디 리스트 조회
	public ArrayList<Member> searchId(Connection con, String searchValue) {
		ArrayList<Member> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchId");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchValue);

			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Member m = new Member();
				m.setMemberId(rset.getString("MEMBER_ID"));
				m.setMemberPwd(rset.getString("MEMBER_PWD"));
				m.setMemberName(rset.getString("MEMBER_NAME"));
				m.setPhone(rset.getString("PHONE"));
				m.setEmail(rset.getString("EMAIL"));
				m.setEnrollDate(rset.getDate("ENROLL_DATE"));
				m.setModifyDate(rset.getDate("MODIFY_DATE"));
				m.setStatus(rset.getString("STATUS"));
				m.setPenaltyCount(rset.getInt("PENALTY_COUNT"));
				
				list.add(m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
			
		return list;
	}

	// 이름으로 조회
	public ArrayList<Member> searchName(Connection con, String searchValue) {
		ArrayList<Member> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchName");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchValue);

			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Member m = new Member();
				m.setMemberId(rset.getString("MEMBER_ID"));
				m.setMemberPwd(rset.getString("MEMBER_PWD"));
				m.setMemberName(rset.getString("MEMBER_NAME"));
				m.setPhone(rset.getString("PHONE"));
				m.setEmail(rset.getString("EMAIL"));
				m.setEnrollDate(rset.getDate("ENROLL_DATE"));
				m.setModifyDate(rset.getDate("MODIFY_DATE"));
				m.setStatus(rset.getString("STATUS"));
				m.setPenaltyCount(rset.getInt("PENALTY_COUNT"));
				
				list.add(m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
			
		return list;
	}

	// 연락처로 조회
	public ArrayList<Member> searchPhone(Connection con, String searchValue) {
		ArrayList<Member> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchPhone");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchValue);

			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Member m = new Member();
				m.setMemberId(rset.getString("MEMBER_ID"));
				m.setMemberPwd(rset.getString("MEMBER_PWD"));
				m.setMemberName(rset.getString("MEMBER_NAME"));
				m.setPhone(rset.getString("PHONE"));
				m.setEmail(rset.getString("EMAIL"));
				m.setEnrollDate(rset.getDate("ENROLL_DATE"));
				m.setModifyDate(rset.getDate("MODIFY_DATE"));
				m.setStatus(rset.getString("STATUS"));
				m.setPenaltyCount(rset.getInt("PENALTY_COUNT"));
				
				list.add(m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
			
		return list;
	}

	public ArrayList<Member> selectAll(Connection con) {
		ArrayList<Member> list = null;
		Statement stmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchAll");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Member m = new Member();
				m.setMemberId(rset.getString("MEMBER_ID"));
				m.setMemberPwd(rset.getString("MEMBER_PWD"));
				m.setMemberName(rset.getString("MEMBER_NAME"));
				m.setPhone(rset.getString("PHONE"));
				m.setEmail(rset.getString("EMAIL"));
				m.setEnrollDate(rset.getDate("ENROLL_DATE"));
				m.setModifyDate(rset.getDate("MODIFY_DATE"));
				m.setStatus(rset.getString("STATUS"));
				m.setPenaltyCount(rset.getInt("PENALTY_COUNT"));
				
				list.add(m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
			
		return list;
	}

	public ArrayList<Member> statusY(Connection con) {
		ArrayList<Member> list = null;
		Statement stmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("statusY");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Member m = new Member();
				m.setMemberId(rset.getString("MEMBER_ID"));
				m.setMemberPwd(rset.getString("MEMBER_PWD"));
				m.setMemberName(rset.getString("MEMBER_NAME"));
				m.setPhone(rset.getString("PHONE"));
				m.setEmail(rset.getString("EMAIL"));
				m.setEnrollDate(rset.getDate("ENROLL_DATE"));
				m.setModifyDate(rset.getDate("MODIFY_DATE"));
				m.setStatus(rset.getString("STATUS"));
				m.setPenaltyCount(rset.getInt("PENALTY_COUNT"));
				
				list.add(m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
			
		return list;
	}

	public ArrayList<Member> statusN(Connection con) {
		ArrayList<Member> list = null;
		Statement stmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("statusN");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Member m = new Member();
				m.setMemberId(rset.getString("MEMBER_ID"));
				m.setMemberPwd(rset.getString("MEMBER_PWD"));
				m.setMemberName(rset.getString("MEMBER_NAME"));
				m.setPhone(rset.getString("PHONE"));
				m.setEmail(rset.getString("EMAIL"));
				m.setEnrollDate(rset.getDate("ENROLL_DATE"));
				m.setModifyDate(rset.getDate("MODIFY_DATE"));
				m.setStatus(rset.getString("STATUS"));
				m.setPenaltyCount(rset.getInt("PENALTY_COUNT"));
				
				list.add(m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
			
		return list;
	}

	public ArrayList<Business> bselectAll(Connection con) {
		ArrayList<Business> list = null;
		Statement stmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("bsearchAll");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Business b = new Business();
				b.setBmemberId(rset.getString("BMEMBER_ID"));
				b.setBmemberPwd(rset.getString("BMEMBER_PWD"));
				b.setCompanyName(rset.getString("COMPANY_NAME"));
				b.setCompanyNo(rset.getString("COMPANY_NO"));
				b.setManagerName(rset.getString("MANAGER_NAME"));
				b.setManagerPhone(rset.getString("MANAGER_PHONE"));
				b.setManagerEmail(rset.getString("MANAGER_EMAIL"));
				b.setCeoName(rset.getString("CEO_NAME"));
				b.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				b.setCompanyAddress(rset.getString("COMPANY_ADDRESS"));
				b.setFax(rset.getString("FAX"));
				b.setHomepage(rset.getString("HOMEPAGE"));
				b.setCertStatus(rset.getString("CERT_STATUS"));
				b.setCertDate(rset.getDate("CERT_DATE"));
				b.setBenrollDate(rset.getDate("BENROLL_DATE"));
				b.setBmodifyDate(rset.getDate("BMODIFY_DATE"));
				b.setBstatus(rset.getString("BSTATUS"));
				b.setBpenaltyCount(rset.getInt("BPENALTY_COUNT"));
				
				list.add(b);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
			
		return list;
	}

	public ArrayList<Business> bsearchId(Connection con, String searchValue) {
		ArrayList<Business> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("bsearchId");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchValue);

			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Business b = new Business();
				b.setBmemberId(rset.getString("BMEMBER_ID"));
				b.setBmemberPwd(rset.getString("BMEMBER_PWD"));
				b.setCompanyName(rset.getString("COMPANY_NAME"));
				b.setCompanyNo(rset.getString("COMPANY_NO"));
				b.setManagerName(rset.getString("MANAGER_NAME"));
				b.setManagerPhone(rset.getString("MANAGER_PHONE"));
				b.setManagerEmail(rset.getString("MANAGER_EMAIL"));
				b.setCeoName(rset.getString("CEO_NAME"));
				b.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				b.setCompanyAddress(rset.getString("COMPANY_ADDRESS"));
				b.setFax(rset.getString("FAX"));
				b.setHomepage(rset.getString("HOMEPAGE"));
				b.setCertStatus(rset.getString("CERT_STATUS"));
				b.setCertDate(rset.getDate("CERT_DATE"));
				b.setBenrollDate(rset.getDate("BENROLL_DATE"));
				b.setBmodifyDate(rset.getDate("BMODIFY_DATE"));
				b.setBstatus(rset.getString("BSTATUS"));
				b.setBpenaltyCount(rset.getInt("BPENALTY_COUNT"));;
				
				list.add(b);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
			
		return list;
	}

	public ArrayList<Business> bsearchName(Connection con, String searchValue) {
		ArrayList<Business> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("bsearchName");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchValue);

			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Business b = new Business();
				b.setBmemberId(rset.getString("BMEMBER_ID"));
				b.setBmemberPwd(rset.getString("BMEMBER_PWD"));
				b.setCompanyName(rset.getString("COMPANY_NAME"));
				b.setCompanyNo(rset.getString("COMPANY_NO"));
				b.setManagerName(rset.getString("MANAGER_NAME"));
				b.setManagerPhone(rset.getString("MANAGER_PHONE"));
				b.setManagerEmail(rset.getString("MANAGER_EMAIL"));
				b.setCeoName(rset.getString("CEO_NAME"));
				b.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				b.setCompanyAddress(rset.getString("COMPANY_ADDRESS"));
				b.setFax(rset.getString("FAX"));
				b.setHomepage(rset.getString("HOMEPAGE"));
				b.setCertStatus(rset.getString("CERT_STATUS"));
				b.setCertDate(rset.getDate("CERT_DATE"));
				b.setBenrollDate(rset.getDate("BENROLL_DATE"));
				b.setBmodifyDate(rset.getDate("BMODIFY_DATE"));
				b.setBstatus(rset.getString("BSTATUS"));
				b.setBpenaltyCount(rset.getInt("BPENALTY_COUNT"));;
				
				list.add(b);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
			
		return list;
	}

	public ArrayList<Business> bsearchCname(Connection con, String searchValue) {
		ArrayList<Business> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("bsearchCname");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchValue);

			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Business b = new Business();
				b.setBmemberId(rset.getString("BMEMBER_ID"));
				b.setBmemberPwd(rset.getString("BMEMBER_PWD"));
				b.setCompanyName(rset.getString("COMPANY_NAME"));
				b.setCompanyNo(rset.getString("COMPANY_NO"));
				b.setManagerName(rset.getString("MANAGER_NAME"));
				b.setManagerPhone(rset.getString("MANAGER_PHONE"));
				b.setManagerEmail(rset.getString("MANAGER_EMAIL"));
				b.setCeoName(rset.getString("CEO_NAME"));
				b.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				b.setCompanyAddress(rset.getString("COMPANY_ADDRESS"));
				b.setFax(rset.getString("FAX"));
				b.setHomepage(rset.getString("HOMEPAGE"));
				b.setCertStatus(rset.getString("CERT_STATUS"));
				b.setCertDate(rset.getDate("CERT_DATE"));
				b.setBenrollDate(rset.getDate("BENROLL_DATE"));
				b.setBmodifyDate(rset.getDate("BMODIFY_DATE"));
				b.setBstatus(rset.getString("BSTATUS"));
				b.setBpenaltyCount(rset.getInt("BPENALTY_COUNT"));;
				
				list.add(b);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
			
		return list;
	}

	public ArrayList<Business> bsearchCert(Connection con, String searchValue) {
		ArrayList<Business> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("bsearchCert");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchValue);

			rset = pstmt.executeQuery();
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Business b = new Business();
				b.setBmemberId(rset.getString("BMEMBER_ID"));
				b.setBmemberPwd(rset.getString("BMEMBER_PWD"));
				b.setCompanyName(rset.getString("COMPANY_NAME"));
				b.setCompanyNo(rset.getString("COMPANY_NO"));
				b.setManagerName(rset.getString("MANAGER_NAME"));
				b.setManagerPhone(rset.getString("MANAGER_PHONE"));
				b.setManagerEmail(rset.getString("MANAGER_EMAIL"));
				b.setCeoName(rset.getString("CEO_NAME"));
				b.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				b.setCompanyAddress(rset.getString("COMPANY_ADDRESS"));
				b.setFax(rset.getString("FAX"));
				b.setHomepage(rset.getString("HOMEPAGE"));
				b.setCertStatus(rset.getString("CERT_STATUS"));
				b.setCertDate(rset.getDate("CERT_DATE"));
				b.setBenrollDate(rset.getDate("BENROLL_DATE"));
				b.setBmodifyDate(rset.getDate("BMODIFY_DATE"));
				b.setBstatus(rset.getString("BSTATUS"));
				b.setBpenaltyCount(rset.getInt("BPENALTY_COUNT"));;
				
				list.add(b);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
			
		return list;
	}

	public ArrayList<Business> bstatusY(Connection con) {
		ArrayList<Business> list = null;
		Statement stmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("bstatusY");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Business b = new Business();
				b.setBmemberId(rset.getString("BMEMBER_ID"));
				b.setBmemberPwd(rset.getString("BMEMBER_PWD"));
				b.setCompanyName(rset.getString("COMPANY_NAME"));
				b.setCompanyNo(rset.getString("COMPANY_NO"));
				b.setManagerName(rset.getString("MANAGER_NAME"));
				b.setManagerPhone(rset.getString("MANAGER_PHONE"));
				b.setManagerEmail(rset.getString("MANAGER_EMAIL"));
				b.setCeoName(rset.getString("CEO_NAME"));
				b.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				b.setCompanyAddress(rset.getString("COMPANY_ADDRESS"));
				b.setFax(rset.getString("FAX"));
				b.setHomepage(rset.getString("HOMEPAGE"));
				b.setCertStatus(rset.getString("CERT_STATUS"));
				b.setCertDate(rset.getDate("CERT_DATE"));
				b.setBenrollDate(rset.getDate("BENROLL_DATE"));
				b.setBmodifyDate(rset.getDate("BMODIFY_DATE"));
				b.setBstatus(rset.getString("BSTATUS"));
				b.setBpenaltyCount(rset.getInt("BPENALTY_COUNT"));;
				
				list.add(b);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
			
		return list;
	}

	public ArrayList<Business> bstatusN(Connection con) {
		ArrayList<Business> list = null;
		Statement stmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("bstatusN");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<>();
			
			while(rset.next()) {
				Business b = new Business();
				b.setBmemberId(rset.getString("BMEMBER_ID"));
				b.setBmemberPwd(rset.getString("BMEMBER_PWD"));
				b.setCompanyName(rset.getString("COMPANY_NAME"));
				b.setCompanyNo(rset.getString("COMPANY_NO"));
				b.setManagerName(rset.getString("MANAGER_NAME"));
				b.setManagerPhone(rset.getString("MANAGER_PHONE"));
				b.setManagerEmail(rset.getString("MANAGER_EMAIL"));
				b.setCeoName(rset.getString("CEO_NAME"));
				b.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				b.setCompanyAddress(rset.getString("COMPANY_ADDRESS"));
				b.setFax(rset.getString("FAX"));
				b.setHomepage(rset.getString("HOMEPAGE"));
				b.setCertStatus(rset.getString("CERT_STATUS"));
				b.setCertDate(rset.getDate("CERT_DATE"));
				b.setBenrollDate(rset.getDate("BENROLL_DATE"));
				b.setBmodifyDate(rset.getDate("BMODIFY_DATE"));
				b.setBstatus(rset.getString("BSTATUS"));
				b.setBpenaltyCount(rset.getInt("BPENALTY_COUNT"));;
				
				list.add(b);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
			
		return list;
	}

	//관 - 회원가입한 회원의 회원번호를 조회
	public int loadbMemberNo(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("loadbMemberNo");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				result = rset.getInt("MAX(BMEMBER_NO)");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return result;
	}

	public int insertBasicPurchase(Connection con, int no) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertBasicPurchase");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, no);
			result = pstmt.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int loadPurchaseNo(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int result = 0;
		
		String query = prop.getProperty("loadPurchaseNo");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				result = rset.getInt("MAX(PURCHASE_NO)");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return result;
	}

	public int insertBasicItem(Connection con, int no, int purchaseNo) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertBasicItem");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, no);
			pstmt.setInt(2, purchaseNo);
			result = pstmt.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}


}














