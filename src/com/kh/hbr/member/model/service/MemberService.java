package com.kh.hbr.member.model.service;

import static com.kh.hbr.common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.ArrayList;

import com.kh.hbr.member.model.dao.MemberDao;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.member.model.vo.Leave;
import com.kh.hbr.member.model.vo.Member;

public class MemberService {
	// 로그인 확인 메소드
	public Member loginCheck(Member requestMember) {
		Connection con = getConnection(); 
		//System.out.println(con);
		
		Member loginUser = new MemberDao().loginCheck(con, requestMember);
		
		close(con);
		
		return loginUser;
	}
	
	// 기업회원 로그인
	public Business bloginCheck(Business requestBmember) {
		Connection con = getConnection(); 
		
		Business loginUser = new MemberDao().bloginCheck(con, requestBmember);
		
		close(con);
		
		return loginUser;
	}

	public int insertMember(Member requestMember) {
		Connection con = getConnection();
		int result = new MemberDao().insertMember(con, requestMember);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}
	
	// 기업회원 회원가입
	public int insertBmember(Business requestBmember) {
		Connection con = getConnection();
		int result = new MemberDao().insertBmember(con, requestBmember);
		
		if(result > 0) {
			commit(con);
			int no = new MemberDao().loadbMemberNo(con);
			
			//조회가 되면 구매테이블에 값 insert
			int insertResult = new MemberDao().insertBasicPurchase(con, no);
			
			if(insertResult > 0) {
				commit(con);
				int purchaseNo = new MemberDao().loadPurchaseNo(con);
				
				int itemResult = new MemberDao().insertBasicItem(con, no, purchaseNo);
				
				if(itemResult > 0) {
					commit(con);
				}else {
					rollback(con);
				}
				
			}else {
				rollback(con);
			}
			
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}

	// 개인회원 중복확인
	public int idCheck(String memberId) {
		Connection con = getConnection();
	
		int result = new MemberDao().idCheck(con, memberId);
		
		close(con);
		
		return result;
	}
	
	// 기업회원 중복확인
	public int bidCheck(String bmemberId) {
		Connection con = getConnection();
		
		int result = new MemberDao().bidCheck(con, bmemberId);
		
		close(con);
		
		return result;
	}

	// 회원정보수정 패스워드 확인
	public Member pwdCheck(Member requestMember) {
		Connection con = getConnection(); 
		
		Member loginUser = new MemberDao().pwdCheck(con, requestMember);

		close(con);
		
		return loginUser;
	}
	
	// 개인회원 수정
	public int updateMember(Member requestMember) {
		Connection con = getConnection();
		int result = new MemberDao().updateMember(con, requestMember);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}
	public Member changedMember(Member requestMember) {
		Connection con = getConnection();
		Member changedMember = new MemberDao().selectChangedMember(con, requestMember);
		
		close(con);
		
		return changedMember;
	}

	public int deleteMember(Member requestMember) {
		Connection con = getConnection();
		int result = new MemberDao().deleteMember(con, requestMember);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}

	public int insertLeave(Leave leaveMember) {
		Connection con = getConnection();
		int result = new MemberDao().insertLeave(con, leaveMember);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}

	public int updateBmember(Business requestBmember) {
		Connection con = getConnection();
		int result = new MemberDao().updateBmember(con, requestBmember);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}

	public Business changedBmember(Business requestBmember) {
		Connection con = getConnection();
		Business changedBmember = new MemberDao().selectChangedBmember(con, requestBmember);
		
		close(con);
		
		return changedBmember;
	}

	public int deleteBmember(Business requestBmember) {
		Connection con = getConnection();
		int result = new MemberDao().deleteBmember(con, requestBmember);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}

	public int updateCompany(Business requestBmember) {
		Connection con = getConnection();
		int result = new MemberDao().updateCompany(con, requestBmember);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}

	public Member findId(Member requestMember) {
		Connection con = getConnection(); 
		
		Member loginUser = new MemberDao().findId(con, requestMember);
		
		close(con);
		
		return loginUser;
	}


	public Business bfindId(Business requestBmember) {
		Connection con = getConnection(); 
		
		Business loginUser = new MemberDao().bfindId(con, requestBmember);
		
		close(con);
		
		return loginUser;
	}

	// 비밀번호 찾기 -> 비밀번호 수정
	public int updatePwd(Member requestMember) {
		Connection con = getConnection();
		int result = new MemberDao().updatePwd(con, requestMember);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}

	public int bupdatePwd(Business requestBmember) {
		Connection con = getConnection();
		int result = new MemberDao().bupdatePwd(con, requestBmember);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		} 
		
		close(con);
		
		return result;
	}

	public ArrayList<Member> searchMember(String searchCondition, String searchValue) {
		Connection con = getConnection();
		ArrayList<Member> list = null;
		
		if(searchCondition.equals("findId")) {
			list = new MemberDao().searchId(con, searchValue);
		} else if(searchCondition.equals("findName")) {
			list = new MemberDao().searchName(con, searchValue);
		} else if(searchCondition.equals("findPhone")) {
			list = new MemberDao().searchPhone(con, searchValue);
		}
		
		close(con);

		return list;
	}

	// 회원정보 전체 조회용 메소드
	public ArrayList<Member> searchAll() {
		Connection con = getConnection();
		
		ArrayList<Member> list = new MemberDao().selectAll(con);
		
		close(con);
		
		return list;
	}

	public ArrayList<Member> statusY() {
		Connection con = getConnection();
		
		ArrayList<Member> list = new MemberDao().statusY(con);
		
		close(con);
		
		return list;
	}

	public ArrayList<Member> statusN() {
		Connection con = getConnection();
		
		ArrayList<Member> list = new MemberDao().statusN(con);
		
		close(con);
		
		return list;
	}

	public ArrayList<Business> bsearchAll() {
		Connection con = getConnection();
		
		ArrayList<Business> list = new MemberDao().bselectAll(con);
		
		close(con);
		
		return list;
	}

	public ArrayList<Business> bsearchMember(String searchCondition, String searchValue) {
		Connection con = getConnection();
		ArrayList<Business> list = null;
		
		if(searchCondition.equals("findId")) {
			list = new MemberDao().bsearchId(con, searchValue);
		} else if(searchCondition.equals("findName")) {
			list = new MemberDao().bsearchName(con, searchValue);
		} else if(searchCondition.equals("findCname")) {
			list = new MemberDao().bsearchCname(con, searchValue);
		} else if(searchCondition.equals("findCert")) {
			list = new MemberDao().bsearchCert(con, searchValue);
		}
		
		close(con);

		return list;
	}

	public ArrayList<Business> bstatusY() {
		Connection con = getConnection();
		
		ArrayList<Business> list = new MemberDao().bstatusY(con);
		
		close(con);
		
		return list;
	}

	public ArrayList<Business> bstatusN() {
		Connection con = getConnection();
		
		ArrayList<Business> list = new MemberDao().bstatusN(con);
		
		close(con);
		
		return list;
	}

}
