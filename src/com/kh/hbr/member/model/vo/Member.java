package com.kh.hbr.member.model.vo;

import java.sql.Date;

public class Member implements java.io.Serializable {
	private int memberNo;		// 개인회원번호
	private String memberId;  	// 아이디
	private String memberPwd;  	// 비밀번호
	private String memberName;	// 회원이름
	private String phone;		// 휴대폰
	private String email;		// 이메일
	private Date enrollDate;	// 가입일
	private Date modifyDate;	// 수정일
	private String status;		// 회원상태
	private int penaltyCount;	// 제재횟수
	
	
	public Member() {}
	
	public Member(int memberNo, String memberId, String memberPwd, String memberName, String phone, String email,
			Date enrollDate, Date modifyDate, String status, int penaltyCount) {
		super();
		this.memberNo = memberNo;
		this.memberId = memberId;
		this.memberPwd = memberPwd;
		this.memberName = memberName;
		this.phone = phone;
		this.email = email;
		this.enrollDate = enrollDate;
		this.modifyDate = modifyDate;
		this.status = status;
		this.penaltyCount = penaltyCount;
	}


	public int getMemberNo() {
		return memberNo;
	}
	public String getMemberId() {
		return memberId;
	}
	public String getMemberPwd() {
		return memberPwd;
	}
	public String getMemberName() {
		return memberName;
	}
	public String getPhone() {
		return phone;
	}
	public String getEmail() {
		return email;
	}
	public Date getEnrollDate() {
		return enrollDate;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public String getStatus() {
		return status;
	}
	public int getPenaltyCount() {
		return penaltyCount;
	}
	public void setMemberNo(int memberNo) {
		this.memberNo = memberNo;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public void setMemberPwd(String memberPwd) {
		this.memberPwd = memberPwd;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setPenaltyCount(int penaltyCount) {
		this.penaltyCount = penaltyCount;
	}
	
	@Override
	public String toString() {
		return "Member [memberNo=" + memberNo + ", memberId=" + memberId + ", memberPwd=" + memberPwd + ", memberName="
				+ memberName + ", phone=" + phone + ", email=" + email + ", enrollDate=" + enrollDate + ", modifyDate="
				+ modifyDate + ", status=" + status + ", penaltyCount=" + penaltyCount + "]";
	}
	
}
