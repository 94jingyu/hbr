package com.kh.hbr.member.model.vo;

import java.sql.Date;

public class Business implements java.io.Serializable {
	private int bmemberNo;			// 기업회원번호
	private String bmemberId;  		// 아이디
	private String bmemberPwd;  	// 비밀번호
	private String companyType; 	// 기업구분
	private String companyName; 	// 기업명
	private String companyNo; 		// 사업자등록번호
	private String managerName; 	// 담당자이름
	private String managerPhone; 	// 담당자연락처
	private String managerEmail; 	// 담당자이메일
	private String ceoName; 		// 대표자이름
	private String companyPhone; 	// 회사연락처
	private String companyAddress; 	// 회사주소
	private String fax; 			// 팩스번호
	private String homepage; 		// 홈페이지주소
	private String certStatus; 		// 기업인증상태
	private Date certDate; 			// 기업인증일
	private Date benrollDate; 		// 가입일
	private Date bmodifyDate; 		// 수정일
	private String bstatus; 		// 회원상태
	private int bpenaltyCount; 		// 제재횟수
	
	public Business() {}

	public Business(int bmemberNo, String bmemberId, String bmemberPwd, String companyType, String companyName,
			String companyNo, String managerName, String managerPhone, String managerEmail, String ceoName,
			String companyPhone, String companyAddress, String fax, String homepage, String certStatus, Date certDate,
			Date benrollDate, Date bmodifyDate, String bstatus, int bpenaltyCount) {
		super();
		this.bmemberNo = bmemberNo;
		this.bmemberId = bmemberId;
		this.bmemberPwd = bmemberPwd;
		this.companyType = companyType;
		this.companyName = companyName;
		this.companyNo = companyNo;
		this.managerName = managerName;
		this.managerPhone = managerPhone;
		this.managerEmail = managerEmail;
		this.ceoName = ceoName;
		this.companyPhone = companyPhone;
		this.companyAddress = companyAddress;
		this.fax = fax;
		this.homepage = homepage;
		this.certStatus = certStatus;
		this.certDate = certDate;
		this.benrollDate = benrollDate;
		this.bmodifyDate = bmodifyDate;
		this.bstatus = bstatus;
		this.bpenaltyCount = bpenaltyCount;
	}

	public int getBmemberNo() {
		return bmemberNo;
	}

	public String getBmemberId() {
		return bmemberId;
	}

	public String getBmemberPwd() {
		return bmemberPwd;
	}

	public String getCompanyType() {
		return companyType;
	}

	public String getCompanyName() {
		return companyName;
	}

	public String getCompanyNo() {
		return companyNo;
	}

	public String getManagerName() {
		return managerName;
	}

	public String getManagerPhone() {
		return managerPhone;
	}

	public String getManagerEmail() {
		return managerEmail;
	}

	public String getCeoName() {
		return ceoName;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public String getFax() {
		return fax;
	}

	public String getHomepage() {
		return homepage;
	}

	public String getCertStatus() {
		return certStatus;
	}

	public Date getCertDate() {
		return certDate;
	}

	public Date getBenrollDate() {
		return benrollDate;
	}

	public Date getBmodifyDate() {
		return bmodifyDate;
	}

	public String getBstatus() {
		return bstatus;
	}

	public int getBpenaltyCount() {
		return bpenaltyCount;
	}

	public void setBmemberNo(int bmemberNo) {
		this.bmemberNo = bmemberNo;
	}

	public void setBmemberId(String bmemberId) {
		this.bmemberId = bmemberId;
	}

	public void setBmemberPwd(String bmemberPwd) {
		this.bmemberPwd = bmemberPwd;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setCompanyNo(String companyNo) {
		this.companyNo = companyNo;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public void setManagerPhone(String managerPhone) {
		this.managerPhone = managerPhone;
	}

	public void setManagerEmail(String managerEmail) {
		this.managerEmail = managerEmail;
	}

	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public void setCertStatus(String certStatus) {
		this.certStatus = certStatus;
	}

	public void setCertDate(Date certDate) {
		this.certDate = certDate;
	}

	public void setBenrollDate(Date benrollDate) {
		this.benrollDate = benrollDate;
	}

	public void setBmodifyDate(Date bmodifyDate) {
		this.bmodifyDate = bmodifyDate;
	}

	public void setBstatus(String bstatus) {
		this.bstatus = bstatus;
	}

	public void setBpenaltyCount(int bpenaltyCount) {
		this.bpenaltyCount = bpenaltyCount;
	}

	@Override
	public String toString() {
		return "Business [bmemberNo=" + bmemberNo + ", bmemberId=" + bmemberId + ", bmemberPwd=" + bmemberPwd
				+ ", companyType=" + companyType + ", companyName=" + companyName + ", companyNo=" + companyNo
				+ ", managerName=" + managerName + ", managerPhone=" + managerPhone + ", managerEmail=" + managerEmail
				+ ", ceoName=" + ceoName + ", companyPhone=" + companyPhone + ", companyAddress=" + companyAddress
				+ ", fax=" + fax + ", homepage=" + homepage + ", certStatus=" + certStatus + ", certDate=" + certDate
				+ ", benrollDate=" + benrollDate + ", bmodifyDate=" + bmodifyDate + ", bstatus=" + bstatus
				+ ", bpenaltyCount=" + bpenaltyCount + "]";
	}
	
	
	
}
