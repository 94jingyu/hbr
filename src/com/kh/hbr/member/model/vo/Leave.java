package com.kh.hbr.member.model.vo;

import java.sql.Date;

public class Leave implements java.io.Serializable {
	private int leaveNo;  			// 탈퇴회원번호
	private String leaveId;  		// 탈퇴회원아이디
	private Date leaveDate;  		// 탈퇴일
	private String leaveReason;  	// 탈퇴사유
		
	public Leave() {}

	public Leave(int leaveNo, String leaveId, Date leaveDate, String leaveReason) {
		super();
		this.leaveNo = leaveNo;
		this.leaveId = leaveId;
		this.leaveDate = leaveDate;
		this.leaveReason = leaveReason;
	}

	public int getLeaveNo() {
		return leaveNo;
	}

	public String getLeaveId() {
		return leaveId;
	}

	public Date getLeaveDate() {
		return leaveDate;
	}

	public String getLeaveReason() {
		return leaveReason;
	}

	public void setLeaveNo(int leaveNo) {
		this.leaveNo = leaveNo;
	}

	public void setLeaveId(String leaveId) {
		this.leaveId = leaveId;
	}

	public void setLeaveDate(Date leaveDate) {
		this.leaveDate = leaveDate;
	}

	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}

	@Override
	public String toString() {
		return "Leave [leaveNo=" + leaveNo + ", leaveId=" + leaveId + ", leaveDate=" + leaveDate + ", leaveReason="
				+ leaveReason + "]";
	}
	
	
	
}
