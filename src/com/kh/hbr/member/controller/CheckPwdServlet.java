package com.kh.hbr.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.hbr.member.model.service.MemberService;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.member.model.vo.Member;

@WebServlet("/checkPwd.me")
public class CheckPwdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CheckPwdServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int loginType = Integer.parseInt(request.getParameter("loginType"));
		
		// 개인회원일 때 
		if(loginType == 0) {
			String memberId = request.getParameter("memberId");
			String memberPwd = request.getParameter("memberPwd");

			Member requestMember = new Member();
			requestMember.setMemberId(memberId);
			requestMember.setMemberPwd(memberPwd);
			
			Member loginUser = new MemberService().loginCheck(requestMember);
			//System.out.println("loginUser : " + loginUser);
			
			if(loginUser != null) {
				HttpSession session = request.getSession();
				session.setAttribute("loginUser", loginUser);
				session.setAttribute("loginType", 0);
				
				response.sendRedirect("views/common/E_UpdateInfoPage3.jsp");
			} else {
				//request.setAttribute("msg", "비밀번호가 틀렸습니다!!");
				request.setAttribute("failCode", "failPassword");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
			}
		// 기업회원일 때 	
		} else if(loginType == 1) {
			String bmemberId = request.getParameter("memberId");
			String bmemberPwd = request.getParameter("memberPwd");
			//System.out.println(bmemberId);
			//System.out.println(bmemberPwd);
			
			Business requestBmember = new Business();
			requestBmember.setBmemberId(bmemberId);
			requestBmember.setBmemberPwd(bmemberPwd);
			
			Business bloginUser = new MemberService().bloginCheck(requestBmember);
			//System.out.println("bloginUser : " + bloginUser);
			
			if(bloginUser != null) {
				HttpSession session = request.getSession();
				session.setAttribute("loginUser", bloginUser);
				session.setAttribute("loginType", 1);
			
				response.sendRedirect("views/common/E_business_UpdateInfoPage3.jsp");
			} else {
				//request.setAttribute("msg", "비밀번호가 틀렸습니다!!");
				request.setAttribute("failCode", "failPassword");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
				
			}

		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
