package com.kh.hbr.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.hbr.member.model.service.MemberService;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.member.model.vo.Member;

@WebServlet("/login.me")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoginServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int loginType = Integer.parseInt(request.getParameter("loginType"));
		//System.out.println("loginType : " + loginType);
		
		// 개인회원일 때 
		if(loginType == 0) {
			String memberId = request.getParameter("memberId");
			String memberPwd = request.getParameter("memberPwd");
			
			/*// 첫번째 자리 빼고 나머지 문자를 *로 치환하는 코드
			String str = "";
			for(int i = 0; i < memberId.length()-1; i++) {
				str += "*";
			}
			String changeStr = memberId.substring(0,1) + str;
			
			System.out.println(memberId);
			System.out.println(changeStr);*/
			
			Member requestMember = new Member();
			requestMember.setMemberId(memberId);
			requestMember.setMemberPwd(memberPwd);
			
			Member loginUser = new MemberService().loginCheck(requestMember);
			//System.out.println("loginUser : " + loginUser);
			
			if(loginUser != null) {
				HttpSession session = request.getSession();
				session.setAttribute("loginUser", loginUser);
				session.setAttribute("loginType", 0);
				
				if(loginUser.getMemberId().equals("admin")) {
					response.sendRedirect("views/guide/admin_mainPage.jsp");
				} else {
					response.sendRedirect("views/guide/user_mainPage.jsp");
				}
				
			} else {
				request.setAttribute("msg", "로그인 실패!!");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
			}
			
			
		// 기업회원일 때 	
		} else if(loginType == 1) {
			String bmemberId = request.getParameter("memberId");
			String bmemberPwd = request.getParameter("memberPwd");
			//System.out.println(bmemberId);
			//System.out.println(bmemberPwd);
			
			Business requestBmember = new Business();
			requestBmember.setBmemberId(bmemberId);
			requestBmember.setBmemberPwd(bmemberPwd);
			
			Business bloginUser = new MemberService().bloginCheck(requestBmember);
			//System.out.println("bloginUser : " + bloginUser);
			
			if(bloginUser != null) {
				HttpSession session = request.getSession();
				session.setAttribute("loginUser", bloginUser);
				session.setAttribute("loginType", 1);
			
				response.sendRedirect("companyMypage.bs");
			} else {
				request.setAttribute("msg", "로그인 실패!!");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
			}

		}

		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
