package com.kh.hbr.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.hbr.member.model.service.MemberService;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.member.model.vo.Member;

@WebServlet("/updateCompany.me")
public class CompanyUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CompanyUpdateServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String bmemberId = request.getParameter("memberId");
		String companyType = request.getParameter("companyType");
		String companyName = request.getParameter("companyName");
		String l1 = request.getParameter("licensee1");
		String l2 = request.getParameter("licensee2");
		String l3 = request.getParameter("licensee3");
		String companyNo = l1 + "-" + l2 + "-" + l3;
		String CeoName = request.getParameter("CeoName");
		String tel1 = request.getParameter("tel1");
		String tel2 = request.getParameter("tel2");
		String companyPhone = tel1 + tel2;
		String address1 = request.getParameter("address1");
		String address2 = request.getParameter("address2");
		String companyAddress = address1 + " " +address2;
		String f1 = request.getParameter("fax1");
		String f2 = request.getParameter("fax2");
		String fax = f1 + f2;
		String homepage = request.getParameter("homepage");
		
		Business requestBmember = new Business();
		requestBmember.setBmemberId(bmemberId);
		requestBmember.setCompanyType(companyType);
		requestBmember.setCompanyName(companyName);
		requestBmember.setCompanyNo(companyNo);
		requestBmember.setCeoName(CeoName);
		requestBmember.setCompanyPhone(companyPhone);
		requestBmember.setCompanyAddress(companyAddress);
		requestBmember.setFax(fax);
		requestBmember.setHomepage(homepage);

		//System.out.println("update request Company : " + requestBmember);
		
		int result = new MemberService().updateCompany(requestBmember);
		int companyYN = Integer.parseInt(request.getParameter("companyYN"));
		
		String page = "";
		if(result > 0) {
			Business changedBmember = new MemberService().changedBmember(requestBmember);
			
			if(changedBmember != null) {
				HttpSession session = request.getSession();
				session.setAttribute("loginUser", changedBmember);
				session.setAttribute("companyYN", 9);
				
				page = "views/common/E_business_UpdateInfoPage4.jsp";
				response.sendRedirect(page);	
			}
		
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "사업자정보수정 실패!");

			request.getRequestDispatcher(page).forward(request, response);
		}
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
