package com.kh.hbr.member.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.member.model.service.MemberService;

@WebServlet("/idCheck.me")
public class idCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public idCheckServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String BmemberId = request.getParameter("memberId");
//		System.out.println(BmemberId);
//		System.out.println(BmemberId);
//		System.out.println(BmemberId);

		int memberCh = new MemberService().idCheck(BmemberId);
		int bmemberCh = new MemberService().bidCheck(BmemberId);

		int result = memberCh + bmemberCh;
//		System.out.println(result);
//		System.out.println(result);
//		System.out.println(result);
		
		
		String text = "";
		if(result == 1) {
			text = "fail";
		} else {
			text = "success";
		}
		
		
		PrintWriter out = response.getWriter();
		out.print(text);
		out.flush();
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
