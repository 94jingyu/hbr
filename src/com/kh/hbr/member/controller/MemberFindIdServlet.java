package com.kh.hbr.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.hbr.member.model.service.MemberService;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.member.model.vo.Member;

@WebServlet("/findId.me")
public class MemberFindIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public MemberFindIdServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int loginType = Integer.parseInt(request.getParameter("loginType"));
		
		// 개인회원일 때 
		if(loginType == 0) {
			String memberName = request.getParameter("memberName");
			String tel1 = request.getParameter("tel1");
			String tel2 = request.getParameter("tel2");
			String phone = tel1 + tel2;
			
			Member requestMember = new Member();
			requestMember.setMemberName(memberName);
			requestMember.setPhone(phone);
			//System.out.println(requestMember);
			
			Member loginUser = new MemberService().findId(requestMember);

			if(loginUser != null) {
				HttpSession session = request.getSession();
				session.setAttribute("loginUser", loginUser);
				session.setAttribute("loginType", 0);
				
				response.sendRedirect("views/common/E_Find_Id2.jsp");
			} else {
				request.setAttribute("msg", "아이디 찾기 실패!!");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
			}
			
		// 기업회원일 때 	
		} else if(loginType == 1) {
			String managerName = request.getParameter("memberName");
			String tel1 = request.getParameter("tel1");
			String tel2 = request.getParameter("tel2");
			String managerPhone = tel1 + tel2;
			
			Business requestBmember = new Business();
			requestBmember.setManagerName(managerName);
			requestBmember.setManagerPhone(managerPhone);
			
			Business bloginUser = new MemberService().bfindId(requestBmember);
			//System.out.println("bloginUser : " + bloginUser);
			
			if(bloginUser != null) {
				HttpSession session = request.getSession();
				session.setAttribute("loginUser", bloginUser);
				session.setAttribute("loginType", 1);
			
				response.sendRedirect("views/common/E_Find_Id2.jsp");
			} else {
				request.setAttribute("msg", "아이디 찾기 실패!!");
				request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
			}

		}
		
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
