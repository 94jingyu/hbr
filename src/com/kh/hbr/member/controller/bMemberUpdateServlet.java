package com.kh.hbr.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.hbr.member.model.service.MemberService;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.member.model.vo.Member;

@WebServlet("/updateBmember.me")
public class bMemberUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public bMemberUpdateServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String bmemberId = request.getParameter("bmemberId");
		String bmemberPwd = request.getParameter("bmemberPwd");
		String managerName = request.getParameter("managerName");
		String tel1 = request.getParameter("tel1");
		String tel2 = request.getParameter("tel2");
		String managerPhone = tel1 + tel2;
		String e = request.getParameter("email1");
		String mail = request.getParameter("email2");
		String managerEmail = e + "@" + mail;
		
		Business requestBmember = new Business();
		requestBmember.setBmemberId(bmemberId);
		requestBmember.setBmemberPwd(bmemberPwd);
		requestBmember.setManagerName(managerName);
		requestBmember.setManagerPhone(managerPhone);
		requestBmember.setManagerEmail(managerEmail);
		
//		System.out.println("update request bMember : " + requestBmember);
		
		int result = new MemberService().updateBmember(requestBmember);
		int companyYN = Integer.parseInt(request.getParameter("companyYN"));
		
		String page = "";
		if(result > 0) {
			Business changedBmember = new MemberService().changedBmember(requestBmember);
			
			if(changedBmember != null) {
				HttpSession session = request.getSession();
				session.setAttribute("loginUser", changedBmember);
				session.setAttribute("companyYN", 0);
				
				page = "views/common/E_business_UpdateInfoPage4.jsp";
				response.sendRedirect(page);	
			}
		
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "회원수정 실패!");

			request.getRequestDispatcher(page).forward(request, response);
		}

	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
