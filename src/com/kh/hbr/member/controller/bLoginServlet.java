package com.kh.hbr.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.hbr.member.model.service.MemberService;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.member.model.vo.Member;

@WebServlet("/blogin.me")
public class bLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public bLoginServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String bmemberId = request.getParameter("bmemberId");
		String bmemberPwd = request.getParameter("bmemberPwd");
		
//		System.out.println("(bLoginServlet) bmemberId : " + bmemberId);
//		System.out.println("(bLoginServlet) bmemberPwd : " + bmemberPwd);
		
		Business requestMember = new Business();
		requestMember.setBmemberId(bmemberId);
		requestMember.setBmemberPwd(bmemberPwd);
		
		Business loginUser = new MemberService().bloginCheck(requestMember);
//		System.out.println("(bLoginServlet) bloginUser : " + loginUser);
		
		if(loginUser != null) {
			HttpSession session = request.getSession();
			session.setAttribute("loginUser", loginUser);
		
			response.sendRedirect("views/guide/user_mainPage.jsp");
		} else {
			request.setAttribute("msg", "로그인 실패!!");
			request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		}
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
