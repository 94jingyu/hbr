package com.kh.hbr.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.hbr.member.model.service.MemberService;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.member.model.vo.Member;

@WebServlet("/updatePwd.me")
public class MemberUpdatePwdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public MemberUpdatePwdServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int loginType = Integer.parseInt(request.getParameter("loginType"));
		
		// 개인회원일 때 
		if(loginType == 0) {
			String memberId = request.getParameter("memberId");
			String newPwd = request.getParameter("newPwd");
			
			Member requestMember = new Member();
			requestMember.setMemberId(memberId);
			requestMember.setMemberPwd(newPwd);
			
			int result = new MemberService().updatePwd(requestMember);
			
			
			String page = "";
			if(result > 0) {
				request.getSession().invalidate();
				response.sendRedirect("views/guide/user_mainPage.jsp");				
			
			} else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "비밀번호 변경 실패!");

				request.getRequestDispatcher(page).forward(request, response);
			}
			
		// 기업회원일 때 	
		} else if(loginType == 1) {
			String bmemberId = request.getParameter("bmemberId");
			String bnewPwd = request.getParameter("newPwd");
			
			Business requestBmember = new Business();
			requestBmember.setBmemberId(bmemberId);
			requestBmember.setBmemberPwd(bnewPwd);
			
			int result = new MemberService().bupdatePwd(requestBmember);
			//System.out.println("bloginUser : " + bloginUser);
			
			String page = "";
			if(result > 0) {
				request.getSession().invalidate();
				response.sendRedirect("views/guide/user_mainPage.jsp");				
			
			} else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "비밀번호 변경 실패!");

				request.getRequestDispatcher(page).forward(request, response);
			}
		}
		

	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
