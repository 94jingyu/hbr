package com.kh.hbr.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.hbr.member.model.service.MemberService;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.member.model.vo.Leave;
import com.kh.hbr.member.model.vo.Member;

@WebServlet("/deleteBmember.me")
public class bMemberDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public bMemberDeleteServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String bmemberId = request.getParameter("bmemberId");
		String[] reasonArr = request.getParameterValues("reason");
		String reason = String.join(",", reasonArr);
		String leaveReason = "";
		
		// 마지막 글자 자르기
		if (reason != null && reason.length() > 0 && reason.charAt(reason.length() - 1) == ',') {
			leaveReason = reason.substring(0, reason.length() - 1);
	    }

				
		Business requestBmember = new Business();
		requestBmember.setBmemberId(bmemberId);
		
		// 탈퇴 테이블
		Leave leaveBmember = new Leave();
		leaveBmember.setLeaveId(bmemberId);
		leaveBmember.setLeaveReason(leaveReason);
		
		int result = new MemberService().deleteBmember(requestBmember);
		int result2 = new MemberService().insertLeave(leaveBmember);
		
		String page = "";
		if(result > 0 && result2 > 0) {
			page = "views/common/successPage.jsp";
			request.setAttribute("successCode", "deleteMember");
			System.out.println("회원탈퇴 성공!");
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "회원탈퇴 실패!");

		}
		request.getRequestDispatcher(page).forward(request, response);
		
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
