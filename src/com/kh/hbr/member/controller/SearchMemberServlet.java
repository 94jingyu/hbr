package com.kh.hbr.member.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.member.model.service.MemberService;
import com.kh.hbr.member.model.vo.Member;

@WebServlet("/searchMember.me")
public class SearchMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SearchMemberServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String searchCondition = request.getParameter("searchCondition");
		String memberStatus = request.getParameter("memberStatus");
		String searchValue = "";
		ArrayList<Member> list = null;
		
//		System.out.println(searchCondition);
//		System.out.println(searchCondition);
		
		if(searchCondition.equals("findAll")) {
			list = new MemberService().searchAll();
		} else if(searchCondition.equals("findId")) {
			searchValue = request.getParameter("searchValue");
			list = new MemberService().searchMember(searchCondition, searchValue);
		} else if(searchCondition.equals("findName")) {
			searchValue = request.getParameter("searchValue");
			list = new MemberService().searchMember(searchCondition, searchValue);
		} else if(searchCondition.equals("findPhone")) {
			searchValue = request.getParameter("searchValue");
			list = new MemberService().searchMember(searchCondition, searchValue);
		} 
			
		if(memberStatus.equals("Y")) {
			list = new MemberService().statusY();
		} else if(memberStatus.equals("N")) {
			list = new MemberService().statusN();
		}
		
//		System.out.println("search list : " + list);	
		
		String page = "";
		if(list != null) {
			request.setAttribute("list", list);
			request.setAttribute("searchCondition", searchCondition);
			
			page = "views/common/E_admin_userList.jsp";
		} else {
			request.setAttribute("msg", "회원 검색에 실패하셨습니다!!");
			
			page = "views/common/errorPage.jsp";
		}
		
		request.getRequestDispatcher(page).forward(request, response);
		
}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
