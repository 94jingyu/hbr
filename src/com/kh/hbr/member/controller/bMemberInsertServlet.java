package com.kh.hbr.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.member.model.service.MemberService;
import com.kh.hbr.member.model.vo.Business;

@WebServlet("/insertBmember.me")
public class bMemberInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public bMemberInsertServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String bmemberId = request.getParameter("bmemberId");
		String bmemberPwd = request.getParameter("bmemberPwd");
		String companyType = request.getParameter("companyType");
		String companyName = request.getParameter("companyName");
		String l1 = request.getParameter("licensee1");
		String l2 = request.getParameter("licensee2");
		String l3 = request.getParameter("licensee3");
		String companyNo = l1 + "-" + l2 + "-" + l3;
		String managerName = request.getParameter("managerName");
		String tel1 = request.getParameter("tel1");
		String tel2 = request.getParameter("tel2");
		String managerPhone = tel1 + tel2;
		String e = request.getParameter("email1");
		String mail = request.getParameter("email2");
		String managerEmail = e + "@" + mail;
		
//		System.out.println(bmemberId);
//		System.out.println(bmemberPwd);
//		System.out.println(companyType);
		
		Business requestBmember = new Business();
		requestBmember.setBmemberId(bmemberId);
		requestBmember.setBmemberPwd(bmemberPwd);
		requestBmember.setCompanyType(companyType);
		requestBmember.setCompanyName(companyName);
		requestBmember.setCompanyNo(companyNo);
		requestBmember.setManagerName(managerName);
		requestBmember.setManagerPhone(managerPhone);
		requestBmember.setManagerEmail(managerEmail);
		
//		System.out.println("insert request bMember : " + requestBmember);
		
		int result = new MemberService().insertBmember(requestBmember);
		
		String page = "";
		if(result > 0) {
			page = "views/common/successPage.jsp";
			request.setAttribute("successCode", "insertBmember");
		
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "회원가입 실패!");
		}
	
		request.getRequestDispatcher(page).forward(request, response);
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
