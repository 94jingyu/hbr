package com.kh.hbr.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.member.model.service.MemberService;
import com.kh.hbr.member.model.vo.Member;

@WebServlet("/insertMember.me")
public class MemberInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public MemberInsertServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		String memberId = request.getParameter("memberId");
		String memberPwd = request.getParameter("memberPwd");
		String memberName = request.getParameter("memberName");
		String tel1 = request.getParameter("tel1");
		String tel2 = request.getParameter("tel2");
		String phone = tel1 + tel2;
		String e = request.getParameter("email1");
		String mail = request.getParameter("email2");
		String email = e + "@" + mail;
		
		Member requestMember = new Member();
		requestMember.setMemberId(memberId);
		requestMember.setMemberPwd(memberPwd);
		requestMember.setMemberName(memberName);
		requestMember.setPhone(phone);
		requestMember.setEmail(email);
		
//		System.out.println("insert request Member : " + requestMember);
		
		int result = new MemberService().insertMember(requestMember);
		
		String page = "";
		if(result > 0) {
			page = "views/common/successPage.jsp";
			request.setAttribute("successCode", "insertMember");
		
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "회원가입 실패!");
		}
	
		request.getRequestDispatcher(page).forward(request, response);
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
