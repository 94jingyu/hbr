/*package com.kh.hbr.resume.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.resume.model.service.ResumeService;
import com.kh.hbr.resume.model.vo.Recruit1;

*//**
 * Servlet implementation class LimitList1Servlet
 *//*
@WebServlet("/limit1.list")
public class LimitList1Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    *//**
     * @see HttpServlet#HttpServlet()
     *//*
    public LimitList1Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	*//**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 *//*
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		ArrayList<Recruit1> slist = new ResumeService().SearchLimit1();
		System.out.println("slist11 : " + slist);
		String page="";
		if(slist!=null) { 
        	page="/h/views/user_JinHyeok/resumeLimit.jsp";
          
          request.setAttribute("limitview",slist);
          
        }else {
           page="views/common/errorPage.jsp";
           request.setAttribute("msg","페이지불러오기 실패");
        }
        request.getRequestDispatcher(page).forward(request, response);
     }

	*//**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 *//*
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
*/