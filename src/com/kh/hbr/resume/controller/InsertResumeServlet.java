package com.kh.hbr.resume.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;


import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.common.MyFileRenamePolicy;
import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.resume.model.service.ResumeService;
import com.kh.hbr.resume.model.vo.Career;
import com.kh.hbr.resume.model.vo.Education;
import com.kh.hbr.resume.model.vo.Resume;
import com.oreilly.servlet.MultipartRequest;

@WebServlet("/insert.res")
public class InsertResumeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public InsertResumeServlet() {
        super();
    } 

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		if(ServletFileUpload.isMultipartContent(request)) {
			// 전송 파일 용량 제한: 500MB로 계산
			int maxSize = 125 * 163 * 500; 
			
			String root = request.getSession().getServletContext().getRealPath("/");
			
			//System.out.println("(InsertRegiserServlet) root : " + root);
			
			// 파일 저장경로
			String savePath = root + "thumbnail_uploadFiles/";
			
			MultipartRequest multiRequest = 
					new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());
			
			ArrayList<String> saveFiles = new ArrayList<String>();
			ArrayList<String> originFiles = new ArrayList<String>();
			
			Enumeration<String> files = multiRequest.getFileNames();
			
			while(files.hasMoreElements()) {
				String name = files.nextElement();
				//System.out.println("(InsertRegiserServlet) name : " + name);
				
				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));
			}
			
		
			
			
			
			
		String resume_title = multiRequest.getParameter("resume_title");
		
		String birth_date = multiRequest.getParameter("birth_date");
		String remail = multiRequest.getParameter("remail");
		String rphone = multiRequest.getParameter("rphone");
		String raddress1 = multiRequest.getParameter("raddress1");
		String raddress2 = multiRequest.getParameter("raddress2");
		String raddress = raddress1 + "" + raddress2;
		String edu_choice = multiRequest.getParameter("schradio");
		String sch_name1[] = multiRequest.getParameterValues("sch_name"); 
		String sch_name="";
		for(int i = 0; i < sch_name1.length; i++) {
			if(!sch_name1[i].equals("")) {
				sch_name = sch_name1[i];
			}
		}
		
		
		/*String graduate_type1 = multiRequest.getParameter("graduate_type1"); 
		String graduate_type2 = multiRequest.getParameter("graduate_type2"); 
		String graduate_type3 = multiRequest.getParameter("graduate_type3"); 
		String graduate_type4 = multiRequest.getParameter("graduate_type4"); 
		
		

		
		String graduate_type = graduate_type1  + graduate_type2 + graduate_type3 + graduate_type4;
		
		System.out.println(graduate_type);*/
		String graduate_type1[] = multiRequest.getParameterValues("graduate_type"); 
		String graduate_type="";
		for(int i = 0; i < graduate_type1.length; i++) {
			if(!graduate_type1[i].equals("")) {
				graduate_type = graduate_type1[i];
			}
		}
		
		String uni_graduate_type = multiRequest.getParameter("uni_graduate_type");
		
		String major = multiRequest.getParameter("major");
		
		//String sch_enroll_date1 = multiRequest.getParameter("sch_enroll_date");
		//String sch_graduate_date1 = multiRequest.getParameter("sch_graduate_date");
		//System.out.println(sch_graduate_date1);
		String sch_enroll_date1[] = multiRequest.getParameterValues("sch_enroll_date");
		String realDate1 = "";
		for(int i = 0; i < sch_enroll_date1.length; i++) {
			if(!sch_enroll_date1[i].equals("")) {
				realDate1 = sch_enroll_date1[i];
				//System.out.println("realDate " + i + " : " + realDate);
			}
		}
		String sch_graduate_date1[] = multiRequest.getParameterValues("sch_graduate_date");
		String realDate2 = "";
		for(int i = 0; i < sch_graduate_date1.length; i++) {
			if(!sch_graduate_date1[i].equals("")) {
				realDate2 = sch_graduate_date1[i];
				//System.out.println("realDate " + i + " : " + realDate);
			}
		}
		
		
		String uni_type = multiRequest.getParameter("uni_type");
		String uni_name = multiRequest.getParameter("uni_name");
		String uni_area = multiRequest.getParameter("uni_area");
		String uni_major = multiRequest.getParameter("uni_major");
		String uni_major_name = multiRequest.getParameter("uni_major_name");
		String uni_score1 = multiRequest.getParameter("uni_score");
		if(uni_score1.equals("")) {
			uni_score1 = "0";
			//System.out.println("realDate " + i + " : " + realDate);
		}
	
		String uni_score_choice1 = multiRequest.getParameter("uni_score_choice");
		
		if(uni_score_choice1==null) {
			uni_score_choice1 = "0";
			//System.out.println("realDate " + i + " : " + realDate);
		}
		
		 
		String career_yn = multiRequest.getParameter("careerradio");
		String company_name = multiRequest.getParameter("company_name");
		
		String company_in_year1 = multiRequest.getParameter("company_in_year");
		
		String company_out_date1 = multiRequest.getParameter("company_out_date");
		
		String job_level = multiRequest.getParameter("job_level");
		String career_year1 = multiRequest.getParameter("career_year");
		if(career_year1.equals("")) {
			career_year1 = "0";
			//System.out.println("realDate " + i + " : " + realDate);
		}
		String uni_enroll_date1 = multiRequest.getParameter("uni_enroll_date");
		String uni_graduate_date1 = multiRequest.getParameter("uni_graduate_date");
		String career_kind = multiRequest.getParameter("career_kind");
		String career_area = multiRequest.getParameter("career_area");
		String career_dept = multiRequest.getParameter("career_dept");
		String task = multiRequest.getParameter("task");
		
		String introduce_title = multiRequest.getParameter("introduce_title");
		String introduce_content = multiRequest.getParameter("introduce_content");
		String interest_area = multiRequest.getParameter("interest_area");
		
		//관심근무 직종1
		String interest_job1 = multiRequest.getParameter("interest_job1");
		//관심 근무 직종2
		String interest_job2 = multiRequest.getParameter("interest_job2");
		//관심 근무 직종3
		String interest_job3 = multiRequest.getParameter("interest_job3");
		String apply_field = interest_job1 + "," + interest_job2 + "," + interest_job3;
		//이력서 공개여부
		
		String resume_open_yn = multiRequest.getParameter("resume_open_yn");
		String gender = multiRequest.getParameter("gender");
		/*System.out.println(career_year1);*/
		
		/*System.out.println(resume_title);
		System.out.println(rname);
		System.out.println(birth_date);
		System.out.println(remail);
		System.out.println(rphone);
		System.out.println(raddress);
		System.out.println(company_out_date);*/
		
		System.out.println("uni :"  + uni_enroll_date1);
		System.out.println("uni2 :"  + uni_graduate_date1);
		
		java.sql.Date sch_enroll_date = null;
		if(realDate1 != "") {
			
			sch_enroll_date = java.sql.Date.valueOf(realDate1);
		} else {
			sch_enroll_date = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		System.out.println("schenroll: "  + sch_enroll_date);
		
		java.sql.Date sch_graduate_date = null;
		if(realDate2 != "") {
			
			sch_graduate_date = java.sql.Date.valueOf(realDate2);
		} else {
			sch_graduate_date = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		
		System.out.println("");
		
		
		java.sql.Date uni_enroll_date = null;
		if(!uni_enroll_date1.equals("")) {
			
			uni_enroll_date = java.sql.Date.valueOf(uni_enroll_date1);
		} else {
			uni_enroll_date = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		System.out.println("unienroll : " + uni_enroll_date);
		
		
		java.sql.Date uni_graduate_date = null;
		if(!uni_graduate_date1.equals("")) {
			
			uni_graduate_date = java.sql.Date.valueOf(uni_graduate_date1);
		} else {
			uni_graduate_date = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		System.out.println("uni_graduate_date : " +  uni_graduate_date);
		
		
		java.sql.Date company_in_year = null;
		if(!company_in_year1.equals("")) {
			
			company_in_year = java.sql.Date.valueOf(company_in_year1);
		} else {
			company_in_year = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		java.sql.Date company_out_date = null;
		if(!company_out_date1.equals("")) {
			
			company_out_date = java.sql.Date.valueOf(company_out_date1);
		} else {
			company_out_date = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		
		int rmember_no = ((Member) request.getSession().getAttribute("loginUser")).getMemberNo();
		int career_year=Integer.parseInt(career_year1);
		double uni_score = Double.parseDouble(uni_score1);
		double uni_score_choice = Double.parseDouble(uni_score_choice1);
			
		//Double.parseDouble
		
		/*System.out.println("resume_title : " + resume_title);
		System.out.println("birth_date : " + birth_date);
		System.out.println("remail : " + remail);
		System.out.println("rphone : " + rphone);
		System.out.println("raddress : " + raddress);
		System.out.println("edu_choice : " + edu_choice);
		System.out.println("sch_name : " + sch_name);
		System.out.println("major : " + major);
		System.out.println("sch_enroll_date : " + sch_enroll_date);
		System.out.println("sch_graduate_date : " + sch_graduate_date);
		System.out.println("career_yn : " + career_yn);
		System.out.println("company_name" + company_name);
		System.out.println("company_in_year : " + company_in_year);
		System.out.println("company_out_datae : " + company_out_date);
		System.out.println("job_level : " + job_level);
		System.out.println("career_year : " + career_year);
		System.out.println("career_kind : " + career_kind);
		System.out.println("career_area : " + career_area);    
		System.out.println("career_dept : " + career_dept);
		System.out.println("task : " + task);
		System.out.println("introduce_title : " + introduce_title);
		System.out.println("introduce_content : " + introduce_content);
		System.out.println("interest_area : " + interest_area);
		System.out.println("interest_job : " + apply_field);
		System.out.println("resume_open_yn : " + resume_open_yn);
		System.out.println("writer : " + rmember_no);
		System.out.println("score : "  + uni_score1);
		System.out.println("uni_score_choice1 : " + uni_score_choice1);
		System.out.println("resume_open_yn : " + resume_open_yn );*/
		Resume resume = new Resume();
		resume.setResume_title(resume_title);
		resume.setBirth_date(birth_date);
		resume.setApply_field(apply_field);
		resume.setResume_open_yn(resume_open_yn);
		resume.setRemail(remail);
		resume.setCareer_yn(career_yn);
		resume.setInterestarea(interest_area); //_ 추가하기
		resume.setIntroducecontent(introduce_content); // _추가
		resume.setIntroducetitle(introduce_title);
		resume.setRaddress(raddress);
		resume.setResume_title(resume_title);
		resume.setRmember_no(rmember_no);
		resume.setGender(gender);
		/*resume.setRtitle(rtitle);
		resume.setRbirth(rbirth);
		resume.setIntroduceTitle(introduceTitle);
		resume.setIntroduceContent(introduceContent);*/
		
		
		
		
		Education edu = new Education();
		
		edu.setEdu_choice(edu_choice);
		edu.setSchool_enroll_date(sch_enroll_date); //sch school 둘중 하나 통일
		edu.setSchool_graduate_date(sch_graduate_date);
		edu.setSchool_name(sch_name);
		edu.setUni_area(uni_area);
		edu.setUni_major(uni_major);
		edu.setUni_major_name(uni_major_name);
		edu.setUni_score(uni_score);
		edu.setUni_score_choice(uni_score_choice);
		edu.setUni_type(uni_type);
		edu.setMajor(major);
		edu.setUni_name(uni_name);
		edu.setGraduate_type(graduate_type);
		edu.setUni_graduate_type(uni_graduate_type);
		edu.setUni_enroll_date(uni_enroll_date);
		edu.setUni_graduate_date(uni_graduate_date);
		
		Career career = new Career();
		career.setCareer_area(career_area);
		career.setCareer_dept(career_dept);
		career.setCareer_kind(career_kind);
		career.setCareer_year(career_year);
		career.setCompany_name(company_name);
		career.setCompany_in_year(company_in_year);
		career.setCompany_out_date(company_out_date);
		career.setJob_level(job_level);
		career.setTask(task); 
		
		ArrayList<Attachment> fileList = new ArrayList<Attachment>();
		for(int i = originFiles.size() - 1; i >= 0; i--) {
			Attachment at = new Attachment();
			at.setFilePath(savePath);
			at.setOriginName(originFiles.get(i));
			at.setChangeName(saveFiles.get(i));
			
			fileList.add(at);
		}
		
		System.out.println(fileList);
		
		int result = 0;
		
		
		result = new ResumeService().insertResume(resume , career , edu, fileList);
		
		
		String page ="";
		if(result>0 ) {
			page="user.my";
			
			request.getSession().setAttribute("resume", resume);
			request.getSession().setAttribute("career", career);
			request.getSession().setAttribute("edu", edu);
			response.sendRedirect(page);
		
		}else {
			for(int i = 0; i < saveFiles.size(); i++) {
				File failedFile = new File(savePath + saveFiles.get(i));
				failedFile.delete();
			}
			
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg","테이블에 정보 삽입 실패!");
			request.getRequestDispatcher(page).forward(request,response);
		}
	}
}
	
	/** 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
