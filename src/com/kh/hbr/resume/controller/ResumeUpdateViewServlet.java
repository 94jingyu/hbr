package com.kh.hbr.resume.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.resume.model.service.ResumeService;
import com.kh.hbr.resume.model.vo.Career;
import com.kh.hbr.resume.model.vo.Education;
import com.kh.hbr.resume.model.vo.Resume;

/**
 * Servlet implementation class ResumeUpdateServlet
 */
@WebServlet("/res.UpView")
public class ResumeUpdateViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResumeUpdateViewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
 
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int num = Integer.parseInt(request.getParameter("num")); 
		HashMap<String,Object> hmap = new ResumeService().resumeUpdateView(num);
		/*System.out.println("hmap : " + hmap);*/
		Member member = (Member) hmap.get("member");
		Resume resume = (Resume) hmap.get("resume");
		Education edu = (Education) hmap.get("edu");
		Career career = (Career) hmap.get("career");
		/*System.out.println("num : " + num);
		System.out.println("hmap : " + hmap);*/  
		String page = "";
		if(hmap != null) {
			page = "views/user_JinHyeok/resumeUpdate.jsp";
			request.getSession().setAttribute("resume", resume);
			request.getSession().setAttribute("member" ,member);
			request.getSession().setAttribute("edu" , edu);
			request.getSession().setAttribute("career" , career);
			response.sendRedirect(page);
			
		}else {
			page = "view/common/errorPage.jsp";
			request.setAttribute("msg", "게시글 상세 보기 실패!");
			
			request.getRequestDispatcher(page).forward(request, response);
		}
		 
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
