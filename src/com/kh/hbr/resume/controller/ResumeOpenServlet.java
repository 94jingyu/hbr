package com.kh.hbr.resume.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.apply.model.service.ApplyService;
import com.kh.hbr.apply.model.vo.Apply;
import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.resume.model.service.ResumeService;
import com.kh.hbr.resume.model.vo.ResumeInterview;

/**
 * Servlet implementation class ResumeOpenServlet
 */
@WebServlet("/res.open")
public class ResumeOpenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResumeOpenServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Member loginUser=(Member)request.getSession().getAttribute("loginUser");
		ArrayList<ResumeInterview> ilist = new ResumeService().resumeinterview(loginUser.getMemberNo());
		System.out.println("ilistServlet : " + ilist);
		String page="";
        if(ilist!=null) { 
        	page="/h/views/user_JinHyeok/myresumeBusiness.jsp";
          request.getSession().setAttribute("resumeopen",ilist);
          response.sendRedirect(page);
        }else {
           page="views/common/errorPage.jsp";
           request.setAttribute("msg","페이지불러오기 실패");
           request.getRequestDispatcher(page).forward(request, response);
        }
     }
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
