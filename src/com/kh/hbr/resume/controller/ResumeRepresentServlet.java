package com.kh.hbr.resume.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.resume.model.service.ResumeService;
import com.kh.hbr.resume.model.vo.Resume;

/**
 * Servlet implementation class ResumeRepresentServlet
 */
@WebServlet("/represent.res")
public class ResumeRepresentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResumeRepresentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
 
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int num = Integer.parseInt(request.getParameter("num"));
		
		
		int rmemberno = ((Member) request.getSession().getAttribute("loginUser")).getMemberNo();
		Resume resume = new Resume();
		resume.setRmember_no(rmemberno);
		resume.setResume_no(num);
		
		/*System.out.println("RepresentNum : " + num);*/
		int result = new ResumeService().representResume(resume);
		
		String page ="";
		if(result>0) { 
			page="res.list";
			request.getRequestDispatcher(page).forward(request, response);
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg","테이블에 정보 삽입 실패!");
			request.getRequestDispatcher(page).forward(request,response);
		}
	
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
