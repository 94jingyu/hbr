package com.kh.hbr.resume.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.resume.model.service.ResumeService;
import com.kh.hbr.resume.model.vo.Resume;

/**
 * Servlet implementation class ResumeListServlet
 */
@WebServlet("/res.list")
public class ResumeListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResumeListServlet() {
        super();
        // TODO Auto-generated constructor stub
    } 
  
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        
        //로그인구현하면
        Member loginUser=(Member)request.getSession().getAttribute("loginUser");
        /*System.out.println("loginUser:"  + loginUser);*/
        
        /*System.out.println("loginUserNo:"+loginUser.getMemberNo());*/
        ArrayList<Resume> list=new ResumeService().selectList(loginUser.getMemberNo());
        
        /*System.out.println("서블릿 list:"+list);*/
        
        String page="";
        if(list!=null) { 
        	page="/h/views/user_JinHyeok/resumeManage2.jsp";
          request.getSession().setAttribute("resumeManage2",list);
          response.sendRedirect(page);
        }else {
           page="views/common/errorPage.jsp";
           request.setAttribute("msg","이력서목록조회 실패");
           request.getRequestDispatcher(page).forward(request, response);
        }
     }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
