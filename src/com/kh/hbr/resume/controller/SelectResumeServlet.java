package com.kh.hbr.resume.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.kh.hbr.resume.model.service.ResumeService;
import com.kh.hbr.resume.model.service.ResumeService_tw;
import com.kh.hbr.resume.model.vo.Resume;

@WebServlet("/selectResume.rs")
public class SelectResumeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectResumeServlet() {  
        super();
    } 
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int memberNo = Integer.parseInt(request.getParameter("memberNo"));
//		System.out.println("memberNo : " + memberNo);
		
		ArrayList<Resume> list = new ResumeService_tw().selectResume(memberNo);
		System.out.println(list);
		
		String page = "";
		
		JSONArray result = new JSONArray();
		JSONObject json = null;
		
		for(Resume rs : list) {
			json = new JSONObject();
			json.put("resumeNo", rs.getResume_no());
			json.put("resumeTitle", URLEncoder.encode(rs.getResume_title(), "UTF-8")); //이력서 3개
			
			result.add(json);
		}
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		
		PrintWriter out = response.getWriter();
		out.print(result.toJSONString());
		out.flush();
		out.close();
		

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
