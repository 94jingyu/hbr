package com.kh.hbr.resume.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.apply.model.service.ApplyService;
import com.kh.hbr.apply.model.vo.Apply;
import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.resume.model.service.ResumeService;
import com.kh.hbr.resume.model.vo.Resume;

/**
 * Servlet implementation class UserMyPageServlet
 */
@WebServlet("/user.my")
public class UserMyPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */ 
    public UserMyPageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Member loginUser=(Member)request.getSession().getAttribute("loginUser");
        /*System.out.println("loginUser:"  + loginUser);
        
        System.out.println("loginUserNo:"+loginUser.getMemberNo());*/
        
        HashMap<String,Object>hmap=new ResumeService().UserMyPage(loginUser.getMemberNo());
		
		ArrayList<Apply> alist = new ApplyService().selectList(loginUser.getMemberNo());
		
		ArrayList<Resume> list= (ArrayList<Resume>) hmap.get("resumeList");
		Member member = (Member) hmap.get("resumeMember");
		
		
		/*Board b = (Board) hmap.get("board");
		ArrayList<Attachment> fileList = (ArrayList<Attachment>) hmap.get("fileList");*/
       
          
        /*System.out.println("서블릿 list:"+list);*/
        
        String page="";
        if(list!=null) {
        	page="/h/views/user_JinHyeok/userMypage.jsp";
          request.getSession().setAttribute("resumeList",list);
          request.getSession().setAttribute("resumeMember", member);
          request.getSession().setAttribute("alist", alist);
          response.sendRedirect(page);
        }else {
           page="views/common/errorPage.jsp";
           request.setAttribute("msg","이력서목록조회 실패");
           request.getRequestDispatcher(page).forward(request, response);
        }
     }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
