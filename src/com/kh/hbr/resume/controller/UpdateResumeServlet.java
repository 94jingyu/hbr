package com.kh.hbr.resume.controller;

import java.io.IOException;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.resume.model.service.ResumeService;
import com.kh.hbr.resume.model.vo.Career;
import com.kh.hbr.resume.model.vo.Education;
import com.kh.hbr.resume.model.vo.Resume;

/**
 * Servlet implementation class UpdateResumeServlet
 */
@WebServlet("/update.res")
public class UpdateResumeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */ 
    public UpdateResumeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int num = Integer.parseInt(request.getParameter("num"));
		/*System.out.println("이력서 업데이트 : " + num );*/
		String resume_title = request.getParameter("resume_title");
		String birth_date = request.getParameter("birth_date");
		String remail = request.getParameter("remail");
		String rphone = request.getParameter("rphone");
		String raddress1 = request.getParameter("raddress1");
		String raddress2 = request.getParameter("raddress2");
		String raddress = raddress1 + "-" + raddress2;
		String edu_choice = request.getParameter("schradio");
		String sch_name1[] = request.getParameterValues("sch_name");
		String sch_name="";
		for(int i = 0; i < sch_name1.length; i++) {
			if(!sch_name1[i].equals("")) {
				/*System.out.println("sch_!" + sch_name1[i]);*/
				sch_name = sch_name1[i];
			}
		}
		String major = request.getParameter("major");
		
		//String sch_enroll_date1 = request.getParameter("sch_enroll_date");
		//String sch_graduate_date1 = request.getParameter("sch_graduate_date");
		//System.out.println(sch_graduate_date1);
		String sch_enroll_date1[] = request.getParameterValues("sch_enroll_date");
		String realDate1 = "";
		for(int i = 0; i < sch_enroll_date1.length; i++) {
			if(!sch_enroll_date1[i].equals("")) {
				realDate1 = sch_enroll_date1[i];
				//System.out.println("realDate " + i + " : " + realDate);
			}
		}
		String sch_graduate_date1[] = request.getParameterValues("sch_graduate_date");
		String realDate2 = "";
		for(int i = 0; i < sch_graduate_date1.length; i++) {
			if(!sch_graduate_date1[i].equals("")) {
				realDate2 = sch_graduate_date1[i];
				//System.out.println("realDate " + i + " : " + realDate);
			}
		}
		String uni_type = request.getParameter("uni_type");
		String uni_name = request.getParameter("uni_name");
		String uni_area = request.getParameter("uni_area");
		String uni_major = request.getParameter("uni_major");
		String uni_major_name = request.getParameter("uni_major_name");
		String uni_score1 = request.getParameter("uni_score");
		String uni_score_choice1 = request.getParameter("uni_score_choice");
		
		String gender = request.getParameter("gender");
		
		 
		String career_yn = request.getParameter("careerradio");
		String company_name = request.getParameter("company_name");
		
		String company_in_year1 = request.getParameter("company_in_year");
		
		String company_out_date1 = request.getParameter("company_out_date");
		
		String job_level = request.getParameter("job_level");
		String career_year1 = request.getParameter("career_year");
		String career_kind = request.getParameter("career_kind");
		String career_area = request.getParameter("career_area");
		String career_dept = request.getParameter("career_dept");
		String task = request.getParameter("task");
		
		String introduce_title = request.getParameter("introduce_title");
		String introduce_content = request.getParameter("introduce_content");
		String interest_area = request.getParameter("interest_area");
		
		//관심근무 직종1
		String interest_job1 = request.getParameter("interest_job1");
		//관심 근무 직종2
		String interest_job2 = request.getParameter("interest_job2");
		//관심 근무 직종3
		String interest_job3 = request.getParameter("interest_job3");
		String apply_field = interest_job1 + "," + interest_job2 + "," + interest_job3;
		//이력서 공개여부
		String resume_open_yn = request.getParameter("resume_open_yn");
		
		String uni_graduate_type = request.getParameter("uni_graduate_type");
		String uni_enroll_date1 = request.getParameter("uni_enroll_date");
		String uni_graduate_date1 = request.getParameter("uni_graduate_date");
		String graduate_type1[] = request.getParameterValues("graduate_type"); 
		String graduate_type="";
		for(int i = 0; i < graduate_type1.length; i++) {
			if(!graduate_type1[i].equals("")) {
				graduate_type = graduate_type1[i];
			}
		}
		/*System.out.println(resume_title);
		System.out.println(rname);
		System.out.println(birth_date);
		System.out.println(remail);
		System.out.println(rphone);
		System.out.println(raddress);
		System.out.println(company_out_date);*/
		java.sql.Date uni_enroll_date = null;
		if(uni_enroll_date1 != "") {
			
			uni_enroll_date = java.sql.Date.valueOf(uni_enroll_date1);
		} else {
			uni_enroll_date = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		java.sql.Date uni_graduate_date = null;
		if(uni_graduate_date1 != "") {
			
			uni_graduate_date = java.sql.Date.valueOf(uni_graduate_date1);
		} else {
			uni_graduate_date = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		java.sql.Date sch_enroll_date = null;
		if(realDate1 != "") {
			 
			sch_enroll_date = java.sql.Date.valueOf(realDate1);
		} else {
			sch_enroll_date = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		
		java.sql.Date sch_graduate_date = null;
		if(realDate2 != "") {
			
			sch_graduate_date = java.sql.Date.valueOf(realDate2);
		} else {
			sch_graduate_date = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		
		java.sql.Date company_in_year = null;
		if(company_in_year1 != "") {
			
			company_in_year = java.sql.Date.valueOf(company_in_year1);
		} else {
			company_in_year = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		java.sql.Date company_out_date = null;
		if(company_out_date1 != "") {
			
			company_out_date = java.sql.Date.valueOf(company_out_date1);
		} else {
			company_out_date = new java.sql.Date(new GregorianCalendar().getTimeInMillis());
		}
		
		int career_year=Integer.parseInt(career_year1);
		double uni_score = Double.parseDouble(uni_score1);
		double uni_score_choice = Double.parseDouble(uni_score_choice1);
		
		//Double.parseDouble
		  
		/*System.out.println("resume_title : " + resume_title);
		System.out.println("birth_date : " + birth_date);
		System.out.println("remail : " + remail);
		System.out.println("rphone : " + rphone);
		System.out.println("raddress : " + raddress);
		System.out.println("edu_choice : " + edu_choice);
		System.out.println("sch_name : " + sch_name);
		System.out.println("major : " + major);
		System.out.println("sch_enroll_date : " + sch_enroll_date);
		System.out.println("sch_graduate_date : " + sch_graduate_date);
		System.out.println("career_yn : " + career_yn);
		System.out.println("company_name" + company_name);
		System.out.println("company_in_year : " + company_in_year);
		System.out.println("company_out_datae : " + company_out_date);
		System.out.println("job_level : " + job_level);
		System.out.println("career_year : " + career_year);
		System.out.println("career_kind : " + career_kind);
		System.out.println("career_area : " + career_area);    
		System.out.println("career_dept : " + career_dept);
		System.out.println("task : " + task);
		System.out.println("introduce_title : " + introduce_title);
		System.out.println("introduce_content : " + introduce_content);
		System.out.println("interest_area : " + interest_area);
		System.out.println("interest_job : " + apply_field);
		System.out.println("resume_open_yn : " + resume_open_yn);
		
		System.out.println("score : "  + uni_score1);
		System.out.println("uni_score_choice1 : " + uni_score_choice1);
		System.out.println("resume_open_yn : " + resume_open_yn );*/
		Resume resume = new Resume();
		resume.setResume_title(resume_title);
		resume.setBirth_date(birth_date);
		resume.setApply_field(apply_field);
		resume.setResume_open_yn(resume_open_yn);
		resume.setRemail(remail);
		resume.setCareer_yn(career_yn);
		resume.setInterestarea(interest_area); //_ 추가하기
		resume.setIntroducecontent(introduce_content); // _추가
		resume.setIntroducetitle(introduce_title);
		resume.setRaddress(raddress);
		resume.setResume_title(resume_title);
		
		resume.setResume_no(num);
		resume.setGender(gender);
		/*System.out.println(resume);*/
		/*resume.setRtitle(rtitle);
		resume.setRbirth(rbirth);
		resume.setIntroduceTitle(introduceTitle);
		resume.setIntroduceContent(introduceContent);*/
		
		Education edu = new Education();
		
		edu.setEdu_choice(edu_choice);
		edu.setSchool_enroll_date(sch_enroll_date); //sch school 둘중 하나 통일
		edu.setSchool_graduate_date(sch_graduate_date);
		edu.setSchool_name(sch_name);
		edu.setUni_area(uni_area);
		edu.setUni_major(uni_major);
		edu.setUni_major_name(uni_major_name);
		edu.setUni_score(uni_score);
		edu.setUni_score_choice(uni_score_choice);
		edu.setUni_type(uni_type);
		edu.setMajor(major);
		edu.setUni_name(uni_name);
		edu.setGraduate_type(graduate_type);
		edu.setUni_graduate_type(uni_graduate_type);
		edu.setUni_enroll_date(uni_enroll_date);
		edu.setUni_graduate_date(uni_graduate_date);
		edu.setResume_no(num);
		
		/*System.out.println(edu);*/
		
		Career career = new Career();
		career.setCareer_area(career_area);
		career.setCareer_dept(career_dept);
		career.setCareer_kind(career_kind);
		career.setCareer_year(career_year);
		career.setCompany_name(company_name);
		career.setCompany_in_year(company_in_year);
		career.setCompany_out_date(company_out_date);
		career.setJob_level(job_level);
		career.setTask(task);
		career.setResume_no(num);
		
		/*System.out.println(career);*/
		
		int result = new ResumeService().updateResume(resume , career , edu);
		
		String page ="";
		if(result>0) {
			page="res.list";
			request.getRequestDispatcher(page).forward(request, response);
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg","테이블에 정보 삽입 실패!");
			request.getRequestDispatcher(page).forward(request,response);
		}
	
	}
		
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
