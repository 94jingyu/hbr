package com.kh.hbr.resume.model.service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import static com.kh.hbr.common.JDBCTemplate.*;

import com.kh.hbr.apply.model.dao.ApplyDao;
import com.kh.hbr.apply.model.vo.Apply;
import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.recruit.model.dao.RecruitDao;
import com.kh.hbr.recruit.model.vo.Recruit;
import com.kh.hbr.resume.model.dao.ResumeDao;
import com.kh.hbr.resume.model.vo.Career;
import com.kh.hbr.resume.model.vo.Education;
import com.kh.hbr.resume.model.vo.Recruit1;
import com.kh.hbr.resume.model.vo.Resume;
import com.kh.hbr.resume.model.vo.ResumeInterview;

public class ResumeService {

	
	public int insertResume(Resume resume, Career career, Education edu, ArrayList<Attachment> fileList) {
		// TODO Auto-generated method stub
		Connection con= getConnection();
		
		int result1 = new ResumeDao().insertResume(con, resume);
		
		int result2 = 0;
		int result3 = 0;
		int result4 = 0;
		if(result1>0) {
			int resume_no = new ResumeDao().selectCurrval(con);
			 
			if(resume_no >0) {
				career.setResume_no(resume_no);
				edu.setResume_no(resume_no);
				
				result2 = new ResumeDao().insertCareer(con,career);
				result3 = new ResumeDao().insertEucation(con,edu);
				
				for(int i = 0; i < fileList.size(); i++) {
					
					fileList.get(i).setBoardNo((resume_no));				
					result4 += new ResumeDao().insertAttachment(con, fileList.get(i));
					
				}
			}
		}else {
		
		}
		int result=0;
		
		if(result1>0 && result2>0 && result3>0/* && result4>0*/) {
			commit(con);
			result=1;
		}else {
			rollback(con);
			result=0;
		}
		close(con);
		return result;
	}
	public ArrayList<Resume> selectResume() {
		
		Connection con = getConnection();
		
		ArrayList<Resume> list = new ResumeDao().selectResume(con);
		
		close(con);
		
		return list;
	}
	public ArrayList<Resume> selectList(int memberNo) {
		// TODO Auto-generated method stub
		 Connection con=getConnection();
	      
	      ArrayList<Resume> list=new ResumeDao().selectList(con,memberNo);
	      /*System.out.println("service list:"+list);*/
	      
	      if(list!=null) {
	         commit(con);
	      }else {
	         rollback(con);        
	      }
	      return list;

	}
	public HashMap<String, Object> resumeDetail(int resumeNum) {
		// TODO Auto-generated method stub
		Connection con = getConnection();
		
		
		
		HashMap<String, Object> hmap = null;
		
		hmap = new ResumeDao().resumeDetail(con, resumeNum);
		
		close(con);
		
		return hmap;
	}
	public HashMap<String, Object> resumeUpdateView(int num) {
		// TODO Auto-generated method stub
		Connection con = getConnection();
		
		
		
		HashMap<String, Object> hmap = null;
		
		hmap = new ResumeDao().resumeUpdateView(con, num);
		
		close(con);
		
		return hmap;
	}
	public int updateResume(Resume resume, Career career, Education edu) {
		// TODO Auto-generated method stub
		Connection con= getConnection();
		
		int result1 = new ResumeDao().updateResume(con, resume);
		
		int result2 = 0;
		int result3 = 0;
		
			
			
				
				result2 = new ResumeDao().updateCareer(con,career);
				/*System.out.println("result2 : " + result2);*/
				result3 = new ResumeDao().updateEucation(con,edu);
				/*System.out.println("result3 : " + result3);*/
			
		
		int result=0;
		
		if(result1>0 && result2>0 && result3>0) {
			commit(con);
			result=1;
		}else {
			rollback(con);
			result=0;
		}
		close(con);
		/*System.out.println("service : " + result);*/
		return result;
	}
	public HashMap<String, Object> UserMyPage(int memberNo) {
		// TODO Auto-generated method stub
		Connection con=getConnection();
	      
	      //ArrayList<Resume> list=new ResumeDao().userMyPage(con,memberNo);
	      
		HashMap<String, Object>hmap=null;
	      
	    hmap = new ResumeDao().userMyPage(con, memberNo);  
		
	      return hmap;

	}
	public int deleteResume(int num) {
		// TODO Auto-generated method stub
		Connection con = getConnection();
		
		int result = new ResumeDao().deleteResume(con, num);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}
	public int representYResume(int num) {
		// TODO Auto-generated method stub
		Connection con = getConnection();
		
		int result = new ResumeDao().representYResume(con, num);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	
	}
	public int representNResume(int num) {
		// TODO Auto-generated method stub
		Connection con = getConnection();
		
		int result = new ResumeDao().representNResume(con, num);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}
	public int representResume(Resume resume) {
		// TODO Auto-generated method stub
		Connection con= getConnection();
		
		int result1 = new ResumeDao().representResume1(con, resume);
		
		int result2 = 0;
		
		
			
			
				
		result2 = new ResumeDao().representResume2(con, resume);
		/*System.out.println("result2 : " + result2);*/
		
			
		
		int result=0;
		
		if(result1>0 && result2>0) {
			commit(con);
			result=1;
		}else {
			rollback(con);
			result=0;
		}
		close(con);
		/*System.out.println("service : " + result);*/
		return result;
	}
	public int ResumeSave(Resume resume, Career career, Education edu, ArrayList<Attachment> fileList) {
		// TODO Auto-generated method stub
		Connection con= getConnection();
		
		int result1 = new ResumeDao().ResumeSave(con, resume);
		
		int result2 = 0;
		int result3 = 0;
		int result4 = 0;
		if(result1>0) {
			int resume_no = new ResumeDao().selectCurrval(con);
			 
			if(resume_no >0) {
				career.setResume_no(resume_no);
				edu.setResume_no(resume_no);
				
				result2 = new ResumeDao().CareerSave(con,career);
				result3 = new ResumeDao().EducationSave(con,edu);
				for(int i = 0; i < fileList.size(); i++) {
					
					fileList.get(i).setBoardNo((resume_no));				
					result4 += new ResumeDao().AttachmentSave(con, fileList.get(i));
					
				}
			}
		}
		else {
		
		}
		int result=0;
		
		if(result1>0 && result2>0 && result3>0) {
			commit(con);
			result=1;
		}else {
			rollback(con);
			result=0;
		}
		close(con);
		return result;
	}
	public ArrayList<ResumeInterview> resumeinterview(int memberNo) {
		// TODO Auto-generated method stub
		Connection con=getConnection();
	      
	      ArrayList<ResumeInterview> ilist=new ResumeDao().resumeinterview(con,memberNo);
	      /*System.out.println("service list:"+list);*/
	      
	      if(ilist!=null) {
	         commit(con);
	      }else {
	         rollback(con);        
	      }
	      return ilist;

	}
	/*public ArrayList<Recruit1> SearchLimit(String company_name) {
		// TODO Auto-generated method stub
		Connection con = getConnection();
		
		ArrayList<Recruit1> slist = new ResumeDao().searchlimit(con, company_name);
		
		close(con);
		
		if(slist != null) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return slist;
	}*/
	public ArrayList<Recruit1> SearchLimit1() {
		// TODO Auto-generated method stub
		Connection con = getConnection();
		
		ArrayList<Recruit1> slist = new ResumeDao().searchlimit(con);
		
		close(con);
		
		
		
		return slist;
	}
}