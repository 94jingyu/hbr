package com.kh.hbr.resume.model.service;

import static com.kh.hbr.common.JDBCTemplate.close;
import static com.kh.hbr.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.ArrayList;

import com.kh.hbr.resume.model.dao.ResumeDao_tw;
import com.kh.hbr.resume.model.vo.Resume;

public class ResumeService_tw {
	
	//태원 이력서 조회용 메소드
	public ArrayList<Resume> selectResume(int memberNo) {
		
		Connection con = getConnection();
		
		ArrayList<Resume> list = new ResumeDao_tw().selectResume(con, memberNo);
		
		close(con);
		
		return list;
	}
	
	
	
}
