package com.kh.hbr.resume.model.vo;

import java.sql.Date;

public class Education {
	private int education_no;
	private int resume_no;
	private String school_name;
	private String major;
	private Date school_enroll_date;
	private Date school_graduate_date;
	private String edu_choice;
	private String uni_type;    
	private String uni_major;
	private String uni_major_name;
	private double uni_score_choice;
	private double uni_score;
	private String uni_area;
	private String uni_name;
	private String graduate_type;
	private String uni_graduate_type;
	private Date uni_enroll_date;
	private Date uni_graduate_date;
	
	
	public Education() {}


	public Education(int education_no, int resume_no, String school_name, String major, Date school_enroll_date,
			Date school_graduate_date, String edu_choice, String uni_type, String uni_major, String uni_major_name,
			double uni_score_choice, double uni_score, String uni_area, String uni_name, String graduate_type,
			String uni_graduate_type, Date uni_enroll_date, Date uni_graduate_date) {
		super();
		this.education_no = education_no;
		this.resume_no = resume_no;
		this.school_name = school_name;
		this.major = major;
		this.school_enroll_date = school_enroll_date;
		this.school_graduate_date = school_graduate_date;
		this.edu_choice = edu_choice;
		this.uni_type = uni_type;
		this.uni_major = uni_major;
		this.uni_major_name = uni_major_name;
		this.uni_score_choice = uni_score_choice;
		this.uni_score = uni_score;
		this.uni_area = uni_area;
		this.uni_name = uni_name;
		this.graduate_type = graduate_type;
		this.uni_graduate_type = uni_graduate_type;
		this.uni_enroll_date = uni_enroll_date;
		this.uni_graduate_date = uni_graduate_date;
	}


	public int getEducation_no() {
		return education_no;
	}


	public void setEducation_no(int education_no) {
		this.education_no = education_no;
	}


	public int getResume_no() {
		return resume_no;
	}


	public void setResume_no(int resume_no) {
		this.resume_no = resume_no;
	}


	public String getSchool_name() {
		return school_name;
	}


	public void setSchool_name(String school_name) {
		this.school_name = school_name;
	}


	public String getMajor() {
		return major;
	}


	public void setMajor(String major) {
		this.major = major;
	}


	public Date getSchool_enroll_date() {
		return school_enroll_date;
	}


	public void setSchool_enroll_date(Date school_enroll_date) {
		this.school_enroll_date = school_enroll_date;
	}


	public Date getSchool_graduate_date() {
		return school_graduate_date;
	}


	public void setSchool_graduate_date(Date school_graduate_date) {
		this.school_graduate_date = school_graduate_date;
	}


	public String getEdu_choice() {
		return edu_choice;
	}


	public void setEdu_choice(String edu_choice) {
		this.edu_choice = edu_choice;
	}


	public String getUni_type() {
		return uni_type;
	}


	public void setUni_type(String uni_type) {
		this.uni_type = uni_type;
	}


	public String getUni_major() {
		return uni_major;
	}


	public void setUni_major(String uni_major) {
		this.uni_major = uni_major;
	}


	public String getUni_major_name() {
		return uni_major_name;
	}


	public void setUni_major_name(String uni_major_name) {
		this.uni_major_name = uni_major_name;
	}


	public double getUni_score_choice() {
		return uni_score_choice;
	}


	public void setUni_score_choice(double uni_score_choice) {
		this.uni_score_choice = uni_score_choice;
	}


	public double getUni_score() {
		return uni_score;
	}


	public void setUni_score(double uni_score) {
		this.uni_score = uni_score;
	}


	public String getUni_area() {
		return uni_area;
	}


	public void setUni_area(String uni_area) {
		this.uni_area = uni_area;
	}


	public String getUni_name() {
		return uni_name;
	}


	public void setUni_name(String uni_name) {
		this.uni_name = uni_name;
	}


	public String getGraduate_type() {
		return graduate_type;
	}


	public void setGraduate_type(String graduate_type) {
		this.graduate_type = graduate_type;
	}


	public String getUni_graduate_type() {
		return uni_graduate_type;
	}


	public void setUni_graduate_type(String uni_graduate_type) {
		this.uni_graduate_type = uni_graduate_type;
	}


	public Date getUni_enroll_date() {
		return uni_enroll_date;
	}


	public void setUni_enroll_date(Date uni_enroll_date) {
		this.uni_enroll_date = uni_enroll_date;
	}


	public Date getUni_graduate_date() {
		return uni_graduate_date;
	}


	public void setUni_graduate_date(Date uni_graduate_date) {
		this.uni_graduate_date = uni_graduate_date;
	}


	@Override
	public String toString() {
		return "Education [education_no=" + education_no + ", resume_no=" + resume_no + ", school_name=" + school_name
				+ ", major=" + major + ", school_enroll_date=" + school_enroll_date + ", school_graduate_date="
				+ school_graduate_date + ", edu_choice=" + edu_choice + ", uni_type=" + uni_type + ", uni_major="
				+ uni_major + ", uni_major_name=" + uni_major_name + ", uni_score_choice=" + uni_score_choice
				+ ", uni_score=" + uni_score + ", uni_area=" + uni_area + ", uni_name=" + uni_name + ", graduate_type="
				+ graduate_type + ", uni_graduate_type=" + uni_graduate_type + ", uni_enroll_date=" + uni_enroll_date
				+ ", uni_graduate_date=" + uni_graduate_date + "]";
	}
	
	
}
