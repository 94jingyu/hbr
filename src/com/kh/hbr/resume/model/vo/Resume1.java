package com.kh.hbr.resume.model.vo;

import java.sql.Date;

public class Resume implements java.io.Serializable{
	
	
	private int resume_no;
	private String resume_title;
	private String birth_date;
	private String raddress;    
	private String introducetitle;
	private String introducecontent;
	private String interestarea;
	private String apply_field;
	private Date resume_enroll_date;
	private Date resume_modify_date;
	private String represent_yn;
	private String delete_yn;
	private String career_yn;
	private int rmember_no;
	private int career_no;
	private String company_name;
	private Date company_in_year;
	private Date company_out_date;
	private String job_level;
	private int career_year;
	private String career_kind;
	private String career_area;
	private String career_dept;
	private String task;
	private int education_no;
	private String school_name; 
	private String major;
	private Date school_enroll_date;
	private Date school_graduate_date;
	private String edu_choice;
	private String uni_type;
	private String uni_major;
	private String uni_major_name;
	private double uni_score_choice;
	private double uni_score;
	private String uni_area;
	
	public Resume() {}

	public Resume(int resume_no, String resume_title, String birth_date, String raddress, String introducetitle,
			String introducecontent, String interestarea, String apply_field, Date resume_enroll_date,
			Date resume_modify_date, String represent_yn, String delete_yn, String career_yn, int rmember_no,
			int career_no, String company_name, Date company_in_year, Date company_out_date, String job_level,
			int career_year, String career_kind, String career_area, String career_dept, String task, int education_no,
			String school_name, String major, Date school_enroll_date, Date school_graduate_date, String edu_choice,
			String uni_type, String uni_major, String uni_major_name, double uni_score_choice, double uni_score,
			String uni_area) {
		super();
		this.resume_no = resume_no;
		this.resume_title = resume_title;
		this.birth_date = birth_date;
		this.raddress = raddress;
		this.introducetitle = introducetitle;
		this.introducecontent = introducecontent;
		this.interestarea = interestarea;
		this.apply_field = apply_field;
		this.resume_enroll_date = resume_enroll_date;
		this.resume_modify_date = resume_modify_date;
		this.represent_yn = represent_yn;
		this.delete_yn = delete_yn;
		this.career_yn = career_yn;
		this.rmember_no = rmember_no;
		this.career_no = career_no;
		this.company_name = company_name;
		this.company_in_year = company_in_year;
		this.company_out_date = company_out_date;
		this.job_level = job_level;
		this.career_year = career_year;
		this.career_kind = career_kind;
		this.career_area = career_area;
		this.career_dept = career_dept;
		this.task = task;
		this.education_no = education_no;
		this.school_name = school_name;
		this.major = major;
		this.school_enroll_date = school_enroll_date;
		this.school_graduate_date = school_graduate_date;
		this.edu_choice = edu_choice;
		this.uni_type = uni_type;
		this.uni_major = uni_major;
		this.uni_major_name = uni_major_name;
		this.uni_score_choice = uni_score_choice;
		this.uni_score = uni_score;
		this.uni_area = uni_area;
	}

	public int getResume_no() {
		return resume_no;
	}

	public void setResume_no(int resume_no) {
		this.resume_no = resume_no;
	}

	public String getResume_title() {
		return resume_title;
	}

	public void setResume_title(String resume_title) {
		this.resume_title = resume_title;
	}

	public String getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(String birth_date) {
		this.birth_date = birth_date;
	}

	public String getRaddress() {
		return raddress;
	}

	public void setRaddress(String raddress) {
		this.raddress = raddress;
	}

	public String getIntroducetitle() {
		return introducetitle;
	}

	public void setIntroducetitle(String introducetitle) {
		this.introducetitle = introducetitle;
	}

	public String getIntroducecontent() {
		return introducecontent;
	}

	public void setIntroducecontent(String introducecontent) {
		this.introducecontent = introducecontent;
	}

	public String getInterestarea() {
		return interestarea;
	}

	public void setInterestarea(String interestarea) {
		this.interestarea = interestarea;
	}

	public String getApply_field() {
		return apply_field;
	}

	public void setApply_field(String apply_field) {
		this.apply_field = apply_field;
	}

	public Date getResume_enroll_date() {
		return resume_enroll_date;
	}

	public void setResume_enroll_date(Date resume_enroll_date) {
		this.resume_enroll_date = resume_enroll_date;
	}

	public Date getResume_modify_date() {
		return resume_modify_date;
	}

	public void setResume_modify_date(Date resume_modify_date) {
		this.resume_modify_date = resume_modify_date;
	}

	public String getRepresent_yn() {
		return represent_yn;
	}

	public void setRepresent_yn(String represent_yn) {
		this.represent_yn = represent_yn;
	}

	public String getDelete_yn() {
		return delete_yn;
	}

	public void setDelete_yn(String delete_yn) {
		this.delete_yn = delete_yn;
	}

	public String getCareer_yn() {
		return career_yn;
	}

	public void setCareer_yn(String career_yn) {
		this.career_yn = career_yn;
	}

	public int getRmember_no() {
		return rmember_no;
	}

	public void setRmember_no(int rmember_no) {
		this.rmember_no = rmember_no;
	}

	public int getCareer_no() {
		return career_no;
	}

	public void setCareer_no(int career_no) {
		this.career_no = career_no;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public Date getCompany_in_year() {
		return company_in_year;
	}

	public void setCompany_in_year(Date company_in_year) {
		this.company_in_year = company_in_year;
	}

	public Date getCompany_out_date() {
		return company_out_date;
	}

	public void setCompany_out_date(Date company_out_date) {
		this.company_out_date = company_out_date;
	}

	public String getJob_level() {
		return job_level;
	}

	public void setJob_level(String job_level) {
		this.job_level = job_level;
	}

	public int getCareer_year() {
		return career_year;
	}

	public void setCareer_year(int career_year) {
		this.career_year = career_year;
	}

	public String getCareer_kind() {
		return career_kind;
	}

	public void setCareer_kind(String career_kind) {
		this.career_kind = career_kind;
	}

	public String getCareer_area() {
		return career_area;
	}

	public void setCareer_area(String career_area) {
		this.career_area = career_area;
	}

	public String getCareer_dept() {
		return career_dept;
	}

	public void setCareer_dept(String career_dept) {
		this.career_dept = career_dept;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public int getEducation_no() {
		return education_no;
	}

	public void setEducation_no(int education_no) {
		this.education_no = education_no;
	}

	public String getSchool_name() {
		return school_name;
	}

	public void setSchool_name(String school_name) {
		this.school_name = school_name;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public Date getSchool_enroll_date() {
		return school_enroll_date;
	}

	public void setSchool_enroll_date(Date school_enroll_date) {
		this.school_enroll_date = school_enroll_date;
	}

	public Date getSchool_graduate_date() {
		return school_graduate_date;
	}

	public void setSchool_graduate_date(Date school_graduate_date) {
		this.school_graduate_date = school_graduate_date;
	}

	public String getEdu_choice() {
		return edu_choice;
	}

	public void setEdu_choice(String edu_choice) {
		this.edu_choice = edu_choice;
	}

	public String getUni_type() {
		return uni_type;
	}

	public void setUni_type(String uni_type) {
		this.uni_type = uni_type;
	}

	public String getUni_major() {
		return uni_major;
	}

	public void setUni_major(String uni_major) {
		this.uni_major = uni_major;
	}

	public String getUni_major_name() {
		return uni_major_name;
	}

	public void setUni_major_name(String uni_major_name) {
		this.uni_major_name = uni_major_name;
	}

	public double getUni_score_choice() {
		return uni_score_choice;
	}

	public void setUni_score_choice(double uni_score_choice) {
		this.uni_score_choice = uni_score_choice;
	}

	public double getUni_score() {
		return uni_score;
	}

	public void setUni_score(double uni_score) {
		this.uni_score = uni_score;
	}

	public String getUni_area() {
		return uni_area;
	}

	public void setUni_area(String uni_area) {
		this.uni_area = uni_area;
	}

	@Override
	public String toString() {
		return "Resume [resume_no=" + resume_no + ", resume_title=" + resume_title + ", birth_date=" + birth_date
				+ ", raddress=" + raddress + ", introducetitle=" + introducetitle + ", introducecontent="
				+ introducecontent + ", interestarea=" + interestarea + ", apply_field=" + apply_field
				+ ", resume_enroll_date=" + resume_enroll_date + ", resume_modify_date=" + resume_modify_date
				+ ", represent_yn=" + represent_yn + ", delete_yn=" + delete_yn + ", career_yn=" + career_yn
				+ ", rmember_no=" + rmember_no + ", career_no=" + career_no + ", company_name=" + company_name
				+ ", company_in_year=" + company_in_year + ", company_out_date=" + company_out_date + ", job_level="
				+ job_level + ", career_year=" + career_year + ", career_kind=" + career_kind + ", career_area="
				+ career_area + ", career_dept=" + career_dept + ", task=" + task + ", education_no=" + education_no
				+ ", school_name=" + school_name + ", major=" + major + ", school_enroll_date=" + school_enroll_date
				+ ", school_graduate_date=" + school_graduate_date + ", edu_choice=" + edu_choice + ", uni_type="
				+ uni_type + ", uni_major=" + uni_major + ", uni_major_name=" + uni_major_name + ", uni_score_choice="
				+ uni_score_choice + ", uni_score=" + uni_score + ", uni_area=" + uni_area + "]";
	}

	
	

	
}
	