package com.kh.hbr.resume.model.vo;

import java.sql.Date;

public class Limit {
	private int limit_num;
	private Date limit_enoll_date;
	private String limit_status;
	private int member_no;
	private int bmember_no;
	
	
	public Limit() {}


	public Limit(int limit_num, Date limit_enoll_date, String limit_status, int member_no, int bmember_no) {
		super();
		this.limit_num = limit_num;
		this.limit_enoll_date = limit_enoll_date;
		this.limit_status = limit_status;
		this.member_no = member_no;
		this.bmember_no = bmember_no;
	}


	public int getLimit_num() {
		return limit_num;
	}


	public void setLimit_num(int limit_num) {
		this.limit_num = limit_num;
	}


	public Date getLimit_enoll_date() {
		return limit_enoll_date;
	}


	public void setLimit_enoll_date(Date limit_enoll_date) {
		this.limit_enoll_date = limit_enoll_date;
	}


	public String getLimit_status() {
		return limit_status;
	}


	public void setLimit_status(String limit_status) {
		this.limit_status = limit_status;
	}


	public int getMember_no() {
		return member_no;
	}


	public void setMember_no(int member_no) {
		this.member_no = member_no;
	}


	public int getBmember_no() {
		return bmember_no;
	}


	public void setBmember_no(int bmember_no) {
		this.bmember_no = bmember_no;
	}


	@Override
	public String toString() {
		return "Limit [limit_num=" + limit_num + ", limit_enoll_date=" + limit_enoll_date + ", limit_status="
				+ limit_status + ", member_no=" + member_no + ", bmember_no=" + bmember_no + "]";
	}

	
	
	
	

}
