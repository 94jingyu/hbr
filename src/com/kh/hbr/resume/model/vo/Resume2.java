package com.kh.hbr.resume.model.vo;

import java.sql.Date;

public class Resume2 {
	private String rtitle;
	private String introduceTitle;
	private String rbirth;
	private String introduceContent;
	    
	
	public Resume2() {}


	public Resume2(String rtitle, String introduceTitle, String rbirth, String introduceContent) {
		super();
		this.rtitle = rtitle;
		this.introduceTitle = introduceTitle;
		this.rbirth = rbirth;
		this.introduceContent = introduceContent;
	}


	public String getRtitle() {
		return rtitle;
	}


	public void setRtitle(String rtitle) {
		this.rtitle = rtitle;
	} 


	public String getIntroduceTitle() {
		return introduceTitle;
	}


	public void setIntroduceTitle(String introduceTitle) {
		this.introduceTitle = introduceTitle;
	}


	public String getRbirth() {
		return rbirth;
	}


	public void setRbirth(String rbirth) {
		this.rbirth = rbirth;
	}


	public String getIntroduceContent() {
		return introduceContent;
	}


	public void setIntroduceContent(String introduceContent) {
		this.introduceContent = introduceContent;
	}


	@Override
	public String toString() {
		return "Resume [rtitle=" + rtitle + ", introduceTitle=" + introduceTitle + ", rbirth=" + rbirth
				+ ", introduceContent=" + introduceContent + "]";
	}
	
	
}
