package com.kh.hbr.resume.model.vo;

import java.sql.Date;

public class Career {
	private int career_no;
	private int resume_no;
	private String company_name;
	private Date company_in_year;   
	private Date company_out_date;
	private String job_level;
	private int career_year;
	private String career_kind;
	private String career_area;
	private String career_dept;
	private String task;
	 
	public Career() {}

	public Career(int career_no, int resume_no, String company_name, Date company_in_year, Date company_out_date,
			String job_level, int career_year, String career_kind, String career_area, String career_dept,
			String task) {
		super();
		this.career_no = career_no;
		this.resume_no = resume_no;
		this.company_name = company_name;
		this.company_in_year = company_in_year;
		this.company_out_date = company_out_date;
		this.job_level = job_level;
		this.career_year = career_year;
		this.career_kind = career_kind; 
		this.career_area = career_area;
		this.career_dept = career_dept;
		this.task = task;
	}

	public int getCareer_no() {
		return career_no;
	}

	public void setCareer_no(int career_no) {
		this.career_no = career_no;
	}

	public int getResume_no() {
		return resume_no;
	}

	public void setResume_no(int resume_no) {
		this.resume_no = resume_no;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public Date getCompany_in_year() {
		return company_in_year;
	}

	public void setCompany_in_year(Date company_in_year) {
		this.company_in_year = company_in_year;
	}

	public Date getCompany_out_date() {
		return company_out_date;
	}

	public void setCompany_out_date(Date company_out_date) {
		this.company_out_date = company_out_date;
	}

	public String getJob_level() {
		return job_level;
	}

	public void setJob_level(String job_level) {
		this.job_level = job_level;
	}

	public int getCareer_year() {
		return career_year;
	}

	public void setCareer_year(int career_year) {
		this.career_year = career_year;
	}

	public String getCareer_kind() {
		return career_kind;
	}

	public void setCareer_kind(String career_kind) {
		this.career_kind = career_kind;
	}

	public String getCareer_area() {
		return career_area;
	}

	public void setCareer_area(String career_area) {
		this.career_area = career_area;
	}

	public String getCareer_dept() {
		return career_dept;
	}

	public void setCareer_dept(String career_dept) {
		this.career_dept = career_dept;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	@Override
	public String toString() {
		return "Career [career_no=" + career_no + ", resume_no=" + resume_no + ", company_name=" + company_name
				+ ", company_in_year=" + company_in_year + ", company_out_date=" + company_out_date + ", job_level="
				+ job_level + ", career_year=" + career_year + ", career_kind=" + career_kind + ", career_area="
				+ career_area + ", career_dept=" + career_dept + ", task=" + task + "]";
	}

	
	
}
