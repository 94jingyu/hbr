package com.kh.hbr.resume.model.vo;

import java.sql.Date;

public class Resume implements java.io.Serializable{
	
	
	private int resume_no;
	private String resume_title;
	private String birth_date;
	private String raddress;
	private String remail;
	private String introducetitle;
	private String introducecontent;
	private String interestarea;
	private String apply_field;
	private String resume_open_yn;
	private Date resume_enroll_date;
	private Date resume_modify_date;
	private String represent_yn;
	private String delete_yn;
	private String career_yn;
	private String complete_yn;
	private int rmember_no;
	private String gender;
	
	public Resume() {}

	public Resume(int resume_no, String resume_title, String birth_date, String raddress, String remail,
			String introducetitle, String introducecontent, String interestarea, String apply_field,
			String resume_open_yn, Date resume_enroll_date, Date resume_modify_date, String represent_yn,
			String delete_yn, String career_yn, String complete_yn, int rmember_no, String gender) {
		super();
		this.resume_no = resume_no;
		this.resume_title = resume_title;
		this.birth_date = birth_date;
		this.raddress = raddress;
		this.remail = remail;
		this.introducetitle = introducetitle;
		this.introducecontent = introducecontent;
		this.interestarea = interestarea;
		this.apply_field = apply_field;
		this.resume_open_yn = resume_open_yn;
		this.resume_enroll_date = resume_enroll_date;
		this.resume_modify_date = resume_modify_date;
		this.represent_yn = represent_yn;
		this.delete_yn = delete_yn;
		this.career_yn = career_yn;
		this.complete_yn = complete_yn;
		this.rmember_no = rmember_no;
		this.gender = gender;
	}

	public int getResume_no() {
		return resume_no;
	}

	public void setResume_no(int resume_no) {
		this.resume_no = resume_no;
	}

	public String getResume_title() {
		return resume_title;
	}

	public void setResume_title(String resume_title) {
		this.resume_title = resume_title;
	}

	public String getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(String birth_date) {
		this.birth_date = birth_date;
	}

	public String getRaddress() {
		return raddress;
	}

	public void setRaddress(String raddress) {
		this.raddress = raddress;
	}

	public String getRemail() {
		return remail;
	}

	public void setRemail(String remail) {
		this.remail = remail;
	}

	public String getIntroducetitle() {
		return introducetitle;
	}

	public void setIntroducetitle(String introducetitle) {
		this.introducetitle = introducetitle;
	}

	public String getIntroducecontent() {
		return introducecontent;
	}

	public void setIntroducecontent(String introducecontent) {
		this.introducecontent = introducecontent;
	}

	public String getInterestarea() {
		return interestarea;
	}

	public void setInterestarea(String interestarea) {
		this.interestarea = interestarea;
	}

	public String getApply_field() {
		return apply_field;
	}

	public void setApply_field(String apply_field) {
		this.apply_field = apply_field;
	}

	public String getResume_open_yn() {
		return resume_open_yn;
	}

	public void setResume_open_yn(String resume_open_yn) {
		this.resume_open_yn = resume_open_yn;
	}

	public Date getResume_enroll_date() {
		return resume_enroll_date;
	}

	public void setResume_enroll_date(Date resume_enroll_date) {
		this.resume_enroll_date = resume_enroll_date;
	}

	public Date getResume_modify_date() {
		return resume_modify_date;
	}

	public void setResume_modify_date(Date resume_modify_date) {
		this.resume_modify_date = resume_modify_date;
	}

	public String getRepresent_yn() {
		return represent_yn;
	}

	public void setRepresent_yn(String represent_yn) {
		this.represent_yn = represent_yn;
	}

	public String getDelete_yn() {
		return delete_yn;
	}

	public void setDelete_yn(String delete_yn) {
		this.delete_yn = delete_yn;
	}

	public String getCareer_yn() {
		return career_yn;
	}

	public void setCareer_yn(String career_yn) {
		this.career_yn = career_yn;
	}

	public String getComplete_yn() {
		return complete_yn;
	}

	public void setComplete_yn(String complete_yn) {
		this.complete_yn = complete_yn;
	}

	public int getRmember_no() {
		return rmember_no;
	}

	public void setRmember_no(int rmember_no) {
		this.rmember_no = rmember_no;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "Resume [resume_no=" + resume_no + ", resume_title=" + resume_title + ", birth_date=" + birth_date
				+ ", raddress=" + raddress + ", remail=" + remail + ", introducetitle=" + introducetitle
				+ ", introducecontent=" + introducecontent + ", interestarea=" + interestarea + ", apply_field="
				+ apply_field + ", resume_open_yn=" + resume_open_yn + ", resume_enroll_date=" + resume_enroll_date
				+ ", resume_modify_date=" + resume_modify_date + ", represent_yn=" + represent_yn + ", delete_yn="
				+ delete_yn + ", career_yn=" + career_yn + ", complete_yn=" + complete_yn + ", rmember_no=" + rmember_no
				+ ", gender=" + gender + "]";
	}


	
 	
}
	