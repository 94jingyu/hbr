package com.kh.hbr.resume.model.vo;

import java.sql.Date;

public class Recruit1 {
	private int interview_no;
	private int bmember_no;
	private int resume_no;
	private int recruit_no;
	private String open_status;
	private Date request_date;
	private Date open_period_start;
	private Date open_period_end;
	private String business_content;
	private String company_name;
	private String recruitTitle;
	
	public Recruit1() {}

	public Recruit1(int interview_no, int bmember_no, int resume_no, int recruit_no, String open_status,
			Date request_date, Date open_period_start, Date open_period_end, String business_content,
			String company_name, String recruitTitle) {
		super();
		this.interview_no = interview_no;
		this.bmember_no = bmember_no;
		this.resume_no = resume_no;
		this.recruit_no = recruit_no;
		this.open_status = open_status;
		this.request_date = request_date;
		this.open_period_start = open_period_start;
		this.open_period_end = open_period_end;
		this.business_content = business_content;
		this.company_name = company_name;
		this.recruitTitle = recruitTitle;
	}

	public int getInterview_no() {
		return interview_no;
	}

	public void setInterview_no(int interview_no) {
		this.interview_no = interview_no;
	}

	public int getBmember_no() {
		return bmember_no;
	}

	public void setBmember_no(int bmember_no) {
		this.bmember_no = bmember_no;
	}

	public int getResume_no() {
		return resume_no;
	}

	public void setResume_no(int resume_no) {
		this.resume_no = resume_no;
	}

	public int getRecruit_no() {
		return recruit_no;
	}

	public void setRecruit_no(int recruit_no) {
		this.recruit_no = recruit_no;
	}

	public String getOpen_status() {
		return open_status;
	}

	public void setOpen_status(String open_status) {
		this.open_status = open_status;
	}

	public Date getRequest_date() {
		return request_date;
	}

	public void setRequest_date(Date request_date) {
		this.request_date = request_date;
	}

	public Date getOpen_period_start() {
		return open_period_start;
	}

	public void setOpen_period_start(Date open_period_start) {
		this.open_period_start = open_period_start;
	}

	public Date getOpen_period_end() {
		return open_period_end;
	}

	public void setOpen_period_end(Date open_period_end) {
		this.open_period_end = open_period_end;
	}

	public String getBusiness_content() {
		return business_content;
	}

	public void setBusiness_content(String business_content) {
		this.business_content = business_content;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public String getRecruitTitle() {
		return recruitTitle;
	}

	public void setRecruitTitle(String recruitTitle) {
		this.recruitTitle = recruitTitle;
	}

	@Override
	public String toString() {
		return "Recruit1 [interview_no=" + interview_no + ", bmember_no=" + bmember_no + ", resume_no=" + resume_no
				+ ", recruit_no=" + recruit_no + ", open_status=" + open_status + ", request_date=" + request_date
				+ ", open_period_start=" + open_period_start + ", open_period_end=" + open_period_end
				+ ", business_content=" + business_content + ", company_name=" + company_name + ", recruitTitle="
				+ recruitTitle + "]";
	}
	
	

}
