package com.kh.hbr.resume.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import static com.kh.hbr.common.JDBCTemplate.*;

import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.recruit.model.vo.Recruit;
import com.kh.hbr.resume.model.vo.Career;
import com.kh.hbr.resume.model.vo.Education;
import com.kh.hbr.resume.model.vo.Resume;

public class ResumeDao_tw {
	private Properties prop = new Properties();
	
	public ResumeDao_tw() {
		String fileName = ResumeDao.class.getResource("/sql/resume/resume-query_tw.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

		//태원 이력서 조회용 메소드
		public ArrayList<Resume> selectResume(Connection con, int memberNo) {
			
			ArrayList<Resume> list = null;
			PreparedStatement pstmt = null;
			ResultSet rset = null;
			
			String query = prop.getProperty("selectResume");
			
			try {
				pstmt = con.prepareStatement(query);	 //psmt는 매개변수로 쿼리문을 호출한다. stmt 일 때는 rset = stmt.executeQuery(query); 
				pstmt.setInt(1, memberNo);
				rset = pstmt.executeQuery();
				
				list = new ArrayList<Resume>();
				
				while(rset.next()) {
					Resume r = new Resume();
					r.setResume_no(rset.getInt("RESUME_NO"));
					r.setResume_title(rset.getString("RESUME_TITLE"));
					
					list.add(r);
				}
				
//				System.out.println(list);
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
				close(rset);
			}
			
			//select 에서는 커밋 롤백을 하지 않아도 된다. 변경값이 없기 때문 
			return list;
		}

}
