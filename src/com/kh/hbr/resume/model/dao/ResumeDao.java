package com.kh.hbr.resume.model.dao;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import static com.kh.hbr.common.JDBCTemplate.*;
import static com.kh.hbr.common.JDBCTemplate.close;

import com.kh.hbr.apply.model.vo.Apply;
import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.recruit.model.vo.Recruit;
import com.kh.hbr.resume.model.vo.Career;
import com.kh.hbr.resume.model.vo.Education;
import com.kh.hbr.resume.model.vo.Recruit1;
import com.kh.hbr.resume.model.vo.Resume;
import com.kh.hbr.resume.model.vo.ResumeInterview;




public class ResumeDao {
	private Properties prop = new Properties();
	
	public ResumeDao() {
		String fileName = ResumeDao.class.getResource("/sql/resume/resume-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int insertResume(Connection con, Resume resume) {
		// TODO Auto-generated method stub
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("insertResume");
		
		try {
			pstmt = con.prepareStatement(query);
			//pstmt.setInt(1, resume.getResume_no());
			pstmt.setString(1, resume.getResume_title());
			pstmt.setString(2, resume.getBirth_date());
			pstmt.setString(3, resume.getRemail());
			pstmt.setString(4, resume.getRaddress());
			pstmt.setString(5, resume.getIntroducetitle());
			pstmt.setString(6, resume.getIntroducecontent());
			pstmt.setString(7, resume.getInterestarea());
			pstmt.setString(8, resume.getApply_field());
			pstmt.setString(9, resume.getResume_open_yn());
			
			
			//pstmt.setDate(10, resume.getResume_enroll_date());
			//pstmt.setDate(11, resume.getResume_modify_date());
			//pstmt.setString(12, resume.getRepresent_yn());
			//pstmt.setString(13, resume.getDelete_yn());
			pstmt.setString(10, resume.getCareer_yn());
			
			pstmt.setInt(11, resume.getRmember_no());
			pstmt.setString(12, resume.getGender());
			
			result=pstmt.executeUpdate();
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		
		return result;
	}
 
	public int selectCurrval(Connection con) {
		// TODO Auto-generated method stub
		Statement stmt = null;
		ResultSet rset = null;
		int resume_no=0;
		String query=prop.getProperty("selectCurrval");
		try {
			stmt=con.createStatement();
			rset= stmt.executeQuery(query);
			
			if(rset.next()) {
				resume_no=rset.getInt("currval");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(stmt);
			close(rset);
		}
		
		return resume_no;
	}

	public int insertCareer(Connection con, Career career) {
		// TODO Auto-generated method stub
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("insertCareer");
		
		try {
			pstmt=con.prepareStatement(query);
			pstmt.setInt(1,career.getResume_no());
			pstmt.setString(2, career.getCompany_name());
			pstmt.setDate(3, career.getCompany_in_year());
			pstmt.setDate(4, career.getCompany_out_date());
			pstmt.setString(5, career.getJob_level());
			pstmt.setInt(6, career.getCareer_year());
			pstmt.setString(7, career.getCareer_kind());
			pstmt.setString(8, career.getCareer_area());
			pstmt.setString(9, career.getCareer_dept());
			pstmt.setString(10, career.getTask());
			
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		return result;
	}

	public int insertEucation(Connection con, Education edu) {
		// TODO Auto-generated method stub
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("insertEducation");
		try {
			pstmt=con.prepareStatement(query);
			pstmt.setInt(1,edu.getResume_no());
			pstmt.setString(2, edu.getSchool_name());
			pstmt.setString(3, edu.getMajor());
			pstmt.setDate(4, edu.getSchool_enroll_date());
			pstmt.setDate(5, edu.getSchool_graduate_date());
			pstmt.setString(6, edu.getEdu_choice());
			pstmt.setString(7,edu.getUni_type());
			pstmt.setString(8, edu.getUni_major());
			pstmt.setString(9, edu.getUni_major_name());
			pstmt.setDouble(10, edu.getUni_score_choice());
			pstmt.setDouble(11, edu.getUni_score());
			pstmt.setDate(12, edu.getUni_enroll_date());
			pstmt.setDate(13, edu.getUni_graduate_date());
			pstmt.setString(14, edu.getGraduate_type());
			pstmt.setString(15, edu.getUni_graduate_type());
			pstmt.setString(16, edu.getUni_name());
			
			
			result=pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		return result;
	}
	
	//이력서 조회용 메소드
		public ArrayList<Resume> selectResume(Connection con) {
			
			ArrayList<Resume> list = null;
			Statement stmt = null;
			ResultSet rset = null;
			
			String query = prop.getProperty("selectResume");
			
			try {
				stmt = con.createStatement();
				rset = stmt.executeQuery(query);
				
				list = new ArrayList<>();
				
				while(rset.next()) {
					Resume r = new Resume();
					r.setResume_no(rset.getInt("RESUME_NO"));
					r.setResume_title(rset.getString("RESUME_TITLE"));
					
					list.add(r);
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(stmt);
				close(rset);
			}
			
			return list;
		}

		public ArrayList<Resume> selectList(Connection con, int memberNo) {
			
			      
			      PreparedStatement pstmt=null;
			      ResultSet rset=null;
			      ArrayList<Resume> list=null;
			      
			      String query=prop.getProperty("selectResumeList");
			      
			      try {
			         pstmt=con.prepareStatement(query);
			         pstmt.setInt(1, memberNo);
			         
			         rset=pstmt.executeQuery();
			         
			         list=new ArrayList<Resume>();
			         while(rset.next()) {
			            Resume resume =new Resume();
			            
						resume.setResume_title(rset.getString("RESUME_TITLE"));
						resume.setResume_modify_date(rset.getDate("RESUME_MODIFY_DATE"));
						resume.setResume_no(rset.getInt("RESUME_NO"));
						resume.setComplete_yn(rset.getString("COMPLETE_YN"));
						resume.setResume_enroll_date(rset.getDate("RESUME_ENROLL_DATE"));
						resume.setRepresent_yn(rset.getString("REPRESENT_YN"));
						resume.setDelete_yn(rset.getString("DELETE_YN"));
			            resume.setResume_open_yn(rset.getString("RESUME_OPEN_YN"));
			            list.add(resume);
			         }
			         
			      } catch (SQLException e) {
			         // TODO Auto-generated catch block
			         e.printStackTrace();
			      }finally {
			         close(pstmt);
			         close(rset);
			      }
			     /* System.out.println("dao list:"+list);*/
			      return list;
			   }

		public HashMap<java.lang.String, java.lang.Object> resumeDetail(Connection con, int resumeNum) {
			// TODO Auto-generated method stub
			
			PreparedStatement pstmt = null;
			ResultSet rset = null;
			Resume resume = null;
			Career career = null;
			Education edu = null;
			Member member = null;
			HashMap <String,Object>hmap= null;
			ArrayList<Attachment> attlist = null;
			Attachment at = null;
			String query = prop.getProperty("resumeDetail");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, resumeNum);
				
				rset = pstmt.executeQuery();
				
				hmap=new HashMap<String,Object>();
				attlist = new ArrayList<Attachment>();
				
				
			
				while(rset.next()) {
					member = new Member();
					member.setMemberNo(rset.getInt("MEMBER_NO"));
					member.setMemberId(rset.getString("MEMBER_ID"));
					member.setMemberName(rset.getString("MEMBER_NAME"));
					member.setPhone(rset.getString("PHONE"));
					
					
					
					
					
					resume = new Resume();
					resume.setResume_no(rset.getInt("RESUME_NO"));
					resume.setResume_title(rset.getString("RESUME_TITLE"));
					resume.setRemail(rset.getString("REMAIL"));
					resume.setResume_open_yn(rset.getString("RESUME_OPEN_YN"));
					resume.setResume_enroll_date(rset.getDate("RESUME_ENROLL_DATE"));
					resume.setResume_modify_date(rset.getDate("RESUME_MODIFY_DATE"));
					resume.setRepresent_yn(rset.getString("REPRESENT_YN"));
					resume.setDelete_yn(rset.getString("DELETE_YN"));
					resume.setCareer_yn(rset.getString("CAREER_YN"));
					resume.setBirth_date(rset.getString("BIRTH_DATE"));
					resume.setRaddress(rset.getString("ADDRESS"));
					resume.setIntroducetitle(rset.getString("INTRODUCE_TITLE"));
					resume.setIntroducecontent(rset.getString("INTRODUCE_CONTENT"));
					resume.setInterestarea(rset.getString("INTEREST_AREA"));
					resume.setApply_field(rset.getString("APPLY_FIELD"));
					resume.setRmember_no(rset.getInt("MEMBER_NO"));
					resume.setComplete_yn(rset.getString("COMPLETE_YN"));
					resume.setGender(rset.getString("GENDER"));
					
					
					
					
					
					career = new Career();
					career.setCareer_no(rset.getInt("CAREER_NO"));
					career.setCompany_in_year(rset.getDate("COMPANY_IN_YEAR"));
					career.setCompany_out_date(rset.getDate("COMPANY_OUT_DATE"));
					career.setCareer_year(rset.getInt("CAREER_YEAR"));
					career.setCompany_name(rset.getString("COMPANY_NAME"));
					career.setCareer_dept(rset.getString("CAREER_DEPT"));
					career.setJob_level(rset.getString("JOB_LEVEL"));
					career.setCareer_kind(rset.getString("CAREER_KIND"));
					career.setCareer_area(rset.getString("CAREER_AREA"));
					career.setTask(rset.getString("TASK"));
					   
					   
					edu = new Education();
					
					edu.setEducation_no(rset.getInt("EDUCATION_NO"));
					edu.setSchool_name(rset.getString("SCHOOL_NAME"));
					edu.setMajor(rset.getString("MAJOR"));
					edu.setSchool_enroll_date(rset.getDate("SCHOOL_ENROLL_DATE"));
					edu.setSchool_graduate_date(rset.getDate("SCHOOL_GRADUATE_DATE"));
					edu.setEdu_choice(rset.getString("EDU_CHOICE"));
					edu.setUni_type(rset.getString("UNI_TYPE"));
					edu.setUni_major(rset.getString("UNI_MAJOR"));
					edu.setUni_major_name(rset.getString("UNI_MAJOR_NAME"));
					edu.setUni_score(rset.getDouble("UNI_SCORE"));
					edu.setUni_score_choice(rset.getDouble("UNI_SCORE_CHOICE"));
					edu.setUni_enroll_date(rset.getDate("UNI_ENROLL_DATE"));
					edu.setUni_graduate_date(rset.getDate("UNI_GRADUATE_DATE"));
					edu.setUni_graduate_type(rset.getString("UNI_GRADUATE_TYPE"));
					edu.setGraduate_type(rset.getString("GRADUATE_TYPE"));
					edu.setUni_name(rset.getString("UNI_NAME"));
					
					at = new Attachment();
					at.setAttachmentNo(rset.getInt("ATTACHMENT_NO"));
					at.setOriginName(rset.getString("ORIGIN_NAME"));
					at.setChangeName(rset.getString("CHANGE_NAME"));
					at.setFilePath(rset.getString("FILE_PATH"));
					at.setUploadDate(rset.getDate("UPLOAD_DATE"));
					
					attlist.add(at);
					
				}
				hmap.put("member", member);
				hmap.put("resume", resume);
				hmap.put("career", career);
				hmap.put("edu", edu);
				hmap.put("attlist", attlist);
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
				close(rset);
			}
			return hmap;
			
			
		}

		public HashMap<String, Object> resumeUpdateView(Connection con, int num) {
			// TODO Auto-generated method stub
			PreparedStatement pstmt = null;
			ResultSet rset = null;
			Resume resume = null;
			Career career = null;
			Education edu = null;
			Member member = null;
			HashMap <String,Object>hmap= null;
			
			String query = prop.getProperty("resumeUpdateView");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, num);
				
				rset = pstmt.executeQuery();
				
				hmap=new HashMap<String,Object>();
				
				
				
			
				while(rset.next()) {
					member = new Member();
					member.setMemberNo(rset.getInt("MEMBER_NO"));
					member.setMemberId(rset.getString("MEMBER_ID"));
					member.setMemberName(rset.getString("MEMBER_NAME"));
					member.setPhone(rset.getString("PHONE"));
					
					
					
					
					
					resume = new Resume();
					resume.setResume_no(rset.getInt("RESUME_NO"));
					resume.setResume_title(rset.getString("RESUME_TITLE"));
					resume.setRemail(rset.getString("REMAIL"));
					resume.setResume_open_yn(rset.getString("RESUME_OPEN_YN"));
					resume.setResume_enroll_date(rset.getDate("RESUME_ENROLL_DATE"));
					resume.setResume_modify_date(rset.getDate("RESUME_MODIFY_DATE"));
					resume.setRepresent_yn(rset.getString("REPRESENT_YN"));
					resume.setDelete_yn(rset.getString("DELETE_YN"));
					resume.setCareer_yn(rset.getString("CAREER_YN"));
					resume.setBirth_date(rset.getString("BIRTH_DATE"));
					resume.setRaddress(rset.getString("ADDRESS"));
					resume.setIntroducetitle(rset.getString("INTRODUCE_TITLE"));
					resume.setIntroducecontent(rset.getString("INTRODUCE_CONTENT"));
					resume.setInterestarea(rset.getString("INTEREST_AREA"));
					resume.setApply_field(rset.getString("APPLY_FIELD"));
					resume.setRmember_no(rset.getInt("MEMBER_NO"));
					resume.setGender(rset.getString("GENDER"));
					
					
					
					
					
					career = new Career();
					career.setCareer_no(rset.getInt("CAREER_NO"));
					career.setCompany_in_year(rset.getDate("COMPANY_IN_YEAR"));
					career.setCompany_out_date(rset.getDate("COMPANY_OUT_DATE"));
					career.setCareer_year(rset.getInt("CAREER_YEAR"));
					career.setCompany_name(rset.getString("COMPANY_NAME"));
					career.setCareer_dept(rset.getString("CAREER_DEPT"));
					career.setJob_level(rset.getString("JOB_LEVEL"));
					career.setCareer_kind(rset.getString("CAREER_KIND"));
					career.setCareer_area(rset.getString("CAREER_AREA"));
					career.setTask(rset.getString("TASK"));
					   
					   
					edu = new Education();
					
					edu.setEducation_no(rset.getInt("EDUCATION_NO"));
					edu.setSchool_name(rset.getString("SCHOOL_NAME"));
					edu.setMajor(rset.getString("MAJOR"));
					edu.setSchool_enroll_date(rset.getDate("SCHOOL_ENROLL_DATE"));
					edu.setSchool_graduate_date(rset.getDate("SCHOOL_GRADUATE_DATE"));
					edu.setEdu_choice(rset.getString("EDU_CHOICE"));
					edu.setUni_type(rset.getString("UNI_TYPE"));
					edu.setUni_major(rset.getString("UNI_MAJOR"));
					edu.setUni_major_name(rset.getString("UNI_MAJOR_NAME"));
					edu.setUni_score(rset.getDouble("UNI_SCORE"));
					edu.setUni_score_choice(rset.getDouble("UNI_SCORE_CHOICE"));
					edu.setUni_enroll_date(rset.getDate("UNI_ENROLL_DATE"));
					edu.setUni_graduate_date(rset.getDate("UNI_GRADUATE_DATE"));
					edu.setUni_graduate_type(rset.getString("UNI_GRADUATE_TYPE"));
					edu.setGraduate_type(rset.getString("UNI_GRADUATE_TYPE"));
					edu.setUni_name(rset.getString("UNI_NAME"));
					
					
				}
				hmap.put("member", member);
				hmap.put("resume", resume);
				hmap.put("career", career);
				hmap.put("edu", edu);
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
				close(rset);
			}
			return hmap;
		}

		public int updateResume(Connection con, Resume resume) {
			// TODO Auto-generated method stub
			PreparedStatement pstmt = null;
			int result = 0;
			String query = prop.getProperty("updateResume");
			
			try {
				pstmt = con.prepareStatement(query);
				//pstmt.setInt(1, resume.getResume_no());
				pstmt.setString(1, resume.getResume_title());
				pstmt.setString(2, resume.getBirth_date());
				pstmt.setString(3, resume.getRemail());
				pstmt.setString(4, resume.getRaddress());
				pstmt.setString(5, resume.getIntroducetitle());
				pstmt.setString(6, resume.getIntroducecontent());
				pstmt.setString(7, resume.getInterestarea());
				pstmt.setString(8, resume.getApply_field());
				pstmt.setString(9, resume.getResume_open_yn());
				//pstmt.setDate(10, resume.getResume_enroll_date());
				//pstmt.setDate(11, resume.getResume_modify_date());
				//pstmt.setString(12, resume.getRepresent_yn());
				//pstmt.setString(13, resume.getDelete_yn());
				pstmt.setString(10, resume.getCareer_yn());
				//pstmt.setInt(10, resume.getRmember_no());
				pstmt.setInt(11, resume.getResume_no());
				pstmt.setString(12,resume.getGender());
				result=pstmt.executeUpdate();
				
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
			}
			
			return result;
		}

		public int updateCareer(Connection con, Career career) {
			// TODO Auto-generated method stub
			PreparedStatement pstmt = null;
			int result = 0;
			String query = prop.getProperty("updateCareer");
			
			try {
				pstmt=con.prepareStatement(query);
				pstmt.setString(1, career.getCompany_name());
				pstmt.setDate(2, career.getCompany_in_year());
				pstmt.setDate(3, career.getCompany_out_date());
				pstmt.setString(4, career.getJob_level());
				pstmt.setInt(5, career.getCareer_year());
				pstmt.setString(6, career.getCareer_kind());
				pstmt.setString(7, career.getCareer_area());
				pstmt.setString(8, career.getCareer_dept());
				pstmt.setString(9, career.getTask());
				pstmt.setInt(10,career.getResume_no());
				
				result=pstmt.executeUpdate();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);  
			}
			return result;
		}

		public int updateEucation(Connection con, Education edu) {
			// TODO Auto-generated method stub
			PreparedStatement pstmt = null;
			int result = 0;
			String query = prop.getProperty("updateEducation");
			try {
				pstmt=con.prepareStatement(query);
				pstmt.setString(1, edu.getSchool_name());
				pstmt.setString(2, edu.getMajor());
				pstmt.setDate(3, edu.getSchool_enroll_date());
				pstmt.setDate(4, edu.getSchool_graduate_date());
				pstmt.setString(5, edu.getEdu_choice());
				pstmt.setString(6,edu.getUni_type());
				pstmt.setString(7, edu.getUni_major());
				pstmt.setString(8, edu.getUni_major_name());
				pstmt.setDouble(9, edu.getUni_score_choice());
				pstmt.setDouble(10, edu.getUni_score());
				pstmt.setDate(11, edu.getUni_enroll_date());
				pstmt.setDate(12, edu.getUni_graduate_date());
				pstmt.setString(13, edu.getGraduate_type());
				pstmt.setString(14, edu.getUni_graduate_type());
				pstmt.setString(15, edu.getUni_name());
				pstmt.setInt(16,edu.getResume_no());
				
				result=pstmt.executeUpdate();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
			}
			return result;
		}

		public HashMap<String, Object> userMyPage(Connection con, int memberNo) {
		      HashMap<String,Object>hmap=null;
			  PreparedStatement pstmt=null;
		      ResultSet rset=null;
		      ArrayList<Resume> list=null;
		      Member member = null;
		      Resume resume = null;
		      String query=prop.getProperty("userMyPage");
		      
		      try {
		         pstmt=con.prepareStatement(query);
		         pstmt.setInt(1, memberNo);
		         
		         rset=pstmt.executeQuery();
		         
		         hmap=new HashMap<String,Object>();
		         list=new ArrayList<Resume>();
		         while(rset.next()) {
		        	member = new Member();
		        	member.setMemberNo(rset.getInt("MEMBER_NO"));
		        	member.setMemberName(rset.getString("MEMBER_NAME"));
		        	member.setPhone(rset.getString("PHONE"));
		        	member.setEmail(rset.getString("EMAIL"));
		        	
		        	
		            resume = new Resume();
		            
					resume.setResume_title(rset.getString("RESUME_TITLE"));
					resume.setResume_modify_date(rset.getDate("RESUME_MODIFY_DATE"));
					resume.setResume_no(rset.getInt("RESUME_NO"));
		            resume.setComplete_yn(rset.getString("COMPLETE_YN"));
					resume.setRepresent_yn(rset.getString("REPRESENT_YN"));
					resume.setResume_enroll_date(rset.getDate("RESUME_ENROLL_DATE"));
					resume.setDelete_yn(rset.getString("DELETE_YN"));
		            list.add(resume);
		            
		         }
		         hmap.put("resumeMember", member);
		         hmap.put("resumeList", list);
		         
		      } catch (SQLException e) {
		         // TODO Auto-generated catch block
		         e.printStackTrace();
		      }finally {
		         close(pstmt);
		         close(rset);
		      }
		      
		      return hmap;
		   }

		public int deleteResume(Connection con, int num) {
			// TODO Auto-generated method stub
			PreparedStatement pstmt = null;
			int result = 0;
			
			String query = prop.getProperty("deleteResume");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, num);
				
				result = pstmt.executeUpdate();
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
			}
			
			return result;
		}

		public int representYResume(Connection con, int num) {
			// TODO Auto-generated method stub
			PreparedStatement pstmt = null;
			int result = 0;
			
			String query = prop.getProperty("representYResume");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, num);
				
				result = pstmt.executeUpdate();
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
			}
			
			return result;
		}

		public int representNResume(Connection con, int num) {
			// TODO Auto-generated method stub
			PreparedStatement pstmt = null;
			int result = 0;
			
			String query = prop.getProperty("representNResume");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, num);
				
				result = pstmt.executeUpdate();
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
			}
			
			return result;
		}

		public int representResume1(Connection con, Resume resume) {
			// TODO Auto-generated method stub
			PreparedStatement pstmt = null;
			int result = 0;
			
			String query = prop.getProperty("represent1Resume");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, resume.getRmember_no());
				
				result = pstmt.executeUpdate();
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
			}
			
			return result;
		}

		public int representResume2(Connection con, Resume resume) {
			// TODO Auto-generated method stub
			PreparedStatement pstmt = null;
			int result = 0;
			
			String query = prop.getProperty("represent2Resume");
			
			try {
				pstmt = con.prepareStatement(query);
				
				pstmt.setInt(1, resume.getResume_no());
				
				result = pstmt.executeUpdate();
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
			}
			
			return result;
		}

		public int ResumeSave(Connection con, Resume resume) {
			// TODO Auto-generated method stub
			PreparedStatement pstmt = null;
			int result = 0;
			String query = prop.getProperty("ResumeSave");
			
			try {
				pstmt = con.prepareStatement(query);
				//pstmt.setInt(1, resume.getResume_no());
				pstmt.setString(1, resume.getResume_title());
				pstmt.setString(2, resume.getBirth_date());
				pstmt.setString(3, resume.getRemail());
				pstmt.setString(4, resume.getRaddress());
				pstmt.setString(5, resume.getIntroducetitle());
				pstmt.setString(6, resume.getIntroducecontent());
				pstmt.setString(7, resume.getInterestarea());
				pstmt.setString(8, resume.getApply_field());
				pstmt.setString(9, resume.getResume_open_yn());
				
				//pstmt.setDate(10, resume.getResume_enroll_date());
				//pstmt.setDate(11, resume.getResume_modify_date());
				//pstmt.setString(12, resume.getRepresent_yn());
				//pstmt.setString(13, resume.getDelete_yn());
				pstmt.setString(10, resume.getCareer_yn());
				
				pstmt.setInt(11, resume.getRmember_no());
				pstmt.setString(12,resume.getGender());
				
				result=pstmt.executeUpdate();
				
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
			}
			
			return result;
		}

		public int selectCurrval1(Connection con) {
			// TODO Auto-generated method stub
			Statement stmt = null;
			ResultSet rset = null;
			int resume_no=0;
			String query=prop.getProperty("selectCurrval");
			try {
				stmt=con.createStatement();
				rset= stmt.executeQuery(query);
				
				if(rset.next()) {
					resume_no=rset.getInt("currval");
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(stmt);
				close(rset);
			}
			
			return resume_no;
		}
		
		public int CareerSave(Connection con, Career career) {
			// TODO Auto-generated method stub
			PreparedStatement pstmt = null;
			int result = 0;
			String query = prop.getProperty("CareerSave");
			
			try {
				pstmt=con.prepareStatement(query);
				pstmt.setInt(1,career.getResume_no());
				pstmt.setString(2, career.getCompany_name());
				pstmt.setDate(3, career.getCompany_in_year());
				pstmt.setDate(4, career.getCompany_out_date());
				pstmt.setString(5, career.getJob_level());
				pstmt.setInt(6, career.getCareer_year());
				pstmt.setString(7, career.getCareer_kind());
				pstmt.setString(8, career.getCareer_area());
				pstmt.setString(9, career.getCareer_dept());
				pstmt.setString(10, career.getTask());
				
				result=pstmt.executeUpdate();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
			}
			return result;
		}

		public int EducationSave(Connection con, Education edu) {
			// TODO Auto-generated method stub
			PreparedStatement pstmt = null;
			int result = 0;
			String query = prop.getProperty("EducationSave");
			try {
				pstmt=con.prepareStatement(query);
				pstmt.setInt(1,edu.getResume_no());
				pstmt.setString(2, edu.getSchool_name());
				pstmt.setString(3, edu.getMajor());
				pstmt.setDate(4, edu.getSchool_enroll_date());
				pstmt.setDate(5, edu.getSchool_graduate_date());
				pstmt.setString(6, edu.getEdu_choice());
				pstmt.setString(7,edu.getUni_type());
				pstmt.setString(8, edu.getUni_major());
				pstmt.setString(9, edu.getUni_major_name());
				pstmt.setDouble(10, edu.getUni_score_choice());
				pstmt.setDouble(11, edu.getUni_score());
				pstmt.setDate(12, edu.getUni_enroll_date());
				pstmt.setDate(13, edu.getUni_graduate_date());
				pstmt.setString(14, edu.getGraduate_type());
				pstmt.setString(15, edu.getUni_graduate_type());
				pstmt.setString(16, edu.getUni_name());
				
				
				result=pstmt.executeUpdate();
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
			}
			return result;
		}

		public int insertAttachment(Connection con, Attachment attachment) {
			// TODO Auto-generated method stub
			PreparedStatement pstmt = null;
			int result = 0;
			
			String query = prop.getProperty("insertAttachment1");

			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, attachment.getBoardNo());
				pstmt.setString(2, attachment.getOriginName());
				pstmt.setString(3, attachment.getChangeName());
				pstmt.setString(4, attachment.getFilePath());
				
				result = pstmt.executeUpdate();
			
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
			}
			
			return result;
		}

		public int AttachmentSave(Connection con, Attachment attachment) {
			// TODO Auto-generated method stub
			PreparedStatement pstmt = null;
			int result = 0;
			
			String query = prop.getProperty("Attachment1Save");

			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, attachment.getBoardNo());
				pstmt.setString(2, attachment.getOriginName());
				pstmt.setString(3, attachment.getChangeName());
				pstmt.setString(4, attachment.getFilePath());
				
				result = pstmt.executeUpdate();
			
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
			}
			
			return result;
		}

		public ArrayList<ResumeInterview> resumeinterview(Connection con, int memberNo) {
			// TODO Auto-generated method stub
			  PreparedStatement pstmt=null;
		      ResultSet rset=null;
		      ArrayList<ResumeInterview> ilist=null;
		      
		      String query=prop.getProperty("resumeinterview");
		      
		      try {
		         pstmt=con.prepareStatement(query);
		         pstmt.setInt(1, memberNo);
		         
		         rset=pstmt.executeQuery();
		         
		         ilist=new ArrayList<ResumeInterview>();
		         while(rset.next()) {
		            ResumeInterview ri =new ResumeInterview();
		            ri.setBmember_no(rset.getInt("BMEMBER_NO"));
		            ri.setBusiness_content(rset.getString("BUSINESS_CONTENT"));
		            ri.setCompany_name(rset.getString("COMPANY_NAME"));
		            ri.setInterview_no(rset.getInt("INTERVIEW_NO"));
		            ri.setOpen_period_end(rset.getDate("OPEN_PERIOD_END"));
		            ri.setOpen_period_start(rset.getDate("OPEN_PERIOD_START"));
		            ri.setOpen_status(rset.getString("OPEN_STATUS"));
		            ri.setRecruit_no(rset.getInt("RECRUIT_NO"));
		            ri.setRequest_date(rset.getDate("REQUEST_DATE"));
		            ri.setResume_no(rset.getInt("RESUME_NO"));
		            
					/*resume.setResume_title(rset.getString("RESUME_TITLE"));
					resume.setResume_modify_date(rset.getDate("RESUME_MODIFY_DATE"));
					resume.setResume_no(rset.getInt("RESUME_NO"));
					resume.setComplete_yn(rset.getString("COMPLETE_YN"));
					resume.setResume_enroll_date(rset.getDate("RESUME_ENROLL_DATE"));
					resume.setRepresent_yn(rset.getString("REPRESENT_YN"));
					resume.setDelete_yn(rset.getString("DELETE_YN"));
		            resume.setResume_open_yn(rset.getString("RESUME_OPEN_YN"));*/
		            ilist.add(ri);
		         }
		         
		      } catch (SQLException e) {
		         // TODO Auto-generated catch block
		         e.printStackTrace();
		      }finally {
		         close(pstmt);
		         close(rset);
		      }
		     /* System.out.println("dao list:"+list);*/
		      return ilist;
		   }

		/*public ArrayList<Recruit1> searchlimit(Connection con, String company_name) {
			// TODO Auto-generated method stub
			ArrayList<Recruit1> slist = null;
			PreparedStatement pstmt = null;
			ResultSet rset = null;
			
			String query = prop.getProperty("searchLimit");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setString(1, company_name);
				rset = pstmt.executeQuery();
				
				slist = new ArrayList<Recruit1>();
				
				while(rset.next()) {

					Recruit1 rec = new Recruit1();
					rec.setBmember_no(rset.getInt("BMEMBER_NO"));
		            rec.setBusiness_content(rset.getString("BUSINESS_CONTENT"));
		            rec.setCompany_name(rset.getString("COMPANY_NAME"));
		            rec.setInterview_no(rset.getInt("INTERVIEW_NO"));
		            rec.setOpen_period_end(rset.getDate("OPEN_PERIOD_END"));
		            rec.setOpen_period_start(rset.getDate("OPEN_PERIOD_START"));
		            rec.setOpen_status(rset.getString("OPEN_STATUS"));
		            rec.setRecruit_no(rset.getInt("RECRUIT_NO"));
		            rec.setRequest_date(rset.getDate("REQUEST_DATE"));
		            rec.setResume_no(rset.getInt("RESUME_NO"));
					rec.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
					slist.add(rec);
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
				close(rset);
			}
			
			return slist;
		}*/

		public ArrayList<Recruit1> searchlimit(Connection con) {
			// TODO Auto-generated method stub
			ArrayList<Recruit1> slist=null;
			Statement stmt=null;
			ResultSet rset=null;
			
			String query=prop.getProperty("limitPage");
			
			try {
				stmt=con.createStatement();
				rset=stmt.executeQuery(query);
				
				slist=new ArrayList<>();
			
				while(rset.next()) {
					Recruit1 rec = new Recruit1();
					rec.setBmember_no(rset.getInt("BMEMBER_NO"));
		            rec.setBusiness_content(rset.getString("BUSINESS_CONTENT"));
		            rec.setCompany_name(rset.getString("COMPANY_NAME"));
		            rec.setInterview_no(rset.getInt("INTERVIEW_NO"));
		            rec.setOpen_period_end(rset.getDate("OPEN_PERIOD_END"));
		            rec.setOpen_period_start(rset.getDate("OPEN_PERIOD_START"));
		            rec.setOpen_status(rset.getString("OPEN_STATUS"));
		            rec.setRecruit_no(rset.getInt("RECRUIT_NO"));
		            rec.setRequest_date(rset.getDate("REQUEST_DATE"));
		            rec.setResume_no(rset.getInt("RESUME_NO"));
					rec.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
					slist.add(rec);
				}
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(stmt);
				close(rset);
			}		
			return slist;
		}
	}