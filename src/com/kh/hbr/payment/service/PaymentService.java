package com.kh.hbr.payment.service;

import static com.kh.hbr.common.JDBCTemplate.close;
import static com.kh.hbr.common.JDBCTemplate.commit;
import static com.kh.hbr.common.JDBCTemplate.getConnection;
import static com.kh.hbr.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.kh.hbr.payment.model.dao.PaymentDao;
import com.kh.hbr.payment.model.vo.ItemCount;
import com.kh.hbr.payment.model.vo.ItemUse;
import com.kh.hbr.payment.model.vo.MainItem;
import com.kh.hbr.payment.model.vo.MyItem;
import com.kh.hbr.payment.model.vo.PageInfo;
import com.kh.hbr.payment.model.vo.Purchase;
import com.kh.hbr.payment.model.vo.Refund;

public class PaymentService {

	//아이템을 구매하여 DB테이블에 값 insert
	public int insertPayment(Purchase requestPurchase) {
		
		Connection con = getConnection();
		
		int result = new PaymentDao().insertPayment(con, requestPurchase);
		int result2 = 0;
		
		if(result > 0) {
			
			int purchaseNo = new PaymentDao().findPurchaseNo(con);
			
			if(purchaseNo > 0) {
				result2 = new PaymentDao().insertNewItem(con, requestPurchase, purchaseNo);
				commit(con);
			}
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result2;
	}

	public ArrayList<MyItem> loadMyItem(int memberNo) {
		Connection con = getConnection();
		
		ArrayList<MyItem> itemList = new PaymentDao().loadMyItem(con, memberNo);
		
		close(con);
		
		return itemList;
	}

	public HashMap<String, ItemCount> loadItemCount(int bMemberNo) {
		Connection con = getConnection();
		
		HashMap<String, ItemCount> itemMap = new PaymentDao().loadItemCount(con, bMemberNo);
		
		close(con);
		
		return itemMap;
	}

	public ArrayList<Purchase> loadMyPaymentList(PageInfo pi, int bMemberNo) {
		Connection con = getConnection();
		
		ArrayList<Purchase> myPaymentList = new PaymentDao().loadMyPaymentList(con, pi, bMemberNo);
		
		close(con);
		
		return myPaymentList;
	}

	public boolean updateRefundResult(int refundNo, int myItemNo, int purchaseNo) {
		Connection con = getConnection();
		
		boolean result = false;
		
		int updateResult = new PaymentDao().updateRefundResult(con, refundNo);
		int changeStatusResult = 0;
		int changePurchaseStatus = 0;
		
		if(updateResult > 0) {
			
			//Refund테이블의 업데이트 결과가 성공하면 다음 단계 수행
			//MYITEM테이블의 아이템 상태값을 환불로 변경
			commit(con);
			changeStatusResult = new PaymentDao().changeStatusResult(con, myItemNo);
			
			if(changeStatusResult > 0) {
				
				//MYITEM테이블의 아이템 상태값이 변경되면 커밋 수행후 다음단계로 이동.
				commit(con);
				changePurchaseStatus = new PaymentDao().changePurchaseStatus(con, purchaseNo);
				
				if(changePurchaseStatus > 0) {
					//마지막 PURCHASE테이블에서도 상태를 변경하는데 성공하면 최종결과를 리턴.
					//최종결과는 의미는 없지만 확인하기위해서 찍어줌
					commit(con);
					result = true;
				} else {
					rollback(con);
				}
				
			} else {
				rollback(con);
			}
			
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public int getListCount() {
		Connection con = getConnection();
		
		int result = new PaymentDao().getListcount(con);
		
		close(con);
		
		return result;
	}

	public ArrayList<Purchase> loadAdminPaymentList(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<Purchase> purList = new PaymentDao().loadAdminPaymentList(con, pi);
		
		close(con);
		
		return purList;
	}

	public int getListCount(String searchValue) {
		Connection con = getConnection();
		
		int result = new PaymentDao().getListcount(con, searchValue);
		
		close(con);
		
		return result;
	}

	public ArrayList<Purchase> loadSearchAdminPaymentList(PageInfo pi, String searchValue) {
		
		Connection con = getConnection();
		
		ArrayList<Purchase> purList = new PaymentDao().loadAdminPaymentList(con, pi, searchValue);
		
		close(con);
		
		return purList;
	}

	public int getRefundListCount(int condition, String value) {
		
		Connection con = getConnection();
			
		int result = new PaymentDao().getRefundListCount(con, condition, value);
		
		close(con);
		
		return result;
	}

	//개인의 환불신청내역 조회
	public ArrayList<Refund> loadRefundList(PageInfo pi, int bMemberNo) {
		Connection con = getConnection();
		
		ArrayList<Refund> refundList = new PaymentDao().loadRefundList(con, pi, bMemberNo);
		
		close(con);
		
		return refundList;
	}
	
	//공고테이블에서 내 아이템 목록을 불러오는 메소드
	public ArrayList<MyItem> loadMyItemOnRecruit(int bMemberNo, String productValue) {
		Connection con = getConnection();
		
		ArrayList<MyItem> itemList = new PaymentDao().loadMyItemOnRecruit(con, bMemberNo, productValue);
		
		close(con);
		
		return itemList;
	}
	
	//공고테이블에서 적용한 아이템을 인서트
	public void insertItem(String noFirst, String countFirst, String noSecond, String countSecond) {
		Connection con = getConnection();
		
		int itemNoFirst;
		int itemCountFirst;
		int itemInsert1;
		
		//현재 공고번호 조회
		int recruitNo = new PaymentDao().loadRecruitNo(con);
		
		if(!noFirst.equals("")) {
			itemNoFirst = Integer.parseInt(noFirst);
			itemCountFirst = Integer.parseInt(countFirst);
			
			itemInsert1 = new PaymentDao().insertMyItem(con, recruitNo, itemNoFirst, itemCountFirst);
			
			if(itemInsert1 > 0) {
				commit(con);
			}else {
				rollback(con);
			}
		}
		
		int itemNoSecond;
		int itemCountSecond;
		int itemInsert2;
		
		if(!noSecond.equals("")) {
			itemNoSecond = Integer.parseInt(noSecond);
			itemCountSecond = Integer.parseInt(countSecond);
			
			itemInsert2 = new PaymentDao().insertMyItem(con, recruitNo, itemNoSecond, itemCountSecond);
			
			if(itemInsert2 > 0) {
				commit(con);
			}else {
				rollback(con);
			}
		}
		
	}

	public int getItemListCount(int memberNo) {
		Connection con = getConnection();
		
		int count = new PaymentDao().getItemListCount(con, memberNo);
		
		close(con);
		
		return count;
	}

	public ArrayList<MyItem> loadMyItem(PageInfo pi, int memberNo) {
		Connection con = getConnection();
		
		ArrayList<MyItem> list = new PaymentDao().loadMyItem(con, pi, memberNo);
		
		close(con);
		
		return list;
	}

	public int getItemSearchListCount(int bNo, String search) {
		Connection con = getConnection();
		
		int count = new PaymentDao().getItemSearchListCount(con, search, bNo);
		
		close(con);
		
		return count;
	}

	public ArrayList<MyItem> loadMyItemSearch(PageInfo pi, int bNo, String search) {
		Connection con = getConnection();
		
		ArrayList<MyItem> list = new PaymentDao().loadMyItemSearch(con, pi, bNo, search);
		
		close(con);
		
		return list;
	}

	public int getItemUseListCount(int memberNo) {
		Connection con = getConnection();
		
		int count = new PaymentDao().getItemUseListCount(con, memberNo);
		
		close(con);
		
		return count;
	}

	public ArrayList<ItemUse> loadMyItemUse(PageInfo pi, int memberNo) {
		Connection con = getConnection();
		
		ArrayList<ItemUse> list = new PaymentDao().loadMyItemUse(con, pi, memberNo);
		
		close(con);
		
		return list;
	}

	public int getPaymentListCount(int memberNo) {
		Connection con = getConnection();
		
		int count = new PaymentDao().getPaymentListCount(con, memberNo);
		
		close(con);
		
		return count;
	}

	public int getRefundListCount(int bMemberNo) {
		Connection con = getConnection();
		
		int count = new PaymentDao().getRefundListCount(con, bMemberNo);
		
		close(con);
		
		return count;
	}

	public int getAdminItemUseListCount() {
		Connection con = getConnection();
		
		int count = new PaymentDao().getAdminItemUseListCount(con);
		
		close(con);
			
		return count;
	}

	public ArrayList<ItemUse> loadAdminItemUse(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList<ItemUse> list = new PaymentDao().loadAdminItemUse(con, pi);
		
		close(con);
		
		return list;
	}

	public ArrayList<ArrayList<MainItem>> loadUserMainPageItem() {
		Connection con = getConnection();
		ArrayList<ArrayList<MainItem>> itemList = new ArrayList();
		
		for(int i = 0; i <= 3; i++) {
			
			String value = "";
			
			switch(i) {
				case 0 : value = "G"; break;
				case 1 : value = "S"; break;
				case 2 : value = "L"; break;
				case 3 : value = "T"; break;
			}
			
			ArrayList<MainItem> list = new PaymentDao().loadUserMainPageItem(con, value);
			
			itemList.add(list);
		}
		
		close(con);
		
		return itemList;
	}

	public boolean loadFavStar(int memberNo, int recruitNo, String kind) {
		Connection con = getConnection();
		
		boolean yn = new PaymentDao().loadFavStar(con, memberNo, recruitNo, kind);
		
		close(con);
		
		return yn;
	}

	public String handleFavStar(int favRecruitNo, int favMemberNo, String kind) {
		Connection con = getConnection();
		String finalYn = "";
		//즐겨찾기가 DB에 있는지 파악
		boolean favYn = new PaymentDao().loadFavStar(con, favMemberNo, favRecruitNo, kind);
		
		if(favYn) {
			//업데이트 처리 Y면 N으로 N이면 Y로
			
			String updateValue = new PaymentDao().loadFavUpdateValue(con, favMemberNo, favRecruitNo, kind);
			
			if(updateValue.equals("Y")) {
				updateValue = "N";
			}else {
				updateValue = "Y";
			}
			
			int upResult = new PaymentDao().updateFavStar(con, favMemberNo, favRecruitNo, updateValue, kind);
			
			if(upResult > 0) {
				commit(con);
				finalYn = updateValue;
			}else {
				rollback(con);
			}
			
		}else {
			//데이터 인서트
			int result = new PaymentDao().insertLoadFavStar(con, favMemberNo, favRecruitNo, kind);
			finalYn = "Y";
			
			if(result > 0) {
				commit(con);
			}else {
				rollback(con);
			}
		}
		
		return finalYn;	
	}

	public String loadFavStarYN(int memberNo, int recruitNo, String kind) {
		Connection con = getConnection();
		
		String yn = new PaymentDao().loadFavStarYN(con, memberNo, recruitNo, kind);
		
		close(con);
		
		return yn;
	}
	
	
	
	
	
	
	
	
	
	
	



}
