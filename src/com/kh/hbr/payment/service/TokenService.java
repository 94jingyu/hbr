package com.kh.hbr.payment.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class TokenService {
	
	public String getToken(
							HttpServletRequest request 
							,HttpServletResponse response
							,JSONObject json
							,String requestURL) throws Exception {

		// requestURL 아임포트 고유키, 시크릿 키 정보를 포함하는 URL 정보 
		String requestToken = "";

		try{

			String requestString = "";
			
			//String의 URL주소를 전달받아 URL개게 생성
			URL url = new URL(requestURL);

			//HttpURLConnection 생성 및 연결
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			//Server 통신에서 출력 가능한 상태로 만듬
			connection.setDoOutput(true); 		
			
			connection.setInstanceFollowRedirects(false);  
			
			//Request의 방식설정 : POST방식
			connection.setRequestMethod("POST");

			//Request의 타입을 설정
			connection.setRequestProperty("Content-Type", "application/json");
			
			//출력을 위한 스트림 생성
			OutputStream os= connection.getOutputStream();

			os.write(json.toString().getBytes());

			connection.connect();

			StringBuilder sb = new StringBuilder(); 

			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {

				BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

				String line = null;  

				while ((line = br.readLine()) != null) {  

					sb.append(line + "\n");  

				}

				br.close();

				requestString = sb.toString();
			}

			os.flush();

			connection.disconnect();

			JSONParser jsonParser = new JSONParser();

			JSONObject jsonObj = (JSONObject) jsonParser.parse(requestString);

			if((Long)jsonObj.get("code")  == 0){

				JSONObject getToken = (JSONObject) jsonObj.get("response");

				System.out.println("getToken==>>"+ getToken.get("access_token") );

				requestToken = (String)getToken.get("access_token");
			}

		}catch(Exception e){

			e.printStackTrace();

			requestToken = "";

		}

		return requestToken;
	}
	
	
	
}
