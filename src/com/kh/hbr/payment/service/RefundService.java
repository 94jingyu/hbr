package com.kh.hbr.payment.service;

import static com.kh.hbr.common.JDBCTemplate.close;
import static com.kh.hbr.common.JDBCTemplate.commit;
import static com.kh.hbr.common.JDBCTemplate.getConnection;
import static com.kh.hbr.common.JDBCTemplate.rollback;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.kh.hbr.payment.model.dao.PaymentDao;
import com.kh.hbr.payment.model.vo.PageInfo;
import com.kh.hbr.payment.model.vo.Purchase;
import com.kh.hbr.payment.model.vo.Refund;

public class RefundService {

	public boolean refundPayment(
			HttpServletRequest request
			, HttpServletResponse response
			, JSONObject requestRefund
			, String url) {
		
		boolean result = false;
		
		try {
			URL requestUrl = new URL(url);
			
			HttpURLConnection conn = (HttpURLConnection) requestUrl.openConnection();
			
			conn.setDoOutput(true);
			
			conn.setInstanceFollowRedirects(false);
			
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-type", "application/json");
			conn.setRequestProperty("Authorization", (String) requestRefund.get("token"));
			
			//출력을 위한 스트림 생성
			OutputStream os = conn.getOutputStream();
			os.write(requestRefund.toString().getBytes());
			
			conn.connect();
			
			if(conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				result = true;
			}
			
			os.flush();
			
			conn.disconnect();
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public void insertRefundRequest(Refund requestRefund) {
		Connection con = getConnection();
		
		int result = new PaymentDao().insertRefundRequest(con, requestRefund);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
	}

	public Purchase loadOneMyPayment(int purchaseNo) {
		Connection con = getConnection();
		
		Purchase pur = new PaymentDao().loadOneMyPayment(con, purchaseNo); 
		
		close(con);
		
		return pur;
	}

	public ArrayList<Refund> loadAdminRefundList(PageInfo pi, int condition, String value) {
		ArrayList<Refund> refundList = null;
		Connection con = getConnection();
		
		refundList = new PaymentDao().loadAdminRefundList(con, pi, condition, value);
		
		close(con);
		
		return refundList;
	}

	public Refund loadRefundImpUid(int refundNo) {
		Connection con = getConnection();
		
		Refund ref = new PaymentDao().loadRefundImpUid(con, refundNo);
		
		close(con);
		
		return ref;
	}

	public int refuseRefund(Refund ref) {
		Connection con = getConnection();
		
		int result = new PaymentDao().refuseRefund(con, ref);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}
	
	
	
}
