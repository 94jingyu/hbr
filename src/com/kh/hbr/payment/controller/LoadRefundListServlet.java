package com.kh.hbr.payment.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.payment.model.vo.MyItem;
import com.kh.hbr.payment.model.vo.PageInfo;
import com.kh.hbr.payment.model.vo.Refund;
import com.kh.hbr.payment.service.PaymentService;

@WebServlet("/refundList.pay")
public class LoadRefundListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoadRefundListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		int bMemberNo = ((Business)request.getSession().getAttribute("loginUser")).getBmemberNo();
		
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		int limit = 10;
		int maxPage;
		int startPage;
		int endPage;
		
		int listCount = new PaymentService().getRefundListCount(bMemberNo);
		
		maxPage = (int) ((double) listCount / limit + 0.9);
		
		startPage = (((int) ((double) currentPage / limit + 0.9)) - 1) * 10 + 1;
		endPage = startPage + 10 - 1;
		
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
			
		ArrayList<Refund> refundList = new PaymentService().loadRefundList(pi, bMemberNo);
		
		String page = "";
		
		if(refundList != null) {
			page = "views/payment/refundList.jsp";
			request.setAttribute("refundList", refundList);
			request.setAttribute("pi", pi);
			request.getRequestDispatcher(page).forward(request, response);
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "환불내역조회실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
