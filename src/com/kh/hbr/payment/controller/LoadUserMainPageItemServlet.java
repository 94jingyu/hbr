package com.kh.hbr.payment.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kh.hbr.payment.model.vo.MainItem;
import com.kh.hbr.payment.service.PaymentService;

@WebServlet("/loadUserMainPageItem")
public class LoadUserMainPageItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoadUserMainPageItemServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ArrayList<ArrayList<MainItem>> itemList = new PaymentService().loadUserMainPageItem();
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		
		new Gson().toJson(itemList, response.getWriter());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
