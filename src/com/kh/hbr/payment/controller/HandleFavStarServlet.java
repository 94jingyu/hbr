package com.kh.hbr.payment.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kh.hbr.payment.service.PaymentService;

@WebServlet("/handleFavStar")
public class HandleFavStarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public HandleFavStarServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int favRecruitNo = Integer.parseInt(request.getParameter("favRecruitNo"));
		int favMemberNo = Integer.parseInt(request.getParameter("favMemberNo"));
		String kind = request.getParameter("kind");
	
		String yn = new PaymentService().handleFavStar(favRecruitNo, favMemberNo, kind);
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		
		new Gson().toJson(yn, response.getWriter());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
