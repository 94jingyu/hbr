package com.kh.hbr.payment.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.payment.model.vo.MyItem;
import com.kh.hbr.payment.model.vo.PageInfo;
import com.kh.hbr.payment.service.PaymentService;

@WebServlet("/searchMyItem")
public class SearchMyItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SearchMyItemServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int value = Integer.parseInt(request.getParameter("value"));
		int bNo = Integer.parseInt(request.getParameter("bNo"));
		
		String search = "";
		
		switch(value) {
		
		case 1 : request.getRequestDispatcher("/myItem.load").forward(request, response);; break;
		case 2 : search = "G"; break;
		case 3 : search = "S"; break;
		case 4 : search = "L"; break;
		case 5 : search = "T"; break;
		case 6 : search = "R"; break;
		}
		
		if(value != 1) {
			int currentPage = 1;
			
			if(request.getParameter("currentPage") != null) {
				currentPage = Integer.parseInt(request.getParameter("currentPage"));
			}
	
			int limit = 10;
			int maxPage;
			int startPage;
			int endPage;
			
			int listCount = new PaymentService().getItemSearchListCount(bNo, search);
			
			maxPage = (int) ((double) listCount / limit + 0.9);
			
			startPage = (((int) ((double) currentPage / limit + 0.9)) - 1) * 10 + 1;
			endPage = startPage + 10 - 1;
			
			if(maxPage < endPage) {
				endPage = maxPage;
			}
			
			PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
				
			ArrayList<MyItem> itemList = new PaymentService().loadMyItemSearch(pi, bNo, search);
			
			String page = "";
			if(itemList != null) {
				page = "views/payment/itemList.jsp";
				request.setAttribute("pi", pi);
				request.setAttribute("itemList", itemList);
			}else {
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg", "아이템 조회에 실패하였습니다.");
			}
			request.getRequestDispatcher(page).forward(request, response);
		
		}
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
