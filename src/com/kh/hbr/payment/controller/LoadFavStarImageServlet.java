package com.kh.hbr.payment.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kh.hbr.payment.service.PaymentService;

@WebServlet("/loadFavStar")
public class LoadFavStarImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoadFavStarImageServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int memberNo = Integer.parseInt(request.getParameter("memberNo"));
		int recruitNo = Integer.parseInt(request.getParameter("recruitNo"));
		String kind = request.getParameter("kind");
		
		boolean yn = new PaymentService().loadFavStar(memberNo, recruitNo, kind);
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		new Gson().toJson(yn, response.getWriter());
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
