package com.kh.hbr.payment.controller;

import java.util.ArrayList;

public class SelectQueryMakerTest {
	public static void main(String[] args) {
		
		ArrayList<String> cols = new ArrayList<>();
		cols.add("EMP_ID");
		cols.add("EMP_NAME");
		cols.add("EMP_NO");
		
		SelectQueryMaker builder = 
				new SelectQueryMaker
					.Builder()
						.select().columns(cols)
						.from().tableName("EMPLOYEE").as("E")
						.join().tableName("BOARD").as("B").on("E.EID = B.WRITER")
						.where().columnName("SALARY").operator("=").value(3000000)
						.and().columnName("EMP_ID").operator("<>").value("?")
						.or().columnName("EMP_NAME").operator("IN").value("(?, ?)")
					.build();
		
		System.out.println(builder.getQuery());
		
		
	}
}
