package com.kh.hbr.payment.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.payment.model.vo.MyItem;
import com.kh.hbr.payment.model.vo.PageInfo;
import com.kh.hbr.payment.model.vo.Purchase;
import com.kh.hbr.payment.service.PaymentService;

@WebServlet("/myPaymentList.load")
public class LoadMyPaymentListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoadMyPaymentListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int bMemberNo = ((Business) request.getSession().getAttribute("loginUser")).getBmemberNo();
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		int memberNo = ((Business) (request.getSession().getAttribute("loginUser"))).getBmemberNo();
		
		int limit = 10;
		int maxPage;
		int startPage;
		int endPage;
		
		int listCount = new PaymentService().getPaymentListCount(memberNo);
		
		maxPage = (int) ((double) listCount / limit + 0.9);
		
		startPage = (((int) ((double) currentPage / limit + 0.9)) - 1) * 10 + 1;
		endPage = startPage + 10 - 1;
		
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
			
		ArrayList<Purchase> requestPurchase = new PaymentService().loadMyPaymentList(pi, bMemberNo);
		
		if(requestPurchase != null) {
			request.setAttribute("requestPurchase", requestPurchase);
			request.setAttribute("pi", pi);
			request.getRequestDispatcher("views/payment/paymentList.jsp").forward(request, response);
		}else {
			request.setAttribute("msg", "결제내역 조회 실패!");
			request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
 