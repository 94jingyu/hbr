package com.kh.hbr.payment.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.payment.model.vo.Refund;
import com.kh.hbr.payment.service.RefundService;

@WebServlet("/refuseRefund.pay")
public class RefuseRefundServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RefuseRefundServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		int refundNo = Integer.parseInt(request.getParameter("refundNo"));
		String refuseReason = request.getParameter("refuseReason");
		
		Refund ref = new Refund();
		ref.setRefundNo(refundNo);
		ref.setRefuseReason(refuseReason);
		
		int result = new RefundService().refuseRefund(ref);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
