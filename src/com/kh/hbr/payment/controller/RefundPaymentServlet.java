package com.kh.hbr.payment.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.kh.hbr.payment.service.PaymentService;
import com.kh.hbr.payment.service.RefundService;

@WebServlet("/refund.pay")
public class RefundPaymentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RefundPaymentServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = request.getParameter("url");
		String token = request.getParameter("accessToken");
		String imp_uid = request.getParameter("imp_uid");
		
		JSONObject requestRefund = new JSONObject();
		requestRefund.put("token", URLEncoder.encode(token, "UTF-8"));
		requestRefund.put("imp_uid", URLEncoder.encode(imp_uid, "UTF-8"));
		
		
		boolean result = new RefundService().refundPayment(request, response, requestRefund, url);
		
		int refundNo = Integer.parseInt(request.getParameter("refundNo"));
		int myItemNo = Integer.parseInt(request.getParameter("myItemNo"));
		int purchaseNo = Integer.parseInt(request.getParameter("purchaseNo"));
		
		//1. 환불에 성공할 경우 환불 테이블의 처리결과 업데이트
		if(result) {
			
			boolean otherResult = new PaymentService().updateRefundResult(refundNo, myItemNo, purchaseNo);
		
		}
		
		String finalResult = "";
		
		if(result) {
			
			finalResult = "true";
			
		}else {
			
			finalResult = "false";
			
		}
		
		PrintWriter out = response.getWriter();
		
		out.print(finalResult);
		out.flush();
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
