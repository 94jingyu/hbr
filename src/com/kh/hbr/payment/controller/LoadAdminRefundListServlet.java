package com.kh.hbr.payment.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.payment.model.vo.PageInfo;
import com.kh.hbr.payment.model.vo.Refund;
import com.kh.hbr.payment.service.PaymentService;
import com.kh.hbr.payment.service.RefundService;

@WebServlet("/adminRefundList.pay")
public class LoadAdminRefundListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoadAdminRefundListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String former = request.getParameter("former");
		String value = request.getParameter("value");
		
		if(former == null) {
			former = "1";
		}
		
		switch(former) {
			case "2" : value = "Y"; break;
			case "3" : value = "N"; break;
		}
		
		int condition = Integer.parseInt(former);
		
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		limit = 10;
		
		int listCount = new PaymentService().getRefundListCount(condition, value);
		
		maxPage = (int) ((double) listCount / limit + 0.9);
		
		startPage = (((int) ((double) currentPage / limit + 0.9)) - 1) * 10 + 1;
		
		endPage = startPage + 10 - 1;
		
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		ArrayList<Refund> refundList = new RefundService().loadAdminRefundList(pi, condition, value);
		
		String page = "";
		if(refundList != null) {
			page = "views/payment/admin_refundList.jsp";
			request.setAttribute("refundList", refundList);
			request.setAttribute("pi", pi);
			request.getRequestDispatcher(page).forward(request, response);
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "리스트조회실패");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
