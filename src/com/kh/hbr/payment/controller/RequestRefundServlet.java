package com.kh.hbr.payment.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.payment.model.vo.Refund;
import com.kh.hbr.payment.service.RefundService;

@WebServlet("/reqRefund.pay")
public class RequestRefundServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RequestRefundServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		int myItemNo = Integer.parseInt(request.getParameter("myItemNo"));
		int purchaseNo = Integer.parseInt(request.getParameter("purchaseNo"));
		String approveNo = request.getParameter("approveNo");
		int memberNo = Integer.parseInt(request.getParameter("bMemberNo"));
		String refundReason = "";
		
		switch(request.getParameter("refundReason")) {
		case "1" : refundReason = "단순변심"; break;
		case "2" : refundReason = "실수로구매"; break;
		default : refundReason = request.getParameter("refundReason").split("\\$")[1]; break;
		}
		
		Refund requestRefund = new Refund();
		requestRefund.setMyItemNo(myItemNo);
		requestRefund.setPurchaseNo(purchaseNo);
		requestRefund.setApproveNo(approveNo);
		requestRefund.setbMemberNo(memberNo);
		requestRefund.setRefundReason(refundReason);
		
		new RefundService().insertRefundRequest(requestRefund);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
