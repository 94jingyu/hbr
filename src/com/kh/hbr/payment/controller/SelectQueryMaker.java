package com.kh.hbr.payment.controller;

import java.util.List;

public class SelectQueryMaker {
	private final StringBuilder query;
	
	public static class Builder {
		private final StringBuilder query;
		
		public Builder() {
			this.query = new StringBuilder();
		}
		
		public Builder select() {
			query.append("SELECT ");
			
			return this;
		}
		
		public Builder columns(List<String> columns) {
			if(columns != null && columns.size() != 0) {
				for(int i = 0; i < columns.size(); i++) {
					if(i == columns.size() - 1) {
						query.append(columns.get(i)).append(" ");
					} else {
						query.append(columns.get(i)).append(", ");
					}
				}
			} 
			
			return this;
		}
		
		public Builder from() {
			query.append("FROM ");
			
			return this; 
		}
		
		public Builder tableName(String tableName) {
			query.append(tableName + " ");
			
			return this;
		}
		
		public Builder as(String alias) {
			query.append(alias + " ");
			
			return this;
		}
		
		public Builder join() {
			query.append("JOIN ");
			
			return this;
		}
		
		public Builder on(String condition) {
			query.append("ON (" + condition + ") ");
			
			return this;
		}
		
		public Builder where() {
			query.append("WHERE ");
			
			return this;
		}
		
		public Builder columnName(String colName) {
			query.append(colName + " ");
			
			return this;
		}
		
		public Builder operator(String op) {
			query.append(op + " ");
			
			return this;
		}

		public Builder value(Object value) {
			query.append(value + " ");
			
			return this;
		}
		
		public Builder and() {
			query.append("AND ");
			
			return this;
		}
		
		public Builder or() {
			query.append("OR ");
			
			return this;
		}
		
		public SelectQueryMaker build() {
			return new SelectQueryMaker(this);
		}
		
		
	}
	
	private SelectQueryMaker(Builder builder) {
		this.query = builder.query;
	}
	
	public StringBuilder getQuery() {
		return query;
	}
}
