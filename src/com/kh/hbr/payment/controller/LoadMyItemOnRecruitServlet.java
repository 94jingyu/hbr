package com.kh.hbr.payment.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.kh.hbr.payment.model.vo.MyItem;
import com.kh.hbr.payment.service.PaymentService;

@WebServlet("/loadMyItem.recruit")
public class LoadMyItemOnRecruitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoadMyItemOnRecruitServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int bMemberNo = Integer.parseInt(request.getParameter("bMemberNo"));
		String productValue = request.getParameter("productValue");
		
		switch(productValue) {
			case "1" : productValue = "G"; break;
			case "2" : productValue = "S"; break;
			case "3" : productValue = "L"; break;
			case "4" : productValue = "T"; break;
		}
		
		//공고테이블에서 내 아이템 목록 불러오는 메소드
		ArrayList<MyItem> itemList = new PaymentService().loadMyItemOnRecruit(bMemberNo, productValue);
		
		JSONObject json = null;
		JSONArray jArray = new JSONArray();
		
		for(MyItem myitem : itemList) {
			json = new JSONObject();
			
			json.put("myItemNo", myitem.getMyItemNo());
			json.put("bMemberNO", myitem.getbMemberNo());
			json.put("purchaseNo", myitem.getPurchaseNo());
			json.put("myItemUnit", myitem.getMyItemUnit());
			json.put("myItemExpDate", URLEncoder.encode(myitem.getMyItemExpDate().toString(), "UTF-8"));
			json.put("productCode", URLEncoder.encode(myitem.getProductCode(), "UTF-8"));
			json.put("purchaseDate", URLEncoder.encode(myitem.getPurchaseDate().toString(), "UTF-8"));
			json.put("productCount", myitem.getProductCount());
			
			jArray.add(json);
		}
		
		response.setContentType("application/json");
		
		PrintWriter out = response.getWriter();
		out.print(jArray.toJSONString());
		
		out.flush();
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
