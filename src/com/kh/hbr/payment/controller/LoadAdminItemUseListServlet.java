package com.kh.hbr.payment.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.payment.model.vo.ItemUse;
import com.kh.hbr.payment.model.vo.PageInfo;
import com.kh.hbr.payment.service.PaymentService;

@WebServlet("/loadAdminItemUse")
public class LoadAdminItemUseListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoadAdminItemUseListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		int limit = 10;
		int maxPage;
		int startPage;
		int endPage;
		
		int listCount = new PaymentService().getAdminItemUseListCount();
		
		maxPage = (int) ((double) listCount / limit + 0.9);
		
		startPage = (((int) ((double) currentPage / limit + 0.9)) - 1) * 10 + 1;
		endPage = startPage + 10 - 1;
		
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
			
		ArrayList<ItemUse> useList = new PaymentService().loadAdminItemUse(pi);
	
		System.out.println(useList);
		
		String page = "";
		
		if(useList != null) {
			page = "views/payment/admin_itemUseList.jsp";
			request.setAttribute("pi", pi);
			request.setAttribute("useList", useList);
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "아이템목록 조회실패!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
