package com.kh.hbr.payment.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.google.gson.JsonObject;
import com.kh.hbr.payment.service.TokenService;

@WebServlet("/token.get")
public class GetAccessTokenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public GetAccessTokenServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String requestURL = "https://api.iamport.kr/users/getToken";
		String imp_key = URLEncoder.encode("8396172427876188", "UTF-8");
		String imp_secret = URLEncoder.encode("mUim174QsZuzLHY2sMK3VETo85tTkyIHs14u88kA7PaTw9VYWG2RlVft1Jnmh53J67degyPonZhU9dS5", "UTF-8");
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("imp_key", imp_key);
		jsonObj.put("imp_secret", imp_secret);
		
		String token = null;
		try {
		
			token = new TokenService().getToken(request, response, jsonObj, requestURL);
	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		PrintWriter out = response.getWriter();
		out.print(token);
		
		out.flush();
		out.close();
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
