package com.kh.hbr.payment.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.payment.model.vo.PageInfo;
import com.kh.hbr.payment.model.vo.Purchase;
import com.kh.hbr.payment.service.PaymentService;

@WebServlet("/searchAdminPaymentList.pay")
public class SearchAdminPaymentListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SearchAdminPaymentListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String searchValue = request.getParameter("searchValue");
		//페이징처리까지 완료한 리스트 호출
		//페이징에 필요한 변수 선언
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		//한페이지에 보여질 목록개수 설정
		limit = 10;
		
		//maxPage를 계산하기 위해 게시물 전체 개수를 조회해야함.
		int listCount = new PaymentService().getListCount(searchValue);
		
		//총페이지 수 계산
		maxPage = (int) ((double) listCount / limit + 0.9);
		
		//현재 페이지에 보여줄 시작페이지 수 계산
		startPage = (((int) ((double) currentPage / limit + 0.9)) - 1) * 10 + 1;
		
		//아래쪽에 보여질 마지막 페이지 수
		endPage = startPage + 10 - 1;
		
		//끝페이지가 최종페이지보다 클경우
		//끝페이지에 최종페이지를 대입하여 숫자를 맞춤
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		ArrayList<Purchase> purList = new PaymentService().loadSearchAdminPaymentList(pi, searchValue);
		
		String page = "";
		if(purList != null) {
			page = "views/payment/admin_searchPayment.jsp";
			request.setAttribute("pi", pi);
			request.setAttribute("purList", purList);
			request.setAttribute("searchValue", searchValue);
			request.getRequestDispatcher(page).forward(request, response);
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "결제내역 조회 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
