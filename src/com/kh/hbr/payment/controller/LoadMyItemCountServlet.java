
package com.kh.hbr.payment.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.payment.model.vo.ItemCount;
import com.kh.hbr.payment.service.PaymentService;

@WebServlet("/itemCount.pay")
public class LoadMyItemCountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoadMyItemCountServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		int bMemberNo = ((Business) (request.getSession().getAttribute("loginUser"))).getBmemberNo();
		
		HashMap<String, ItemCount> countMap = new PaymentService().loadItemCount(bMemberNo);
		
		JSONObject result = new JSONObject();
		result.put("G", countMap.get("item").getGoldCount());
		result.put("S", countMap.get("item").getSilverCount());
		result.put("L", countMap.get("item").getSteelCount());
		result.put("T", countMap.get("item").getStarCount());
		result.put("R", countMap.get("item").getResumeCount());
		
		PrintWriter out = response.getWriter();
		out.print(result.toJSONString());
		out.flush();
		out.close();
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
