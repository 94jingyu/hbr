package com.kh.hbr.payment.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.kh.hbr.payment.model.vo.Purchase;
import com.kh.hbr.payment.service.RefundService;

@WebServlet("/loadOneMyPayment.pay")
public class LoadOneMyPaymentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoadOneMyPaymentServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int purchaseNo = Integer.parseInt(request.getParameter("purchaseNo"));
		
		Purchase pur = new RefundService().loadOneMyPayment(purchaseNo);
		
		JSONObject json = new JSONObject();
		json.put("bMemberNo", pur.getbMemberNo());
		json.put("purchaseNo", pur.getPurchaseNo());
		json.put("myItemNo", pur.getMyItemNo());
		json.put("approveNo", URLEncoder.encode(pur.getApproveNo(), "UTF-8"));
		
		JSONArray jArray = new JSONArray();
		jArray.add(json);
		
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.print(jArray.toJSONString());
		
		out.flush();
		out.close();
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
