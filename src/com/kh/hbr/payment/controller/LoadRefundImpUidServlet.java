package com.kh.hbr.payment.controller;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.kh.hbr.payment.model.vo.Refund;
import com.kh.hbr.payment.service.RefundService;

@WebServlet("/impUid.pay")
public class LoadRefundImpUidServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoadRefundImpUidServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		int refundNo = Integer.parseInt(request.getParameter("refundNo"));
		
		Refund ref = new RefundService().loadRefundImpUid(refundNo); 
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		new Gson().toJson(ref, response.getWriter());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
