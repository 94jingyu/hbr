package com.kh.hbr.payment.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.payment.model.vo.Purchase;
import com.kh.hbr.payment.service.PaymentService;

@WebServlet("/buyProduct.pay")
public class BuyProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public BuyProductServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int bMemberNo = Integer.parseInt(request.getParameter("bMemberNo"));
		String productCode = request.getParameter("productCode");
		String approveNo = request.getParameter("approveNo");
		
		Purchase requestPurchase = new Purchase();
		requestPurchase.setbMemberNo(bMemberNo);
		requestPurchase.setProductCode(productCode);
		requestPurchase.setApproveNo(approveNo);
		
		//구매테이블에 해당 값 insert
		int result = new PaymentService().insertPayment(requestPurchase);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
