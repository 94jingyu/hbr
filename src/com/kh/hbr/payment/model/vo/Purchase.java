package com.kh.hbr.payment.model.vo;

import java.sql.Date;

public class Purchase implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int purchaseNo;
	private String productCode;
	private String productName;
	private int bMemberNo;
	private Date purchaseDate;
	private String approveNo;
	private int price;
	private int salesUnit;
	private int myItemNo;
	private int myItemUnit;
	private String bMemberName;
	private String bMemberId;
	private String refundYn;
	
	public Purchase() {}

	public Purchase(int purchaseNo, String productCode, String productName, int bMemberNo, Date purchaseDate,
			String approveNo, int price, int salesUnit, int myItemNo, int myItemUnit, String bMemberName, String bMemberId
			, String refundYn) {
		super();
		this.purchaseNo = purchaseNo;
		this.productCode = productCode;
		this.productName = productName;
		this.bMemberNo = bMemberNo;
		this.purchaseDate = purchaseDate;
		this.approveNo = approveNo;
		this.price = price;
		this.salesUnit = salesUnit;
		this.myItemNo = myItemNo;
		this.myItemUnit = myItemUnit;
		this.bMemberName = bMemberName;
		this.bMemberId = bMemberId;
		this.refundYn = refundYn;
	}
	
	public String getRefundYn() {
		return refundYn;
	}

	public void setRefundYn(String refundYn) {
		this.refundYn = refundYn;
	}

	public String getbMemberId() {
		return bMemberId;
	}

	public void setbMemberId(String bMemberId) {
		this.bMemberId = bMemberId;
	}

	public String getbMemberName() {
		return bMemberName;
	}

	public void setbMemberName(String bMemberName) {
		this.bMemberName = bMemberName;
	}

	public int getMyItemNo() {
		return myItemNo;
	}

	public void setMyItemNo(int myItemNo) {
		this.myItemNo = myItemNo;
	}

	public int getPurchaseNo() {
		return purchaseNo;
	}

	public void setPurchaseNo(int purchaseNo) {
		this.purchaseNo = purchaseNo;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getbMemberNo() {
		return bMemberNo;
	}

	public void setbMemberNo(int bMemberNo) {
		this.bMemberNo = bMemberNo;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getApproveNo() {
		return approveNo;
	}

	public void setApproveNo(String approveNo) {
		this.approveNo = approveNo;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getSalesUnit() {
		return salesUnit;
	}

	public void setSalesUnit(int salesUnit) {
		this.salesUnit = salesUnit;
	}

	public int getMyItemUnit() {
		return myItemUnit;
	}

	public void setMyItemUnit(int myItemUnit) {
		this.myItemUnit = myItemUnit;
	}

	@Override
	public String toString() {
		return "Purchase [purchaseNo=" + purchaseNo + ", productCode=" + productCode + ", productName=" + productName
				+ ", bMemberNo=" + bMemberNo + ", purchaseDate=" + purchaseDate + ", approveNo=" + approveNo
				+ ", price=" + price + ", salesUnit=" + salesUnit + ", myItemNo=" + myItemNo + ", myItemUnit="
				+ myItemUnit + ", bMemberName=" + bMemberName + ", bMemberId=" + bMemberId + ", refundYn=" + refundYn
				+ "]";
	}
}
