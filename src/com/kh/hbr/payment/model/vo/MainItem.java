package com.kh.hbr.payment.model.vo;

import java.sql.Date;

public class MainItem implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private int useNo;
	private int myitemNo;
	private int target;
	private int useUnit;
	private Date useDate;
	private String stringUseDate;
	private String useYn;
	private String targetKind;
	private Date applyDate;
	private String stringApplyDate;
	private String recruitDeleteYn;
	private String companyName;
	private String recruitTitle;
	private Date expirationDate;
	private String stringExpirationDate;
	private String changeName;
	private String filePath;
	private String productCode;
	
	public MainItem() {}

	public MainItem(int useNo, int myitemNo, int target, int useUnit, Date useDate, String stringUseDate, String useYn,
			String targetKind, Date applyDate, String stringApplyDate, String recruitDeleteYn, String companyName,
			String recruitTitle, Date expirationDate, String stringExpirationDate, String changeName, String filePath, String productCode) {
		super();
		this.useNo = useNo;
		this.myitemNo = myitemNo;
		this.target = target;
		this.useUnit = useUnit;
		this.useDate = useDate;
		this.stringUseDate = stringUseDate;
		this.useYn = useYn;
		this.targetKind = targetKind;
		this.applyDate = applyDate;
		this.stringApplyDate = stringApplyDate;
		this.recruitDeleteYn = recruitDeleteYn;
		this.companyName = companyName;
		this.recruitTitle = recruitTitle;
		this.expirationDate = expirationDate;
		this.stringExpirationDate = stringExpirationDate;
		this.changeName = changeName;
		this.filePath = filePath;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public int getUseNo() {
		return useNo;
	}

	public void setUseNo(int useNo) {
		this.useNo = useNo;
	}

	public int getMyitemNo() {
		return myitemNo;
	}

	public void setMyitemNo(int myitemNo) {
		this.myitemNo = myitemNo;
	}

	public int getTarget() {
		return target;
	}

	public void setTarget(int target) {
		this.target = target;
	}

	public int getUseUnit() {
		return useUnit;
	}

	public void setUseUnit(int useUnit) {
		this.useUnit = useUnit;
	}

	public Date getUseDate() {
		return useDate;
	}

	public void setUseDate(Date useDate) {
		this.useDate = useDate;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getTargetKind() {
		return targetKind;
	}

	public void setTargetKind(String targetKind) {
		this.targetKind = targetKind;
	}

	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	public String getRecruitDeleteYn() {
		return recruitDeleteYn;
	}

	public void setRecruitDeleteYn(String recruitDeleteYn) {
		this.recruitDeleteYn = recruitDeleteYn;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getRecruitTitle() {
		return recruitTitle;
	}

	public void setRecruitTitle(String recruitTitle) {
		this.recruitTitle = recruitTitle;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getChangeName() {
		return changeName;
	}

	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getStringUseDate() {
		return stringUseDate;
	}

	public void setStringUseDate(String stringUseDate) {
		this.stringUseDate = stringUseDate;
	}

	public String getStringApplyDate() {
		return stringApplyDate;
	}

	public void setStringApplyDate(String stringApplyDate) {
		this.stringApplyDate = stringApplyDate;
	}

	public String getStringExpirationDate() {
		return stringExpirationDate;
	}

	public void setStringExpirationDate(String stringExpirationDate) {
		this.stringExpirationDate = stringExpirationDate;
	}

	@Override
	public String toString() {
		return "MainItem [useNo=" + useNo + ", myitemNo=" + myitemNo + ", target=" + target + ", useUnit=" + useUnit
				+ ", useDate=" + useDate + ", stringUseDate=" + stringUseDate + ", useYn=" + useYn + ", targetKind="
				+ targetKind + ", applyDate=" + applyDate + ", stringApplyDate=" + stringApplyDate
				+ ", recruitDeleteYn=" + recruitDeleteYn + ", companyName=" + companyName + ", recruitTitle="
				+ recruitTitle + ", expirationDate=" + expirationDate + ", stringExpirationDate=" + stringExpirationDate
				+ ", changeName=" + changeName + ", filePath=" + filePath + ", productCode=" + productCode + "]";
	}
}
