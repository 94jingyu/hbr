package com.kh.hbr.payment.model.vo;

import java.sql.Date;

public class ItemUse implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private int useNo;
	private int myItemNo;
	private int target;
	private int useUnit;
	private Date useDate;
	private String useYn;
	private String targetKind;
	private Date applyDate;
	private String productName;
	private String recruitTitle;
	
	public ItemUse() {}

	public ItemUse(int useNo, int myItemNo, int target, int useUnit, Date useDate, String useYn, String targetKind,
			Date applyDate, String productName, String recruitTitle) {
		super();
		this.useNo = useNo;
		this.myItemNo = myItemNo;
		this.target = target;
		this.useUnit = useUnit;
		this.useDate = useDate;
		this.useYn = useYn;
		this.targetKind = targetKind;
		this.applyDate = applyDate;
		this.productName = productName;
		this.recruitTitle = recruitTitle;
	}

	public int getUseNo() {
		return useNo;
	}

	public void setUseNo(int useNo) {
		this.useNo = useNo;
	}

	public int getMyItemNo() {
		return myItemNo;
	}

	public void setMyItemNo(int myItemNo) {
		this.myItemNo = myItemNo;
	}

	public int getTarget() {
		return target;
	}

	public void setTarget(int target) {
		this.target = target;
	}

	public int getUseUnit() {
		return useUnit;
	}

	public void setUseUnit(int useUnit) {
		this.useUnit = useUnit;
	}

	public Date getUseDate() {
		return useDate;
	}

	public void setUseDate(Date useDate) {
		this.useDate = useDate;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getTargetKind() {
		return targetKind;
	}

	public void setTargetKind(String targetKind) {
		this.targetKind = targetKind;
	}

	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getRecruitTitle() {
		return recruitTitle;
	}

	public void setRecruitTitle(String recruitTitle) {
		this.recruitTitle = recruitTitle;
	}

	@Override
	public String toString() {
		return "ItemUse [useNo=" + useNo + ", myItemNo=" + myItemNo + ", target=" + target + ", useUnit=" + useUnit
				+ ", useDate=" + useDate + ", useYn=" + useYn + ", targetKind=" + targetKind + ", applyDate="
				+ applyDate + ", productName=" + productName + ", recruitTitle=" + recruitTitle + "]";
	}
}
