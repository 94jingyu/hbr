package com.kh.hbr.payment.model.vo;

public class ItemCount implements java.io.Serializable{
	private int goldCount;
	private int silverCount;
	private int steelCount;
	private int starCount;
	private int resumeCount;
	
	public ItemCount() {}

	public ItemCount(int goldCount, int silverCount, int steelCount, int starCount, int resumeCount) {
		super();
		this.goldCount = goldCount;
		this.silverCount = silverCount;
		this.steelCount = steelCount;
		this.starCount = starCount;
		this.resumeCount = resumeCount;
	}

	public int getGoldCount() {
		return goldCount;
	}

	public void setGoldCount(int goldCount) {
		this.goldCount = goldCount;
	}

	public int getSilverCount() {
		return silverCount;
	}

	public void setSilverCount(int silverCount) {
		this.silverCount = silverCount;
	}

	public int getSteelCount() {
		return steelCount;
	}

	public void setSteelCount(int steelCount) {
		this.steelCount = steelCount;
	}

	public int getStarCount() {
		return starCount;
	}

	public void setStarCount(int starCount) {
		this.starCount = starCount;
	}

	public int getResumeCount() {
		return resumeCount;
	}

	public void setResumeCount(int resumeCount) {
		this.resumeCount = resumeCount;
	}

	@Override
	public String toString() {
		return "ItemCount [goldCount=" + goldCount + ", silverCount=" + silverCount + ", steelCount=" + steelCount
				+ ", starCount=" + starCount + ", resumeCount=" + resumeCount + "]";
	}
	
}	
