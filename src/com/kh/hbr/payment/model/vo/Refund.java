package com.kh.hbr.payment.model.vo;

import java.sql.Date;

public class Refund implements java.io.Serializable {

	private int refundNo;
	private int myItemNo;
	private int purchaseNo;
	private String approveNo;
	private int bMemberNo;
	private String refundReason;
	private Date refundApplyDate;
	private Date resultDate;
	private String refundResult;
	private String memberId;
	private String productName;
	private String refuseReason;

	public Refund() {}

	public int getRefundNo() {
		return refundNo;
	}

	public Refund(int refundNo, int myItemNo, int purchaseNo, String approveNo, int bMemberNo, String refundReason,
			Date refundApplyDate, Date resultDate, String refundResult, String memberId, String productName,
			String refuseReason) {
		super();
		this.refundNo = refundNo;
		this.myItemNo = myItemNo;
		this.purchaseNo = purchaseNo;
		this.approveNo = approveNo;
		this.bMemberNo = bMemberNo;
		this.refundReason = refundReason;
		this.refundApplyDate = refundApplyDate;
		this.resultDate = resultDate;
		this.refundResult = refundResult;
		this.memberId = memberId;
		this.productName = productName;
		this.refuseReason = refuseReason;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setRefundNo(int refundNo) {
		this.refundNo = refundNo;
	}

	public int getMyItemNo() {
		return myItemNo;
	}

	public void setMyItemNo(int myItemNo) {
		this.myItemNo = myItemNo;
	}

	public int getPurchaseNo() {
		return purchaseNo;
	}

	public void setPurchaseNo(int purchaseNo) {
		this.purchaseNo = purchaseNo;
	}

	public String getApproveNo() {
		return approveNo;
	}

	public void setApproveNo(String approveNo) {
		this.approveNo = approveNo;
	}

	public int getbMemberNo() {
		return bMemberNo;
	}

	public void setbMemberNo(int bMemberNo) {
		this.bMemberNo = bMemberNo;
	}

	public String getRefundReason() {
		return refundReason;
	}

	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}

	public Date getRefundApplyDate() {
		return refundApplyDate;
	}

	public void setRefundApplyDate(Date refundApplyDate) {
		this.refundApplyDate = refundApplyDate;
	}

	public Date getResultDate() {
		return resultDate;
	}

	public void setResultDate(Date resultDate) {
		this.resultDate = resultDate;
	}

	public String getRefundResult() {
		return refundResult;
	}

	public void setRefundResult(String refundResult) {
		this.refundResult = refundResult;
	}

	public String getRefuseReason() {
		return refuseReason;
	}

	public void setRefuseReason(String refuseReason) {
		this.refuseReason = refuseReason;
	}

	@Override
	public String toString() {
		return "Refund [refundNo=" + refundNo + ", myItemNo=" + myItemNo + ", purchaseNo=" + purchaseNo + ", approveNo="
				+ approveNo + ", bMemberNo=" + bMemberNo + ", refundReason=" + refundReason + ", refundApplyDate="
				+ refundApplyDate + ", resultDate=" + resultDate + ", refundResult=" + refundResult + ", memberId="
				+ memberId + ", productName=" + productName + ", refuseReason=" + refuseReason + "]";
	}
}
