package com.kh.hbr.payment.model.vo;

import java.sql.Date;

public class MyItem implements java.io.Serializable {
	private int myItemNo;
	private int bMemberNo;
	private int purchaseNo;
	private int myItemUnit;
	private Date myItemExpDate;
	private String myItemStatus;
	private String productCode;
	private String productName;
	private int productCount; //구매단위를 뜻함
	private Date purchaseDate;
	
	public MyItem() {}

	public MyItem(int myItemNo, int bMemberNo, int purchaseNo, int myItemUnit, Date myItemExpDate, String myItemStatus,
			String productCode, String productName, int productCount, Date purchaseDate) {
		super();
		this.myItemNo = myItemNo;
		this.bMemberNo = bMemberNo;
		this.purchaseNo = purchaseNo;
		this.myItemUnit = myItemUnit;
		this.myItemExpDate = myItemExpDate;
		this.myItemStatus = myItemStatus;
		this.productCode = productCode;
		this.productName = productName;
		this.productCount = productCount;
		this.purchaseDate = purchaseDate;
	}

	public int getMyItemNo() {
		return myItemNo;
	}

	public void setMyItemNo(int myItemNo) {
		this.myItemNo = myItemNo;
	}

	public int getbMemberNo() {
		return bMemberNo;
	}

	public void setbMemberNo(int bMemberNo) {
		this.bMemberNo = bMemberNo;
	}

	public int getPurchaseNo() {
		return purchaseNo;
	}

	public void setPurchaseNo(int purchaseNo) {
		this.purchaseNo = purchaseNo;
	}

	public int getMyItemUnit() {
		return myItemUnit;
	}

	public void setMyItemUnit(int myItemUnit) {
		this.myItemUnit = myItemUnit;
	}

	public Date getMyItemExpDate() {
		return myItemExpDate;
	}

	public void setMyItemExpDate(Date myItemExpDate) {
		this.myItemExpDate = myItemExpDate;
	}

	public String getMyItemStatus() {
		return myItemStatus;
	}

	public void setMyItemStatus(String myItemStatus) {
		this.myItemStatus = myItemStatus;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getProductCount() {
		return productCount;
	}

	public void setProductCount(int productCount) {
		this.productCount = productCount;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	@Override
	public String toString() {
		return "MyItem [myItemNo=" + myItemNo + ", bMemberNo=" + bMemberNo + ", purchaseNo=" + purchaseNo
				+ ", myItemUnit=" + myItemUnit + ", myItemExpDate=" + myItemExpDate + ", myItemStatus=" + myItemStatus
				+ ", productCode=" + productCode + ", productName=" + productName + ", productCount=" + productCount
				+ ", purchaseDate=" + purchaseDate + "]";
	}

}
