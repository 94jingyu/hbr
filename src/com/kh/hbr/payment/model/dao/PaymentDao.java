package com.kh.hbr.payment.model.dao;

import static com.kh.hbr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.kh.hbr.payment.model.vo.ItemCount;
import com.kh.hbr.payment.model.vo.ItemUse;
import com.kh.hbr.payment.model.vo.MainItem;
import com.kh.hbr.payment.model.vo.MyItem;
import com.kh.hbr.payment.model.vo.PageInfo;
import com.kh.hbr.payment.model.vo.Purchase;
import com.kh.hbr.payment.model.vo.Refund;

public class PaymentDao {
	private Properties prop = new Properties();
	
	
	public PaymentDao() {
		String filePath = PaymentDao.class.getResource("/sql/payment/payment-query.properties").getPath();
		try {
			prop.load(new FileReader(filePath));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public PaymentDao(Connection con, PageInfo pi, int memberNo) {
		// TODO Auto-generated constructor stub
	}

	public int insertPayment(Connection con, Purchase requestPurchase) {
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("insertPayment");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, requestPurchase.getProductCode());
			pstmt.setString(2, requestPurchase.getProductCode());
			pstmt.setInt(3, requestPurchase.getbMemberNo());
			pstmt.setString(4, requestPurchase.getApproveNo());
			result = pstmt.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	//구매번호 조회
	public int findPurchaseNo(Connection con) {
		int purchaseNo = 0;
		Statement stmt = null;
		String query = prop.getProperty("findPurchaseNO");
		ResultSet rset = null;
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			if(rset.next()) {
				purchaseNo = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return purchaseNo;
	}

	public int insertNewItem(Connection con, Purchase requestPurchase, int purchaseNo) {
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("insertNewItem");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, requestPurchase.getbMemberNo());
			pstmt.setInt(2, purchaseNo);
			pstmt.setInt(3, purchaseNo);
			pstmt.setInt(4, purchaseNo);
			pstmt.setString(5, requestPurchase.getProductCode());
			result = pstmt.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public ArrayList<MyItem> loadMyItem(Connection con, int memberNo) {
		ArrayList<MyItem> itemList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("loadMyItem");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberNo);
			rset = pstmt.executeQuery();
			itemList = new ArrayList<MyItem>();
			
			while(rset.next()) {
				MyItem item = new MyItem();
				item.setMyItemNo(rset.getInt("MYITEM_NO"));
				item.setbMemberNo(rset.getInt("BMEMBER_NO"));
				item.setPurchaseNo(rset.getInt("PURCHASE_NO"));
				item.setMyItemUnit(rset.getInt("MYITEM_UNIT"));
				item.setMyItemExpDate(rset.getDate("MYITEM_EXP_DATE"));
				item.setMyItemStatus(rset.getString("MYITEM_STATUS"));
				item.setProductCode(rset.getString("PRODUCT_CODE"));
				item.setProductName(rset.getString("PRODUCT_NAME"));
				item.setProductCount(rset.getInt("SALES_UNIT"));
				item.setPurchaseDate(rset.getDate("PURCHASE_DATE"));
				
				itemList.add(item);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return itemList;
	}

	public HashMap<String, ItemCount> loadItemCount(Connection con, int bMemberNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String query = prop.getProperty("loadItemCount");
		HashMap<String, ItemCount> itemMap = null;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, bMemberNo);
			pstmt.setInt(2, bMemberNo);
			pstmt.setInt(3, bMemberNo);
			pstmt.setInt(4, bMemberNo);
			pstmt.setInt(5, bMemberNo);
			rset = pstmt.executeQuery();
			
			itemMap = new HashMap<String, ItemCount>();
			
			if(rset.next()) {
				
				ItemCount item = new ItemCount();
				item.setGoldCount(rset.getInt("G"));
				item.setSilverCount(rset.getInt("S"));
				item.setSteelCount(rset.getInt("L"));
				item.setStarCount(rset.getInt("T"));
				item.setResumeCount(rset.getInt("R"));
				
				itemMap.put("item", item);
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return itemMap;
	}

	public ArrayList<Purchase> loadMyPaymentList(Connection con, PageInfo pi, int bMemberNo) {
		ArrayList<Purchase> myPaymentList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("loadMyPaymentList");
		int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
		int endRow = startRow + pi.getLimit() - 1;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, bMemberNo);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			rset = pstmt.executeQuery();
			
			myPaymentList = new ArrayList<Purchase>();
			
			while(rset.next()) {
				Purchase pur = new Purchase();
				pur.setPurchaseNo(rset.getInt("PURCHASE_NO"));
				pur.setProductCode(rset.getString("PRODUCT_CODE"));
				pur.setProductName(rset.getString("PRODUCT_NAME"));
				pur.setSalesUnit(rset.getInt("SALES_UNIT"));
				pur.setPurchaseDate(rset.getDate("PURCHASE_DATE"));
				pur.setPrice(rset.getInt("PRICE"));
				pur.setMyItemNo(rset.getInt("MYITEM_NO"));
				pur.setApproveNo(rset.getString("APPROVE_NO"));
				pur.setMyItemUnit(rset.getInt("MYITEM_UNIT"));
				pur.setRefundYn(rset.getString("REFUND_YN"));
				
				myPaymentList.add(pur);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return myPaymentList;
	}

	public int insertRefundRequest(Connection con, Refund requestRefund) {
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("insertRefundRequest");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, requestRefund.getMyItemNo());
			pstmt.setInt(2, requestRefund.getPurchaseNo());
			pstmt.setString(3, requestRefund.getApproveNo());
			pstmt.setInt(4, requestRefund.getbMemberNo());
			pstmt.setString(5, requestRefund.getRefundReason());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public Purchase loadOneMyPayment(Connection con, int purchaseNo) {

		Purchase pur = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("loadOneMyPayment");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, purchaseNo);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				pur = new Purchase();
				pur.setbMemberNo(rset.getInt("BMEMBER_NO"));
				pur.setPurchaseNo(rset.getInt("PURCHASE_NO"));
				pur.setMyItemNo(rset.getInt("MYITEM_NO"));
				pur.setApproveNo(rset.getString("APPROVE_NO"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return pur;
	}

	public ArrayList<Refund> loadAdminRefundList(Connection con, PageInfo pi, int condition, String value) {
		ArrayList<Refund> refundList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Refund ref = null;
		
		String query = "";
		
		int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
		int endRow = startRow + pi.getLimit() - 1;
		
		if(condition == 1) {
			query = prop.getProperty("loadAdminRefundList");
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, startRow);
				pstmt.setInt(2, endRow);
				
				rset = pstmt.executeQuery();
				
				refundList = new ArrayList<Refund>();
				while(rset.next()) {
					ref = new Refund();
					ref.setRefundNo(rset.getInt("REFUND_NO"));
					ref.setMyItemNo(rset.getInt("MYITEM_NO"));
					ref.setPurchaseNo(rset.getInt("PURCHASE_NO"));
					ref.setApproveNo(rset.getString("APPROVE_NO"));
					ref.setbMemberNo(rset.getInt("BMEMBER_NO"));
					ref.setRefundReason(rset.getString("REFUND_REASON"));
					ref.setRefundApplyDate(rset.getDate("REFUND_APPLY_DATE"));
					ref.setResultDate(rset.getDate("REFUND_RESULT_DATE"));
					ref.setRefundResult(rset.getString("REFUND_RESULT"));
					ref.setProductName(rset.getString("PRODUCT_NAME"));
					ref.setMemberId(rset.getString("MANAGER_NAME"));
					
					refundList.add(ref);
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
				close(rset);
			}
			
			
			
		}else {
			query = prop.getProperty("loadAdminRefundListWithValue");
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, startRow);
				pstmt.setInt(2, endRow);
				pstmt.setString(3, value);
				
				rset = pstmt.executeQuery();
				
				refundList = new ArrayList<Refund>();
				while(rset.next()) {
					ref = new Refund();
					ref.setRefundNo(rset.getInt("REFUND_NO"));
					ref.setMyItemNo(rset.getInt("MYITEM_NO"));
					ref.setPurchaseNo(rset.getInt("PURCHASE_NO"));
					ref.setApproveNo(rset.getString("APPROVE_NO"));
					ref.setbMemberNo(rset.getInt("BMEMBER_NO"));
					ref.setRefundReason(rset.getString("REFUND_REASON"));
					ref.setRefundApplyDate(rset.getDate("REFUND_APPLY_DATE"));
					ref.setResultDate(rset.getDate("REFUND_RESULT_DATE"));
					ref.setRefundResult(rset.getString("REFUND_RESULT"));
					ref.setProductName(rset.getString("PRODUCT_NAME"));
					ref.setMemberId(rset.getString("MANAGER_NAME"));
					
					refundList.add(ref);
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
				close(rset);
			}
			
			
		}
		
		return refundList;
	}

	public Refund loadRefundImpUid(Connection con, int refundNo) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Refund ref = null;
		String query = prop.getProperty("loadRefundImpUid");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, refundNo);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				ref = new Refund();
				ref.setRefundNo(rset.getInt("REFUND_NO"));
				ref.setMyItemNo(rset.getInt("MYITEM_NO"));
				ref.setPurchaseNo(rset.getInt("PURCHASE_NO"));
				ref.setApproveNo(rset.getString("APPROVE_NO"));
				ref.setbMemberNo(rset.getInt("BMEMBER_NO"));
				ref.setRefundReason(rset.getString("REFUND_REASON"));
				ref.setRefundApplyDate(rset.getDate("REFUND_APPLY_DATE"));
				ref.setResultDate(rset.getDate("REFUND_RESULT_DATE"));
				ref.setRefundResult(rset.getString("REFUND_RESULT"));
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return ref;
	}

	public int updateRefundResult(Connection con, int refundNo) {
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("updateRefundResult");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, refundNo);
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int changeStatusResult(Connection con, int myItemNo) {
		PreparedStatement pstmt = null;
		int changeResult = 0;
		String query = prop.getProperty("changeStatusResult");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, myItemNo);
			changeResult = pstmt.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return changeResult;
	}

	public int changePurchaseStatus(Connection con, int purchaseNo) {

		PreparedStatement pstmt = null;
		int changePurchaseStatus = 0;
		String query = prop.getProperty("changePurchaseStatus");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, purchaseNo);
			changePurchaseStatus = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return changePurchaseStatus;
	}

	public int refuseRefund(Connection con, Refund ref) {
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("refuseRefund");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, ref.getRefuseReason());
			pstmt.setInt(2, ref.getRefundNo());
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int getListcount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		String query = prop.getProperty("listCount");
		int result = 0;
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		
		return result;
	}

	public ArrayList<Purchase> loadAdminPaymentList(Connection con, PageInfo pi) {
		PreparedStatement pstmt = null;
		ArrayList<Purchase> purList = null;
		ResultSet rset = null;
		String query = prop.getProperty("loadAdminPaymentList");
		
		try {
			pstmt = con.prepareStatement(query);
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			purList = new ArrayList<Purchase>();
			
			while(rset.next()) {
				Purchase pur = new Purchase();
				pur.setPurchaseNo(rset.getInt("PURCHASE_NO"));
				pur.setbMemberId(rset.getString("BMEMBER_ID"));
				pur.setbMemberName(rset.getString("MANAGER_NAME"));
				pur.setProductName(rset.getString("PRODUCT_NAME"));
				pur.setSalesUnit(rset.getInt("SALES_UNIT"));
				pur.setPrice(rset.getInt("PRICE"));
				pur.setPurchaseDate(rset.getDate("PURCHASE_DATE"));

				purList.add(pur);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return purList;
	}

	public ArrayList<Purchase> searchAdminPayment(Connection con, String itemName, int condition, String searchValue) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Purchase> searchList = null;
		
		String queryA = prop.getProperty("searchAdminPaymentwithId");
		String queryB = prop.getProperty("searchAdminPaymentwithName");
		
		return searchList;
	}

	public int getListcount(Connection con, String searchValue) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String query = prop.getProperty("listCountWithValue");
		int result = 0;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchValue);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return result;
	}

	public ArrayList<Purchase> loadAdminPaymentList(Connection con, PageInfo pi, String searchValue) {
		
		PreparedStatement pstmt = null;
		ArrayList<Purchase> purList = null;
		ResultSet rset = null;
		String query = prop.getProperty("loadSearchAdminPaymentList");
		
		try {
			pstmt = con.prepareStatement(query);
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			
			pstmt.setString(1, searchValue);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			purList = new ArrayList<Purchase>();
			
			while(rset.next()) {
				Purchase pur = new Purchase();
				pur.setPurchaseNo(rset.getInt("PURCHASE_NO"));
				pur.setbMemberId(rset.getString("BMEMBER_ID"));
				pur.setbMemberName(rset.getString("MANAGER_NAME"));
				pur.setProductName(rset.getString("PRODUCT_NAME"));
				pur.setSalesUnit(rset.getInt("SALES_UNIT"));
				pur.setPrice(rset.getInt("PRICE"));
				pur.setPurchaseDate(rset.getDate("PURCHASE_DATE"));

				purList.add(pur);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return purList;
	}

	public int getRefundListCount(Connection con, int condition, String value) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int result = 0;
		String query = "";
		
		if(condition == 1) {
			query = prop.getProperty("getRefundListCount");
			try {
				pstmt = con.prepareStatement(query);
				rset = pstmt.executeQuery();
				
				if(rset.next()) {
					result = rset.getInt(1);
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
				close(pstmt);
			}
			
		}else {
			query = prop.getProperty("getRefundListCountWithValue");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setString(1, value);
				rset = pstmt.executeQuery();
				
				if(rset.next()) {
					result = rset.getInt(1);
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
				close(rset);
			}
		}
		
		return result;
	}

	public ArrayList<Refund> loadRefundList(Connection con, PageInfo pi, int bMemberNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String query = prop.getProperty("loadRefundList");
		ArrayList<Refund> refundList = null;
		
		int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
		int endRow = startRow + pi.getLimit() - 1;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, bMemberNo);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			rset = pstmt.executeQuery();
			
			refundList = new ArrayList<Refund>();
			
			while(rset.next()) {
				Refund ref = new Refund();
				ref.setRefundNo(rset.getInt("REFUND_NO"));
				ref.setMyItemNo(rset.getInt("MYITEM_NO"));
				ref.setPurchaseNo(rset.getInt("PURCHASE_NO"));
				ref.setApproveNo(rset.getString("APPROVE_NO"));
				ref.setbMemberNo(rset.getInt("BMEMBER_NO"));
				ref.setRefundReason(rset.getString("REFUND_REASON"));
				ref.setRefundApplyDate(rset.getDate("REFUND_APPLY_DATE"));
				ref.setResultDate(rset.getDate("REFUND_RESULT_DATE"));
				ref.setRefundResult(rset.getString("REFUND_RESULT"));
				ref.setRefuseReason(rset.getString("REFUSE_REASON"));
				ref.setProductName(rset.getString("PRODUCT_NAME"));
				
				refundList.add(ref);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return refundList;
	}

	public ArrayList<MyItem> loadMyItemOnRecruit(Connection con, int bMemberNo, String productValue) {
		ArrayList<MyItem> itemList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("loadMyItemOnRecruit");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, bMemberNo);
			pstmt.setString(2, productValue);
			
			rset = pstmt.executeQuery();
			
			itemList = new ArrayList<MyItem>();
			while(rset.next()) {
				MyItem mi = new MyItem();
				mi.setMyItemNo(rset.getInt("MYITEM_NO"));
				mi.setbMemberNo(rset.getInt("BMEMBER_NO"));
				mi.setPurchaseNo(rset.getInt("PURCHASE_NO"));
				mi.setMyItemUnit(rset.getInt("MYITEM_UNIT"));
				mi.setMyItemExpDate(rset.getDate("MYITEM_EXP_DATE"));
				mi.setProductCode(rset.getString("PRODUCT_CODE"));
				mi.setPurchaseDate(rset.getDate("PURCHASE_DATE"));
				mi.setProductCount(rset.getInt("SALES_UNIT"));
				
				itemList.add(mi);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return itemList;
	}

	public int insertMyItem(Connection con, int recruitNo, int itemNoFirst, int itemCountFirst) {
		int itemInsert1 = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("insertMyItem");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, itemNoFirst);
			pstmt.setInt(2, recruitNo);
			pstmt.setInt(3, itemCountFirst);
			itemInsert1 = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return itemInsert1;
	}

	public int loadRecruitNo(Connection con) {
		int recruitNo = 0;
		Statement stmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("loadRecruitNo");
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				recruitNo = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return recruitNo;
	}

	public int getItemListCount(Connection con, int memberNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int count = 0;
		String query = prop.getProperty("itemListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberNo);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				count = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return count;
	}

	public ArrayList<MyItem> loadMyItem(Connection con, PageInfo pi, int memberNo) {
		ArrayList<MyItem> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("loadMyItemPaging");
		
		int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
		int endRow = startRow + pi.getLimit() - 1;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberNo);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<MyItem>();
			while(rset.next()) {
				MyItem mi = new MyItem();
				mi.setMyItemNo(rset.getInt("MYITEM_NO"));
				mi.setbMemberNo(rset.getInt("BMEMBER_NO"));
				mi.setPurchaseNo(rset.getInt("PURCHASE_NO"));
				mi.setMyItemUnit(rset.getInt("MYITEM_UNIT"));
				mi.setMyItemExpDate(rset.getDate("MYITEM_EXP_DATE"));
				mi.setMyItemStatus(rset.getString("MYITEM_STATUS"));
				mi.setProductCode(rset.getString("PRODUCT_CODE"));
				mi.setProductCount(rset.getInt("SALES_UNIT"));
				mi.setProductName(rset.getString("PRODUCT_NAME"));
				mi.setPurchaseDate(rset.getDate("PURCHASE_DATE"));
				
				list.add(mi);
			};
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public int getItemSearchListCount(Connection con, String search, int bNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int count = 0;
		String query = prop.getProperty("itemSearchListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, bNo);
			pstmt.setString(2, search);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				count = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return count;
	}

	public ArrayList<MyItem> loadMyItemSearch(Connection con, PageInfo pi, int bNo, String search) {
		ArrayList<MyItem> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("loadMyItemPagingSearch");
		
		int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
		int endRow = startRow + pi.getLimit() - 1;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, bNo);
			pstmt.setString(2, search);
			pstmt.setInt(3, startRow);
			pstmt.setInt(4, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<MyItem>();
			while(rset.next()) {
				MyItem mi = new MyItem();
				mi.setMyItemNo(rset.getInt("MYITEM_NO"));
				mi.setbMemberNo(rset.getInt("BMEMBER_NO"));
				mi.setPurchaseNo(rset.getInt("PURCHASE_NO"));
				mi.setMyItemUnit(rset.getInt("MYITEM_UNIT"));
				mi.setMyItemExpDate(rset.getDate("MYITEM_EXP_DATE"));
				mi.setMyItemStatus(rset.getString("MYITEM_STATUS"));
				mi.setProductCode(rset.getString("PRODUCT_CODE"));
				mi.setProductCount(rset.getInt("SALES_UNIT"));
				mi.setProductName(rset.getString("PRODUCT_NAME"));
				mi.setPurchaseDate(rset.getDate("PURCHASE_DATE"));
				
				list.add(mi);
			};
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public int getItemUseListCount(Connection con, int memberNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int count = 0;
		String query = prop.getProperty("ItemUseListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberNo);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				count = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return count;
	}

	public ArrayList<ItemUse> loadMyItemUse(Connection con, PageInfo pi, int memberNo) {
		ArrayList<ItemUse> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("loadMyItemUse");
		
		int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
		int endRow = startRow + pi.getLimit() - 1;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberNo);
			pstmt.setInt(2, startRow);
			pstmt.setInt(3, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<ItemUse>();
			while(rset.next()) {
				ItemUse iu = new ItemUse();
				iu.setUseNo(rset.getInt("USE_NO"));
				iu.setMyItemNo(rset.getInt("MYITEM_NO"));
				iu.setTarget(rset.getInt("TARGET"));
				iu.setUseUnit(rset.getInt("USE_UNIT"));
				iu.setUseDate(rset.getDate("USE_DATE"));
				iu.setUseYn(rset.getString("USE_YN"));
				iu.setTargetKind(rset.getString("TARGET_KIND"));
				iu.setApplyDate(rset.getDate("APPLY_DATE"));
				iu.setProductName(rset.getString("PRODUCT_NAME"));
				iu.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				
				list.add(iu);
			};
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public int getPaymentListCount(Connection con, int memberNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int count = 0;
		String query = prop.getProperty("PaymentListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberNo);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				count = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return count;
	}

	public int getRefundListCount(Connection con, int bMemberNo) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		int count = 0;
		String query = prop.getProperty("getRefundFrontListCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, bMemberNo);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				count = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return count;
	}

	public int getAdminItemUseListCount(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		int count = 0;
		String query = prop.getProperty("adminItemUseListCount");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				count = rset.getInt(1);
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return count;
	}

	public ArrayList<ItemUse> loadAdminItemUse(Connection con, PageInfo pi) {
		ArrayList<ItemUse> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("loadAdminItemUse");
		
		int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
		int endRow = startRow + pi.getLimit() - 1;
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<ItemUse>();
			while(rset.next()) {
				ItemUse iu = new ItemUse();
				iu.setUseNo(rset.getInt("USE_NO"));
				iu.setMyItemNo(rset.getInt("MYITEM_NO"));
				iu.setTarget(rset.getInt("TARGET"));
				iu.setUseUnit(rset.getInt("USE_UNIT"));
				iu.setUseDate(rset.getDate("USE_DATE"));
				iu.setUseYn(rset.getString("USE_YN"));
				iu.setTargetKind(rset.getString("TARGET_KIND"));
				iu.setApplyDate(rset.getDate("APPLY_DATE"));
				iu.setProductName(rset.getString("PRODUCT_NAME"));
				iu.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				
				list.add(iu);
			};
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	public ArrayList<MainItem> loadUserMainPageItem(Connection con, String value) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<MainItem> itemList = null;
		
		String query = prop.getProperty("loadUserMainPageItem");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, value);
			pstmt.setInt(2, 1);
			pstmt.setInt(3, 12);
			
			rset = pstmt.executeQuery();
			itemList = new ArrayList<MainItem>();
			
			while(rset.next()) {
				MainItem mi = new MainItem();
				mi.setUseNo(rset.getInt("USE_NO"));
				mi.setMyitemNo(rset.getInt("MYITEM_NO"));
				mi.setTarget(rset.getInt("TARGET"));
				mi.setUseUnit(rset.getInt("USE_UNIT"));
				mi.setUseDate(rset.getDate("USE_DATE"));
				mi.setStringUseDate(rset.getDate("USE_DATE").toString());
				mi.setUseYn(rset.getString("USE_YN"));
				mi.setTargetKind(rset.getString("TARGET_KIND"));
				mi.setApplyDate(rset.getDate("APPLY_DATE"));
				mi.setStringApplyDate(rset.getDate("APPLY_DATE").toString());
				mi.setRecruitDeleteYn(rset.getString("RECRUIT_DELETE_YN"));
				mi.setCompanyName(rset.getString("COMPANY_NAME"));
				mi.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				mi.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
				mi.setStringExpirationDate(rset.getDate("EXPIRATIONDATE").toString());
				mi.setChangeName(rset.getString("CHANGE_NAME"));
				mi.setFilePath(rset.getString("FILE_PATH"));
				mi.setProductCode(rset.getString("PRODUCT_CODE"));
				
				itemList.add(mi);
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return itemList;
	}

	public boolean loadFavStar(Connection con, int memberNo, int recruitNo, String kind) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		boolean yn = false;
		
		String query = prop.getProperty("loadFavStar");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberNo);
			pstmt.setInt(2, recruitNo);
			pstmt.setString(3, kind);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				yn = true;
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return yn;
	}

	public int insertLoadFavStar(Connection con, int favMemberNo, int favRecruitNo, String kind) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertLoadFavStar");
		
		try {
			
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, favRecruitNo);
			pstmt.setInt(2, favMemberNo);
			pstmt.setString(3, kind);
			result = pstmt.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public String loadFavUpdateValue(Connection con, int favMemberNo, int favRecruitNo, String kind) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String yn = "";
		
		String query = prop.getProperty("loadFavUpdateValue");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, favMemberNo);
			pstmt.setInt(2, favRecruitNo);
			pstmt.setString(3, kind);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				yn = rset.getString(1);
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return yn;
	}

	public int updateFavStar(Connection con, int favMemberNo, int favRecruitNo, String updateValue, String kind) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateFavStar");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, updateValue);
			pstmt.setInt(2, favMemberNo);
			pstmt.setInt(3, favRecruitNo);
			pstmt.setString(4, kind);
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public String loadFavStarYN(Connection con, int memberNo, int recruitNo, String kind) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		String yn = "";
		
		String query = prop.getProperty("loadFavStarYN");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberNo);
			pstmt.setInt(2, recruitNo);
			pstmt.setString(3, kind);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				yn = rset.getString("FAV_YN");
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return yn;
	}
	
	
	
	
	
	
	
}
