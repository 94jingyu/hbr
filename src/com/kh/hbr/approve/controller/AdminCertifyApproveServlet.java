package com.kh.hbr.approve.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.approve.model.service.ApproveService;
import com.kh.hbr.member.model.vo.Business;

@WebServlet("/certifyApprove.ap")
public class AdminCertifyApproveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public AdminCertifyApproveServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//뷰에서 클릭한 기업의 아이디를 가져옴
		String bID = request.getParameter("bMemberId");
				
		//아이디가 공백문자와 함께 넘어와서 공백문자를 제거함
		String afterBID = bID.replace(" ", "");

		Business bMember = new Business();
				
		bMember.setBmemberId(afterBID);
		
		//BUSINESS 테이블에 데이터 UPDATE
		int result = new ApproveService().certifyApprove1(bMember);
		
		//COMPANY_APPROVE 테이블에 데이터 UPDATE
		int result2 = new ApproveService().certifyApprove2(bMember);
		
		/*System.out.println("business 테이블에 데이터 업데이트 : " + result);
		System.out.println("company_appove 테이블에 데이터 업데이트 : " + result2);*/
		
		String page="";
		
		if(result == 1 && result2 == 1) {
			
			//기업인증내역 불러오는 서블릿으로 이동
			page = "selectApprove.ap";
		
		} else {
			
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "기업 인증 승인 실패!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
