package com.kh.hbr.approve.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.approve.model.service.ApproveService;
import com.kh.hbr.member.model.vo.Business;

import java.util.ArrayList;


@WebServlet("/bringInfo.ap")
public class RegisteredCompanyInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public RegisteredCompanyInfoServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		//현재 로그인한 유저의 정보를 불러옴
		int bMemberNo = ((Business) (request.getSession().getAttribute("loginUser"))).getBmemberNo();
		
		Business bMember = new Business();
		
		bMember.setBmemberNo(bMemberNo);
		
		//유저의 bMemberNo를 service에 bMember로 넘겨줌, list에는 service -> dao -> DB를 거쳐온 결과를 담아줌
		ArrayList<Business> list = new ApproveService().bringInfo(bMember);
		
		String page = "";
		if(list != null) {

			page="views/business_DY/DY_business_ApproveRequest.jsp";
			request.setAttribute("list", list);
			
		}else {
			
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "기업 정보 불러오기 실패!");
		}
		
		//DB를 거쳐온 결과를 가지고 page로 이동할 건데 위에 적어준 page(경로)로 이동할거고, request와 response를 가지고 이동함 / if()일 경우와 else일 경우 모두 밑의 코드를
		//실행할 거라서 밖에 빼줌
		request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
