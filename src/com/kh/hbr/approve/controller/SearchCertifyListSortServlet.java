package com.kh.hbr.approve.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.approve.model.vo.CertifyList;

@WebServlet("/searchCertifySort.ap")
public class SearchCertifyListSortServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public SearchCertifyListSortServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//선택된 인증상태를 가져옴
		String searchCondition = request.getParameter("searchCondition");
		String[] searchList = request.getParameterValues("listInfo");
		
		/*System.out.println("sort 서블릿 searchCondition : " + searchCondition);*/
		/*System.out.println("searchList : " + searchList[0]);*/
		
		for(int i = 0; i < searchList.length; i++) {
			/*System.out.println("sort 서블릿 searchList : " + searchList[i]);*/
		}
		
		/*if(searchCondition.equals("findWaiting")) {
	
		}else if(searchCondition.equals("findApproved")) {
		
		}else if(searchCondition.equals("findRejected")) {

		}else if(searchCondition.equals("findRejected")) {
			
		}else if(searchCondition.equals("findRTotal")) {
			
		}*/
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
