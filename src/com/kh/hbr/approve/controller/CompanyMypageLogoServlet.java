package com.kh.hbr.approve.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.kh.hbr.approve.model.service.MypageService;
import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.common.MyFileRenamePolicy;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.recruit.model.service.RecruitService;
import com.oreilly.servlet.MultipartRequest;

@WebServlet("/mypageLogo.bs")
public class CompanyMypageLogoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public CompanyMypageLogoServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int bMemberNo = ((Business) (request.getSession().getAttribute("loginUser"))).getBmemberNo();

		System.out.println("로고 서블릿 기업회원번호 : " + bMemberNo);

		Business bMember = new Business();

		bMember.setBmemberNo(bMemberNo);

		//사진 추가 부분
		if(ServletFileUpload.isMultipartContent(request)) {
			// 전송 파일 용량 제한: 10MB로 계산
			int maxSize = 1024 * 1024 * 500; 

			String root = request.getSession().getServletContext().getRealPath("/");

			System.out.println("(InsertRegiserServlet) root : " + root);

			// 파일 저장경로
			String savePath = root + "thumbnail_uploadFiles/";

			MultipartRequest multiRequest = 
					new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());

			ArrayList<String> saveFiles = new ArrayList<String>();
			ArrayList<String> originFiles = new ArrayList<String>();



			Enumeration<String> files = multiRequest.getFileNames();

			System.out.println("files : " + files);

			while(files.hasMoreElements()) {
				String name = files.nextElement();
				System.out.println("(InsertRegiserServlet) name : " + name);

				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));
			}

			System.out.println("saveFiles : " + saveFiles);
			System.out.println("originFiles : " + originFiles);

			ArrayList<Attachment> fileList = new ArrayList<Attachment>();
			for(int i = originFiles.size() - 1; i >= 0; i--) {
				Attachment at = new Attachment();
				at.setFilePath(savePath);
				at.setOriginName(originFiles.get(i));
				at.setChangeName(saveFiles.get(i));

				fileList.add(at);
			}
			System.out.println("fileList : " + fileList);

			int result = new MypageService().registerLogo(bMember, fileList);

			System.out.println("로고 서블릿 result : " + result);

			String page = "";
			if(result > 0) {
				page="companyMypage.bs";
				/*request.getSession().setAttribute("companyLogo", requestRecruit);*/
				response.sendRedirect(page);

			}else {				
				//실패시 저장된 사진 삭제
				for(int i = 0; i < saveFiles.size(); i++) {
					File failedFile = new File(savePath + saveFiles.get(i));
					failedFile.delete();
				}				
				page = "views/common/errorPage.jsp";
				request.setAttribute("msg","로고 등록 실패!");
				request.getRequestDispatcher(page).forward(request,response);
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		doGet(request, response);
	}

}
