package com.kh.hbr.approve.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.approve.model.service.ApproveService;
import com.kh.hbr.member.model.vo.Business;

import java.util.ArrayList;

@WebServlet("/approveDetail.ap")
public class AdminCertifyDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AdminCertifyDetailServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//뷰에서 클릭한 기업의 아이디를 가져옴
		String bID = request.getParameter("bMemberId");
		
		//아이디가 공백문자와 함께 넘어와서 공백문자를 제거함
		String afterBID = bID.replace(" ", "");

		Business bMember = new Business();
		
		bMember.setBmemberId(afterBID);
		
		ArrayList<Business> list = new ApproveService().approveDetail(bMember);
		
		String page = "";
		if(list != null) {
			page="views/business_DY/DY_admin_BusinessApprove_detail.jsp";
			request.setAttribute("list", list);
		
		}else {
			
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "기업인증신청 상세보기 실패!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
