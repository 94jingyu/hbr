package com.kh.hbr.approve.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.approve.model.service.ApplicantService;

@WebServlet("/insertInterview.bs")
public class InsertInterviewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public InsertInterviewServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		//기업회원번호
		int bNo = Integer.parseInt(request.getParameter("bNo"));
		
		//이력서번호
		int resumeNo = Integer.parseInt(request.getParameter("resumeNo"));
		
		//개인회원번호
		int uNo = Integer.parseInt(request.getParameter("uNo"));
		
		/*System.out.println("면접제의 서블릿 기업회원번호 : " + bNo);
		System.out.println("면접제의 서블릿 이력서번호 : " + resumeNo);
		System.out.println("면접제의 서블릿 개인회원번호 : " + uNo);*/
		
		//COMPANY_INTERVIEW 테이블에 INSERT
		int result = new ApplicantService().insertInterviewTable(bNo, resumeNo);
		
		String page="";
		
		if(result == 1) {
			
			//기업인증내역 불러오는 서블릿으로 이동
			page = "applicantSearch.bs";
		
		} else {
			
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "면접제의내역 업데이트 실패!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
