package com.kh.hbr.approve.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.approve.model.service.ManageService;
import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.resume.model.vo.Career;
import com.kh.hbr.resume.model.vo.Education;
import com.kh.hbr.resume.model.vo.Resume;

@WebServlet("/resumeDetail.bs")
public class CompanyResumeDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public CompanyResumeDetailServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		//UpdateOpenYN servlet에서 넘겨준 이력서번호를 변수에 담아줌
		int resumeNo = Integer.parseInt(request.getParameter("resumeNo"));
		int applyNo = Integer.parseInt(request.getParameter("applyNo"));
		
		/*System.out.println("update 서블릿에서 넘어온 이력서번호 : " + resumeNo);*/
		
		HashMap<String,Object> hmap = new ManageService().resumeDetail(resumeNo, applyNo);
		
		/*System.out.println("이력서 상세보기 서블릿 hmap : " + hmap);*/
		
		Member member = (Member) hmap.get("member");
		Resume resume = (Resume) hmap.get("resume");
		Education edu = (Education) hmap.get("edu");
		Career career = (Career) hmap.get("career");
		ArrayList<Attachment> attlist = (ArrayList<Attachment>) hmap.get("attlist");
		
		String page="";
		if(hmap != null) {
			page = "views/business_DY/DY_business_ResumeDetail.jsp";
			
			request.getSession().setAttribute("resume", resume);
			request.getSession().setAttribute("member" ,member);
			request.getSession().setAttribute("edu", edu);
			request.getSession().setAttribute("career" , career);
			request.getSession().setAttribute("attlist", attlist);
			
			response.sendRedirect(page);
		
		} else {
			page = "views/common/errorPage.jsp";
			
			request.setAttribute("msg", "이력서 상세 보기 실패!");
			request.getRequestDispatcher(page).forward(request, response);
			
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		doGet(request, response);
	}

}
