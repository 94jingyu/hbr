package com.kh.hbr.approve.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.approve.model.service.ManageService;

@WebServlet("/updateOpenYN.bs")
public class UpdateOpenYNServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public UpdateOpenYNServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//applicantManage jsp에서 넘겨준 이력서번호를 변수에 담아줌
		int resumeNo = Integer.parseInt(request.getParameter("resumeNo"));
		int applyNo = Integer.parseInt(request.getParameter("applyNo"));

		System.out.println("jsp에서 넘어온 이력서번호 : " + resumeNo);
		System.out.println("jsp에서 넘어온 입사지원번호 : " + applyNo);

		//APPLY 테이블 열람여부 UPDATE
		int result = new ManageService().updateOpenYN(resumeNo, applyNo);

		System.out.println("apply 테이블 열람여부 업데이트 : " + result);

		String page="";
		if(result == 1) {
			//이력서 상세보기 서블릿으로 이동
			page = "resumeDetail.bs";
			request.setAttribute("resumeNo", resumeNo);
			request.setAttribute("applyNo", applyNo);

		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "APPLY 테이블 열람여부 업데이트 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
