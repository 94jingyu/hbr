package com.kh.hbr.approve.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.approve.model.service.ManageService;
import com.kh.hbr.member.model.vo.Business;

import java.util.ArrayList;

@WebServlet("/applicantManage.bs")
public class ApplicantManageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ApplicantManageServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		//보금자리 관리 페이지에서 현재 로그인되어 있는 유저의 기업회원번호를 넘겨줘서 가져옴
		int bnum = Integer.parseInt(request.getParameter("bmemberNo"));
		
		/*System.out.println("넘어온 기업회원번호 : " + bnum);*/
		
		Business bMember = new Business();
		
		bMember.setBmemberNo(bnum);
		
		ArrayList list = new ManageService().selectApplicantInfo(bMember);
		
		System.out.println("servlet list : " + list);
		
		String page="";
		if(list != null) {
				
			page="views/business_DY/DY_business_ApplicantManage.jsp";
			request.getSession().setAttribute("applicantList", list);
			response.sendRedirect(page);
		
		} else {
			
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "지원자 내역 불러오기 실패!");
			request.getRequestDispatcher(page).forward(request, response);	
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
