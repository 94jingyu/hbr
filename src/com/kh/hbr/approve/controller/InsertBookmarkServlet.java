package com.kh.hbr.approve.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.approve.model.service.ApplicantService;

@WebServlet("/insertBookmark.bs")
public class InsertBookmarkServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public InsertBookmarkServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//기업회원번호
		int bNo = Integer.parseInt(request.getParameter("bNo"));

		//이력서번호
		int resumeNo = Integer.parseInt(request.getParameter("resumeNo"));

		//개인회원번호
		int uNo = Integer.parseInt(request.getParameter("uNo"));
	
		int result = new ApplicantService().insertBookmark(bNo, resumeNo, uNo);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
