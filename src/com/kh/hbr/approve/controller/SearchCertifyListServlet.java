package com.kh.hbr.approve.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.approve.model.service.ApproveService;
import com.kh.hbr.approve.model.vo.PageInfo;

@WebServlet("/searchCertify.ap")
public class SearchCertifyListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public SearchCertifyListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//검색한 아이디명을 가져옴
		String searchValue = request.getParameter("searchValue");
		
		//선택된 인증상태를 가져옴
		String state = request.getParameter("state");
		
		ArrayList searchList = null;
		
		/*System.out.println("searchValue : " + searchValue);
		System.out.println("state : " + state);*/

		//아이디 검색
		if(searchValue != null) {
			searchValue = request.getParameter("searchValue");
			searchList = new ApproveService().searchId(searchValue);
			
		} else {
			//sort - 인증상태 (대기중)
			if(state.equals("findWaiting")) {
				searchList = new ApproveService().searchWaiting(state);
			//sort - 인증상태 (승인)
			} else if(state.equals("findApproved")) {
				searchList = new ApproveService().searchApproved(state);
			//sort - 인증상태 (반려)
			} else if(state.equals("findRejected")) {
				searchList = new ApproveService().searchRejected(state);
			//sort - 인증상태 (재신청)
			} else if(state.equals("findReapply")) {
				searchList = new ApproveService().searchReapply(state);
			}
		}

		/*if(searchCondition.equals("findTotal")) {
			searchValue = request.getParameter("searchValue");
			searchList = new ApproveService().searchAll(searchCondition, searchValue);	
			
		} else if(searchCondition.equals("findID")) {
			searchValue = request.getParameter("searchValue");
			searchList = new ApproveService().searchCertInfo(searchCondition, searchValue);
			
		} else if(searchCondition.equals("findBname")) {
			searchValue = request.getParameter("searchValue");
			searchList = new ApproveService().searchCertInfo(searchCondition, searchValue);
			
		} else if(searchCondition.equals("findStatus")) {
			searchValue = request.getParameter("searchValue");
			searchList = new ApproveService().searchCertInfo(searchCondition, searchValue);
		}*/
		
		System.out.println("servlet searchList : " + searchList);
		
		String page = "";
		if(searchList != null) {
			page = "views/business_DY/DY_admin_SearchBusinessApprove.jsp";
			request.setAttribute("searchList", searchList);

		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "검색 실패!");
		}	
		request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
