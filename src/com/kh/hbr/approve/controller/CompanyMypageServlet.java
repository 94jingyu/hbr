package com.kh.hbr.approve.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.approve.model.service.MypageService;
import com.kh.hbr.approve.model.vo.Mypage;
import com.kh.hbr.member.model.vo.Business;

import java.util.ArrayList;

@WebServlet("/companyMypage.bs")
public class CompanyMypageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public CompanyMypageServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//현재 로그인되어있는 유저의 기업회원번호를 넘겨받음
		/*int bMemberNo = Integer.parseInt(request.getParameter("bmemberNo"));*/

		int bMemberNo = ((Business) (request.getSession().getAttribute("loginUser"))).getBmemberNo();
		
		Business bMember = new Business();
		
		bMember.setBmemberNo(bMemberNo);
		
		//상품보유현황
		ArrayList itemList = new MypageService().itemInfo(bMember);
		
		//공고현황
		ArrayList recruitTotalList = new MypageService().recruitTotal(bMember);
		
		//최근 등록한 보금자리 
		ArrayList recruitInfoList = new MypageService().recruitInfo(bMember);
		
		//기업인증현황에 필요한 정보들
		ArrayList certStatusList = new MypageService().certStatusInfo(bMember);
		
		//기업 로고 
		ArrayList fileList = new MypageService().selectThumbnail(bMember);
		
		System.out.println("기업 로고 fileList : " + fileList);
		
		String page="";
		if(itemList != null && recruitTotalList != null && recruitInfoList != null && certStatusList != null) {
				
			page="views/business_DY/DY_business_MyPage.jsp";
			request.getSession().setAttribute("itemList", itemList);
			request.getSession().setAttribute("recruitTotalList", recruitTotalList);
			request.getSession().setAttribute("recruitInfoList", recruitInfoList);
			request.getSession().setAttribute("certStatusList", certStatusList);
			request.getSession().setAttribute("fileList", fileList);
			response.sendRedirect(page);
			
		} else {
			
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "마이페이지 불러오기 실패!");
			request.getRequestDispatcher(page).forward(request, response);	
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
