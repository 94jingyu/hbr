package com.kh.hbr.approve.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.approve.model.service.ApplicantService;
import com.kh.hbr.board.jengukBoard.model.vo.Attachment;

import java.util.ArrayList;
import java.util.HashMap;

import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.resume.model.vo.Career;
import com.kh.hbr.resume.model.vo.Education;
import com.kh.hbr.resume.model.vo.Resume;

@WebServlet("/applicantDetail.bs")
public class ApplicantSearchDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ApplicantSearchDetailServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//인재검색에서 클릭한 이력서의 번호를 가져옴
		int rNum = Integer.parseInt(request.getParameter("rNum"));
		
		HashMap<String,Object> hmap = new ApplicantService().resumeDetail(rNum);
		
		Member member = (Member) hmap.get("member");
		Resume resume = (Resume) hmap.get("resume");
		Education edu = (Education) hmap.get("edu");
		Career career = (Career) hmap.get("career");
		ArrayList<Attachment> attlist = (ArrayList<Attachment>) hmap.get("attlist");
		
		String page = "";
		if(hmap != null) {
			page = "views/business_DY/DY_business_ApplicantSearch_ResumeDetail.jsp";
			request.getSession().setAttribute("resume", resume);
			request.getSession().setAttribute("member" ,member);
			request.getSession().setAttribute("edu", edu);
			request.getSession().setAttribute("career" , career);
			request.getSession().setAttribute("attlist", attlist);
			response.sendRedirect(page);
			
		}else {
			page = "view/common/errorPage.jsp";
			request.setAttribute("msg", "이력서 상세 보기 실패!");
			
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
