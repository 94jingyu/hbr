package com.kh.hbr.approve.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kh.hbr.approve.model.service.ApproveService;
import com.kh.hbr.member.model.vo.Business;

@WebServlet("/insertBInfo.ap")
public class CompanyCertifyApplyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CompanyCertifyApplyServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		//현재 로그인 되어 있는 유저의 넘버
		//member폴더에 있는 LoginServlet의 session.setAttribute("loginUser", bloginUser); -> 키값이 loginUser라서 아래에 이걸 넣어줌
		int bMemberNo = ((Business) (request.getSession().getAttribute("loginUser"))).getBmemberNo();
		
		//대표자이름
		String ceoName = request.getParameter("ceoName");
		
		//회사연락처
		String regionTel = request.getParameter("regionTel");
		String telephone = request.getParameter("telephone");
		String telephone2 = request.getParameter("telephone2");
	    String bTelephone = regionTel + "-"+ telephone + "-" + telephone2;
	
		//회사주소
		String address1 = request.getParameter("address1");
		String address2 = request.getParameter("address2");
		String address = address1 + "-" + address2;
		
		//팩스번호 (지역번호)
		String regionFax = request.getParameter("regionFax");
		//팩스번호 (상세번호)
	    String fax = request.getParameter("fax");
	    String fax2 = request.getParameter("fax2");

		//홈페이지주소
		String homepage = request.getParameter("homepage");
		
		Business bMember = new Business();
		
		bMember.setBmemberNo(bMemberNo);
		bMember.setCeoName(ceoName);
		bMember.setCompanyPhone(bTelephone);
		bMember.setCompanyAddress(address);
		bMember.setHomepage(homepage);
		
		//fax 지역번호가 '선택'일경우 DB에 NULL 값이 들어가게 설정
		if(regionFax.equals("no")) {
			String noFax = "";
			bMember.setFax(noFax);
			
		} else {
			String yesFax = regionFax + "-" + fax+ "-" + fax2;
			bMember.setFax(yesFax);
		}
		
		/*System.out.println("기업 사업자 정보 (insertServlet): " + bMember);*/
		
		//BUSINESS 테이블에 데이터 UPDATE
		int result = new ApproveService().updateBusinessInfo(bMember);
		
		//COMPANY_APPROVE 테이블에 데이터 INSERT
		int result2 = new ApproveService().insertApproveInfo(bMember);
		
		/*System.out.println("===result=== : " + result);
		System.out.println("===result2=== : " + result2);*/
		
		String page="";
		//UPDATE와 INSERT 모두 성공했을경우
		if(result == 1 && result2 == 1) {
			
			//바뀐 정보를 담아줄 변수 선언
			Business changedBmember = new ApproveService().changedBmember(bMember);
			
			/*System.out.println("changedBmember : " + changedBmember);*/
			
			//changedBmember에 값이 있을경우
			if(changedBmember != null) {
				
				HttpSession session = request.getSession();
				
				//세션에 loginUser를 정보가 바뀐 유저객체(changedBmember)로 갱신해줌
				session.setAttribute("loginUser", changedBmember);
				
				page = "views/business_DY/DY_business_ApproveRequestSuccess.jsp";
				
				//성공시 새로고침해도 update한 정보가 DB에 영향을 미치지 않도록 해줌
				response.sendRedirect(page);
			}
		
		//UPDATE와 INSERT 실패했을경우
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "기업 인증 신청 실패!");
			request.getRequestDispatcher(page).forward(request, response);
			
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
