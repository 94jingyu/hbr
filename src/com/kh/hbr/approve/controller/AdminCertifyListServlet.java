package com.kh.hbr.approve.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.approve.model.service.ApproveService;
import com.kh.hbr.approve.model.vo.PageInfo;

@WebServlet("/selectApprove.ap")
public class AdminCertifyListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public AdminCertifyListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//페이징에 필요한 변수 선언
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		//한페이지에 보여질 목록개수 설정
		limit = 10;

		//maxPage를 계산하기 위해서 게시물 전체 개수를 조회해야함
		int listCount = new ApproveService().getListCount();
		
		/*System.out.println("listCount : " + listCount);*/

		//총페이지 수 계산
		maxPage = (int) ((double) listCount / limit + 0.9);
		
		/*System.out.println("maxPage : " + maxPage);*/

		//현재 페이지에 보여줄 시작페이지 수 계산
		startPage = (((int) ((double) currentPage / limit + 0.9)) - 1) * 10 + 1;

		/*System.out.println("startPage : " + startPage);*/
		
		//아래쪽에 보여질 마지막 페이지 수
		endPage = startPage + 10 - 1;
		
		/*System.out.println("endPage : " + endPage);*/

		//끝페이지가 최종페이지보다 클경우
		//끝페이지에 최종페이지를 대입하여 숫자를 맞춤
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		//관리자 기업인증내역 페이지 테이블에 데이터를 불러옴
		ArrayList list = new ApproveService().selectApproveInfo(pi);
		
		/*System.out.println("selectApproveInfo : " + list);*/
		
		//검색 기능
		/*String searchCondition = request.getParameter("searchCondition");
		String searchValue = "";
		
		ArrayList searchList = null;
		
		if(searchCondition != null) {
			
		}*/

		String page="";
		if(list != null) {
				
			page="views/business_DY/DY_admin_BusinessApprove.jsp";
			request.setAttribute("pi", pi);
			request.setAttribute("list", list);
		
		} else {
			
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "기업인증내역 불러오기 실패!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
