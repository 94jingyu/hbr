package com.kh.hbr.approve.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.approve.model.service.ApplicantService;

import java.util.ArrayList;

@WebServlet("/bookmarkInfo.bs")
public class BookmarkInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public BookmarkInfoServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		int bNo = Integer.parseInt(request.getParameter("bNo"));

		ArrayList list = new ApplicantService().bookmarkInfo(bNo);
		
		String page = "";
		if(list != null) {
			page="views/business_DY/DY_business_ResumeManage_bookmark.jsp";
			request.getSession().setAttribute("list", list);
		
		}else {
			
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "스크랩 인재정보 조회 실패!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
