package com.kh.hbr.approve.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.approve.model.service.ManageService;
import com.kh.hbr.approve.model.vo.PageInfo;
import com.kh.hbr.member.model.vo.Business;

@WebServlet("/searchApplicant.bs")
public class SearchApplicantServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public SearchApplicantServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//현재 로그인 되어있는 기업회원번호
		int bMemberNo = ((Business) (request.getSession().getAttribute("loginUser"))).getBmemberNo();

		System.out.println("bMemberNo : " + bMemberNo);

		//페이징에 필요한 변수 선언
		/*int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;

		currentPage = 1;

		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		//한페이지에 보여질 목록개수 설정
		limit = 10;

		//maxPage를 계산하기 위해서 게시물 전체 개수를 조회해야함
		int listCount = new ManageService().getListCount();

		//총페이지 수 계산
		maxPage = (int) ((double) listCount / limit + 0.9);

		//현재 페이지에 보여줄 시작페이지 수 계산
		startPage = (((int) ((double) currentPage / limit + 0.9)) - 1) * 10 + 1;

		//아래쪽에 보여질 마지막 페이지 수
		endPage = startPage + 10 - 1;

		//끝페이지가 최종페이지보다 클경우
		//끝페이지에 최종페이지를 대입하여 숫자를 맞춤
		if(maxPage < endPage) {
			endPage = maxPage;
		}

		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);*/

		String searchRegion = request.getParameter("searchRegion");
		String searchJob = request.getParameter("searchJob");
		
		System.out.println("searchRegion : " + searchRegion);
		System.out.println("searchJob : " + searchJob);
		
		//인재검색 테이블에 데이터를 불러옴
		/*ArrayList list = new ManageService().selectSearchApplicant(pi);*/
		ArrayList list = null;
		
		//전국 / 전체 선택
		if(searchRegion.equals("all") && searchJob.equals("all")) {
			list = new ManageService().searchAll();
		
		//전국 / 관심직종 선택
		} else if(searchRegion.equals("all") && !searchJob.equals("all")) {
			list = new ManageService().searchAllAndJob(searchJob);
			System.out.println("관심직종만 선택");
			
		//관심지역 선택 / 전체
		} else if(!searchRegion.equals("all") && searchJob.equals("all")) {
			list = new ManageService().searchRegionAndAll(searchRegion);
			System.out.println("관심지역만 선택");

		//관심지역 선택 / 관심직종 선택
		} else if(!searchRegion.equals("all") && !searchJob.equals("all")) {
			list = new ManageService().searchRegionAndJob(searchRegion, searchJob);
			System.out.println("둘다선택");
		}
		
		String page = "";
		if(list != null) {
			request.getSession().setAttribute("searchList", list);		
			page="views/business_DY/DY_business_ResultApplicantSearch.jsp";
			
		} else {
			request.setAttribute("msg", "인재 검색 실패!");			
			page = "views/common/errorPage.jsp";
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
