package com.kh.hbr.approve.model.dao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.kh.hbr.approve.model.vo.Applicant;
import com.kh.hbr.approve.model.vo.ApplicantManage;
import com.kh.hbr.approve.model.vo.Approve;
import com.kh.hbr.approve.model.vo.CertifyList;
import com.kh.hbr.approve.model.vo.PageInfo;
import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.resume.model.vo.Career;
import com.kh.hbr.resume.model.vo.Education;
import com.kh.hbr.resume.model.vo.Resume;

import static com.kh.hbr.common.JDBCTemplate.*;

public class ManageDao {
	
	private Properties prop = new Properties();
	
	public ManageDao() {
		
		String fileName = ManageDao.class.getResource("/sql/approve/manage-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//지원자 관리 페이지 테이블에 데이터를 불러옴
	public ArrayList selectApplicantInfo(Connection con, Business bMember) {
		
		ArrayList list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectApplicantInfo");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, bMember.getBmemberNo());
			rset = pstmt.executeQuery();
			list = new ArrayList();
			
			while(rset.next()) {
				ApplicantManage am = new ApplicantManage();
				
				am.setBmemberNo(rset.getInt("BMEMBER_NO"));
				am.setMemberNo(rset.getInt("MEMBER_NO"));
				am.setRecruitNo(rset.getInt("RECRUIT_NO"));
				am.setStatus(rset.getString("STATUS"));
				am.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				am.setMemberName(rset.getString("MEMBER_NAME"));
				am.setResumeTitle(rset.getString("RESUME_TITLE"));
				am.setApplyEnrollDate(rset.getDate("APPLY_ENROLL_DATE"));
				am.setOpenYN(rset.getString("OPEN_YN"));
				am.setResumeNo(rset.getInt("RESUME_NO"));
				am.setApplyNo(rset.getInt("APPLY_NO"));
				
				list.add(am);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	//지원자 이름 클릭시 이력서 상세보기
	public HashMap<String, Object> resumeDetail(Connection con, int resumeNo, int applyNo) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Resume resume = null;
		Career career = null;
		Education edu = null;
		Member member = null;
		
		HashMap <String,Object> hmap = null;
		ArrayList<Attachment> attlist = null;
		Attachment at = null;
		
		String query = prop.getProperty("bsResumeDetail");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, resumeNo);
			
			rset = pstmt.executeQuery();
			
			hmap = new HashMap<String,Object>();
			attlist = new ArrayList<Attachment>();
			
			while(rset.next()) {
				member = new Member();
				member.setMemberNo(rset.getInt("MEMBER_NO"));
				member.setMemberId(rset.getString("MEMBER_ID"));
				member.setMemberName(rset.getString("MEMBER_NAME"));
				member.setPhone(rset.getString("PHONE"));
				
				resume = new Resume();
				resume.setResume_no(rset.getInt("RESUME_NO"));
				resume.setResume_title(rset.getString("RESUME_TITLE"));
				resume.setRemail(rset.getString("REMAIL"));
				resume.setResume_open_yn(rset.getString("RESUME_OPEN_YN"));
				resume.setResume_enroll_date(rset.getDate("RESUME_ENROLL_DATE"));
				resume.setResume_modify_date(rset.getDate("RESUME_MODIFY_DATE"));
				resume.setRepresent_yn(rset.getString("REPRESENT_YN"));
				resume.setDelete_yn(rset.getString("DELETE_YN"));
				resume.setCareer_yn(rset.getString("CAREER_YN"));
				resume.setBirth_date(rset.getString("BIRTH_DATE"));
				resume.setRaddress(rset.getString("ADDRESS"));
				resume.setIntroducetitle(rset.getString("INTRODUCE_TITLE"));
				resume.setIntroducecontent(rset.getString("INTRODUCE_CONTENT"));
				resume.setInterestarea(rset.getString("INTEREST_AREA"));
				resume.setApply_field(rset.getString("APPLY_FIELD"));
				resume.setRmember_no(rset.getInt("MEMBER_NO"));
				resume.setComplete_yn(rset.getString("COMPLETE_YN"));
				resume.setGender(rset.getString("GENDER"));
				
				career = new Career();
				career.setCareer_no(rset.getInt("CAREER_NO"));
				career.setCompany_in_year(rset.getDate("COMPANY_IN_YEAR"));
				career.setCompany_out_date(rset.getDate("COMPANY_OUT_DATE"));
				career.setCareer_year(rset.getInt("CAREER_YEAR"));
				career.setCompany_name(rset.getString("COMPANY_NAME"));
				career.setCareer_dept(rset.getString("CAREER_DEPT"));
				career.setJob_level(rset.getString("JOB_LEVEL"));
				career.setCareer_kind(rset.getString("CAREER_KIND"));
				career.setCareer_area(rset.getString("CAREER_AREA"));
				career.setTask(rset.getString("TASK"));
				   				   
				edu = new Education();				
				edu.setEducation_no(rset.getInt("EDUCATION_NO"));
				edu.setSchool_name(rset.getString("SCHOOL_NAME"));
				edu.setMajor(rset.getString("MAJOR"));
				edu.setSchool_enroll_date(rset.getDate("SCHOOL_ENROLL_DATE"));
				edu.setSchool_graduate_date(rset.getDate("SCHOOL_GRADUATE_DATE"));
				edu.setEdu_choice(rset.getString("EDU_CHOICE"));
				edu.setUni_type(rset.getString("UNI_TYPE"));
				edu.setUni_major(rset.getString("UNI_MAJOR"));
				edu.setUni_major_name(rset.getString("UNI_MAJOR_NAME"));
				edu.setUni_score(rset.getDouble("UNI_SCORE"));
				edu.setUni_score_choice(rset.getDouble("UNI_SCORE_CHOICE"));
				edu.setUni_enroll_date(rset.getDate("UNI_ENROLL_DATE"));
				edu.setUni_graduate_date(rset.getDate("UNI_GRADUATE_DATE"));
				edu.setUni_graduate_type(rset.getString("UNI_GRADUATE_TYPE"));
				edu.setGraduate_type(rset.getString("GRADUATE_TYPE"));
				edu.setUni_name(rset.getString("UNI_NAME"));
				
				at = new Attachment();
				at.setAttachmentNo(rset.getInt("ATTACHMENT_NO"));
				at.setOriginName(rset.getString("ORIGIN_NAME"));
				at.setChangeName(rset.getString("CHANGE_NAME"));
				at.setFilePath(rset.getString("FILE_PATH"));
				at.setUploadDate(rset.getDate("UPLOAD_DATE"));
				
				attlist.add(at);
			}
			
			hmap.put("member", member);
			hmap.put("resume", resume);
			hmap.put("career", career);
			hmap.put("edu", edu);
			hmap.put("attlist", attlist);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return hmap;
	}

	//APPLY 테이블 열람여부 UPDATE
	public int updateOpenYN(Connection con, int resumeNo, int applyNo) {
		
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("updateOpenYN");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, resumeNo);
			pstmt.setInt(2, applyNo);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	//인재검색 이력서 불러오기 (페이징)
	public int getListCount(Connection con) {
	
		Statement stmt = null;
		ResultSet rset = null;
		String query = prop.getProperty("listCount");
		int result = 0;
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}

		return result;
	}

	//인재검색 테이블에 데이터를 불러옴
	public ArrayList selectSearchApplicant(Connection con, PageInfo pi) {
		ArrayList list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectSearchApplicant");
		
		try {
			pstmt = con.prepareStatement(query);
			
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			list = new ArrayList();
			
			while(rset.next()) {
				
				Applicant ac = new Applicant();
				
				ac.setResumeNo(rset.getInt("RESUME_NO"));
				ac.setResumeTitle(rset.getString("RESUME_TITLE"));
				ac.setBirthDate(rset.getString("BIRTH_DATE"));
				ac.setrEmail(rset.getString("REMAIL"));
				ac.setAddress(rset.getString("ADDRESS"));
				ac.setIntroduceTitle(rset.getString("INTRODUCE_TITLE"));
				ac.setIntroduceContent(rset.getString("INTRODUCE_CONTENT"));
				ac.setInterestArea(rset.getString("INTEREST_AREA"));
				ac.setApplyField(rset.getString("APPLY_FIELD"));
				ac.setOpenYN(rset.getString("RESUME_OPEN_YN"));
				ac.setEnrollDate(rset.getDate("RESUME_ENROLL_DATE"));
				ac.setModifyDate(rset.getDate("RESUME_MODIFY_DATE"));
				ac.setRepresentYN(rset.getString("REPRESENT_YN"));
				ac.setDeleteYN(rset.getString("DELETE_YN"));
				ac.setCareerYN(rset.getString("CAREER_YN"));
				ac.setCompleteYN(rset.getString("COMPLETE_YN"));
				ac.setMemberNo(rset.getInt("MEMBER_NO"));
				ac.setMemberName(rset.getString("MEMBER_NAME"));
				
				list.add(ac);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
		
	}

	//인재검색 - 전국 / 관심직종 선택
	public ArrayList searchAllAndJob(Connection con, String searchJob) {
		ArrayList list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchAllAndJob");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, searchJob);
			
			rset = pstmt.executeQuery();			
			list = new ArrayList();
			
			while(rset.next()) {
				Applicant ac = new Applicant();
				
				ac.setResumeNo(rset.getInt("RESUME_NO"));
				ac.setResumeTitle(rset.getString("RESUME_TITLE"));
				ac.setBirthDate(rset.getString("BIRTH_DATE"));
				ac.setrEmail(rset.getString("REMAIL"));
				ac.setAddress(rset.getString("ADDRESS"));
				ac.setIntroduceTitle(rset.getString("INTRODUCE_TITLE"));
				ac.setIntroduceContent(rset.getString("INTRODUCE_CONTENT"));
				ac.setInterestArea(rset.getString("INTEREST_AREA"));
				ac.setApplyField(rset.getString("APPLY_FIELD"));
				ac.setOpenYN(rset.getString("RESUME_OPEN_YN"));
				ac.setEnrollDate(rset.getDate("RESUME_ENROLL_DATE"));
				ac.setModifyDate(rset.getDate("RESUME_MODIFY_DATE"));
				ac.setRepresentYN(rset.getString("REPRESENT_YN"));
				ac.setDeleteYN(rset.getString("DELETE_YN"));
				ac.setCareerYN(rset.getString("CAREER_YN"));
				ac.setCompleteYN(rset.getString("COMPLETE_YN"));
				ac.setMemberNo(rset.getInt("MEMBER_NO"));
				ac.setMemberName(rset.getString("MEMBER_NAME"));
				
				list.add(ac);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	//인재검색 - 관심지역 선택 / 전체
	public ArrayList searchRegionAndAll(Connection con, String searchRegion) {
		ArrayList list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchRegionAndAll");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, searchRegion);
			
			rset = pstmt.executeQuery();			
			list = new ArrayList();
			
			while(rset.next()) {
				Applicant ac = new Applicant();
				
				ac.setResumeNo(rset.getInt("RESUME_NO"));
				ac.setResumeTitle(rset.getString("RESUME_TITLE"));
				ac.setBirthDate(rset.getString("BIRTH_DATE"));
				ac.setrEmail(rset.getString("REMAIL"));
				ac.setAddress(rset.getString("ADDRESS"));
				ac.setIntroduceTitle(rset.getString("INTRODUCE_TITLE"));
				ac.setIntroduceContent(rset.getString("INTRODUCE_CONTENT"));
				ac.setInterestArea(rset.getString("INTEREST_AREA"));
				ac.setApplyField(rset.getString("APPLY_FIELD"));
				ac.setOpenYN(rset.getString("RESUME_OPEN_YN"));
				ac.setEnrollDate(rset.getDate("RESUME_ENROLL_DATE"));
				ac.setModifyDate(rset.getDate("RESUME_MODIFY_DATE"));
				ac.setRepresentYN(rset.getString("REPRESENT_YN"));
				ac.setDeleteYN(rset.getString("DELETE_YN"));
				ac.setCareerYN(rset.getString("CAREER_YN"));
				ac.setCompleteYN(rset.getString("COMPLETE_YN"));
				ac.setMemberNo(rset.getInt("MEMBER_NO"));
				ac.setMemberName(rset.getString("MEMBER_NAME"));
				
				list.add(ac);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	//인재검색 - 관심지역 선택 / 관심직종 선택
	public ArrayList searchRegionAndJob(Connection con, String searchRegion, String searchJob) {
		
		ArrayList list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchRegionAndJob");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, searchRegion);
			pstmt.setString(2, searchJob);
			
			rset = pstmt.executeQuery();			
			list = new ArrayList();
			
			while(rset.next()) {
				Applicant ac = new Applicant();
				
				ac.setResumeNo(rset.getInt("RESUME_NO"));
				ac.setResumeTitle(rset.getString("RESUME_TITLE"));
				ac.setBirthDate(rset.getString("BIRTH_DATE"));
				ac.setrEmail(rset.getString("REMAIL"));
				ac.setAddress(rset.getString("ADDRESS"));
				ac.setIntroduceTitle(rset.getString("INTRODUCE_TITLE"));
				ac.setIntroduceContent(rset.getString("INTRODUCE_CONTENT"));
				ac.setInterestArea(rset.getString("INTEREST_AREA"));
				ac.setApplyField(rset.getString("APPLY_FIELD"));
				ac.setOpenYN(rset.getString("RESUME_OPEN_YN"));
				ac.setEnrollDate(rset.getDate("RESUME_ENROLL_DATE"));
				ac.setModifyDate(rset.getDate("RESUME_MODIFY_DATE"));
				ac.setRepresentYN(rset.getString("REPRESENT_YN"));
				ac.setDeleteYN(rset.getString("DELETE_YN"));
				ac.setCareerYN(rset.getString("CAREER_YN"));
				ac.setCompleteYN(rset.getString("COMPLETE_YN"));
				ac.setMemberNo(rset.getInt("MEMBER_NO"));
				ac.setMemberName(rset.getString("MEMBER_NAME"));
				
				list.add(ac);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	//인재검색 - 전체검색
	public ArrayList searchAll(Connection con) {
		ArrayList list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchAll");
		
		try {
			pstmt = con.prepareStatement(query);
			
			rset = pstmt.executeQuery();
			list = new ArrayList();
			
			while(rset.next()) {
				
				Applicant ac = new Applicant();
				
				ac.setResumeNo(rset.getInt("RESUME_NO"));
				ac.setResumeTitle(rset.getString("RESUME_TITLE"));
				ac.setBirthDate(rset.getString("BIRTH_DATE"));
				ac.setrEmail(rset.getString("REMAIL"));
				ac.setAddress(rset.getString("ADDRESS"));
				ac.setIntroduceTitle(rset.getString("INTRODUCE_TITLE"));
				ac.setIntroduceContent(rset.getString("INTRODUCE_CONTENT"));
				ac.setInterestArea(rset.getString("INTEREST_AREA"));
				ac.setApplyField(rset.getString("APPLY_FIELD"));
				ac.setOpenYN(rset.getString("RESUME_OPEN_YN"));
				ac.setEnrollDate(rset.getDate("RESUME_ENROLL_DATE"));
				ac.setModifyDate(rset.getDate("RESUME_MODIFY_DATE"));
				ac.setRepresentYN(rset.getString("REPRESENT_YN"));
				ac.setDeleteYN(rset.getString("DELETE_YN"));
				ac.setCareerYN(rset.getString("CAREER_YN"));
				ac.setCompleteYN(rset.getString("COMPLETE_YN"));
				ac.setMemberNo(rset.getInt("MEMBER_NO"));
				ac.setMemberName(rset.getString("MEMBER_NAME"));
				
				list.add(ac);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

}
