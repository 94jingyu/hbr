package com.kh.hbr.approve.model.dao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Properties;

import com.kh.hbr.approve.model.vo.Approve;
import com.kh.hbr.approve.model.vo.CertifyList;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.approve.model.vo.PageInfo;

import static com.kh.hbr.common.JDBCTemplate.*;

public class ApproveDao {
	
	private Properties prop = new Properties();
	
	public ApproveDao() {
		String fileName = ApproveDao.class.getResource("/sql/approve/approve-query.properties").getPath();
	
		try {
			prop.load(new FileReader(fileName));
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//기업인증신청 - 사업자 정보 BUSINESS 테이블에 UPDATE
	public int updateBusinessInfo(Connection con, Business bMember) {
		
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("updateBusinessInfo");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, bMember.getCeoName());
			pstmt.setString(2, bMember.getCompanyPhone());
			pstmt.setString(3, bMember.getCompanyAddress());
			pstmt.setString(4, bMember.getFax());
			pstmt.setString(5, bMember.getHomepage());
			pstmt.setInt(6, bMember.getBmemberNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}


	//기업인증신청 - 인증정보 COMPANY_APPROVE 테이블에 INSERT
	public int insertApproveInfo(Connection con, Business bMember) {
		
		int result2 = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("insertApproveInfo");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, bMember.getBmemberNo());
			
			result2 = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result2;
	}


	//기업인증신청에 기존 사업자등록번호, 기업구분, 기업명 정보 불러오기
	public ArrayList<Business> bringInfo(Connection con, Business bMember) {
	
		ArrayList<Business> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("bringInfo");
		
		try {

			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, bMember.getBmemberNo());
			rset = pstmt.executeQuery();
			list = new ArrayList<>();
			
			while(rset.next()) {
				
				Business bs = new Business();
				
				//rset COMPANY_NO의 값을 가져와서 Business CompanyNo에 넣어줌
				bs.setCompanyNo(rset.getString("COMPANY_NO"));
				bs.setCompanyType(rset.getString("COMPANY_TYPE"));
				bs.setCompanyName(rset.getString("COMPANY_NAME"));
				
				list.add(bs);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	//업데이트된 유저의 정보를 불러옴
	public Business selectChangedBmember(Connection con, Business bMember) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Business changedBmember = null;
		
		String query = prop.getProperty("changedInfo");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, bMember.getBmemberNo());
			
			rset = pstmt.executeQuery();
			
			//select 해온 유저의 정보들을 꺼내서 설정해줌
			if(rset.next()) {
				changedBmember = new Business();
				changedBmember.setBmemberNo(rset.getInt("BMEMBER_NO"));
				changedBmember.setBmemberId(rset.getString("BMEMBER_ID"));
				changedBmember.setBmemberPwd(rset.getString("BMEMBER_PWD"));
				changedBmember.setCompanyName(rset.getString("COMPANY_NAME"));
				changedBmember.setCompanyNo(rset.getString("COMPANY_NO"));
				changedBmember.setManagerName(rset.getString("MANAGER_NAME"));
				changedBmember.setManagerPhone(rset.getString("MANAGER_PHONE"));
				changedBmember.setManagerEmail(rset.getString("MANAGER_EMAIL"));
				changedBmember.setCeoName(rset.getString("CEO_NAME"));
				changedBmember.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				changedBmember.setCompanyAddress(rset.getString("COMPANY_ADDRESS"));
				changedBmember.setFax(rset.getString("FAX"));
				changedBmember.setHomepage(rset.getString("HOMEPAGE"));
				changedBmember.setCertStatus(rset.getString("CERT_STATUS"));
				changedBmember.setCertDate(rset.getDate("CERT_DATE"));
				changedBmember.setBenrollDate(rset.getDate("BENROLL_DATE"));
				changedBmember.setBmodifyDate(rset.getDate("BMODIFY_DATE"));
				changedBmember.setBstatus(rset.getString("BSTATUS"));
				changedBmember.setBpenaltyCount(rset.getInt("BPENALTY_COUNT"));
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return changedBmember;
	}

	//관리자 기업인증내역 페이지 테이블에 데이터를 불러옴
	public ArrayList selectApproveInfo(Connection con, PageInfo pi) {
		
		ArrayList list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectApproveInfo");
		
		try {
			pstmt = con.prepareStatement(query);
			
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			list = new ArrayList();
			
			while(rset.next()) {
				
				CertifyList cl = new CertifyList();
				
				cl.setCompanyName(rset.getString("COMPANY_NAME"));
				cl.setCompanyNo(rset.getString("COMPANY_NO"));
				cl.setBmemberId(rset.getString("BMEMBER_ID"));
				cl.setApproveNo(rset.getInt("APPROVE_NO"));
				cl.setApplyDate(rset.getDate("APPLY_DATE"));
				cl.setApproveStatus(rset.getString("APPROVE_STATUS"));
				cl.setApproveDate(rset.getDate("APPROVE_DATE"));
				cl.setRejectReason(rset.getString("REJECT_REASON"));
				cl.setrNum(rset.getInt("RNUM"));
				
				list.add(cl);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}
	
	//관리자 기업인증신청 상세보기
	public ArrayList<Business> approveDetail(Connection con, Business bMember) {
		
		ArrayList<Business> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("approveDetail");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, bMember.getBmemberId());
			rset = pstmt.executeQuery();
			list = new ArrayList<>();
			
			while(rset.next()) {
				Business bs = new Business();
				
				bs.setBmemberId(rset.getString("BMEMBER_ID"));
				bs.setCompanyNo(rset.getString("COMPANY_NO"));
				bs.setCompanyType(rset.getString("COMPANY_TYPE"));
				bs.setCompanyName(rset.getString("COMPANY_NAME"));
				bs.setCeoName(rset.getString("CEO_NAME"));
				bs.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				bs.setCompanyAddress(rset.getString("COMPANY_ADDRESS"));
				bs.setFax(rset.getString("FAX"));
				bs.setHomepage(rset.getString("HOMEPAGE"));
				bs.setCertStatus(rset.getString("CERT_STATUS"));
				
				list.add(bs);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	//기업인증 승인 - business 테이블 UPDATE
	public int certifyApprove1(Connection con, Business bMember) {
		
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("certifyApprove1");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, bMember.getBmemberId());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

	//기업인증 승인 - company_approve 테이블 UPDATE
	public int certifyApprove2(Connection con, Business bMember) {

		int result2 = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("certifyApprove2");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, bMember.getBmemberId());
			pstmt.setString(2, "승인");
			
			result2 = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result2;
	}

	//기업인증 반려 - business 테이블 UPDATE
	public int certifyReject1(Connection con, Business bMember) {
		
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("certifyReject1");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, bMember.getBmemberId());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	//기업인증 반려 - company_approve 테이블 UPDATE
	public int certifyReject2(Connection con, Business bMember, Approve approve) {
	
		int result2 = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("certifyReject2");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, bMember.getBmemberId());
			pstmt.setString(2, "반려");
			pstmt.setString(3, approve.getRejectReason());
			
			result2 = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result2;
	}

	//maxPage를 계산하기 위해서 게시물 전체 개수를 조회 - 기업인증내역 전체 불러오기
	public int getListCount(Connection con) {
		
		Statement stmt = null;
		ResultSet rset = null;
		String query = prop.getProperty("listCount");
		int result = 0;
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				result = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}

		return result;
	}

	//관리자 기업인증내역 검색조건 - 전체
	public ArrayList searchAll(Connection con, String searchValue) {
		
		ArrayList searchList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchAll");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchValue);
			
			rset = pstmt.executeQuery();
			
			searchList = new ArrayList();
			
			while(rset.next()) {
				Approve ap = new Approve();
				
				ap.setBmemberId(rset.getString("BMEMBER_ID"));
				ap.setCompanyNo(rset.getString("COMPANY_NO"));
				ap.setCompanyName(rset.getString("COMPANY_NAME"));
				ap.setApplyDate(rset.getDate("APPLY_DATE"));
				ap.setApproveDate(rset.getDate("APPROVE_DATE"));
				ap.setApproveStatus(rset.getString("APPROVE_STATUS"));
				ap.setRejectReason(rset.getString("REJECT_REASON"));
				ap.setBmemberNo(rset.getInt("BMEMBER_NO"));
				
				searchList.add(ap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		return searchList;
	}

	//관리자 기업인증내역 아이디 검색 (select 선택 안함)
	public ArrayList searchId(Connection con, String searchValue) {
		
		ArrayList searchList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchId");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchValue);
			
			rset = pstmt.executeQuery();
			
			searchList = new ArrayList();
			
			while(rset.next()) {
				Business bs = new Business();
				
				bs.setCompanyName(rset.getString("COMPANY_NAME"));
				bs.setCompanyNo(rset.getString("COMPANY_NO"));
				bs.setBmemberId(rset.getString("BMEMBER_ID"));
				
				Approve ap = new Approve();
				
				ap.setApproveNo(rset.getInt("APPROVE_NO"));
				ap.setApplyDate(rset.getDate("APPLY_DATE"));
				ap.setApproveStatus(rset.getString("APPROVE_STATUS"));
				ap.setApproveDate(rset.getDate("APPROVE_DATE"));
				ap.setRejectReason(rset.getString("REJECT_REASON"));
				
				searchList.add(bs);
				searchList.add(ap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return searchList;
	}

	//관리자 기업인증내역 sort (인증상태 - 대기중)
	public ArrayList searchWaiting(Connection con, String state) {
		
		ArrayList searchList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchWaiting");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, "대기중");
			
			rset = pstmt.executeQuery();
			
			searchList = new ArrayList();
			
			while(rset.next()) {
				Business bs = new Business();
				
				bs.setCompanyName(rset.getString("COMPANY_NAME"));
				bs.setCompanyNo(rset.getString("COMPANY_NO"));
				bs.setBmemberId(rset.getString("BMEMBER_ID"));
				
				Approve ap = new Approve();
				
				ap.setApproveNo(rset.getInt("APPROVE_NO"));
				ap.setApplyDate(rset.getDate("APPLY_DATE"));
				ap.setApproveStatus(rset.getString("APPROVE_STATUS"));
				ap.setApproveDate(rset.getDate("APPROVE_DATE"));
				ap.setRejectReason(rset.getString("REJECT_REASON"));
				
				searchList.add(bs);
				searchList.add(ap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return searchList;
	}

	//관리자 기업인증내역 sort (인증상태 - 승인)
	public ArrayList searchApproved(Connection con, String state) {
		
		ArrayList searchList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchApproved");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, "승인");
			
			rset = pstmt.executeQuery();
			
			searchList = new ArrayList();
			
			while(rset.next()) {
				Business bs = new Business();
				
				bs.setCompanyName(rset.getString("COMPANY_NAME"));
				bs.setCompanyNo(rset.getString("COMPANY_NO"));
				bs.setBmemberId(rset.getString("BMEMBER_ID"));
				
				Approve ap = new Approve();
				
				ap.setApproveNo(rset.getInt("APPROVE_NO"));
				ap.setApplyDate(rset.getDate("APPLY_DATE"));
				ap.setApproveStatus(rset.getString("APPROVE_STATUS"));
				ap.setApproveDate(rset.getDate("APPROVE_DATE"));
				ap.setRejectReason(rset.getString("REJECT_REASON"));
				
				searchList.add(bs);
				searchList.add(ap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return searchList;
	}

	//관리자 기업인증내역 sort (인증상태 - 반려)
	public ArrayList searchRejected(Connection con, String state) {
		
		ArrayList searchList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchRejected");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, "반려");
			
			rset = pstmt.executeQuery();
			
			searchList = new ArrayList();
			
			while(rset.next()) {
				Business bs = new Business();
				
				bs.setCompanyName(rset.getString("COMPANY_NAME"));
				bs.setCompanyNo(rset.getString("COMPANY_NO"));
				bs.setBmemberId(rset.getString("BMEMBER_ID"));
				
				Approve ap = new Approve();
				
				ap.setApproveNo(rset.getInt("APPROVE_NO"));
				ap.setApplyDate(rset.getDate("APPLY_DATE"));
				ap.setApproveStatus(rset.getString("APPROVE_STATUS"));
				ap.setApproveDate(rset.getDate("APPROVE_DATE"));
				ap.setRejectReason(rset.getString("REJECT_REASON"));
				
				searchList.add(bs);
				searchList.add(ap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return searchList;
	}

	//관리자 기업인증내역 sort (인증상태 - 재신청)
	public ArrayList searchReapply(Connection con, String state) {
		ArrayList searchList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchReapply");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, "재신청");
			
			rset = pstmt.executeQuery();
			
			searchList = new ArrayList();
			
			while(rset.next()) {
				Business bs = new Business();
				
				bs.setCompanyName(rset.getString("COMPANY_NAME"));
				bs.setCompanyNo(rset.getString("COMPANY_NO"));
				bs.setBmemberId(rset.getString("BMEMBER_ID"));
				
				Approve ap = new Approve();
				
				ap.setApproveNo(rset.getInt("APPROVE_NO"));
				ap.setApplyDate(rset.getDate("APPLY_DATE"));
				ap.setApproveStatus(rset.getString("APPROVE_STATUS"));
				ap.setApproveDate(rset.getDate("APPROVE_DATE"));
				ap.setRejectReason(rset.getString("REJECT_REASON"));
				
				searchList.add(bs);
				searchList.add(ap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return searchList;
	}

	//기업인증 재신청 - Business 테이블 UPDATE
	public int reapplyUpdateInfo(Connection con, Business bMember) {
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("reapplyUpdateInfo");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, bMember.getCeoName());
			pstmt.setString(2, bMember.getCompanyPhone());
			pstmt.setString(3, bMember.getCompanyAddress());
			pstmt.setString(4, bMember.getFax());
			pstmt.setString(5, bMember.getHomepage());
			pstmt.setInt(6, bMember.getBmemberNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	//기업인증 재신청 - APPROVE 테이블 INSERT
	public int reapplyInsertInfo(Connection con, Business bMember) {
		int result2 = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("reapplyUpdateInfo2");
		String rejectReason = null;
		Date approveDate = null;
		String status = "재신청";
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, rejectReason);
			pstmt.setDate(2, approveDate);
			pstmt.setString(3, status);
			pstmt.setInt(4, bMember.getBmemberNo());
			
			result2 = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result2;
	}

	//관리자 기업인증내역 검색조건 - 기업명
	/*public ArrayList searchBname(Connection con, String searchValue) {
		ArrayList searchList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchBname");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchValue);
			
			rset = pstmt.executeQuery();
			
			searchList = new ArrayList();
			
			while(rset.next()) {
				Approve ap = new Approve();
				
				ap.setBmemberId(rset.getString("BMEMBER_ID"));
				ap.setCompanyNo(rset.getString("COMPANY_NO"));
				ap.setCompanyName(rset.getString("COMPANY_NAME"));
				ap.setApplyDate(rset.getDate("APPLY_DATE"));
				ap.setApproveDate(rset.getDate("APPROVE_DATE"));
				ap.setApproveStatus(rset.getString("APPROVE_STATUS"));
				ap.setRejectReason(rset.getString("REJECT_REASON"));
				ap.setBmemberNo(rset.getInt("BMEMBER_NO"));
				
				searchList.add(ap);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return searchList;
	}*/

	//관리자 기업인증내역 검색조건 - 인증상태
	/*public ArrayList searchStatus(Connection con, String searchValue) {
		
		ArrayList searchList = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("searchStatus");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, searchValue);
			
			rset = pstmt.executeQuery();
			
			searchList = new ArrayList();
			
			while(rset.next()) {
				Business bs = new Business();
				
				bs.setCompanyName(rset.getString("COMPANY_NAME"));
				bs.setCompanyNo(rset.getString("COMPANY_NO"));
				bs.setBmemberId(rset.getString("BMEMBER_ID"));
				
				Approve ap = new Approve();
				
				ap.setApproveNo(rset.getInt("APPROVE_NO"));
				ap.setApplyDate(rset.getDate("APPLY_DATE"));
				ap.setApproveStatus(rset.getString("APPROVE_STATUS"));
				ap.setApproveDate(rset.getDate("APPROVE_DATE"));
				ap.setRejectReason(rset.getString("REJECT_REASON"));
				
				searchList.add(bs);
				searchList.add(ap);
			}
				
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return searchList;
	}*/
	
}
