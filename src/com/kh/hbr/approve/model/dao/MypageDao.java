package com.kh.hbr.approve.model.dao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import com.kh.hbr.approve.model.vo.Mypage;
import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.payment.model.vo.MyItem;

import static com.kh.hbr.common.JDBCTemplate.*;

public class MypageDao {
	
	private Properties prop = new Properties();
	
	public MypageDao() {
		
		String fileName = MypageDao.class.getResource("/sql/approve/mypage-query.properties").getPath();
	
		try {
			prop.load(new FileReader(fileName));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//마이페이지 상품보유현황에 나타내줄 상품별 총 개수
	public ArrayList itemInfo(Connection con, Business bMember) {
		
		ArrayList list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("ItemTotal");
		
		try {
			pstmt = con.prepareStatement(query);

			pstmt.setInt(1, bMember.getBmemberNo());
			pstmt.setString(2, "사용");
			pstmt.setInt(3, bMember.getBmemberNo());
			pstmt.setString(4, "사용");
			pstmt.setInt(5, bMember.getBmemberNo());
			pstmt.setString(6, "사용");
			pstmt.setInt(7, bMember.getBmemberNo());
			pstmt.setString(8, "사용");
			pstmt.setInt(9, bMember.getBmemberNo());
			pstmt.setString(10, "사용");

			rset = pstmt.executeQuery();
			list = new ArrayList();
			
			while(rset.next()) {
				
				Mypage mp = new Mypage();
				
				mp.setGoldTotal(rset.getInt("GOLD"));
				mp.setSilverTotal(rset.getInt("SILVER"));
				mp.setIronTotal(rset.getInt("IRON"));
				mp.setStarTotal(rset.getInt("STAR"));
				mp.setResumeTotal(rset.getInt("RESUME"));
				
				list.add(mp);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	//마이페이지 공고현황 
	public ArrayList recruitTotal(Connection con, Business bMember) {
		
		ArrayList list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("recruitTotal");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, bMember.getBmemberNo());
			pstmt.setInt(2, bMember.getBmemberNo());
			pstmt.setString(3, "심사대기");
			pstmt.setInt(4, bMember.getBmemberNo());
			pstmt.setInt(5, bMember.getBmemberNo());
			
			rset = pstmt.executeQuery();
			list = new ArrayList();
			
			while(rset.next()) {
				
				Mypage mp = new Mypage();
				
				mp.setStatusApproved(rset.getInt("APPROVED"));
				mp.setStatusWaiting(rset.getInt("WAITING"));
				mp.setStatusEnd(rset.getInt("END"));
				mp.setStatusRejected(rset.getInt("REJECTED"));
				
				list.add(mp);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	//마이페이지  최근 등록한 보금자리
	public ArrayList recruitInfo(Connection con, Business bMember) {
		
		ArrayList list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("recruitInfo");
		
		try {
			pstmt = con.prepareStatement(query);	
			pstmt.setInt(1, bMember.getBmemberNo());
			
			rset = pstmt.executeQuery();	
			list = new ArrayList();
			
			while(rset.next()) {
				
				Mypage mp = new Mypage();
				
				mp.setRecruitNo(rset.getInt("RECRUIT_NO"));
				mp.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				mp.setEnrollDate(rset.getDate("ENROLLDATE"));
				mp.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
				mp.setRecruitMajor(rset.getString("RECRUIT_MAJOR"));
				mp.setProductName(rset.getString("PRODUCT_NAME"));
				mp.setStatus(rset.getString("STATUS"));
				mp.setClickCount(rset.getInt("CLICK_COUNT"));
				
				list.add(mp);
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	//기업인증현황에 필요한 정보들
	public ArrayList certStatusInfo(Connection con, Business bMember) {
		ArrayList list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("certStatusInfo");
		
		try {
			pstmt = con.prepareStatement(query);	
			pstmt.setInt(1, bMember.getBmemberNo());
			
			rset = pstmt.executeQuery();	
			list = new ArrayList();
			
			while(rset.next()) {
				
				Mypage mp = new Mypage();
				
				mp.setCompanyAddress(rset.getString("COMPANY_ADDRESS"));
				mp.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				mp.setCertStatus(rset.getString("CERT_STATUS"));

				list.add(mp);
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	//마이페이지 로고 등록
		public int insertAttachment(Connection con, Attachment attachment, int bid) {
			PreparedStatement pstmt = null;
			int result = 0;
			
			String query = prop.getProperty("insertAttachment");

			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, bid);
				pstmt.setString(2, attachment.getOriginName());
				pstmt.setString(3, attachment.getChangeName());
				pstmt.setString(4, attachment.getFilePath());
				
				result = pstmt.executeUpdate();
			
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
			}
			
			return result;
		}

		//마이페이지 로고 불러오기
		public ArrayList selectThumbnail(Connection con, Business bMember) {

			ArrayList list = null;
			Attachment at = null;
			PreparedStatement pstmt = null;
			ResultSet rset = null;

			String query = prop.getProperty("selectThumbnail");

			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, bMember.getBmemberNo());

				rset = pstmt.executeQuery();

				list = new ArrayList();

				while(rset.next()) {

					at = new Attachment();
					at.setAttachmentNo(rset.getInt("ATTACHMENT_NO"));
					at.setOriginName(rset.getString("ORIGIN_NAME"));
					at.setChangeName(rset.getString("CHANGE_NAME"));
					at.setFilePath(rset.getString("FILE_PATH"));
					at.setUploadDate(rset.getDate("UPLOAD_DATE"));

					list.add(at);					
				}

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
				close(rset);
			}		
			return list;
		}

}
