package com.kh.hbr.approve.model.dao;

import static com.kh.hbr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.kh.hbr.approve.model.vo.Bookmark;
import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.resume.model.vo.Career;
import com.kh.hbr.resume.model.vo.Education;
import com.kh.hbr.resume.model.vo.Resume;

public class ApplicantDao {
	
private Properties prop = new Properties();
	
	public ApplicantDao() {
		String fileName = ApplicantDao.class.getResource("/sql/approve/applicant-query.properties").getPath();
	
		try {
			prop.load(new FileReader(fileName));
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//인재검색 이력서 상세보기
	public HashMap<String, Object> resumeDetail(Connection con, int rNum) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Resume resume = null;
		Career career = null;
		Education edu = null;
		Member member = null;
		HashMap <String,Object>hmap= null;
		ArrayList<Attachment> attlist = null;
		Attachment at = null;
		String query = prop.getProperty("resumeDetail");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, rNum);
			
			rset = pstmt.executeQuery();
			
			hmap=new HashMap<String,Object>();
			attlist = new ArrayList<Attachment>();
								
			while(rset.next()) {
				member = new Member();
				member.setMemberNo(rset.getInt("MEMBER_NO"));
				member.setMemberId(rset.getString("MEMBER_ID"));
				member.setMemberName(rset.getString("MEMBER_NAME"));
				member.setPhone(rset.getString("PHONE"));
																			
				resume = new Resume();
				resume.setResume_no(rset.getInt("RESUME_NO"));
				resume.setResume_title(rset.getString("RESUME_TITLE"));
				resume.setRemail(rset.getString("REMAIL"));
				resume.setResume_open_yn(rset.getString("RESUME_OPEN_YN"));
				resume.setResume_enroll_date(rset.getDate("RESUME_ENROLL_DATE"));
				resume.setResume_modify_date(rset.getDate("RESUME_MODIFY_DATE"));
				resume.setRepresent_yn(rset.getString("REPRESENT_YN"));
				resume.setDelete_yn(rset.getString("DELETE_YN"));
				resume.setCareer_yn(rset.getString("CAREER_YN"));
				resume.setBirth_date(rset.getString("BIRTH_DATE"));
				resume.setRaddress(rset.getString("ADDRESS"));
				resume.setIntroducetitle(rset.getString("INTRODUCE_TITLE"));
				resume.setIntroducecontent(rset.getString("INTRODUCE_CONTENT"));
				resume.setInterestarea(rset.getString("INTEREST_AREA"));
				resume.setApply_field(rset.getString("APPLY_FIELD"));
				resume.setRmember_no(rset.getInt("MEMBER_NO"));
				resume.setComplete_yn(rset.getString("COMPLETE_YN"));
				resume.setGender(rset.getString("GENDER"));
														
				career = new Career();
				career.setCareer_no(rset.getInt("CAREER_NO"));
				career.setCompany_in_year(rset.getDate("COMPANY_IN_YEAR"));
				career.setCompany_out_date(rset.getDate("COMPANY_OUT_DATE"));
				career.setCareer_year(rset.getInt("CAREER_YEAR"));
				career.setCompany_name(rset.getString("COMPANY_NAME"));
				career.setCareer_dept(rset.getString("CAREER_DEPT"));
				career.setJob_level(rset.getString("JOB_LEVEL"));
				career.setCareer_kind(rset.getString("CAREER_KIND"));
				career.setCareer_area(rset.getString("CAREER_AREA"));
				career.setTask(rset.getString("TASK"));
				   				   
				edu = new Education();				
				edu.setEducation_no(rset.getInt("EDUCATION_NO"));
				edu.setSchool_name(rset.getString("SCHOOL_NAME"));
				edu.setMajor(rset.getString("MAJOR"));
				edu.setSchool_enroll_date(rset.getDate("SCHOOL_ENROLL_DATE"));
				edu.setSchool_graduate_date(rset.getDate("SCHOOL_GRADUATE_DATE"));
				edu.setEdu_choice(rset.getString("EDU_CHOICE"));
				edu.setUni_type(rset.getString("UNI_TYPE"));
				edu.setUni_major(rset.getString("UNI_MAJOR"));
				edu.setUni_major_name(rset.getString("UNI_MAJOR_NAME"));
				edu.setUni_score(rset.getDouble("UNI_SCORE"));
				edu.setUni_score_choice(rset.getDouble("UNI_SCORE_CHOICE"));
				edu.setUni_enroll_date(rset.getDate("UNI_ENROLL_DATE"));
				edu.setUni_graduate_date(rset.getDate("UNI_GRADUATE_DATE"));
				edu.setUni_graduate_type(rset.getString("UNI_GRADUATE_TYPE"));
				edu.setGraduate_type(rset.getString("GRADUATE_TYPE"));
				edu.setUni_name(rset.getString("UNI_NAME"));
				
				at = new Attachment();
				at.setAttachmentNo(rset.getInt("ATTACHMENT_NO"));
				at.setOriginName(rset.getString("ORIGIN_NAME"));
				at.setChangeName(rset.getString("CHANGE_NAME"));
				at.setFilePath(rset.getString("FILE_PATH"));
				at.setUploadDate(rset.getDate("UPLOAD_DATE"));
				
				attlist.add(at);
				
			}
			
			hmap.put("member", member);
			hmap.put("resume", resume);
			hmap.put("career", career);
			hmap.put("edu", edu);
			hmap.put("attlist", attlist);
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			
		}finally {
			close(pstmt);
			close(rset);
		}
		return hmap;
	}

	//APPLY 테이블에 UPDATE
	public int updateApplyTable(Connection con, int bNo, int resumeNo) {
		
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("updateApplyTable");
		
		try {
			pstmt = con.prepareStatement(query);
			/*pstmt.setString();*/
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}

		return result;
	}

	//COMPANY_INTERVIEW 테이블에 INSERT
	public int insertInterviewTable(Connection con, int bNo, int resumeNo) {
		int result2 = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("insertInterviewTable");
		
		try {
			pstmt = con.prepareStatement(query);
			/*pstmt.setInt();*/
			
			result2 = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result2;
	}

	//bookmark 테이블에 INSERT
	public int insertBookmark(Connection con, int bNo, int resumeNo, int uNo) {
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("insertBookmark");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, bNo);
			pstmt.setInt(2, uNo);
			pstmt.setInt(3, resumeNo);
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	//스크랩 인재정보 목록 조회
	public ArrayList bookmarkInfo(Connection con, int bNo) {
		
		ArrayList list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("bookmarkInfo");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setInt(1, bNo);
			rset = pstmt.executeQuery();
			list = new ArrayList();
			
			while(rset.next()) {
				
				Bookmark b = new Bookmark();
				
				b.setMemberName(rset.getString("MEMBER_NAME"));
				b.setResumeTitle(rset.getString("RESUME_TITLE"));
				b.setApplyField(rset.getString("APPLY_FIELD"));
				b.setInterestArea(rset.getString("INTEREST_AREA"));
				b.setBookmarkDate(rset.getDate("BOOKMARK_DATE"));
				b.setBookmarkNo(rset.getInt("BOOKMARK_NO"));
				b.setBmemberNo(rset.getInt("BMEMBER_NO"));
				
				list.add(b);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}
}
