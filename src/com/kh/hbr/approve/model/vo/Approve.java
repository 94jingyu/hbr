package com.kh.hbr.approve.model.vo;

import java.sql.Date;

public class Approve implements java.io.Serializable {
	private int approveNo; //기업인증 신청번호
	private int bmemberNo; //기업회원번호
	private String approveYN; //인증완료여부
	private String rejectReason; //반려사유
	private String approveStatus; //기업인증상태 - 대기중/승인/반려
	private Date applyDate; //기업인증 신청일자
	private Date approveDate; //기업인증 완료일자
	private int rNum; //페이징 시 사용되는 rownum
	
	private String bmemberId; //기업회원 아이디
	private String companyNo; //사업자등록번호
	private String companyName; //기업명
	
	public Approve() {}

	public Approve(int approveNo, int bmemberNo, String approveYN, String rejectReason, String approveStatus,
			Date applyDate, Date approveDate, int rNum, String bmemberId, String companyNo, String companyName) {
		super();
		this.approveNo = approveNo;
		this.bmemberNo = bmemberNo;
		this.approveYN = approveYN;
		this.rejectReason = rejectReason;
		this.approveStatus = approveStatus;
		this.applyDate = applyDate;
		this.approveDate = approveDate;
		this.rNum = rNum;
		this.bmemberId = bmemberId;
		this.companyNo = companyNo;
		this.companyName = companyName;
	}

	public int getApproveNo() {
		return approveNo;
	}

	public int getBmemberNo() {
		return bmemberNo;
	}

	public String getApproveYN() {
		return approveYN;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public Date getApplyDate() {
		return applyDate;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveNo(int approveNo) {
		this.approveNo = approveNo;
	}

	public void setBmemberNo(int bmemberNo) {
		this.bmemberNo = bmemberNo;
	}

	public void setApproveYN(String approveYN) {
		this.approveYN = approveYN;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public int getrNum() {
		return rNum;
	}

	public void setrNum(int rNum) {
		this.rNum = rNum;
	}

	public String getBmemberId() {
		return bmemberId;
	}

	public String getCompanyNo() {
		return companyNo;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setBmemberId(String bmemberId) {
		this.bmemberId = bmemberId;
	}

	public void setCompanyNo(String companyNo) {
		this.companyNo = companyNo;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Override
	public String toString() {
		return "Approve [approveNo=" + approveNo + ", bmemberNo=" + bmemberNo + ", approveYN=" + approveYN
				+ ", rejectReason=" + rejectReason + ", approveStatus=" + approveStatus + ", applyDate=" + applyDate
				+ ", approveDate=" + approveDate + ", rNum=" + rNum + ", bmemberId=" + bmemberId + ", companyNo="
				+ companyNo + ", companyName=" + companyName + "]";
	}

	
}
