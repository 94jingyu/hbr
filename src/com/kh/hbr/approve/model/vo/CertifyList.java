package com.kh.hbr.approve.model.vo;

import java.sql.Date;

public class CertifyList implements java.io.Serializable {
	private int approveNo; //기업인증 신청번호
	private Date applyDate; //기업인증 신청일자
	private String approveStatus; //기업인증상태 - 대기중/승인/반려
	private Date approveDate; //기업인증 완료일자
	private String rejectReason; //반려사유
	private int rNum; //페이징 시 사용되는 rownum

	private String companyName; //기업명
	private String companyNo; //사업자등록번호
	private String bmemberId; //기업회원 아이디

	public CertifyList() {}

	public CertifyList(int approveNo, Date applyDate, String approveStatus, Date approveDate, String rejectReason,
			int rNum, String companyName, String companyNo, String bmemberId) {
		super();
		this.approveNo = approveNo;
		this.applyDate = applyDate;
		this.approveStatus = approveStatus;
		this.approveDate = approveDate;
		this.rejectReason = rejectReason;
		this.rNum = rNum;
		this.companyName = companyName;
		this.companyNo = companyNo;
		this.bmemberId = bmemberId;
	}

	public int getApproveNo() {
		return approveNo;
	}

	public Date getApplyDate() {
		return applyDate;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public int getrNum() {
		return rNum;
	}

	public String getCompanyName() {
		return companyName;
	}

	public String getCompanyNo() {
		return companyNo;
	}

	public String getBmemberId() {
		return bmemberId;
	}

	public void setApproveNo(int approveNo) {
		this.approveNo = approveNo;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public void setrNum(int rNum) {
		this.rNum = rNum;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setCompanyNo(String companyNo) {
		this.companyNo = companyNo;
	}

	public void setBmemberId(String bmemberId) {
		this.bmemberId = bmemberId;
	}

	@Override
	public String toString() {
		return "CertifyList [approveNo=" + approveNo + ", applyDate=" + applyDate + ", approveStatus=" + approveStatus
				+ ", approveDate=" + approveDate + ", rejectReason=" + rejectReason + ", rNum=" + rNum
				+ ", companyName=" + companyName + ", companyNo=" + companyNo + ", bmemberId=" + bmemberId + "]";
	}
}
