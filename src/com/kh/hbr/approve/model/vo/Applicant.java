package com.kh.hbr.approve.model.vo;

import java.sql.Date;

public class Applicant implements java.io.Serializable {

	private int resumeNo;
	private String resumeTitle;
	private String birthDate;
	private String rEmail;
	private String address;
	private String introduceTitle;
	private String introduceContent;
	private String interestArea;
	private String applyField;
	private String openYN;
	private Date enrollDate;
	private Date modifyDate;
	private String representYN;
	private String deleteYN;
	private String careerYN;
	private String completeYN;
	private int memberNo;
	private String memberName;
	
	public Applicant() {}

	public Applicant(int resumeNo, String resumeTitle, String birthDate, String rEmail, String address,
			String introduceTitle, String introduceContent, String interestArea, String applyField, String openYN,
			Date enrollDate, Date modifyDate, String representYN, String deleteYN, String careerYN, String completeYN,
			int memberNo, String memberName) {
		super();
		this.resumeNo = resumeNo;
		this.resumeTitle = resumeTitle;
		this.birthDate = birthDate;
		this.rEmail = rEmail;
		this.address = address;
		this.introduceTitle = introduceTitle;
		this.introduceContent = introduceContent;
		this.interestArea = interestArea;
		this.applyField = applyField;
		this.openYN = openYN;
		this.enrollDate = enrollDate;
		this.modifyDate = modifyDate;
		this.representYN = representYN;
		this.deleteYN = deleteYN;
		this.careerYN = careerYN;
		this.completeYN = completeYN;
		this.memberNo = memberNo;
		this.memberName = memberName;
	}

	public int getResumeNo() {
		return resumeNo;
	}

	public String getResumeTitle() {
		return resumeTitle;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public String getrEmail() {
		return rEmail;
	}

	public String getAddress() {
		return address;
	}

	public String getIntroduceTitle() {
		return introduceTitle;
	}

	public String getIntroduceContent() {
		return introduceContent;
	}

	public String getInterestArea() {
		return interestArea;
	}

	public String getApplyField() {
		return applyField;
	}

	public String getOpenYN() {
		return openYN;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public String getRepresentYN() {
		return representYN;
	}

	public String getDeleteYN() {
		return deleteYN;
	}

	public String getCareerYN() {
		return careerYN;
	}

	public String getCompleteYN() {
		return completeYN;
	}

	public int getMemberNo() {
		return memberNo;
	}

	public void setResumeNo(int resumeNo) {
		this.resumeNo = resumeNo;
	}

	public void setResumeTitle(String resumeTitle) {
		this.resumeTitle = resumeTitle;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public void setrEmail(String rEmail) {
		this.rEmail = rEmail;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setIntroduceTitle(String introduceTitle) {
		this.introduceTitle = introduceTitle;
	}

	public void setIntroduceContent(String introduceContent) {
		this.introduceContent = introduceContent;
	}

	public void setInterestArea(String interestArea) {
		this.interestArea = interestArea;
	}

	public void setApplyField(String applyField) {
		this.applyField = applyField;
	}

	public void setOpenYN(String openYN) {
		this.openYN = openYN;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public void setRepresentYN(String representYN) {
		this.representYN = representYN;
	}

	public void setDeleteYN(String deleteYN) {
		this.deleteYN = deleteYN;
	}

	public void setCareerYN(String careerYN) {
		this.careerYN = careerYN;
	}

	public void setCompleteYN(String completeYN) {
		this.completeYN = completeYN;
	}

	public void setMemberNo(int memberNo) {
		this.memberNo = memberNo;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	@Override
	public String toString() {
		return "Applicant [resumeNo=" + resumeNo + ", resumeTitle=" + resumeTitle + ", birthDate=" + birthDate
				+ ", rEmail=" + rEmail + ", address=" + address + ", introduceTitle=" + introduceTitle
				+ ", introduceContent=" + introduceContent + ", interestArea=" + interestArea + ", applyField="
				+ applyField + ", openYN=" + openYN + ", enrollDate=" + enrollDate + ", modifyDate=" + modifyDate
				+ ", representYN=" + representYN + ", deleteYN=" + deleteYN + ", careerYN=" + careerYN + ", completeYN="
				+ completeYN + ", memberNo=" + memberNo + ", memberName=" + memberName + "]";
	}
}
