package com.kh.hbr.approve.model.vo;

import java.sql.Date;

public class ApplicantManage implements java.io.Serializable{
	private int bmemberNo; //기업회원번호
	private int memberNo; //개인회원번호
	private int recruitNo; //채용공고번호
	private int applyNo; //입사지원 번호
	
	private String status; //보금자리 상태
	private String recruitTitle; //보금자리 제목
	private String memberName; //지원자이름
	private String resumeTitle; //이력서 제목
	private Date applyEnrollDate; //지원일
	private String openYN; //열람여부
	private int resumeNo; //이력서 번호
	
	public ApplicantManage() {}

	public ApplicantManage(int bmemberNo, int memberNo, int recruitNo, int applyNo, String status, String recruitTitle,
			String memberName, String resumeTitle, Date applyEnrollDate, String openYN, int resumeNo) {
		super();
		this.bmemberNo = bmemberNo;
		this.memberNo = memberNo;
		this.recruitNo = recruitNo;
		this.applyNo = applyNo;
		this.status = status;
		this.recruitTitle = recruitTitle;
		this.memberName = memberName;
		this.resumeTitle = resumeTitle;
		this.applyEnrollDate = applyEnrollDate;
		this.openYN = openYN;
		this.resumeNo = resumeNo;
	}

	public String getStatus() {
		return status;
	}

	public String getRecruitTitle() {
		return recruitTitle;
	}

	public String getMemberName() {
		return memberName;
	}

	public String getResumeTitle() {
		return resumeTitle;
	}

	public Date getApplyEnrollDate() {
		return applyEnrollDate;
	}

	public String getOpenYN() {
		return openYN;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setRecruitTitle(String recruitTitle) {
		this.recruitTitle = recruitTitle;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public void setResumeTitle(String resumeTitle) {
		this.resumeTitle = resumeTitle;
	}

	public void setApplyEnrollDate(Date applyEnrollDate) {
		this.applyEnrollDate = applyEnrollDate;
	}

	public void setOpenYN(String openYN) {
		this.openYN = openYN;
	}

	public int getBmemberNo() {
		return bmemberNo;
	}

	public int getMemberNo() {
		return memberNo;
	}

	public int getRecruitNo() {
		return recruitNo;
	}

	public void setBmemberNo(int bmemberNo) {
		this.bmemberNo = bmemberNo;
	}

	public void setMemberNo(int memberNo) {
		this.memberNo = memberNo;
	}

	public void setRecruitNo(int recruitNo) {
		this.recruitNo = recruitNo;
	}

	public int getResumeNo() {
		return resumeNo;
	}

	public void setResumeNo(int resumeNo) {
		this.resumeNo = resumeNo;
	}

	public int getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(int applyNo) {
		this.applyNo = applyNo;
	}

	@Override
	public String toString() {
		return "ApplicantManage [bmemberNo=" + bmemberNo + ", memberNo=" + memberNo + ", recruitNo=" + recruitNo
				+ ", applyNo=" + applyNo + ", status=" + status + ", recruitTitle=" + recruitTitle + ", memberName="
				+ memberName + ", resumeTitle=" + resumeTitle + ", applyEnrollDate=" + applyEnrollDate + ", openYN="
				+ openYN + ", resumeNo=" + resumeNo + "]";
	}

}
