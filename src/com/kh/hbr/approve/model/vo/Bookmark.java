package com.kh.hbr.approve.model.vo;

import java.sql.Date;

public class Bookmark {

	private int bookmarkNo;
	private int bmemberNo;
	private int memberNo;
	private int resumeNo;
	private Date bookmarkDate;
	private String memberName;
	private String resumeTitle;
	private String applyField;
	private String interestArea;
	
	public Bookmark() {}

	public Bookmark(int bookmarkNo, int bmemberNo, int memberNo, int resumeNo, Date bookmarkDate, String memberName,
			String resumeTitle, String applyField, String interestArea) {
		super();
		this.bookmarkNo = bookmarkNo;
		this.bmemberNo = bmemberNo;
		this.memberNo = memberNo;
		this.resumeNo = resumeNo;
		this.bookmarkDate = bookmarkDate;
		this.memberName = memberName;
		this.resumeTitle = resumeTitle;
		this.applyField = applyField;
		this.interestArea = interestArea;
	}

	public int getBookmarkNo() {
		return bookmarkNo;
	}

	public int getBmemberNo() {
		return bmemberNo;
	}

	public int getMemberNo() {
		return memberNo;
	}

	public int getResumeNo() {
		return resumeNo;
	}

	public Date getBookmarkDate() {
		return bookmarkDate;
	}

	public String getMemberName() {
		return memberName;
	}

	public String getResumeTitle() {
		return resumeTitle;
	}

	public String getApplyField() {
		return applyField;
	}

	public String getInterestArea() {
		return interestArea;
	}

	public void setBookmarkNo(int bookmarkNo) {
		this.bookmarkNo = bookmarkNo;
	}

	public void setBmemberNo(int bmemberNo) {
		this.bmemberNo = bmemberNo;
	}

	public void setMemberNo(int memberNo) {
		this.memberNo = memberNo;
	}

	public void setResumeNo(int resumeNo) {
		this.resumeNo = resumeNo;
	}

	public void setBookmarkDate(Date bookmarkDate) {
		this.bookmarkDate = bookmarkDate;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public void setResumeTitle(String resumeTitle) {
		this.resumeTitle = resumeTitle;
	}

	public void setApplyField(String applyField) {
		this.applyField = applyField;
	}

	public void setInterestArea(String interestArea) {
		this.interestArea = interestArea;
	}

	@Override
	public String toString() {
		return "Bookmark [bookmarkNo=" + bookmarkNo + ", bmemberNo=" + bmemberNo + ", memberNo=" + memberNo
				+ ", resumeNo=" + resumeNo + ", bookmarkDate=" + bookmarkDate + ", memberName=" + memberName
				+ ", resumeTitle=" + resumeTitle + ", applyField=" + applyField + ", interestArea=" + interestArea
				+ "]";
	}
	
	
	
	
	
}
