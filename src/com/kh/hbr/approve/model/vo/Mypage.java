package com.kh.hbr.approve.model.vo;

import java.sql.Date;

public class Mypage implements java.io.Serializable {
	
	//금도끼 개수
	private int goldTotal;
	
	//은도끼 개수
	private int silverTotal;
	
	//쇠도끼 개수
	private int ironTotal;
	
	//반짝 아이템 개수
	private int starTotal;
	
	//이력서 열람권 개수
	private int resumeTotal;
	
	//공고현황
	private int statusWaiting;
	private int statusApproved;
	private int statusEnd;
	private int statusRejected;
	
	//최근 등록한 보금자리
	private int recruitNo;
	private String recruitTitle;
	private Date enrollDate;
	private Date expirationDate;
	private String recruitMajor;
	private String productName;
	private String status;
	private int clickCount;
	
	//기업인증 현황에 필요한 정보들
	private String companyAddress; 
	private String companyPhone;
	private String certStatus;
	
	public Mypage() {}

	public Mypage(int goldTotal, int silverTotal, int ironTotal, int starTotal, int resumeTotal, int statusWaiting,
			int statusApproved, int statusEnd, int statusRejected, int recruitNo, String recruitTitle, Date enrollDate,
			Date expirationDate, String recruitMajor, String productName, String status, int clickCount,
			String companyAddress, String companyPhone, String certStatus) {
		super();
		this.goldTotal = goldTotal;
		this.silverTotal = silverTotal;
		this.ironTotal = ironTotal;
		this.starTotal = starTotal;
		this.resumeTotal = resumeTotal;
		this.statusWaiting = statusWaiting;
		this.statusApproved = statusApproved;
		this.statusEnd = statusEnd;
		this.statusRejected = statusRejected;
		this.recruitNo = recruitNo;
		this.recruitTitle = recruitTitle;
		this.enrollDate = enrollDate;
		this.expirationDate = expirationDate;
		this.recruitMajor = recruitMajor;
		this.productName = productName;
		this.status = status;
		this.clickCount = clickCount;
		this.companyAddress = companyAddress;
		this.companyPhone = companyPhone;
		this.certStatus = certStatus;
	}

	public int getGoldTotal() {
		return goldTotal;
	}

	public int getSilverTotal() {
		return silverTotal;
	}

	public int getIronTotal() {
		return ironTotal;
	}

	public int getStarTotal() {
		return starTotal;
	}

	public int getResumeTotal() {
		return resumeTotal;
	}

	public void setGoldTotal(int goldTotal) {
		this.goldTotal = goldTotal;
	}

	public void setSilverTotal(int silverTotal) {
		this.silverTotal = silverTotal;
	}

	public void setIronTotal(int ironTotal) {
		this.ironTotal = ironTotal;
	}

	public void setStarTotal(int starTotal) {
		this.starTotal = starTotal;
	}

	public void setResumeTotal(int resumeTotal) {
		this.resumeTotal = resumeTotal;
	}

	public int getStatusWaiting() {
		return statusWaiting;
	}

	public int getStatusApproved() {
		return statusApproved;
	}

	public int getRecruitNo() {
		return recruitNo;
	}

	public String getRecruitTitle() {
		return recruitTitle;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public String getRecruitMajor() {
		return recruitMajor;
	}

	public String getProductName() {
		return productName;
	}

	public String getStatus() {
		return status;
	}

	public int getClickCount() {
		return clickCount;
	}

	public void setStatusWaiting(int statusWaiting) {
		this.statusWaiting = statusWaiting;
	}

	public void setStatusApproved(int statusApproved) {
		this.statusApproved = statusApproved;
	}

	public void setRecruitNo(int recruitNo) {
		this.recruitNo = recruitNo;
	}

	public void setRecruitTitle(String recruitTitle) {
		this.recruitTitle = recruitTitle;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public void setRecruitMajor(String recruitMajor) {
		this.recruitMajor = recruitMajor;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setClickCount(int clickCount) {
		this.clickCount = clickCount;
	}

	public int getStatusEnd() {
		return statusEnd;
	}

	public int getStatusRejected() {
		return statusRejected;
	}

	public void setStatusEnd(int statusEnd) {
		this.statusEnd = statusEnd;
	}

	public void setStatusRejected(int statusRejected) {
		this.statusRejected = statusRejected;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public String getCertStatus() {
		return certStatus;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public void setCertStatus(String certStatus) {
		this.certStatus = certStatus;
	}

	@Override
	public String toString() {
		return "Mypage [goldTotal=" + goldTotal + ", silverTotal=" + silverTotal + ", ironTotal=" + ironTotal
				+ ", starTotal=" + starTotal + ", resumeTotal=" + resumeTotal + ", statusWaiting=" + statusWaiting
				+ ", statusApproved=" + statusApproved + ", statusEnd=" + statusEnd + ", statusRejected="
				+ statusRejected + ", recruitNo=" + recruitNo + ", recruitTitle=" + recruitTitle + ", enrollDate="
				+ enrollDate + ", expirationDate=" + expirationDate + ", recruitMajor=" + recruitMajor
				+ ", productName=" + productName + ", status=" + status + ", clickCount=" + clickCount
				+ ", companyAddress=" + companyAddress + ", companyPhone=" + companyPhone + ", certStatus=" + certStatus
				+ "]";
	}
}
