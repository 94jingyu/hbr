package com.kh.hbr.approve.model.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.kh.hbr.approve.model.dao.ApproveDao;
import com.kh.hbr.approve.model.dao.ManageDao;
import com.kh.hbr.approve.model.vo.PageInfo;
import com.kh.hbr.member.model.vo.Business;

import java.sql.Connection;
import static com.kh.hbr.common.JDBCTemplate.*;

public class ManageService {

	//지원자 관리 페이지 테이블에 데이터를 불러옴
	public ArrayList selectApplicantInfo(Business bMember) {
		
		Connection con = getConnection();
		
		ArrayList list = new ManageDao().selectApplicantInfo(con, bMember);
		
		close(con);
		
		return list;
	}

	//지원자 이름 클릭시 이력서 상세보기
	public HashMap<String, Object> resumeDetail(int resumeNo, int applyNo) {
		
		Connection con = getConnection();
		
		HashMap<String, Object> hmap = null;
		
		hmap = new ManageDao().resumeDetail(con, resumeNo, applyNo);
		
		close(con);
		
		return hmap;
	}

	//APPLY 테이블 열람여부 UPDATE
	public int updateOpenYN(int resumeNo, int applyNo) {

		Connection con = getConnection();

		int result = new ManageDao().updateOpenYN(con, resumeNo, applyNo);

		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}

		close(con);

		return result;
	}

	//인재검색 이력서 불러오기 (페이징)
	public int getListCount() {
		Connection con = getConnection();
		
		int result = new ManageDao().getListCount(con);
		
		close(con);
		
		return result;
	}

	//인재검색 테이블에 데이터를 불러옴
	public ArrayList selectSearchApplicant(PageInfo pi) {
		Connection con = getConnection();
		
		ArrayList list = new ManageDao().selectSearchApplicant(con, pi);
		
		close(con);
		
		return list;
	}

	//인재검색 - 전국 / 관심직종 선택
	public ArrayList searchAllAndJob(String searchJob) {
		Connection con = getConnection();		
		
		ArrayList list = new ManageDao().searchAllAndJob(con, searchJob);

		close(con);
		
		return list;	
	}

	//인재검색 - 관심지역 선택 / 전체
	public ArrayList searchRegionAndAll(String searchRegion) {
		Connection con = getConnection();		
		
		ArrayList list = new ManageDao().searchRegionAndAll(con, searchRegion);

		close(con);
		
		return list;
	}

	//인재검색 - 관심지역 선택 / 관심직종 선택
	public ArrayList searchRegionAndJob(String searchRegion, String searchJob) {
		Connection con = getConnection();		
		
		ArrayList list = new ManageDao().searchRegionAndJob(con, searchRegion, searchJob);
		
		close(con);
		
		return list;
	}

	//인재검색 - 전체 검색
	public ArrayList searchAll() {
		Connection con = getConnection();
		
		ArrayList list = new ManageDao().searchAll(con);
		
		close(con);
		
		return list;
	}


}
