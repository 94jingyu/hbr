package com.kh.hbr.approve.model.service;

import static com.kh.hbr.common.JDBCTemplate.close;
import static com.kh.hbr.common.JDBCTemplate.commit;
import static com.kh.hbr.common.JDBCTemplate.getConnection;
import static com.kh.hbr.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import com.kh.hbr.approve.model.dao.ApplicantDao;
import com.kh.hbr.approve.model.dao.ApproveDao;
import com.kh.hbr.approve.model.dao.ManageDao;
import com.kh.hbr.member.model.vo.Business;

public class ApplicantService {

	//인재검색 이력서 상세보기
	public HashMap<String, Object> resumeDetail(int rNum) {
		Connection con = getConnection();
		
		HashMap<String, Object> hmap = null;
		
		hmap = new ApplicantDao().resumeDetail(con, rNum);
		
		close(con);
		
		return hmap;
	}
	
	//APPLY 테이블에 UPDATE
	public int updateApplyTable(int bNo, int resumeNo) {
		Connection con = getConnection();
		
		int result = new ApplicantDao().updateApplyTable(con, bNo, resumeNo);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	//COMPANY_INTERVIEW 테이블에 INSERT
	public int insertInterviewTable(int bNo, int resumeNo) {
		Connection con = getConnection();
		
		int result2 = new ApplicantDao().insertInterviewTable(con, bNo, resumeNo);
		
		if(result2 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result2;
	}

	//bookmark 테이블에 INSERT
	public int insertBookmark(int bNo, int resumeNo, int uNo) {
		Connection con = getConnection();
		
		int result = new ApplicantDao().insertBookmark(con, bNo, resumeNo, uNo);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	//스크랩 인재정보 목록 조회
	public ArrayList bookmarkInfo(int bNo) {
		Connection con = getConnection();
		
		ArrayList list = new ApplicantDao().bookmarkInfo(con, bNo);
		
		close(con);
		
		return list;
	}


}
