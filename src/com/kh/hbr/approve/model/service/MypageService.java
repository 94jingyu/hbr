package com.kh.hbr.approve.model.service;

import java.util.ArrayList;

import com.kh.hbr.approve.model.dao.MypageDao;
import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.recruit.model.dao.RecruitDao;

import java.sql.Connection;
import static com.kh.hbr.common.JDBCTemplate.*;

public class MypageService {

	//마이페이지 상품보유현황에 나타내줄 상품별 총 개수
	public ArrayList itemInfo(Business bMember) {
		
		Connection con = getConnection();
		
		ArrayList list = new MypageDao().itemInfo(con, bMember);
		
		close(con);
		
		return list;
	}

	//마이페이지 공고현황
	public ArrayList recruitTotal(Business bMember) {
		
		Connection con = getConnection();
		
		ArrayList list = new MypageDao().recruitTotal(con, bMember);
		
		close(con);
		
		return list;
	}

	//마이페이지  최근 등록한 보금자리
	public ArrayList recruitInfo(Business bMember) {

		Connection con = getConnection();
		
		ArrayList list = new MypageDao().recruitInfo(con, bMember);
		
		close(con);
		
		return list;
	}

	//기업인증현황에 필요한 정보들
	public ArrayList certStatusInfo(Business bMember) {
		
		Connection con = getConnection();
		
		ArrayList list = new MypageDao().certStatusInfo(con, bMember);
		
		close(con);
		
		return list;
	}

	//마이페이지 로고 등록
	public int registerLogo(Business bMember, ArrayList<Attachment> fileList) {
		Connection con = getConnection();

		int result = 0;

		int bid = bMember.getBmemberNo();
		System.out.println("서비스 bid : " + bid);

		if(bid >= 0) {
			for(int i = 0; i < fileList.size(); i++) {
				fileList.get(i).setBoardNo((bid));				
				result += new MypageDao().insertAttachment(con, fileList.get(i), bid);
				commit(con);	
			}


		}else {
			rollback(con);
		}

		close(con);

		return result;

	}

	//마이페이지 로고 불러오기
	public ArrayList selectThumbnail(Business bMember) {

		Connection con = getConnection();

		ArrayList list = new MypageDao().selectThumbnail(con, bMember);

		close(con);

		return list;
	}
	
}
