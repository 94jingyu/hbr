package com.kh.hbr.approve.model.service;

import com.kh.hbr.approve.model.dao.ApproveDao;
import com.kh.hbr.member.model.vo.Business;
import com.kh.hbr.approve.model.vo.PageInfo;
import com.kh.hbr.approve.model.vo.Approve;
import static com.kh.hbr.common.JDBCTemplate.*;

import java.sql.Connection;
import java.util.ArrayList;

public class ApproveService {

	//기업인증신청 - 사업자 정보 BUSINESS 테이블에 UPDATE
	public int updateBusinessInfo(Business bMember) {
		
		Connection con = getConnection();
		
		int result = new ApproveDao().updateBusinessInfo(con, bMember);
		
		/*System.out.println("result : " + result);*/
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}
	
	//기업인증신청 - 인증정보 COMPANY_APPROVE 테이블에 INSERT
	public int insertApproveInfo(Business bMember) {
	
		Connection con = getConnection();
		
		int result2 = new ApproveDao().insertApproveInfo(con, bMember);
		
		if(result2 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result2;
	}

	//기업인증신청에 기존 사업자등록번호, 기업구분, 기업명 정보 불러오기
	public ArrayList<Business> bringInfo(Business bMember) {
	
		Connection con = getConnection();
		
		ArrayList<Business> list = new ApproveDao().bringInfo(con, bMember);
		
		close(con);
		
		return list;
	}

	//업데이트된 유저의 정보를 가져옴
	public Business changedBmember(Business bMember) {
		
		Connection con = getConnection();
		
		Business changedBmember = new ApproveDao().selectChangedBmember(con, bMember);

		close(con);
		
		return changedBmember;
	}

	//관리자 기업인증내역 페이지 테이블에 데이터를 불러옴
	public ArrayList selectApproveInfo(PageInfo pi) {
		
		Connection con = getConnection();
		
		ArrayList list = new ApproveDao().selectApproveInfo(con, pi);
		
		close(con);
		
		return list;
	}

	//관리자 기업인증신청 상세보기
	public ArrayList<Business> approveDetail(Business bMember) {
	
		Connection con = getConnection();
		
		ArrayList<Business> list = new ApproveDao().approveDetail(con, bMember);
		
		close(con);
		
		return list;
	}

	//기업인증 승인 - business 테이블 UPDATE
	public int certifyApprove1(Business bMember) {
		
		Connection con = getConnection();
		
		int result = new ApproveDao().certifyApprove1(con, bMember);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	//기업인증 승인 - company_approve 테이블 UPDATE
	public int certifyApprove2(Business bMember) {
		
		Connection con = getConnection();
		
		int result2 = new ApproveDao().certifyApprove2(con, bMember);
		
		if(result2 > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result2;
	}

	//기업인증 반려 - business 테이블 UPDATE
	public int certifyReject1(Business bMember) {
		
		Connection con = getConnection();
		
		int result = new ApproveDao().certifyReject1(con, bMember);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		System.out.println("Service result1 : " + result);
		
		return result;
	}

	//기업인증 반려 - company_approve 테이블 UPDATE
	public int certifyReject2(Business bMember, Approve approve) {
		
		Connection con = getConnection();
		
		int result2 = new ApproveDao().certifyReject2(con, bMember, approve);
		
		if(result2 > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		System.out.println("Service result1 : " + result2);
		
		return result2;
	}

	//maxPage를 계산하기 위해서 게시물 전체 개수를 조회 - 기업인증내역 전체 불러오기
	public int getListCount() {
		Connection con = getConnection();
		
		int result = new ApproveDao().getListCount(con);
		
		close(con);
		
		return result;
	}

	//관리자 기업인증내역 검색조건 - 전체
	public ArrayList searchAll(String searchCondition, String searchValue) {
		Connection con = getConnection();
		
		ArrayList searchList = new ApproveDao().searchAll(con, searchValue);
		
		close(con);
		
		return searchList;
	}

	//관리자 기업인증내역 아이디 검색 (select 선택 안함)
	public ArrayList searchId(String searchValue) {
		Connection con = getConnection();		
		
		ArrayList searchList = new ApproveDao().searchId(con, searchValue);
		
		close(con);
		
		return searchList;	
	}
	
	//관리자 기업인증내역 sort (인증상태 - 대기중)
	public ArrayList searchWaiting(String state) {
		Connection con = getConnection();		
		
		ArrayList searchList = new ApproveDao().searchWaiting(con, state);

		close(con);
		
		return searchList;	
	}

	//관리자 기업인증내역 sort (인증상태 - 승인)
	public ArrayList searchApproved(String state) {
		Connection con = getConnection();		
		
		ArrayList searchList = new ApproveDao().searchApproved(con, state);

		close(con);
		
		return searchList;	
	}

	//관리자 기업인증내역 sort (인증상태 - 반려)
	public ArrayList searchRejected(String state) {
		Connection con = getConnection();		
		
		ArrayList searchList = new ApproveDao().searchRejected(con, state);

		close(con);
		
		return searchList;	
	}
	
	//관리자 기업인증내역 sort (인증상태 - 재신청)
	public ArrayList searchReapply(String state) {
		Connection con = getConnection();		
		
		ArrayList searchList = new ApproveDao().searchReapply(con, state);

		close(con);
		
		return searchList;	
	}

	//기업인증 재신청 - Business 테이블 UPDATE
	public int reapplyUpdateInfo(Business bMember) {
		Connection con = getConnection();
		
		int result = new ApproveDao().reapplyUpdateInfo(con, bMember);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	//기업인증 재신청 - APPROVE 테이블 INSERT
	public int reapplyInsertInfo(Business bMember) {
		Connection con = getConnection();
		
		int result2 = new ApproveDao().reapplyInsertInfo(con, bMember);
		
		if(result2 > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result2;
	}

}
