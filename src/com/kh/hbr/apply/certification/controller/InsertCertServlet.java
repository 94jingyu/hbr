package com.kh.hbr.apply.certification.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kh.hbr.apply.certification.model.service.CertificationService;
import com.kh.hbr.apply.certification.model.vo.Certification;
import com.kh.hbr.member.model.vo.Member;

@WebServlet("/insert.certNo")
public class InsertCertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public InsertCertServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		int applyNo = Integer.parseInt(request.getParameter("applyNo"));
		int memberNo = ((Member) request.getSession().getAttribute("loginUser")).getMemberNo();
		
		System.out.println("증명서를 위한 applyNo : " + applyNo);
		System.out.println("증명서를 위한 memberNo : " + memberNo);
		
		
		Certification cert = new Certification();
		cert.setApplyNo(applyNo);
		cert.setMemberNo(memberNo);
		
		ArrayList<Certification> clist = new CertificationService().insertCert(cert);
		System.out.println("증명서발급용 지원내역 : " + clist);
		
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		new Gson().toJson(clist, response.getWriter());
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
