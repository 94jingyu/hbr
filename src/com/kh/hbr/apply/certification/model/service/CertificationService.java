package com.kh.hbr.apply.certification.model.service;

import static com.kh.hbr.common.JDBCTemplate.*;
import java.sql.Connection;
import java.util.ArrayList;

import com.kh.hbr.apply.certification.model.dao.CertificationDao;
import com.kh.hbr.apply.certification.model.vo.Certification;

public class CertificationService {

	public ArrayList<Certification> insertCert(Certification cert) {

		Connection con = getConnection();
		
		ArrayList<Certification> clist = null;
		
		int result = new CertificationDao().insertCert(con, cert);
		
		
		if(result > 0) {
			
			commit(con);
			
			clist = new CertificationDao().selectList(con, cert.getMemberNo());
			
		} else {
			
			rollback(con);
		}
		
		close(con);
		
		return clist;
	}
	



}
