package com.kh.hbr.apply.certification.model.dao;

import static com.kh.hbr.common.JDBCTemplate.*;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import com.kh.hbr.apply.certification.model.vo.Certification;

public class CertificationDao {

	private Properties prop = new Properties();
	
	public CertificationDao() {
		
		String fileName = Certification.class.getResource("/sql/apply/cert-query.properties").getPath();

		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//증명서 발급번호 insert
	public int insertCert(Connection con, Certification cert) {

		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertCert");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, cert.getApplyNo());
			pstmt.setInt(2, cert.getMemberNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result;
	}

	//증명서 발급 리스트 조회
	public ArrayList<Certification> selectList(Connection con, int memberNo) {

		ArrayList<Certification> clist = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectCertList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberNo);
			
			rset = pstmt.executeQuery();
			
			clist = new ArrayList<Certification>();
			
			while(rset.next()) {
				Certification cert = new Certification();
				
				cert.setCertNo(rset.getInt("CERT_NO"));
				cert.setMemberName(rset.getString("MEMBER_NAME"));
				cert.setPhone(rset.getString("PHONE"));
				cert.setApplyEnrollDate(rset.getDate("APPLY_ENROLL_DATE"));
				cert.setApplyEnrollDateYear(cert.getApplyEnrollDate().toString()); //문자로 변환 
				cert.setCompanyName(rset.getString("COMPANY_NAME"));
				cert.setDetailArea(rset.getString("DETAIL_AREA"));
				cert.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				
				clist.add(cert);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		
		return clist;
	}

	

	
}
