package com.kh.hbr.apply.certification.model.vo;

import java.sql.Date;

public class Certification implements java.io.Serializable{
	
	private int certNo;
	private int memberNo;
	private String memberName;
	private String phone;
	private int applyNo;
	private Date applyEnrollDate;
	private String applyEnrollDateYear;
	
	private String companyName;
	private String DetailArea;
	private String companyPhone;
	
	public Certification() {}

	public Certification(int certNo, int memberNo, String memberName, String phone, int applyNo, Date applyEnrollDate,
			String companyName, String detailArea, String companyPhone, String applyEnrollDateYear) {
		super();
		this.certNo = certNo;
		this.memberNo = memberNo;
		this.memberName = memberName;
		this.phone = phone;
		this.applyNo = applyNo;
		this.applyEnrollDate = applyEnrollDate;
		this.companyName = companyName;
		this.DetailArea = detailArea;
		this.companyPhone = companyPhone;
		this.applyEnrollDateYear = applyEnrollDateYear;
	}

	public String getApplyEnrollDateYear() {
		return applyEnrollDateYear;
	}

	public void setApplyEnrollDateYear(String applyEnrollDateYear) {
		this.applyEnrollDateYear = applyEnrollDateYear;
	}

	public int getCertNo() {
		return certNo;
	}

	public void setCertNo(int certNo) {
		this.certNo = certNo;
	}

	public int getMemberNo() {
		return memberNo;
	}

	public void setMemberNo(int memberNo) {
		this.memberNo = memberNo;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(int applyNo) {
		this.applyNo = applyNo;
	}

	public Date getApplyEnrollDate() {
		return applyEnrollDate;
	}

	public void setApplyEnrollDate(Date applyEnrollDate) {
		this.applyEnrollDate = applyEnrollDate;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDetailArea() {
		return DetailArea;
	}

	public void setDetailArea(String detailArea) {
		DetailArea = detailArea;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	@Override
	public String toString() {
		return "Certification [certNo=" + certNo + ", memberNo=" + memberNo + ", memberName=" + memberName + ", phone="
				+ phone + ", applyNo=" + applyNo + ", applyEnrollDate=" + applyEnrollDate + ", applyEnrollDateYear="
				+ applyEnrollDateYear + ", companyName=" + companyName + ", DetailArea=" + DetailArea
				+ ", companyPhone=" + companyPhone + "]";
	}

	

	
	
}