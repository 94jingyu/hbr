package com.kh.hbr.apply.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.apply.model.service.ApplyService;
import com.kh.hbr.apply.model.vo.Apply;
import com.kh.hbr.member.model.vo.Member;

@WebServlet("/selectApplyList.ap")
public class SelectApplyListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectApplyListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//로그인 구현화면
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		/*System.out.println("loginUser : " + loginUser);*/
		
		ArrayList<Apply> alist = new ApplyService().selectList(loginUser.getMemberNo());
		/*System.out.println("지원내역리스트 : " + alist);*/
		
		
		String page = "";
		if(alist != null) {
			page = "/h/views/user_TAEWON/user_applyHistory.jsp";
			request.getSession().setAttribute("alist", alist);
			response.sendRedirect(page);
		} else { 
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "입사지원내역조회 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}
	
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
