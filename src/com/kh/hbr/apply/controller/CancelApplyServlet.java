package com.kh.hbr.apply.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.apply.model.service.ApplyService;
import com.kh.hbr.apply.model.vo.Apply;
import com.kh.hbr.member.model.vo.Member;

@WebServlet("/cancel.ap")
public class CancelApplyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CancelApplyServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//지원취소 서블릿 cancel_yn -> canceled
		int memberNo = ((Member) request.getSession().getAttribute("loginUser")).getMemberNo();
		
		String cancelApplyNo = (String) request.getParameter("cancelApplyNo");
		String[] noStr = cancelApplyNo.split("#");
		int[] cancelNo = new int[noStr.length];

		for(int i = 0; i < cancelNo.length; i++) {
			cancelNo[i] = Integer.parseInt(noStr[i]);
			
			/*System.out.println(cancelNo[i]);*/
		}
		
		Apply cancelApply = new Apply();
		cancelApply.setMemberNo(memberNo);
		
		boolean result = new ApplyService().cancelApply(cancelNo, cancelApply);
		/*System.out.println("입사지원취소용 result : " + result);*/
		
		
		if(result) {
			request.getRequestDispatcher("/selectApplyList.ap").forward(request, response);
		} else {
			request.setAttribute("msg", "입사지원내역 삭제 실패");
			request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		}
		
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
