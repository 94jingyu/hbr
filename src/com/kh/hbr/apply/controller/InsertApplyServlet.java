package com.kh.hbr.apply.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.apply.model.service.ApplyService;
import com.kh.hbr.apply.model.vo.Apply;
import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.recruit.model.service.RecruitService_Search;
import com.kh.hbr.recruit.model.vo.Recruit;

import sun.java2d.pipe.RegionClipSpanIterator;


@WebServlet("/insert.ap")
public class InsertApplyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public InsertApplyServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int resumeNo = Integer.parseInt(request.getParameter("resumeNo")); //불러 온 값
		int recruitNo = Integer.parseInt(request.getParameter("recruitNo")); //불러온 값
		int memberNo = ((Member) request.getSession().getAttribute("loginUser")).getMemberNo(); //불러온 값
		
//		System.out.println("resumeNo : " + resumeNo);
//		System.out.println("recruitNo : " + recruitNo);
//		System.out.println("memberNo : " + memberNo);
		
		Apply apply = new Apply();
		apply.setResumeNo(resumeNo);
		apply.setRecruitNo(recruitNo);
		apply.setMemberNo(memberNo);
		
		int result = new ApplyService().insertApply(apply);
		
		System.out.println("apply result : " + result);
		
		String page = "";
		if (result > 0) {
			page = "views/user_TAEWON/user_apply.jsp";
			request.getSession().setAttribute("result", result);
			response.sendRedirect(page);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "입사지원 실패");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
