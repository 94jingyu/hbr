package com.kh.hbr.apply.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.print.attribute.standard.RequestingUserName;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.apply.model.service.ApplyService;
import com.kh.hbr.apply.model.vo.Apply;
import com.kh.hbr.member.model.vo.Member;

@WebServlet("/search.ap")
public class SearchApplyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SearchApplyServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
		String openYn = request.getParameter("openYn");
		String cancelYn = request.getParameter("cancelYn");

		/*System.out.println("loginUser : " + loginUser);
		System.out.println("select 검색용 openYn : " + openYn);
		System.out.println("select 검색용 cancelYn : " + cancelYn);*/
		
		ArrayList<Apply> list = null;
		
		
		//열람여부 <select> 검색
		if(openYn.equals("Y")) {
			list = new ApplyService().openY(loginUser.getMemberNo());
			System.out.println("<select> openYn 검색 list : " + list);
		} else if(openYn.equals("N")) {
			list = new ApplyService().openN(loginUser.getMemberNo());
			System.out.println("<select> openYn 검색 list : " + list);
		}
		

		//지원상태 <select> 검색
		if(cancelYn.equals("applied")) {
			list = new ApplyService().applied(loginUser.getMemberNo());
			System.out.println("<select> applied 검색 list : " + list);
		} else if(cancelYn.equals("canceled")) {
			list = new ApplyService().canceled(loginUser.getMemberNo());
			System.out.println("<select> canceled 검색 list : " + list);
		}
		
		
		//공고마감상태여부 <select> 검색
		
		
		
		
		// 페이지 출력 안 됨 ㅠㅠ
		String page = "";
		if(list != null) {
			page = "views/user_TAEWON/user_applyHistory.jsp";
			request.setAttribute("list", list);
			response.sendRedirect(page);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "열람 검색 실패");
			request.getRequestDispatcher(page).forward(request, response);
		}
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
