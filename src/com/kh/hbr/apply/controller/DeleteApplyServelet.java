package com.kh.hbr.apply.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.apply.model.service.ApplyService;
import com.kh.hbr.apply.model.vo.Apply;
import com.kh.hbr.member.model.vo.Member;

@WebServlet("/update.ap")
public class DeleteApplyServelet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DeleteApplyServelet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//지원내역 취소용 서블릿, status 값 Y -> N
		int memberNo = ((Member) request.getSession().getAttribute("loginUser")).getMemberNo(); //불러온 값

		String deleteApplyNo = (String) request.getParameter("deleteApplyNo");
		String[] noStr = deleteApplyNo.split("#");
		int[] deleteNo = new int[noStr.length];
		
		for(int i = 0; i < deleteNo.length; i++) {
			deleteNo[i] = Integer.parseInt(noStr[i]);
			
			/*System.out.println(deleteNo[i]);*/
		}
		
		Apply deleteApply = new Apply();
		deleteApply.setMemberNo(memberNo);
		
		
		boolean result = new ApplyService().deleteApply(deleteNo, deleteApply);
		/*System.out.println("입사지원내역 삭제 result : " + result);*/
		
		
		if(result) {
			request.getRequestDispatcher("/selectApplyList.ap").forward(request, response);
		} else {
			request.setAttribute("msg", "입사지원내역 삭제 실패");
			request.getRequestDispatcher("views/common/errorPage.jsp").forward(request, response);
		}
		
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
