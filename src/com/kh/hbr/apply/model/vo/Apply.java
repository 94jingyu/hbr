package com.kh.hbr.apply.model.vo;

import java.sql.Date;

public class Apply implements java.io.Serializable{
	
	private int applyNo;
	private Date applyEnrollDate;
	private int resumeNo;
	private int recruitNo;
	private int memberNo;
	private String openYn;
	private String companyName;
	private String companyPhone;
	private String detailArea;
	private String recruitTitle;
	private Date expirationDate;
	private String cancelYn;
	private String status;
	
	public Apply() {}

	public Apply(int applyNo, Date applyEnrollDate, int resumeNo, int recruitNo, int memberNo, String openYn,
			String companyName, String companyPhone, String detailArea, String recruitTitle, Date expirationDate,
			String cancelYn, String status) {
		super();
		this.applyNo = applyNo;
		this.applyEnrollDate = applyEnrollDate;
		this.resumeNo = resumeNo;
		this.recruitNo = recruitNo;
		this.memberNo = memberNo;
		this.openYn = openYn;
		this.companyName = companyName;
		this.companyPhone = companyPhone;
		this.detailArea = detailArea;
		this.recruitTitle = recruitTitle;
		this.expirationDate = expirationDate;
		this.cancelYn = cancelYn;
		this.status = status;
	}

	public int getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(int applyNo) {
		this.applyNo = applyNo;
	}

	public Date getApplyEnrollDate() {
		return applyEnrollDate;
	}

	public void setApplyEnrollDate(Date applyEnrollDate) {
		this.applyEnrollDate = applyEnrollDate;
	}

	public int getResumeNo() {
		return resumeNo;
	}

	public void setResumeNo(int resumeNo) {
		this.resumeNo = resumeNo;
	}

	public int getRecruitNo() {
		return recruitNo;
	}

	public void setRecruitNo(int recruitNo) {
		this.recruitNo = recruitNo;
	}

	public int getMemberNo() {
		return memberNo;
	}

	public void setMemberNo(int memberNo) {
		this.memberNo = memberNo;
	}

	public String getOpenYn() {
		return openYn;
	}

	public void setOpenYn(String openYn) {
		this.openYn = openYn;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public String getDetailArea() {
		return detailArea;
	}

	public void setDetailArea(String detailArea) {
		this.detailArea = detailArea;
	}

	public String getRecruitTitle() {
		return recruitTitle;
	}

	public void setRecruitTitle(String recruitTitle) {
		this.recruitTitle = recruitTitle;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getCancelYn() {
		return cancelYn;
	}

	public void setCancelYn(String cancelYn) {
		this.cancelYn = cancelYn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Apply [applyNo=" + applyNo + ", applyEnrollDate=" + applyEnrollDate + ", resumeNo=" + resumeNo
				+ ", recruitNo=" + recruitNo + ", memberNo=" + memberNo + ", openYn=" + openYn + ", companyName="
				+ companyName + ", companyPhone=" + companyPhone + ", detailArea=" + detailArea + ", recruitTitle="
				+ recruitTitle + ", expirationDate=" + expirationDate + ", cancelYn=" + cancelYn + ", status=" + status
				+ "]";
	}


}