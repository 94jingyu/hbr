package com.kh.hbr.apply.model.dao;

import static com.kh.hbr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import com.kh.hbr.apply.model.vo.Apply;
import com.kh.hbr.recruit.model.vo.Recruit;
import com.sun.corba.se.spi.orbutil.fsm.State;

public class ApplyDao {

	private Properties prop = new Properties();
	
	public ApplyDao() {
		
		String fileName = Recruit.class.getResource("/sql/apply/apply-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	//입사지원내역 리스트 조회
	public ArrayList<Apply> selectList(Connection con, int memberNo) {
		
		ArrayList<Apply> alist = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectApplyList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberNo);
			rset = pstmt.executeQuery();
			
			alist = new ArrayList<Apply>();
			
			while(rset.next()) {

				Apply apply = new Apply();
				apply.setApplyNo(rset.getInt("APPLY_NO")); //와 진짜 이걸 못보냐
				apply.setApplyEnrollDate(rset.getDate("APPLY_ENROLL_DATE"));
				apply.setOpenYn(rset.getString("OPEN_YN"));
				apply.setCompanyName(rset.getString("COMPANY_NAME"));
				apply.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				apply.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
				apply.setCancelYn(rset.getString("CANCEL_YN"));
				
				alist.add(apply);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return alist;
	}
	
	//입사지원 insert
	public int insertApply(Connection con, Apply apply) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertApply");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, apply.getResumeNo());
			pstmt.setInt(2, apply.getRecruitNo());
			pstmt.setInt(3, apply.getMemberNo());

			result = pstmt.executeUpdate();

			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	
	//입사지원내역 삭제 : SQL업데이트문 사용
	public int deleteApply(Connection con, int i, Apply deleteApply) {

		PreparedStatement pstmt = null;
		int result = 0;

		String query = prop.getProperty("deleteApply");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, i);
			pstmt.setInt(2, deleteApply.getMemberNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
			
		return result;
	}

	
	//입사지원취소 : SQL업데이트문 사용
	public int cancelApply(Connection con, int i, Apply cancelApply) {

		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("cancelApply");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, i);
			pstmt.setInt(2, cancelApply.getMemberNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}


	//열람여부 <select> Y 검색
	public ArrayList<Apply> openY(Connection con, int memberNo) {

		ArrayList<Apply> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("openY");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberNo);
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Apply>();
			
			while(rset.next()) {

				Apply apply = new Apply();
				apply.setApplyNo(rset.getInt("APPLY_NO"));
				apply.setApplyEnrollDate(rset.getDate("APPLY_ENROLL_DATE"));
				apply.setOpenYn(rset.getString("OPEN_YN"));
				apply.setCompanyName(rset.getString("COMPANY_NAME"));
				apply.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				apply.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
				apply.setCancelYn(rset.getString("CANCEL_YN"));
				
				list.add(apply);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	//열람여부 <select> N 검색
	public ArrayList<Apply> openN(Connection con, int memberNo) {

		ArrayList<Apply> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("openN");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberNo);
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Apply>();
			
			while(rset.next()) {

				Apply apply = new Apply();
				apply.setApplyNo(rset.getInt("APPLY_NO"));
				apply.setApplyEnrollDate(rset.getDate("APPLY_ENROLL_DATE"));
				apply.setOpenYn(rset.getString("OPEN_YN"));
				apply.setCompanyName(rset.getString("COMPANY_NAME"));
				apply.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				apply.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
				apply.setCancelYn(rset.getString("CANCEL_YN"));
				
				list.add(apply);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	//열람여부 cancelYn <select> applied 검색
	public ArrayList<Apply> applied(Connection con, int memberNo) {

		ArrayList<Apply> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("applied");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberNo);
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Apply>();
			
			while(rset.next()) {

				Apply apply = new Apply();
				apply.setApplyNo(rset.getInt("APPLY_NO"));
				apply.setApplyEnrollDate(rset.getDate("APPLY_ENROLL_DATE"));
				apply.setOpenYn(rset.getString("OPEN_YN"));
				apply.setCompanyName(rset.getString("COMPANY_NAME"));
				apply.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				apply.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
				apply.setCancelYn(rset.getString("CANCEL_YN"));
				
				list.add(apply);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	//열람여부 cancelYn <select> canceled 검색
	public ArrayList<Apply> canceled(Connection con, int memberNo) {
		
		ArrayList<Apply> list = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("canceled");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberNo);
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Apply>();
			
			while(rset.next()) {

				Apply apply = new Apply();
				apply.setApplyNo(rset.getInt("APPLY_NO"));
				apply.setApplyEnrollDate(rset.getDate("APPLY_ENROLL_DATE"));
				apply.setOpenYn(rset.getString("OPEN_YN"));
				apply.setCompanyName(rset.getString("COMPANY_NAME"));
				apply.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				apply.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
				apply.setCancelYn(rset.getString("CANCEL_YN"));
				
				list.add(apply);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return list;
	}

	
	
	
	
	
	
	
	
	
	
}






























