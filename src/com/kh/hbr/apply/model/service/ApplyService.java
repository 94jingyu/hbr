package com.kh.hbr.apply.model.service;

import static com.kh.hbr.common.JDBCTemplate.*;
import java.sql.Connection;
import java.util.ArrayList;

import com.kh.hbr.apply.model.dao.ApplyDao;
import com.kh.hbr.apply.model.vo.Apply;

public class ApplyService {

	//입사지원 리스트 조회
	public ArrayList<Apply> selectList(int memberNo) {
		
		Connection con = getConnection();
		
		ArrayList<Apply> alist = new ApplyDao().selectList(con, memberNo);
		
		close(con);
		
		if(alist != null) {
			commit(con);
		} else {
			rollback(con);
		}
		
		return alist;
	}

	
	//입사지원하기
	public int insertApply(Apply apply) {
		
		Connection con = getConnection();
		
		int result = new ApplyDao().insertApply(con, apply);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	
	//입사지원내역 업데이트 (입사지원 삭제)
	public boolean deleteApply(int[] deleteNo, Apply deleteApply) {
		
		Connection con = getConnection();
		boolean finalResult = true;
		int result = 0;
		
		for(int i = 0; i < deleteNo.length; i++ ) {
			
			result = new ApplyDao().deleteApply(con, deleteNo[i], deleteApply);
			
			if(result > 0) {
				commit(con);
			} else {
				rollback(con);
			}
			
			if(result <= 0) {
				finalResult = false;
				
			}
			
		}
		
		return finalResult;
	}
	

	//입사지원내역 업데이트 (입사지원 취소)
	public boolean cancelApply(int[] cancelNo, Apply cancelApply) {
		
		Connection con = getConnection();
		boolean finalResult = true;
		int result = 0;
		
		for(int i = 0; i < cancelNo.length; i++ ) {
			
			result = new ApplyDao().cancelApply(con, cancelNo[i], cancelApply);
			
			if(result > 0) {
				commit(con);
			} else {
				rollback(con);
			}
			
			if(result <= 0) {
				finalResult = false;
				
			}
			
		}
		
		return finalResult;
	}

	
	
	//열람여부 open <select> Y 검색 
	public ArrayList<Apply> openY(int memberNo) {
		
		Connection con = getConnection();
		
		ArrayList<Apply> list = new ApplyDao().openY(con, memberNo); 
		
		close(con);
		
		return list;
	}

	//열람여부 open <select> N 검색
	public ArrayList<Apply> openN(int memberNo) {
		
		Connection con = getConnection();
		
		ArrayList<Apply> list = new ApplyDao().openN(con, memberNo); 
		
		close(con);
		
		return list;
	}

	//열람여부 cancelYn <select> applied 검색
	public ArrayList<Apply> applied(int memberNo) {
		
		Connection con = getConnection();
		
		ArrayList<Apply> list = new ApplyDao().applied(con, memberNo);
		
		close(con);
		
		return list;
	}

	//열람여부 cancelYn <select> canceled검색
	public ArrayList<Apply> canceled(int memberNo) {
		
		Connection con = getConnection();
		
		ArrayList<Apply> list = new ApplyDao().canceled(con, memberNo);
		
		close(con);
				
		return list;
	}

	

}
