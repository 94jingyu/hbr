package com.kh.hbr.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

// 공통적으로 작성하는 부분을 관리한다. (ex: request.setCharacterEncoding("UTF-8");) 

@WebFilter("/*")   // 필터 맵핑주소
public class CommonFilter implements Filter {

	
    public CommonFilter() {
    }

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest hRequest = (HttpServletRequest) request;

		if(hRequest.getMethod().equalsIgnoreCase("post")) {
			hRequest.setCharacterEncoding("UTF-8");
			//System.out.println("인코딩 완료!");  // 확인용
		}
		
		chain.doFilter(request, response);  // 필터가 여러 개일때 다음 필터로 전송 해주는 역할을 함
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}
