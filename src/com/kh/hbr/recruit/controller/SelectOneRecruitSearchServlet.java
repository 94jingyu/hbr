package com.kh.hbr.recruit.controller;
 
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.recruit.model.service.RecruitService_Search;
import com.kh.hbr.recruit.model.vo.Recruit;

@WebServlet("/selectOneSearch.rc")
public class SelectOneRecruitSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectOneRecruitSearchServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int num = Integer.parseInt(request.getParameter("num"));
		//System.out.println("recruitNum : " + num);
		
		HashMap<String, Object> hmap = new RecruitService_Search().selectOneThumbnailMap(num);
		/*System.out.println("thumbnailMap : " + hmap);*/
		
		Recruit searchRecruit = (Recruit) hmap.get("searchRecruit");
		ArrayList<Attachment> fileList = (ArrayList<Attachment>) hmap.get("fileList");
		/*Recruit searchRecruit = new RecruitService_Search().selectOne(num);
		System.out.println(searchRecruit);*/
		
		String page = "";
		if(hmap != null) {
			page = "views/user_TAEWON/user_apply.jsp";
			request.getSession().setAttribute("searchRecruit", searchRecruit);
			request.getSession().setAttribute("fileList", fileList);
			response.sendRedirect(page);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "채용공고상세보기 실패");
			request.getRequestDispatcher(page).forward(request, response);
		}
			
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
