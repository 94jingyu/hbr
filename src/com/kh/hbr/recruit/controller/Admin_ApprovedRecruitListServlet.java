package com.kh.hbr.recruit.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.recruit.model.service.RecruitService;
import com.kh.hbr.recruit.model.vo.Recruit;

/**
 * Servlet implementation class Admin_RecruitListServlet
 */
@WebServlet("/approvedList.ad")
public class Admin_ApprovedRecruitListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Admin_ApprovedRecruitListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ArrayList<Recruit> approvedList = new RecruitService().approvedList();
		
		//System.out.println("approved list servlet : " + approvedList );
	
		String page = "";
		if(approvedList != null) {
			page = "views/boguem_dj/DJ_admin_register.jsp";
			request.setAttribute("approvedList", approvedList);
		} else { 
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "공지사항 조회 실패!!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
