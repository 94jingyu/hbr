package com.kh.hbr.recruit.controller;
 
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.common.MyFileRenamePolicy;
import com.kh.hbr.payment.service.PaymentService;
import com.kh.hbr.recruit.model.service.RecruitService;
import com.kh.hbr.recruit.model.vo.Recruit;
import com.oreilly.servlet.MultipartRequest;

/**
 * Servlet implementation class RegisterUpdateCompleteServlet
 */
@WebServlet("/registerUpdateComplete.bg")
public class RegisterUpdateCompleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterUpdateCompleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
request.setCharacterEncoding("UTF-8");

		
		//사진 추가 부분
		if(ServletFileUpload.isMultipartContent(request)) {
			// 전송 파일 용량 제한: 500MB로 계산
			int maxSize = 156 * 60 * 500; 
			
			String root = request.getSession().getServletContext().getRealPath("/");
			
			System.out.println("(InsertRegiserServlet) root : " + root);
			
			// 파일 저장경로
			String savePath = root + "thumbnail_uploadFiles/";
			
			MultipartRequest multiRequest = 
					new MultipartRequest(request, savePath, maxSize, "UTF-8", new MyFileRenamePolicy());

			ArrayList<String> saveFiles = new ArrayList<String>();
			ArrayList<String> originFiles = new ArrayList<String>();
			
			Enumeration<String> files = multiRequest.getFileNames();
			
			while(files.hasMoreElements()) {
				String name = files.nextElement();
				System.out.println("(InsertRegiserServlet) name : " + name);
				
				saveFiles.add(multiRequest.getFilesystemName(name));
				originFiles.add(multiRequest.getOriginalFileName(name));
			}
			System.out.println("(InsertRegiserServlet) saveFiles : " + saveFiles);
			System.out.println("(InsertRegiserServlet) originFiles : " + originFiles);
		  
	
		 
			String bmemberNo = multiRequest.getParameter("bmemberNo");
		 String recruitNo = multiRequest.getParameter("recruitNo");
		 String companyName = multiRequest.getParameter("companyName");
		 String homepage = multiRequest.getParameter("homepage");
		 String businessContent = multiRequest.getParameter("businessContent");
		 String managerName = multiRequest.getParameter("managerName");
		 String managerEmail1 = multiRequest.getParameter("managerEmail1");
		 String managerEmail2 = multiRequest.getParameter("managerEmail2");
		 String managerEmail = managerEmail1 + "@" + managerEmail2;
		 String managerPhone1 = multiRequest.getParameter("managerPhone1");
		 String managerPhone2 = multiRequest.getParameter("managerPhone2");
		 String managerPhone3 = multiRequest.getParameter("managerPhone3");
		 String managerPhone = managerPhone1 + managerPhone2+ managerPhone3;
		/* System.out.println("1  :" + recruitNo ); 
		 System.out.println("2  :" + companyName); 
		 System.out.println("3  :" + homepage); 
		 System.out.println("4  :" + businessContent); 
		 System.out.println("5  :" + managerName); 
		 System.out.println("8  :" + managerEmail); 
		 System.out.println("9  :" + managerPhone);*/ 
		 
		
		 String companyPhone1 = multiRequest.getParameter("companyPhone1");
		 String companyPhone2 = multiRequest.getParameter("companyPhone2");
		 String companyPhone3 = multiRequest.getParameter("companyPhone3");
		 String companyPhone = companyPhone1 + "-" + companyPhone2 + "-" +companyPhone3;
		 String fax1 = multiRequest.getParameter("fax1");
		 String fax2 = multiRequest.getParameter("fax2");
		 String fax3 = multiRequest.getParameter("fax3");
		 String fax = fax1 + "-" + fax2 + "-" + fax3;
		 String recruitTitle = multiRequest.getParameter("recruitTitle");
		 String recruitMajor = multiRequest.getParameter("recruitMajor");
		 String jobType = multiRequest.getParameter("jobType");
		 String jobDate = multiRequest.getParameter("jobDate");
		 String jobTime1 = multiRequest.getParameter("jobTime1");
		 String jobTime2 = multiRequest.getParameter("jobTime2");
		 String jobTime3 = multiRequest.getParameter("jobTime3");
		 String jobTime4 = multiRequest.getParameter("jobTime4");
		 String jobTime = jobTime1+ ":" + jobTime2 + "~" + jobTime3 + ":" +jobTime4;
		 String jobTimeNego = multiRequest.getParameter("jobTimeNego");
		 String salaryType = multiRequest.getParameter("salaryType");
		 String salary = multiRequest.getParameter("salary");
		 /*System.out.println("10  :" + companyPhone );     
		 System.out.println("11  :" + fax );     
		 System.out.println("12  :" + recruitTitle);    
		 System.out.println("13  :" + recruitMajor);       
		 System.out.println("14  :" + jobType);
		 System.out.println("15  :" + jobDate);    
		 System.out.println("16  :" + jobTime);  
		 System.out.println("17  :" + jobTimeNego);  
		 System.out.println("18  :" + salaryType);   
		 System.out.println("19  :" + salary);  */ 
		 String salaryNego = multiRequest.getParameter("salaryNego");
		 String detailArea1 = multiRequest.getParameter("Area1");
		 String detailArea2 = multiRequest.getParameter("Area2");
		 String detailArea = detailArea1 +"-"+ detailArea2;
		 String interestGender = multiRequest.getParameter("interestGender");
		 String interestAgeYn = multiRequest.getParameter("interestAgeYn");
		 String interestAge1 = multiRequest.getParameter("interestAge1");
		 String interestAge2 = multiRequest.getParameter("interestAge2");
		 String interestAge = interestAge1 + "~" + interestAge2 ; 
		 String eduYn = multiRequest.getParameter("eduYn");
		 String education = multiRequest.getParameter("education");
		 String careerChoice = multiRequest.getParameter("careerChoice");
		 String careerPeriod = multiRequest.getParameter("careerPeriod");
		 String recruitPeople = multiRequest.getParameter("recruitPeople");
		 String preference = multiRequest.getParameter("preference");
		 String welfare = multiRequest.getParameter("welfare");
		 String detailText = multiRequest.getParameter("detailText");
		 /*System.out.println("20  :" + salaryNego );     
		 System.out.println("21  :" + detailArea );     
		 System.out.println("22  :" + interestGender);    
		 System.out.println("23  :" + interestAgeYn);       
		 System.out.println("24  :" + interestAge);
		 System.out.println("25  :" + eduYn);    
		 System.out.println("26  :" + education);  
		 System.out.println("27  :" + careerChoice);  
		 System.out.println("28  :" + careerPeriod);   
		 System.out.println("29  :" + preference); 
		 System.out.println("30  :" + welfare); 
		 System.out.println("31  :" + detailText); 
		 System.out.println("31  :" + recruitPeople); */
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		
			        
		 Recruit requestRecruit = new Recruit();
		 requestRecruit.setRecruitNo(Integer.parseInt(recruitNo));
		 requestRecruit.setBmemberNo(Integer.parseInt(bmemberNo));
	
		 requestRecruit.setCompanyName(companyName);
		 requestRecruit.setHomepage(homepage);
		 requestRecruit.setBusinessContent(businessContent);
		 requestRecruit.setManagerName(managerName);
		 requestRecruit.setManagerEmail(managerEmail);
		 requestRecruit.setManagerPhone(managerPhone);
		 requestRecruit.setCompanyPhone(companyPhone);
		 requestRecruit.setFax(fax);
		 requestRecruit.setRecruitTitle(recruitTitle);
		 requestRecruit.setRecruitMajor(recruitMajor);
		 requestRecruit.setJobType(jobType);
		 requestRecruit.setJobDate(jobDate);
		 requestRecruit.setJobTime(jobTime);
		 requestRecruit.setJobTimeNego(jobTimeNego);
		 requestRecruit.setSalaryType(salaryType);
		 requestRecruit.setSalary(salary);
		 requestRecruit.setSalaryNego(salaryNego);
		 requestRecruit.setDetailArea(detailArea);
		 requestRecruit.setInterestGender(interestGender);
		 requestRecruit.setInterestAgeYn(interestAgeYn);
		 requestRecruit.setInterestAge(interestAge);
		 requestRecruit.setEduYn(eduYn);
		 requestRecruit.setEducation(education);
		 requestRecruit.setCareerChoice(careerChoice);
		 requestRecruit.setCareerPeriod(careerPeriod);
		 requestRecruit.setRecruitPeople(recruitPeople);
		 requestRecruit.setPreference(preference);
		 requestRecruit.setWelfare(welfare);
		 requestRecruit.setDetailText(detailText);

		// System.out.println("AAinsert request recruit : " + requestRecruit);
		
		 ArrayList<Attachment> fileList = new ArrayList<Attachment>();
			for(int i = originFiles.size() - 1; i >= 0; i--) {
				Attachment at = new Attachment();
				at.setFilePath(savePath);
				at.setOriginName(originFiles.get(i));
				at.setChangeName(saveFiles.get(i));
				
				fileList.add(at);
			}
		// System.out.println("a : "+ fileList );
		 int result = new RecruitService().updateRecruitComplete(requestRecruit, fileList);
		 
		 //승인을 다시 심사대기로 상태 변경하기
		  int recruitno = Integer.parseInt(recruitNo);
		 
		 int result2 = new RecruitService().updateCheckStatus(recruitno);
		
		String page ="";
		if(result>0) {
			page="views/boguem_dj/DJ_boguem_register_returnComplete.jsp";
			request.getSession().setAttribute("requestRecruit", requestRecruit);
			response.sendRedirect(page);
			
		}else {
			
			//실패시 저장된 사진 삭제
			for(int i = 0; i < saveFiles.size(); i++) {
				File failedFile = new File(savePath + saveFiles.get(i));
				failedFile.delete();
			}
			
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg","테이블에 정보 삽입 실패!");
			request.getRequestDispatcher(page).forward(request,response);
		}
	}
}
	
		
		//값 update하기 사진 수정 전
	/*	request.setCharacterEncoding("UTF-8");
        
		
		 String bmemberNo = request.getParameter("bmemberNo");
		 String productName = request.getParameter("productName");
		 String companyName = request.getParameter("companyName");
		 String homepage = request.getParameter("homepage");
		 String businessContent = request.getParameter("businessContent");
		 String managerName = request.getParameter("managerName");
		 String managerEmail1 = request.getParameter("managerEmail1");
		 String managerEmail2 = request.getParameter("managerEmail2");
		 String managerEmail = managerEmail1 + "@" + managerEmail2;
		 String managerPhone1 = request.getParameter("managerPhone1");
		 String managerPhone2 = request.getParameter("managerPhone2");
		 String managerPhone3 = request.getParameter("managerPhone3");
		 String managerPhone = managerPhone1 + managerPhone2 + managerPhone3;
		 String companyPhone1 = request.getParameter("companyPhone1");
		 String companyPhone2 = request.getParameter("companyPhone2");
		 String companyPhone3 = request.getParameter("companyPhone3");
		 String companyPhone = companyPhone1 + "-" + companyPhone2 + "-" +companyPhone3;
		 String fax1 = request.getParameter("fax1");
		 String fax2 = request.getParameter("fax2");
		 String fax3 = request.getParameter("fax3");
		 String fax = fax1 + "-" + fax2 + "-" + fax3;
		 String recruitTitle = request.getParameter("recruitTitle");
		 String recruitMajor = request.getParameter("recruitMajor");
		 String jobType = request.getParameter("jobType");
		 String[] jobTypeArr = request.getParameterValues("jobType");
		 String jobType = String.join(",", jobTypeArr);
		 String jobDate = request.getParameter("jobDate");
		 String jobTime1 = request.getParameter("jobTime1");
		 String jobTime2 = request.getParameter("jobTime2");
		 String jobTime3 = request.getParameter("jobTime3");
		 String jobTime4 = request.getParameter("jobTime4");
		 String jobTime = jobTime1+ ":" + jobTime2 + "~" + jobTime3 + ":" +jobTime4;
		 String jobTimeNego = request.getParameter("jobTimeNego");
		 String salaryType = request.getParameter("salaryType");
		 String salary = request.getParameter("salary");
		 String salaryNego = request.getParameter("salaryNego");
		 String detailArea1 = request.getParameter("Area1");
		 String detailArea2 = request.getParameter("Area2");
		 String detailArea = detailArea1 +"-"+ detailArea2;
		 String interestGender = request.getParameter("interestGender");
		 String interestAgeYn = request.getParameter("interestAgeYn");
		 String interestAge1 = request.getParameter("interestAge1");
		 String interestAge2 = request.getParameter("interestAge2");
		 String interestAge = interestAge1 + "~" + interestAge2 ; 
		 String eduYn = request.getParameter("eduYn");
		 String education = request.getParameter("education");
		 String careerChoice = request.getParameter("careerChoice");
		 String careerPeriod = request.getParameter("careerPeriod");
		 String recruitPeople = request.getParameter("recruitPeople");
		 String preference = request.getParameter("preference");
		 String[] preferenceArr = request.getParameterValues("preference");
		 String preference = String.join(",", preferenceArr);
		 String[] welfareArr = request.getParameterValues("welfare");
		 String welfare = String.join(",", welfareArr);
		 String welfare = request.getParameter("welfare");
		 String detailText = request.getParameter("detailText");
		 String recruitNo = request.getParameter("recruitNo");
		 
	
		 Recruit requestRecruit = new Recruit();
		 
		 requestRecruit.setBmemberNo(Integer.parseInt(bmemberNo));
		 requestRecruit.setCompanyName(companyName);
		 requestRecruit.setHomepage(homepage);
		 requestRecruit.setBusinessContent(businessContent);
		 requestRecruit.setManagerName(managerName);
		 requestRecruit.setManagerEmail(managerEmail);
		 requestRecruit.setManagerPhone(managerPhone);
		 requestRecruit.setCompanyPhone(companyPhone);
		 requestRecruit.setFax(fax);
		 requestRecruit.setRecruitTitle(recruitTitle);
		 requestRecruit.setRecruitMajor(recruitMajor);
		 requestRecruit.setJobType(jobType);
		 requestRecruit.setJobDate(jobDate);
		 requestRecruit.setJobTime(jobTime);
		 requestRecruit.setJobTimeNego(jobTimeNego);
		 requestRecruit.setSalaryType(salaryType);
		 requestRecruit.setSalary(salary);
		 requestRecruit.setSalaryNego(salaryNego);
		 requestRecruit.setDetailArea(detailArea);
		 requestRecruit.setInterestGender(interestGender);
		 requestRecruit.setInterestAgeYn(interestAgeYn);
		 requestRecruit.setInterestAge(interestAge);
		 requestRecruit.setEduYn(eduYn);
		 requestRecruit.setEducation(education);
		 requestRecruit.setCareerChoice(careerChoice);
		 requestRecruit.setCareerPeriod(careerPeriod);
		 requestRecruit.setRecruitPeople(recruitPeople);
		 requestRecruit.setPreference(preference);
		 requestRecruit.setWelfare(welfare);
		 requestRecruit.setDetailText(detailText);
		 requestRecruit.setRecruitNo(Integer.parseInt(recruitNo));

		//System.out.println("update servlet recruit : " + requestRecruit);
		//여기까지가 다시 저장하는건
		int result = new RecruitService().updateRecruitComplete(requestRecruit);
		//새롭게 저장된 값을 다시 가지고 돌아와야하난 부분을 작성해야한다. 
		int registerNum =Integer.parseInt(recruitNo);
		
		Recruit result2 = new RecruitService().updateRecruitComplete2(registerNum);
		
		
		String page ="";
		if( result>0) {
		if( result>0  || result2!=null ) {
			page="views/boguem_dj/DJ_boguem_register_returnView.jsp";
			request.getSession().setAttribute("requestRecruit", requestRecruit);
			response.sendRedirect(page);
			
		}else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg","보금자리 수정 실패!");
			request.getRequestDispatcher(page).forward(request,response);
		}
	*/
	
	
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
