package com.kh.hbr.recruit.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.recruit.model.service.RecruitService_Search;
import com.kh.hbr.recruit.model.vo.Recruit;

@WebServlet("/searchCondition.rc")
public class SearchRecruitConditionServlet_tw extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SearchRecruitConditionServlet_tw() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String before = request.getParameter("condition");
		String[] condition = before.split("#");
		
		ArrayList<Recruit> list = new RecruitService_Search().selectConditonList(condition);
		System.out.println("조건검색 list : " + list);
		
		
		String page = "";
		if(list != null) {
			page = "views/user_TAEWON/user_searchRecruitSearch.jsp";
			request.setAttribute("list", list);
			
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "검색 실패");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
