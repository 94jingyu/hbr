package com.kh.hbr.recruit.controller;
 
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.recruit.model.service.RecruitService;
import com.kh.hbr.recruit.model.vo.Recruit;

/**
 * Servlet implementation class ApprovedServlet
 */
@WebServlet("/approved.ad")
public class ApprovedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ApprovedServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String recruitno = request.getParameter("recruitNo");
		//System.out.println(recruitno);
		int recruitNo = Integer.parseInt(recruitno);
		
		
		
		//recruit_check table update
		int result1 = new RecruitService().approvedRecruit(recruitNo);
		//System.out.println("1차 결과 : "+result1);
		//recruit table update
		int result2 = new RecruitService().approvedRecruitUpdate(recruitNo);
		//System.out.println("2차 결과 : "+result2);
		
		String productcount = request.getParameter("productCount");
		
		//System.out.println(productcount);
		
		int productCount = Integer.parseInt(productcount);
		if(productCount>0) {
		//use_product table update
		int result3 = new RecruitService().approvedProductUpdate(recruitNo, productCount);
		//System.out.println("3차 결과 : "+result3);
		
		String itemno = request.getParameter("itemNo");
		
		//System.out.println(itemno);
		
		int itemNo = Integer.parseInt(itemno);
		//myitem table update
		
		int result4 = new RecruitService().approvedMyitemUpdate(itemNo, productCount);
		
		ArrayList<Recruit> approvedList = new RecruitService().approvedList();
		
		//System.out.println("approved list servlet : " + approvedList );
		
		
		String page = "";
		if(result1 > 0 && result2 > 0 && result3 > 0 && result4 > 0) {
			page = "views/boguem_dj/DJ_admin_register.jsp";
			request.setAttribute("approvedList", approvedList);
			/*request.getSession().setAttribute("approvedList", approvedList);
			response.sendRedirect(page);
			*/
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "채용공고 승인 실패"); 
			
		}
		
		request.getRequestDispatcher(page).forward(request, response);
		}else {
			ArrayList<Recruit> approvedList = new RecruitService().approvedList();
			
			//System.out.println("approved list servlet : " + approvedList );
			
			String page = "";
			if(result1 == 1 && result2 == 1) {
				page = "views/boguem_dj/DJ_admin_register.jsp";
				request.setAttribute("approvedList", approvedList);
				/*request.getSession().setAttribute("approvedList", approvedList);
				response.sendRedirect(page);
				*/
			}else {
				page="views/common/errorPage.jsp";
				request.setAttribute("msg", "채용공고 승인 실패"); 
				
			}
			
			request.getRequestDispatcher(page).forward(request, response);
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
