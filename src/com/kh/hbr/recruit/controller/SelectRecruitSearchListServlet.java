package com.kh.hbr.recruit.controller;
 
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.recruit.model.service.RecruitService_Search;
import com.kh.hbr.recruit.model.vo.PageInfo;
import com.kh.hbr.recruit.model.vo.Recruit;


@WebServlet("/selectListSearch.rc")
public class SelectRecruitSearchListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SelectRecruitSearchListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//		//로그인
//		Member loginUser = (Member) request.getSession().getAttribute("loginUser");
//		//System.out.println("loginUser : " + loginUser);
		
		//---------- 페이징 처리 전
//		ArrayList<Recruit> slist = new RecruitService_Search().selectSearchList();
//		System.out.println("slist : " + slist);
		
		
		//---------- 페이징 처리 후 ----------
		int currentPage;
		int limit;
		int maxPage;
		int startPage;
		int endPage;
		
		currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		//한 페이지 게시물 개수
		limit = 10;
		
		RecruitService_Search rs = new RecruitService_Search();
		int listCount = rs.getListCount();
		System.out.println("listCount : " + listCount);
		
		maxPage = (int) ((double) listCount / limit + 0.9);
		startPage = (((int) ((double) currentPage / limit + 0.9)) - 1) * 10 + 1;
		endPage = startPage + 10 - 1;
		
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		PageInfo pi = new PageInfo(currentPage, listCount, limit, maxPage, startPage, endPage);
		
		ArrayList<Recruit> slist = new RecruitService_Search().selectSearchList(pi);
		System.out.println("페이징 slist : " + slist);
		
		
//		String page = "";
//		if(slist != null) {
//			page = "views/user_TAEWON/user_searchRecruit.jsp";
//			request.getSession().setAttribute("slist", slist);
//			request.setAttribute("pi", pi);
//			response.sendRedirect(page);
//		} else {
//			page = "views/common/errorPage.jsp";
//			request.setAttribute("msg", "채용공고 조회 실패!");
//			request.getRequestDispatcher(page).forward(request, response);
//		}
		
		String page = "";
		if(slist != null) {
			page = "views/user_TAEWON/user_searchRecruit.jsp";
			request.setAttribute("slist", slist);
			request.setAttribute("pi", pi);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "채용공고 조회 실패!");
		}
		request.getRequestDispatcher(page).forward(request, response);
		
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
