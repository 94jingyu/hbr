package com.kh.hbr.recruit.controller;
 
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.recruit.model.service.RecruitService;
import com.kh.hbr.recruit.model.vo.Recruit;

/**
 * Servlet implementation class SecondRejectServlet
 */
@WebServlet("/secondReject.ad")
public class SecondRejectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SecondRejectServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String recruitno = request.getParameter("recruitNo");
		int recruitNo = Integer.parseInt(recruitno);
		

		
		String reasonDetail = request.getParameter("reasonDetail");
		
	
		//System.out.println("recruitNo : "+recruitNo);
		//System.out.println("reasonDetail : "+reasonDetail);
		
		
		
		
		
		//RECRUIT_CHECK UPDATE
		int result1 = new RecruitService().rejectRecruit(recruitNo, reasonDetail);
		//RECRUIT UPDATE
		int result2 = new RecruitService().rejectRecruitUpdate(recruitNo);
		
		ArrayList<Recruit> approvedList = new RecruitService().secondRejectList();
		
		//System.out.println("approved list servlet : " + approvedList );
		
		
		String page = "";
		if(result1 == 1 && result2 == 1) {
			page = "views/boguem_dj/DJ_admin_register_screening.jsp";
			request.setAttribute("approvedList", approvedList);
	
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "채용공고 반려 실패");
			
		}
		request.getRequestDispatcher(page).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
