package com.kh.hbr.recruit.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.recruit.model.service.RecruitService;
import com.kh.hbr.recruit.model.vo.Recruit;

/**
 * Servlet implementation class DJ_admin_sorting
 */
@WebServlet("/sorting.ad")
public class DJ_admin_sorting extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DJ_admin_sorting() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String selecT = request.getParameter("select");
		int select = Integer.parseInt(selecT);
		ArrayList<Recruit> approvedList = null;
		
		//System.out.println(select);
		
		if(select == 0) {
			approvedList = new RecruitService().approvedList();
		}else if(select == 1){
			approvedList = new RecruitService().sortingList1();
		}else if(select == 2){
			approvedList = new RecruitService().sortingList2();
		}else if(select == 3){
			approvedList = new RecruitService().sortingList3();
		}
		//System.out.println("sortingList servlet : " + approvedList );
		
		String page = "";
		if(approvedList != null) {
			page = "views/boguem_dj/DJ_admin_register.jsp";
			request.setAttribute("approvedList", approvedList);
		} else { 
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "sorting 리스트 조회 실패!");
		}
		
		request.getRequestDispatcher(page).forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
