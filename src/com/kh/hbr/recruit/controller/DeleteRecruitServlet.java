package com.kh.hbr.recruit.controller;
 
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.recruit.model.service.RecruitService;
import com.kh.hbr.recruit.model.vo.Recruit;

/**
 * Servlet implementation class DeleteRecruitServlet
 */
@WebServlet("/deleteRecruit.bg")
public class DeleteRecruitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteRecruitServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String num = request.getParameter("num");
		String bnum = request.getParameter("bnum");
		
		//System.out.println("deleteRecruit num : "+ num);
		//System.out.println("bmem List num : "+ bnum);
		
		int recruitNum = Integer.parseInt(num);
		int listNum = Integer.parseInt(bnum);
		
		int recruit = new RecruitService().deleteRecruit(recruitNum);
		ArrayList<Recruit> list = new RecruitService().selectList(listNum);
		

		String page="";
		if(list != null) {
			page="views/boguem_dj/DJ_boguem_register_manage.jsp";
			/*request.setAttribute("list", list);*/
			request.getSession().setAttribute("list", list);
			response.sendRedirect(page);
		}else {
			page="views/common/errorPage.jsp";
			request.setAttribute("msg", "채용공고 목록 조회 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
