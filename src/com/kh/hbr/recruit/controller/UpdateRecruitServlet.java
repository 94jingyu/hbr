package com.kh.hbr.recruit.controller;
 
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.recruit.model.service.RecruitService;
import com.kh.hbr.recruit.model.vo.Recruit;

/**
 * Servlet implementation class UpdateRecruitServlet
 */
@WebServlet("/updateRecruit.bg")
public class UpdateRecruitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateRecruitServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int num = Integer.parseInt(request.getParameter("num"));
		
		HashMap<String, Object> hmap = new RecruitService().selectThumbnail3Map(num);
		
		System.out.println("thumbnail map : " + hmap);
		
		Recruit recruit = (Recruit) hmap.get("recruit");
		ArrayList<Attachment> fileList = (ArrayList<Attachment>) hmap.get("fileList");
		
		String page = "";
		if(hmap != null) {
			page = "views/boguem_dj/DJ_boguem_register_update.jsp";
			request.getSession().setAttribute("recruit", recruit);
			request.getSession().setAttribute("fileList", fileList);
			response.sendRedirect(page);
		} else {
			page = "views/common/errorPage.jsp";
			request.setAttribute("msg", "사진 게시판 상세 보기 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}
		
		
		
		//기존것
		/*String num = request.getParameter("num");
		//System.out.println("updateRegister num : " + num);
		int updateRegisterNum =Integer.parseInt(num);
		
		Recruit recruit = new RecruitService().updateRegister(updateRegisterNum);
		
		//System.out.println("업데이트 전 : "+recruit);
		
		String page = "";
		if(recruit != null) {
			page = "views/boguem_dj/DJ_boguem_register_update.jsp";
			request.getSession().setAttribute("recruit", recruit);
			response.sendRedirect(page);
		}else {
			page = "view/common/errorPage.jsp";
			request.setAttribute("msg", "게시글 상세 보기 실패!");
			request.getRequestDispatcher(page).forward(request, response);
		}*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
