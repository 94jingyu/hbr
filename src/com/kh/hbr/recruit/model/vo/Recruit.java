package com.kh.hbr.recruit.model.vo;

import java.sql.Date;

public class Recruit implements java.io.Serializable {
	
	private int bmemberNo;
	private int recruitNo;
	private String productName;
	//2차 추가
	private int productCount;
	private int itemNo;
	//
	private String companyName;
	private String homepage;
	private String businessContent;
	private String managerName;
	private String managerEmail;
	private String managerPhone;
	private String companyPhone;
	private String fax;
	private String recruitTitle;
	private String recruitMajor;
	private String jobType;
	private String jobDate;
	private String jobTime;
	private String jobTimeNego;
	private String salaryType;
	private String salary; 
	private String salaryNego;
	private String detailArea;
	private String interestGender;
	private String interestAgeYn;
	private String interestAge;
	private String eduYn;
	private String education;
	private String careerChoice;
	private String careerPeriod;
	private String recruitPeople;
	private String preference;
	private String welfare;
	private String detailText;
	private int clickCount;
	private String status;
	private String recruitDeleteYn;
	private Date enrollDate;
	private Date modifyDate;
	private Date expirationDate;
	
	//채용공고에 필요한 컬럼들...
		private String bmemberId;
		private int recruitCheckNo;
		private Date recruitCheckDate;
		private String approvedCode;
		private String rejectReason;
	
	public Recruit() {
		super();
	}

	public Recruit(int bmemberNo, int recruitNo, String productName, int productCount, int itemNo, String companyName,
			String homepage, String businessContent, String managerName, String managerEmail, String managerPhone,
			String companyPhone, String fax, String recruitTitle, String recruitMajor, String jobType, String jobDate,
			String jobTime, String jobTimeNego, String salaryType, String salary, String salaryNego, String detailArea,
			String interestGender, String interestAgeYn, String interestAge, String eduYn, String education,
			String careerChoice, String careerPeriod, String recruitPeople, String preference, String welfare,
			String detailText, int clickCount, String status, String recruitDeleteYn, Date enrollDate, Date modifyDate,
			Date expirationDate, String bmemberId, int recruitCheckNo, Date recruitCheckDate, String approvedCode,
			String rejectReason) {
		super();
		this.bmemberNo = bmemberNo;
		this.recruitNo = recruitNo;
		this.productName = productName;
		this.productCount = productCount;
		this.itemNo = itemNo;
		this.companyName = companyName;
		this.homepage = homepage;
		this.businessContent = businessContent;
		this.managerName = managerName;
		this.managerEmail = managerEmail;
		this.managerPhone = managerPhone;
		this.companyPhone = companyPhone;
		this.fax = fax;
		this.recruitTitle = recruitTitle;
		this.recruitMajor = recruitMajor;
		this.jobType = jobType;
		this.jobDate = jobDate;
		this.jobTime = jobTime;
		this.jobTimeNego = jobTimeNego;
		this.salaryType = salaryType;
		this.salary = salary;
		this.salaryNego = salaryNego;
		this.detailArea = detailArea;
		this.interestGender = interestGender;
		this.interestAgeYn = interestAgeYn;
		this.interestAge = interestAge;
		this.eduYn = eduYn;
		this.education = education;
		this.careerChoice = careerChoice;
		this.careerPeriod = careerPeriod;
		this.recruitPeople = recruitPeople;
		this.preference = preference;
		this.welfare = welfare;
		this.detailText = detailText;
		this.clickCount = clickCount;
		this.status = status;
		this.recruitDeleteYn = recruitDeleteYn;
		this.enrollDate = enrollDate;
		this.modifyDate = modifyDate;
		this.expirationDate = expirationDate;
		this.bmemberId = bmemberId;
		this.recruitCheckNo = recruitCheckNo;
		this.recruitCheckDate = recruitCheckDate;
		this.approvedCode = approvedCode;
		this.rejectReason = rejectReason;
	}

	public int getBmemberNo() {
		return bmemberNo;
	}

	public void setBmemberNo(int bmemberNo) {
		this.bmemberNo = bmemberNo;
	}

	public int getRecruitNo() {
		return recruitNo;
	}

	public void setRecruitNo(int recruitNo) {
		this.recruitNo = recruitNo;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getProductCount() {
		return productCount;
	}

	public void setProductCount(int productCount) {
		this.productCount = productCount;
	}

	public int getItemNo() {
		return itemNo;
	}

	public void setItemNo(int itemNo) {
		this.itemNo = itemNo;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public String getBusinessContent() {
		return businessContent;
	}

	public void setBusinessContent(String businessContent) {
		this.businessContent = businessContent;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public String getManagerEmail() {
		return managerEmail;
	}

	public void setManagerEmail(String managerEmail) {
		this.managerEmail = managerEmail;
	}

	public String getManagerPhone() {
		return managerPhone;
	}

	public void setManagerPhone(String managerPhone) {
		this.managerPhone = managerPhone;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getRecruitTitle() {
		return recruitTitle;
	}

	public void setRecruitTitle(String recruitTitle) {
		this.recruitTitle = recruitTitle;
	}

	public String getRecruitMajor() {
		return recruitMajor;
	}

	public void setRecruitMajor(String recruitMajor) {
		this.recruitMajor = recruitMajor;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getJobDate() {
		return jobDate;
	}

	public void setJobDate(String jobDate) {
		this.jobDate = jobDate;
	}

	public String getJobTime() {
		return jobTime;
	}

	public void setJobTime(String jobTime) {
		this.jobTime = jobTime;
	}

	public String getJobTimeNego() {
		return jobTimeNego;
	}

	public void setJobTimeNego(String jobTimeNego) {
		this.jobTimeNego = jobTimeNego;
	}

	public String getSalaryType() {
		return salaryType;
	}

	public void setSalaryType(String salaryType) {
		this.salaryType = salaryType;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getSalaryNego() {
		return salaryNego;
	}

	public void setSalaryNego(String salaryNego) {
		this.salaryNego = salaryNego;
	}

	public String getDetailArea() {
		return detailArea;
	}

	public void setDetailArea(String detailArea) {
		this.detailArea = detailArea;
	}

	public String getInterestGender() {
		return interestGender;
	}

	public void setInterestGender(String interestGender) {
		this.interestGender = interestGender;
	}

	public String getInterestAgeYn() {
		return interestAgeYn;
	}

	public void setInterestAgeYn(String interestAgeYn) {
		this.interestAgeYn = interestAgeYn;
	}

	public String getInterestAge() {
		return interestAge;
	}

	public void setInterestAge(String interestAge) {
		this.interestAge = interestAge;
	}

	public String getEduYn() {
		return eduYn;
	}

	public void setEduYn(String eduYn) {
		this.eduYn = eduYn;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getCareerChoice() {
		return careerChoice;
	}

	public void setCareerChoice(String careerChoice) {
		this.careerChoice = careerChoice;
	}

	public String getCareerPeriod() {
		return careerPeriod;
	}

	public void setCareerPeriod(String careerPeriod) {
		this.careerPeriod = careerPeriod;
	}

	public String getRecruitPeople() {
		return recruitPeople;
	}

	public void setRecruitPeople(String recruitPeople) {
		this.recruitPeople = recruitPeople;
	}

	public String getPreference() {
		return preference;
	}

	public void setPreference(String preference) {
		this.preference = preference;
	}

	public String getWelfare() {
		return welfare;
	}

	public void setWelfare(String welfare) {
		this.welfare = welfare;
	}

	public String getDetailText() {
		return detailText;
	}

	public void setDetailText(String detailText) {
		this.detailText = detailText;
	}

	public int getClickCount() {
		return clickCount;
	}

	public void setClickCount(int clickCount) {
		this.clickCount = clickCount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRecruitDeleteYn() {
		return recruitDeleteYn;
	}

	public void setRecruitDeleteYn(String recruitDeleteYn) {
		this.recruitDeleteYn = recruitDeleteYn;
	}

	public Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getBmemberId() {
		return bmemberId;
	}

	public void setBmemberId(String bmemberId) {
		this.bmemberId = bmemberId;
	}

	public int getRecruitCheckNo() {
		return recruitCheckNo;
	}

	public void setRecruitCheckNo(int recruitCheckNo) {
		this.recruitCheckNo = recruitCheckNo;
	}

	public Date getRecruitCheckDate() {
		return recruitCheckDate;
	}

	public void setRecruitCheckDate(Date recruitCheckDate) {
		this.recruitCheckDate = recruitCheckDate;
	}

	public String getApprovedCode() {
		return approvedCode;
	}

	public void setApprovedCode(String approvedCode) {
		this.approvedCode = approvedCode;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	@Override
	public String toString() {
		return "Recruit [bmemberNo=" + bmemberNo + ", recruitNo=" + recruitNo + ", productName=" + productName
				+ ", productCount=" + productCount + ", itemNo=" + itemNo + ", companyName=" + companyName
				+ ", homepage=" + homepage + ", businessContent=" + businessContent + ", managerName=" + managerName
				+ ", managerEmail=" + managerEmail + ", managerPhone=" + managerPhone + ", companyPhone=" + companyPhone
				+ ", fax=" + fax + ", recruitTitle=" + recruitTitle + ", recruitMajor=" + recruitMajor + ", jobType="
				+ jobType + ", jobDate=" + jobDate + ", jobTime=" + jobTime + ", jobTimeNego=" + jobTimeNego
				+ ", salaryType=" + salaryType + ", salary=" + salary + ", salaryNego=" + salaryNego + ", detailArea="
				+ detailArea + ", interestGender=" + interestGender + ", interestAgeYn=" + interestAgeYn
				+ ", interestAge=" + interestAge + ", eduYn=" + eduYn + ", education=" + education + ", careerChoice="
				+ careerChoice + ", careerPeriod=" + careerPeriod + ", recruitPeople=" + recruitPeople + ", preference="
				+ preference + ", welfare=" + welfare + ", detailText=" + detailText + ", clickCount=" + clickCount
				+ ", status=" + status + ", recruitDeleteYn=" + recruitDeleteYn + ", enrollDate=" + enrollDate
				+ ", modifyDate=" + modifyDate + ", expirationDate=" + expirationDate + ", bmemberId=" + bmemberId
				+ ", recruitCheckNo=" + recruitCheckNo + ", recruitCheckDate=" + recruitCheckDate + ", approvedCode="
				+ approvedCode + ", rejectReason=" + rejectReason + "]";
	}

	 

	
}
