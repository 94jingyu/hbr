package com.kh.hbr.recruit.model.dao;

import static com.kh.hbr.common.JDBCTemplate.close;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.recruit.model.vo.PageInfo;
import com.kh.hbr.recruit.model.vo.Recruit;

public class RecruitDao_Search {

	private Properties prop = new Properties();

	public RecruitDao_Search() {
		String fileName = RecruitDao.class.getResource("/sql/recruit/recruitSearch-query.properties").getPath();

		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//태원 채용공고페이지 selectSearchList
	public ArrayList<Recruit> selectSearchList(Connection con) {

		ArrayList<Recruit> slist = null;
		Statement stmt = null;
		ResultSet rset = null;

		String query = prop.getProperty("selectSearchList");

		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);

			slist = new ArrayList<>();

			while(rset.next()) {

				Recruit r = new Recruit();
				r.setRecruitNo(rset.getInt("RECRUIT_NO"));
				r.setDetailArea(rset.getString("DETAIL_AREA"));
				r.setCompanyName(rset.getString("COMPANY_NAME"));
				r.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				r.setJobTime(rset.getString("JOB_TIME"));
				r.setSalaryType(rset.getString("SALARY_TYPE"));
				r.setSalary(rset.getString("SALARY"));
				r.setEnrollDate(rset.getDate("ENROLLDATE"));
				
				slist.add(r);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}

		return slist;
	}

	//채용공고 상세보기 클릭 
	public Recruit selectOne(Connection con, int recruitNum) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Recruit searchRecruit = null;
		
		String query = prop.getProperty("selectOneSearch");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, recruitNum);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				searchRecruit = new Recruit();
				
//				searchRecruit.setRecruitNo(rset.getInt("RECRUIT_NO"));
//				searchRecruit.setDetailArea(rset.getString("DETAIL_AREA"));
//				searchRecruit.setCompanyName(rset.getString("RECRUIT_TITLE"));
//				searchRecruit.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
//				searchRecruit.setJobTime(rset.getString("JOB_TIME"));
//				searchRecruit.setSalaryType(rset.getString("SALARY_TYPE"));
//				searchRecruit.setSalary(rset.getNString("SALARY"));
//				searchRecruit.setEnrollDate(rset.getDate("ENROLLDATE"));
				
				searchRecruit.setRecruitNo(rset.getInt("RECRUIT_NO"));
				searchRecruit.setProductName(rset.getString("PRODUCT_NAME"));
				searchRecruit.setCompanyName(rset.getString("COMPANY_NAME"));
				searchRecruit.setHomepage(rset.getString("HOMEPAGE"));
				searchRecruit.setBusinessContent(rset.getString("BUSINESS_CONTENT"));
				searchRecruit.setManagerName(rset.getString("MANAGER_NAME"));
				searchRecruit.setManagerEmail(rset.getString("MANAGER_EMAIL"));
				searchRecruit.setManagerPhone(rset.getString("MANAGER_PHONE"));
				searchRecruit.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				searchRecruit.setFax(rset.getString("FAX"));
				searchRecruit.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				searchRecruit.setRecruitMajor(rset.getString("RECRUIT_MAJOR"));
				searchRecruit.setJobType(rset.getString("JOB_TYPE"));
				searchRecruit.setJobDate(rset.getString("JOB_DATE"));
				searchRecruit.setJobTime(rset.getString("JOB_TIME"));
				searchRecruit.setJobTimeNego(rset.getString("JOB_TIME_NEGO"));
				searchRecruit.setSalaryType(rset.getString("SALARY_TYPE"));
				searchRecruit.setSalary(rset.getString("SALARY"));
				searchRecruit.setSalaryNego(rset.getString("SALARY_NEGO"));
				searchRecruit.setDetailArea(rset.getString("DETAIL_AREA"));
				searchRecruit.setInterestGender(rset.getString("INTEREST_GENDER"));
				searchRecruit.setInterestAgeYn(rset.getString("INTEREST_AGE_YN"));
				searchRecruit.setInterestAge(rset.getString("INTEREST_AGE"));
				searchRecruit.setEduYn(rset.getString("EDU_YN"));
				searchRecruit.setEducation(rset.getString("EDUCATION"));
				searchRecruit.setCareerChoice(rset.getString("CAREER_CHOICE"));
				searchRecruit.setCareerPeriod(rset.getString("CAREER_PERIOD"));
				searchRecruit.setRecruitPeople(rset.getString("RECRUIT_PEOPLE"));
				searchRecruit.setPreference(rset.getString("PREFERENCE"));
				searchRecruit.setWelfare(rset.getString("WELFARE"));
				searchRecruit.setDetailText(rset.getString("DETAIL_TEXT"));
				searchRecruit.setRecruitDeleteYn(rset.getString("RECRUIT_DELETE_YN"));
				searchRecruit.setClickCount(rset.getInt("CLICK_COUNT"));
				searchRecruit.setStatus(rset.getString("STATUS"));
				searchRecruit.setEnrollDate(rset.getDate("ENROLLDATE"));
				searchRecruit.setModifyDate(rset.getDate("MODIFYDATE"));
				searchRecruit.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
				
			} 
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return searchRecruit;
	}
	
	
	//채용공고 상세보기 클릭 카운트
	public int updateCount(Connection con, int num) {

		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateCount");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			pstmt.setInt(2, num);
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	//사진가져오기
	public HashMap<String, Object> selectOneThumbnailMap(Connection con, int num) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		HashMap<String, Object> hmap = null;
		Recruit searchRecruit = null;
		ArrayList<Attachment> list = null;
		Attachment at = null;
		
		String query = prop.getProperty("selectRecruitView");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, num);
			
			rset = pstmt.executeQuery();
			
			hmap = new HashMap<String, Object>();
			list = new ArrayList<Attachment>();
			
			if(rset.next()) {
				searchRecruit = new Recruit();
				searchRecruit.setBmemberNo(rset.getInt("BMEMBER_NO"));
				searchRecruit.setRecruitNo(rset.getInt("RECRUIT_NO"));
				searchRecruit.setCompanyName(rset.getString("COMPANY_NAME"));
				searchRecruit.setHomepage(rset.getString("HOMEPAGE"));
				searchRecruit.setBusinessContent(rset.getString("BUSINESS_CONTENT"));
				searchRecruit.setManagerName(rset.getString("MANAGER_NAME"));
				searchRecruit.setManagerEmail(rset.getString("MANAGER_EMAIL"));
				searchRecruit.setManagerPhone(rset.getString("MANAGER_PHONE"));
				searchRecruit.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				searchRecruit.setFax(rset.getString("FAX"));
				searchRecruit.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				searchRecruit.setRecruitMajor(rset.getString("RECRUIT_MAJOR"));
				searchRecruit.setJobType(rset.getString("JOB_TYPE"));
				searchRecruit.setJobDate(rset.getString("JOB_DATE"));
				searchRecruit.setJobTime(rset.getString("JOB_TIME"));
				searchRecruit.setJobTimeNego(rset.getString("JOB_TIME_NEGO"));
				searchRecruit.setSalaryType(rset.getString("SALARY_TYPE"));
				searchRecruit.setSalary(rset.getString("SALARY"));
				searchRecruit.setSalaryNego(rset.getString("SALARY_NEGO"));
				searchRecruit.setDetailArea(rset.getString("DETAIL_AREA"));
				searchRecruit.setInterestGender(rset.getString("INTEREST_GENDER"));
				searchRecruit.setInterestAgeYn(rset.getString("INTEREST_AGE_YN"));
				searchRecruit.setInterestAge(rset.getString("INTEREST_AGE"));
				searchRecruit.setEduYn(rset.getString("EDU_YN"));
				searchRecruit.setEducation(rset.getString("EDUCATION"));
				searchRecruit.setCareerChoice(rset.getString("CAREER_CHOICE"));
				searchRecruit.setCareerPeriod(rset.getString("CAREER_PERIOD"));
				searchRecruit.setRecruitPeople(rset.getString("RECRUIT_PEOPLE"));
				searchRecruit.setPreference(rset.getString("PREFERENCE"));
				searchRecruit.setWelfare(rset.getString("WELFARE"));
				searchRecruit.setDetailText(rset.getString("DETAIL_TEXT"));
				searchRecruit.setRecruitDeleteYn(rset.getString("RECRUIT_DELETE_YN"));
				searchRecruit.setClickCount(rset.getInt("CLICK_COUNT"));
				searchRecruit.setStatus(rset.getString("STATUS"));
				searchRecruit.setEnrollDate(rset.getDate("ENROLLDATE"));
				searchRecruit.setModifyDate(rset.getDate("MODIFYDATE"));
				searchRecruit.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
				
				at = new Attachment();
				at.setAttachmentNo(rset.getInt("ATTACHMENT_NO"));
				at.setOriginName(rset.getString("ORIGIN_NAME"));
				at.setChangeName(rset.getString("CHANGE_NAME"));
				at.setFilePath(rset.getString("FILE_PATH"));
				at.setUploadDate(rset.getDate("UPLOAD_DATE"));
				
				list.add(at);
				
			}
			
			hmap.put("searchRecruit", searchRecruit);
			hmap.put("fileList", list);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		
		return hmap;
	}

	
	//검색조건 
	public ArrayList<Recruit> selectConditonList(Connection con, String[] condition) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Recruit searchRecruit = null;
		
		ArrayList<Recruit> list = null;
		
		
		String query = prop.getProperty("selectConditionList");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, condition[0]);
			pstmt.setString(2, condition[1]);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList(); 
			
			while(rset.next()) {
				
				Recruit r = new Recruit();
				r.setRecruitNo(rset.getInt("RECRUIT_NO"));
				r.setDetailArea(rset.getString("DETAIL_AREA"));
				r.setCompanyName(rset.getString("COMPANY_NAME"));
				r.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				r.setJobTime(rset.getString("JOB_TIME"));
				r.setSalaryType(rset.getString("SALARY_TYPE"));
				r.setSalary(rset.getString("SALARY"));
				r.setEnrollDate(rset.getDate("ENROLLDATE"));
				
				list.add(r);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
			
		}
		
		
		return list;
	}

	//페이징 전체게시물
	public int getListCount(Connection con) {

		Statement stmt = null;
		ResultSet rset = null;
		
		int listCount = 0;
		
		String query = prop.getProperty("listCount");
	
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				listCount = rset.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(rset);
		}
		
		return listCount;
	}
	
	//페이징
	public ArrayList<Recruit> selectSearchList(Connection con, PageInfo pi) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Recruit> list = null;
		
		String query = prop.getProperty("selectListSearchWithPaging");
		
		try {
			pstmt = con.prepareStatement(query);
			
			int startRow = (pi.getCurrentPage() - 1) * pi.getLimit() + 1;
			int endRow = startRow + pi.getLimit() - 1;
			
			pstmt.setInt(1, startRow);
			pstmt.setInt(2, endRow);
			
			rset = pstmt.executeQuery();
			
			list = new ArrayList<Recruit>();
			
			while(rset.next()) {
				
				Recruit r = new Recruit();
				
				r.setRecruitNo(rset.getInt("RECRUIT_NO"));
				r.setDetailArea(rset.getString("DETAIL_AREA"));
				r.setCompanyName(rset.getString("COMPANY_NAME"));
				r.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				r.setJobTime(rset.getString("JOB_TIME"));
				r.setSalaryType(rset.getString("SALARY_TYPE"));
				r.setSalary(rset.getString("SALARY"));
				r.setEnrollDate(rset.getDate("ENROLLDATE"));
				
				list.add(r);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		
		return list;
	}





}
