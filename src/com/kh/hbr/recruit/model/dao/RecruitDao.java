package com.kh.hbr.recruit.model.dao;
 
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.recruit.model.vo.Recruit;
import static com.kh.hbr.common.JDBCTemplate.*;
import static com.kh.hbr.common.JDBCTemplate.close;


public class RecruitDao {
	
	private Properties prop = new Properties();
	
	public RecruitDao() {
		String fileName = RecruitDao.class.getResource("/sql/recruit/recruit-query.properties").getPath();
		
		try {
			prop.load(new FileReader(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int insertRecruit(Connection con, Recruit requestRecruit) {
		
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("insertRecruit");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1,  requestRecruit.getBmemberNo());
			/*pstmt.setString( requestRecruit.getProductName());*/
			pstmt.setString(2,  requestRecruit.getCompanyName());
			pstmt.setString(3,  requestRecruit.getHomepage());
			pstmt.setString(4,  requestRecruit.getBusinessContent());
			pstmt.setString(5,  requestRecruit.getManagerName());
			pstmt.setString(6,  requestRecruit.getManagerEmail());
			pstmt.setString(7,  requestRecruit.getManagerPhone());
			pstmt.setString(8,  requestRecruit.getCompanyPhone());
			pstmt.setString(9,  requestRecruit.getFax());
			pstmt.setString(10, requestRecruit.getRecruitTitle());
			pstmt.setString(11, requestRecruit.getRecruitMajor());
			pstmt.setString(12, requestRecruit.getJobType());
			pstmt.setString(13, requestRecruit.getJobDate());
			pstmt.setString(14, requestRecruit.getJobTime());
			pstmt.setString(15, requestRecruit.getJobTimeNego());
			pstmt.setString(16, requestRecruit.getSalaryType());
			pstmt.setString(17, requestRecruit.getSalary());
			pstmt.setString(18, requestRecruit.getSalaryNego());
			pstmt.setString(19, requestRecruit.getDetailArea());
			pstmt.setString(20, requestRecruit.getInterestGender());
			pstmt.setString(21, requestRecruit.getInterestAgeYn());
			pstmt.setString(22, requestRecruit.getInterestAge());
			pstmt.setString(23, requestRecruit.getEduYn());
			pstmt.setString(24, requestRecruit.getEducation());
			pstmt.setString(25, requestRecruit.getCareerChoice());
			pstmt.setString(26, requestRecruit.getCareerPeriod());
			pstmt.setString(27, requestRecruit.getRecruitPeople());
			pstmt.setString(28, requestRecruit.getPreference());
			pstmt.setString(29, requestRecruit.getWelfare());
			pstmt.setString(30, requestRecruit.getDetailText());
			                
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {                                  
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}

	public ArrayList<Recruit> selectList(Connection con, int memberNum) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		ArrayList<Recruit> list = null;
		
		String query = prop.getProperty("selectList");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, memberNum);
			
			rset =pstmt.executeQuery();
			list = new ArrayList<>();
			System.out.println("dao:"+list);
			
			while(rset.next()) {
				Recruit rc = new Recruit();
				rc.setRecruitNo(rset.getInt("RECRUIT_NO"));
				rc.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				rc.setProductName(rset.getString("PRODUCT_NAME"));
				rc.setClickCount(rset.getInt("CLICK_COUNT"));
				rc.setStatus(rset.getString("STATUS"));
				rc.setEnrollDate(rset.getDate("ENROLLDATE"));
				rc.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
				rc.setDetailArea(rset.getString("DETAIL_AREA"));
				rc.setRecruitMajor(rset.getString("RECRUIT_MAJOR"));
				
				list.add(rc);
			
		}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		/*ArrayList<Recruit> list = null;
		Statement stmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectList");
		
		try {
			stmt = con.createStatement();
			System.out.println("list : "+list);
			rset = stmt.executeQuery(query);
			
			list = new ArrayList<>();
			System.out.println("dao:"+list);
			
			while(rset.next()) {
				Recruit rc = new Recruit();
				rc.setRecruitNo(rset.getInt("RECRUIT_NO"));
				rc.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				rc.setProductName(rset.getString("PRODUCT_NAME"));
				rc.setClickCount(rset.getString("CLICK_COUNT"));
				rc.setStatus(rset.getString("STATUS"));
				rc.setEnrollDate(rset.getDate("ENROLLDATE"));
				rc.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
				rc.setDetailArea(rset.getString("DETAIL_AREA"));
				rc.setRecruitMajor(rset.getString("RECRUIT_MAJOR"));
				
				list.add(rc);
				
			}
			System.out.println("dao:"+list);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(stmt);
			close(rset);
		}*/
		
		
		
		return list;
	}

	public Recruit recruitVeiw(Connection con, int recruitNum) {
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		Recruit recruit = null;
		
		String query = prop.getProperty("recruitView");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, recruitNum);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				recruit = new Recruit();
				recruit.setBmemberNo(rset.getInt("BMEMBER_NO"));
				recruit.setRecruitNo(rset.getInt("RECRUIT_NO"));
				recruit.setProductName(rset.getString("PRODUCT_NAME"));
				recruit.setCompanyName(rset.getString("COMPANY_NAME"));
				recruit.setHomepage(rset.getString("HOMEPAGE"));
				recruit.setBusinessContent(rset.getString("BUSINESS_CONTENT"));
				recruit.setManagerName(rset.getString("MANAGER_NAME"));
				recruit.setManagerEmail(rset.getString("MANAGER_EMAIL"));
				recruit.setManagerPhone(rset.getString("MANAGER_PHONE"));
				recruit.setCompanyPhone(rset.getString("COMPANY_PHONE"));
				recruit.setFax(rset.getString("FAX"));
				recruit.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
				recruit.setRecruitMajor(rset.getString("RECRUIT_MAJOR"));
				recruit.setJobType(rset.getString("JOB_TYPE"));
				recruit.setJobDate(rset.getString("JOB_DATE"));
				recruit.setJobTime(rset.getString("JOB_TIME"));
				recruit.setJobTimeNego(rset.getString("JOB_TIME_NEGO"));
				recruit.setSalaryType(rset.getString("SALARY_TYPE"));
				recruit.setSalary(rset.getString("SALARY"));
				recruit.setSalaryNego(rset.getString("SALARY_NEGO"));
				recruit.setDetailArea(rset.getString("DETAIL_AREA"));
				recruit.setInterestGender(rset.getString("INTEREST_GENDER"));
				recruit.setInterestAgeYn(rset.getString("INTEREST_AGE_YN"));
				recruit.setInterestAge(rset.getString("INTEREST_AGE"));
				recruit.setEduYn(rset.getString("EDU_YN"));
				recruit.setEducation(rset.getString("EDUCATION"));
				recruit.setCareerChoice(rset.getString("CAREER_CHOICE"));
				recruit.setCareerPeriod(rset.getString("CAREER_PERIOD"));
				recruit.setRecruitPeople(rset.getString("RECRUIT_PEOPLE"));
				recruit.setPreference(rset.getString("PREFERENCE"));
				recruit.setWelfare(rset.getString("WELFARE"));
				recruit.setDetailText(rset.getString("DETAIL_TEXT"));
				recruit.setRecruitDeleteYn(rset.getString("RECRUIT_DELETE_YN"));
				recruit.setClickCount(rset.getInt("CLICK_COUNT"));
				recruit.setStatus(rset.getString("STATUS"));
				recruit.setEnrollDate(rset.getDate("ENROLLDATE"));
				recruit.setModifyDate(rset.getDate("MODIFYDATE"));
				recruit.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
				
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close(pstmt);
			close(rset);
		}
		
		return recruit;
		
	
	}

	

	public int updateRecruitComplete(Connection con, Recruit requestRecruit) {
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("updateRegister");
		String status = "심사대기";
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1,    requestRecruit.getCompanyName());
			pstmt.setString(2,  requestRecruit.getHomepage());
			pstmt.setString(3,  requestRecruit.getBusinessContent());
			pstmt.setString(4,  requestRecruit.getManagerName());
			pstmt.setString(5,  requestRecruit.getManagerEmail());
			pstmt.setString(6,  requestRecruit.getManagerPhone());
			pstmt.setString(7,  requestRecruit.getCompanyPhone());
			pstmt.setString(8,  requestRecruit.getFax());
			pstmt.setString(9,  requestRecruit.getRecruitTitle());
			pstmt.setString(10, requestRecruit.getRecruitMajor());
			pstmt.setString(11, requestRecruit.getJobType());
			pstmt.setString(12, requestRecruit.getJobDate());
			pstmt.setString(13, requestRecruit.getJobTime());
			pstmt.setString(14, requestRecruit.getJobTimeNego());
			pstmt.setString(15, requestRecruit.getSalaryType());
			pstmt.setString(16, requestRecruit.getSalary());
			pstmt.setString(17, requestRecruit.getSalaryNego());
			pstmt.setString(18, requestRecruit.getDetailArea());
			pstmt.setString(19, requestRecruit.getInterestGender());
			pstmt.setString(20, requestRecruit.getInterestAgeYn());
			pstmt.setString(21, requestRecruit.getInterestAge());
			pstmt.setString(22, requestRecruit.getEduYn());
			pstmt.setString(23, requestRecruit.getEducation());
			pstmt.setString(24, requestRecruit.getCareerChoice());
			pstmt.setString(25, requestRecruit.getCareerPeriod());
			pstmt.setString(26, requestRecruit.getRecruitPeople());
			pstmt.setString(27, requestRecruit.getPreference());
			pstmt.setString(28, requestRecruit.getWelfare());
			pstmt.setString(29, requestRecruit.getDetailText());
			pstmt.setString(30, status);
			pstmt.setInt   (31, requestRecruit.getRecruitNo());
			                
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {                                  
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}

	public int deleteRecruit(Connection con, int recruitNum) {
		int result = 0;
		PreparedStatement pstmt = null;
		String query = prop.getProperty("deleteRecruit");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1,  recruitNum);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public int findRecruitNo(Connection con) {
		int recruitNo = 0;
		Statement stmt = null;
		String query = prop.getProperty("findRecruitNo");
		ResultSet rset = null;
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			if(rset.next()) {
				recruitNo = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(stmt);
			close(rset);
		}
		return recruitNo;
	}

	public int insertRecruitCheck(Connection con, int recruitNo) {
		int result2 = 0;
		PreparedStatement pstmt = null;
		String query =prop.getProperty("insertRecruitCheck");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, recruitNo);
			
			result2 =pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close(pstmt);
		}
		return result2;
	}

	//관리자 공고 심사온 리스트 불러오기
		public ArrayList<Recruit> approvedList(Connection con) {
			ArrayList<Recruit> approvedList = null;
			Statement stmt = null;
			ResultSet rset = null;
			//System.out.println("여기도 왔니?");
			String query = prop.getProperty("approvedList");
			
			try {
				stmt = con.createStatement();
				rset = stmt.executeQuery(query);
				
				approvedList = new ArrayList<>();
				
				while(rset.next()) {
					Recruit r = new Recruit();
					r.setBmemberId(rset.getString("BMEMBER_ID"));
					r.setManagerName(rset.getString("MANAGER_NAME"));
					r.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
					r.setStatus(rset.getString("STATUS"));
					r.setEnrollDate(rset.getDate("ENROLLDATE"));
					r.setRecruitCheckDate(rset.getDate("RECRUIT_CHECK_DATE"));
					r.setRecruitNo(rset.getInt("RECRUIT_NO"));
					
					approvedList.add(r);
					
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(stmt);
				close(rset);
			}
			
			return approvedList;
		}

		
		//채용공고 반려 부분
		public int rejectRecruit(Connection con, int recruitNo, String reasonDetail) {
			
			int result1 = 0;
			PreparedStatement pstmt = null;
	
			String query = prop.getProperty("rejectRecruit");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setString(1, reasonDetail);
				pstmt.setInt(2, recruitNo);
				
				result1 = pstmt.executeUpdate();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
			}
			return result1;
			
		}

		public int rejectRecruitUpdate(Connection con, int recruitNo) {
			int result2 = 0;
			PreparedStatement pstmt = null;
	
			String query = prop.getProperty("rejectRecruitUpdate");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, recruitNo);
				
				result2 = pstmt.executeUpdate();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
			}
			return result2;
		}
		//승인된 리스트만 불러오기.
		public ArrayList<Recruit> listApproved(Connection con) {
			ArrayList<Recruit> approvedList = null;
			Statement stmt = null;
			ResultSet rset = null;
			
			String query = prop.getProperty("listApproved");
			
			try {
				stmt = con.createStatement();
				rset = stmt.executeQuery(query);
				
				approvedList = new ArrayList<>();
				
				while(rset.next()) {
					Recruit r = new Recruit();
					r.setBmemberId(rset.getString("BMEMBER_ID"));
					r.setManagerName(rset.getString("MANAGER_NAME"));
					r.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
					r.setStatus(rset.getString("STATUS"));
					r.setEnrollDate(rset.getDate("ENROLLDATE"));
					r.setRecruitCheckDate(rset.getDate("RECRUIT_CHECK_DATE"));
					r.setRecruitNo(rset.getInt("RECRUIT_NO"));
					
					approvedList.add(r);
					
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(stmt);
				close(rset);
			}
			
			return approvedList;
		}

		public ArrayList<Recruit> secondRejectList(Connection con) {
			ArrayList<Recruit> approvedList = null;
			Statement stmt = null;
			ResultSet rset = null;
			
			String query = prop.getProperty("listApproved");
			
			try {
				stmt = con.createStatement();
				rset = stmt.executeQuery(query);
				
				approvedList = new ArrayList<>();
				
				while(rset.next()) {
					Recruit r = new Recruit();
					r.setBmemberId(rset.getString("BMEMBER_ID"));
					r.setManagerName(rset.getString("MANAGER_NAME"));
					r.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
					r.setStatus(rset.getString("STATUS"));
					r.setEnrollDate(rset.getDate("ENROLLDATE"));
					r.setRecruitCheckDate(rset.getDate("RECRUIT_CHECK_DATE"));
					r.setRecruitNo(rset.getInt("RECRUIT_NO"));
					
					approvedList.add(r);
					
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(stmt);
				close(rset);
			}
			
			return approvedList;
		}

		public int approvedRecruit(Connection con, int recruitNo) {
			int result1 = 0;
			PreparedStatement pstmt = null;
	
			String query = prop.getProperty("approvedRecruit");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, recruitNo);
				
				result1 = pstmt.executeUpdate();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
			}
			return result1;
		}

		public int approvedRecruitUpdate(Connection con, int recruitNo) {
			int result2 = 0;
			PreparedStatement pstmt = null;
	
			String query = prop.getProperty("approvedRecruitUpdate");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, recruitNo);
				
				result2 = pstmt.executeUpdate();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
			}
			return result2;
		}

		public int rejectItem(Connection con, int recruitNo) {
			int result3 = 0;
			PreparedStatement pstmt = null;
	
			String query = prop.getProperty("rejectItem");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, recruitNo);
				
				result3 = pstmt.executeUpdate();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
			}
			return result3;
		}
		

		public int approvedProductUpdate(Connection con, int recruitNo, int productCount) {
			int result3 = 0;
			PreparedStatement pstmt = null;
	
			String query = prop.getProperty("approvedProductUpdate");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, productCount);
				pstmt.setInt(2, recruitNo);
				
				result3 = pstmt.executeUpdate();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
			}
			return result3;
		}
 
		public int approvedMyitemUpdate(Connection con, int itemNo, int productCount) {
			int result4 = 0;
			PreparedStatement pstmt = null;
	
			String query = prop.getProperty("approvedMyitemUpdate");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, itemNo);
				pstmt.setInt(2, productCount);
				pstmt.setInt(3, itemNo);
				
				result4 = pstmt.executeUpdate();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
			}
			return result4;
		}
		//currval
		public int selectCurrval(Connection con) {
			Statement stmt = null;
			ResultSet rset = null;
			int bid = 0;
			
			String query = prop.getProperty("selectCurrval");
			
			try {
				stmt = con.createStatement();
				rset = stmt.executeQuery(query);
				
				if(rset.next()) {
					bid = rset.getInt("currval");
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(stmt);
				close(rset);
			}
			
			return bid;
		}
		//로고등록관련 메소드
		public int insertAttachment(Connection con, Attachment attachment) {
			PreparedStatement pstmt = null;
			int result = 0;
			
			String query = prop.getProperty("insertAttachment");

			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, attachment.getBoardNo());
				pstmt.setString(2, attachment.getOriginName());
				pstmt.setString(3, attachment.getChangeName());
				pstmt.setString(4, attachment.getFilePath());
				
				result = pstmt.executeUpdate();
			
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close(pstmt);
			}
			
			return result;
			
		}

		public HashMap<String, Object> selectThumbnailMap(Connection con, int num) {
			
			PreparedStatement pstmt = null;
			ResultSet rset = null;
			HashMap<String, Object> hmap = null;
			Recruit recruit = null;
			ArrayList<Attachment> list = null;
			Attachment at = null;
			
			String query = prop.getProperty("selectRecruitView");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, num);
				
				rset = pstmt.executeQuery();
				
				hmap = new HashMap<String, Object>();
				list = new ArrayList<Attachment>();
				
				if(rset.next()) {
					recruit = new Recruit();
					recruit.setBmemberNo(rset.getInt("BMEMBER_NO"));
					recruit.setRecruitNo(rset.getInt("RECRUIT_NO"));
					recruit.setCompanyName(rset.getString("COMPANY_NAME"));
					recruit.setHomepage(rset.getString("HOMEPAGE"));
					recruit.setBusinessContent(rset.getString("BUSINESS_CONTENT"));
					recruit.setManagerName(rset.getString("MANAGER_NAME"));
					recruit.setManagerEmail(rset.getString("MANAGER_EMAIL"));
					recruit.setManagerPhone(rset.getString("MANAGER_PHONE"));
					recruit.setCompanyPhone(rset.getString("COMPANY_PHONE"));
					recruit.setFax(rset.getString("FAX"));
					recruit.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
					recruit.setRecruitMajor(rset.getString("RECRUIT_MAJOR"));
					recruit.setJobType(rset.getString("JOB_TYPE"));
					recruit.setJobDate(rset.getString("JOB_DATE"));
					recruit.setJobTime(rset.getString("JOB_TIME"));
					recruit.setJobTimeNego(rset.getString("JOB_TIME_NEGO"));
					recruit.setSalaryType(rset.getString("SALARY_TYPE"));
					recruit.setSalary(rset.getString("SALARY"));
					recruit.setSalaryNego(rset.getString("SALARY_NEGO"));
					recruit.setDetailArea(rset.getString("DETAIL_AREA"));
					recruit.setInterestGender(rset.getString("INTEREST_GENDER"));
					recruit.setInterestAgeYn(rset.getString("INTEREST_AGE_YN"));
					recruit.setInterestAge(rset.getString("INTEREST_AGE"));
					recruit.setEduYn(rset.getString("EDU_YN"));
					recruit.setEducation(rset.getString("EDUCATION"));
					recruit.setCareerChoice(rset.getString("CAREER_CHOICE"));
					recruit.setCareerPeriod(rset.getString("CAREER_PERIOD"));
					recruit.setRecruitPeople(rset.getString("RECRUIT_PEOPLE"));
					recruit.setPreference(rset.getString("PREFERENCE"));
					recruit.setWelfare(rset.getString("WELFARE"));
					recruit.setDetailText(rset.getString("DETAIL_TEXT"));
					recruit.setRecruitDeleteYn(rset.getString("RECRUIT_DELETE_YN"));
					recruit.setClickCount(rset.getInt("CLICK_COUNT"));
					recruit.setStatus(rset.getString("STATUS"));
					recruit.setEnrollDate(rset.getDate("ENROLLDATE"));
					recruit.setModifyDate(rset.getDate("MODIFYDATE"));
					recruit.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
					recruit.setRejectReason(rset.getString("REJECT_REASON"));
					
					at = new Attachment();
					at.setAttachmentNo(rset.getInt("ATTACHMENT_NO"));
					at.setOriginName(rset.getString("ORIGIN_NAME"));
					at.setChangeName(rset.getString("CHANGE_NAME"));
					at.setFilePath(rset.getString("FILE_PATH"));
					at.setUploadDate(rset.getDate("UPLOAD_DATE"));
					
					list.add(at);
					
				}
				
				hmap.put("recruit", recruit);
				hmap.put("fileList", list);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
				close(rset);
			}
			
			return hmap;
		}
		//기존 채용공고 심사 상세보기
		public Recruit checkRecruitView2(Connection con, int recruitNum) {
			PreparedStatement pstmt = null;
			ResultSet rset = null;
			Recruit recruit = null;
			
			String query = prop.getProperty("checkRecruitView");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, recruitNum);
				
				rset = pstmt.executeQuery();
				
				if(rset.next()) {
					recruit = new Recruit();                                        
					recruit.setBmemberNo(rset.getInt("BMEMBER_NO"));                
					recruit.setRecruitNo(rset.getInt("RECRUIT_NO"));                
					recruit.setProductName(rset.getString("PRODUCT_NAME"));         
					recruit.setProductCount(rset.getInt("USE_UNIT"));               
					recruit.setItemNo(rset.getInt("MYITEM_NO"));                    
					recruit.setCompanyName(rset.getString("COMPANY_NAME"));         
					recruit.setHomepage(rset.getString("HOMEPAGE"));                
					recruit.setBusinessContent(rset.getString("BUSINESS_CONTENT")); 
					recruit.setManagerName(rset.getString("MANAGER_NAME"));         
					recruit.setManagerEmail(rset.getString("MANAGER_EMAIL"));       
					recruit.setManagerPhone(rset.getString("MANAGER_PHONE"));       
					recruit.setCompanyPhone(rset.getString("COMPANY_PHONE"));       
					recruit.setFax(rset.getString("FAX"));                          
					recruit.setRecruitTitle(rset.getString("RECRUIT_TITLE"));       
					recruit.setRecruitMajor(rset.getString("RECRUIT_MAJOR"));       
					recruit.setJobType(rset.getString("JOB_TYPE"));                 
					recruit.setJobDate(rset.getString("JOB_DATE"));                 
					recruit.setJobTime(rset.getString("JOB_TIME"));                 
					recruit.setJobTimeNego(rset.getString("JOB_TIME_NEGO"));        
					recruit.setSalaryType(rset.getString("SALARY_TYPE"));           
					recruit.setSalary(rset.getString("SALARY"));                    
					recruit.setSalaryNego(rset.getString("SALARY_NEGO"));           
					recruit.setDetailArea(rset.getString("DETAIL_AREA"));           
					recruit.setInterestGender(rset.getString("INTEREST_GENDER"));   
					recruit.setInterestAgeYn(rset.getString("INTEREST_AGE_YN"));    
					recruit.setInterestAge(rset.getString("INTEREST_AGE"));         
					recruit.setEduYn(rset.getString("EDU_YN"));                     
					recruit.setEducation(rset.getString("EDUCATION"));              
					recruit.setCareerChoice(rset.getString("CAREER_CHOICE"));       
					recruit.setCareerPeriod(rset.getString("CAREER_PERIOD"));       
					recruit.setRecruitPeople(rset.getString("RECRUIT_PEOPLE"));     
					recruit.setPreference(rset.getString("PREFERENCE"));            
					recruit.setWelfare(rset.getString("WELFARE"));                  
					recruit.setDetailText(rset.getString("DETAIL_TEXT"));           
					recruit.setRecruitDeleteYn(rset.getString("RECRUIT_DELETE_YN"));
					recruit.setClickCount(rset.getInt("CLICK_COUNT"));              
					recruit.setStatus(rset.getString("STATUS"));                    
					recruit.setEnrollDate(rset.getDate("ENROLLDATE"));
					recruit.setModifyDate(rset.getDate("MODIFYDATE"));
					recruit.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
					
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
				close(rset);
			}
			
			return recruit;
		}
		
		//기존 채용공고 뷰 부분
		public HashMap<String, Object> checkRecruitView2Map(Connection con, int num) {
			PreparedStatement pstmt = null;
			ResultSet rset = null;
			HashMap<String, Object> hmap = null;
			Recruit recruit = null;
			ArrayList<Attachment> list = null;
			Attachment at = null;
			
			String query = prop.getProperty("checkRecruitView");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, num);
				
				rset = pstmt.executeQuery();
				
				hmap = new HashMap<String, Object>();
				list = new ArrayList<Attachment>();
				
				if(rset.next()) {
					recruit = new Recruit();                                        
					recruit.setBmemberNo(rset.getInt("BMEMBER_NO"));                
					recruit.setRecruitNo(rset.getInt("RECRUIT_NO"));                
					recruit.setProductName(rset.getString("PRODUCT_NAME"));         
					recruit.setProductCount(rset.getInt("USE_UNIT"));               
					recruit.setItemNo(rset.getInt("MYITEM_NO"));                    
					recruit.setCompanyName(rset.getString("COMPANY_NAME"));         
					recruit.setHomepage(rset.getString("HOMEPAGE"));                
					recruit.setBusinessContent(rset.getString("BUSINESS_CONTENT")); 
					recruit.setManagerName(rset.getString("MANAGER_NAME"));         
					recruit.setManagerEmail(rset.getString("MANAGER_EMAIL"));       
					recruit.setManagerPhone(rset.getString("MANAGER_PHONE"));       
					recruit.setCompanyPhone(rset.getString("COMPANY_PHONE"));       
					recruit.setFax(rset.getString("FAX"));                          
					recruit.setRecruitTitle(rset.getString("RECRUIT_TITLE"));       
					recruit.setRecruitMajor(rset.getString("RECRUIT_MAJOR"));       
					recruit.setJobType(rset.getString("JOB_TYPE"));                 
					recruit.setJobDate(rset.getString("JOB_DATE"));                 
					recruit.setJobTime(rset.getString("JOB_TIME"));                 
					recruit.setJobTimeNego(rset.getString("JOB_TIME_NEGO"));        
					recruit.setSalaryType(rset.getString("SALARY_TYPE"));           
					recruit.setSalary(rset.getString("SALARY"));                    
					recruit.setSalaryNego(rset.getString("SALARY_NEGO"));           
					recruit.setDetailArea(rset.getString("DETAIL_AREA"));           
					recruit.setInterestGender(rset.getString("INTEREST_GENDER"));   
					recruit.setInterestAgeYn(rset.getString("INTEREST_AGE_YN"));    
					recruit.setInterestAge(rset.getString("INTEREST_AGE"));         
					recruit.setEduYn(rset.getString("EDU_YN"));                     
					recruit.setEducation(rset.getString("EDUCATION"));              
					recruit.setCareerChoice(rset.getString("CAREER_CHOICE"));       
					recruit.setCareerPeriod(rset.getString("CAREER_PERIOD"));       
					recruit.setRecruitPeople(rset.getString("RECRUIT_PEOPLE"));     
					recruit.setPreference(rset.getString("PREFERENCE"));            
					recruit.setWelfare(rset.getString("WELFARE"));                  
					recruit.setDetailText(rset.getString("DETAIL_TEXT"));           
					recruit.setRecruitDeleteYn(rset.getString("RECRUIT_DELETE_YN"));
					recruit.setClickCount(rset.getInt("CLICK_COUNT"));              
					recruit.setStatus(rset.getString("STATUS"));                    
					recruit.setEnrollDate(rset.getDate("ENROLLDATE"));
					recruit.setModifyDate(rset.getDate("MODIFYDATE"));
					recruit.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
					recruit.setRejectReason(rset.getString("REJECT_REASON"));
				
				at = new Attachment();
				at.setAttachmentNo(rset.getInt("ATTACHMENT_NO"));
				at.setOriginName(rset.getString("ORIGIN_NAME"));
				at.setChangeName(rset.getString("CHANGE_NAME"));
				at.setFilePath(rset.getString("FILE_PATH"));
				at.setUploadDate(rset.getDate("UPLOAD_DATE"));
				
				list.add(at);
		}
		
				hmap.put("recruit", recruit);
				hmap.put("fileList", list);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
				close(rset);
			}
			
			return hmap;
		

		
		}
		//기존 승인후 재심사 부분
		public Recruit checkRecruitView(Connection con, int recruitNum) {
			PreparedStatement pstmt = null;
			ResultSet rset = null;
			Recruit recruit = null;
			
			String query = prop.getProperty("recruitView");
			
			try {
				pstmt = con.prepareStatement(query);
				pstmt.setInt(1, recruitNum);
				
				rset = pstmt.executeQuery();
				
				if(rset.next()) {
					recruit = new Recruit();
					recruit.setBmemberNo(rset.getInt("BMEMBER_NO"));
					recruit.setRecruitNo(rset.getInt("RECRUIT_NO"));
					recruit.setProductName(rset.getString("PRODUCT_NAME"));
					recruit.setCompanyName(rset.getString("COMPANY_NAME"));
					recruit.setHomepage(rset.getString("HOMEPAGE"));
					recruit.setBusinessContent(rset.getString("BUSINESS_CONTENT"));
					recruit.setManagerName(rset.getString("MANAGER_NAME"));
					recruit.setManagerEmail(rset.getString("MANAGER_EMAIL"));
					recruit.setManagerPhone(rset.getString("MANAGER_PHONE"));
					recruit.setCompanyPhone(rset.getString("COMPANY_PHONE"));
					recruit.setFax(rset.getString("FAX"));
					recruit.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
					recruit.setRecruitMajor(rset.getString("RECRUIT_MAJOR"));
					recruit.setJobType(rset.getString("JOB_TYPE"));
					recruit.setJobDate(rset.getString("JOB_DATE"));
					recruit.setJobTime(rset.getString("JOB_TIME"));
					recruit.setJobTimeNego(rset.getString("JOB_TIME_NEGO"));
					recruit.setSalaryType(rset.getString("SALARY_TYPE"));
					recruit.setSalary(rset.getString("SALARY"));
					recruit.setSalaryNego(rset.getString("SALARY_NEGO"));
					recruit.setDetailArea(rset.getString("DETAIL_AREA"));
					recruit.setInterestGender(rset.getString("INTEREST_GENDER"));
					recruit.setInterestAgeYn(rset.getString("INTEREST_AGE_YN"));
					recruit.setInterestAge(rset.getString("INTEREST_AGE"));
					recruit.setEduYn(rset.getString("EDU_YN"));
					recruit.setEducation(rset.getString("EDUCATION"));
					recruit.setCareerChoice(rset.getString("CAREER_CHOICE"));
					recruit.setCareerPeriod(rset.getString("CAREER_PERIOD"));
					recruit.setRecruitPeople(rset.getString("RECRUIT_PEOPLE"));
					recruit.setPreference(rset.getString("PREFERENCE"));
					recruit.setWelfare(rset.getString("WELFARE"));
					recruit.setDetailText(rset.getString("DETAIL_TEXT"));
					recruit.setRecruitDeleteYn(rset.getString("RECRUIT_DELETE_YN"));
					recruit.setClickCount(rset.getInt("CLICK_COUNT"));
					recruit.setStatus(rset.getString("STATUS"));
					recruit.setEnrollDate(rset.getDate("ENROLLDATE"));
					recruit.setModifyDate(rset.getDate("MODIFYDATE"));
					recruit.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
					
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(pstmt);
				close(rset);
			}
			
			return recruit;
		}
		
		
		//sorting list 승인
		public ArrayList<Recruit> sortingList1(Connection con) {
			ArrayList<Recruit> approvedList = null;
			Statement stmt = null;
			ResultSet rset = null;
			
			String query = prop.getProperty("sortingList1");
			
			try {
				stmt = con.createStatement();
				
				rset = stmt.executeQuery(query);
				
				
				approvedList = new ArrayList<>();
				
				while(rset.next()) {
					Recruit r = new Recruit();
					r.setBmemberId(rset.getString("BMEMBER_ID"));
					r.setManagerName(rset.getString("MANAGER_NAME"));
					r.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
					r.setStatus(rset.getString("STATUS"));
					r.setEnrollDate(rset.getDate("ENROLLDATE"));
					r.setRecruitCheckDate(rset.getDate("RECRUIT_CHECK_DATE"));
					r.setRecruitNo(rset.getInt("RECRUIT_NO"));
					
					approvedList.add(r);
					
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				close(stmt);
				close(rset);
			}
			
			return approvedList;
		}
		//sorting list 거절
				public ArrayList<Recruit> sortingList2(Connection con) {
					ArrayList<Recruit> approvedList = null;
					Statement stmt = null;
					ResultSet rset = null;
					
					String query = prop.getProperty("sortingList2");
					
					try {
						stmt = con.createStatement();
						
						rset = stmt.executeQuery(query);
						
						
						approvedList = new ArrayList<>();
						
						while(rset.next()) {
							Recruit r = new Recruit();
							r.setBmemberId(rset.getString("BMEMBER_ID"));
							r.setManagerName(rset.getString("MANAGER_NAME"));
							r.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
							r.setStatus(rset.getString("STATUS"));
							r.setEnrollDate(rset.getDate("ENROLLDATE"));
							r.setRecruitCheckDate(rset.getDate("RECRUIT_CHECK_DATE"));
							r.setRecruitNo(rset.getInt("RECRUIT_NO"));
							
							approvedList.add(r);
							
							
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}finally {
						close(stmt);
						close(rset);
					}
					
					return approvedList;
				}
				//sorting list 대기
				public ArrayList<Recruit> sortingList3(Connection con) {
					ArrayList<Recruit> approvedList = null;
					Statement stmt = null;
					ResultSet rset = null;
					
					String query = prop.getProperty("sortingList3");
					
					try {
						stmt = con.createStatement();
						
						rset = stmt.executeQuery(query);
						
						
						approvedList = new ArrayList<>();
						
						while(rset.next()) {
							Recruit r = new Recruit();
							r.setBmemberId(rset.getString("BMEMBER_ID"));
							r.setManagerName(rset.getString("MANAGER_NAME"));
							r.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
							r.setStatus(rset.getString("STATUS"));
							r.setEnrollDate(rset.getDate("ENROLLDATE"));
							r.setRecruitCheckDate(rset.getDate("RECRUIT_CHECK_DATE"));
							r.setRecruitNo(rset.getInt("RECRUIT_NO"));
							
							approvedList.add(r);
							
							
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}finally {
						close(stmt);
						close(rset);
					}
					
					return approvedList;
				}
				
				public Recruit updateRegister(Connection con, int updateRegisterNum) {
					PreparedStatement pstmt = null;
					ResultSet rset = null;
					Recruit recruit = null;
					
					String query = prop.getProperty("recruitView");
					
					try {
						pstmt = con.prepareStatement(query);
						pstmt.setInt(1, updateRegisterNum);
						
						rset = pstmt.executeQuery();
						
						if(rset.next()) {
							recruit = new Recruit();
							recruit.setBmemberNo(rset.getInt("BMEMBER_NO"));
							recruit.setRecruitNo(rset.getInt("RECRUIT_NO"));
							recruit.setProductName(rset.getString("PRODUCT_NAME"));
							recruit.setCompanyName(rset.getString("COMPANY_NAME"));
							recruit.setHomepage(rset.getString("HOMEPAGE"));
							recruit.setBusinessContent(rset.getString("BUSINESS_CONTENT"));
							recruit.setManagerName(rset.getString("MANAGER_NAME"));
							recruit.setManagerEmail(rset.getString("MANAGER_EMAIL"));
							recruit.setManagerPhone(rset.getString("MANAGER_PHONE"));
							recruit.setCompanyPhone(rset.getString("COMPANY_PHONE"));
							recruit.setFax(rset.getString("FAX"));
							recruit.setRecruitTitle(rset.getString("RECRUIT_TITLE"));
							recruit.setRecruitMajor(rset.getString("RECRUIT_MAJOR"));
							recruit.setJobType(rset.getString("JOB_TYPE"));
							recruit.setJobDate(rset.getString("JOB_DATE"));
							recruit.setJobTime(rset.getString("JOB_TIME"));
							recruit.setJobTimeNego(rset.getString("JOB_TIME_NEGO"));
							recruit.setSalaryType(rset.getString("SALARY_TYPE"));
							recruit.setSalary(rset.getString("SALARY"));
							recruit.setSalaryNego(rset.getString("SALARY_NEGO"));
							recruit.setDetailArea(rset.getString("DETAIL_AREA"));
							recruit.setInterestGender(rset.getString("INTEREST_GENDER"));
							recruit.setInterestAgeYn(rset.getString("INTEREST_AGE_YN"));
							recruit.setInterestAge(rset.getString("INTEREST_AGE"));
							recruit.setEduYn(rset.getString("EDU_YN"));
							recruit.setEducation(rset.getString("EDUCATION"));
							recruit.setCareerChoice(rset.getString("CAREER_CHOICE"));
							recruit.setCareerPeriod(rset.getString("CAREER_PERIOD"));
							recruit.setRecruitPeople(rset.getString("RECRUIT_PEOPLE"));
							recruit.setPreference(rset.getString("PREFERENCE"));
							recruit.setWelfare(rset.getString("WELFARE"));
							recruit.setDetailText(rset.getString("DETAIL_TEXT"));
							recruit.setRecruitDeleteYn(rset.getString("RECRUIT_DELETE_YN"));
							recruit.setClickCount(rset.getInt("CLICK_COUNT"));
							recruit.setStatus(rset.getString("STATUS"));
							recruit.setEnrollDate(rset.getDate("ENROLLDATE"));
							recruit.setModifyDate(rset.getDate("MODIFYDATE"));
							recruit.setExpirationDate(rset.getDate("EXPIRATIONDATE"));
							
						
							
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}finally {
						close(pstmt);
						close(rset);
					}
					
					return recruit;
				}
				//채용공고 수정하는 뷰 연결
				public HashMap<String, Object> selectThumbnail3Map(Connection con, int num) {
					PreparedStatement pstmt = null;
					ResultSet rset = null;
					HashMap<String, Object> hmap = null;
					Recruit recruit = null;
					ArrayList<Attachment> list = null;
					Attachment at = null;
					
					String query = prop.getProperty("recruitView2");
					
					try {
						pstmt = con.prepareStatement(query);
						pstmt.setInt(1, num);
						
						rset = pstmt.executeQuery();
						
						hmap = new HashMap<String, Object>();
						list = new ArrayList<Attachment>();
						
						if(rset.next()) {
							recruit = new Recruit();                                        
							recruit.setBmemberNo(rset.getInt("BMEMBER_NO"));                
							recruit.setRecruitNo(rset.getInt("RECRUIT_NO"));                
							recruit.setProductName(rset.getString("PRODUCT_NAME"));         
							recruit.setCompanyName(rset.getString("COMPANY_NAME"));         
							recruit.setHomepage(rset.getString("HOMEPAGE"));                
							recruit.setBusinessContent(rset.getString("BUSINESS_CONTENT")); 
							recruit.setManagerName(rset.getString("MANAGER_NAME"));         
							recruit.setManagerEmail(rset.getString("MANAGER_EMAIL"));       
							recruit.setManagerPhone(rset.getString("MANAGER_PHONE"));       
							recruit.setCompanyPhone(rset.getString("COMPANY_PHONE"));       
							recruit.setFax(rset.getString("FAX"));                          
							recruit.setRecruitTitle(rset.getString("RECRUIT_TITLE"));       
							recruit.setRecruitMajor(rset.getString("RECRUIT_MAJOR"));       
							recruit.setJobType(rset.getString("JOB_TYPE"));                 
							recruit.setJobDate(rset.getString("JOB_DATE"));                 
							recruit.setJobTime(rset.getString("JOB_TIME"));                 
							recruit.setJobTimeNego(rset.getString("JOB_TIME_NEGO"));        
							recruit.setSalaryType(rset.getString("SALARY_TYPE"));           
							recruit.setSalary(rset.getString("SALARY"));                    
							recruit.setSalaryNego(rset.getString("SALARY_NEGO"));           
							recruit.setDetailArea(rset.getString("DETAIL_AREA"));           
							recruit.setInterestGender(rset.getString("INTEREST_GENDER"));   
							recruit.setInterestAgeYn(rset.getString("INTEREST_AGE_YN"));    
							recruit.setInterestAge(rset.getString("INTEREST_AGE"));         
							recruit.setEduYn(rset.getString("EDU_YN"));                     
							recruit.setEducation(rset.getString("EDUCATION"));              
							recruit.setCareerChoice(rset.getString("CAREER_CHOICE"));       
							recruit.setCareerPeriod(rset.getString("CAREER_PERIOD"));       
							recruit.setRecruitPeople(rset.getString("RECRUIT_PEOPLE"));     
							recruit.setPreference(rset.getString("PREFERENCE"));            
							recruit.setWelfare(rset.getString("WELFARE"));                  
							recruit.setDetailText(rset.getString("DETAIL_TEXT"));           
							recruit.setRecruitDeleteYn(rset.getString("RECRUIT_DELETE_YN"));
							recruit.setClickCount(rset.getInt("CLICK_COUNT"));              
							recruit.setStatus(rset.getString("STATUS"));                    
							recruit.setEnrollDate(rset.getDate("ENROLLDATE"));              
							recruit.setModifyDate(rset.getDate("MODIFYDATE"));              
							recruit.setExpirationDate(rset.getDate("EXPIRATIONDATE"));      
							
							at = new Attachment();
							at.setAttachmentNo(rset.getInt("ATTACHMENT_NO"));
							at.setOriginName(rset.getString("ORIGIN_NAME"));
							at.setChangeName(rset.getString("CHANGE_NAME"));
							at.setFilePath(rset.getString("FILE_PATH"));
							at.setUploadDate(rset.getDate("UPLOAD_DATE"));
							
							list.add(at);
							
						}
						
						hmap.put("recruit", recruit);
						hmap.put("fileList", list);
						
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}finally {
						close(pstmt);
						close(rset);
					}
					
					return hmap;
				}

				public int updateRecruitAttachment(Connection con, Attachment attachment) {
					PreparedStatement pstmt = null;
					int result = 0;
					
					String query = prop.getProperty("updateRecruitAttachment");

					try {
						pstmt = con.prepareStatement(query);
						pstmt.setString(1, attachment.getOriginName());
						pstmt.setString(2, attachment.getChangeName());
						pstmt.setString(3, attachment.getFilePath());
						pstmt.setInt(4, attachment.getBoardNo());
						
						result = pstmt.executeUpdate();
					
						
					} catch (SQLException e) {
						e.printStackTrace();
					} finally {
						close(pstmt);
					}
					
					return result;
				}
				//공고 수정시 상태값 심사 대기 부분으로 바꾸기
				public int updateStatus(Connection con, int recruitno) {
					int result2 = 0;
					PreparedStatement pstmt = null;
					String query = prop.getProperty("updateStatus");
					try {
						pstmt = con.prepareStatement(query);
						pstmt.setInt(1,  recruitno);
						
						result2 = pstmt.executeUpdate();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return result2;
				}

				public int updateCheckStatus(Connection con, int recruitno) {
					int result2 = 0;
					PreparedStatement pstmt = null;
					String query = prop.getProperty("updateStatus");
					String status = "심사대기";
					try {
						pstmt = con.prepareStatement(query);
						pstmt.setString(1,  status);
						pstmt.setInt(2,  recruitno);
						
						result2 = pstmt.executeUpdate();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return result2;
				}

				
							
				
				

/*
				public ArrayList<Recruit> searchNum(Connection con, String status, String searchValue, int bmemberno) {
					ArrayList<Recruit> list = null;
					ResultSet rset = null;
					PreparedStatement pstmt = null;
					int result = 0;
					
					String query = prop.getProperty("searchNum");
					
					try {
						pstmt = con.prepareStatement(query);
						pstmt.setString(1, status );
						pstmt.setString(2, searchValue);
						pstmt.setInt(3, bmemberno);
						
						result = pstmt.executeUpdate();
						
						list = new ArrayList<>();
						
						
							
							}
				}
						
					
					return list;
				*/
					
				
}
