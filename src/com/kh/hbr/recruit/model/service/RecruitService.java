package com.kh.hbr.recruit.model.service;
 
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;


import com.kh.hbr.board.jengukBoard.model.vo.Attachment;
import com.kh.hbr.member.model.dao.MemberDao;
import com.kh.hbr.member.model.vo.Member;
import com.kh.hbr.recruit.model.dao.RecruitDao;
import com.kh.hbr.recruit.model.vo.Recruit;

import static com.kh.hbr.common.JDBCTemplate.*;

public class RecruitService {

	public int insertRecruit(Recruit requestRecruit) {
		
		Connection con = getConnection();
		
		int result = new RecruitDao().insertRecruit(con, requestRecruit);
		
		////System.out.println("service : "+result);
		
		int result2 = 0;
		
		if(result > 0) {
			
			int recruitNo = new RecruitDao().findRecruitNo(con);
			
			if(recruitNo>0) {
				result2 = new RecruitDao().insertRecruitCheck(con,recruitNo);
				commit(con);
			}
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public ArrayList<Recruit> selectList(int memberNum) {
		
		Connection con = getConnection();
		
		ArrayList<Recruit> list = new RecruitDao().selectList(con, memberNum);
		
		//System.out.println("service :"+list);
		
		close(con);
		
		return list;
	}

	public Recruit recruitView(int recruitNum) {
		
		Connection con = getConnection();
		
		int result = 0;
		
		Recruit recruit = new RecruitDao().recruitVeiw(con,recruitNum);
		
		//System.out.println("뷰서비스 : " + recruit);
		if(result >0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return recruit;
	}

	


	
	public int deleteRecruit(int recruitNum) {
		Connection con = getConnection();
		
		int result = new RecruitDao().deleteRecruit(con, recruitNum);
		
		//System.out.println("deleteRecruit service : "+result);
		
		if(result > 0) {
			commit(con);
		}else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}



	//관리자 부분 공고심사 들어온 모든 리스트 불러오기
		public ArrayList<Recruit> approvedList() {
			
			Connection con = getConnection();
				//System.out.println("여긴왔니?");
				ArrayList<Recruit> approvedList = new RecruitDao().approvedList(con);
				
				close(con);
				
				return approvedList;
			}
		//수정 부분에서 클릭하고 마감일자 가져오기
		/*public Recruit updateRecruitComplete2(int registerNum) {
			Connection con = getConnection();
			
			Recruit result2 = new RecruitDao().updateRecruitComplete2(con, registerNum);
			
			//System.out.println("update service : "+result2);
			
			
			
			if(result2 !=null) {
				commit(con);
			}else {
				rollback(con);
			}
			
			close(con);
			
			return result2;
		}*/

		
		//채용공고 반려 메소드
		public int rejectRecruit(int recruitNo, String reasonDetail) {
			Connection con = getConnection();
			
			
			int recruit1 = new RecruitDao().rejectRecruit(con, recruitNo, reasonDetail);
			
			//System.out.println("업데이트 뷰서비스 : " + recruit1);
			if(recruit1 >0) {
				commit(con);
			}else {
				rollback(con);
			}
			
			close(con);
			
			//System.out.println("reject Service result : "+recruit1);
			
			return recruit1;
		}
		//반려 recruit 테이블 업데이트
		public int rejectRecruitUpdate(int recruitNo) {
			Connection con = getConnection();

			
			int recruit2 = new RecruitDao().rejectRecruitUpdate(con, recruitNo);
			
			//System.out.println("2업데이트 뷰서비스 : " + recruit2);
			if(recruit2 >0) {
				commit(con);
			}else {
				rollback(con);
			}
			
			close(con);
			
			//System.out.println("2reject Service result2 : "+recruit2);
			
			return recruit2;
		}

		public ArrayList<Recruit> listApproved() {
			Connection con = getConnection();
			
			ArrayList<Recruit> approvedList = new RecruitDao().listApproved(con);
			
			close(con);
			
			return approvedList;
		}
		//채용공고현황 리스트 메소드
		public ArrayList<Recruit> secondRejectList() {
			Connection con = getConnection();
			
			ArrayList<Recruit> approvedList = new RecruitDao().secondRejectList(con);
			
			close(con);
			
			return approvedList;
		}
		//승인처리 recruit_check update
		public int approvedRecruit(int recruitNo) {
			Connection con = getConnection();

			
			int result1 = new RecruitDao().approvedRecruit(con, recruitNo);
			
			//System.out.println("1업데이트 뷰서비스 : " + result1);
			if(result1 >0) {
				commit(con);
			}else {
				rollback(con);
			}
			
			close(con);
			
			//System.out.println("1승인 서비스 : "+result1);
			
			return result1;
		}
		//승인처리 recruit update
		public int approvedRecruitUpdate(int recruitNo) {
			Connection con = getConnection();

			
			int result2 = new RecruitDao().approvedRecruitUpdate(con, recruitNo);
			
			//System.out.println("2업데이트 뷰서비스 : " + result2);
			if(result2 >0) {
				commit(con);
			}else {
				rollback(con);
			}
			
			close(con);
			
			//System.out.println("2승인 서비스 : "+result2);
			
			return result2;
		}

		public int rejectItem(int recruitNo) {
			Connection con = getConnection();

			
			int recruit3 = new RecruitDao().rejectItem(con, recruitNo);
			
			//System.out.println("3업데이트 뷰서비스 : " + recruit3);
			if(recruit3 >0) {
				commit(con);
			}else {
				rollback(con);
			}
			
			close(con);
			
			//System.out.println("3rejectItem : "+recruit3);
			
			return recruit3;
		}
		
		//use_product table update
		public int approvedProductUpdate(int recruitNo, int productCount) {
			Connection con = getConnection();

			
			int result3 = new RecruitDao().approvedProductUpdate(con, recruitNo, productCount);
			
			//System.out.println("3승인시상품사용 테이블업데이트 뷰서비스 : " + result3);
			if(result3 >0) {
				commit(con);
			}else {
				rollback(con);
			}
			
			close(con);
			
			//System.out.println("3승인시 상품 사용 서비스 : "+result3);
			
			return result3;
		}
		//myItem table update
		public int approvedMyitemUpdate(int itemNo, int productCount) {
			Connection con = getConnection();

			
			int result4 = new RecruitDao().approvedMyitemUpdate(con, itemNo, productCount);
			
			//System.out.println("4승인시 마이아이템 테이블업데이트 뷰서비스 : " + result4);
			if(result4 >0) {
				commit(con);
			}else {
				rollback(con);
			}
			
			close(con);
			
			//System.out.println("4승인시 마이아이템 서비스 : "+result4);
			
			return result4;
		}
		//로고첨부로 인해 변경된 메소드
		public int insertRecruit(Recruit requestRecruit, ArrayList<Attachment> fileList) {
			Connection con = getConnection();
			
			int result = new RecruitDao().insertRecruit(con, requestRecruit);
			
			//System.out.println("service : "+result);
			
			int result2 = 0;
			
			int result3 = 0;//첨부파일
			int bid = new RecruitDao().selectCurrval(con);
			
			if(result > 0) {
				
				int recruitNo = new RecruitDao().findRecruitNo(con);
				
				if(recruitNo>0) {
					result2 = new RecruitDao().insertRecruitCheck(con,recruitNo);
					
					commit(con);
						if(bid > 0) {
							for(int i = 0; i < fileList.size(); i++) {
								fileList.get(i).setBoardNo((bid));				
								result3 += new RecruitDao().insertAttachment(con, fileList.get(i));
								
						}
						
					}
				}else {
				rollback(con);
				}
			
			}
			close(con);
			
			return result;

		 
		}
		
		//사진가져오기(보금자리 view)
		public HashMap<String, Object> selectThumbnailMap(int num) {
			Connection con = getConnection();
/*			int result = new JengukDao().updateCount(con, num);*/
			
			HashMap<String, Object> hmap = null;
			
				
		    hmap = new RecruitDao().selectThumbnailMap(con, num);
			
		   
			close(con);
			
			
			return hmap;
		}
		//기존 관리자 심사 view
		public Recruit checkRecruitView2(int recruitNum) {
			Connection con = getConnection();
			
			int result = 0;
			
			Recruit recruit = new RecruitDao().checkRecruitView2(con,recruitNum);
			
			//System.out.println("심사 서비스: " + recruit);
			if(result >0) {
				commit(con);
			}else {
				rollback(con);
			}
			
			close(con);
			
			return recruit;
		}
		//새로운 관리자 심사 view
		public HashMap<String, Object> checkRecruitView2Map(int num) {
			Connection con = getConnection();
			
			HashMap<String, Object> hmap = null;
			
				
		    hmap = new RecruitDao().checkRecruitView2Map(con, num);
			
		   
			close(con);
			
			
			return hmap;
		}
		////승인 받은것 삭제하는 부분 기존것
		public Recruit checkRecruitView(int recruitNum) {
			Connection con = getConnection();
			
			int result = 0;
			
			Recruit recruit = new RecruitDao().checkRecruitView(con,recruitNum);
			
			//System.out.println("재심사 서비스: " + recruit);
			if(result >0) {
				commit(con);
			}else {
				rollback(con);
			}
			
			close(con);
			
			return recruit;
		}
		//sorting list 승인
		public ArrayList<Recruit> sortingList1() {
			Connection con = getConnection();
			
			ArrayList<Recruit> approvedList = new RecruitDao().sortingList1(con);
			
			close(con);
			
			return approvedList;
		}
		//sorting list 거절
		public ArrayList<Recruit> sortingList2() {
			Connection con = getConnection();
			
			ArrayList<Recruit> approvedList = new RecruitDao().sortingList2(con);
			
			close(con);
			
			return approvedList;
		}
		//sorting list 대기
		public ArrayList<Recruit> sortingList3() {
			Connection con = getConnection();
			
			ArrayList<Recruit> approvedList = new RecruitDao().sortingList3(con);
			
			close(con);
			
			return approvedList;
		}
		//기존의 updateRegister 메소드
		public Recruit updateRegister(int updateRegisterNum) {
			Connection con = getConnection();
			
			int result = 0;
			
			Recruit recruit = new RecruitDao().updateRegister(con,updateRegisterNum);
			
			//System.out.println("업데이트 뷰서비스 : " + recruit);
			if(result >0) {
				commit(con);
			}else {
				rollback(con);
			}
			
			close(con);
			
			return recruit;
			
		}
		
		//기존의 updateRegister 메소드를 변경시킨건
		public HashMap<String, Object> selectThumbnail3Map(int num) {
			Connection con = getConnection();
						
			HashMap<String, Object> hmap = null;
						
			hmap = new RecruitDao().selectThumbnail3Map(con, num);
					   
			close(con);
						
			return hmap;
		}
		//기존 채용공고 update
		public int updateRecruitComplete(Recruit requestRecruit) {
			Connection con = getConnection();
			
			int result = new RecruitDao().updateRecruitComplete(con, requestRecruit);
			
			//System.out.println("update service : "+result);
			
			
			
			if(result > 0) {
				commit(con);
			}else {
				rollback(con);
			}
			
			close(con);
			
			return result;
		}
		//사진등록을한 채용공고 update
		public int updateRecruitComplete(Recruit requestRecruit, ArrayList<Attachment> fileList) {
			Connection con = getConnection();
			
			int result = new RecruitDao().updateRecruitComplete(con, requestRecruit);
			
			System.out.println("service : "+result);
			
			
			
			
			int result3 = 0;//첨부파일
			int bid = requestRecruit.getRecruitNo();
			
			if(result > 0) {
				
					commit(con);
						if(bid > 0) {
							for(int i = 0; i < fileList.size(); i++) {
								fileList.get(i).setBoardNo((bid));				
								result3 += new RecruitDao().updateRecruitAttachment(con, fileList.get(i));
								
						}
						
					}
				}else {
				rollback(con);
				}
			
			
			close(con);
			
			return result;

		}
		//채용공고 조회 리스트 공고번호 검색(공고상태가 전체가 아닐경우)
		/*public ArrayList<Recruit> searchNum(String status, String searchValue, int bmemberno) {
			Connection con = getConnection();
			
			ArrayList<Recruit> list = new RecruitDao().searchNum(con, status, searchValue, bmemberno);
			
			close(con);
			
			return list;
		}
*/
		//공고 수정시 재승인 부분
		public int updateStatus(int recruitno) {
			Connection con = getConnection();
			
			int result2 = new RecruitDao().updateStatus(con, recruitno);
			
			//System.out.println("update service : "+result);
			
			
			
			if(result2 > 0) {
				commit(con);
			}else {
				rollback(con);
			}
			
			close(con);
			
			return result2;
		}

		public int updateCheckStatus(int recruitno) {
			Connection con = getConnection();
			
			int result2 = new RecruitDao().updateCheckStatus(con, recruitno);
			
			//System.out.println("update service : "+result);
			
			
			
			if(result2 > 0) {
				commit(con);
			}else {
				rollback(con);
			}
			
			close(con);
			
			return result2;
		}
		
		//마지막 전체 닫기
}
	 

	



	

