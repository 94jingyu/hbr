package com.kh.hbr.recruit.model.service;

import static com.kh.hbr.common.JDBCTemplate.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import com.kh.hbr.recruit.model.dao.RecruitDao_Search;
import com.kh.hbr.recruit.model.vo.PageInfo;
import com.kh.hbr.recruit.model.vo.Recruit;

public class RecruitService_Search {

	//태원 채용공고페이지 selectSearchList
	public ArrayList<Recruit> selectSearchList() {

		Connection con = getConnection();

		ArrayList<Recruit> slist = new RecruitDao_Search().selectSearchList(con);

		close(con);

		return slist;
	}
	
	/*//채용공고 상세보기 클릭 
	public Recruit selectOne(int recruitNum) {
		
		Connection con = getConnection();
		
		//조회수 업데이트
		int result = 0;
		
		Recruit searchRecruit = new RecruitDao_Search().selectOne(con, recruitNum);
		
		if(searchRecruit != null) {
			result = new RecruitDao_Search().updateCount(con, recruitNum);
			if(result > 0) {
				commit(con);
				searchRecruit.setClickCount(searchRecruit.getClickCount() + 1);
			} else {
				rollback(con);
			}
		}
		
		close(con);
		
		return searchRecruit;
	}*/

	
	
	//채용공고 상세보기 사진포함
	public HashMap<String, Object> selectOneThumbnailMap(int num) {
		
		Connection con = getConnection();
		
		int result = new RecruitDao_Search().updateCount(con, num);
		
		
		HashMap<String, Object> hmap = null;
		
		
		if(result > 0) {
			commit(con);
			hmap = new RecruitDao_Search().selectOneThumbnailMap(con, num);
		} else {
			rollback(con);
		}
				
		
		close(con);
		
		
		return hmap;
	}

	//조건검색 리스트
	public ArrayList<Recruit> selectConditonList(String[] condition) {
		
		Connection con = getConnection();
		
		ArrayList<Recruit> list = new RecruitDao_Search().selectConditonList(con, condition);
		
		close(con);
		
		return list;
	}

	//페이징
	public int getListCount() {
		
		Connection con = getConnection();
		
		int listCount = new RecruitDao_Search().getListCount(con);
		
		close(con);
		
		return listCount;
	}
	
	//페이징
	public ArrayList<Recruit> selectSearchList(PageInfo pi) {

		Connection con = getConnection();
		
		ArrayList<Recruit> list = new RecruitDao_Search().selectSearchList(con, pi);
		
		close(con);
		
		return list;
	}

	



}









