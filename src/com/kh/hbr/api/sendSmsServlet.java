package com.kh.hbr.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

@WebServlet("/sendSms.api")
public class sendSmsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public sendSmsServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String phone = request.getParameter("phone");
		// System.out.println("수신 번호 : " + phone);

		// 실제 사용할 때는 아래 주석을 해제한다.
		//String randomNum = phone;

		String randomNum = new Coolsms().sms(phone);
			
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		new Gson().toJson(randomNum, response.getWriter());
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
